﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider
                .state('fwd-save-requirement', {
                    url: '/fwd-save-requirement/:Id',
                    templateUrl: 'forward-bidding/views/save-fwd-requirement.html',
                    params: {
                        reqObj: null,
                        prDetail: null,
                        prDetailsList: null,
                        prItemsList: null,
                        selectedTemplate: null,
                        selectedPRNumbers: null
                    }
                })
                .state('pages.profile.fwd-requirements', {
                    url: '/fwd-requirements',
                    templateUrl: 'forward-bidding/views/fwd-requirements.html',
                    params: { filters: null },
                    onEnter: function (store, $state) {
                        if (!(store.get('sessionid') && store.get('verified') && store.get('emailverified'))) { 
                            $state.go('pages.profile.profile-about');
                            swal("Warning", "Please verify your Credential documents/OTP", "warning");
                        }
                    },
                    onExit: function () {
                        //write code when you change state
                        ////console.log('exit');
                    },
                    resolve: {
                        loadPlugin: function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                {
                                    name: 'css',
                                    insertBefore: '#app-level',
                                    files: [
                                        'vendors/bower_components/lightgallery/light-gallery/css/lightGallery.css'
                                    ]
                                },
                                {
                                    name: 'vendors',
                                    files: [
                                        'vendors/bower_components/ofline/offline.js',
                                        'vendors/bower_components/lightgallery/light-gallery/js/lightGallery.min.js'
                                    ]
                                }
                            ]);
                        }
                    }
                }).state('pages.profile.fwd-vendor-leads', {
                    url: '/fwd-vendor-leads',
                    templateUrl: 'forward-bidding/views/fwd-vendor-leads.html',
                    onEnter: function (store, $state) {
                        ////console.log('entry');
                        if (store.get('sessionid') && store.get('verified') && store.get('emailverified')) {
                            ////console.log('verified user');
                        } else {
                            $state.go('pages.profile.profile-about');
                            swal("Warning", "Please verify your Credential documents/OTP", "warning");
                        }
                    },
                    onExit: function () {
                        //write code when you change state
                        ////console.log('exit');
                    },
                    resolve: {
                        loadPlugin: function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                {
                                    name: 'css',
                                    insertBefore: '#app-level',
                                    files: [
                                        'vendors/bower_components/lightgallery/light-gallery/css/lightGallery.css'
                                    ]
                                },
                                {
                                    name: 'vendors',
                                    files: [
                                        'vendors/bower_components/ofline/offline.js',
                                        'vendors/bower_components/lightgallery/light-gallery/js/lightGallery.min.js'
                                    ]
                                }
                            ]);
                        }
                    }
                }).state('fwd-consalidatedReport', {
                    url: '/fwd-consalidatedReport',
                    templateUrl: 'forward-bidding/views/fwd-consalidated-report.html'
                }).state('fwd-comparatives', {
                    url: '/fwd-comparatives/:reqID',
                    templateUrl: 'forward-bidding/views/fwd-comparatives.html'
                }).state('fwd-req-savingsPreNegotiation', {
                    url: '/fwd-req-savingsPreNegotiation/:Id',
                    templateUrl: 'forward-bidding/views/fwd-req-savingsPreNegotiation.html'
                }).state('fwd-req-savings', {
                    url: '/fwd-req-savings/:Id',
                    templateUrl: 'forward-bidding/views/fwd-req-savings.html'
                }).state('fwd-reqTechSupport', {
                    url: '/fwd-reqTechSupport/:reqId',
                    templateUrl: 'forward-bidding/views/fwd-reqTechSupport.html'
                }).state('fwd-reminders', {
                    url: '/fwd-reminders/:Id',
                    templateUrl: 'forward-bidding/views/fwd-reminders.html'
                }).state('fwd-reports', {
                    url: '/fwd-reports/:reqID',
                    templateUrl: 'forward-bidding/views/fwd-reports.html'
                }).state('fwd-bidhistory', {
                    url: '/fwd-bidhistory/:Id/:reqID/:isfwd',
                    templateUrl: 'forward-bidding/views/fwd-bidHistory.html'
                }).state('fwd-audit', {
                    url: '/fwd-audit/:reqID',
                    templateUrl: 'forward-bidding/views/fwd-audit.html'
                });
        }]);