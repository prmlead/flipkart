prmApp.constant('PRMForwardBidDomain', 'forward-bidding/svc/PRMForwardBid.svc/REST/');
prmApp.constant('signalRFwdHubName', 'fwdRequirementHub');
prmApp.service('PRMForwardBidService', ["PRMForwardBidDomain", "userService", "httpServices", "$window",
    function (PRMForwardBidDomain, userService, httpServices, $window) {


        var PRMForwardBidService = this;

        PRMForwardBidService.getrequirementdata = function (params) {
            let url = PRMForwardBidDomain + 'getrequirementdata?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid;
            return httpServices.get(url);
        };

        PRMForwardBidService.postrequirementdata = function (params) {
            var myDate = new Date();
            var myEpoch = parseInt(myDate.getTime() / 1000);
            params.minBidAmount = 0;
            params.postedOn = "/Date(" + myEpoch + "000+0000)/";
            params.timeLeft = -1;
            params.price = -1;
            var requirement = {
                "requirement": {
                    "title": params.title,
                    "reqType": params.isRFP ? 2 : 1,
                    "description": params.description,
                    "category": params.category,
                    "subcategories": params.subcategories,
                    "urgency": params.urgency,
                    "budget": params.budget,
                    "attachmentName": params.attachmentName,
                    "deliveryLocation": params.deliveryLocation,
                    "taxes": params.taxes,
                    "paymentTerms": params.paymentTerms,
                    "lotId": params.lotId,
                    "requirementID": params.requirementID,
                    "customerID": params.customerID,
                    "isClosed": params.isClosed,
                    "endTime": null,
                    "sessionID": params.sessionID,
                    "errorMessage": "",
                    "timeLeft": -1,
                    "price": -1,
                    "auctionVendors": params.auctionVendors,
                    "startTime": null,
                    "status": "",
                    "postedOn": "/Date(" + myEpoch + "000+0000)/",
                    "custLastName": params.customerLastname,
                    "custFirstName": params.customerFirstname,
                    "deliveryTime": params.deliveryTime,
                    "includeFreight": params.includeFreight,
                    "inclusiveTax": params.inclusiveTax,
                    "minBidAmount": 0,
                    "checkBoxEmail": params.checkBoxEmail,
                    "checkBoxSms": params.checkBoxSms,
                    "quotationFreezTime": params.quotationFreezTime,
                    "currency": params.currency,
                    "timeZoneID": params.timeZoneID,
                    "listRequirementItems": params.listRequirementItems,
                    "isTabular": params.isTabular,
                    "reqComments": params.reqComments,
                    "itemsAttachment": params.itemsAttachment,
                    "isSubmit": params.isSubmit,
                    "isQuotationPriceLimit": params.isQuotationPriceLimit,
                    "quotationPriceLimit": params.quotationPriceLimit,
                    "noOfQuotationReminders": params.noOfQuotationReminders,
                    "remindersTimeInterval": params.remindersTimeInterval,
                    "custCompID": params.custCompID,
                    "customerEmails": params.customerEmails,
                    "customerCompanyName": params.customerCompanyName,
                    "deleteQuotations": params.deleteQuotations,
                    "indentID": params.indentID,
                    "expStartTime": params.expStartTime,
                    "cloneID": params.cloneID,
                    "isDiscountQuotation": params.isDiscountQuotation,
                    "contactDetails": params.contactDetails,
                    "generalTC": params.generalTC,
                    "isRevUnitDiscountEnable": params.isRevUnitDiscountEnable,
                    "multipleAttachments": params.multipleAttachments,
                    "contractStartTime": params.contractStartTime,
                    "contractEndTime": params.contractEndTime,
                    "isContract": params.isContract,
                    "prCode": params.prCode,
                    "PR_ID": params.PR_ID,
                    "PR_ITEM_IDS": params.selectedPRItemIds,
                    "isTechScoreReq": params.isTechScoreReq,
                    "auditComments": params.auditComments,
                    "biddingType": params.biddingType,
                    "templateId": params.templateId,
                    "projectName": params.projectName,
                    "projectDetails": params.projectDetails,
                    "prNumbers": params.PR_NUMBERS
                }, "attachment": params.attachment
            };
            let url = PRMForwardBidDomain + 'requirementsave';
            return httpServices.post(url, requirement);
        };

        PRMForwardBidService.deleteAttachment = function (Attaachmentparams) {
            let url = PRMForwardBidDomain + 'deleteattachment';
            return httpServices.post(url, Attaachmentparams);
        };

        PRMForwardBidService.getPreviousItemPrice = function (itemDetails) {
            let url = PRMForwardBidDomain + 'itempreviousprice?companyid=' + itemDetails.compID + '&productname=' + itemDetails.productIDorName.trim() + '&productno=' + itemDetails.productNo.trim() + '&brand=' + itemDetails.productBrand.trim() + '&sessionid=' + itemDetails.sessionID;
            return httpServices.get(url);
        };

        PRMForwardBidService.GetLastPrice = function (itemDetails) {
            let url = PRMForwardBidDomain + 'getlastprice?productIDorName=' + itemDetails.productIDorName.trim() + '&companyid=' + itemDetails.compID + '&sessionid=' + itemDetails.sessionID;
            return httpServices.get(url);
        };

        PRMForwardBidService.GetCompanyLeads = function (params) {
            let url = PRMForwardBidDomain + 'getcompanyleads?userid=' + params.userid + '&searchstring=' + params.searchstring + '&searchtype=' + params.searchtype + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.GetReqDepartments = function (userid, reqid, sessionid) {
            let url = PRMForwardBidDomain + 'getreqdepartments?userid=' + userid + '&reqid=' + reqid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.SaveReqDepartments = function (params) {
            let url = PRMForwardBidDomain + 'savereqdepartments';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.GetReqDeptDesig = function (userid, reqid, sessionid) {
            var url = PRMForwardBidDomain + 'getreqdeptdesig?userid=' + userid + '&reqid=' + reqid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.SaveReqDeptDesig = function (params) {
            var url = PRMForwardBidDomain + 'savereqdeptdesig';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.SaveRequirementTerms = function (params) {
            let url = PRMForwardBidDomain + 'saverequirementterms';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.GetRequirementTerms = function (userid, reqid, sessionID) {
            let url = PRMForwardBidDomain + 'getrequirementterms?userid=' + userid + '&reqid=' + reqid + '&sessionid=' + sessionID;
            return httpServices.get(url);
        };

        PRMForwardBidService.DeleteRequirementTerms = function (params) {
            let url = PRMForwardBidDomain + 'deleterequirementterms';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.saveRequirementSetting = function (params) {
            let url = PRMForwardBidDomain + 'saverequirementsetting';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.getRequirementSettings = function (params) {
            let url = PRMForwardBidDomain + 'requirementsettings?reqid=' + params.reqid + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.getRequirementVendorAnalysis = function (params) {
            let url = PRMForwardBidDomain + 'requirementvendoranalysis?vendorids=' + params.vendorids + '&itemids=' + params.itemids + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.uploadRequirementItemsSaveExcel = function (params) {
            var url = PRMForwardBidDomain + 'uploadrequirementitemssaveexcel';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.getmyAuctions = function (params) {
            if (!params.onlyrfq) {
                params.onlyrfq = 0;
            }

            if (!params.onlyrfp) {
                params.onlyrfp = 0;
            }
            let url = PRMForwardBidDomain + 'getmyauctions?userid=' + params.userid + '&fromDate=' + params.fromDate + '&toDate=' + params.toDate + '&status=' + params.status + '&search=' + params.search + '&allBuyer=' + params.allBuyer + '&page=' + params.page + '&pagesize=' + params.pagesize + '&sessionid=' + params.sessionid + '&onlyrfq=' + params.onlyrfq + '&onlyrfp=' + params.onlyrfp;
            return httpServices.get(url);
        };

        PRMForwardBidService.getrequirementsreport = function (params) {
            var url = PRMForwardBidDomain + 'getrequirementsreport?userid=' + params.userid + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.SaveRunningItemPrice = function (params) {
            let url = PRMForwardBidDomain + 'saverunningitemprice';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.GetBidHistory = function (params) {
            let url = PRMForwardBidDomain + 'getbidhistory?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid;
            return httpServices.get(url);
        };

        PRMForwardBidService.getpricecomparison = function (params) {
            let url = PRMForwardBidDomain + 'getpricecomparison?reqid=' + params.reqid + "&userID=" + params.userid + "&sessionid=" + params.sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.itemwiseselectvendor = function (params) {
            let url = PRMForwardBidDomain + 'itemwiseselectvendor';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.updateStatus = function (params) {
            let url = PRMForwardBidDomain + 'updatestatus';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.uploadquotationsfromexcel = function (params) {
            let url = PRMForwardBidDomain + 'uploadquotationsfromexcel';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.uploadclientsidequotation = function (params) {
            let url = PRMForwardBidDomain + 'uploadclientsidequotation';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.getrevisedquotations = function (params) {
            let url = PRMForwardBidDomain + 'getrevisedquotations';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.updatedeliverdate = function (params) {
            let url = PRMForwardBidDomain + 'updateexpdelandpaydate';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.updatepaymentdate = function (params) {
            let url = PRMForwardBidDomain + 'updateexpdelandpaydate';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.checkForRestartNegotiation = function (params) {
            let url = PRMForwardBidDomain + 'checkForRestartNegotiation';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.SaveReqNegSettings = function (params) {
            let url = PRMForwardBidDomain + 'SaveReqNegSettings';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.makeabid = function (params) {
            let url = PRMForwardBidDomain + 'makeabid';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.revquotationupload = function (params) {
            let url = PRMForwardBidDomain + 'revquotationupload';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.isnegotiationended = function (reqid, sessionid) {
            let url = PRMForwardBidDomain + 'isnegotiationended?reqid=' + reqid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.DeleteVendorFromAuction = function (params) {
            let url = PRMForwardBidDomain + 'deletevendorfromauction';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.uploadQuotation = function (params) {
            let url = PRMForwardBidDomain + 'uploadquotation';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.UploadRequirementItemsCeilingSave = function (params) {
            let url = PRMForwardBidDomain + 'uploadrequirementitemsceilingsave';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.QuatationAprovel = function (params) {
            let url = PRMForwardBidDomain + 'quatationaprovel';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.GetReportsRequirement = function (params) {
            let url = PRMForwardBidDomain + 'getreportsrequirement?reqid=' + params.reqid + "&userid=" + params.userid + "&sessionid=" + params.sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.UpdatePriceCap = function (params) {
            let url = PRMForwardBidDomain + 'updatepricecap';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.savepricecap = function (params) {
            let url = PRMForwardBidDomain + 'savepricecap';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.EnableMarginFields = function (params) {
            let url = PRMForwardBidDomain + 'enablemarginfields';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.SaveDifferentialFactor = function (params) {
            let url = PRMForwardBidDomain + 'savedifferentialfactor';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.UpdateCBTime = function (params) {
            let url = PRMForwardBidDomain + 'updatecbtime';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.CBStopQuotations = function (params) {
            let url = PRMForwardBidDomain + 'cbstopquotations';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.CBMarkAsComplete = function (params) {
            let url = PRMForwardBidDomain + 'cbmarkascomplete';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.markAsCompleteREQ = function (params) {
            let url = PRMForwardBidDomain + 'markAsCompleteREQ';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.toggleCounterBid = function (params) {
            let url = PRMForwardBidDomain + 'togglecounterbid';
            return httpServices.post(url, params);
        };
        
        PRMForwardBidService.saveRequirementItemList = function (params) {
            let url = PRMForwardBidDomain + 'saverequirementitemlist';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.ItemQuotationApproval = function (params) {
            let url = PRMForwardBidDomain + 'itemquotationapproval';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.refreshRequirementCurrency = function (params) {
            let url = PRMForwardBidDomain + 'refreshrequirementcurrency?reqid=' + params.reqid + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.validateCurrencyRate = function (params) {
            let url = PRMForwardBidDomain + 'validatecurrencyrate?reqid=' + params.reqid + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.SendQuotationApprovalStatusEmail = function (params) {
            let url = PRMForwardBidDomain + 'sendquotationapprovalstatusemail';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.getReductionSettings = function (params) {
            let url = PRMForwardBidDomain + 'getReductionSettings?reqid=' + params.reqid + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.saveReductionLevelSetting = function (params) {
            let url = PRMForwardBidDomain + 'saveReductionLevelSetting';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.UploadQuotationForSelectVendorCeilingPrices = function (params) {
            let url = PRMForwardBidDomain + 'uploadQuotationForSelectVendorCeilingPrices';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.GetIsAuthorized = function (userid, reqid, sessionid) {
            let url = PRMForwardBidDomain + 'getisauthorized?userid=' + userid + '&reqid=' + reqid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.StartNegotiation = function (params) {
            var url = PRMForwardBidDomain + 'startNegotiation';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.GetReqData = function (params) {
            let url = PRMForwardBidDomain + 'getreqdata?reqid=' + params.reqid + "&sessionid=" + params.sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.SavePriceCap = function (params) {
            let url = PRMForwardBidDomain + 'savepricecap';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.getactiveleads = function (params) {
            let url = PRMForwardBidDomain + 'getactiveleads?userid=' + params.userid + '&page=' + params.page + '&limit=' + params.limit + "&sessionid=" + params.sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.getConsolidatedReport = function (fromDate, toDate) {
            let url = PRMForwardBidDomain + 'getconsolidatedreports?from=' + fromDate + '&to=' + toDate + '&userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMForwardBidService.GetReqReportForExcel = function (reqid, sessionid) {
            let url = PRMForwardBidDomain + 'getreqreportforexcel?reqid=' + reqid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.GetPriceComparisonPreNegotiation = function (params) {
            let url = PRMForwardBidDomain + 'getpricecomparisonprenegotiation?reqid=' + params.reqid + "&userID=" + params.userid + "&sessionid=" + params.sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.vendorreminders = function (params) {
            var url = PRMForwardBidDomain + 'vendorreminders';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.GetVendorReminders = function (params) {
            let url = PRMForwardBidDomain + 'getvendorreminders?reqid=' + params.reqid + "&userid=" + params.userid + "&sessionid=" + params.sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.downloadTemplate = function (template, userid, reqid, templateid) {
            if (!templateid) {
                templateid = 0;
            }

            let url = PRMForwardBidDomain + 'gettemplates?template=' + template + '&userid=' + userid + '&reqid=' + reqid + '&compID=' + userService.getUserCompanyId() + '&templateid=' + templateid + '&sessionid=' + userService.getUserToken();
            let data = httpServices.get(url).then(function (response) {
                if (response) {
                    response = b64toBlob(response, "application/ vnd.openxmlformats - officedocument.spreadsheetml.sheet");
                    var linkElement = document.createElement('a');
                    try {
                        //var blob = b64toBlob(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");//new Blob([response], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                        var url = $window.URL.createObjectURL(response);

                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute("download", template + ".xlsx");

                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    }
                    catch (ex) {
                        console.log("date error");
                    }
                    return response;
                }
            }, function (result) {
                //console.log("date error");
            });
        };

        function b64toBlob(b64Data, contentType, sliceSize) {
            contentType = contentType || '';
            sliceSize = sliceSize || 512;

            var byteCharacters = atob(b64Data);
            var byteArrays = [];

            for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                var slice = byteCharacters.slice(offset, offset + sliceSize);

                var byteNumbers = new Array(slice.length);
                for (var i = 0; i < slice.length; i++) {
                    byteNumbers[i] = slice.charCodeAt(i);
                }

                var byteArray = new Uint8Array(byteNumbers);

                byteArrays.push(byteArray);
            }

            var blob = new Blob(byteArrays, { type: contentType });
            return blob;
        }

        PRMForwardBidService.getLiveBiddingReport = function (reqID) {
            let url = PRMForwardBidDomain + 'getlivebiddingreport?reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMForwardBidService.getLiveBiddingReport = function (reqID) {
            let url = PRMForwardBidDomain + 'getlivebiddingreport?reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMForwardBidService.GetReqDetails = function (reqID) {
            let url = PRMForwardBidDomain + 'getreqdetails?reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMForwardBidService.getItemWiseReport = function (reqID) {
            let url = PRMForwardBidDomain + 'getitemwisereport?reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMForwardBidService.getDeliveryDateReport = function (reqID) {
            let url = PRMForwardBidDomain + 'deliverytimelinereport?reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMForwardBidService.getPaymentTermsReport = function (reqID) {
            let url = PRMForwardBidDomain + 'paymenttermsreport?reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMForwardBidService.GetBidHistory = function (params) {
            let url = PRMForwardBidDomain + 'getbidhistory?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid;
            return httpServices.get(url);
        };

        PRMForwardBidService.GetCBPricesAudit = function (reqid, vendorid, sessionid) {
            let url = PRMForwardBidDomain + 'getcbpricesaudit?reqid=' + reqid + '&vendorid=' + vendorid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.GetItemBidHistory = function (params) {
            let url = PRMForwardBidDomain + 'GetItemBidHistory?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid;
            return httpServices.get(url);
        };

        PRMForwardBidService.getrequirementaudit = function (reqID) {
            let url = PRMForwardBidDomain + 'getrequirementaudit?reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        return PRMForwardBidService;

}]);