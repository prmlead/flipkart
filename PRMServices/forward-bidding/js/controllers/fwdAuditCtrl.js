prmApp
    // =========================================================================
    // COMMON FORMS
    // =========================================================================

    .controller('fwdAuditCtrl', ["$state", "$stateParams", "$scope", "PRMForwardBidService", "userService",
        "$http", "$window", "domain", "fileReader", "growlService", "$log", "$filter", "ngDialog",
        function ($state, $stateParams, $scope, PRMForwardBidService, userService,
            $http, $window, domain, fileReader, growlService, $log, $filter, ngDialog) {
            $scope.auditLogs = [];
            $scope.reqId = $stateParams.reqID;
            $scope.requirementDetails = {};
            $scope.auditData = [];
            $scope.auditDetails = [];
            $scope.filteredDetails = [];
            $scope.getAuditData = function () {
                if ($scope.reqId) {
                    PRMForwardBidService.getrequirementaudit($scope.reqId)
                        .then(function (response) {
                            console.log(response);
                            if (response.errorMessage) {
                                console.log(response.errorMessage);
                            } else {
                                //$scope.createAuditTable(response);
                                $scope.auditData = response[0].Value;
                                $scope.auditDetails = response[1].Value;
                            }

                        });
                }
            };

            $scope.getAuditData();

            $scope.getAuditDetails = function (audit) {
                $scope.auditData.forEach(function (audit1, vI) {
                    audit1.expanded = false;
                });

                audit.expanded = true;
                $scope.filteredDetails = _.filter($scope.auditDetails, function (detail) {
                    return detail.auditVersion === audit.auditVersion;
                });

                if ($scope.filteredDetails) {
                    $scope.filteredDetails.forEach(function (val, vI) {
                        val.dateCreated1 = userService.toLocalDate(val.dateCreated);
                    });
                } else {
                    $scope.filteredDetails = [];
                }
            };

            

            $scope.getRequirementDetails = function () {
                PRMForwardBidService.getrequirementdata({ "reqid": $scope.reqId, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                    .then(function (response) {
                        if (response) {
                            $scope.requirementDetails = response;
                        }
                    });
            };
            
            $scope.getRequirementDetails();

        }]);