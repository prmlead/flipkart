﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using PRMServices.Models;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMForwardBid
    {
        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getrequirementdata?reqid={reqID}&userid={userID}&sessionid={sessionID}")]
        Requirement GetRequirementData(int reqID, int userID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "requirementdatalite?reqid={reqID}&sessionid={sessionID}")]
        RequirementLite GetRequirementDataLite(int reqID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "requirementvendoranalysis?vendorids={vendorids}&itemids={itemids}&sessionid={sessionid}")]
        KeyValuePair[] GetRequirementVendorAnalysis(string vendorids, string itemids, string sessionid);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "requirementsettings?reqid={reqid}&sessionid={sessionid}")]
        List<RequirementSetting> GetRequirementSettings(int reqid, string sessionid);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getrequirementterms?userid={userID}&reqid={reqID}&sessionid={sessionID}")]
        List<RequirementTerms> GetRequirementTerms(int reqID, int userID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getreqdepartments?userid={userID}&reqid={reqID}&sessionid={sessionID}")]
        List<ReqDepartments> GetReqDepartments(int userID, int reqID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanyleads?userid={userID}&searchstring={searchString}&searchtype={searchType}&sessionid={sessionID}")]
        List<Requirement> GetCompanyLeads(int userID, string searchString, string searchType, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getlastprice?companyid={companyid}&productIDorName={productIDorName}&sessionid={sessionid}")]
        List<LastPrices> GetLastPrice(int companyid, string productIDorName, string sessionid);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "itempreviousprice?productname={productname}&productno={productno}&brand={brand}&companyid={companyid}&sessionid={sessionid}")]
        VendorDetails ItemPreviousPrice(string productname, string productno, string brand, int companyid, string sessionid);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getmyauctions?userid={userID}&fromDate={fromDate}&toDate={toDate}&status={status}&search={search}&allBuyer={allBuyer}&page={page}&pagesize={pagesize}&sessionid={sessionID}&onlyrfq={onlyrfq}&onlyrfp={onlyrfp}")]
        List<Requirement> GetMyAuctions(int userID, string fromDate, string toDate, string status, string search, string allBuyer, int page, int pagesize, string sessionID, int onlyrfq = 0, int onlyrfp = 0);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getrequirementsreport?userid={userID}&sessionid={sessionID}")]
        List<ReportData> GetRequirementsReport(int userID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getbidhistory?reqid={reqID}&userID={userID}&sessionid={sessionID}")]
        List<Comment> GetBidHistory(int reqID, int userID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getpricecomparison?reqid={reqID}&userid={userID}&sessionid={sessionID}")]
        PriceComparison GetPriceComparison(int reqID, int userID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "isnegotiationended?reqid={reqID}&sessionid={sessionID}")]
        Response IsNegotiationEnded(int reqID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getreportsrequirement?reqid={reqID}&userid={userID}&sessionid={sessionID}")]
        Requirement GetReportsRequirement(int reqID, int userID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "refreshrequirementcurrency?reqid={reqid}&sessionid={sessionid}")]
        Response RefreshRequirementCurrency(int reqid, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "validatecurrencyrate?reqid={reqid}&sessionid={sessionid}")]
        Response ValidateCurrencyRate(int reqid, string sessionid);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getReductionSettings?reqid={reqid}&sessionid={sessionid}")]
        List<ReductionSetting> getReductionSettings(int reqid, string sessionid);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getisauthorized?userid={userID}&reqid={reqID}&sessionid={sessionID}")]
        Response GetIsAuthorized(int userID, int reqID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getreqdata?reqid={reqID}&sessionid={sessionID}")]
        Requirement GetReqData(int reqID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getactiveleads?userid={userID}&sessionid={sessionID}")]
        List<Requirement> GetActiveLeads(int userID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getconsolidatedreports?from={from}&to={to}&userid={userID}&sessionid={sessionID}")]
        List<ConsolidatedReport> GetConsolidatedReports(string sessionID, string from, string to, int userID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getreqreportforexcel?reqid={reqID}&sessionid={sessionID}")]
        ExcelRequirement GetReqReportForExcel(int reqID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getpricecomparisonprenegotiation?reqid={reqID}&userid={userID}&sessionid={sessionID}")]
        PriceComparison GetPriceComparisonPreNegotiation(int reqID, int userID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getvendorreminders?reqid={reqID}&userid={userID}&sessionid={sessionID}")]
        List<Reminders> GetVendorReminders(int reqID, int userID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "gettemplates?template={template}&compid={compID}&userid={userID}&reqid={reqID}&from={fromDate}&to={toDate}&templateid={templateid}&sessionid={sessionID}")]
        string GetTemplates(string template, int compID, int userID, int reqID, string fromDate, string toDate, int templateid, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getlivebiddingreport?reqid={reqID}&count={count}&sessionid={sessionID}")]
        LiveBidding[] GetLiveBiddingReport(int reqID, int count, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getreqdetails?reqid={reqID}&sessionid={sessionID}")]
        ReportsRequirement GetReqDetails(int reqID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getitemwisereport?reqid={reqID}&sessionid={sessionID}")]
        ItemWiseReport[] GetItemWiseReport(int reqID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "deliverytimelinereport?reqid={reqID}&sessionid={sessionID}")]
        DeliveryTimeLine[] GetDeliveryTimeLineReport(int reqID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "paymenttermsreport?reqid={reqID}&sessionid={sessionID}")]
        DeliveryTimeLine[] GetPaymentTermsReport(int reqID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcbpricesaudit?reqid={reqID}&vendorid={vendorID}&sessionid={sessionID}")]
        List<CBPrices> GetCBPricesAudit(int reqID, int vendorID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetItemBidHistory?reqid={reqID}&userID={userID}&sessionid={sessionID}")]
        List<RequirementItems> GetItemBidHistory(int reqID, int userID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getrequirementaudit?reqid={reqID}&sessionid={sessionID}")]
        Dictionary<string, List<Audit>> GetRequirementAudit(int reqID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "uploadrequirementitemssaveexcel")]
        List<RequirementItems> uploadRequirementItemsSaveExcel(int reqID, bool isrfp, int userID, int compId, string sessionID, byte[] requirementItemsAttachment, int templateid);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "vendorreminders")]
        Response VendorReminders(int reqID, int userID, string message, int[] vendorIDs, string[] vendorCompanyNames, string sessionID, string requestType);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savepricecap")]
        Response SavePriceCap(int reqID, List<RequirementItems> listReqItems,
            double price, int isDiscountQuotation, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.WrappedRequest,
       UriTemplate = "startNegotiation")]
        Response StartNegotiation(int reqID, int userID, string sessionID);

        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, UriTemplate = "uploadQuotationForSelectVendorCeilingPrices")]
        Response UploadQuotationForSelectVendorCeilingPrices(List<RequirementItems> quotationObject, int userID, int reqID, string sessionID, double price,
           double tax, double freightcharges, double vendorBidPrice, string warranty, string payment, string duration,
           string validity, int revised, string DesfileName, double discountAmount, List<RequirementTaxes> listRequirementTaxes,
           string otherProperties, string gstNumber, double packingCharges, double packingChargesTaxPercentage, double packingChargesWithTax,
           double installationCharges, double installationChargesTaxPercentage, double installationChargesWithTax, double revpackingCharges, double revinstallationCharges,
           double revpackingChargesWithTax, double revinstallationChargesWithTax, string selectedVendorCurrency,
           string uploadType, List<FileUpload> multipleAttachments,
           double freightCharges,
           double freightChargesTaxPercentage,
           double freightChargesWithTax,
           double revfreightCharges,
           double revfreightChargesWithTax,
           string INCO_TERMS, int TaxFiledValidation, string isVendAckChecked, int surrogateId = 0, string surrogateComments = "", string quotFreezeTime = "");

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "saveReductionLevelSetting")]
        Response saveReductionLevelSetting(ReductionSetting reductionSetting);

        [OperationContract]
        [WebInvoke(Method = "POST",
       BodyStyle = WebMessageBodyStyle.WrappedRequest,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "sendquotationapprovalstatusemail")]
        Response SendQuotationApprovalStatusEmail(List<RequirementItems> items, int vendorId, int reqId, int customerId, string sessionId);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "itemquotationapproval")]
        Response ItemQuotationApproval(int reqId, int customerId, int vendorId, int itemId, bool value, string comment, string sessionId);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "saverequirementitemlist")]
        Response SaveRequirementItemList(RequirementItems[] requirementitemlist, int reqid, int vendorid, int user, string sessionid);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "togglecounterbid")]
        Response ToggleCounterBid(int reqId, int isCounterBid, int user, string sessionId);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "markAsCompleteREQ")]
        Response MarkAsCompleteREQ(int isREQCompleted, int userID, int reqID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "cbstopquotations")]
        Response CBStopQuotations(int stopCBQuotations, int userID, int reqID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "cbmarkascomplete")]
        Response CBMarkAsComplete(int isCBCompleted, int userID, int reqID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "updatecbtime")]
        Response UpdateCBTime(DateTime cbEndTime, int userID, int reqID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savedifferentialfactor")]
        Response SaveDifferentialFactor(int reqID, int customerID, int vendorID, decimal value, string reason, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
       BodyStyle = WebMessageBodyStyle.WrappedRequest,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "enablemarginfields")]
        Response EnableMarginFields(int isEnabled, int reqID, string sessionID);

        
        [OperationContract]
        [WebInvoke(Method = "POST",
       BodyStyle = WebMessageBodyStyle.WrappedRequest,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "updatepricecap")]
        Response UpdatePriceCap(int uID, int reqID, double price, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "quatationaprovel")]
        Response QuatationAprovel(int reqID, int customerID, int vendorID, bool value, string reason, string sessionID,
            string action, decimal technicalScore);

        [OperationContract]
        [WebInvoke(Method = "POST",
       BodyStyle = WebMessageBodyStyle.WrappedRequest,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "uploadrequirementitemsceilingsave")]
        Response UploadRequirementItemsCeilingSave(int reqID, string sessionID, byte[] requirementItemsAttachment, int templateid);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, UriTemplate = "uploadquotation")]
        Response UploadQuotation(List<RequirementItems> quotationObject, int userID, int reqID, string sessionID, double price,
            double tax, double freightcharges, double vendorBidPrice, string warranty, string payment, string duration,
            string validity, int revised, string DesfileName, double discountAmount, List<RequirementTaxes> listRequirementTaxes,
            string otherProperties, string gstNumber, double packingCharges, double packingChargesTaxPercentage, double packingChargesWithTax,
            double installationCharges, double installationChargesTaxPercentage, double installationChargesWithTax, double revpackingCharges, double revinstallationCharges,
            double revpackingChargesWithTax, double revinstallationChargesWithTax, string selectedVendorCurrency,
            string uploadType, List<FileUpload> multipleAttachments,

            double freightCharges,
            double freightChargesTaxPercentage,
            double freightChargesWithTax,
            double revfreightCharges,
            double revfreightChargesWithTax,
            string INCO_TERMS, int TaxFiledValidation, string isVendAckChecked, int surrogateId = 0, string surrogateComments = "", string quotFreezeTime = "");

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "deletevendorfromauction")]
        Response DeleteVendorFromAuction(int userID, int reqID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "revquotationupload")]
        Response RevQuotationUpload(int reqID, int userID, double price, byte[] quotation, string quotationName, string sessionID, double tax);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "makeabid")]
        Response MakeABid(int reqID, int userID, double price, byte[] quotation, string quotationName, string sessionID,
            double tax, double freightcharges, string warranty, string payment, string duration, string validity,
            List<RequirementItems> quotationObject, int revised, double priceWithoutTax, string type, double discountAmount,
            List<RequirementTaxes> listRequirementTaxes, string otherProperties, bool ignorevalidations, double itemrevtotalprice, double vendorBidPrice,
            double revfreightCharges, double revpackingCharges, double revinstallationCharges, int surrogateId = 0, string surrogateComments = "");

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Wrapped,
        UriTemplate = "SaveReqNegSettings")]
        Response SaveReqNegSettings(int reqID, string sessionID, string negotiationDuretion, NegotiationSettings NegotiationSettings);

        [OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.WrappedRequest,
       UriTemplate = "checkForRestartNegotiation")]
        Response checkForRestartNegotiation(int reqID, int userID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "updateexpdelandpaydate")]
        Response UpadteExpDelAndPayDate(int reqid, DateTime date, string type, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "getrevisedquotations")]
        Response GetRevisedQuotations(int reqID, int userID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "uploadclientsidequotation")]
        List<RequirementItems> UploadClientSideQuotation(int reqID, int userID, string sessionID, byte[] quotationAttachment);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest, 
        ResponseFormat = WebMessageFormat.Json, 
        UriTemplate = "uploadquotationsfromexcel")]
        Response UploadQuotationsFromExcel(int reqID, int userID, string sessionID, byte[] quotationAttachment);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "saverunningitemprice")]
        Response SaveRunningItemPrice(List<RequirementItems> itemsList, int userID, int reqID, double price, double vendorBidPrice, double freightcharges,
            double revfreightCharges, double revpackingCharges, double revinstallationCharges);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "updatestatus")]
        Response UpdateStatus(int reqid, int userid, string status, string type, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
       BodyStyle = WebMessageBodyStyle.WrappedRequest,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "itemwiseselectvendor")]
        Response ItemWiseSelectVendor(int userID, int vendorID, int reqID, int itemID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
       BodyStyle = WebMessageBodyStyle.WrappedRequest,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "savereqdepartments")]
        Response SaveReqDepartments(List<ReqDepartments> listReqDepartments, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "deleteattachment")]
        Response DeleteAttachment(int userID, int reqID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savereqdeptdesig")]
        Response SaveReqDeptDesig(List<ReqDepartments> listReqDepartments, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "saverequirementterms")]
        Response SaveRequirementTerms(List<RequirementTerms> listRequirementTerms, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "deleterequirementterms")]
        Response DeleteRequirementTerms(List<int> listTerms, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
       BodyStyle = WebMessageBodyStyle.WrappedRequest,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "saverequirementsetting")]
        Response SaveRequirementSetting(RequirementSetting requirementSetting);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "requirementsave")]
        Response RequirementSave(Requirement requirement, byte[] attachment);
    }
}
