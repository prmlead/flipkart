﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ServiceModel.Activation;
using PRMServices.Models;
using PRMServices.SQLHelper;
using CORE = PRM.Core.Common;
using System.Web;
using PRMServices.Common;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Configuration;
using System.IO;
using System.Data;
using OfficeOpenXml;
using OfficeOpenXml.DataValidation;
using OfficeOpenXml.Style;

using CATALOG = PRMServices.Models.Catalog;
using OfficeOpenXml;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using PRMServices.SignalR;
using PdfSharp.Pdf;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using System.Drawing;
using PRMServices.Models.Catalog;

namespace PRMServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMForwardBid : IPRMForwardBid
    {

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
        PRMCatalogService catalog = new PRMCatalogService();
        PRMServices prmService = new PRMServices();
        private readonly int decimal_round = Convert.ToInt32(ConfigurationManager.AppSettings["ROUNDING_DECIMALS"]);

        #region Services

        public Response RequirementSave(Requirement requirement, byte[] attachment)
        {
            //GetCurrencyConversionToDB(requirement);
            bool isMFCDTemplate = false;
            bool isCASSTemplate = false;
            List<models.PRMTemplateFields> templateFields = null;
            PRMCustomFieldService fieldService = new PRMCustomFieldService();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            ValidateSession(requirement.SessionID);
            if (requirement.ListRequirementItems != null && requirement.ListRequirementItems.Count > 0)
            {
                requirement.ListRequirementItems = requirement.ListRequirementItems.Where(i => !(i.ItemID <= 0 && i.IsDeleted > 0)).ToList();
            }

            if (requirement.TemplateId > 0)
            {
                templateFields = fieldService.GetTemplateFieldsTemp(requirement.TemplateId, string.Empty);
                if (templateFields != null && templateFields.Count > 0 && templateFields.Any(t => t.FIELD_NAME == "PRODUCT_CODE"))
                {
                    isCASSTemplate = templateFields.First(t => t.FIELD_NAME == "PRODUCT_CODE").FIELD_LABEL.ToUpper().Contains("CAS");
                    isMFCDTemplate = templateFields.First(t => t.FIELD_NAME == "PRODUCT_NUMBER").FIELD_LABEL.ToUpper().Contains("MFCD");
                }
            }

            int negotationDuration = requirement.NegotiationSettings != null ? Convert.ToInt32(requirement.NegotiationSettings.NegotiationDuration) : 15;
            Response response = new Response();
            string folderPath = HttpContext.Current.Server.MapPath(Utilities.FILE_URL);
            string requirementItemXML = GenerateEmailBody("SaveRequirementItemsXML");
            string reqPosting = "REQ_POSTING";
            string editReq = "EDIT_REQ";
            UserDetails customerDetails = new UserDetails();
            if (requirement.CloneID > 0)
            {
                requirement.RequirementID = 0;
                foreach (var item in requirement.ListRequirementItems)
                {
                    item.ItemID = 0;
                    item.PR_ID = string.Empty;
                    item.PR_ITEM_ID = string.Empty;
                }
            }

            requirement.PostedOn = DateTime.UtcNow;
            try
            {

                ValidateSession(requirement.SessionID);
                if (requirement.RequirementID > 0)
                {
                    Requirement oldReq = GetRequirementDataOfflinePrivate(requirement.RequirementID, requirement.CustomerID, requirement.SessionID);
                    var vendorsToRemove = GetVendorsToRemove(requirement, oldReq);
                    Response res = RemoveVendorFromAuction(vendorsToRemove, requirement.RequirementID, requirement.SessionID);
                    var itemsToRemove = GetItemsToRemove(requirement, oldReq);
                    Response res1 = RemoveItemFromAuction(itemsToRemove, requirement.RequirementID, requirement.SessionID, "");
                }

                string fileName = string.Empty;
                List<Attachment> CustomerListAttachment = new List<Attachment>();
                List<Attachment> VendorListAttachment = new List<Attachment>();
                var filenameTemp = string.Empty;
                if (requirement.MultipleAttachments != null && requirement.MultipleAttachments.Count > 0)
                {
                    foreach (FileUpload fd in requirement.MultipleAttachments)
                    {
                        fd.FileName = Regex.Replace(fd.FileName, @"[^0-9a-zA-Z.]+", "PRM_");
                        if (fd.FileStream.Length > 0 && !string.IsNullOrEmpty(fd.FileName))
                        {
                            var attachName = string.Empty;
                            long tick = DateTime.UtcNow.Ticks;
                            attachName = HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "req" + tick + "_user" + requirement.CustomerID + "_" + fd.FileName);
                            SaveFile(attachName, fd.FileStream);
                            attachName = "req" + tick + "_user" + requirement.CustomerID + "_" + fd.FileName;
                            Response res = SaveAttachment(attachName);
                            if (res.ErrorMessage != "")
                            {
                                response.ErrorMessage = res.ErrorMessage;
                            }

                            fd.FileID = res.ObjectID;
                            if (fd.FileID > 0)
                            {
                                Attachment singleAttachment = new Attachment(new MemoryStream(fd.FileStream), attachName);
                                CustomerListAttachment.Add(singleAttachment);
                                VendorListAttachment.Add(singleAttachment);
                            }
                        }
                        else if (fd.FileID > 0)
                        {
                            Attachment singleAttachment = null;
                            var fileData = DownloadFile(Convert.ToString(fd.FileID), requirement.SessionID);
                            if (fileData.FileStream != null && !string.IsNullOrEmpty(fileData.FileName))
                            {
                                singleAttachment = new Attachment(fileData.FileStream, fileData.FileName);
                            }

                            CustomerListAttachment.Add(singleAttachment);
                            VendorListAttachment.Add(singleAttachment);
                        }

                        fileName += Convert.ToString(fd.FileID) + ",";
                    }

                    fileName = fileName.Substring(0, fileName.Length - 1);
                }

                sd.Add("P_REQ_ID", requirement.RequirementID);
                sd.Add("P_BIDDING_TYPE", requirement.BiddingType);
                sd.Add("P_LOT_ID", requirement.LotId);
                sd.Add("P_REQ_TITLE", requirement.Title);
                sd.Add("P_REQ_TYPE", requirement.ReqType);
                sd.Add("P_REQ_DESC", requirement.Description);
                string categoryVal = string.Empty;
                foreach (string category in requirement.Category)
                {
                    categoryVal = category + ",";
                }
                if (!string.IsNullOrEmpty(categoryVal))
                {
                    categoryVal = categoryVal.Substring(0, categoryVal.Length - 1);

                }
                sd.Add("P_REQ_CATEGORY", categoryVal);
                sd.Add("P_REQ_URGENCY", requirement.Urgency);
                sd.Add("P_REQ_BUDGET", requirement.Budget);
                sd.Add("P_REQ_DELIVERY_LOC", requirement.DeliveryLocation);
                sd.Add("P_REQ_TAXES", "");
                sd.Add("P_REQ_PAYMENT_TERMS", requirement.PaymentTerms);
                sd.Add("P_CONT_START_TIME", requirement.ContractStartTime);
                sd.Add("P_CONT_END_TIME", requirement.ContractEndTime);
                sd.Add("P_REQ_SUBCATEGORIES", requirement.Subcategories);
                sd.Add("P_U_ID", requirement.CustomerID);
                sd.Add("P_CLOSED", requirement.IsClosed);
                sd.Add("P_REQ_ATTACHMENT", fileName);
                sd.Add("P_END_TIME", requirement.EndTime);
                sd.Add("P_START_TIME", requirement.StartTime);
                sd.Add("P_DELIVERY_TIME", requirement.DeliveryTime);
                requirement.IncludeFreight = true;
                sd.Add("P_INCLUDE_FREIGHT", Convert.ToInt32(requirement.IncludeFreight));
                requirement.InclusiveTax = ConfigurationManager.AppSettings["RANK_INCLUSIVE_TAX"] != null ? Convert.ToBoolean(ConfigurationManager.AppSettings["RANK_INCLUSIVE_TAX"]) : false;
                sd.Add("P_INCLUSIVE_TAX", Convert.ToInt32(requirement.InclusiveTax));
                sd.Add("P_REQ_COMMENTS", requirement.ReqComments);
                if (requirement.QuotationFreezTime == DateTime.MaxValue || requirement.QuotationFreezTime == DateTime.MinValue)
                {
                    requirement.QuotationFreezTime = DateTime.MaxValue;
                }

                sd.Add("P_QUOTATION_FREEZ_TIME", requirement.QuotationFreezTime);
                sd.Add("P_REQ_CURRENCY", requirement.Currency);
                sd.Add("P_REQ_TIMEZONE", requirement.TimeZoneID);
                sd.Add("P_REQ_IS_TABULAR", Convert.ToInt32(requirement.IsTabular));
                sd.Add("P_IS_QUOTATION_PRICE_LIMIT", Convert.ToInt32(requirement.IsQuotationPriceLimit));
                sd.Add("P_QUOTATION_PRICE_LIMIT", requirement.QuotationPriceLimit);
                sd.Add("P_NO_OF_QUOTATION_REMINDERS", requirement.NoOfQuotationReminders);
                sd.Add("P_REMINDERS_TIME_INTERVAL", requirement.RemindersTimeInterval);
                sd.Add("P_INDENT_ID", requirement.IndentID);
                sd.Add("P_EXP_START_TIME", requirement.ExpStartTime);
                sd.Add("P_IS_DISCOUNT_QUOTATION", requirement.IsDiscountQuotation);
                int isUnitPriceBidding = 0;
                if (requirement.IsTabular)
                {
                    isUnitPriceBidding = 1;
                }

                sd.Add("P_IS_UNIT_PRICE_BIDDING", Convert.ToInt32(isUnitPriceBidding));
                sd.Add("P_CONTACT_DETAILS", requirement.ContactDetails);
                sd.Add("P_GENERAL_TC", requirement.GeneralTC);
                sd.Add("P_IS_REV_UNIT_DISCOUNT_ENABLE", requirement.IsRevUnitDiscountEnable);
                sd.Add("P_IS_CONTRACT", requirement.IsContract);
                sd.Add("P_TECH_SCORE_REQ", requirement.IsTechScoreReq);
                sd.Add("P_AUDIT_COMMENTS", requirement.AuditComments);
                sd.Add("P_PROJECT_NAME", requirement.ProjectName ?? string.Empty);
                sd.Add("P_PROJECT_DETAILS", requirement.ProjectDetails ?? string.Empty);
                sd.Add("P_QUOTES_TO_BE_RECIEVED", requirement.AuctionVendors.Count > 0 ? requirement.AuctionVendors.Where(a => a.CompanyName != "PRICE_CAP").ToList().Count : -1);
                sd.Add("P_PR_NUMBERS", requirement.PRNumbers);
                DataSet ds = sqlHelper.SelectList("fwd_RequirementSave", sd);

                Attachment emilAttachmentReqPDF = null;
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    requirement.RequirementNumber = ds.Tables[0].Rows[0]["REQ_NUMBER"] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0]["REQ_NUMBER"]) : string.Empty;
                    if (requirement.RequirementID > 0 && requirement.DeleteQuotations)
                    {
                        string deleteRemindersQuery = string.Format("update fwd_requirementdetails set QUOTES_TO_BE_RECIEVED = 0 where REQ_ID = {0};DELETE FROM fwd_quotationreminders " +
                            " WHERE REQ_ID = {0};" +
                            " DELETE FROM fwd_quotationtaxes " +
                            " WHERE REQ_ID = {0};" +
                            " DELETE FROM fwd_quotations " +
                            " WHERE REQ_ID = {0};" +
                            " UPDATE fwd_auctiondetails SET IS_ACCEPTED = 0, VEND_INIT_PRICE = 0, VEND_RUN_PRICE = 0, QUOTATION_URL = null," +
                            " VEND_TAXES = 0, VEND_TOTAL_PRICE = 0, VEND_TOTAL_PRICE_RUNNING = 0, IS_QUOTATION_REJECTED = -1," +
                            " QUOTATION_REJECTED_REASON = '', VEND_FREIGHT = 0, REV_VEND_FREIGHT = 0, WARRANTY = '', PAYMENT = '', DURATION = ''," +
                            " VALIDITY = '', DISCOUNT = 0, NO_OF_QUOTATION_REMINDERS_SENT = 0, OTHER_PROPERTIES = ''," +
                            " REV_PRICE = 0, REV_VEND_TOTAL_PRICE = 0, GST_NUMBER = '', MULTIPLE_ATTACHMENTS = null," +
                            " IS_ACCEPTED_TC = 0,PACKING_CHARGES = 0,PACKING_CHARGES_TAX_PERCENTAGE = 0,PACKING_CHARGES_WITH_TAX = 0," +
                            " INSTALLATION_CHARGES = 0,INSTALLATION_CHARGES_TAX_PERCENTAGE = 0,INSTALLATION_CHARGES_WITH_TAX = 0, " +
                            " SELECTED_VENDOR_CURRENCY = '', " +
                            " REV_PRICE_CB = 0, " +
                            " REV_VEND_TOTAL_PRICE_CB = 0, " +
                            " REV_VEND_FREIGHT_CB = 0, " +
                            " VEND_TOTAL_PRICE_RUNNING_CB = 0, " +
                            " REV_PACKING_CHARGES_CB = 0, " +
                            " REV_INSTALLATION_CHARGES_CB = 0, " +
                            " VEND_REVISED_TOTAL_PRICE_CB = 0, " +
                            " VEND_FREIGHT_CB = 0, " +
                            " FREEZE_CB = 0, " +
                            " VEND_FREEZE_CB = 0, " +
                            " FREIGHT_CHARGES = 0, " +
                            " FREIGHT_CHARGES_TAX_PERCENTAGE = 0, " +
                            " FREIGHT_CHARGES_WITH_TAX = 0, " +
                            " REV_FREIGHT_CHARGES = 0, " +
                            " REV_FREIGHT_CHARGES_WITH_TAX = 0, " +
                            " PACKING_CHARGES_CB = 0, " +
                            " FREIGHT_CHARGES_CB = 0, " +
                            " REV_FREIGHT_CHARGES_CB = 0," +
                            " INCO_TERMS = '', " +
                            " DIFFERENTIAL_FACTOR = 0, IS_QUOT_LATE = 0" +
                            " WHERE REQ_ID = {0};" +
                            " DELETE FROM fwd_quotations " +
                            " WHERE REQ_ID = {0}", requirement.RequirementID);
                        DataSet ds1 = sqlHelper.ExecuteQuery(deleteRemindersQuery);
                    }
                }

                string queryWatchers = string.Empty;
                foreach (CustomerEmails customerEmail in requirement.CustomerEmails)
                {
                    queryWatchers += $"CALL fwd_SaveWatchers({response.ObjectID}, '{customerEmail.Mail}', {customerEmail.UserID}, {customerEmail.Status});";
                }

                if (!string.IsNullOrEmpty(queryWatchers))
                {
                    sqlHelper.ExecuteNonQuery_IUD(queryWatchers);
                }

                string quotationItems = string.Empty;
                if (requirement.ItemsAttachment == null)
                {
                    foreach (RequirementItems Item in requirement.ListRequirementItems)
                    {
                        CATALOG.CatalogResponse catalogItemID = new CATALOG.CatalogResponse();
                        if (Item.CatalogueItemID == 0)
                        {
                            CATALOG.Product reqProduct = new CATALOG.Product();
                            reqProduct.ProductId = 0;
                            reqProduct.ProductCode = string.Empty;
                            if (isCASSTemplate)
                            {
                                reqProduct.CasNumber = Item.ProductCode;
                            }
                            else
                            {
                                reqProduct.ProductCode = Item.ProductCode;
                            }

                            if (isMFCDTemplate)
                            {
                                reqProduct.MfcdCode = Item.ProductNo;
                            }
                            else
                            {
                                reqProduct.ProductNo = Item.ProductNo;
                            }

                            reqProduct.CompanyId = requirement.CustCompID;
                            reqProduct.ProductName = Item.ProductIDorName;
                            //reqProduct.ProductNo = Item.ProductNo;
                            reqProduct.ProdQty = Item.ProductQuantityIn;
                            reqProduct.IsValid = 1;
                            reqProduct.ModifiedBy = Convert.ToInt32(requirement.CustomerID);
                            catalogItemID = catalog.AddProduct(reqProduct, requirement.SessionID);
                            Item.CatalogueItemID = catalogItemID.ResponseId > 0 ? catalogItemID.ResponseId : catalogItemID.ObjectId;
                            Item.IsNewItem = true;
                            Item.IsCoreProductCategory = 1;
                        }
                    }

                    foreach (var item in requirement.ListRequirementItems)
                    {
                        item.ProductDescription = item.ProductDescription.Replace("'", "");
                        if (requirement.IsRFP)
                        {
                            item.ProductIDorName = item.ProductIDorName.Replace("'", "");
                            item.ProductDescription = item.ProductDescription.Replace("'", "");
                        }

                        var itemResponse = SaveRequirementItems(item, response.ObjectID, requirement.AuctionVendors, requirement.SessionID, requirement.CustomerID);
                        if (itemResponse.ErrorMessage == "")
                        {
                            if (requirement.IsTabular)
                            {
                                quotationItems += String.Format(requirementItemXML, item.ProductIDorName, item.ProductNo, item.ProductDescription, item.ProductQuantity, item.ProductBrand, item.OthersBrands);
                            }
                        }
                    }

                    //if (!string.IsNullOrEmpty(requirement.PR_ID))
                    //{
                    //    Response prRFQResponse = LinkPRWithRFQ(requirement.PR_ID, response.ObjectID,requirement.PR_ITEM_IDS);
                    //}
                }
                else
                {
                    DataTable currentData = new DataTable();
                    string sheetName = string.Empty;
                    using (MemoryStream ms = new MemoryStream())
                    {
                        ms.Write(requirement.ItemsAttachment, 0, requirement.ItemsAttachment.Length);
                        using (ExcelPackage package = new ExcelPackage(ms))
                        {
                            currentData = Utilities.WorksheetToDataTable(package.Workbook.Worksheets[1]);
                            sheetName = package.Workbook.Worksheets[1].Name;
                        }
                    }

                    if (sheetName.Equals("RequirementDetails", StringComparison.InvariantCultureIgnoreCase))
                    {
                        int count = 1;
                        List<RequirementItems> excelItems = new List<RequirementItems>();
                        foreach (DataRow row in currentData.Rows)
                        {
                            try
                            {
                                RequirementItems item = new RequirementItems();
                                item.ItemID = (row.IsNull("ItemID") || row["ItemID"] == DBNull.Value || string.IsNullOrEmpty(row["ItemID"].ToString().Trim())) ? 0 : Convert.ToInt32(row["ItemID"].ToString().Trim());
                                item.ProductIDorName = (row.IsNull("ProductName") || row["ProductName"] == DBNull.Value) ? string.Empty : Convert.ToString(row["ProductName"]).Trim();
                                item.ProductNo = (row.IsNull("ProductNumber") || row["ProductNumber"] == DBNull.Value) ? string.Empty : Convert.ToString(row["ProductNumber"]).Trim();
                                item.ProductDescription = (row.IsNull("Description") || row["Description"] == DBNull.Value) ? string.Empty : Convert.ToString(row["Description"]).Trim();
                                item.ProductQuantity = (row.IsNull("Quantity") || row["Quantity"] == DBNull.Value || string.IsNullOrEmpty(row["Quantity"].ToString().Trim())) ? 0 : Convert.ToDouble(row["Quantity"].ToString().Trim());
                                item.ProductQuantityIn = (row.IsNull("Units") || row["Units"] == DBNull.Value) ? string.Empty : Convert.ToString(row["Units"]).Trim();
                                item.ProductBrand = (row.IsNull("PreferredBrand") || row["PreferredBrand"] == DBNull.Value) ? string.Empty : Convert.ToString(row["PreferredBrand"]).Trim();
                                item.OthersBrands = (row.IsNull("OtherBrands") || row["OtherBrands"] == DBNull.Value) ? string.Empty : Convert.ToString(row["OtherBrands"]).Trim();
                                count++;
                                excelItems.Add(item);
                            }
                            catch
                            {
                                response.Message += "Row No. " + count + " has invalid values. Please check the excel sheet you uploaded.";
                                continue;
                            }
                        }

                        foreach (var item in excelItems)
                        {
                            var itemResponse = SaveRequirementItems(item, response.ObjectID, requirement.AuctionVendors, requirement.SessionID, requirement.CustomerID);
                            if (itemResponse.ErrorMessage == "")
                            {
                                if (requirement.IsTabular)
                                {
                                    quotationItems += String.Format(requirementItemXML, item.ProductIDorName, item.ProductNo, item.ProductDescription, item.ProductQuantity, item.ProductBrand, item.OthersBrands);
                                }
                            }
                        }
                    }
                }

                if (requirement.AuctionVendors.Count > 0)
                {
                    string vendorsQuery = string.Empty;
                    foreach (VendorDetails vendor in requirement.AuctionVendors)
                    {
                        //Response response1 = new Response();
                        //response1 = AddVendorToAuction(vendor, response.ObjectID, requirement.SessionID, requirement.CustomerID);
                        vendorsQuery += $"CALL fwd_AddVendorToAuction({response.ObjectID}, {vendor.VendorID}, {vendor.InitialPrice}, {requirement.CustomerID},'{requirement.GeneralTC}');";
                    }

                    if (!string.IsNullOrEmpty(vendorsQuery))
                    {
                        sqlHelper.ExecuteNonQuery_IUD(vendorsQuery);
                    }
                }

                string saveRequirementCatalogue = string.Empty;
                foreach (RequirementItems Item in requirement.ListRequirementItems)
                {
                    if (Item.IsNewItem)
                    {
                        saveRequirementCatalogue += $"CALL fwd_SaveRequirementCatalogue({response.ObjectID}, {requirement.CustomerID}, {Item.CatalogueItemID});";
                    }
                }

                if (!string.IsNullOrEmpty(saveRequirementCatalogue))
                {
                    sqlHelper.ExecuteNonQuery_IUD(saveRequirementCatalogue);
                }

                if (!string.IsNullOrEmpty(requirement.BiddingType) && requirement.BiddingType == "SPOT")
                {

                    sqlHelper.ExecuteNonQuery_IUD($"CALL fwd_SaveSpotBidding({response.ObjectID});");//,{ requirement.CustomerID},'{requirement.ExpStartTime}','{requirement.GeneralTC}','{requirement.}'
                }


                customerDetails = Utilities.GetUserDetails(requirement.CustomerID);
                requirement.ListRequirementItems = requirement.ListRequirementItems.OrderByDescending(l => l.IsCoreProductCategory).ToList();
                if (requirement.IsSubmit == 1)
                {
                    long ticks1 = DateTime.UtcNow.Ticks;
                    string PDFfileName = string.Empty;
                    PDFfileName = "reqPDF" + response.ObjectID + "_" + ticks1 + ".pdf";
                    filenameTemp = "reqPDF" + response.ObjectID + "_" + ticks1 + ".pdf";
                    int flag = 0;
                    List<KeyValuePair<string, DataTable>> dtbl = FwdPdfUtilities.MakeDataTable(response.ObjectID, requirement, quotationItems, customerDetails, folderPath, flag, requirement.BiddingType);
                    FwdPdfUtilities.ExportDataTableToPdf(dtbl, @folderPath + "reqPDF" + response.ObjectID + "_" + ticks1 + ".pdf", response.ObjectID, requirement, flag);
                    Response responce = SaveAttachment(PDFfileName);
                    PDFfileName = responce.ObjectID.ToString();
                    string attachmentsave = SaveReqAttachments(response.ObjectID, requirement.CustomerID, PDFfileName, 0, "PDFREQUIREMENT");
                    if (PDFfileName != "")
                    {
                        var fileData = DownloadFile(PDFfileName, requirement.SessionID);
                        if (fileData.FileStream != null && !string.IsNullOrEmpty(fileData.FileName))
                        {
                            emilAttachmentReqPDF = new Attachment(fileData.FileStream, fileData.FileName);
                        }
                    }
                }

                if (requirement.AuctionVendors.Count > 0)
                {
                    UserInfo customerInfo = Utilities.GetUserNew(requirement.CustomerID);
                    foreach (VendorDetails vendor in requirement.AuctionVendors)
                    {
                        Response response1 = new Response();
                        response1.ObjectID = response.ObjectID;
                        response1.ErrorMessage = "";

                        if (requirement.RequirementID < 1)
                        {
                            requirement.Module = reqPosting;
                        }
                        else if (response1.ObjectID < 0)
                        {
                            requirement.Module = reqPosting;
                        }
                        else
                        {
                            requirement.Module = editReq;
                        }

                        if (ds.Tables[0].Rows[0][1] != null && response1.ErrorMessage == "" && requirement.IsSubmit == 1)
                        {
                            string body = string.Empty;
                            UserInfo user = Utilities.GetUserNew(vendor.VendorID);

                            User altUser = GetReqAlternateCommunications(response.ObjectID, vendor.VendorID);

                            if (requirement.RequirementID < 1)
                            {

                                string ScreenName = "ADD_REQUIREMENT";
                                if (requirement.CheckBoxEmail == true)
                                {
                                    body = GenerateEmailBody("Vendoremail", requirement.BiddingType);
                                    body = String.Format(body, user.FirstName, user.LastName, response.ObjectID, requirement.Title, Utilities.toLocal(requirement.QuotationFreezTime), customerInfo.Institution, Utilities.toLocal(requirement.ExpStartTime));
                                    SendEmail(user.Email + "," + user.AltEmail + "," + altUser.AltEmail, "You have a new Requirement -Req Number: " + requirement.RequirementNumber + " Title: " + requirement.Title, body, response.ObjectID, vendor.VendorID, requirement.Module, requirement.SessionID, emilAttachmentReqPDF, VendorListAttachment, Utilities.toLocal(requirement.QuotationFreezTime), 15, "QUOTATION_FREEZE").ConfigureAwait(false);
                                }
                                string body1 = GenerateEmailBody("Vendorsms", requirement.BiddingType);
                                if (requirement.CheckBoxSms == true)
                                {

                                    body1 = String.Format(body1, user.FirstName, user.LastName, response.ObjectID, requirement.Title, Utilities.toLocal(requirement.QuotationFreezTime), customerInfo.Institution, Utilities.toLocal(requirement.ExpStartTime));
                                    body1 = body1.Replace("<br/>", "");
                                }
                            }
                            else
                            {
                                if (response1.ObjectID < 0)
                                {
                                    string ScreenName = "ADD_REQUIREMENT";
                                    if (requirement.CheckBoxEmail == true)
                                    {
                                        body = GenerateEmailBody("Vendoremail");
                                        body = String.Format(body, user.FirstName, user.LastName, response.ObjectID, requirement.Title, Utilities.toLocal(requirement.QuotationFreezTime), customerInfo.Institution, Utilities.toLocal(requirement.ExpStartTime));
                                        SendEmail(user.Email + "," + user.AltEmail + "," + altUser.AltEmail, "You have a new Reqquirement - ReqID: " + response.ObjectID + " Title: " + requirement.Title, body, response.ObjectID, vendor.VendorID, requirement.Module, requirement.SessionID, emilAttachmentReqPDF, VendorListAttachment, Utilities.toLocal(requirement.QuotationFreezTime), 15, "QUOTATION_FREEZE").ConfigureAwait(false);
                                    }
                                    string body1 = GenerateEmailBody("Vendorsms");
                                    if (requirement.CheckBoxSms == true)
                                    {
                                        body1 = String.Format(body1, user.FirstName, user.LastName, response.ObjectID, requirement.Title, Utilities.toLocal(requirement.QuotationFreezTime), customerInfo.Institution, Utilities.toLocal(requirement.ExpStartTime));
                                        body1 = body1.Replace("<br/>", "");
                                    }
                                }
                                else
                                {
                                    if (requirement.CheckBoxEmail == true)
                                    {
                                        body = GenerateEmailBody("VendoremailForReqUpdate", requirement.BiddingType);
                                        body = String.Format(body, user.FirstName, user.LastName, response.ObjectID, requirement.Title, Utilities.toLocal(requirement.QuotationFreezTime), Utilities.toLocal(requirement.ExpStartTime));
                                        SendEmail(user.Email + "," + user.AltEmail + "," + altUser.AltEmail, "Requirement details updated for Req Number: " + requirement.RequirementNumber + " Title: " + requirement.Title, body, response.ObjectID, vendor.VendorID, requirement.Module, requirement.SessionID, emilAttachmentReqPDF, VendorListAttachment, Utilities.toLocal(requirement.QuotationFreezTime), 15, "QUOTATION_FREEZE").ConfigureAwait(false);

                                    }
                                    if (requirement.CheckBoxSms == true)
                                    {
                                        string ScreenName = "UPDATE_REQUIREMENT";
                                        string body1 = GenerateEmailBody("VendorsmsForReqUpdate", requirement.BiddingType);
                                        body1 = String.Format(body1, user.FirstName, user.LastName, response.ObjectID, requirement.Title, Utilities.toLocal(requirement.QuotationFreezTime), Utilities.toLocal(requirement.ExpStartTime));
                                        body1 = body1.Replace("<br/>", "");
                                    }
                                }
                            }
                        }
                        if (response1.ErrorMessage != "")
                        {
                            throw new Exception(response1.ErrorMessage);
                        }
                    }
                }

                Attachment emilAttachmentReqPDFCustomer = null;
                if (requirement.IsSubmit == 1)
                {
                    long ticks2 = DateTime.UtcNow.Ticks;
                    string CustomerPDFfileName = string.Empty;
                    int margin = 24;
                    CustomerPDFfileName = "CustomerReqPDF" + response.ObjectID + "_" + ticks2 + ".pdf";
                    filenameTemp = "CustomerReqPDF" + response.ObjectID + "_" + ticks2 + ".pdf";
                    int flag = 1;
                    List<KeyValuePair<string, DataTable>> dtbl = FwdPdfUtilities.MakeDataTable(response.ObjectID, requirement, quotationItems, customerDetails, folderPath, flag, requirement.BiddingType);
                    FwdPdfUtilities.ExportDataTableToPdf(dtbl, @folderPath + "CustomerReqPDF" + response.ObjectID + "_" + ticks2 + ".pdf", response.ObjectID, requirement, flag);
                    Response responce = SaveAttachment(CustomerPDFfileName);
                    CustomerPDFfileName = responce.ObjectID.ToString();
                    string attachmentsave = SaveReqAttachments(response.ObjectID, requirement.CustomerID, CustomerPDFfileName, 0, "REQ_PDF_CUSTOMER");
                    if (CustomerPDFfileName != "")
                    {
                        var fileData = DownloadFile(CustomerPDFfileName, requirement.SessionID);
                        if (fileData.FileStream != null && !string.IsNullOrEmpty(fileData.FileName))
                        {
                            emilAttachmentReqPDFCustomer = new Attachment(fileData.FileStream, fileData.FileName);
                        }
                    }
                }

                if (requirement.IsSubmit == 1)
                {
                    string watchers = string.Empty;
                    if (requirement.CustomerEmails != null && requirement.CustomerEmails.Count > 0)
                    {
                        watchers = string.Join(";", requirement.CustomerEmails.Select(e => e.Mail));
                    }

                    Requirement req = GetRequirementDataOfflinePrivate(response.ObjectID, requirement.CustomerID, requirement.SessionID);
                    UserInfo customer = Utilities.GetUserNew(req.CustomerID);
                    UserInfo superUser = Utilities.GetSuperUser(req.SuperUserID);
                    string emailBody = string.Empty;
                    emailBody = GenerateEmailBody("PostRequirementemail", requirement.BiddingType);
                    requirement.QuotationFreezTime = Utilities.toLocal(requirement.QuotationFreezTime);
                    requirement.ExpStartTime = Utilities.toLocal(requirement.ExpStartTime);
                    string emailBody1 = String.Format(emailBody, customer.FirstName, customer.LastName, response.ObjectID, requirement.Title, requirement.QuotationFreezTime, requirement.Currency, requirement.ExpStartTime);
                    SendEmail(customer.Email + "," + customer.AltEmail, "Your Requirement has been successfully posted! Req Number: " + requirement.RequirementNumber + " Title: " + requirement.Title, emailBody1, response.ObjectID, req.CustomerID, requirement.Module, requirement.SessionID, emilAttachmentReqPDFCustomer, CustomerListAttachment, requirement.QuotationFreezTime, 15, "QUOTATION_FREEZE", watchers).ConfigureAwait(false);
                    string body2 = GenerateEmailBody("PostRequirementsms", requirement.BiddingType);
                    string body4 = String.Format(body2, customer.FirstName, customer.LastName, response.ObjectID, requirement.Title, requirement.QuotationFreezTime, requirement.Currency, requirement.ExpStartTime);
                    string body3 = String.Format(body2, superUser.FirstName, superUser.LastName, response.ObjectID, requirement.Title, requirement.QuotationFreezTime, requirement.Currency, requirement.ExpStartTime);
                    body3 = body3.Replace("<br/>", "");
                    body4 = body4.Replace("<br/>", "");                   
                }

                int remindersCount = 0;
                DateTime inputQuotationFreezeTime = DateTime.UtcNow;
                if (requirement.IsSubmit == 1)
                {
                    Response responseSaveQuotationReminders = SaveQuotationReminders(response.ObjectID, inputQuotationFreezeTime, "DELETE");
                }

                if (requirement.IsSubmit == 1)
                {
                    remindersCount = requirement.NoOfQuotationReminders;
                    DateTime createdQuotationFreezTime = Convert.ToDateTime(requirement.QuotationFreezTime); ;
                    inputQuotationFreezeTime = Convert.ToDateTime(requirement.QuotationFreezTime);

                    for (int i = 1; i <= remindersCount; i++)
                    {
                        inputQuotationFreezeTime = QuotationReminders(inputQuotationFreezeTime, requirement.RemindersTimeInterval, 9, 19, createdQuotationFreezTime);
                        Response responseSaveQuotationReminders = SaveQuotationReminders(response.ObjectID, inputQuotationFreezeTime, "SAVE");
                    }
                }

                if (requirement.RequirementID <= 0)
                {
                    requirement.RequirementID = response.ObjectID;
                }

                if (requirement != null && requirement.RequirementID > 0 && requirement.ListRequirementItems != null && requirement.ListRequirementItems.Count > 0)
                {
                    List<string> prIds = new List<string>();
                    List<string> prItemIds = new List<string>();
                    foreach (var item in requirement.ListRequirementItems)
                    {
                        if (!string.IsNullOrWhiteSpace(item.PR_ID) && !string.IsNullOrWhiteSpace(item.PR_ITEM_ID))
                        {
                            prIds.AddRange(item.PR_ID.Split(',').ToList());
                            prItemIds.AddRange(item.PR_ITEM_ID.Split(',').ToList());
                        }
                    }

                    if (prIds.Count > 0 && prItemIds.Count > 0)
                    {
                        prIds = prIds.Distinct().ToList();
                        prItemIds = prItemIds.Distinct().ToList();
                        Response prRFQResponse = LinkPRWithRFQ(string.Join(",", prIds), requirement.RequirementID, string.Join(",", prItemIds));
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                response.ErrorMessage = ex.Message + ex.StackTrace;
            }

            return response;
        }

        public List<CurrencyFactors> GetCurrencyFactors(int compID)
        {

            List<CurrencyFactors> listCurrencyFactors = new List<CurrencyFactors>();
            try
            {
                string query = $"select * , 'currencyfactor' as [TYPE] from currencyfactor where COMP_ID  = {compID}";
                DataSet ds = sqlHelper.ExecuteQuery(query);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        CurrencyFactors currencyFactors = new CurrencyFactors();
                        currencyFactors.CurrencyFactorID = row["CURRENCY_FACTOR_ID"] != DBNull.Value ? Convert.ToInt32(row["CURRENCY_FACTOR_ID"]) : 0;
                        currencyFactors.CurrencyCode = row["CURRENCY_CODE"] != DBNull.Value ? Convert.ToString(row["CURRENCY_CODE"]) : string.Empty;
                        currencyFactors.CompID = row["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["COMP_ID"]) : 0;
                        currencyFactors.CurrencyFactor = row["CURRENCY_FACTOR"] != DBNull.Value ? Convert.ToDecimal(row["CURRENCY_FACTOR"]) : 0;
                        currencyFactors.StartDate = row["START_DATE"] != DBNull.Value ? Convert.ToDateTime(row["START_DATE"]) : DateTime.MaxValue;
                        currencyFactors.EndDate = row["END_DATE"] != DBNull.Value ? Convert.ToDateTime(row["END_DATE"]) : DateTime.MaxValue;
                        currencyFactors.Type = row["TYPE"] != DBNull.Value ? Convert.ToString(row["TYPE"]) : string.Empty;

                        listCurrencyFactors.Add(currencyFactors);
                    }
                }
            }
            catch (Exception ex)
            {
                CurrencyFactors currencyFactors = new CurrencyFactors();
                currencyFactors.ErrorMessage = ex.Message;
                listCurrencyFactors.Add(currencyFactors);
            }

            return listCurrencyFactors;
        }

        public Requirement GetRequirementData(int reqID, int userID, string sessionID)
        {
            Requirement requirement = new Requirement();
            requirement = this.GetRequirementDetails(reqID, userID, 0);
            return requirement;
        }

        public RequirementLite GetRequirementDataLite(int reqID, string sessionID)
        {
            var details = this.GetRequirementDetails(reqID);
            return details;
        }

        public KeyValuePair[] GetRequirementVendorAnalysis(string vendorids, string itemids, string sessionid)
        {
            List<KeyValuePair> details = new List<KeyValuePair>();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_VENDORS", vendorids);
                sd.Add("P_ITEMS", itemids);
                DataSet ds = sqlHelper.SelectList("fwd_GetRequirementVendorAnalysis", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        decimal minQuotationTime = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToDecimal(ds.Tables[0].Rows[0][0]) : 0;
                        decimal maxQuotationTime = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToDecimal(ds.Tables[0].Rows[0][1]) : 0;
                        KeyValuePair minQuotationTimeKey = new KeyValuePair();
                        minQuotationTimeKey.Key1 = "MIN_QUOTATION_TIME";
                        minQuotationTimeKey.DecimalVal = minQuotationTime;
                        details.Add(minQuotationTimeKey);

                        KeyValuePair maxQuotationTimeKey = new KeyValuePair();
                        maxQuotationTimeKey.Key1 = "MAX_QUOTATION_TIME";
                        maxQuotationTimeKey.DecimalVal = maxQuotationTime;
                        details.Add(maxQuotationTimeKey);
                    }

                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        foreach (var row in ds.Tables[1].AsEnumerable())
                        {
                            KeyValuePair detail = new KeyValuePair();
                            detail.Key = row["CATALOGUE_ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row["CATALOGUE_ITEM_ID"]) : 0;
                            detail.DecimalVal = row["AVG_REV_PRICE"] != DBNull.Value ? Convert.ToDecimal(row["AVG_REV_PRICE"]) : 0;
                            detail.DecimalVal1 = row["L1_REV_PRICE"] != DBNull.Value ? Convert.ToDecimal(row["L1_REV_PRICE"]) : 0;
                            detail.Key1 = row["PRODUCT_NAME"] != DBNull.Value ? Convert.ToString(row["PRODUCT_NAME"]) : "";
                            details.Add(detail);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return details.ToArray();
        }

        public List<RequirementSetting> GetRequirementSettings(int reqid, string sessionid)
        {
            List<RequirementSetting> settings = GetReqSettings(reqid);

            return settings;
        }

        public List<RequirementSetting> GetReqSettings(int reqid)
        {
            List<RequirementSetting> settings = new List<RequirementSetting>();
            try
            {
                CORE.DataNamesMapper<RequirementSetting> mapper = new CORE.DataNamesMapper<RequirementSetting>();
                string query = $@"SELECT REQ_ID, REQ_SETTING, REQ_SETTING_VALUE FROM fwd_RequirementSettings WHERE REQ_ID = {reqid}";
                var dt = sqlHelper.SelectQuery(query);
                settings = mapper.Map(dt).ToList();
            }
            catch (Exception ex)
            {
                settings.Clear();
                settings.Add(new RequirementSetting() { ErrorMessage = ex.Message });
            }

            return settings;
        }

        public Response SaveRequirementSetting(RequirementSetting requirementSetting)
        {
            Response response = new Response();
            try
            {
                int isValidSession = ValidateSession(requirementSetting.SessionID);
                string query = $"DELETE FROM fwd_requirementsettings WHERE REQ_ID = {requirementSetting.REQ_ID} AND REQ_SETTING = '{requirementSetting.REQ_SETTING}'";
                sqlHelper.ExecuteNonQuery_IUD(query);

                query = $@"INSERT INTO fwd_requirementsettings (REQ_ID, REQ_SETTING, REQ_SETTING_VALUE, DATE_CREATED, DATE_MODIFIED, CREATED_BY, MODIFIED_BY) VALUES 
                    ({requirementSetting.REQ_ID}, '{requirementSetting.REQ_SETTING}', '{requirementSetting.REQ_SETTING_VALUE}', UTC_TIMESTAMP, UTC_TIMESTAMP, {requirementSetting.USER}, {requirementSetting.USER})";
                sqlHelper.ExecuteNonQuery_IUD(query);
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveRequirementTerms(List<RequirementTerms> listRequirementTerms, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {
                int isValidSession = ValidateSession(sessionID);
                foreach (RequirementTerms RT in listRequirementTerms)
                {
                    sd.Add("P_RT_ID", RT.ReqTermsID);
                    sd.Add("P_REQ_ID", RT.ReqID);
                    sd.Add("P_U_ID", RT.UserID);
                    sd.Add("P_RT_DAYS", RT.ReqTermsDays);
                    sd.Add("P_RT_PERCENT", RT.ReqTermsPercent);
                    sd.Add("P_RT_TYPE", RT.ReqTermsType);
                    sd.Add("P_IS_REVISED", RT.IsRevised);
                    DataSet ds = sqlHelper.SelectList("fwd_SaveRequirementTerms", sd);
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                    {
                        response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                        response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public List<RequirementTerms> GetRequirementTerms(int reqID, int userID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<RequirementTerms> listRequirementTerms = new List<RequirementTerms>();
            try
            {
                int isValidSession = ValidateSession(sessionID);
                sd.Add("P_U_ID", userID);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("fwd_GetRequirementTerms", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        RequirementTerms rt = new RequirementTerms();

                        rt.ReqTermsID = row["RT_ID"] != DBNull.Value ? Convert.ToInt32(row["RT_ID"]) : 0;
                        rt.ReqID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                        rt.UserID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                        rt.ReqTermsDays = row["RT_DAYS"] != DBNull.Value ? Convert.ToInt32(row["RT_DAYS"]) : 0;
                        rt.ReqTermsPercent = row["RT_PERCENT"] != DBNull.Value ? Convert.ToInt32(row["RT_PERCENT"]) : 0;
                        rt.ReqTermsType = row["RT_TYPE"] != DBNull.Value ? Convert.ToString(row["RT_TYPE"]) : string.Empty;
                        rt.IsRevised = row["IS_REVISED"] != DBNull.Value ? Convert.ToInt32(row["IS_REVISED"]) : 0;
                        rt.RefReqTermID = row["REF_RT_ID"] != DBNull.Value ? Convert.ToInt32(row["REF_RT_ID"]) : 0;

                        //user.IsValid = row["IS_VALID"] != DBNull.Value ? (Convert.ToInt32(row["IS_VALID"]) == 1 ? true : false) : false;

                        listRequirementTerms.Add(rt);
                    }
                }
            }
            catch (Exception ex)
            {
                RequirementTerms rt = new RequirementTerms();
                rt.ErrorMessage = ex.Message;
                listRequirementTerms.Add(rt);
            }
            return listRequirementTerms;
        }

        public Response DeleteRequirementTerms(List<int> listTerms, string sessionID)
        {
            Response response = new Response();
            try
            {

                string query = string.Empty;
                foreach (int id in listTerms)
                {
                    query += string.Format("DELETE FROM fwd_requirementterms WHERE RT_ID = {0};", id);
                }

                sqlHelper.ExecuteQuery(query);
                response.ObjectID = 1;

            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public List<ReqDepartments> GetReqDepartments(int userID, int reqID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<ReqDepartments> listReqDepartments = new List<ReqDepartments>();
            try
            {
                sd.Add("P_U_ID", userID);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("fwd_GetReqDepartments", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        ReqDepartments reqDepartments = new ReqDepartments();
                        reqDepartments.CompanyDepartments = new CompanyDepartments();
                        reqDepartments.ReqDeptID = row["REQ_DEPT_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_DEPT_ID"]) : 0;
                        reqDepartments.ReqID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                        reqDepartments.UserID = userID;
                        reqDepartments.IsValid = row["IS_VALID"] != DBNull.Value ? (Convert.ToInt32(row["IS_VALID"]) == 1 ? true : false) : false;
                        reqDepartments.CompanyDepartments.DeptID = row["DEPT_ID"] != DBNull.Value ? Convert.ToInt32(row["DEPT_ID"]) : 0;
                        reqDepartments.CompanyDepartments.CompID = row["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["COMP_ID"]) : 0;
                        reqDepartments.CompanyDepartments.DeptCode = row["DEPT_CODE"] != DBNull.Value ? Convert.ToString(row["DEPT_CODE"]) : string.Empty;
                        reqDepartments.CompanyDepartments.DeptDesc = row["DEPT_DESC"] != DBNull.Value ? Convert.ToString(row["DEPT_DESC"]) : string.Empty;
                        reqDepartments.CompanyDepartments.DeptAdmin = row["DEPT_ADMIN"] != DBNull.Value ? Convert.ToInt32(row["DEPT_ADMIN"]) : 0;
                        listReqDepartments.Add(reqDepartments);
                    }
                }
            }
            catch (Exception ex)
            {
                ReqDepartments reqDepartments = new ReqDepartments();
                reqDepartments.ErrorMessage = ex.Message;
                listReqDepartments.Add(reqDepartments);
            }
            return listReqDepartments;
        }

        public List<Requirement> GetCompanyLeads(int userID, string searchString, string searchType, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<Requirement> ListRequirement = new List<Requirement>();

            if (string.IsNullOrEmpty(searchString))
            {
                return ListRequirement;
            }

            searchString = "%" + searchString + "%";
            try
            {
                sd.Add("P_U_ID", userID);
                sd.Add("P_SEARCH_STRING", searchString);
                sd.Add("P_SEARCH_TYPE", searchType);
                DataSet ds = sqlHelper.SelectList("fwd_GetCompanyLeads", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        Requirement Requirement = new Requirement();
                        Requirement.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                        Requirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                        Requirement.StringCategory = row["REQ_CATEGORY"] != DBNull.Value ? Convert.ToString(row["REQ_CATEGORY"]) : string.Empty;
                        Requirement.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToInt32(row["SAVINGS"]) : 0;
                        Requirement.VendorsCount = row["VENDORS_COUNT"] != DBNull.Value ? Convert.ToInt32(row["VENDORS_COUNT"]) : 0;
                        Requirement.IntialLeastVendorPrice = row["INITIAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row["INITIAL_PRICE"]) : 0;
                        Requirement.LeastVendorPrice = row["CLOSING_PRICE"] != DBNull.Value ? Convert.ToDouble(row["CLOSING_PRICE"]) : 0;
                        Requirement.PostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                        Requirement.Title = Requirement.Title.ToUpper();
                        ListRequirement.Add(Requirement);
                    }
                }
            }
            catch (Exception ex)
            {
                Requirement Requirement = new Requirement();
                Requirement.ErrorMessage = ex.Message;
                ListRequirement.Add(Requirement);
            }

            return ListRequirement;
        }

        public List<LastPrices> GetLastPrice(int companyid, string productIDorName, string sessionid)
        {
            List<LastPrices> vendordetails = new List<LastPrices>();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                Utilities.ValidateSession(sessionid);
                sd.Add("P_COMP_ID", companyid);
                sd.Add("P_PROD_ID", productIDorName);
                DataSet ds = sqlHelper.SelectList("fwd_GetLastPrice", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        LastPrices details = new LastPrices();
                        // DataRow row = ds.Tables[0].Rows[0];
                        details.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                        details.CurrentTime = row["DATE_MODIFIED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_MODIFIED"]) : DateTime.Now;
                        details.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                        details.ProductIDorName = row["PROD_ID"] != DBNull.Value ? Convert.ToString(row["PROD_ID"]) : string.Empty;
                        details.ItemID = row["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row["ITEM_ID"]) : 0;
                        details.RevPrice = row["REVICED_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REVICED_PRICE"]) : 0;
                        details.UnitPrice = row["UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row["UNIT_PRICE"]) : 0;
                        details.Quantity = row["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row["QUANTITY"]) : 0;
                        vendordetails.Add(details);
                    }
                }
            }
            catch (Exception ex)
            {
                LastPrices details = new LastPrices();
                details.ErrorMessage = ex.Message;
                vendordetails.Add(details);
            }

            return vendordetails;
        }

        public VendorDetails ItemPreviousPrice(string productname, string productno, string brand, int companyid, string sessionid)
        {
            VendorDetails vendordetails = new VendorDetails();
            try
            {
                int isValidSession = ValidateSession(sessionid);
                string query = $@"select TOP 1 Q.REQ_ID, (Q.REV_UNIT_PRICE * AD.CURRENCY_FACTOR) REV_UNIT_PRICE, AD.DATE_MODIFIED, VC.COMPANY_NAME
                    from fwd_quotations Q
                    INNER JOIN fwd_requirementitems RI ON Q.REQ_ID = RI.REQ_ID
                    INNER JOIN fwd_auctiondetails AD ON AD.REQ_ID = Q.REQ_ID AND AD.U_ID = Q.U_ID
                    INNER JOIN company CC ON CC.COMP_ID = getcompanyid(RI.CREATED_BY)
                    INNER JOIN company VC ON VC.COMP_ID = getcompanyid(Q.U_ID)
                    where TRIM(Q.PROD_ID) = TRIM('{productname}') AND TRIM(Q.PROD_NO) = TRIM('{productno}') and TRIM(RI.BRAND) = TRIM('{brand}') and CC.COMP_ID = {companyid}
                    AND Q.REV_UNIT_PRICE > 0 
                    AND Q.U_ID NOT IN (SELECT CREATED_BY FROM company WHERE COMPANY_NAME = 'PRICE_CAP') 
                    AND cast(Q.DATE_CREATED as DATE) BETWEEN (select cast(CONFIG_VALUE as DATE) From companyconfiguration where CONFIG_KEY = 'BEST_PRICE_FROM' AND COMP_ID = {companyid}) AND 
                    (select cast(CONFIG_VALUE as DATE) From companyconfiguration where CONFIG_KEY  = 'BEST_PRICE_TO' AND COMP_ID = {companyid})
                    order by (Q.REV_UNIT_PRICE * AD.CURRENCY_FACTOR) DESC";

                DataSet ds = sqlHelper.ExecuteQuery(query);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    vendordetails.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                    vendordetails.InitialPrice = row["REV_UNIT_PRICE"] != DBNull.Value ? Convert.ToInt32(row["REV_UNIT_PRICE"]) : 0;
                    vendordetails.CurrentTime = row["DATE_MODIFIED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_MODIFIED"]) : DateTime.UtcNow;
                    vendordetails.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                }
            }
            catch (Exception ex)
            {
                vendordetails.ErrorMessage = ex.Message;
            }

            return vendordetails;
        }

        public Response DeleteAttachment(int userID, int reqID)
        {
            Response response = new Response();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", userID);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("fwd_DeleteReqAttachment", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveReqDeptDesig(List<ReqDepartments> listReqDepartments, string sessionID)
        {
            Response response = new Response();
            try
            {
                ValidateSession(sessionID);
                foreach (ReqDepartments RDD in listReqDepartments)
                {
                    foreach (ReqDesignations RDESIG in RDD.ListReqDesignations)
                    {
                        SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                        int isValid = 0;
                        if (RDD.IsValid && RDESIG.IsValid)
                        {
                            isValid = 1;
                        };

                        sd.Add("P_REQ_ID", RDD.ReqID);
                        sd.Add("P_DEPT_ID", RDD.CompanyDepartments.DeptID);
                        sd.Add("P_DESIG_ID", RDESIG.CompanyDesignations.DesigID);
                        sd.Add("P_IS_VALID", isValid);
                        sd.Add("P_CREATED_BY", isValid);
                        DataSet ds = sqlHelper.SelectList("fwd_SaveReqDeptDesig", sd);
                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                        {
                            response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                            response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveReqDepartments(List<ReqDepartments> listReqDepartments, string sessionID)
        {
            Response response = new Response();
            try
            {
                ValidateSession(sessionID);
                foreach (ReqDepartments RD in listReqDepartments)
                {
                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_REQ_DEPT_ID", RD.ReqDeptID);
                    sd.Add("P_REQ_ID", RD.ReqID);
                    sd.Add("P_DEPT_ID", RD.CompanyDepartments.DeptID);
                    sd.Add("P_IS_VALID", RD.IsValid);
                    sd.Add("P_CREATED_BY", RD.CreatedBy);

                    DataSet ds = sqlHelper.SelectList("fwd_SaveReqDepartments", sd);

                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                    {
                        response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                        response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public List<Requirement> GetMyAuctions(int userID, string fromDate, string toDate, string status, string search, string allBuyer, int page, int pagesize, string sessionID, int onlyrfq = 0, int onlyrfp = 0)
        {
            List<Requirement> myAuctions = new List<Requirement>();
            try
            {
                ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", userID);
                //sd.Add("P_FROM_DATE", fromDate != "" || fromDate!= "undefined" ? fromDate : null) ;
                sd.Add("P_FROM_DATE", fromDate == "" || fromDate == "undefined" ? null : fromDate);
                //sd.Add("P_TO_DATE", toDate != "" || toDate != "undefined" ? toDate: null);
                sd.Add("P_TO_DATE", toDate == "" || toDate == "undefined" ? null : toDate);
                sd.Add("P_STATUS", status == "" || status == "ALL" ? string.Empty : status);
                sd.Add("P_SEARCH", search ?? string.Empty);
                sd.Add("P_ALL", allBuyer == "" || allBuyer == null || allBuyer == "ALL" ? string.Empty : allBuyer);
                sd.Add("P_ONLY_RFQ", onlyrfq);
                sd.Add("P_ONLY_RFP", onlyrfp);
                sd.Add("P_PAGE", page);
                sd.Add("P_PAGE_SIZE", pagesize);
                DataSet ds = sqlHelper.SelectList("fwd_GetRequirements_paging_1", sd);

                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        Requirement requirement = new Requirement();

                        string[] arr = new string[] { };
                        byte[] next = new byte[] { };
                        requirement.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : -1;
                        requirement.BiddingType = row["BIDDING_TYPE"] != DBNull.Value ? Convert.ToString(row["BIDDING_TYPE"]) : string.Empty;
                        requirement.CustomerID = userID;
                        requirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                        requirement.DateModified = row["DATE_MODIFIED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_MODIFIED"]) : DateTime.MinValue;
                        requirement.EndTime = row["END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["END_TIME"]) : DateTime.MaxValue;
                        if (requirement.EndTime == null)
                        {
                            requirement.EndTime = DateTime.MaxValue;
                        }
                        requirement.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                        if (requirement.StartTime == null)
                        {
                            requirement.StartTime = DateTime.MaxValue;
                        }
                        requirement.PostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                        // requirement.Price = row["VEND_MIN_PRICE"] != DBNull.Value ? Convert.ToDouble(row["VEND_MIN_PRICE"]) : 0;
                        // double RunPrice = row["VEND_MIN_RUN_PRICE"] != DBNull.Value ? Convert.ToDouble(row["VEND_MIN_RUN_PRICE"]) : 0;
                        DateTime start = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                        //if (start < DateTime.UtcNow && RunPrice > 0)
                        //{
                        //    requirement.Price = RunPrice;
                        //}
                        //string POLink = row["PURCHASE_ORDER_LINK"] != DBNull.Value ? (row["PURCHASE_ORDER_LINK"].ToString()) : string.Empty;
                        //requirement.Description = row["REQ_DESC"] != DBNull.Value ? Convert.ToString(row["REQ_DESC"]) : string.Empty;
                        //requirement.Category = row["REQ_CATEGORY"] != DBNull.Value ? Convert.ToString(row["REQ_CATEGORY"]).Split(',') : arr;
                        requirement.Urgency = row["REQ_URGENCY"] != DBNull.Value ? Convert.ToString(row["REQ_URGENCY"]) : string.Empty;
                        // requirement.Budget = row["REQ_BUDGET"] != DBNull.Value ? Convert.ToString(row["REQ_BUDGET"]) : string.Empty;
                        requirement.DeliveryLocation = row["REQ_DELIVERY_LOC"] != DBNull.Value ? Convert.ToString(row["REQ_DELIVERY_LOC"]) : string.Empty;
                        // requirement.Taxes = row["REQ_TAXES"] != DBNull.Value ? Convert.ToString(row["REQ_TAXES"]) : string.Empty;
                        requirement.PaymentTerms = row["REQ_PAYMENT_TERMS"] != DBNull.Value ? Convert.ToString(row["REQ_PAYMENT_TERMS"]) : string.Empty;
                        requirement.Currency = row["REQ_CURRENCY"] != DBNull.Value ? Convert.ToString(row["REQ_CURRENCY"]) : string.Empty;
                        string Status = row["REQ_STATUS"] != DBNull.Value ? Convert.ToString(row["REQ_STATUS"]) : string.Empty;
                        requirement.CustomerID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                        requirement.IsDiscountQuotation = row["IS_DISCOUNT_QUOTATION"] != DBNull.Value ? Convert.ToInt32(row["IS_DISCOUNT_QUOTATION"]) : 0;

                        requirement.NoOfVendorsInvited = row["NO_OF_VENDORS_INVITED"] != DBNull.Value ? Convert.ToInt32(row["NO_OF_VENDORS_INVITED"]) : 0;
                        requirement.NoOfVendorsParticipated = row["NO_OF_VENDORS_PARTICIPATED"] != DBNull.Value ? Convert.ToInt32(row["NO_OF_VENDORS_PARTICIPATED"]) : 0;

                        requirement.QuotationFreezTime = row["QUOTATION_FREEZ_TIME"] != DBNull.Value ? Convert.ToDateTime(row["QUOTATION_FREEZ_TIME"]) : DateTime.UtcNow;
                        requirement.DeliveryTime = row["REQ_DELIVERY_TIME"] != DBNull.Value ? Convert.ToString(row["REQ_DELIVERY_TIME"]) : string.Empty;
                        requirement.ExpStartTime = row["EXP_START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["EXP_START_TIME"]) : DateTime.UtcNow;
                        requirement.UserName = row["USER_NAME"] != DBNull.Value ? Convert.ToString(row["USER_NAME"]) : string.Empty;
                        requirement.ProjectDetails = row["PROJECT_DETAILS"] != DBNull.Value ? Convert.ToString(row["PROJECT_DETAILS"]) : string.Empty;
                        requirement.ProjectName = row["PROJECT_NAME"] != DBNull.Value ? Convert.ToString(row["PROJECT_NAME"]) : string.Empty;

                        requirement.IS_CB_ENABLED = row["IS_CB_ENABLED"] != DBNull.Value ? (Convert.ToInt32(row["IS_CB_ENABLED"]) == 1 ? true : false) : false;
                        requirement.CB_END_TIME = row["CB_END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["CB_END_TIME"]) : DateTime.UtcNow;
                        // requirement.CB_STOP_QUOTATIONS = row["CB_STOP_QUOTATIONS"] != DBNull.Value ? (Convert.ToInt32(row["CB_STOP_QUOTATIONS"]) == 1 ? true : false) : false;
                        requirement.IS_CB_COMPLETED = row["IS_CB_COMPLETED"] != DBNull.Value ? (Convert.ToInt32(row["IS_CB_COMPLETED"]) == 1 ? true : false) : false;
                        requirement.IS_CB_NO_REGRET = row["IS_CB_NO_REGRET"] != DBNull.Value ? (Convert.ToInt32(row["IS_CB_NO_REGRET"]) == 1 ? true : false) : false;
                        requirement.PRNumbers = row["PR_NUMBERS"] != DBNull.Value ? Convert.ToString(row["PR_NUMBERS"]) : string.Empty;
                        requirement.RequirementNumber = row["REQ_NUMBER"] != DBNull.Value ? Convert.ToString(row["REQ_NUMBER"]) : string.Empty;
                        var tempReqType = row["REQ_TYPE"] != DBNull.Value ? Convert.ToString(row["REQ_TYPE"]) : string.Empty;
                        requirement.IsRFP = tempReqType == "2";
                        var prStatus = row["REQ_PR_STATUS"] != DBNull.Value ? Convert.ToString(row["REQ_PR_STATUS"]) : string.Empty;

                        requirement.TotalCount = row["TOTAL_ROWS"] != DBNull.Value ? Convert.ToInt32(row["TOTAL_ROWS"]) : 0;
                        DateTime now = DateTime.UtcNow;
                        requirement.Status = Status;

                        if (requirement.Status == "QCS Pending")
                        {
                            if (!string.IsNullOrEmpty(requirement.PRNumbers) && !string.IsNullOrEmpty(prStatus) && prStatus.Equals("CLOSED", StringComparison.InvariantCultureIgnoreCase))
                            {
                                requirement.Status = "Closed";
                            }
                            else
                            {
                                requirement.Status = "Negotiation Ended";
                            }
                        }

                        requirement.AuctionVendors = new List<VendorDetails>();
                        requirement.CustFirstName = string.Empty;
                        requirement.CustLastName = string.Empty;
                        //requirement.Status = string.Empty;
                        requirement.SessionID = string.Empty;
                        requirement.ErrorMessage = string.Empty;

                        myAuctions.Add(requirement);
                    }

                }
            }
            catch (Exception ex)
            {
                Requirement auction = new Requirement();
                auction.ErrorMessage = ex.Message;
                myAuctions.Add(auction);
            }

            return myAuctions;
        }
        public List<ReportData> GetRequirementsReport(int userID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<ReportData> reports = new List<ReportData>();
            try
            {
                int isValidSession = ValidateSession(sessionID);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("rp_fwd_GetReportData", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        ReportData report = new ReportData();
                        string[] arr = new string[] { };
                        byte[] next = new byte[] { };
                        report.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : -1;
                        report.ItemID = row["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row["ITEM_ID"]) : -1;
                        report.VendorID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : -1;
                        report.CustomerID = userID;
                        report.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                        report.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                        report.UserPhone = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
                        report.UserEmail = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                        report.ProductBrand = row["BRAND"] != DBNull.Value ? Convert.ToString(row["BRAND"]) : string.Empty;
                        report.ProductQuantityIn = row["QUANTITY_IN"] != DBNull.Value ? Convert.ToString(row["QUANTITY_IN"]) : string.Empty;
                        report.Status = row["STATUS"] != DBNull.Value ? Convert.ToString(row["STATUS"]) : string.Empty;
                        report.UnitMRP = row["UNIT_MRP"] != DBNull.Value ? Convert.ToDecimal(row["UNIT_MRP"]) : 0;
                        report.ProductQuantity = row["QUANTITY"] != DBNull.Value ? Convert.ToDecimal(row["QUANTITY"]) : 0;
                        report.RevUnitDiscount = row["REV_UNIT_DISCOUNT"] != DBNull.Value ? Convert.ToDecimal(row["REV_UNIT_DISCOUNT"]) : 0;
                        report.UnitDiscount = row["UNIT_DISCOUNT"] != DBNull.Value ? Convert.ToDecimal(row["UNIT_DISCOUNT"]) : 0;
                        report.Gst = row["GST"] != DBNull.Value ? Convert.ToDecimal(row["GST"]) : 0;
                        report.PostedDate = row["POSTED_DATE"] != DBNull.Value ? Convert.ToDateTime(row["POSTED_DATE"]) : DateTime.MaxValue;
                        report.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                        report.Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                        report.OthersBrands = row["OTHER_BRANDS"] != DBNull.Value ? Convert.ToString(row["OTHER_BRANDS"]) : string.Empty;
                        report.CostPrice = (report.UnitMRP * 100 * 100) / ((report.UnitDiscount * 100) + 10000 + (report.UnitDiscount * report.Gst) + (report.Gst * 100));
                        report.RevCostPrice = (report.UnitMRP * 100 * 100) / ((report.RevUnitDiscount * 100) + 10000 + (report.RevUnitDiscount * report.Gst) + (report.Gst * 100));
                        report.NetPrice = report.CostPrice * (1 + report.Gst / 100);
                        report.RevNetPrice = report.RevCostPrice * (1 + report.Gst / 100);
                        report.MarginAmount = report.UnitMRP - report.NetPrice;
                        report.RevMarginAmount = report.UnitMRP - report.RevNetPrice;
                        reports.Add(report);
                    }

                    List<int> reqIDList = reports.Select(r => r.RequirementID).Distinct().ToList();
                    foreach (int reqID in reqIDList)
                    {
                        List<ReportData> repList = reports.Where(r => r.RequirementID == reqID).ToList();
                        decimal maxInitMargin = repList.Max(x => x.UnitDiscount);
                        foreach (ReportData rep in repList)
                        {
                            rep.MaxInitMargin = maxInitMargin;
                        }
                        decimal maxFinalMargin = repList.Max(x => x.RevUnitDiscount);
                        foreach (ReportData rep in repList)
                        {
                            rep.MaxFinalMargin = maxFinalMargin;
                            rep.Savings = rep.MaxFinalMargin - rep.MaxInitMargin;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ReportData r = new ReportData();
                r.ErrorMessage = ex.Message;

                reports.Add(r);
            }

            return reports;
        }

        public List<Comment> GetBidHistory(int reqID, int userID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<Comment> bidHistory = new List<Comment>();

            try
            {
                int isValidSession = ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("fwd_GetNegotiationHistory", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        Comment history = new Comment();

                        history.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : -1;
                        history.UserID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : -1;
                        history.FirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                        history.LastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                        history.CreatedTime = row["BID_TIME"] != DBNull.Value ? Convert.ToDateTime(row["BID_TIME"]) : DateTime.MaxValue;
                        history.BidAmount = row["PRICE"] != DBNull.Value ? Utilities.RoundValue(Convert.ToDouble(row["PRICE"])) : 0;
                        history.Currency = row["REQ_CURRENCY"] != DBNull.Value ? Convert.ToString(row["REQ_CURRENCY"]) : string.Empty;
                        history.RejectReson = row["REJECT_RESON"] != DBNull.Value ? Convert.ToString(row["REJECT_RESON"]) : string.Empty;
                        bidHistory.Add(history);
                    }
                }

                double factor = 1; //Utilities.GetRequirementCurrencyFactor(reqID);
                foreach (var bid in bidHistory)
                {
                    bid.BidAmount = Utilities.RoundValue(bid.BidAmount / factor);
                }
            }
            catch (Exception ex)
            {
                Comment history = new Comment();
                history.ErrorMessage = ex.Message;
                bidHistory.Add(history);
            }

            return bidHistory;
        }

        public PriceComparison GetPriceComparison(int reqID, int userID, string sessionID)
        {
            PriceComparison requirement = new PriceComparison();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_U_ID", userID);
                requirement.requirement = GetRequirementData(reqID, userID, sessionID);
                requirement.requirement.AuctionVendors = requirement.requirement.AuctionVendors.Where(v => v.IsRevQuotationRejected != 1).Where(v => v.RevPrice > 0).Where(v => v.CompanyName != "PRICE_CAP").ToList(); //.OrderBy(v => v.RevPrice)
                DataSet ds = sqlHelper.SelectList("fwd_GetPriceComparison", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        DataRow row1 = ds.Tables[1].Rows[0];
                        requirement.MinQuotationPrice = row1["VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["VEND_TOTAL_PRICE"]) : 0;
                    }
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        requirement.PriceCompareObject = new List<PriceCompareObject>();
                        foreach (DataRow row2 in ds.Tables[2].Rows)
                        {
                            PriceCompareObject prCompObj = new PriceCompareObject();
                            prCompObj.ItemID = row2["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_ID"]) : 0;
                            prCompObj.MinPriceBidder = row2["LEAST_ITEM_BIDDER"] != DBNull.Value ? Convert.ToInt32(row2["LEAST_ITEM_BIDDER"]) : 0;
                            prCompObj.MinPrice = row2["MIN_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["MIN_PRICE"]) : 0;
                            prCompObj.MinUnitPrice = row2["UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["UNIT_PRICE"]) : 0;
                            prCompObj.LeastBidderPrice = row2["LEAST_BIDDER_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["LEAST_BIDDER_PRICE"]) : 0;
                            prCompObj.LeastBidderUnitPrice = row2["LEAST_BIDDER_UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["LEAST_BIDDER_UNIT_PRICE"]) : 0;
                            string vendorNames = string.Empty;
                            prCompObj.VendorName = vendorNames;
                            prCompObj.ProductName = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                            prCompObj.ProductDescription = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                            prCompObj.Quantity = row2["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row2["QUANTITY"]) : 0;
                            prCompObj.Units = row2["UNITS"] != DBNull.Value ? Convert.ToString(row2["UNITS"]) : string.Empty;
                            if (ds.Tables[2].Rows.Count > 0)
                            {
                                int L1_U_ID = requirement.requirement.AuctionVendors[0].VendorID;
                                if (requirement.requirement.AuctionVendors[0].CompanyName == "PRICE_CAP")
                                {
                                    L1_U_ID = requirement.requirement.AuctionVendors[1].VendorID;
                                }

                                List<PriceCompareObject> listPriceComparision = new List<PriceCompareObject>();
                                List<PriceCompareObject> listPriceComparisionFilterd = new List<PriceCompareObject>();
                                foreach (DataRow row3 in ds.Tables[3].Rows)
                                {
                                    PriceCompareObject priceComparisionObject = new PriceCompareObject();

                                    priceComparisionObject.QuotationID = row3["QUOT_ID"] != DBNull.Value ? Convert.ToInt32(row3["QUOT_ID"]) : 0;
                                    priceComparisionObject.UserID = row3["U_ID"] != DBNull.Value ? Convert.ToInt32(row3["U_ID"]) : 0;
                                    priceComparisionObject.ItemID = row3["item_id"] != DBNull.Value ? Convert.ToInt32(row3["item_id"]) : 0;
                                    priceComparisionObject.MinPrice = row3["REVICED_PRICE"] != DBNull.Value ? Convert.ToDouble(row3["REVICED_PRICE"]) : 0;
                                    priceComparisionObject.RevUnitPrice = row3["REV_UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row3["REV_UNIT_PRICE"]) : 0;
                                    priceComparisionObject.Brand = row3["brand"] != DBNull.Value ? Convert.ToString(row3["brand"]) : string.Empty;
                                    priceComparisionObject.Duration = row3["DURATION"] != DBNull.Value ? Convert.ToString(row3["DURATION"]) : string.Empty;
                                    priceComparisionObject.Payment = row3["PAYMENT"] != DBNull.Value ? Convert.ToString(row3["PAYMENT"]) : string.Empty;
                                    priceComparisionObject.VendorName = row3["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row3["COMPANY_NAME"]) : string.Empty;

                                    priceComparisionObject.OtherProperties = row3["OTHER_PROPERTIES"] != DBNull.Value ? Convert.ToString(row3["OTHER_PROPERTIES"]) : string.Empty;

                                    priceComparisionObject.MinUnitPrice = row3["UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row3["UNIT_PRICE"]) : 0;

                                    listPriceComparision.Add(priceComparisionObject);
                                }

                                listPriceComparisionFilterd = listPriceComparision.Where(v => v.ItemID == prCompObj.ItemID).Where(v => v.RevUnitPrice == prCompObj.MinUnitPrice).ToList(); //.OrderBy(v => v.MinPrice)



                                foreach (PriceCompareObject priceComparisionObject in listPriceComparisionFilterd)
                                {
                                    foreach (VendorDetails vd in requirement.requirement.AuctionVendors)
                                    {
                                        if (priceComparisionObject.UserID == vd.VendorID)
                                        {
                                            prCompObj.VendorName += priceComparisionObject.VendorName + ",";
                                            prCompObj.Brand += priceComparisionObject.Brand + ",";
                                            prCompObj.Duration += priceComparisionObject.Duration + ",";
                                            prCompObj.Payment += priceComparisionObject.Payment + ",";
                                            prCompObj.OtherProperties += priceComparisionObject.OtherProperties + ",";
                                        }
                                    }
                                }

                                prCompObj.VendorName = prCompObj.VendorName.Substring(0, prCompObj.VendorName.Length - 1);
                                prCompObj.Brand = prCompObj.Brand.Substring(0, prCompObj.Brand.Length - 1);
                                prCompObj.Duration = prCompObj.Duration.Substring(0, prCompObj.Duration.Length - 1);
                                prCompObj.Payment = prCompObj.Payment.Substring(0, prCompObj.Payment.Length - 1);
                                prCompObj.OtherProperties = prCompObj.OtherProperties.Substring(0, prCompObj.OtherProperties.Length - 1);

                                foreach (PriceCompareObject priceComparisionObject in listPriceComparision)
                                {
                                    if (L1_U_ID == priceComparisionObject.UserID && prCompObj.ItemID == priceComparisionObject.ItemID)
                                    {
                                        prCompObj.BrandL1 = priceComparisionObject.Brand;
                                        prCompObj.DurationL1 = priceComparisionObject.Duration;
                                        prCompObj.PaymentL1 = priceComparisionObject.Payment;
                                        prCompObj.OtherPropertiesL1 = priceComparisionObject.OtherProperties;
                                    }
                                }
                            }
                            requirement.PriceCompareObject.Add(prCompObj);
                        }
                    }

                    if (ds.Tables[4].Rows.Count > 0)
                    {
                        foreach (DataRow row2 in ds.Tables[4].Rows)
                        {
                            int vendorID = row2["U_ID"] != DBNull.Value ? Convert.ToInt32(row2["U_ID"]) : 0;
                            decimal techEvalScore = row2["TOTAL_MARKS"] != DBNull.Value ? Convert.ToInt32(row2["TOTAL_MARKS"]) : 0;
                            requirement.requirement.AuctionVendors.Where(v => v.VendorID == vendorID).FirstOrDefault().TechEvalScore = techEvalScore;
                        }
                    }
                }

                //CurrencyConversion
                var factor = 1;// Utilities.GetRequirementCurrencyFactor(reqID);
                requirement.MinQuotationPrice = Utilities.RoundValue(requirement.MinQuotationPrice / factor);
                if (requirement.PriceCompareObject != null && requirement.PriceCompareObject.Count > 0)
                {
                    foreach (var priceObj in requirement.PriceCompareObject)
                    {
                        priceObj.MinPrice = Utilities.RoundValue(priceObj.MinPrice / factor);
                        priceObj.LeastBidderPrice = Utilities.RoundValue(priceObj.LeastBidderPrice / factor);
                        priceObj.MinUnitPrice = Utilities.RoundValue(priceObj.MinUnitPrice / factor);
                        priceObj.LeastBidderUnitPrice = Utilities.RoundValue(priceObj.LeastBidderUnitPrice / factor);
                    }
                }
                //^CurrencyConversion
            }
            catch (Exception ex)
            {
                requirement.ErrorMessage = ex.Message;
            }

            return requirement;
        }

        public Response IsNegotiationEnded(int reqID, string sessionID)
        {
            Response response = new Response();
            try
            {
                ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("fwd_IsNegotiationEnded", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }
            return response;
        }

        public Requirement GetReportsRequirement(int reqID, int userID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Requirement requirement = new Requirement();
            UserDetails customerDetailReport = new UserDetails();
            try
            {
                int isValidSession = ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("fwd_Reports_Requirement", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    string[] arr = new string[] { };
                    byte[] test = new byte[] { };
                    requirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                    requirement.Description = row["REQ_DESC"] != DBNull.Value ? Convert.ToString(row["REQ_DESC"]) : string.Empty;
                    requirement.Category = row["REQ_CATEGORY"] != DBNull.Value ? Convert.ToString(row["REQ_CATEGORY"]).Split(',') : arr;
                    requirement.Subcategories = row["REQ_SUBCATEGORIES"] != DBNull.Value ? Convert.ToString(row["REQ_SUBCATEGORIES"]) : string.Empty;
                    requirement.PostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                    requirement.Urgency = row["REQ_URGENCY"] != DBNull.Value ? Convert.ToString(row["REQ_URGENCY"]) : string.Empty;
                    requirement.Budget = row["REQ_BUDGET"] != DBNull.Value ? Convert.ToString(row["REQ_BUDGET"]) : string.Empty;
                    requirement.DeliveryLocation = row["REQ_DELIVERY_LOC"] != DBNull.Value ? Convert.ToString(row["REQ_DELIVERY_LOC"]) : string.Empty;
                    requirement.Taxes = row["REQ_TAXES"] != DBNull.Value ? Convert.ToString(row["REQ_TAXES"]) : string.Empty;
                    requirement.PaymentTerms = row["REQ_PAYMENT_TERMS"] != DBNull.Value ? Convert.ToString(row["REQ_PAYMENT_TERMS"]) : string.Empty;
                    requirement.Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                    string Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                    requirement.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                    requirement.CustomerID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                    requirement.CustFirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                    requirement.CustLastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                    requirement.DeliveryTime = row["REQ_DELIVERY_TIME"] != DBNull.Value ? Convert.ToString(row["REQ_DELIVERY_TIME"]) : string.Empty;
                    requirement.RequirementID = reqID;
                    int isPOSent = row["IS_PO_SENT"] != DBNull.Value ? Convert.ToInt32(row["IS_PO_SENT"]) : 0;
                    string fileName = row["REQ_ATTACHMENT"] != DBNull.Value ? Convert.ToString(row["REQ_ATTACHMENT"]) : string.Empty;
                    string POLink = row["PURCHASE_ORDER_LINK"] != DBNull.Value ? (row["PURCHASE_ORDER_LINK"].ToString()) : string.Empty;
                    requirement.POLink = !string.IsNullOrEmpty(POLink) ? POLink : string.Empty;
                    string URL = fileName != string.Empty ? fileName : string.Empty;
                    requirement.AttachmentName = fileName;
                    requirement.EndTime = row["END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["END_TIME"]) : DateTime.MaxValue;
                    requirement.InclusiveTax = row["REQ_INCLUSIVE_TAX"] != DBNull.Value ? (Convert.ToInt32(row["REQ_INCLUSIVE_TAX"]) == 1 ? true : false) : false;
                    requirement.IncludeFreight = row["REQ_INCLUDE_FREIGHT"] != DBNull.Value ? (Convert.ToInt32(row["REQ_INCLUDE_FREIGHT"]) == 1 ? true : false) : false;
                    requirement.SuperUserID = row["SUPER_U_ID"] != DBNull.Value ? Convert.ToInt32(row["SUPER_U_ID"]) : requirement.CustomerID;
                    requirement.SelectedVendorID = row["SELECTED_VENDOR_ID"] != DBNull.Value ? Convert.ToInt32(row["SELECTED_VENDOR_ID"]) : requirement.CustomerID;
                    requirement.CustomerCompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                    requirement.MinReduceAmount = row["MIN_REDUCE_AMOUNT"] != DBNull.Value ? Convert.ToInt32(row["MIN_REDUCE_AMOUNT"]) : 0;
                    requirement.QuotationFreezTime = row["QUOTATION_FREEZ_TIME"] != DBNull.Value ? Convert.ToDateTime(row["QUOTATION_FREEZ_TIME"]) : DateTime.MaxValue;
                    requirement.IsTabular = row["REQ_IS_TABULAR"] != DBNull.Value ? (Convert.ToInt32(row["REQ_IS_TABULAR"]) == 1 ? true : false) : false;
                    requirement.ReqComments = row["REQ_COMMENTS"] != DBNull.Value ? Convert.ToString(row["REQ_COMMENTS"]) : string.Empty;

                    if (requirement.MinReduceAmount > 0)
                    {
                        requirement.MinBidAmount = requirement.MinReduceAmount;
                    }
                    else
                    {
                        requirement.MinBidAmount = 0;
                        //requirement.MinBidAmount = Math.Round(Convert.ToDouble(requirement.Budget) / 100, decimal_round);
                    }

                    requirement.CustomerCompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                    requirement.IsStopped = row["IS_STOPPED"] != DBNull.Value ? (Convert.ToInt32(row["IS_STOPPED"]) == 1 ? true : false) : false;
                    requirement.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                    requirement.Currency = row["REQ_CURRENCY"] != DBNull.Value ? Convert.ToString(row["REQ_CURRENCY"]) : string.Empty;
                    requirement.TimeZone = row["REQ_TIMEZONE"] != DBNull.Value ? Convert.ToString(row["REQ_TIMEZONE"]) : string.Empty;
                    requirement.IsNegotiationEnded = row["IS_NEGOTIATION_ENDED"] != DBNull.Value ? Convert.ToInt32(row["IS_NEGOTIATION_ENDED"]) : 0;
                    requirement.NegotiationDuration = row["NEGOTIATION_DURATION"] != DBNull.Value ? Convert.ToString(row["NEGOTIATION_DURATION"]) : string.Empty;
                    requirement.MinVendorComparision = row["VENDOR_COMPARISION_MIN_AMOUNT"] != DBNull.Value ? Convert.ToDouble(row["VENDOR_COMPARISION_MIN_AMOUNT"]) : 0;

                    NegotiationSettings NegotiationSettings = new NegotiationSettings();

                    NegotiationSettings.MinReductionAmount = row["UD_MIN_REDUCE_AMOUNT"] != DBNull.Value ? Convert.ToDouble(row["UD_MIN_REDUCE_AMOUNT"]) : 0;
                    NegotiationSettings.RankComparision = row["UD_VENDOR_COMPARISION_MIN_AMOUNT"] != DBNull.Value ? Convert.ToDouble(row["UD_VENDOR_COMPARISION_MIN_AMOUNT"]) : 0;
                    NegotiationSettings.NegotiationDuration = row["UD_NEGOTIATION_DURATION"] != DBNull.Value ? Convert.ToString(row["UD_NEGOTIATION_DURATION"]) : string.Empty;

                    requirement.ReqPDF = row["REQ_PDF"] != DBNull.Value ? Convert.ToInt32(row["REQ_PDF"]) : 0;
                    requirement.ReqPDFCustomer = row["REQ_PDF_CUSTOMER"] != DBNull.Value ? Convert.ToInt32(row["REQ_PDF_CUSTOMER"]) : 0;

                    requirement.NoOfVendorsInvited = row["NUMBER_OF_VENDORS_INVITED"] != DBNull.Value ? Convert.ToInt32(row["NUMBER_OF_VENDORS_INVITED"]) : 0;
                    requirement.NoOfVendorsParticipated = row["NUMBER_OF_VENDORS_PARTICIPATED"] != DBNull.Value ? Convert.ToInt32(row["NUMBER_OF_VENDORS_PARTICIPATED"]) : 0;
                    requirement.PriceBeforeNegotiation = row["PRICE_BEF_NEGOTIATION"] != DBNull.Value ? Convert.ToDouble(row["PRICE_BEF_NEGOTIATION"]) : 0;
                    requirement.priceAfterNegotiation = row["PRICE_AFT_NEGOTIATION"] != DBNull.Value ? Convert.ToDouble(row["PRICE_AFT_NEGOTIATION"]) : 0;

                    requirement.NegotiationSettings = NegotiationSettings;

                    DateTime now = DateTime.UtcNow;
                    if (requirement.EndTime != DateTime.MaxValue && requirement.EndTime > now && requirement.StartTime < now)
                    {
                        DateTime date = Convert.ToDateTime(requirement.EndTime);
                        long diff = Convert.ToInt64((date - now).TotalSeconds);
                        requirement.TimeLeft = diff;
                        requirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString());
                    }
                    else if (requirement.StartTime != DateTime.MaxValue && requirement.StartTime > now)
                    {
                        DateTime date = Convert.ToDateTime(requirement.StartTime);
                        long diff = Convert.ToInt64((date - now).TotalSeconds);
                        requirement.TimeLeft = diff;
                        requirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString());
                    }
                    else if (requirement.EndTime < now)
                    {
                        requirement.TimeLeft = -1;
                        if ((Status == Utilities.GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString()) || Status == Utilities.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString())) && requirement.EndTime < now)
                        {
                            requirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.NegotiationEnded.ToString());
                            EndNegotiation(requirement.RequirementID, requirement.CustomerID, sessionID);
                        }
                        else
                        {
                            requirement.Status = Status;
                        }
                    }
                    else if (requirement.StartTime == DateTime.MaxValue)
                    {
                        requirement.TimeLeft = -1;
                        requirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.UNCONFIRMED.ToString());
                    }
                    if (Status == Utilities.GetEnumDesc<PRMStatus>(PRMStatus.DELETED.ToString()))
                    {
                        requirement.TimeLeft = -1;
                        requirement.Status = Status;
                    }

                    requirement.CustomerReqAccess = false;
                    if (ds.Tables[3].Rows.Count > 0)
                    {
                        foreach (DataRow row3 in ds.Tables[3].Rows)
                        {
                            int reqCustomerId = row3["U_ID"] != DBNull.Value ? Convert.ToInt32(row3["U_ID"]) : 0;

                            if (userID == reqCustomerId)
                            {
                                requirement.CustomerReqAccess = true;
                            }
                        }
                    }

                    List<VendorDetails> vendorDetails = new List<VendorDetails>();
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        foreach (DataRow row1 in ds.Tables[1].Rows)
                        {
                            VendorDetails vendor = new VendorDetails();
                            vendor.VendorID = row1["U_ID"] != DBNull.Value ? Convert.ToInt32(row1["U_ID"]) : 0;
                            vendor.VendorName = row1["VENDOR_NAME"] != DBNull.Value ? Convert.ToString(row1["VENDOR_NAME"]) : string.Empty;
                            vendor.InitialPrice = row1["VEND_INIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["VEND_INIT_PRICE"]) : 0;
                            vendor.RunningPrice = row1["VEND_RUN_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["VEND_RUN_PRICE"]) : 0;
                            vendor.TotalInitialPrice = row1["VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["VEND_TOTAL_PRICE"]) : 0;
                            vendor.TotalRunningPrice = row1["VEND_TOTAL_PRICE_RUNNING"] != DBNull.Value ? Convert.ToDouble(row1["VEND_TOTAL_PRICE_RUNNING"]) : 0;
                            vendor.City = row1["UAD_CITY"] != DBNull.Value ? Convert.ToString(row1["UAD_CITY"]) : string.Empty;
                            vendor.Taxes = row1["TAXES"] != DBNull.Value ? Convert.ToDouble(row1["TAXES"]) : 0;
                            vendor.Rating = row1["RATING"] != DBNull.Value ? Convert.ToDouble(row1["RATING"]) : 0;

                            vendor.Taxes = row1["VEND_TAXES"] != DBNull.Value ? Convert.ToInt32(row1["VEND_TAXES"]) : 0;
                            vendor.CompanyName = row1["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row1["COMPANY_NAME"]) : string.Empty;

                            //vendor.InitialPriceWithOutTaxFreight = row1["VEND_INIT_PRICE_WITHOUT_TAX"] != DBNull.Value ? Convert.ToDouble(row1["VEND_INIT_PRICE_WITHOUT_TAX"]) : 0;
                            //vendor.VendorFreight = row1["VEND_FREIGHT"] != DBNull.Value ? Convert.ToDouble(row1["VEND_FREIGHT"]) : 0;

                            vendor.Warranty = row1["WARRANTY"] != DBNull.Value ? Convert.ToString(row1["WARRANTY"]) : string.Empty;
                            vendor.Payment = row1["PAYMENT"] != DBNull.Value ? Convert.ToString(row1["PAYMENT"]) : string.Empty;
                            vendor.Duration = row1["DURATION"] != DBNull.Value ? Convert.ToString(row1["DURATION"]) : string.Empty;
                            vendor.Validity = row1["VALIDITY"] != DBNull.Value ? Convert.ToString(row1["VALIDITY"]) : string.Empty;

                            vendor.OtherProperties = row1["OTHER_PROPERTIES"] != DBNull.Value ? Convert.ToString(row1["OTHER_PROPERTIES"]) : string.Empty;

                            vendor.IsQuotationRejected = row1["IS_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToInt32(row1["IS_QUOTATION_REJECTED"]) : 0;
                            vendor.QuotationRejectedComment = row1["QUOTATION_REJECTED_REASON"] != DBNull.Value ? Convert.ToString(row1["QUOTATION_REJECTED_REASON"]) : string.Empty;
                            vendor.PO = new RequirementPO();

                            vendor.PO.POLink = row1["PO_ATT_ID"] != DBNull.Value ? Convert.ToString(row1["PO_ATT_ID"]) : "0";

                            string fileName1 = row1["QUOTATION_URL"] != DBNull.Value ? Convert.ToString(row1["QUOTATION_URL"]) : string.Empty;
                            string URL1 = fileName1 != string.Empty ? fileName1 : string.Empty;
                            vendor.QuotationUrl = URL1;

                            string fileName2 = row1["REV_QUOTATION_URL"] != DBNull.Value ? Convert.ToString(row1["REV_QUOTATION_URL"]) : string.Empty;
                            string URL2 = fileName2 != string.Empty ? fileName2 : string.Empty;
                            vendor.RevQuotationUrl = URL2;

                            vendor.RevPrice = row1["REV_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["REV_PRICE"]) : 0;
                            //vendor.RevVendorFreight = row1["REV_VEND_FREIGHT"] != DBNull.Value ? Convert.ToDouble(row1["REV_VEND_FREIGHT"]) : 0;
                            vendor.RevVendorTotalPrice = row1["REV_VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["REV_VEND_TOTAL_PRICE"]) : 0;
                            vendor.RevVendorTotalPriceNoTax = row1["REV_VEND_TOTAL_PRICE_NO_TAX"] != DBNull.Value ? Convert.ToDouble(row1["REV_VEND_TOTAL_PRICE_NO_TAX"]) : 0;

                            vendor.IsRevQuotationRejected = row1["IS_REV_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToInt32(row1["IS_REV_QUOTATION_REJECTED"]) : 0;
                            vendor.RevQuotationRejectedComment = row1["REV_QUOTATION_REJECTED_REASON"] != DBNull.Value ? Convert.ToString(row1["REV_QUOTATION_REJECTED_REASON"]) : string.Empty;
                            //vendor.LastActiveTime = row1["LAST_ACTIVE_TIME"] != DBNull.Value ? Convert.ToString(row1["LAST_ACTIVE_TIME"]) : string.Empty;
                            //vendor.LastActiveTime = GetLastActiveTime(sessionID, vendor.VendorID);
                            vendor.LastActiveTime = row1["LAST_ACTIVE_TIME"] != DBNull.Value ? Convert.ToString(row1["LAST_ACTIVE_TIME"]) : string.Empty;
                            vendor.Discount = row1["DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row1["DISCOUNT"]) : 0;
                            vendor.ReductionPercentage = row1["REDUCTION_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row1["REDUCTION_PERCENTAGE"]) : 0;
                            vendor.AuctionModifiedDate = row1["AUCTION_MODIFIED_DATE"] != DBNull.Value ? Convert.ToDateTime(row1["AUCTION_MODIFIED_DATE"]) : DateTime.MaxValue;
                            vendor.ReductionPercentage = Math.Round(vendor.ReductionPercentage, decimal_round);

                            vendorDetails.Add(vendor);
                        }
                    }

                    List<VendorDetails> v2 = new List<VendorDetails>();
                    if (requirement.StartTime > DateTime.UtcNow)
                    {
                        v2 = vendorDetails.Where(v => v.IsQuotationRejected == 0).Where(v => v.TotalRunningPrice > 0).OrderByDescending(v => v.TotalRunningPrice).ThenBy(p => p.AuctionModifiedDate).ToList();
                        v2.AddRange(vendorDetails.Where(v => v.IsQuotationRejected != 0).Where(v => v.TotalRunningPrice > 0).OrderByDescending(v => v.TotalRunningPrice).ThenBy(p => p.AuctionModifiedDate).ToList());
                        v2.AddRange(vendorDetails.Where(v => v.InitialPrice == 0).ToList());
                    }
                    else
                    {
                        v2 = vendorDetails.Where(v => v.IsQuotationRejected == 0).Where(v => v.TotalRunningPrice > 0).OrderByDescending(v => v.TotalRunningPrice).ThenBy(p => p.AuctionModifiedDate).ToList();
                        v2.AddRange(vendorDetails.Where(v => v.IsQuotationRejected != 0).Where(v => v.TotalRunningPrice > 0).OrderByDescending(v => v.TotalRunningPrice).ThenBy(p => p.AuctionModifiedDate).ToList());
                        v2.AddRange(vendorDetails.Where(v => v.TotalRunningPrice == 0).ToList());
                    }
                    if (v2.Count > 0)
                    {
                        VendorDetails vendor1 = v2[0];
                        if (vendor1.TotalRunningPrice == 0)
                        {
                            requirement.Price = vendor1.TotalInitialPrice;
                        }
                        else if (vendor1.TotalInitialPrice < vendor1.TotalRunningPrice && vendor1.TotalInitialPrice != 0)
                        {
                            requirement.Price = vendor1.TotalInitialPrice;
                        }
                        else
                        {
                            requirement.Price = vendor1.TotalRunningPrice;
                        }
                    }
                    foreach (VendorDetails vendor in v2.Where(v => v.IsQuotationRejected == 0).OrderByDescending(v => v.TotalInitialPrice).ThenBy(p => p.AuctionModifiedDate).ToList())
                    {
                        vendor.Rank = v2.IndexOf(vendor) + 1;
                    }
                    if (requirement.CustomerID == userID || requirement.SuperUserID == userID || requirement.CustomerReqAccess == true)
                    {
                        requirement.AuctionVendors = v2;
                    }
                    else
                    {
                        requirement.AuctionVendors = v2.Where(v => v.VendorID == userID).OrderByDescending(v => v.TotalInitialPrice).ThenBy(p => p.AuctionModifiedDate).ToList();
                    }

                    requirement.SelectedVendor = requirement.AuctionVendors.FirstOrDefault(v => v.VendorID == requirement.SelectedVendorID);

                    int productSNo = 0;

                    List<RequirementItems> ListRequirementItems = new List<RequirementItems>();
                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        foreach (DataRow row2 in ds.Tables[2].Rows)
                        {
                            RequirementItems RequirementItems = new RequirementItems();
                            RequirementItems.ProductSNo = productSNo++;
                            RequirementItems.ItemID = row2["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_ID"]) : 0;
                            RequirementItems.RequirementID = row2["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row2["REQ_ID"]) : 0;
                            RequirementItems.ProductIDorName = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                            RequirementItems.ProductNo = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                            RequirementItems.ProductDescription = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                            RequirementItems.ProductQuantity = row2["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row2["QUANTITY"]) : 0;
                            RequirementItems.ProductBrand = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;
                            RequirementItems.OthersBrands = row2["OTHER_BRAND"] != DBNull.Value ? Convert.ToString(row2["OTHER_BRAND"]) : string.Empty;
                            RequirementItems.ProductImageID = row2["IMAGE_ID"] != DBNull.Value ? Convert.ToInt32(row2["IMAGE_ID"]) : 0;
                            RequirementItems.IsDeleted = row2["IS_DELETED"] != DBNull.Value ? Convert.ToInt32(row2["IS_DELETED"]) : 0;
                            RequirementItems.ProductQuantityIn = row2["QUANTITY_IN"] != DBNull.Value ? Convert.ToString(row2["QUANTITY_IN"]) : string.Empty;
                            RequirementItems.SelectedVendorID = row2["SELECTED_VENDOR_ID"] != DBNull.Value ? Convert.ToInt32(row2["SELECTED_VENDOR_ID"]) : 0;

                            RequirementItems.ProductIDorNameCustomer = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                            RequirementItems.ProductNoCustomer = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                            RequirementItems.ProductDescriptionCustomer = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                            RequirementItems.ProductBrandCustomer = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;
                            RequirementItems.ItemMinReduction = row2["ITEM_MIN_REDUCTION"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_MIN_REDUCTION"]) : 0;
                            RequirementItems.ItemLastPrice = row2["LAST_ITEM_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["LAST_ITEM_PRICE"]) : 0;
                            RequirementItems.HsnCodeCustomer = row2["HSN_CODE"] != DBNull.Value ? Convert.ToString(row2["HSN_CODE"]) : string.Empty;
                            RequirementItems.HsnCode = row2["HSN_CODE"] != DBNull.Value ? Convert.ToString(row2["HSN_CODE"]) : string.Empty;
                            //RequirementItems.ITEM_LEVEL_LEAST_PRICE = row2["ITEM_LEVEL_LEAST_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_LEVEL_LEAST_PRICE"]) : 0;

                            ListRequirementItems.Add(RequirementItems);
                        }
                        requirement.ItemSNoCount = productSNo;
                    }

                    requirement.ListRequirementItems = ListRequirementItems;

                    PriceComparison priceComparision = GetPriceComparison(requirement.RequirementID, requirement.CustomerID, sessionID);

                    double totalItemsL1Price = 0;
                    double totalItemsMinimunPrice = 0;
                    double itemWiseSavings = 0;
                    customerDetailReport = Utilities.GetUserDetails(requirement.CustomerID);
                    if (priceComparision.PriceCompareObject != null)
                    {
                        for (var i = 0; i < priceComparision.PriceCompareObject.Count; i++)
                        {
                            totalItemsL1Price += priceComparision.PriceCompareObject[i].LeastBidderPrice;
                            totalItemsMinimunPrice += priceComparision.PriceCompareObject[i].MinPrice;
                        }

                        itemWiseSavings = totalItemsL1Price - totalItemsMinimunPrice;
                    }

                    Attachment emilAttachmentReqPDF = null;
                    long ticks = DateTime.UtcNow.Ticks;
                    string PDFfileName = string.Empty;
                    int margin = 24;
                    PdfDocument pdf = PdfGenerator.GeneratePdf(GenerateReportRequirementHTML(requirement, itemWiseSavings, customerDetailReport), PdfSharp.PageSize.A4, margin);
                    pdf.Save(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "REQ_COMPLETE_REPORT_" + requirement.RequirementID + ticks + ".pdf"));
                    PDFfileName = "REQ_COMPLETE_REPORT_" + requirement.RequirementID + ticks + ".pdf";
                    Response responce = SaveAttachment(PDFfileName);
                    PDFfileName = responce.ObjectID.ToString();
                    string attachmentsave = SaveReqAttachments(requirement.RequirementID, requirement.CustomerID, PDFfileName, 0, "PDF_REPORT_REQUIREMENT");
                    if (requirement.IsTabular)
                    {
                        Attachment emilAttachmentReportReqItems = null;

                        long ticks1 = DateTime.UtcNow.Ticks;

                        string PDFfileName1 = string.Empty;
                        int margin1 = 24;
                        PdfDocument pdf1 = PdfGenerator.GeneratePdf(GenerateReportRequirementItemstHTML(requirement, priceComparision, itemWiseSavings, customerDetailReport), PdfSharp.PageSize.A4, margin1);
                        pdf1.Save(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "REQ_ITEM_WISE_REPORT_" + requirement.RequirementID + ticks1 + ".pdf"));
                        PDFfileName1 = "REQ_ITEM_WISE_REPORT_" + requirement.RequirementID + ticks1 + ".pdf";
                        Response responce1 = SaveAttachment(PDFfileName1);
                        PDFfileName1 = responce1.ObjectID.ToString();
                        string attachmentsave1 = SaveReqAttachments(requirement.RequirementID, requirement.CustomerID, PDFfileName1, 0, "PDF_REPORT_ITEM_WISE");
                    }
                }
            }
            catch (Exception ex)
            {
                Requirement req = new Requirement();
                req.ErrorMessage = ex.Message;
                return req;
            }

            return requirement;
        }

        public Response RefreshRequirementCurrency(int reqid, string sessionid)
        {
            Response response = new Response();

            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqid);
                DataSet ds = sqlHelper.SelectList("fwd_HandleRequirementCurrency", sd);
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response ValidateCurrencyRate(int reqid, string sessionid)
        {
            Response response = new Response();

            try
            {
                Utilities.ValidateSession(sessionid);
                string query = $@"SELECT * FROM (SELECT RD.REQ_ID, RD.REQ_CURRENCY AS CURRENCY, RS.REQ_SETTING_VALUE, CF.CURRENCY_FACTOR, (CASE WHEN RS.REQ_SETTING_VALUE = CF.CURRENCY_FACTOR THEN 0 ELSE 1 END) AS IS_CHANGED 
                                FROM fwd_RequirementDetails RD 
                                INNER JOIN fwd_RequirementSettings RS ON RS.REQ_ID = RD.REQ_ID AND RS.REQ_SETTING = CONCAT('CURRENCY_', RD.REQ_ID, '_', RD.REQ_CURRENCY)
                                INNER JOIN CurrencyFactor CF ON CF.CURRENCY_CODE = RD.REQ_CURRENCY AND CF.COMP_ID = getcompanyid(RD.U_ID)
                                WHERE RD.REQ_ID = {reqid} AND RS.REQ_SETTING LIKE 'CURRENCY%'
                                UNION
                                SELECT AD.REQ_ID, IFNULL(AD.SELECTED_VENDOR_CURRENCY, '') AS CURRENCY, RS.REQ_SETTING_VALUE, CF.CURRENCY_FACTOR, (CASE WHEN RS.REQ_SETTING_VALUE = CF.CURRENCY_FACTOR THEN 0 ELSE 1 END) AS IS_CHANGED
                                FROM fwd_AuctionDetails AD 
                                INNER JOIN fwd_RequirementDetails RD ON RD.REQ_ID = AD.REQ_ID
                                INNER JOIN fwd_RequirementSettings RS ON RS.REQ_ID = AD.REQ_ID AND RS.REQ_SETTING = CONCAT('CURRENCY_', AD.REQ_ID, '_', IFNULL(AD.SELECTED_VENDOR_CURRENCY, ''))
                                INNER JOIN CurrencyFactor CF ON CF.CURRENCY_CODE = IFNULL(AD.SELECTED_VENDOR_CURRENCY, '') AND CF.COMP_ID = getcompanyid(RD.U_ID)
                                WHERE AD.REQ_ID = {reqid} AND RS.REQ_SETTING LIKE 'CURRENCY%') AS TEMP WHERE IS_CHANGED = 1;";

                DataSet ds = sqlHelper.ExecuteQuery(query);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    response.ObjectID = 1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            response.CurrentTime = Convert.ToDateTime(DateTime.UtcNow.ToString());
            return response;
        }

        public List<ReductionSetting> getReductionSettings(int reqid, string sessionid)
        {
            List<ReductionSetting> settings = new List<ReductionSetting>();
            try
            {
                Utilities.ValidateSession(sessionid);
                CORE.DataNamesMapper<ReductionSetting> mapper = new CORE.DataNamesMapper<ReductionSetting>();
                string query = $@"SELECT REQ_ID, REDUCTION_SETTING, REDUCTION_SETTING_VALUE FROM fwd_reductionsettings WHERE REQ_ID = {reqid}";
                var dt = sqlHelper.SelectQuery(query);
                settings = mapper.Map(dt).ToList();
            }
            catch (Exception ex)
            {
                settings.Clear();
                settings.Add(new ReductionSetting() { ErrorMessage = ex.Message });
            }

            return settings;
        }

        public Response GetIsAuthorized(int userID, int reqID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {
                sd.Add("P_U_ID", userID);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("fwd_GetIsAuthorized", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }
            }

            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response StartNegotiation(int reqID, int userID, string sessionID)
        {
            string ScreenName = "START_NEGOTIATION";
            Response response = new Response();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                int isValidSession = ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("fwd_StartNegotiation", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response UploadQuotationForSelectVendorCeilingPrices(List<RequirementItems> quotationObject,
            int userID,
            int reqID,
            string sessionID,
            double price, // 5
            double tax,
            double freightcharges,
            double vendorBidPrice,
            string warranty,
            string payment, // 10
            string duration,
            string validity,
            int revised,
            string desFileName,
            double discountAmount, // 15
            List<RequirementTaxes> listRequirementTaxes,
            string otherProperties,
            string gstNumber,
            double packingCharges,
            double packingChargesTaxPercentage, // 20
            double packingChargesWithTax,
            double installationCharges,
            double installationChargesTaxPercentage,
            double installationChargesWithTax,
            double revpackingCharges, // 25
            double revinstallationCharges,
            double revpackingChargesWithTax,
            double revinstallationChargesWithTax,
            string selectedVendorCurrency,
            string uploadType, // 30
            List<FileUpload> multipleAttachments,
            double freightCharges,
            double freightChargesTaxPercentage,
            double freightChargesWithTax,
            double revfreightCharges,
            double revfreightChargesWithTax,
            string INCO_TERMS, int TaxFiledValidation,
            string isVendAckChecked,
            int surrogateId = 0, string SurrogateComments = "",
            string quotFreezeTime = "")
        {
            string moreInfo = string.Empty;
            string countAtt = string.Empty;
            int CallerID = userID;
            string ScreenName = "UPLOAD_QUOTATION";
            string RevScreenName = "UPLOAD_REV_QUOTATION";
            Response response = new Response();
            DataSet ds = new DataSet();
            //int customerID = 0;
            //DataSet ds1 = GetRequirementDetailsHub(reqID);
            //Requirement requirement = GetRequirementDataFilter(customerID, sessionID, ds1);
            //customerID = requirement.CustomerID;
            Response quotationFileObject = new Response();

            try
            {
                string quotationItems = string.Empty;
                string queryValues = string.Empty;
                int count = 0;
                string delete = string.Format("DELETE FROM fwd_tempquotations WHERE REQ_ID = {0} AND U_ID  = {1};", reqID, userID);
                string query = delete + "INSERT INTO fwd_tempquotations(U_ID, REQ_ID, ITEM_ID, PROD_ID, PROD_NO, DESCRIPTION, BRAND," +
                    " OTHER_BRANDS, VEND_TAXES, WARRANTY, PAYMENT, DURATION, VALIDITY, DISCOUNT, OTHER_PROPERTIES, UNIT_PRICE," +
                    "REV_UNIT_PRICE, PRICE, VEND_INIT_PRICE, VEND_FREIGHT, VEND_TOTAL_PRICE, IMAGE_ID, IS_REVISED," +
                    "C_GST, S_GST, I_GST, UNIT_MRP, UNIT_DISCOUNT, REV_UNIT_DISCOUNT, GST_NUMBER, VENDOR_UNITS, IS_REGRET, " +
                    "REGRET_COMMENTS,ITEM_LEVEL_INITIALCOMMENT,ITEM_LEVEL_REVCOMMENT," +
                    "ITEM_FREIGHT_CHARGES, ITEM_FREIGHT_TAX,PACKING_CHARGES,PACKING_CHARGES_TAX_PERCENTAGE,PACKING_CHARGES_WITH_TAX," +
                    "INSTALLATION_CHARGES,INSTALLATION_CHARGES_TAX_PERCENTAGE,INSTALLATION_CHARGES_WITH_TAX,REV_PACKING_CHARGES," +
                    "REV_INSTALLATION_CHARGES,SELECTED_VENDOR_CURRENCY,ITEM_REV_FREIGHT_CHARGES," +
                    "REV_INSTALLATION_CHARGES_WITH_TAX,REV_PACKING_CHARGES_WITH_TAX," +

                    "FREIGHT_CHARGES," +
                    "FREIGHT_CHARGES_TAX_PERCENTAGE," +
                    "FREIGHT_CHARGES_WITH_TAX," +
                    "REV_FREIGHT_CHARGES," +
                    "REV_FREIGHT_CHARGES_WITH_TAX, " +
                    "INCO_TERMS, " +
                    "ProductQuotationTemplateJson,IS_VEND_ACK_CHECKED,HSN_CODE,IS_QUOT_LATE " +
                    ") Values";

                moreInfo = "TESING2";
                foreach (RequirementItems quotation in quotationObject)
                {
                    double itemPrice = 0;
                    double unitPrice = 0;
                    if (revised == 0)
                    {
                        itemPrice = quotation.ItemPrice;
                        unitPrice = quotation.UnitPrice;
                        quotation.ItemLevelRevComments = quotation.ItemLevelInitialComments;
                    }
                    //To handle single quote issue, crap code, need to improve.
                    string columns = "({0},{1},{2},~$^{3}~$^,~$^{4}~$^,~$^{5}~$^,~$^{6}~$^," +
                        "~$^{7}~$^,{8},~$^{9}~$^,~$^{10}~$^,~$^{11}~$^,~$^{12}~$^,{13},~$^{14}~$^,{15}," +
                        "{16},{17},{18},{19},{20},{21},{22}," +
                        "{23},{24},{25},{26}," +
                        "{27},{28}," +
                        "~$^{29}~$^,~$^{30}~$^,{31},~$^{32}~$^,~$^{33}~$^,~$^{34}~$^,~$^{35}~$^,~$^{36}~$^,~$^{37}~$^,~$^{38}~$^,~$^{39}~$^,~$^{40}~$^,~$^{41}~$^,~$^{42}~$^," +
                        "~$^{43}~$^,~$^{44}~$^,~$^{45}~$^,~$^{46}~$^,~$^{47}~$^,~$^{48}~$^," +

                        "~$^{49}~$^," +
                        "~$^{50}~$^," +
                        "~$^{51}~$^," +
                        "~$^{52}~$^," +
                        "~$^{53}~$^," +
                        "~$^{54}~$^," +
                        "~$^{55}~$^," +
                        "~$^{56}~$^," +
                        "~$^{57}~$^,~$^{58}~$^),";

                    queryValues = queryValues + string.Format(columns,
                        userID, reqID, quotation.ItemID, quotation.ProductIDorName, quotation.ProductNo, quotation.ProductDescription, quotation.ProductBrand,
                        quotation.OthersBrands, tax, warranty, payment, duration, validity, discountAmount, otherProperties, quotation.CeilingPrice,
                        quotation.RevUnitPrice, itemPrice, price, freightcharges, vendorBidPrice, quotation.FileId,
                        revised, quotation.CGst, quotation.SGst, quotation.IGst,
                        quotation.UnitMRP, quotation.UnitDiscount, quotation.RevUnitDiscount,
                        gstNumber, quotation.VendorUnits, (quotation.IsRegret ? 1 : 0), quotation.RegretComments, quotation.ItemLevelInitialComments, quotation.ItemLevelRevComments,
                        0, 0, packingCharges, packingChargesTaxPercentage, packingChargesWithTax,
                        installationCharges, installationChargesTaxPercentage, installationChargesWithTax, revpackingCharges, revinstallationCharges,
                        selectedVendorCurrency, 0, revinstallationChargesWithTax, revpackingChargesWithTax,

                        freightCharges,
                        freightChargesTaxPercentage,
                        freightChargesWithTax,
                        revfreightCharges,
                        revfreightChargesWithTax,
                        INCO_TERMS,
                        quotation.ProductQuotationTemplateJson, isVendAckChecked, quotation.HsnCode, 0);

                    count++;

                }

                response.ObjectID = count;
                if (!string.IsNullOrEmpty(queryValues))
                {
                    queryValues = queryValues.Replace(@"'", "\\'");
                    queryValues = queryValues.Replace("~$^", "'");
                    queryValues = queryValues.Substring(0, queryValues.Length - 1);
                    queryValues = queryValues + ";";
                    query = query + queryValues;

                    sqlHelper.ExecuteQuery(query);

                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_U_ID", userID);
                    sd.Add("P_REQ_ID", reqID);
                    sd.Add("P_SURROGATE_ID", surrogateId);
                    sd.Add("P_SURROGATE_COMMENTS", SurrogateComments);
                    ds = sqlHelper.SelectList("fwd_UploadQuotationBulkForCeilingPrice", sd);
                }

            }
            catch (Exception ex)
            {
                logger.Error("ERROR UPLOAD" + moreInfo + "TESTING", ex);
                response.ErrorMessage = ex.Message + moreInfo;// + "------" +  ex.InnerException.Message;                
            }

            response.SessionID = sessionID;

            return response;
        }

        public Response saveReductionLevelSetting(ReductionSetting reductionSetting)
        {
            Response response = new Response();
            try
            {
                int isValidSession = ValidateSession(reductionSetting.SessionID);
                string query = $"DELETE FROM fwd_reductionsettings WHERE REQ_ID = {reductionSetting.REQ_ID} AND REDUCTION_SETTING = '{reductionSetting.REDUCTION_SETTING}'";
                sqlHelper.ExecuteNonQuery_IUD(query);

                query = $@"INSERT INTO fwd_reductionsettings (REQ_ID, REDUCTION_SETTING, REDUCTION_SETTING_VALUE, DATE_CREATED, DATE_MODIFIED, CREATED_BY, MODIFIED_BY) VALUES 
                    ({reductionSetting.REQ_ID}, '{reductionSetting.REDUCTION_SETTING}', '{reductionSetting.REDUCTION_SETTING_VALUE}', NOW(), NOW(), {reductionSetting.USER}, {reductionSetting.USER})";
                sqlHelper.ExecuteNonQuery_IUD(query);
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SendQuotationApprovalStatusEmail(List<RequirementItems> items, int vendorId, int reqId, int customerId, string sessionId)
        {
            bool isAllSameState = false;
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {
                if (items != null && items.Count > 0)
                {
                    int coreItems = items.Where(i => i.IsCoreProductCategory > 0).Count();
                    bool isAllRejected = items.Where(i => i.IsCoreProductCategory > 0 && i.IsItemQuotationRejected == 1).Count() == coreItems;
                    bool isAllApproved = items.Where(i => i.IsCoreProductCategory > 0 && i.IsItemQuotationRejected == 0).Count() == coreItems;

                    int isAnyItemsApproved = items.Where(i => i.IsCoreProductCategory > 0 && i.IsItemQuotationRejected == 0).Count();
                    int isAnyItemsRejected = items.Where(i => i.IsCoreProductCategory > 0 && i.IsItemQuotationRejected == 1).Count();
                    int noAction = items.Where(i => i.IsCoreProductCategory > 0 && i.IsItemQuotationRejected == -1).Count();

                    bool isPartiallyApproved = false;
                    if (!isAllRejected && !isAllApproved)
                    {
                        isPartiallyApproved = items.Any(i => i.IsCoreProductCategory > 0 && (i.IsItemQuotationRejected == 1 || i.IsItemQuotationRejected == 0));
                    }
                    if (isAllRejected || isAllApproved) { isPartiallyApproved = false; }
                    if (isAnyItemsApproved == 1) { isPartiallyApproved = false; }
                    if (isAnyItemsRejected == 1 && isAnyItemsRejected != coreItems && noAction > 1) { isPartiallyApproved = false; }

                    bool doSendEmail = ConfigurationManager.AppSettings["TENDER_VENDOR_QUOTATION_EMAIL"] != null ? Convert.ToBoolean(ConfigurationManager.AppSettings["TENDER_VENDOR_QUOTATION_EMAIL"]) : false;

                    if (doSendEmail)
                    {
                        Requirement Requirement = GetRequirementDataOfflinePrivate(reqId, customerId, sessionId);
                        UserInfo Customer = GetUser(customerId);
                        UserInfo Vendor = GetUser(vendorId);

                        User altUser = GetReqAlternateCommunications(Requirement.RequirementID, Convert.ToInt32(Vendor.UserID));
                        Requirement.Module = "VENDOR_QUOTATION_APPROVED";

                        if (isAllApproved && Requirement.IsDiscountQuotation != 2)
                        {
                            string body = GenerateEmailBody("VendoremailForQuotationAprovel");
                            body = String.Format(body, Vendor.FirstName, Vendor.LastName, Requirement.RequirementNumber, Requirement.Title, "", Customer.FirstName, Customer.LastName);
                            SendEmail(Vendor.Email + "," + Vendor.AltEmail + "," + altUser.AltEmail, "ReqID: " + reqId + " Title: " + Requirement.Title + " - Quotation Shortlisted", body, reqId, vendorId, Requirement.Module, sessionId).ConfigureAwait(false);

                            string body2 = GenerateEmailBody("VendorsmsForQuotationAprovel");
                            body2 = String.Format(body2, Vendor.FirstName, Vendor.LastName, reqId, Requirement.Title, "", Customer.FirstName, Customer.LastName);
                            body2 = body2.Replace("<br/>", "");
                            //  SendSMS(string.Empty, Vendor.PhoneNum + "," + Vendor.AltPhoneNum + "," + altUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body2.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), reqId, vendorId, Requirement.Module, sessionId).ConfigureAwait(false);
                        }

                        if (isAllRejected && Requirement.IsDiscountQuotation != 2)
                        {
                            string body = GenerateEmailBody("VendoremailForQuotationRejection");
                            body = String.Format(body, Vendor.FirstName, Vendor.LastName, Requirement.RequirementNumber, Requirement.Title, string.Empty, Customer.FirstName, Customer.LastName);
                            SendEmail(Vendor.Email + "," + Vendor.AltEmail + "," + altUser.AltEmail, "Req Number: " + Requirement.RequirementNumber + " Title: " + Requirement.Title + " - Quotation Rejected", body, reqId, vendorId, Requirement.Module, sessionId).ConfigureAwait(false);

                            string body2 = GenerateEmailBody("VendorsmsForQuotationRejection");
                            body2 = String.Format(body2, Vendor.FirstName, Vendor.LastName, reqId, Requirement.Title, string.Empty, Customer.FirstName, Customer.LastName);
                            body2 = body2.Replace("<br/>", "");
                            // SendSMS(string.Empty, Vendor.PhoneNum + "," + Vendor.AltPhoneNum + "," + altUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body2.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), reqId, vendorId, Requirement.Module, sessionId).ConfigureAwait(false);
                        }

                        if (isPartiallyApproved && Requirement.IsDiscountQuotation != 2)
                        {
                            string body = GenerateEmailBody("VendoremailForPartialQuotationApproved");
                            body = String.Format(body, Vendor.FirstName, Vendor.LastName, reqId, Requirement.Title, "", Customer.FirstName, Customer.LastName);
                            SendEmail(Vendor.Email + "," + Vendor.AltEmail + "," + altUser.AltEmail, "ReqID: " + reqId + " Title: " + Requirement.Title, body, reqId, vendorId, Requirement.Module, sessionId).ConfigureAwait(false);

                            string body2 = GenerateEmailBody("VendoremailForPartialQuotationApproved");
                            body2 = String.Format(body2, Vendor.FirstName, Vendor.LastName, reqId, Requirement.Title, "", Customer.FirstName, Customer.LastName);
                            body2 = body2.Replace("<br/>", "");
                            //  SendSMS(string.Empty, Vendor.PhoneNum + "," + Vendor.AltPhoneNum + "," + altUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body2.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), reqId, vendorId, Requirement.Module, sessionId).ConfigureAwait(false);
                        }

                        if (Requirement.IsDiscountQuotation != 2 && (isAnyItemsApproved == 1 && coreItems != isAnyItemsApproved && isAnyItemsRejected > 1) || (isAnyItemsRejected == 1 && isAnyItemsRejected != coreItems && noAction > 1))
                        {
                            string body = GenerateEmailBody("VendoremailForPartialQuotationRejection");
                            body = String.Format(body, Vendor.FirstName, Vendor.LastName, reqId, Requirement.Title, "", Customer.FirstName, Customer.LastName);
                            SendEmail(Vendor.Email + "," + Vendor.AltEmail + "," + altUser.AltEmail, "ReqID: " + reqId + " Title: " + Requirement.Title, body, reqId, vendorId, Requirement.Module, sessionId).ConfigureAwait(false);

                            string body2 = GenerateEmailBody("VendoremailForPartialQuotationRejection");
                            body2 = String.Format(body2, Vendor.FirstName, Vendor.LastName, reqId, Requirement.Title, "", Customer.FirstName, Customer.LastName);
                            body2 = body2.Replace("<br/>", "");
                            // SendSMS(string.Empty, Vendor.PhoneNum + "," + Vendor.AltPhoneNum + "," + altUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body2.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), reqId, vendorId, Requirement.Module, sessionId).ConfigureAwait(false);
                        }
                        //if (Requirement.IsDiscountQuotation != 2 && )
                        //{

                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response ItemQuotationApproval(int reqId, int customerId, int vendorId, int itemId, bool value, string comment, string sessionId)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {
                string ScreenName = "";
                if (value == false || comment == "")
                {
                    ScreenName = "VENDOR_QUOTATION_APPROVED";
                }
                else if (value == true && comment != "")
                {
                    ScreenName = "VENDOR_QUOTATION_REJECTED";
                }
                if (value)
                {
                    comment = "Reject Comments:" + comment;
                }
                else
                {

                    comment = "Approved:" + comment;
                }

                int itemQuotationRejected = -1;
                itemQuotationRejected = value ? 1 : 0;

                string query = $"UPDATE fwd_quotations SET IS_ITEM_QUOTATION_REJECTED = {itemQuotationRejected}, ITEM_QUOTATION_REJECTED_COMMENT = '{comment}', DATE_MODIFIED=UTC_TIMESTAMP, MODIFIED_BY={customerId} WHERE ITEM_ID = {itemId} AND REQ_ID = {reqId} AND U_ID = {vendorId}";
                sqlHelper.ExecuteQuery(query);
                if (!value)
                {
                    query = $"UPDATE fwd_auctiondetails SET IS_QUOTATION_REJECTED = {itemQuotationRejected}, QUOTATION_REJECTED_REASON = 'Approved-', DATE_MODIFIED=UTC_TIMESTAMP, MODIFIED_BY={customerId} WHERE REQ_ID = {reqId} AND U_ID = {vendorId};";
                    query += $@"UPDATE fwd_quotations 
	                            SET IS_ITEM_QUOTATION_REJECTED = 0
	                            FROM fwd_quotations Q
	                            INNER JOIN  fwd_requirementitems RI
                                ON Q.ITEM_ID = RI.ITEM_ID
                                WHERE Q.REQ_ID = {reqId} AND RI.REQ_ID = {reqId} AND Q.U_ID = {vendorId} AND dbo.fn_Is_CoreCategoryProduct(RI.CATALOGUE_ITEM_ID, '') <>  1;";
                    sqlHelper.ExecuteQuery(query);
                }
                else
                {
                    DataTable table = sqlHelper.SelectQuery($"SELECT * FROM fwd_quotations WHERE U_ID = {vendorId} AND REQ_ID = {reqId} AND IS_ITEM_QUOTATION_REJECTED <= 0");
                    if (table != null && table.Rows.Count <= 0)
                    {
                        query = $"UPDATE fwd_auctiondetails SET IS_QUOTATION_REJECTED = 1, QUOTATION_REJECTED_REASON = '{comment}', DATE_MODIFIED=UTC_TIMESTAMP, MODIFIED_BY={customerId} WHERE REQ_ID = {reqId} AND U_ID = {vendorId}";
                        sqlHelper.ExecuteQuery(query);
                    }
                }

                //Process Overall Rejection
                var itemLevelRejectionStatusQuery = $@"SELECT IS_ITEM_QUOTATION_REJECTED FROM fwd_quotations Q INNER JOIN fwd_requirementitems RI ON RI.ITEM_ID = Q.ITEM_ID
                                                WHERE Q.REQ_ID = {reqId} AND RI.REQ_ID = {reqId} AND U_ID = {vendorId} AND dbo.fn_Is_CoreCategoryProduct(RI.CATALOGUE_ITEM_ID, '') = 1
                                                GROUP BY IS_ITEM_QUOTATION_REJECTED";
                var itemLevelRejectionStatusDS = sqlHelper.ExecuteQuery(itemLevelRejectionStatusQuery);
                if (itemLevelRejectionStatusDS != null && itemLevelRejectionStatusDS.Tables.Count > 0 && itemLevelRejectionStatusDS.Tables[0].Rows.Count == 1)
                {
                    int flag = Convert.ToInt32(itemLevelRejectionStatusDS.Tables[0].Rows[0][0]);
                    query = $@"UPDATE fwd_AuctionDetails SET IS_QUOTATION_REJECTED = {flag} WHERE REQ_ID = {reqId} AND U_ID = {vendorId};";

                    query += $@"UPDATE fwd_quotations 
	                            SET IS_ITEM_QUOTATION_REJECTED = {flag}
	                            FROM fwd_quotations Q
	                            INNER JOIN fwd_requirementitems RI
                                ON Q.ITEM_ID = RI.ITEM_ID
                                WHERE Q.REQ_ID = {reqId} AND RI.REQ_ID = {reqId} AND Q.U_ID = {vendorId} AND dbo.fn_Is_CoreCategoryProduct(RI.CATALOGUE_ITEM_ID, '') <>  1;";
                    sqlHelper.ExecuteNonQuery_IUD(query);
                }


                bool doSendEmail = ConfigurationManager.AppSettings["TENDER_VENDOR_QUOTATION_EMAIL"] != null ? Convert.ToBoolean(ConfigurationManager.AppSettings["TENDER_VENDOR_QUOTATION_EMAIL"]) : false;
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveRequirementItemList(RequirementItems[] requirementitemlist, int reqid, int vendorid, int user, string sessionid)
        {
            Response response = new Response();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                int isValidSession = ValidateSession(sessionid);
                if (requirementitemlist != null && requirementitemlist.Length > 0)
                {
                    string query = string.Empty;
                    foreach (var item in requirementitemlist)
                    {
                        if (item.ItemID > 0)
                        {
                            if (vendorid > 0)
                            {
                                query += $"UPDATE fwd_quotations SET CEILING_PRICE = {item.CeilingPrice},ProductQuotationTemplateJson= '{item.ProductQuotationTemplateJson}',DATE_MODIFIED=UTC_TIMESTAMP,MODIFIED_BY = {user} WHERE ITEM_ID = {item.ItemID} AND U_ID = {vendorid} AND REQ_ID = {reqid};";
                            }
                            else
                            {
                                query += $@"UPDATE fwd_requirementitems SET CEILING_PRICE = {item.CeilingPrice},PRODUCT_QUOTATION_JSON= '{item.ProductQuotationTemplateJson}',DATE_MODIFIED=UTC_TIMESTAMP,MODIFIED_BY = {user} WHERE ITEM_ID = {item.ItemID} AND REQ_ID = {reqid};";
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(query))
                    {
                        sqlHelper.SelectQuery(query);
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response ToggleCounterBid(int reqId, int isCounterBid, int user, string sessionId)
        {
            Response response = new Response();
            try
            {
                int isValidSession = ValidateSession(sessionId);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqId);
                sd.Add("P_IS_CB_ENABLED", isCounterBid);
                sd.Add("P_MODIFIED_BY", user);
                DataSet ds = sqlHelper.SelectList("fwd_ConvertToCounterBid", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response MarkAsCompleteREQ(int isREQCompleted, int userID, int reqID, string sessionID)
        {
            Response response = new Response();
            Requirement reqVendor = new Requirement();
            UserInfo customer = new UserInfo();
            UserInfo vendor = new UserInfo();

            string subject = string.Empty;
            string bodyEMAIL = string.Empty;
            string bodySMS = string.Empty;
            string bodyTELEGRAM = string.Empty;
            string actionTelegaram = string.Empty;
            string ScreenName = "";
            try
            {
                int isValidSession = ValidateSession(sessionID);
                string query = string.Format("UPDATE fwd_requirementdetails SET LOCK_REQ = {0} WHERE REQ_ID = {1} and U_ID = {2};", isREQCompleted, reqID, userID);
                sqlHelper.ExecuteNonQuery_IUD(query);
                response.ObjectID = reqID;

                reqVendor = GetRequirementData(reqID, userID, sessionID);
                customer = GetUser(reqVendor.CustomerID);

                reqVendor.AuctionVendors = reqVendor.AuctionVendors.Where(v => v.IsQuotationRejected == 0).ToList();

                foreach (VendorDetails v in reqVendor.AuctionVendors)
                {
                    ScreenName = "MARK_AS_COMPLETE_CUSTOMER";

                    vendor = GetUser(v.VendorID);

                    subject = customer.Institution + " IS NO MORE ACCEPTING QUOTATIONS.";
                    bodyEMAIL = GenerateEmailBody("MarkAsCompleteVendorEMAIL");
                    bodySMS = GenerateEmailBody("MarkAsCompleteVendorSMS");
                    bodyTELEGRAM = GenerateEmailBody("UniqeTemplateTELEGRAM");
                    actionTelegaram = "MARK AS COMPLETE";

                    bodyEMAIL = String.Format(bodyEMAIL, vendor.FirstName, vendor.LastName, reqVendor.RequirementID, reqVendor.Title, customer.Institution);
                    SendEmail(vendor.Email + "," + vendor.AltEmail,
                                            "REQ-ID: " + reqID + " - " + subject,
                                            bodyEMAIL, reqVendor.RequirementID, v.VendorID, ScreenName,
                                            sessionID).ConfigureAwait(false);

                    bodySMS = String.Format(bodySMS, vendor.FirstName, vendor.LastName, reqVendor.RequirementID, reqVendor.Title, customer.Institution);
                    bodySMS = bodySMS.Replace("<br/>", "");
                    bodyTELEGRAM = string.Format(bodyTELEGRAM, reqVendor.RequirementID, actionTelegaram, customer.Institution, vendor.Institution, "");
                    CBUtility.SendCBTelegramAlerts(bodyTELEGRAM);

                }

                #region TO CUSTOMER SUB USER

                ScreenName = "MARK_AS_COMPLETE_CUSTOMER";
                subject = "REQUIREMENT IS MARKED AS COMPLETED.";
                bodyEMAIL = GenerateEmailBody("MarkAsCompleteCustomerEMAIL");
                bodySMS = GenerateEmailBody("MarkAsCompleteCustomerSMS");
                bodyTELEGRAM = GenerateEmailBody("UniqeTemplateTELEGRAM");
                actionTelegaram = "MARK AS COMPLETE";

                bodyEMAIL = String.Format(bodyEMAIL, customer.FirstName, customer.LastName, reqVendor.RequirementID, reqVendor.Title, vendor.Institution);
                SendEmail(customer.Email + "," + customer.AltEmail,
                                        "REQ-ID: " + reqID + " - " + subject,
                                        bodyEMAIL, reqVendor.RequirementID, Convert.ToInt32(customer.UserID), ScreenName,
                                        sessionID).ConfigureAwait(false);

                bodySMS = String.Format(bodySMS, customer.FirstName, customer.LastName, reqVendor.RequirementID, reqVendor.Title, vendor.Institution);
                bodySMS = bodySMS.Replace("<br/>", "");
                #endregion TO CUSTOMER SUB USER

                #region TO CUSTOMER SUPER USER
                if (reqVendor.CustomerID != reqVendor.SuperUserID)
                {
                    ScreenName = "MARK_AS_COMPLETE_CUSTOMER";

                    User superCustomer = new User();
                    superCustomer = reqVendor.SuperUser;

                    subject = "REQUIREMENT IS MARKED AS COMPLETED.";
                    bodyEMAIL = GenerateEmailBody("MarkAsCompleteCustomerEMAIL");
                    bodySMS = GenerateEmailBody("MarkAsCompleteCustomerSMS");
                    bodyTELEGRAM = GenerateEmailBody("UniqeTemplateTELEGRAM");
                    actionTelegaram = "MARK AS COMPLETE";

                    bodyEMAIL = String.Format(bodyEMAIL, superCustomer.FirstName, superCustomer.LastName, reqVendor.RequirementID, reqVendor.Title, vendor.Institution);
                    SendEmail(superCustomer.Email + "," + superCustomer.AltEmail,
                                            "REQ-ID: " + reqID + " - " + subject,
                                            bodyEMAIL, reqVendor.RequirementID, Convert.ToInt32(superCustomer.UserID), ScreenName,
                                            sessionID).ConfigureAwait(false);

                    bodySMS = String.Format(bodySMS, superCustomer.FirstName, superCustomer.LastName, reqVendor.RequirementID, reqVendor.Title, vendor.Institution);
                    bodySMS = bodySMS.Replace("<br/>", "");;
                }
                #endregion TO CUSTOMER SUPER USER



            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }
            return response;
        }

        public Response CBStopQuotations(int stopCBQuotations, int userID, int reqID, string sessionID)
        {
            Response response = new Response();
            int isValid = ValidateSession(sessionID);
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_CB_STOP_QUOTATIONS", stopCBQuotations);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("fwd_CBStopQuotations", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {

                }

                CBUtility.CBStopQuotationsAlerts(userID, reqID, sessionID);

            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }
            return response;
        }

        public Response CBMarkAsComplete(int isCBCompleted, int userID, int reqID, string sessionID)
        {
            Response response = new Response();
            int isValid = ValidateSession(sessionID);
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_IS_CB_COMPLETED", isCBCompleted);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("fwd_CBMarkAsComplete", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {

                }

                CBUtility.CBMarkAsCompleteAlerts(isCBCompleted, userID, reqID, sessionID);
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }
            return response;
        }

        public Response UpdateCBTime(DateTime cbEndTime, int userID, int reqID, string sessionID)
        {
            Response response = new Response();
            int isValid = ValidateSession(sessionID);
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_CB_END_TIME", cbEndTime);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("fwd_UpdateCBTime", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {

                }

                CBUtility.UpdateCBTimeAlerts(cbEndTime, userID, reqID, sessionID);
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }
            return response;
        }

        public Response SaveDifferentialFactor(int reqID, int customerID, int vendorID, decimal value, string reason, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_U_ID", customerID);
                sd.Add("P_VENDOR_ID", vendorID);
                sd.Add("P_VALUE", value);
                sd.Add("P_REASON", reason);
                DataSet ds = sqlHelper.SelectList("fwd_SaveDifferentialFactor", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;


                }
                FwdRequirementHub rh = new FwdRequirementHub();
                string parties = reqID + "$" + customerID + "$" + sessionID;
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }
        public Response EnableMarginFields(int isEnabled, int reqID, string sessionID)
        {
            Response response = new Response();
            try
            {
                string query = string.Format("UPDATE fwd_requirementdetails SET IS_REV_UNIT_DISCOUNT_ENABLE = {0} WHERE REQ_ID = {1};", isEnabled, reqID);
                DataSet ds = sqlHelper.ExecuteQuery(query);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response UpdatePriceCap(int uID, int reqID, double price, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {
                int isValidSession = ValidateSession(sessionID);
                sd.Add("P_U_ID", uID);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_PRICE", price);
                sqlHelper.SelectList("fwd_UpdateSingleVendorPrice", sd);
                response.ObjectID = uID;
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response QuatationAprovel(int reqID, int customerID, int vendorID, bool value, string reason, string sessionID,
            string action, decimal technicalScore)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {
                string ScreenName = "";
                if (value == false || reason == "")
                {
                    ScreenName = "VENDOR_QUOTATION_APPROVED";
                }
                else if (value == true && reason != "")
                {
                    ScreenName = "VENDOR_QUOTATION_REJECTED";
                }
                if (value)
                {
                    reason = "Reject Comments-" + reason;
                }
                else
                {
                    reason = "Approved-" + reason;
                }

                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_U_ID", customerID);
                sd.Add("P_VENDOR_ID", vendorID);
                sd.Add("P_VALUE", value);
                sd.Add("P_REASON", reason);
                sd.Add("P_ACTION", action);
                sd.Add("P_TECHNICAL_SCORE", technicalScore);
                DataSet ds = sqlHelper.SelectList("fwd_QuatationAprovel", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    string query = $"UPDATE fwd_quotations SET IS_ITEM_QUOTATION_REJECTED = {(value ? 1 : 0)}, ITEM_QUOTATION_REJECTED_COMMENT = '{reason}', DATE_MODIFIED=UTC_TIMESTAMP, MODIFIED_BY={customerID} WHERE REQ_ID = {reqID} AND U_ID = {vendorID}";
                    sqlHelper.ExecuteQuery(query);

                    query = string.Format("INSERT INTO fwd_negotiationaudit " +
                    "(U_ID, REQ_ID, PRICE, BID_TIME, REJECT_RESON) " +
                    "SELECT U_ID, REQ_ID, REV_VEND_TOTAL_PRICE, UTC_TIMESTAMP, '{2}' " +
                    "FROM fwd_auctiondetails WHERE REQ_ID = {0} AND U_ID = {1};", reqID, vendorID, Regex.Replace(reason, "\'", ""));
                    sqlHelper.ExecuteNonQuery_IUD(query);

                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;

                    Requirement Requirement = GetRequirementDataOfflinePrivate(reqID, customerID, sessionID);
                    UserInfo Customer = GetUser(customerID);
                    UserInfo Vendor = GetUser(vendorID);

                    User altUser = GetReqAlternateCommunications(Requirement.RequirementID, Convert.ToInt32(Vendor.UserID));
                    Requirement.Module = ScreenName;
                    bool doSendEmail = ConfigurationManager.AppSettings["TENDER_VENDOR_QUOTATION_EMAIL"] != null ? Convert.ToBoolean(ConfigurationManager.AppSettings["TENDER_VENDOR_QUOTATION_EMAIL"]) : false;

                    if (doSendEmail)
                    {
                        if (!value && Requirement.IsDiscountQuotation != 2)
                        {
                            string body = GenerateEmailBody("VendoremailForQuotationAprovel");
                            body = String.Format(body, Vendor.FirstName, Vendor.LastName, Requirement.RequirementNumber, Requirement.Title, reason, Customer.FirstName, Customer.LastName);
                            SendEmail(Vendor.Email + "," + Vendor.AltEmail + "," + altUser.AltEmail, "Req Number: " + Requirement.RequirementNumber + " Title: " + Requirement.Title + " - Quotation Shortlisted", body, reqID, vendorID, Requirement.Module, sessionID).ConfigureAwait(false);

                            string body2 = GenerateEmailBody("VendorsmsForQuotationAprovel");
                            body2 = String.Format(body2, Vendor.FirstName, Vendor.LastName, Requirement.RequirementNumber, Requirement.Title, reason, Customer.FirstName, Customer.LastName);
                            body2 = body2.Replace("<br/>", "");
                            // SendSMS(string.Empty, Vendor.PhoneNum + "," + Vendor.AltPhoneNum + "," + altUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body2.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), reqID, vendorID, Requirement.Module, sessionID).ConfigureAwait(false);
                        }
                        else if (value && Requirement.IsDiscountQuotation != 2)
                        {
                            string body = GenerateEmailBody("VendoremailForQuotationRejection");
                            body = String.Format(body, Vendor.FirstName, Vendor.LastName, Requirement.RequirementNumber, Requirement.Title, reason, Customer.FirstName, Customer.LastName);
                            SendEmail(Vendor.Email + "," + Vendor.AltEmail + "," + altUser.AltEmail, "Req Number: " + Requirement.RequirementNumber + " Title: " + Requirement.Title + " - Quotation Rejected", body, reqID, vendorID, Requirement.Module, sessionID).ConfigureAwait(false);

                            string body2 = GenerateEmailBody("VendorsmsForQuotationRejection");
                            body2 = String.Format(body2, Vendor.FirstName, Vendor.LastName, reqID, Requirement.Title, reason, Customer.FirstName, Customer.LastName);
                            body2 = body2.Replace("<br/>", "");
                            //  SendSMS(string.Empty, Vendor.PhoneNum + "," + Vendor.AltPhoneNum + "," + altUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body2.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), reqID, vendorID, Requirement.Module, sessionID).ConfigureAwait(false);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response UploadRequirementItemsCeilingSave(int reqID, string sessionID, byte[] requirementItemsAttachment, int templateid)
        {
            Response response = new Response();
            string sheetName = string.Empty;
            if (requirementItemsAttachment != null)
            {
                DataTable currentData = new DataTable();
                using (MemoryStream ms = new MemoryStream())
                {
                    ms.Write(requirementItemsAttachment, 0, requirementItemsAttachment.Length);
                    using (ExcelPackage package = new ExcelPackage(ms))
                    {
                        currentData = Utilities.WorksheetToDataTable(package.Workbook.Worksheets[1]);
                        sheetName = package.Workbook.Worksheets[1].Name;
                    }
                }

                if (sheetName.Equals("CEILING_DETAILS", StringComparison.InvariantCultureIgnoreCase) && currentData.Rows.Count > 0)
                {
                    string query = "";
                    try
                    {
                        foreach (DataRow row in currentData.AsEnumerable())
                        {
                            int itemId = (row.IsNull("PRODUCT_ID") || row["PRODUCT_ID"] == DBNull.Value || string.IsNullOrEmpty(row["PRODUCT_ID"].ToString().Trim())) ? 0 : Convert.ToInt32(row["PRODUCT_ID"].ToString().Trim());
                            decimal ceilingPrice = (row.IsNull("CEILING_PRICE") || row["CEILING_PRICE"] == DBNull.Value || string.IsNullOrEmpty(row["CEILING_PRICE"].ToString().Trim())) ? 0 : Convert.ToDecimal(row["CEILING_PRICE"].ToString().Trim());
                            int userId = (row.IsNull("USER_ID") || row["USER_ID"] == DBNull.Value || string.IsNullOrEmpty(row["USER_ID"].ToString().Trim())) ? 0 : Convert.ToInt32(row["USER_ID"].ToString().Trim());
                            string userType = (row.IsNull("USER_TYPE") || row["USER_TYPE"] == DBNull.Value || string.IsNullOrEmpty(row["USER_TYPE"].ToString().Trim())) ? string.Empty : Convert.ToString(row["USER_TYPE"].ToString().Trim());
                            if (itemId > 0)
                            {
                                if (!string.IsNullOrEmpty(userType) && userType.Equals("CUSTOMER", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    query += $@"UPDATE fwd_requirementitems SET CEILING_PRICE = {ceilingPrice} WHERE REQ_ID = {reqID} AND ITEM_ID = {itemId};";
                                }

                                if (!string.IsNullOrEmpty(userType) && userType.Equals("VENDOR", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    query += $@"UPDATE fwd_quotations SET CEILING_PRICE = {ceilingPrice} WHERE REQ_ID = {reqID} AND ITEM_ID = {itemId} AND U_ID = {userId};";
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(query))
                        {
                            sqlHelper.ExecuteNonQuery_IUD(query);
                        }
                    }
                    catch (Exception ex)
                    {
                        response.ErrorMessage = ex.Message;
                    }
                }
            }

            return response;
        }

        public Response UploadQuotation(List<RequirementItems> quotationObject,
            int userID,
            int reqID,
            string sessionID,
            double price, // 5
            double tax,
            double freightcharges,
            double vendorBidPrice,
            string warranty,
            string payment, // 10
            string duration,
            string validity,
            int revised,
            string desFileName,
            double discountAmount, // 15
            List<RequirementTaxes> listRequirementTaxes,
            string otherProperties,
            string gstNumber,
            double packingCharges,
            double packingChargesTaxPercentage, // 20
            double packingChargesWithTax,
            double installationCharges,
            double installationChargesTaxPercentage,
            double installationChargesWithTax,
            double revpackingCharges, // 25
            double revinstallationCharges,
            double revpackingChargesWithTax,
            double revinstallationChargesWithTax,
            string selectedVendorCurrency,
            string uploadType, // 30
            List<FileUpload> multipleAttachments,
            double freightCharges,
            double freightChargesTaxPercentage,
            double freightChargesWithTax,
            double revfreightCharges,
            double revfreightChargesWithTax,
            string INCO_TERMS, int TaxFiledValidation,
            string isVendAckChecked,
            int surrogateId = 0, string SurrogateComments = "",
            string quotFreezeTime = "")
        {
            string moreInfo = string.Empty;
            string countAtt = string.Empty;
            int CallerID = userID;
            string ScreenName = "UPLOAD_QUOTATION";
            string RevScreenName = "UPLOAD_REV_QUOTATION";
            Response response = new Response();
            DataSet ds = new DataSet();
            int customerID = 0;
            moreInfo = "TESING1";
            DataSet ds1 = GetRequirementDetailsHub(reqID);
            moreInfo = "TESING2";
            Requirement requirement = GetRequirementDataFilter(customerID, sessionID, ds1);
            moreInfo = "TESING3";
            //Requirement requirement = GetRequirementData(reqID, customerID, sessionID);
            customerID = requirement.CustomerID;
            Response quotationFileObject = new Response();

            try
            {
                warranty = !string.IsNullOrEmpty(warranty) ? warranty.Replace("'", "") : warranty;
                validity = !string.IsNullOrEmpty(validity) ? validity.Replace("'", "") : validity;
                otherProperties = !string.IsNullOrEmpty(otherProperties) ? otherProperties.Replace("'", "") : otherProperties;
                duration = !string.IsNullOrEmpty(duration) ? duration.Replace("'", "") : duration;
                payment = !string.IsNullOrEmpty(payment) ? payment.Replace("'", "") : payment;

                string quotationItems = string.Empty;
                if (requirement.IsTabular && quotationObject.Where(v => v.ItemPrice < 0).ToList<RequirementItems>().Count > 0)
                {
                    response.ErrorMessage = "Please enter the price of all items before submitting.";
                    return response;
                }

                string multipleFileIDs = string.Empty;

                List<Attachment> CustomerListAttachment = new List<Attachment>();
                List<Attachment> VendorListAttachment = new List<Attachment>();
                List<Attachment> ListAttachment = new List<Attachment>();
                moreInfo = "TESING3.1";
                if (multipleAttachments != null && multipleAttachments.Count > 0)
                {


                    foreach (FileUpload fd in multipleAttachments)
                    {
                        //* ATTACHMENT FIX *//
                        fd.FileName = Regex.Replace(fd.FileName, @"[^0-9a-zA-Z.]+", "PRM_");
                        //* ATTACHMENT FIX *//
                        if (fd != null && fd.FileStream != null && fd.FileStream.Length > 0 && !string.IsNullOrEmpty(fd.FileName))
                        {

                            var attachName = string.Empty;

                            long tick = DateTime.UtcNow.Ticks;
                            attachName = System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "REQ_" + reqID + "_USER_" + userID + "_" + tick + "_" + fd.FileName);
                            SaveFile(attachName, fd.FileStream);
                            //SaveFileAsync(fileName, attachment);

                            attachName = "REQ_" + reqID + "_USER_" + userID + "_" + tick + "_" + fd.FileName;

                            Response res = SaveAttachment(attachName);
                            if (res.ErrorMessage != "")
                            {
                                response.ErrorMessage = res.ErrorMessage;
                            }

                            fd.FileID = res.ObjectID;

                            countAtt += "file ID---;" + fd.FileID;

                            if (fd.FileID > 0)
                            {
                                Attachment singleAttachment = null;
                                var fileData = DownloadFile(Convert.ToString(fd.FileID), sessionID);
                                if (fileData.FileStream != null && !string.IsNullOrEmpty(fileData.FileName))
                                {
                                    countAtt += "1---;";
                                    singleAttachment = new Attachment(fileData.FileStream, fileData.FileName);
                                }

                                CustomerListAttachment.Add(singleAttachment);
                                VendorListAttachment.Add(singleAttachment);
                                ListAttachment.Add(singleAttachment);
                            }

                        }
                        else if (fd != null && fd.FileID > 0)
                        {
                            Attachment singleAttachment = null;
                            var fileData = DownloadFile(Convert.ToString(fd.FileID), sessionID);
                            if (fileData.FileStream != null && !string.IsNullOrEmpty(fileData.FileName))
                            {
                                countAtt += "2---;";
                                singleAttachment = new Attachment(fileData.FileStream, fileData.FileName);
                            }

                            CustomerListAttachment.Add(singleAttachment);
                            VendorListAttachment.Add(singleAttachment);
                            ListAttachment.Add(singleAttachment);
                        }

                        multipleFileIDs += Convert.ToString(fd.FileID) + ",";

                    }
                    multipleFileIDs = multipleFileIDs.Substring(0, multipleFileIDs.Length - 1);
                }
                moreInfo = "TESING3.3";
                var isQuotLate = 0;
                if (revised == 0)
                {
                    moreInfo = "TESING3.3.1";
                    if (requirement.QuotationFreezTime.HasValue)
                    {
                        moreInfo = "TESING3.3.2";
                        moreInfo = "TESING3.3.3";
                        moreInfo = "TESING3.3.4";
                        DateTime currentDateTime = DateTime.UtcNow;
                        int result = DateTime.Compare(currentDateTime, requirement.QuotationFreezTime.Value);
                        isQuotLate = requirement.BiddingType == "SPOT" ? 0 : result;
                    }
                }
                else if (revised == 1)
                {
                    if (ds1 != null && ds1.Tables.Count > 0 && ds1.Tables[1].Rows.Count > 0)
                    {
                        moreInfo = "TESING3.6";
                        foreach (DataRow row1 in ds1.Tables[1].Rows)
                        {
                            VendorDetails vendor = new VendorDetails();
                            vendor.VendorID = row1["U_ID"] != DBNull.Value ? Convert.ToInt32(row1["U_ID"]) : 0;
                            vendor.IS_QUOT_LATE = row1["IS_QUOT_LATE"] != DBNull.Value ? Convert.ToInt32(row1["IS_QUOT_LATE"]) : 0;
                            if (CallerID == vendor.VendorID)
                            {
                                isQuotLate = vendor.IS_QUOT_LATE;
                            }
                        }
                    }
                }

                moreInfo = "TESING3.4";

                // if result = -1 (currentDateTime is earlier than freezeTime)
                // if result = 0 (currentDateTime is equal to freezeTime)
                // if result = 1 (currentDateTime id later than freezeTime)

                DataSet dataSet = sqlHelper.ExecuteQuery(string.Format("UPDATE fwd_auctiondetails SET MULTIPLE_ATTACHMENTS = '{2}' WHERE REQ_ID = {0} AND U_ID = {1};",
                    reqID, userID, multipleFileIDs));
                moreInfo = "TESING3.2";
                int loopcount = 0;
                string queryValues = string.Empty;
                int count = 0;
                string delete = string.Format("DELETE FROM fwd_tempquotations WHERE REQ_ID = {0} AND U_ID  = {1};", reqID, userID);
                string query = delete + "INSERT INTO fwd_tempquotations(U_ID, REQ_ID, ITEM_ID, PROD_ID, PROD_NO, DESCRIPTION, BRAND," +
                    " OTHER_BRANDS, VEND_TAXES, WARRANTY, PAYMENT, DURATION, VALIDITY, DISCOUNT, OTHER_PROPERTIES, UNIT_PRICE," +
                    "REV_UNIT_PRICE, PRICE, VEND_INIT_PRICE, VEND_FREIGHT, VEND_TOTAL_PRICE, IMAGE_ID, IS_REVISED," +
                    "C_GST, S_GST, I_GST, UNIT_MRP, UNIT_DISCOUNT, REV_UNIT_DISCOUNT, GST_NUMBER, VENDOR_UNITS, IS_REGRET, " +
                    "REGRET_COMMENTS,ITEM_LEVEL_INITIALCOMMENT,ITEM_LEVEL_REVCOMMENT," +
                    "ITEM_FREIGHT_CHARGES, ITEM_FREIGHT_TAX,PACKING_CHARGES,PACKING_CHARGES_TAX_PERCENTAGE,PACKING_CHARGES_WITH_TAX," +
                    "INSTALLATION_CHARGES,INSTALLATION_CHARGES_TAX_PERCENTAGE,INSTALLATION_CHARGES_WITH_TAX,REV_PACKING_CHARGES," +
                    "REV_INSTALLATION_CHARGES,SELECTED_VENDOR_CURRENCY,ITEM_REV_FREIGHT_CHARGES," +
                    "REV_INSTALLATION_CHARGES_WITH_TAX,REV_PACKING_CHARGES_WITH_TAX," +

                    "FREIGHT_CHARGES," +
                    "FREIGHT_CHARGES_TAX_PERCENTAGE," +
                    "FREIGHT_CHARGES_WITH_TAX," +
                    "REV_FREIGHT_CHARGES," +
                    "REV_FREIGHT_CHARGES_WITH_TAX, " +
                    "INCO_TERMS, " +
                    "ProductQuotationTemplateJson,IS_VEND_ACK_CHECKED,HSN_CODE,IS_QUOT_LATE " +
                    ") Values";

                moreInfo = "TESING2";
                foreach (RequirementItems quotation in quotationObject)
                {

                    string fileURL = string.Empty;
                    if (quotation.ItemAttachment != null)
                    {
                        long ticks = DateTime.UtcNow.Ticks;
                        fileURL = System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "user" + userID + "_" + ticks + quotation.AttachmentName);
                        Utilities.SaveFile(fileURL, quotation.ItemAttachment);
                        fileURL = "user" + userID + "_" + ticks + quotation.AttachmentName;
                        Response res = SaveAttachment(fileURL);
                        fileURL = res.ObjectID.ToString();
                        quotation.FileId = Convert.ToInt32(fileURL);
                    }

                    double itemPrice = 0;
                    double unitPrice = 0;

                    if (revised == 0)
                    {
                        itemPrice = quotation.ItemPrice;
                        unitPrice = quotation.UnitPrice;
                        quotation.ItemLevelRevComments = quotation.ItemLevelInitialComments;
                    }
                    else if (revised == 1)
                    {
                        itemPrice = quotation.RevItemPrice;
                        unitPrice = quotation.RevUnitPrice;
                    }
                    //To handle single quote issue, crap code, need to improve.
                    string columns = "({0},{1},{2},~$^{3}~$^,~$^{4}~$^,~$^{5}~$^,~$^{6}~$^," +
                        "~$^{7}~$^,{8},~$^{9}~$^,~$^{10}~$^,~$^{11}~$^,~$^{12}~$^,{13},~$^{14}~$^,{15}," +
                        "{16},{17},{18},{19},{20},{21},{22}," +
                        "{23},{24},{25},{26}," +
                        "{27},{28}," +
                        "~$^{29}~$^,~$^{30}~$^,{31},~$^{32}~$^,~$^{33}~$^,~$^{34}~$^,~$^{35}~$^,~$^{36}~$^,~$^{37}~$^,~$^{38}~$^,~$^{39}~$^,~$^{40}~$^,~$^{41}~$^,~$^{42}~$^," +
                        "~$^{43}~$^,~$^{44}~$^,~$^{45}~$^,~$^{46}~$^,~$^{47}~$^,~$^{48}~$^," +

                        "~$^{49}~$^," +
                        "~$^{50}~$^," +
                        "~$^{51}~$^," +
                        "~$^{52}~$^," +
                        "~$^{53}~$^," +
                        "~$^{54}~$^," +
                        "~$^{55}~$^," +
                        "~$^{56}~$^," +
                        "~$^{57}~$^,~$^{58}~$^),";

                    queryValues = queryValues + string.Format(columns,
                        userID, reqID, quotation.ItemID, quotation.ProductIDorName, quotation.ProductNo, quotation.ProductDescription, quotation.ProductBrand,
                        quotation.OthersBrands, tax, warranty, payment, duration, validity, discountAmount, otherProperties, quotation.UnitPrice,
                        quotation.RevUnitPrice, itemPrice, price, freightcharges, vendorBidPrice, quotation.FileId,
                        revised, quotation.CGst, quotation.SGst, quotation.IGst,
                        quotation.UnitMRP, quotation.UnitDiscount, quotation.RevUnitDiscount,
                        gstNumber, quotation.VendorUnits, (quotation.IsRegret ? 1 : 0), quotation.RegretComments.Replace("'", ""), quotation.ItemLevelInitialComments.Replace("'", ""),
                        quotation.ItemLevelRevComments.Replace("'", ""),
                        0, 0, packingCharges, packingChargesTaxPercentage, packingChargesWithTax,
                        installationCharges, installationChargesTaxPercentage, installationChargesWithTax, revpackingCharges, revinstallationCharges,
                        selectedVendorCurrency, 0, revinstallationChargesWithTax, revpackingChargesWithTax,

                        freightCharges,
                        freightChargesTaxPercentage,
                        freightChargesWithTax,
                        revfreightCharges,
                        revfreightChargesWithTax,
                        INCO_TERMS,
                        quotation.ProductQuotationTemplateJson, isVendAckChecked, quotation.HsnCode, isQuotLate);

                    count++;

                }

                response.ObjectID = count;
                if (!string.IsNullOrEmpty(queryValues))
                {
                    queryValues = queryValues.Replace(@"'", "\\'");
                    queryValues = queryValues.Replace("~$^", "'");
                    queryValues = queryValues.Substring(0, queryValues.Length - 1);
                    queryValues = queryValues + ";";
                    query = query + queryValues;

                    sqlHelper.ExecuteQuery(query);

                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_U_ID", userID);
                    sd.Add("P_REQ_ID", reqID);
                    sd.Add("P_SURROGATE_ID", surrogateId);
                    sd.Add("P_SURROGATE_COMMENTS", SurrogateComments);
                    ds = sqlHelper.SelectList("fwd_UploadQuotationBulk", sd);

                    sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_VENDOR_ID", userID);
                    sd.Add("P_REQ_ID", reqID);
                    ds = sqlHelper.SelectList("fwd_HandleRequirementVendorCurrency", sd);
                }


                bool isUOMDiffertent = false;
                int isRegret = 0;
                isRegret = quotationObject.Where(x => x.IsRegret == true).Count();

                foreach (RequirementItems quotation in quotationObject)
                {

                    double itemPrice = 0;
                    double unitPrice = 0;

                    if (revised == 0)
                    {
                        itemPrice = quotation.ItemPrice;
                        unitPrice = quotation.UnitPrice;
                    }
                    else if (revised == 1)
                    {
                        itemPrice = quotation.RevItemPrice;
                        unitPrice = quotation.RevUnitPrice;
                    }


                    loopcount = loopcount + 1;

                    if (loopcount == 1)
                    {
                        // SaveNegotiationAudit(reqID, userID, vendorBidPrice);
                    }


                    double itemUnitDiscount = 0;
                    double itemCostPrice = 0;

                    if (revised == 1)
                    {
                        //quotation.RevItemPrice = quotation.RevisedItemPrice;
                        response.ErrorMessage = "reviced";
                        itemUnitDiscount = quotation.RevUnitDiscount;

                    }
                    else if (revised == 0)
                    {
                        itemUnitDiscount = quotation.UnitDiscount;
                    }

                    itemCostPrice = (quotation.UnitMRP * 100 * 100) / ((itemUnitDiscount * 100) + 10000 + (itemUnitDiscount * (quotation.CGst + quotation.SGst + quotation.IGst)) + ((quotation.CGst + quotation.SGst + quotation.IGst) * 100));

                    if (requirement.IsTabular)
                    {

                        if (requirement.IsDiscountQuotation == 0)
                        {
                            double Gst = (quotation.CGst + quotation.SGst + quotation.IGst);

                            if (quotation.IsRegret == true)
                            {
                                itemPrice = 0;

                                unitPrice = 0;
                                quotation.CGst = 0;
                                quotation.SGst = 0;
                                quotation.IGst = 0;
                                Gst = 0;
                                quotation.UnitMRP = 0;
                                itemUnitDiscount = 0;
                            }



                            if (quotation.ProductQuantityIn != quotation.VendorUnits)
                            {
                                itemPrice = 0;
                                isUOMDiffertent = true;
                            }

                            //quotationObject



                            string xml = string.Empty;
                            xml = GenerateEmailBody("QuptationUploadXML");
                            if (isRegret > 0)
                            {
                                xml = GenerateEmailBody("QuptationUploadIsRegretXML");
                            }

                            string itemComments = string.Empty;

                            if (revised == 1)
                            {
                                itemComments = quotation.ItemLevelRevComments;
                            }
                            else if (revised == 0)
                            {
                                itemComments = quotation.ItemLevelInitialComments;
                            }

                            double freight = 0;
                            if (revised == 1)
                            {
                                freight = 0;//quotation.ItemRevFreightCharges + ((quotation.ItemRevFreightCharges / 100) * quotation.ItemFreightTAX);
                            }
                            else
                            {
                                freight = 0;//quotation.ItemFreightCharges + ((quotation.ItemFreightCharges / 100) * quotation.ItemFreightTAX);
                            }

                            xml = String.Format(xml, quotation.ProductIDorName, quotation.ProductNo, quotation.ProductDescription,
                                quotation.ProductQuantity, quotation.ProductBrand, itemPrice, unitPrice, quotation.CGst,
                                quotation.SGst, quotation.IGst, quotation.UnitMRP, itemUnitDiscount, Gst, quotation.VendorUnits, quotation.RegretComments,
                                quotation.ProductQuantityIn, itemComments, selectedVendorCurrency, revinstallationChargesWithTax,
                                revpackingChargesWithTax, freight,

                                revfreightChargesWithTax);
                            quotationItems += xml;

                        }
                        else if (requirement.IsDiscountQuotation == 1)
                        {
                            double freight = 0;
                            if (revised == 1)
                            {
                                freight = 0;//quotation.ItemRevFreightCharges + ((quotation.ItemRevFreightCharges / 100) * quotation.ItemFreightTAX);
                            }
                            else
                            {
                                freight = 0;//quotation.ItemFreightCharges + ((quotation.ItemFreightCharges / 100) * quotation.ItemFreightTAX);
                            }

                            double reductionPrice = 0;
                            if (revised == 1)
                            {
                                reductionPrice = (quotation.UnitMRP - quotation.RevUnitPrice) * quotation.ProductQuantity;
                            }
                            else
                            {
                                reductionPrice = (quotation.UnitMRP - quotation.UnitPrice) * quotation.ProductQuantity;
                            }

                            var gst = quotation.CGst + quotation.SGst + quotation.IGst;

                            string xml = string.Empty;
                            xml = GenerateEmailBody("DiscountQuptationUploadXML");

                            string itemComments = string.Empty;

                            if (revised == 1)
                            {
                                itemComments = quotation.ItemLevelRevComments;
                            }
                            else if (revised == 0)
                            {
                                itemComments = quotation.ItemLevelInitialComments;
                            }

                            xml = String.Format(xml, quotation.ProductIDorName, quotation.ProductNo, quotation.ProductDescription,
                                quotation.ProductQuantity, quotation.ProductBrand, itemPrice, unitPrice, gst,
                                quotation.SGst, quotation.IGst, quotation.UnitMRP, itemUnitDiscount,
                                Math.Round(reductionPrice, decimal_round), itemComments, selectedVendorCurrency, revinstallationChargesWithTax,
                                revpackingChargesWithTax, freight,

                                revfreightChargesWithTax);
                            quotationItems += xml;
                        }
                        else if (requirement.IsDiscountQuotation == 2)
                        {
                            int decimalPoint = 4;

                            string xml = string.Empty;
                            xml = GenerateEmailBody("MarginQuptationUploadXML");
                            xml = String.Format(xml, quotation.ProductIDorName, quotation.ProductNo, quotation.ProductBrand,
                                quotation.ProductQuantity,
                                Math.Round(itemCostPrice, decimalPoint),
                                quotation.CGst + quotation.SGst + quotation.IGst,
                                Math.Round(quotation.UnitMRP, decimalPoint),
                                Math.Round(itemCostPrice * (1 + (quotation.CGst + quotation.SGst + quotation.IGst) / 100), decimalPoint),
                                Math.Round(quotation.UnitMRP - (itemCostPrice * (1 + (quotation.CGst + quotation.SGst + quotation.IGst) / 100)), decimalPoint),
                                Math.Round(itemUnitDiscount, decimalPoint));
                            quotationItems += xml;
                        }
                    }
                    else
                    {
                        string xml1 = string.Empty;
                        xml1 = GenerateEmailBody("DecQuptationUploadXML");
                        xml1 = String.Format(xml1, requirement.Title, requirement.Description, vendorBidPrice);
                        quotationItems = xml1;
                    }

                }

                string quotationTaxes = string.Empty;
                string folderPath = HttpContext.Current.Server.MapPath(Utilities.FILE_URL);
                //Task.Factory.StartNew(() =>
                //{
                List<RequirementTerms> listRequirementTerms = new List<RequirementTerms>();
                string deliveryTermsDays = string.Empty;
                string deliveryTermsPercent = string.Empty;
                string paymentTermsDays = string.Empty;
                string paymentTermsPercent = string.Empty;
                string paymentTermsType = string.Empty;

                deliveryTermsDays = "<td>Days</td><td>N/A</td>";
                deliveryTermsPercent = "<td>Percent</td><td>N/A</td>";

                paymentTermsDays = "<td>Days</td><td>N/A</td>";
                paymentTermsPercent = "<td>Percent</td><td>N/A</td>";
                paymentTermsType = "<td>Type</td><td>N/A</td>";


                if (listRequirementTerms.Count > 0)
                {

                    deliveryTermsDays = "<td>Days</td>";
                    deliveryTermsPercent = "<td>Percent</td>";

                    paymentTermsDays = "<td>Days</td>";
                    paymentTermsPercent = "<td>Percent</td>";
                    paymentTermsType = "<td>Type</td>";


                    foreach (RequirementTerms reqterms in listRequirementTerms)
                    {

                        if (reqterms.ReqTermsType == "DELIVERY")
                        {
                            string xmlfordeliverytermsdays = string.Empty;
                            xmlfordeliverytermsdays = GenerateEmailBody("QuptationUploadDeliveryTermsDaysXML");
                            xmlfordeliverytermsdays = String.Format(xmlfordeliverytermsdays, reqterms.ReqTermsDays);
                            deliveryTermsDays += xmlfordeliverytermsdays;


                            string xmlfordeliverytermspercent = string.Empty;
                            xmlfordeliverytermspercent = GenerateEmailBody("QuptationUploadDeliveryTermsPercentXML");
                            xmlfordeliverytermspercent = String.Format(xmlfordeliverytermspercent, reqterms.ReqTermsPercent);
                            deliveryTermsPercent += xmlfordeliverytermspercent;

                        }

                        if (reqterms.ReqTermsType == "PAYMENT")
                        {

                            string type = string.Empty;

                            if (reqterms.ReqTermsDays == 0)
                            {
                                type = "OD";
                            }
                            else if (reqterms.ReqTermsDays > 0)
                            {
                                type = "P";
                            }
                            else if (reqterms.ReqTermsDays < 0)
                            {
                                reqterms.ReqTermsDays = -(reqterms.ReqTermsDays);

                                type = "A";
                            }

                            string xmlforpaymenttermsDays = string.Empty;
                            xmlforpaymenttermsDays = GenerateEmailBody("QuptationUploadPaymentTermsDaysXML");
                            xmlforpaymenttermsDays = String.Format(xmlforpaymenttermsDays, reqterms.ReqTermsDays);
                            paymentTermsDays += xmlforpaymenttermsDays;

                            string xmlforpaymenttermsPercent = string.Empty;
                            xmlforpaymenttermsPercent = GenerateEmailBody("QuptationUploadPaymentTermsPercentXML");
                            xmlforpaymenttermsPercent = String.Format(xmlforpaymenttermsPercent, reqterms.ReqTermsPercent);
                            paymentTermsPercent += xmlforpaymenttermsPercent;

                            string xmlforpaymenttermsType = string.Empty;
                            xmlforpaymenttermsType = GenerateEmailBody("QuptationUploadPaymentTermsTypeXML");
                            xmlforpaymenttermsType = String.Format(xmlforpaymenttermsType, type);
                            paymentTermsType += xmlforpaymenttermsType;
                        }

                    }
                }

                Attachment emilAttachmentQuotationPDF = null;

                if (string.IsNullOrEmpty(desFileName))
                {
                    long ticks1 = DateTime.UtcNow.Ticks;
                    string fileName = string.Empty;
                    int margin = 12;


                    if (isUOMDiffertent)
                    {
                        vendorBidPrice = 0;
                        price = 0;
                    }

                    string title = requirement.Title;

                    List<KeyValuePair<string, DataTable>> dtbl = new List<KeyValuePair<string, DataTable>>();

                    if (requirement.IsDiscountQuotation == 0)
                    {
                        var localGst = prmService.GetCompanyConfiguration(requirement.CustomerCompanyId, "GST_LOCAL_CODE", sessionID);
                        string localGstValue = (localGst != null && localGst.Count > 0) ? localGst[0].ConfigValue : string.Empty;
                        dtbl = FwdPdfUtilities.MakeDataTableQuotation(quotationObject, reqID, customerID, userID, sessionID,
                        warranty, payment, duration, validity, price, tax, freightcharges, vendorBidPrice, requirement.IsTabular,
                        revised, quotationTaxes, discountAmount, otherProperties, deliveryTermsDays, deliveryTermsPercent, paymentTermsDays,
                        paymentTermsPercent, paymentTermsType, folderPath, gstNumber, isRegret, selectedVendorCurrency,
                        packingCharges, packingChargesTaxPercentage, packingChargesWithTax, revpackingCharges, revpackingChargesWithTax,
                        installationCharges, installationChargesTaxPercentage, installationChargesWithTax, revinstallationCharges, revinstallationChargesWithTax,
                        freightCharges, freightChargesTaxPercentage, freightChargesWithTax, revfreightCharges, revfreightChargesWithTax,
                        INCO_TERMS, TaxFiledValidation, requirement.DeliveryLocation, requirement, localGstValue);

                    }
                    else if (requirement.IsDiscountQuotation == 1)
                    {

                        dtbl = FwdPdfUtilities.MakeDataTableDiscountQuotation(quotationObject, reqID, customerID, userID, sessionID,
                        warranty, payment, duration, validity, price, tax, freightcharges, vendorBidPrice, requirement.IsTabular,
                        revised, quotationTaxes, discountAmount, otherProperties, deliveryTermsDays, deliveryTermsPercent, paymentTermsDays,
                        paymentTermsPercent, paymentTermsType, folderPath, gstNumber, isRegret, selectedVendorCurrency,
                        packingCharges, packingChargesTaxPercentage, packingChargesWithTax, revpackingCharges, revpackingChargesWithTax,
                        installationCharges, installationChargesTaxPercentage, installationChargesWithTax, revinstallationCharges, revinstallationChargesWithTax,
                        freightCharges, freightChargesTaxPercentage, freightChargesWithTax, revfreightCharges, revfreightChargesWithTax,
                        INCO_TERMS, TaxFiledValidation);

                    }
                    else if (requirement.IsDiscountQuotation == 2)
                    {

                        dtbl = FwdPdfUtilities.MakeDataTableMarginQuotation(quotationObject, reqID, customerID, userID, sessionID,
                        warranty, payment, duration, validity, price, tax, freightcharges, vendorBidPrice, requirement.IsTabular,
                        revised, quotationTaxes, discountAmount, otherProperties, deliveryTermsDays, deliveryTermsPercent, paymentTermsDays,
                        paymentTermsPercent, paymentTermsType, folderPath, gstNumber, isRegret, packingCharges,
                        selectedVendorCurrency, revinstallationChargesWithTax, revpackingChargesWithTax, revfreightChargesWithTax,
                        INCO_TERMS, TaxFiledValidation);

                    }

                    FwdPdfUtilities.ExportDataTableQuotationToPdf(dtbl, @folderPath + "reqQuotation" + reqID + userID + "_" + customerID + "_" + ticks1 + ".pdf", response.ObjectID, customerID, userID, sessionID, duration, gstNumber, validity, revised, title, reqID, requirement, selectedVendorCurrency);

                    fileName = "reqQuotation" + reqID + userID + "_" + customerID + "_" + ticks1 + ".pdf";
                    quotationFileObject = SaveAttachment(fileName);
                    fileName = quotationFileObject.ObjectID.ToString();
                    string attachmentsave = SaveReqAttachments(reqID, userID, fileName, revised, "PDFQUOTATION");
                    if (uploadType != "BULK_EXCEL")
                    {
                        var fileData = DownloadFile(fileName, sessionID);
                        if (fileData.FileStream != null && !string.IsNullOrEmpty(fileData.FileName))
                        {
                            emilAttachmentQuotationPDF = new Attachment(fileData.FileStream, fileData.FileName);
                        }
                    }
                }
                if (uploadType != "BULK_EXCEL")
                {
                    if (!string.IsNullOrEmpty(desFileName))
                    {
                        string uploadattachmentsave = SaveReqAttachments(reqID, userID, desFileName, revised, "PDFQUOTATION");

                        var fileData = DownloadFile(desFileName, sessionID);
                        if (fileData.FileStream != null && !string.IsNullOrEmpty(fileData.FileName))
                        {
                            emilAttachmentQuotationPDF = new Attachment(fileData.FileStream, fileData.FileName);
                        }
                    }
                }

                // SignalR

                if (uploadType != "BULK_EXCEL")
                {
                    SignalRObj signalRObj = new SignalRObj();
                    List<object> payLoad = new List<object>();
                    //DataSet ds1 = GetRequirementDetailsHub(requirement.RequirementID);
                    Requirement req = GetRequirementDataFilter(requirement.CustomerID, sessionID, ds1);
                    req.LAST_BID_ID = userID;

                    if (req.AuctionVendors.Where(v => v.VendorID == userID).First() != null)
                    {
                        var currentVendor = req.AuctionVendors.Where(v => v.VendorID == userID).First();
                        if (currentVendor != null && string.IsNullOrEmpty(currentVendor.QuotationUrl))
                        {
                            foreach (var item in quotationObject)
                            {
                                if (item.IsCoreProductCategory > 0)
                                {
                                    item.IsItemQuotationRejected = -1;
                                }
                            }
                        }

                        var tempFactor = sqlHelper.ExecuteQuery($"select CURRENCY_FACTOR From fwd_auctiondetails where REQ_ID = {reqID} and U_ID = {userID}");
                        if (tempFactor != null && tempFactor.Tables.Count > 0 && tempFactor.Tables[0].Rows.Count > 0)
                        {
                            req.AuctionVendors.Where(v => v.VendorID == userID).First().VendorCurrencyFactor = tempFactor.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToDecimal(tempFactor.Tables[0].Rows[0][0]) : 1;
                        }

                        req.AuctionVendors.Where(v => v.VendorID == userID).First().SelectedVendorCurrency = selectedVendorCurrency;
                        req.AuctionVendors.Where(v => v.VendorID == userID).First().QuotationUrl = quotationFileObject.ObjectID.ToString();
                        if (revised == 1)
                        {
                            req.AuctionVendors.Where(v => v.VendorID == userID).First().RevQuotationUrl = quotationFileObject.ObjectID.ToString();
                        }
                        //req.AuctionVendors.Where(v => v.VendorID == userID).First().RevQuotationUrl = quotationFileObject.ObjectID.ToString();
                        req.AuctionVendors.Where(v => v.VendorID == userID).First().ListRequirementItems = quotationObject;
                        req.AuctionVendors.Where(v => v.VendorID == userID).First().RunningPrice = price;
                        req.AuctionVendors.Where(v => v.VendorID == userID).First().InitialPrice = price;
                    }

                    payLoad.Add(req);

                    InvolvedParties involvedParties = new InvolvedParties();
                    involvedParties.RequirementID = req.RequirementID;
                    involvedParties.CustomerID = req.CustomerID;
                    involvedParties.SuperUserID = req.SuperUserID;
                    involvedParties.CustCompID = req.CustCompID;
                    involvedParties.UserIDList = req.AuctionVendors.Select(x => x.VendorID).ToArray();
                    involvedParties.CallerID = CallerID;
                    var context = GlobalHost.ConnectionManager.GetHubContext<FwdRequirementHub>();
                    involvedParties.MethodName = "UploadQuotation";

                    if (revised == 1)
                    {
                        involvedParties.MethodName = "RevUploadQuotation";
                    }

                    req.Inv = involvedParties;
                    signalRObj.PayLoad = payLoad.ToArray();
                    signalRObj.Inv = involvedParties;
                    context.Clients.Group(Utilities.GroupName + req.RequirementID + "_CUSTOMER").checkRequirement(signalRObj);

                    if (requirement.BiddingType != "SPOT")
                    {
                        UserInfo vendor = Utilities.GetUserNew(userID);

                        User altUser = GetReqAlternateCommunications(reqID, userID);

                        string emailBody = string.Empty;
                        emailBody = GenerateEmailBody("QuotationUploadedVendoremail");
                        emailBody = String.Format(emailBody, vendor.FirstName, vendor.LastName, response.ObjectID, reqID);

                        if (revised == 0)
                        {
                            requirement.Module = ScreenName;
                        }
                        else if (revised == 1)
                        {
                            requirement.Module = RevScreenName;
                        }

                        SendEmail(vendor.Email + "," + vendor.AltEmail + "," + altUser.AltEmail, "Your Quotation has been successfully posted for Req Number: " + requirement.RequirementNumber + " Title: " + requirement.Title, emailBody, requirement.RequirementID, Convert.ToInt32(vendor.UserID), requirement.Module,
                            sessionID, emilAttachmentQuotationPDF, VendorListAttachment).ConfigureAwait(false);
                        string body2 = GenerateEmailBody("QuotationUploadedVendorsms");
                        body2 = String.Format(body2, vendor.FirstName, vendor.LastName, response.ObjectID, reqID);
                        body2 = body2.Replace("<br/>", "");

                        //if (!string.IsNullOrEmpty(uploadType) && uploadType != "BULK_EXCEL" && uploadType.Equals("BULK_MARGIN_VEND_QUOTATION", StringComparison.InvariantCultureIgnoreCase))
                        //{
                        //  SendSMS(string.Empty, vendor.PhoneNum + "," + vendor.AltPhoneNum + "," + altUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body2.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), requirement.RequirementID, Convert.ToInt32(vendor.UserID), requirement.Module, sessionID).ConfigureAwait(false);
                        //}
                        //Requirement requirement1 = GetRequirementDataOfflinePrivate(reqID, customerID, sessionID);
                        UserDetails customer = Utilities.GetUserDetails(customerID);

                        string bodySuper = GenerateEmailBody("QuotationUploadedemail");
                        bodySuper = String.Format(bodySuper, customer.FirstName, customer.LastName, vendor.FirstName, vendor.LastName, req.Title);

                        string body2Super = GenerateEmailBody("QuotationUploadedsms");
                        body2Super = String.Format(body2Super, customer.FirstName, customer.LastName, vendor.FirstName, vendor.LastName, req.Title);
                        body2Super = body2Super.Replace("<br/>", "");
                        if (requirement != null && !string.IsNullOrEmpty(requirement.BiddingType) && requirement.BiddingType.Equals("TENDER", StringComparison.InvariantCultureIgnoreCase))
                        {
                            SendEmail(customer.Email + "," + customer.AltEmail, "Req Number: " + requirement.RequirementNumber + " Title: " + requirement.Title + " - " + vendor.FirstName + " " + vendor.LastName + " has uploaded a quotation",
                            bodySuper, reqID, Convert.ToInt32(customer.UserID), requirement.Module, sessionID, null, VendorListAttachment).ConfigureAwait(false);
                        }
                        else
                        {
                            SendEmail(customer.Email + "," + customer.AltEmail, "Req Number: " + requirement.RequirementNumber + " Title: " + requirement.Title + " - " + vendor.FirstName + " " + vendor.LastName + " has uploaded a quotation",
                            bodySuper, reqID, Convert.ToInt32(customer.UserID), requirement.Module, sessionID, emilAttachmentQuotationPDF, VendorListAttachment).ConfigureAwait(false);
                        }

                        //  SendSMS(string.Empty, customer.PhoneNum + "," + customer.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body2Super.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), requirement.RequirementID, Convert.ToInt32(customer.UserID), requirement.Module, sessionID).ConfigureAwait(false);
                        string bodyTelegram = GenerateEmailBody("QuotationUploadTelegramsms");
                        bodyTelegram = String.Format(bodyTelegram, vendor.Institution, vendor.FirstName, vendor.LastName, requirement.Title, requirement.Description, price, tax, freightcharges, vendorBidPrice);
                        bodyTelegram = bodyTelegram.Replace("<br/>", "");

                        //TelegramMsg tgMsg = new TelegramMsg();
                        //tgMsg.Message = "QUOTATION UPLOADED FOR THE REQ ID:" + reqID + (bodyTelegram.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]);
                        //SendTelegramMsg(tgMsg);
                    }

                }
                else
                {
                    //string BULKbodyTelegram = GenerateEmailBody("BULKQuotationUploadTelegramsms");
                    //BULKbodyTelegram = String.Format(BULKbodyTelegram, reqID, userID);
                    //BULKbodyTelegram = BULKbodyTelegram.Replace("<br/>", "");

                    //TelegramMsg tgMsg = new TelegramMsg();
                    //tgMsg.Message = "QUOTATION UPLOADED FOR THE REQ ID:" + reqID + (BULKbodyTelegram.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]);
                    //SendTelegramMsg(tgMsg);
                }

                //});
            }
            catch (Exception ex)
            {
                logger.Error("ERROR UPLOAD" + moreInfo + "TESTING", ex);
                response.ErrorMessage = ex.Message + moreInfo;// + "------" +  ex.InnerException.Message;                
            }

            response.SessionID = sessionID;
            UserInfo user = Utilities.GetUserNew(userID);
            response.UserInfo = new UserInfo();
            response.UserInfo = user;

            return response;
        }

        public Response DeleteVendorFromAuction(int userID, int reqID, string sessionID)
        {
            string ScreenName = "DELETE_VENDOR";
            Response response = new Response();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                int isValidSession = ValidateSession(sessionID);
                sd.Add("P_U_ID", userID);
                sd.Add("P_REQ_ID", reqID);

                DataSet ds = sqlHelper.SelectList("fwd_RemoveVendorFromAuction", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    if (response.ObjectID > 0)
                    {
                        var requirement = GetRequirementDataOffline(reqID, userID, sessionID);
                        UserInfo user = GetUser(userID);
                        string body = GenerateEmailBody("VendoremailForRemoval");
                        body = String.Format(body, user.FirstName, user.LastName, reqID);
                        SendEmail(user.Email + "," + user.AltEmail, "Req Number: " + requirement.RequirementNumber + " - Please ignore the following requirement", body, reqID, Convert.ToInt32(user.UserID), "", sessionID).ConfigureAwait(false);                       
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response RevQuotationUpload(int reqID, int userID, double price, byte[] quotation, string quotationName, string sessionID, double tax)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            int CallerID = userID;
            string ScreenName = "MAKE_QUOTATION";
            string moreInfo = string.Empty;
            Response response = new Response();
            try
            {
                int isValidSession = ValidateSession(sessionID);
                string fileName = string.Empty;
                if (quotation != null)
                {
                    fileName = System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "req" + reqID + "_user" + userID + "_" + quotationName);
                    SaveFile(fileName, quotation);
                    fileName = "req" + reqID + "_user" + userID + "_" + quotationName;
                    Response res = SaveAttachment(fileName);
                    fileName = res.ObjectID.ToString();
                }

                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_PRICE", price);
                sd.Add("P_U_ID", userID);
                sd.Add("P_QUOTATION_URL", fileName);
                sd.Add("P_TAX", tax);

                DataSet ds = sqlHelper.SelectList("fwd_RevQuotationUpload", sd);
                moreInfo = "STORED PROCEDURE COMPLETED; ";
                UserInfo vendor = Utilities.GetUserNew(userID);

                User altUser = GetReqAlternateCommunications(reqID, userID);

                Requirement requirement = GetRequirementDataOfflinePrivate(reqID, userID, sessionID);

                if (fileName != string.Empty)
                {
                    string emailBody = string.Empty;
                    emailBody = GenerateEmailBody("QuotationUploadedVendoremail");
                    emailBody = String.Format(emailBody, vendor.FirstName, vendor.LastName, response.ObjectID, reqID);
                    SendEmail(vendor.Email + "," + vendor.AltEmail + "," + altUser.AltEmail, "ReqID: " + reqID + " Title: " + requirement.Title + " - Your Quotation has been successfully posted!", emailBody, reqID, Convert.ToInt32(vendor.UserID), requirement.Module, sessionID).ConfigureAwait(false);

                    string body2 = GenerateEmailBody("QuotationUploadedVendorsms");
                    body2 = String.Format(body2, vendor.FirstName, vendor.LastName, response.ObjectID, reqID);
                    body2 = body2.Replace("<br/>", "");
                    //  SendSMS(string.Empty, vendor.PhoneNum + "," + vendor.AltPhoneNum + "," + altUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body2.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), reqID, Convert.ToInt32(vendor.UserID), requirement.Module, sessionID).ConfigureAwait(false);

                }


                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    moreInfo = "STEP 2; ";
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                    if (response.ErrorMessage == string.Empty)
                    {
                        moreInfo = "STEP 3; ";
                        List<UserInfo> users = GetUsersInvolved(reqID, Utilities.GetEnumDesc<PRMRoles>(PRMRoles.CUSTOMER.ToString()), userID);
                        if (!string.IsNullOrEmpty(fileName))
                        {
                            string body = GenerateEmailBody("QuotationUploadedemail");
                            body = string.Format(body, users[0].FirstName, users[0].LastName, users[1].FirstName, users[1].LastName, reqID);
                            SendEmail(users[0].Email + "," + users[0].AltEmail, "Req Number: " + requirement.RequirementNumber + " Title: " + requirement.Title + " - " + users[1].FirstName + " " + users[1].LastName + " has uploaded a quotation", body, reqID, Convert.ToInt32(vendor.UserID), requirement.Module, sessionID).ConfigureAwait(false);
                            moreInfo = "STEP 4; ";
                            string body1 = GenerateEmailBody("QuotationUploadedsms");
                            moreInfo = "STEP 4.1; ";
                            body1 = String.Format(body1, users[0].FirstName, users[0].LastName, users[1].FirstName, users[1].LastName, reqID);
                            moreInfo = "STEP 4.2; ";
                            body1 = body1.Replace("<br/>", "");
                            moreInfo = "STEP 4.3; " + users[0].PhoneNum + " | " + body1;
                            // SendSMS(string.Empty, users[0].PhoneNum + "," + users[0].AltPhoneNum, System.Web.HttpUtility.UrlEncode(body1.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), reqID, Convert.ToInt32(vendor.UserID), requirement.Module, sessionID).ConfigureAwait(false);
                            
                            int custID = Int32.Parse(users[0].UserID);
                            Requirement req1 = GetRequirementDataOfflinePrivate(reqID, custID, sessionID);

                            string bodyTelegram = GenerateEmailBody("QuotationUploadTelegramsms");
                            bodyTelegram = String.Format(bodyTelegram, vendor.Institution, vendor.FirstName, vendor.LastName, req1.Title, req1.Description, price, tax, 0, 0);
                            bodyTelegram = bodyTelegram.Replace("<br/>", "");
                            moreInfo = "STEP 4.5; ";
                            moreInfo = "STEP 5; ";

                            moreInfo = "STEP 6; ";
                            UserInfo superUser = Utilities.GetSuperUser(custID);
                            moreInfo = "STEP 7; ";
                            string bodySuper = GenerateEmailBody("QuotationUploadedemail");
                            bodySuper = String.Format(bodySuper, superUser.FirstName, superUser.LastName, users[1].FirstName, users[1].LastName, reqID);

                            string body2Super = GenerateEmailBody("QuotationUploadedsms");
                            body2Super = String.Format(body2Super, superUser.FirstName, superUser.LastName, users[1].FirstName, users[1].LastName, reqID);
                            body2Super = body2Super.Replace("<br/>", "");

                            string subUserID = userID.ToString();


                            if (superUser.UserID != subUserID && users[0].PhoneNum != superUser.PhoneNum)
                            {
                                SendEmail(superUser.Email + "," + superUser.AltEmail, "Req Number: " + requirement.RequirementNumber + " Title: " + requirement.Title + " - " + users[1].FirstName + " " + users[1].LastName + " has uploaded a quotation", bodySuper, reqID, Convert.ToInt32(users[1].UserID), requirement.Module, sessionID).ConfigureAwait(false);

                            }
                        }


                        Requirement req = GetRequirementDataOfflinePrivate(reqID, userID, sessionID);
                        InvolvedParties involvedParties = new InvolvedParties();
                        involvedParties.RequirementID = req.RequirementID;
                        involvedParties.CustomerID = req.CustomerID;
                        involvedParties.SuperUserID = req.SuperUserID;
                        involvedParties.CallerID = CallerID;
                        involvedParties.UserIDList = req.AuctionVendors.Select(x => x.VendorID).ToArray();
                        involvedParties.MethodName = "RevisedQuotation";
                        involvedParties.CustCompID = req.CustCompID;
                        try
                        {
                            var context = GlobalHost.ConnectionManager.GetHubContext<FwdRequirementHub>();
                            //context.Clients.Group(Utilities.GroupName + req.RequirementID).checkRequirement(req);
                            context.Clients.Group(Utilities.GroupName + req.RequirementID + Utilities.UserIDComponent + req.CustomerID).checkRequirement(req);
                            context.Clients.Group(Utilities.GroupName + req.RequirementID + Utilities.UserIDComponent + req.SuperUserID).checkRequirement(req);
                            foreach (int vendorID in involvedParties.UserIDList)
                            {
                                req = GetRequirementData(reqID, vendorID, sessionID);
                                context.Clients.Group(Utilities.GroupName + req.RequirementID + Utilities.UserIDComponent + vendorID).checkRequirement(req);
                            }
                        }
                        catch (Exception ex)
                        {
                            fileName = System.Web.HttpContext.Current.Server.MapPath("/WEBQA/Exceptions.txt");
                            File.WriteAllText(fileName, ex.Message);
                            fileName = "req" + reqID + "_user" + userID + "_" + quotationName;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = moreInfo + ex.Message + " | " + ex.StackTrace;
            }

            return response;
        }

        public Response MakeABid(int reqID, int userID, double price, byte[] quotation, string quotationName, string sessionID,
            double tax, double freightcharges, string warranty, string payment, string duration, string validity,
            List<RequirementItems> quotationObject, int revised, double priceWithoutTax, string type, double discountAmount,
            List<RequirementTaxes> listRequirementTaxes, string otherProperties, bool ignorevalidations, double itemrevtotalprice, double vendorBidPrice,
            double revfreightCharges, double revpackingCharges, double revinstallationCharges, int surrogateId = 0,
            string surrogateComments = "")
        {
            string moreInfo = string.Empty;
            int CallerID = userID;

            Response response = new Response();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                ValidateSession(sessionID);
                long ticks = DateTime.UtcNow.Ticks;
                string fileName = string.Empty;

                if (type == "quotation")
                {
                }
                else
                {
                    sd.Add("P_REQ_ID", reqID);
                    sd.Add("P_PRICE", price);
                    sd.Add("P_U_ID", userID);
                    sd.Add("P_QUOTATION_URL", fileName);
                    sd.Add("P_TAX", tax);
                    sd.Add("P_VEND_FREIGHT", freightcharges);
                    sd.Add("P_WARRANTY", warranty);
                    sd.Add("P_PAYMENT", payment);
                    sd.Add("P_DURATION", duration);
                    sd.Add("P_VALIDITY", validity);
                    sd.Add("P_IGNORE_VALIDATIONS", ignorevalidations ? 1 : 0);
                    sd.Add("P_OTHER_PROPERTIES", otherProperties);
                    sd.Add("P_SURROGATE_ID", surrogateId);
                    sd.Add("P_SURROGATE_COMMENTS", surrogateComments);
                    DataSet ds = sqlHelper.SelectList("fwd_MakeBid", sd);
                    moreInfo = "STORED PROCEDURE COMPLETED; ";

                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                    {
                        response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                        response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;

                        if (response.ErrorMessage == "Auction is not running anymore.")
                        {
                            response.ErrorMessage = string.Empty;
                        }

                        if (response.ErrorMessage == string.Empty)
                        {
                            #region SaveRunningItemPrices

                            if (quotationObject.Count <= 20)
                            {
                                string queryValues = string.Empty;
                                int count = 0;
                                string delete = string.Format("DELETE FROM fwd_runningitempricetemp WHERE REQ_ID = {0} AND U_ID  = {1};", reqID, userID);
                                string query = delete + "INSERT INTO fwd_runningitempricetemp (ITEM_ID, REQ_ID, U_ID, REV_UNIT_PRICE, " +
                                    "REV_ITEM_PRICE, REVICED_PRICE, VEND_REVISED_TOTAL_PRICE, " +
                                    "VEND_FREIGHT, REV_UNIT_DISCOUNT,ITEM_REV_FREIGHT_CHARGES," +
                                    "REV_FREIGHT_CHARGES, REV_PACKING_CHARGES, REV_INSTALLATION_CHARGES, " +
                                    "ProductQuotationTemplateJson) Values";
                                foreach (RequirementItems item in quotationObject)
                                {
                                    double prevRevCostPrice = (item.UnitMRP * 100 * 100) / ((item.RevUnitDiscount * 100) + 10000 + (item.RevUnitDiscount * (item.CGst + item.SGst + item.IGst)) + ((item.CGst + item.SGst + item.IGst) * 100));
                                    prevRevCostPrice = Math.Round(prevRevCostPrice, decimal_round);
                                    //if (prevRevCostPrice > item.RevCostPrice)
                                    //{
                                    //    continue;
                                    //}


                                    queryValues = queryValues + string.Format("({0},{1},'{2}',{3},{4},{5},{6},{7},{8},{9}," +
                                        "{10},{11},{12}," +
                                        "'{13}'),",
                                        item.ItemID, reqID, userID, item.RevUnitPrice, item.RevItemPrice, price, vendorBidPrice, freightcharges, item.RevUnitDiscount,
                                        0,
                                        revfreightCharges, revpackingCharges, revinstallationCharges,
                                        item.ProductQuotationTemplateJson);
                                    count++;

                                }
                                response.ObjectID = count;
                                if (queryValues.Length > 0)
                                {
                                    queryValues = queryValues.Substring(0, queryValues.Length - 1);
                                    queryValues = queryValues + ";";
                                    query = query + queryValues;
                                    sqlHelper.ExecuteQuery(query);

                                    SortedDictionary<object, object> sd2 = new SortedDictionary<object, object>() { };
                                    sd2.Add("P_U_ID", userID);
                                    sd2.Add("P_REQ_ID", reqID);
                                    sqlHelper.SelectList("fwd_SaveRunningItemPriceBulk", sd2);
                                }
                            }

                            #endregion SaveRunningItemPrices
                            // SignalR

                            SignalRObj signalRObj = new SignalRObj();
                            List<object> payLoad = new List<object>();
                            DataSet ds1 = GetRequirementDetailsHub(reqID);

                            if (ds1 != null && ds1.Tables.Count > 0 && ds1.Tables[0].Rows.Count > 0)
                            {
                                DataRow row = ds1.Tables[0].Rows[0];
                                userID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                            }

                            var context = GlobalHost.ConnectionManager.GetHubContext<FwdRequirementHub>();
                            Requirement req = GetRequirementDataFilter(userID, sessionID, ds1);
                            payLoad.Add(req);

                            InvolvedParties involvedParties = new InvolvedParties();
                            involvedParties.RequirementID = req.RequirementID;
                            involvedParties.CustomerID = req.CustomerID;
                            involvedParties.SuperUserID = req.SuperUserID;
                            involvedParties.CallerID = CallerID;
                            involvedParties.CustCompID = req.CustCompID;
                            involvedParties.UserIDList = req.AuctionVendors.Select(x => x.VendorID).ToArray();
                            involvedParties.MethodName = "MakeBid";
                            req.Inv = involvedParties;
                            signalRObj.Inv = involvedParties;
                            signalRObj.PayLoad = payLoad.ToArray();
                            context.Clients.Group(Utilities.GroupName + req.RequirementID + "_CUSTOMER").checkRequirement(signalRObj);

                            signalRObj.Inv = involvedParties;

                            foreach (int vendorID in involvedParties.UserIDList)
                            {
                                payLoad = new List<object>();
                                req = GetRequirementDataFilter(vendorID, sessionID, ds1);
                                payLoad.Add(req);
                                signalRObj.PayLoad = payLoad.ToArray();
                                context.Clients.Group(Utilities.GroupName + req.RequirementID + "_" + vendorID).checkRequirement(signalRObj);
                            }

                            //dispose
                            ds1 = null;
                            req = null;
                            payLoad = null;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = moreInfo + ex.Message + " | " + ex.StackTrace;
            }


            return response;
        }

        public Response SaveReqNegSettings(int reqID, string sessionID, string negotiationDuration, NegotiationSettings NegotiationSettings)
        {

            string moreInfo = string.Empty;
            Response response = new Response();
            try
            {
                if (ConfigurationManager.AppSettings["DatabaseProvider"] == "MSSQL")
                {
                    NegotiationSettings.NegotiationDuration = NegotiationSettings.NegotiationDuration.Replace(".0", "").Replace("0 ", "").Trim();
                }

                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_MIN_REDUCE_AMOUNT", NegotiationSettings.MinReductionAmount);
                sd.Add("P_VENDOR_COMPARISION_MIN_AMOUNT", NegotiationSettings.RankComparision);
                sd.Add("P_NEGOTIATION_DURATION", TimeSpan.Parse(NegotiationSettings.NegotiationDuration).TotalSeconds);
                DataSet ds = sqlHelper.SelectList("fwd_SaveReqNegSettings", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = ds.Tables[0].Rows[0][1].ToString();
                }

            }
            catch (Exception ex)
            {
                response.ErrorMessage = moreInfo + ex.Message + ex.StackTrace;
            }

            return response;
        }

        public Response checkForRestartNegotiation(int reqID, int userID, string sessionID)
        {
            Response response = new Response();
            try
            {
                int isValidSession = ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("fwd_checkRestartNegotiation", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }

            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response UpadteExpDelAndPayDate(int reqid, DateTime date, string type, string sessionID)
        {
            Response response = new Response();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                int isValidSession = ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqid);
                sd.Add("P_DATE", date);
                sd.Add("P_TYPE", type);
                DataSet ds = sqlHelper.SelectList("fwd_UpadteExpDelAndPayDate", sd);
                Requirement req = new Requirement();
                req = GetRequirementDataOfflinePrivate(reqid, 0, sessionID);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;

                    int CustomerID = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][1].ToString()) : -1;
                    int VendorID = ds.Tables[0].Rows[0][2] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][2].ToString()) : -1;

                    if (response.ObjectID > 0)
                    {
                        if (type == "DELIVERY")
                        {
                            UserInfo Customer = GetUser(CustomerID);
                            UserInfo Vendor = GetUser(VendorID);

                            string body = GenerateEmailBody("UpdateExpDelDateCustomerEmail");
                            body = string.Format(GenerateEmailBody("UpdateExpDelDateCustomerEmail"), Customer.FirstName, Customer.LastName, reqid, date);
                            string subject = string.Format(GenerateEmailBody("UpdateExpDelDateCustomerSubj"), reqid);
                            string sms = string.Format(GenerateEmailBody("UpdateExpDelDateCustomerSMS"), Customer.FirstName, Customer.LastName, reqid, date);

                            SendEmail(Customer.Email + "," + Customer.AltEmail, subject, body, reqid, 0, "", sessionID).ConfigureAwait(false);
                            string body1 = GenerateEmailBody("UpdateExpDelDateVendorEmail");
                            body1 = string.Format(GenerateEmailBody("UpdateExpDelDateVendorEmail"), Vendor.FirstName, Vendor.LastName, reqid, date);
                            string subject1 = string.Format(GenerateEmailBody("UpdateExpDelDateVendorSubj"), reqid);
                            string sms1 = string.Format(GenerateEmailBody("UpdateExpDelDateVendorSMS"), Vendor.FirstName, Vendor.LastName, reqid, date);

                            SendEmail(Vendor.Email + "," + Vendor.AltEmail, subject1, body1, reqid, 0, "", sessionID).ConfigureAwait(false);
                           
                        }

                        if (type == "PAYMENT")
                        {
                            UserInfo Customer = GetUser(CustomerID);
                            UserInfo Vendor = GetUser(VendorID);

                            string body = GenerateEmailBody("UpdatePaymentDateCustomerEmail");
                            body = string.Format(GenerateEmailBody("UpdatePaymentDateCustomerEmail"), Customer.FirstName, Customer.LastName, reqid, date);
                            string subject = string.Format(GenerateEmailBody("UpdatePaymentDateCustomerSubj"), reqid);
                            string sms = string.Format(GenerateEmailBody("UpdatePaymentDateCustomerSMS"), Customer.FirstName, Customer.LastName, reqid, date);

                            SendEmail(Customer.Email + "," + Customer.AltEmail, subject, body, reqid, 0, "", sessionID).ConfigureAwait(false);
                            sms = sms.Replace("<br/>", "");
                            // SendSMS(string.Empty, Customer.PhoneNum + "," + Customer.AltPhoneNum, System.Web.HttpUtility.UrlEncode(sms.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), reqid, 0, "", sessionID).ConfigureAwait(false);

                            string body1 = GenerateEmailBody("UpdatePaymentDateVendorEmail");
                            body1 = string.Format(GenerateEmailBody("UpdatePaymentDateVendorEmail"), Vendor.FirstName, Vendor.LastName, reqid, date);
                            string subject1 = string.Format(GenerateEmailBody("UpdatePaymentDateVendorSubj"), reqid);
                            string sms1 = string.Format(GenerateEmailBody("UpdatePaymentDateVendorSMS"), Vendor.FirstName, Vendor.LastName, reqid, date);

                            SendEmail(Vendor.Email + "," + Vendor.AltEmail, subject1, body1, reqid, 0, "", sessionID).ConfigureAwait(false);
                            sms1 = sms1.Replace("<br/>", "");
                            //  SendSMS(string.Empty, Vendor.PhoneNum + "," + Vendor.AltPhoneNum, System.Web.HttpUtility.UrlEncode(sms1.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), reqid, 0, "", sessionID).ConfigureAwait(false);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response GetRevisedQuotations(int reqID, int userID, string sessionID)
        {
            Response response = new Response();
            try
            {
                Requirement requirement = GetRequirementData(reqID, userID, sessionID);
                Dictionary<int, Requirement> vendorsDictionary = new Dictionary<int, Requirement>();
                foreach (VendorDetails vd in requirement.AuctionVendors)
                {
                    Requirement vendorReq = GetRequirementData(reqID, vd.VendorID, sessionID);
                    vendorsDictionary.Add(vd.VendorID, vendorReq);
                }

                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    Requirement req = entry.Value;
                    double priceWithoutTax = 0;
                    double priceWithTax = 0;
                    double gst = 0;
                    double freight = 0;//req.AuctionVendors[0].RevVendorFreight;
                    var TaxFiledValidation = 0;
                    foreach (RequirementItems item in req.ListRequirementItems)
                    {
                        priceWithoutTax += item.RevItemPrice;
                        gst = item.CGst + item.IGst + item.SGst;
                        priceWithTax = priceWithoutTax * (1 + gst / 100);


                        if (item.CGst > 0 || item.SGst > 0)
                        {
                            TaxFiledValidation = 1;
                        }
                        else if (item.IGst > 0)
                        {
                            TaxFiledValidation = 0;
                        }
                    }
                    if (req.IsNegotiationEnded == 1 && req.AuctionVendors[0].TotalRunningPrice > 0 && req.AuctionVendors[0].CompanyName != "PRICE_CAP")
                    {
                        List<FileUpload> multipleAttachments = new List<FileUpload>();

                        if (req.AuctionVendors[0].MultipleAttachments.Length > 0)
                        {
                            int[] la = Array.ConvertAll(req.AuctionVendors[0].MultipleAttachments.Split(','), int.Parse);
                            foreach (int a in la)
                            {
                                FileUpload fileUpload = new FileUpload();
                                fileUpload.FileID = a;
                                multipleAttachments.Add(fileUpload);
                            }
                        }


                        Response res = UploadQuotation(req.ListRequirementItems, req.AuctionVendors[0].VendorID, reqID, sessionID, priceWithoutTax,
                            gst, freight, req.AuctionVendors[0].RevVendorTotalPrice, req.AuctionVendors[0].Warranty,
                            req.AuctionVendors[0].Payment, req.AuctionVendors[0].Duration, req.AuctionVendors[0].Validity, 1, "",
                            req.AuctionVendors[0].Discount, req.ListRequirementTaxes, req.AuctionVendors[0].OtherProperties,
                            req.AuctionVendors[0].GstNumber,
                            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                            req.AuctionVendors[0].SelectedVendorCurrency,
                            "BULK_EXCEL", multipleAttachments,
                            0, 0, 0, 0, 0,
                            req.AuctionVendors[0].INCO_TERMS, TaxFiledValidation, req.AuctionVendors[0].IsVendAckChecked, 0, "", "");

                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage += ex.Message;
            }

            return response;
        }

        public List<RequirementItems> UploadClientSideQuotation(int reqID, int userID, string sessionID, byte[] quotationAttachment)
        {
            int CallerID = userID;
            string sheetName = string.Empty;
            List<RequirementItems> vendorItems = new List<RequirementItems>();
            Requirement requirement = GetRequirementData(reqID, userID, sessionID);
            if (quotationAttachment != null)
            {
                DataTable currentData = new DataTable();
                using (MemoryStream ms = new MemoryStream())
                {
                    ms.Write(quotationAttachment, 0, quotationAttachment.Length);
                    using (ExcelPackage package = new ExcelPackage(ms))
                    {
                        currentData = Utilities.WorksheetToDataTable(package.Workbook.Worksheets[1]);
                        sheetName = package.Workbook.Worksheets[1].Name;
                    }
                }

                var mainSheetName = "";
                if (sheetName.Contains("UNIT_QUOTATION"))
                {
                    mainSheetName = "UNIT_QUOTATION_" + reqID;
                }
                if (sheetName.Contains("UNIT_BIDDING"))
                {
                    mainSheetName = "UNIT_BIDDING_" + reqID;
                }

                if (sheetName.Contains("UNIT_QUOTATION"))
                {
                    if (sheetName.Equals(mainSheetName, StringComparison.InvariantCultureIgnoreCase))
                    {
                        List<models.PRMTemplateFields> temlateFields = null;
                        PRMCustomFieldService fieldService = new PRMCustomFieldService();
                        PRMServices service = new PRMServices();
                        vendorItems = requirement.AuctionVendors.First(v => v.VendorID == userID).ListRequirementItems;
                        int vendorID = userID;
                        RequirementItems currentItem = new RequirementItems();
                        int totalRows = currentData.Rows.Count;
                        var reqSettings = service.GetRequirementSettings(reqID, sessionID);
                        if (reqSettings != null && reqSettings.Any(s => s.REQ_SETTING == "TEMPLATE_ID"))
                        {
                            temlateFields = fieldService.GetTemplateFieldsTemp(Convert.ToInt32(reqSettings.First(s => s.REQ_SETTING == "TEMPLATE_ID").REQ_SETTING_VALUE), string.Empty);
                        }

                        var qtyFieldName = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_QUANTITY")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_QUANTITY").FIELD_LABEL : "RFQ Qty";
                        var descFieldName = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_ITEMDETAILS")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_ITEMDETAILS").FIELD_LABEL : "Description";

                        foreach (DataRow row in currentData.Rows)
                        {
                            try
                            {
                                Response response2 = new Response();
                                string productId = (row.IsNull("PRODUCT_ID") || row["PRODUCT_ID"] == DBNull.Value || string.IsNullOrEmpty(row["PRODUCT_ID"].ToString().Trim())) ? string.Empty : Convert.ToString(row["PRODUCT_ID"].ToString().Trim());
                                if (string.IsNullOrEmpty(productId))
                                {
                                    continue;
                                }

                                double unitDiscount = 0;
                                double unitMRP = 0;
                                double unitPrice = 0;
                                if (requirement.IsDiscountQuotation == 1)
                                {
                                    unitMRP = (row.IsNull("UNIT_PRICE") || row["UNIT_PRICE"] == DBNull.Value || string.IsNullOrEmpty(row["UNIT_PRICE"].ToString().Trim())) ? 0 : Convert.ToDouble(row["UNIT_PRICE"].ToString().Trim());
                                    unitDiscount = (row.IsNull("UNIT_DISCOUNT") || row["UNIT_DISCOUNT"] == DBNull.Value || string.IsNullOrEmpty(row["UNIT_DISCOUNT"].ToString().Trim())) ? 0 : Convert.ToDouble(row["UNIT_DISCOUNT"].ToString().Trim());
                                }
                                else
                                {
                                    unitPrice = (row.IsNull("UNIT_PRICE") || row["UNIT_PRICE"] == DBNull.Value || string.IsNullOrEmpty(row["UNIT_PRICE"].ToString().Trim())) ? 0 : Convert.ToDouble(row["UNIT_PRICE"].ToString().Trim());
                                }
                                int ReqID = (row.IsNull("REQUIREMENT_ID") || row["REQUIREMENT_ID"] == DBNull.Value || string.IsNullOrEmpty(row["REQUIREMENT_ID"].ToString().Trim())) ? 0 : Convert.ToInt32(row["REQUIREMENT_ID"].ToString().Trim());
                                double tax = (row.IsNull("TAX") || row["TAX"] == DBNull.Value || string.IsNullOrEmpty(row["TAX"].ToString().Trim())) ? 0 : Convert.ToDouble(row["TAX"].ToString().Trim());
                                string description = (row.IsNull(descFieldName) || row[descFieldName] == DBNull.Value || string.IsNullOrEmpty(row[descFieldName].ToString().Trim())) ? string.Empty : Convert.ToString(row[descFieldName].ToString().Trim());
                                double quantity = 0;
                                if (row.Table.Columns.Contains(qtyFieldName))
                                {
                                    quantity = (row.IsNull(qtyFieldName) || row[qtyFieldName] == DBNull.Value || string.IsNullOrEmpty(row[qtyFieldName].ToString().Trim())) ? 0 : Convert.ToDouble(row[qtyFieldName].ToString().Trim());
                                }
                                else
                                {
                                    qtyFieldName = "Qty";
                                    quantity = (row.IsNull(qtyFieldName) || row[qtyFieldName] == DBNull.Value || string.IsNullOrEmpty(row[qtyFieldName].ToString().Trim())) ? 0 : Convert.ToDouble(row[qtyFieldName].ToString().Trim());
                                }
                                

                                double netPrice = (row.IsNull("TOTAL_PRICE") || row["TOTAL_PRICE"] == DBNull.Value || string.IsNullOrEmpty(row["TOTAL_PRICE"].ToString().Trim())) ? 0 : Convert.ToDouble(row["TOTAL_PRICE"].ToString().Trim());
                                string comments = (row.IsNull(descFieldName) || row[descFieldName] == DBNull.Value) ? string.Empty : Convert.ToString(row[descFieldName]).Trim();
                                string isRegret = (row.IsNull("Regret") || row["Regret"] == DBNull.Value) ? string.Empty : Convert.ToString(row["Regret"]);
                                string regretComments = (row.IsNull("Regret Comments") || row["Regret Comments"] == DBNull.Value) ? string.Empty : Convert.ToString(row["Regret Comments"]);

                                //if (unitPrice < 0)
                                //{
                                //    currentItem.ErrorMessage = "test";
                                //    break;
                                //}


                                int itemID = 0;
                                int subItemIdID = 0;
                                if (!string.IsNullOrEmpty(productId))
                                {
                                    if (productId.Contains("-"))
                                    {
                                        itemID = Convert.ToInt32(productId.Split('-')[0]);
                                        subItemIdID = Convert.ToInt32(productId.Split('-')[1]);
                                    }
                                    else
                                    {
                                        itemID = Convert.ToInt32(productId);
                                    }
                                }

                                currentItem = vendorItems.First(i => i.ItemID == itemID);

                                List<CATALOG.ProductQuotationTemplate> jsonToList = JsonConvert.DeserializeObject<List<CATALOG.ProductQuotationTemplate>>(currentItem.ProductQuotationTemplateJson);
                                if (subItemIdID > 0)
                                {
                                    var currentSubItem = jsonToList.First(j => j.T_ID == subItemIdID);
                                    if (currentSubItem.HAS_SPECIFICATION > 0)
                                    {
                                        //currentSubItem.DESCRIPTION = description;
                                        currentSubItem.SPECIFICATION = description;
                                    }
                                    if (currentSubItem.HAS_PRICE > 0)
                                    {
                                        if (requirement.IsDiscountQuotation == 1)
                                        {
                                            currentSubItem.BULK_PRICE = Convert.ToDecimal(unitMRP);
                                        }
                                        else
                                        {
                                            currentSubItem.BULK_PRICE = Convert.ToDecimal(unitPrice);
                                        }
                                    }
                                    if (currentSubItem.HAS_QUANTITY > 0)
                                    {
                                        currentSubItem.CONSUMPTION = Convert.ToDecimal(quantity);
                                    }
                                    if (currentSubItem.HAS_TAX > 0)
                                    {
                                        currentSubItem.TAX = Convert.ToDecimal(tax);
                                    }

                                    currentItem.ProductQuotationTemplateJson = JsonConvert.SerializeObject(jsonToList);
                                }
                                else
                                {
                                    //double sGst = tax / 2;// (row.IsNull("SGST") || row["SGST"] == DBNull.Value || string.IsNullOrEmpty(row["SGST"].ToString().Trim())) ? 0 : Convert.ToDouble(row["SGST"].ToString().Trim());
                                    //double cGst = tax / 2;//(row.IsNull("CGST") || row["CGST"] == DBNull.Value || string.IsNullOrEmpty(row["CGST"].ToString().Trim())) ? 0 : Convert.ToDouble(row["CGST"].ToString().Trim());

                                    if (requirement.IsDiscountQuotation == 1)
                                    {
                                        currentItem.UnitDiscount = unitDiscount;
                                        currentItem.RevUnitDiscount = unitDiscount;
                                        currentItem.UnitMRP = unitMRP;
                                    }
                                    else
                                    {
                                        currentItem.UnitPrice = unitPrice;
                                        currentItem.RevUnitPrice = unitPrice;
                                    }
                                    currentItem.CGst = 0;
                                    currentItem.SGst = 0;
                                    currentItem.IGst = tax;
                                    currentItem.ItemPrice = netPrice;
                                    currentItem.ProductDescription = description;
                                    currentItem.IsRegret = !string.IsNullOrEmpty(isRegret) && isRegret.Equals("TRUE", StringComparison.InvariantCultureIgnoreCase) ? true : false;
                                    currentItem.RegretComments = currentItem.IsRegret ? regretComments : string.Empty;
                                    currentItem.ItemLevelInitialComments = comments;
                                }
                            }
                            catch (Exception ex)
                            {
                                RequirementItems err = new RequirementItems();
                                err.ErrorMessage += ex.Message;
                            }
                        }
                    }
                    else
                    {
                        RequirementItems errorItem = new RequirementItems();
                        vendorItems.Add(errorItem);
                        vendorItems[0].ErrorMessage = "You are uploading different excel.Please upload correct excel" + " " + "'" + mainSheetName + "'";
                    }
                }

                if (sheetName.Contains("UNIT_BIDDING"))
                {
                    if (sheetName.Equals(mainSheetName, StringComparison.InvariantCultureIgnoreCase))
                    {
                        vendorItems = requirement.AuctionVendors.First(v => v.VendorID == userID).ListRequirementItems;
                        int vendorID = userID;
                        RequirementItems currentItem = new RequirementItems();
                        int totalRows = currentData.Rows.Count;
                        foreach (DataRow row in currentData.Rows)
                        {
                            try
                            {
                                Response response2 = new Response();
                                string productId = (row.IsNull("PRODUCT_ID") || row["PRODUCT_ID"] == DBNull.Value || string.IsNullOrEmpty(row["PRODUCT_ID"].ToString().Trim())) ? string.Empty : Convert.ToString(row["PRODUCT_ID"].ToString().Trim());
                                if (string.IsNullOrEmpty(productId))
                                {
                                    continue;
                                }

                                int ReqID = (row.IsNull("REQUIREMENT_ID") || row["REQUIREMENT_ID"] == DBNull.Value || string.IsNullOrEmpty(row["REQUIREMENT_ID"].ToString().Trim())) ? 0 : Convert.ToInt32(row["REQUIREMENT_ID"].ToString().Trim());
                                double revUnitDiscount = 0;
                                double revUnitPrice = 0;
                                if (requirement.IsDiscountQuotation == 1)
                                {
                                    revUnitDiscount = (row.IsNull("REV.UNIT_DISCOUNT") || row["REV.UNIT_DISCOUNT"] == DBNull.Value || string.IsNullOrEmpty(row["REV.UNIT_DISCOUNT"].ToString().Trim())) ? 0 : Convert.ToDouble(row["REV.UNIT_DISCOUNT"].ToString().Trim());
                                }
                                else
                                {
                                    revUnitPrice = (row.IsNull("REV.UNIT_PRICE") || row["REV.UNIT_PRICE"] == DBNull.Value || string.IsNullOrEmpty(row["REV.UNIT_PRICE"].ToString().Trim())) ? 0 : Convert.ToDouble(row["REV.UNIT_PRICE"].ToString().Trim());
                                }

                                int itemID = 0;
                                int subItemIdID = 0;
                                if (!string.IsNullOrEmpty(productId))
                                {
                                    if (productId.Contains("-"))
                                    {
                                        itemID = Convert.ToInt32(productId.Split('-')[0]);
                                        subItemIdID = Convert.ToInt32(productId.Split('-')[1]);
                                    }
                                    else
                                    {
                                        itemID = Convert.ToInt32(productId);
                                    }
                                }

                                currentItem = vendorItems.First(i => i.ItemID == itemID);
                                List<CATALOG.ProductQuotationTemplate> jsonToList = JsonConvert.DeserializeObject<List<CATALOG.ProductQuotationTemplate>>(currentItem.ProductQuotationTemplateJson);
                                if (subItemIdID > 0)
                                {
                                    var currentSubItem = jsonToList.First(j => j.T_ID == subItemIdID);
                                    if (currentSubItem.HAS_PRICE > 0)
                                    {
                                        currentSubItem.REV_BULK_PRICE = Convert.ToDecimal(revUnitPrice);
                                    }

                                    currentItem.ProductQuotationTemplateJson = JsonConvert.SerializeObject(jsonToList);
                                }
                                else
                                {
                                    if (revUnitPrice < currentItem.RevUnitPrice && requirement.IsDiscountQuotation == 0)
                                    {
                                        throw new Exception($"Item: '{currentItem.ProductIDorName}' revised unit price is less than previous item value.");
                                    }

                                    if (currentItem.RevUnitDiscount < revUnitDiscount && requirement.IsDiscountQuotation == 1)
                                    {
                                        throw new Exception($"Item: '{currentItem.ProductIDorName}' revised discount is greater than previous item value.");
                                    }

                                    if (requirement.IsDiscountQuotation == 1)
                                    {
                                        currentItem.RevUnitDiscount = revUnitDiscount;
                                    }
                                    else
                                    {
                                        currentItem.RevUnitPrice = revUnitPrice;
                                    }



                                }
                            }
                            catch (Exception ex)
                            {
                                RequirementItems err = new RequirementItems();
                                err.ErrorMessage += ex.Message;
                            }
                        }
                    }
                    else
                    {
                        RequirementItems errorItem = new RequirementItems();
                        vendorItems.Add(errorItem);
                        vendorItems[0].ErrorMessage = "You are uploading different excel.Please upload correct excel" + " " + "'" + mainSheetName + "'";
                    }
                }


            }

            return vendorItems;
        }

        public Response UploadQuotationsFromExcel(int reqID, int userID, string sessionID, byte[] quotationAttachment)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            int CallerID = userID;
            Response response = new Response();
            List<RequirementItems> items = new List<RequirementItems>();
            List<VendorDetails> listVendorProperties = new List<VendorDetails>();
            string sheetName = string.Empty;
            int customerID = 0;
            Requirement requirement = GetRequirementData(reqID, 0, sessionID);
            if (quotationAttachment != null)
            {
                DataTable currentData = new DataTable();
                using (MemoryStream ms = new MemoryStream())
                {
                    ms.Write(quotationAttachment, 0, quotationAttachment.Length);
                    using (ExcelPackage package = new ExcelPackage(ms))
                    {
                        currentData = Utilities.WorksheetToDataTable(package.Workbook.Worksheets[1]);
                        sheetName = package.Workbook.Worksheets[1].Name;
                    }
                }

                if (sheetName.Equals("QuotationDetails", StringComparison.InvariantCultureIgnoreCase))
                {
                    int count = 1;
                    foreach (DataRow row in currentData.Rows)
                    {
                        try
                        {
                            Response response2 = new Response();
                            RequirementItems item = new RequirementItems();
                            int ItemID = (row.IsNull("ItemID") || row["ItemID"] == DBNull.Value || string.IsNullOrEmpty(row["ItemID"].ToString().Trim())) ? 0 : Convert.ToInt32(row["ItemID"].ToString().Trim());
                            string ProductName = (row.IsNull("ProductName") || row["ProductName"] == DBNull.Value) ? string.Empty : Convert.ToString(row["ProductName"]).Trim();
                            string ProductNumber = (row.IsNull("ProductNumber") || row["ProductNumber"] == DBNull.Value) ? string.Empty : Convert.ToString(row["ProductNumber"]).Trim();
                            string Description = (row.IsNull("Description") || row["Description"] == DBNull.Value) ? string.Empty : Convert.ToString(row["Description"]).Trim();
                            double Quantity = (row.IsNull("Quantity") || row["Quantity"] == DBNull.Value || string.IsNullOrEmpty(row["Quantity"].ToString().Trim())) ? 0 : Convert.ToDouble(row["Quantity"].ToString().Trim());
                            string Units = (row.IsNull("Units") || row["Units"] == DBNull.Value) ? string.Empty : Convert.ToString(row["Units"]).Trim();
                            string Brand = (row.IsNull("Brand") || row["Brand"] == DBNull.Value) ? string.Empty : Convert.ToString(row["Brand"]).Trim();
                            double sGST = (row.IsNull("sGst") || row["sGst"] == DBNull.Value || string.IsNullOrEmpty(row["sGst"].ToString().Trim())) ? 0 : Convert.ToDouble(row["sGst"].ToString().Trim());
                            double cGST = (row.IsNull("cGst") || row["cGst"] == DBNull.Value || string.IsNullOrEmpty(row["cGst"].ToString().Trim())) ? 0 : Convert.ToDouble(row["cGst"].ToString().Trim());
                            double iGST = (row.IsNull("iGst") || row["iGst"] == DBNull.Value || string.IsNullOrEmpty(row["iGst"].ToString().Trim())) ? 0 : Convert.ToDouble(row["iGst"].ToString().Trim());

                            double UnitPrice = 0;
                            double Price = 0;
                            double UnitDiscount = 0;
                            double UnitMRP = 0;

                            if (requirement.IsDiscountQuotation == 0)
                            {
                                UnitPrice = (row.IsNull("UnitPrice") || row["UnitPrice"] == DBNull.Value || string.IsNullOrEmpty(row["UnitPrice"].ToString().Trim())) ? 0 : Convert.ToDouble(row["UnitPrice"].ToString().Trim());
                                Price = (UnitPrice + (UnitPrice * ((sGST + cGST + iGST) / 100))) * Quantity;
                            }
                            else if (requirement.IsDiscountQuotation == 1)
                            {
                                UnitMRP = (row.IsNull("UnitMRP") || row["UnitMRP"] == DBNull.Value || string.IsNullOrEmpty(row["UnitMRP"].ToString().Trim())) ? 0 : Convert.ToDouble(row["UnitMRP"].ToString().Trim());
                                UnitDiscount = (row.IsNull("UnitDiscount") || row["UnitDiscount"] == DBNull.Value || string.IsNullOrEmpty(row["UnitDiscount"].ToString().Trim())) ? 0 : Convert.ToDouble(row["UnitDiscount"].ToString().Trim());
                            }

                            if (ItemID > 0 && !string.IsNullOrEmpty(ProductName) && !string.IsNullOrEmpty(Units)
                                && Price >= 0 && UnitPrice >= 0 && Quantity > 0)
                            {
                                item.ItemID = ItemID;
                                item.ProductIDorName = ProductName;
                                item.ProductNo = ProductNumber;
                                item.ProductDescription = Description;
                                item.ProductQuantity = Convert.ToDouble(Quantity);
                                item.ProductQuantityIn = Units;
                                item.ProductBrand = Brand;
                                item.ItemPrice = Price;
                                item.UnitPrice = Convert.ToDouble(UnitPrice);
                                item.CGst = Convert.ToDouble(cGST);
                                item.SGst = Convert.ToDouble(sGST);
                                item.IGst = Convert.ToDouble(iGST);
                                item.UnitMRP = Convert.ToDouble(UnitMRP);
                                item.UnitDiscount = Convert.ToDouble(UnitDiscount);
                                if (Double.IsNaN(item.UnitDiscount))
                                {
                                    item.UnitDiscount = 0;
                                }
                                item.VendorUnits = Units;

                                items.Add(item);
                                count++;
                            }
                            else
                            {
                                response.ErrorMessage = "The values you entered in the excel are invalid. Please make sure all values in the field";
                                if (Price < 0)
                                {
                                    response.ErrorMessage += " 'Price' ";
                                }
                                else if (UnitPrice < 0)
                                {
                                    response.ErrorMessage += " 'UnitPrice' ";
                                }
                                else if (Quantity <= 0)
                                {
                                    response.ErrorMessage += " 'Quantity' ";
                                }
                                else
                                {
                                    response.ErrorMessage += "s ";
                                }
                                response.ErrorMessage += "are valid";
                                return response;
                            }
                        }
                        catch
                        {
                            response.ErrorMessage = "Row No. " + count + " has invalid values. Please check your excel sheet. ";
                            return response;
                        }
                    }
                }

                if (sheetName.Equals("MARGIN_QUOTATION", StringComparison.InvariantCultureIgnoreCase))
                {
                    int count = 1;

                    foreach (DataRow row in currentData.Rows)
                    {
                        try
                        {
                            Response response2 = new Response();
                            RequirementItems item = new RequirementItems();

                            int vendorID = (row.IsNull("VENDOR_ID") || row["VENDOR_ID"] == DBNull.Value || string.IsNullOrEmpty(row["VENDOR_ID"].ToString().Trim())) ? 0 : Convert.ToInt32(row["VENDOR_ID"].ToString().Trim());
                            int ItemID = (row.IsNull("ITEM_ID") || row["ITEM_ID"] == DBNull.Value || string.IsNullOrEmpty(row["ITEM_ID"].ToString().Trim())) ? 0 : Convert.ToInt32(row["ITEM_ID"].ToString().Trim());

                            if (ItemID > 0 && vendorID > 0)
                            {

                                VendorDetails vendorProperties = new VendorDetails();
                                string ProductName = (row.IsNull("PRODUCT_NAME") || row["PRODUCT_NAME"] == DBNull.Value) ? string.Empty : Convert.ToString(row["PRODUCT_NAME"]).Trim();
                                string Brand = (row.IsNull("BRAND") || row["BRAND"] == DBNull.Value) ? string.Empty : Convert.ToString(row["BRAND"]).Trim();
                                double Quantity = (row.IsNull("QUANTITY") || row["QUANTITY"] == DBNull.Value || string.IsNullOrEmpty(row["QUANTITY"].ToString().Trim())) ? 0 : Convert.ToDouble(row["QUANTITY"].ToString().Trim());
                                double costPrice = (row.IsNull("COST_PRICE") || row["COST_PRICE"] == DBNull.Value || string.IsNullOrEmpty(row["COST_PRICE"].ToString().Trim())) ? 0 : Convert.ToDouble(row["COST_PRICE"].ToString().Trim());
                                double gst = (row.IsNull("GST") || row["GST"] == DBNull.Value || string.IsNullOrEmpty(row["GST"].ToString().Trim())) ? 0 : Convert.ToDouble(row["GST"].ToString().Trim());
                                double sGST = gst / 2;
                                double cGST = gst / 2;
                                double iGST = 0;
                                double UnitMrp = (row.IsNull("MRP") || row["MRP"] == DBNull.Value || string.IsNullOrEmpty(row["MRP"].ToString().Trim())) ? 0 : Convert.ToDouble(row["MRP"].ToString().Trim());
                                double netPrice = (row.IsNull("NET_PRICE") || row["NET_PRICE"] == DBNull.Value || string.IsNullOrEmpty(row["NET_PRICE"].ToString().Trim())) ? 0 : Convert.ToDouble(row["NET_PRICE"].ToString().Trim());
                                double marginAmount = (row.IsNull("MARGIN_AMOUNT") || row["MARGIN_AMOUNT"] == DBNull.Value || string.IsNullOrEmpty(row["MARGIN_AMOUNT"].ToString().Trim())) ? 0 : Convert.ToDouble(row["MARGIN_AMOUNT"].ToString().Trim());
                                double UnitDiscount = (row.IsNull("MARGIN_PERC") || row["MARGIN_PERC"] == DBNull.Value || string.IsNullOrEmpty(row["MARGIN_PERC"].ToString().Trim())) ? 0 : Convert.ToDouble(row["MARGIN_PERC"].ToString().Trim());
                                string ProductNumber = (row.IsNull("HSN_CODE") || row["HSN_CODE"] == DBNull.Value) ? string.Empty : Convert.ToString(row["HSN_CODE"]).Trim();


                                string Description = string.Empty;
                                string Units = (row.IsNull("UNITS") || row["UNITS"] == DBNull.Value) ? string.Empty : Convert.ToString(row["UNITS"]).Trim();

                                string otherProperties = (row.IsNull("VENDOR_REMARK") || row["VENDOR_REMARK"] == DBNull.Value) ? string.Empty : Convert.ToString(row["VENDOR_REMARK"]).Trim();
                                string otherBrands = (row.IsNull("MANUFACTURER_NAME") || row["MANUFACTURER_NAME"] == DBNull.Value) ? string.Empty : Convert.ToString(row["MANUFACTURER_NAME"]).Trim();


                                vendorProperties.VendorID = vendorID;
                                vendorProperties.OtherProperties = otherProperties;


                                item.VendorID = vendorID;
                                item.ItemID = ItemID;
                                item.ProductIDorName = ProductName;
                                item.ProductBrand = Brand;
                                item.ProductQuantity = Convert.ToDouble(Quantity);
                                item.CostPrice = Convert.ToDouble(costPrice);
                                item.CGst = Convert.ToDouble(cGST);
                                item.SGst = Convert.ToDouble(sGST);
                                item.IGst = Convert.ToDouble(iGST);
                                item.UnitMRP = Convert.ToDouble(UnitMrp);
                                item.NetPrice = Convert.ToDouble(netPrice);
                                item.MarginAmount = Convert.ToDouble(marginAmount);
                                item.UnitDiscount = Convert.ToDouble(UnitDiscount);
                                if (Double.IsNaN(item.UnitDiscount))
                                {
                                    item.UnitDiscount = 0;
                                }

                                item.ProductNo = ProductNumber;
                                item.ProductDescription = Description;
                                item.ProductQuantityIn = Units;
                                item.OthersBrands = otherBrands;
                                item.VendorUnits = Units;


                                items.Add(item);
                                listVendorProperties.Add(vendorProperties);
                                count++;

                            }
                            else
                            {

                            }
                        }
                        catch
                        {
                            response.ErrorMessage = "Row No. " + count + " has invalid values. Please check your excel sheet. ";
                            return response;
                        }
                    }
                }

                if (sheetName.Equals("UNIT_PRICE_QUOTATION", StringComparison.InvariantCultureIgnoreCase))
                {
                    int count = 1;

                    foreach (DataRow row in currentData.Rows)
                    {
                        try
                        {
                            Response response2 = new Response();
                            RequirementItems item = new RequirementItems();

                            int vendorID = (row.IsNull("VENDOR_ID") || row["VENDOR_ID"] == DBNull.Value || string.IsNullOrEmpty(row["VENDOR_ID"].ToString().Trim())) ? 0 : Convert.ToInt32(row["VENDOR_ID"].ToString().Trim());
                            int ItemID = (row.IsNull("PRODUCT_ID") || row["PRODUCT_ID"] == DBNull.Value || string.IsNullOrEmpty(row["PRODUCT_ID"].ToString().Trim())) ? 0 : Convert.ToInt32(row["PRODUCT_ID"].ToString().Trim());
                            double unitPrice = (row.IsNull("PRICE") || row["PRICE"] == DBNull.Value || string.IsNullOrEmpty(row["PRICE"].ToString().Trim())) ? 0 : Convert.ToDouble(row["PRICE"].ToString().Trim());
                            int ReqID = (row.IsNull("REQUIREMENT_ID") || row["REQUIREMENT_ID"] == DBNull.Value || string.IsNullOrEmpty(row["REQUIREMENT_ID"].ToString().Trim())) ? 0 : Convert.ToInt32(row["REQUIREMENT_ID"].ToString().Trim());
                            if (ItemID > 0 && vendorID > 0 && unitPrice > 0)
                            {

                                VendorDetails vendorProperties = new VendorDetails();

                                string ProductName = (row.IsNull("PRODUCT_NAME") || row["PRODUCT_NAME"] == DBNull.Value) ? string.Empty : Convert.ToString(row["PRODUCT_NAME"]).Trim();
                                string Brand = (row.IsNull("BRAND") || row["BRAND"] == DBNull.Value) ? string.Empty : Convert.ToString(row["BRAND"]).Trim();
                                double Quantity = (row.IsNull("QUANTITY") || row["QUANTITY"] == DBNull.Value || string.IsNullOrEmpty(row["QUANTITY"].ToString().Trim())) ? 0 : Convert.ToDouble(row["QUANTITY"].ToString().Trim());

                                double gst = (row.IsNull("GST") || row["GST"] == DBNull.Value || string.IsNullOrEmpty(row["GST"].ToString().Trim())) ? 0 : Convert.ToDouble(row["GST"].ToString().Trim());
                                double sGST = gst / 2;
                                double cGST = gst / 2;
                                double iGST = 0;
                                double UnitMrp = (row.IsNull("FREIGHT") || row["FREIGHT"] == DBNull.Value || string.IsNullOrEmpty(row["FREIGHT"].ToString().Trim())) ? 0 : Convert.ToDouble(row["FREIGHT"].ToString().Trim());
                                double netPrice = (row.IsNull("TOTAL_PRICE") || row["TOTAL_PRICE"] == DBNull.Value || string.IsNullOrEmpty(row["TOTAL_PRICE"].ToString().Trim())) ? 0 : Convert.ToDouble(row["TOTAL_PRICE"].ToString().Trim());
                                //double marginAmount = (row.IsNull("MARGIN_AMOUNT") || row["MARGIN_AMOUNT"] == DBNull.Value || string.IsNullOrEmpty(row["MARGIN_AMOUNT"].ToString().Trim())) ? 0 : Convert.ToDouble(row["MARGIN_AMOUNT"].ToString().Trim());
                                //double UnitDiscount = (row.IsNull("MARGIN_PERC") || row["MARGIN_PERC"] == DBNull.Value || string.IsNullOrEmpty(row["MARGIN_PERC"].ToString().Trim())) ? 0 : Convert.ToDouble(row["MARGIN_PERC"].ToString().Trim());
                                string ProductNumber = (row.IsNull("PRODUCT_NO") || row["PRODUCT_NO"] == DBNull.Value) ? string.Empty : Convert.ToString(row["PRODUCT_NO"]).Trim();


                                string Description = (row.IsNull("DESCRIPTION") || row["DESCRIPTION"] == DBNull.Value) ? string.Empty : Convert.ToString(row["DESCRIPTION"]).Trim();
                                string Units = (row.IsNull("UNITS") || row["UNITS"] == DBNull.Value) ? string.Empty : Convert.ToString(row["UNITS"]).Trim();

                                //string otherProperties = (row.IsNull("VENDOR_REMARK") || row["VENDOR_REMARK"] == DBNull.Value) ? string.Empty : Convert.ToString(row["VENDOR_REMARK"]).Trim();

                                vendorProperties.VendorID = vendorID;
                                //vendorProperties.OtherProperties = otherProperties;


                                item.VendorID = vendorID;
                                item.ItemID = ItemID;
                                item.ProductIDorName = ProductName;
                                item.ProductBrand = Brand;
                                item.ProductQuantity = Convert.ToDouble(Quantity);
                                item.UnitPrice = Convert.ToDouble(unitPrice);
                                item.RevUnitPrice = Convert.ToDouble(unitPrice);
                                item.CGst = Convert.ToDouble(cGST);
                                item.SGst = Convert.ToDouble(sGST);
                                item.IGst = Convert.ToDouble(iGST);
                                //item.RevisedFreightcharges = Convert.ToDouble(UnitMrp);
                                item.ItemPrice = Convert.ToDouble(netPrice);

                                item.ProductNo = ProductNumber;
                                item.ProductDescription = Description;
                                item.ProductQuantityIn = Units;
                                item.RequirementID = ReqID;

                                item.VendorUnits = Units;

                                items.Add(item);
                                listVendorProperties.Add(vendorProperties);
                                count++;

                            }
                            else
                            {

                            }
                        }
                        catch
                        {
                            response.ErrorMessage = "Row No. " + count + " has invalid values. Please check your excel sheet. ";
                            return response;
                        }
                    }
                }

                if (sheetName.Equals("BULK_REQ_MARGIN_QUOTATION", StringComparison.InvariantCultureIgnoreCase) || sheetName.Equals("BULK_MARGIN_VEND_QUOTATION", StringComparison.InvariantCultureIgnoreCase))
                {
                    int count = 1;

                    foreach (DataRow row in currentData.Rows)
                    {
                        try
                        {
                            Response response2 = new Response();
                            RequirementItems item = new RequirementItems();
                            int vendorID = (row.IsNull("VENDOR_ID") || row["VENDOR_ID"] == DBNull.Value || string.IsNullOrEmpty(row["VENDOR_ID"].ToString().Trim())) ? 0 : Convert.ToInt32(row["VENDOR_ID"].ToString().Trim());
                            int ItemID = (row.IsNull("ITEM_ID") || row["ITEM_ID"] == DBNull.Value || string.IsNullOrEmpty(row["ITEM_ID"].ToString().Trim())) ? 0 : Convert.ToInt32(row["ITEM_ID"].ToString().Trim());
                            int ReqID = (row.IsNull("REQ_ID") || row["REQ_ID"] == DBNull.Value || string.IsNullOrEmpty(row["REQ_ID"].ToString().Trim())) ? 0 : Convert.ToInt32(row["REQ_ID"].ToString().Trim());

                            if (ItemID > 0 && vendorID > 0 && ReqID > 0)
                            {
                                string ProductName = (row.IsNull("PRODUCT_NAME") || row["PRODUCT_NAME"] == DBNull.Value) ? string.Empty : Convert.ToString(row["PRODUCT_NAME"]).Trim();
                                string Brand = (row.IsNull("BRAND") || row["BRAND"] == DBNull.Value) ? string.Empty : Convert.ToString(row["BRAND"]).Trim();
                                double Quantity = (row.IsNull("QUANTITY") || row["QUANTITY"] == DBNull.Value || string.IsNullOrEmpty(row["QUANTITY"].ToString().Trim())) ? 0 : Convert.ToDouble(row["QUANTITY"].ToString().Trim());
                                double Quantity_Orig = (row.IsNull("QUANTITY_ORIG") || row["QUANTITY_ORIG"] == DBNull.Value || string.IsNullOrEmpty(row["QUANTITY_ORIG"].ToString().Trim())) ? 0 : Convert.ToDouble(row["QUANTITY_ORIG"].ToString().Trim());
                                double costPrice = (row.IsNull("COST_PRICE") || row["COST_PRICE"] == DBNull.Value || string.IsNullOrEmpty(row["COST_PRICE"].ToString().Trim())) ? 0 : Convert.ToDouble(row["COST_PRICE"].ToString().Trim());
                                double gst = (row.IsNull("GST") || row["GST"] == DBNull.Value || string.IsNullOrEmpty(row["GST"].ToString().Trim())) ? 0 : Convert.ToDouble(row["GST"].ToString().Trim());
                                double sGST = gst / 2;
                                double cGST = gst / 2;
                                double iGST = 0;
                                double UnitMrp = (row.IsNull("MRP") || row["MRP"] == DBNull.Value || string.IsNullOrEmpty(row["MRP"].ToString().Trim())) ? 0 : Convert.ToDouble(row["MRP"].ToString().Trim());
                                double netPrice = (row.IsNull("NET_PRICE") || row["NET_PRICE"] == DBNull.Value || string.IsNullOrEmpty(row["NET_PRICE"].ToString().Trim())) ? 0 : Convert.ToDouble(row["NET_PRICE"].ToString().Trim());
                                double marginAmount = (row.IsNull("MARGIN_AMOUNT") || row["MARGIN_AMOUNT"] == DBNull.Value || string.IsNullOrEmpty(row["MARGIN_AMOUNT"].ToString().Trim())) ? 0 : Convert.ToDouble(row["MARGIN_AMOUNT"].ToString().Trim());
                                double UnitDiscount = (row.IsNull("MARGIN_PERC") || row["MARGIN_PERC"] == DBNull.Value || string.IsNullOrEmpty(row["MARGIN_PERC"].ToString().Trim())) ? 0 : Convert.ToDouble(row["MARGIN_PERC"].ToString().Trim());
                                string ProductNumber = (row.IsNull("HSN_CODE") || row["HSN_CODE"] == DBNull.Value) ? string.Empty : Convert.ToString(row["HSN_CODE"]).Trim();
                                string Description = string.Empty;
                                string Units = (row.IsNull("UNITS") || row["UNITS"] == DBNull.Value) ? string.Empty : Convert.ToString(row["UNITS"]).Trim();
                                string otherProperties = (row.IsNull("VENDOR_REMARK") || row["VENDOR_REMARK"] == DBNull.Value) ? string.Empty : Convert.ToString(row["VENDOR_REMARK"]).Trim();
                                string otherBrands = (row.IsNull("MANUFACTURER_NAME") || row["MANUFACTURER_NAME"] == DBNull.Value) ? string.Empty : Convert.ToString(row["MANUFACTURER_NAME"]).Trim();

                                item.VendorID = vendorID;
                                item.ItemID = ItemID;
                                item.ProductIDorName = ProductName;
                                item.ProductBrand = Brand;
                                item.ProductQuantity = Convert.ToDouble(Quantity);
                                if (sheetName.Equals("BULK_MARGIN_VEND_QUOTATION", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    item.ProductQuantity = Quantity_Orig;
                                }
                                item.CostPrice = Convert.ToDouble(costPrice);
                                item.CGst = Convert.ToDouble(cGST);
                                item.SGst = Convert.ToDouble(sGST);
                                item.IGst = Convert.ToDouble(iGST);
                                item.UnitMRP = Convert.ToDouble(UnitMrp);
                                item.NetPrice = Convert.ToDouble(costPrice * (1 + (cGST + sGST + iGST) / 100));
                                item.MarginAmount = Convert.ToDouble(UnitMrp - item.NetPrice);
                                item.UnitDiscount = Convert.ToDouble(item.MarginAmount * 100 / item.NetPrice);
                                if (Double.IsNaN(item.UnitDiscount))
                                {
                                    item.UnitDiscount = 0;
                                }
                                item.ProductNo = ProductNumber;
                                item.ProductDescription = Description;
                                item.ProductQuantityIn = Units;
                                item.VendorRemarks = otherProperties;
                                item.RequirementID = ReqID;
                                item.OthersBrands = otherBrands;

                                item.VendorUnits = Units;

                                items.Add(item);
                                count++;
                            }
                            else
                            {

                            }
                        }
                        catch
                        {
                            response.ErrorMessage = "Row No. " + count + " has invalid values. Please check your excel sheet. ";
                            return response;
                        }
                    }
                }

                if (sheetName.Equals("BULK_MARGIN_VEND_BIDDING", StringComparison.InvariantCultureIgnoreCase))
                {
                    int count = 1;

                    foreach (DataRow row in currentData.Rows)
                    {
                        try
                        {
                            Response response2 = new Response();
                            RequirementItems item = new RequirementItems();

                            int vendorID = (row.IsNull("VENDOR_ID") || row["VENDOR_ID"] == DBNull.Value || string.IsNullOrEmpty(row["VENDOR_ID"].ToString().Trim())) ? 0 : Convert.ToInt32(row["VENDOR_ID"].ToString().Trim());
                            int ItemID = (row.IsNull("ITEM_ID") || row["ITEM_ID"] == DBNull.Value || string.IsNullOrEmpty(row["ITEM_ID"].ToString().Trim())) ? 0 : Convert.ToInt32(row["ITEM_ID"].ToString().Trim());
                            int ReqID = (row.IsNull("REQ_ID") || row["REQ_ID"] == DBNull.Value || string.IsNullOrEmpty(row["REQ_ID"].ToString().Trim())) ? 0 : Convert.ToInt32(row["REQ_ID"].ToString().Trim());
                            string errMsg = string.Empty;
                            if (ItemID > 0 && vendorID > 0 && ReqID > 0)
                            {
                                string ProductName = (row.IsNull("PRODUCT_NAME") || row["PRODUCT_NAME"] == DBNull.Value) ? string.Empty : Convert.ToString(row["PRODUCT_NAME"]).Trim();
                                string Brand = (row.IsNull("BRAND") || row["BRAND"] == DBNull.Value) ? string.Empty : Convert.ToString(row["BRAND"]).Trim();
                                double Quantity = (row.IsNull("QUANTITY") || row["QUANTITY"] == DBNull.Value || string.IsNullOrEmpty(row["QUANTITY"].ToString().Trim())) ? 0 : Convert.ToDouble(row["QUANTITY"].ToString().Trim());
                                double costPrice = (row.IsNull("REV_COST_PRICE") || row["REV_COST_PRICE"] == DBNull.Value || string.IsNullOrEmpty(row["REV_COST_PRICE"].ToString().Trim())) ? 0 : Convert.ToDouble(row["REV_COST_PRICE"].ToString().Trim());
                                double costPriceOrig = (row.IsNull("REV_COST_PRICE_ORIG") || row["REV_COST_PRICE_ORIG"] == DBNull.Value || string.IsNullOrEmpty(row["REV_COST_PRICE_ORIG"].ToString().Trim())) ? 0 : Convert.ToDouble(row["REV_COST_PRICE_ORIG"].ToString().Trim());
                                double gst = (row.IsNull("GST") || row["GST"] == DBNull.Value || string.IsNullOrEmpty(row["GST"].ToString().Trim())) ? 0 : Convert.ToDouble(row["GST"].ToString().Trim());
                                double sGST = gst / 2;
                                double cGST = gst / 2;
                                double iGST = 0;
                                double UnitMrp = (row.IsNull("MRP") || row["MRP"] == DBNull.Value || string.IsNullOrEmpty(row["MRP"].ToString().Trim())) ? 0 : Convert.ToDouble(row["MRP"].ToString().Trim());
                                double netPrice = (row.IsNull("REV_NET_PRICE") || row["REV_NET_PRICE"] == DBNull.Value || string.IsNullOrEmpty(row["REV_NET_PRICE"].ToString().Trim())) ? 0 : Convert.ToDouble(row["REV_NET_PRICE"].ToString().Trim());
                                double marginAmount = (row.IsNull("REV_MARGIN_AMOUNT") || row["REV_MARGIN_AMOUNT"] == DBNull.Value || string.IsNullOrEmpty(row["REV_MARGIN_AMOUNT"].ToString().Trim())) ? 0 : Convert.ToDouble(row["REV_MARGIN_AMOUNT"].ToString().Trim());
                                double UnitDiscount = marginAmount / netPrice * 100;

                                //(row.IsNull("REV_MARGIN_PERC") || row["REV_MARGIN_PERC"] == DBNull.Value || string.IsNullOrEmpty(row["REV_MARGIN_PERC"].ToString().Trim())) ? 0 : Convert.ToDouble(row["REV_MARGIN_PERC"].ToString().Trim());

                                string ProductNumber = (row.IsNull("HSN_CODE") || row["HSN_CODE"] == DBNull.Value) ? string.Empty : Convert.ToString(row["HSN_CODE"]).Trim();
                                string Description = string.Empty;
                                string Units = (row.IsNull("UNITS") || row["UNITS"] == DBNull.Value) ? string.Empty : Convert.ToString(row["UNITS"]).Trim();
                                string otherProperties = (row.IsNull("VENDOR_REMARK") || row["VENDOR_REMARK"] == DBNull.Value) ? string.Empty : Convert.ToString(row["VENDOR_REMARK"]).Trim();

                                if (costPriceOrig < costPrice)
                                {
                                    costPrice = costPriceOrig;
                                }
                                item.VendorID = vendorID;
                                item.ItemID = ItemID;
                                item.ProductIDorName = ProductName;
                                item.ProductBrand = Brand;
                                item.ProductQuantity = Convert.ToDouble(Quantity);
                                item.CostPrice = Convert.ToDouble(costPrice);
                                item.CGst = Convert.ToDouble(cGST);
                                item.SGst = Convert.ToDouble(sGST);
                                item.IGst = Convert.ToDouble(iGST);
                                item.UnitMRP = Convert.ToDouble(UnitMrp);
                                item.NetPrice = Convert.ToDouble(costPrice * (1 + (cGST + sGST + iGST) / 100));
                                item.MarginAmount = Convert.ToDouble(UnitMrp - item.NetPrice);
                                item.UnitDiscount = Convert.ToDouble(item.MarginAmount / item.NetPrice * 100);
                                if (Double.IsNaN(item.UnitDiscount))
                                {
                                    item.UnitDiscount = 0;
                                }
                                item.ProductNo = ProductNumber;
                                item.ProductDescription = Description;
                                item.ProductQuantityIn = Units;
                                item.VendorRemarks = otherProperties;
                                item.RequirementID = ReqID;

                                item.VendorUnits = Units;

                                items.Add(item);
                                count++;
                            }
                            else
                            {

                            }
                        }
                        catch
                        {
                            response.ErrorMessage = "Row No. " + count + " has invalid values. Please check your excel sheet. ";
                            return response;
                        }
                    }
                }
            }

            string moreInfo = string.Empty;
            DataSet ds = new DataSet();
            customerID = requirement.CustomerID;
            try
            {
                if (sheetName.Equals("MARGIN_QUOTATION", StringComparison.InvariantCultureIgnoreCase))
                {
                    Requirement requirement1 = GetRequirementData(reqID, requirement.CustomerID, sessionID);
                    foreach (VendorDetails vd in requirement1.AuctionVendors)
                    {
                        List<RequirementItems> listReqItems = new List<RequirementItems>();
                        List<RequirementTaxes> listRequirementTaxes = new List<RequirementTaxes>();
                        listReqItems = items.Where(v => v.VendorID == vd.VendorID).ToList();
                        double priceWithoutTax = 0;
                        double price = 0;
                        foreach (RequirementItems item in listReqItems)
                        {
                            item.RevUnitDiscount = item.UnitDiscount;
                            priceWithoutTax += item.NetPrice * item.ProductQuantity;
                            price += item.NetPrice * item.ProductQuantity;
                        }

                        string vendorRemark = string.Empty;
                        foreach (VendorDetails properties in listVendorProperties)
                        {
                            if (vd.VendorID == properties.VendorID)
                            {
                                vendorRemark = properties.OtherProperties;
                            }
                        }

                        UploadQuotation(listReqItems, vd.VendorID, requirement.RequirementID, sessionID, priceWithoutTax,
                        0, 0, price, "", "", "", "",
                        0, "", 0, listRequirementTaxes, vendorRemark, "", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "", "BULK_EXCEL", null,
                        0, 0, 0, 0, 0, "", 0, "", 0, "", "");
                    }
                }

                else if (sheetName.Equals("BULK_REQ_MARGIN_QUOTATION", StringComparison.InvariantCultureIgnoreCase)
                    || sheetName.Equals("BULK_MARGIN_VEND_QUOTATION", StringComparison.InvariantCultureIgnoreCase)
                    || sheetName.Equals("BULK_MARGIN_VEND_BIDDING", StringComparison.InvariantCultureIgnoreCase))
                {
                    int[] vendors = items.Select(i => i.VendorID).Distinct<int>().ToArray();
                    foreach (int vendor in vendors)
                    {
                        List<RequirementItems> listReqItems = new List<RequirementItems>();
                        List<RequirementTaxes> listRequirementTaxes = new List<RequirementTaxes>();
                        listReqItems = items.Where(v => v.VendorID == vendor).ToList();
                        int[] requirements = listReqItems.Select(x => x.RequirementID).Distinct().ToArray();
                        string reqList = string.Empty;
                        reqList = string.Join(",", requirements);
                        if (requirements.Length > 0 && !string.IsNullOrEmpty(reqList) && (sheetName.Equals("BULK_REQ_MARGIN_QUOTATION") || sheetName.Equals("BULK_MARGIN_VEND_QUOTATION")))
                        {
                            string query = string.Format("SELECT DISTINCT(RD.REQ_ID), RD.REQ_TITLE FROM fwd_requirementdetails RD " +
                                " INNER JOIN fwd_auctiondetails AD ON RD.REQ_ID = AD.REQ_ID " +
                                " WHERE RD.REQ_ID IN ({0}) AND AD.U_ID IN ({1}) AND (RD.START_TIME > UTC_TIMESTAMP OR RD.START_TIME IS null) AND " +
                                " AD.IS_QUOTATION_REJECTED != 0" +
                                ";", reqList, vendor);

                            DataSet dsValidation = sqlHelper.ExecuteQuery(query);
                            if (dsValidation != null && dsValidation.Tables.Count > 0)
                            {
                                List<int> validReqs = new List<int>();
                                foreach (DataRow row in dsValidation.Tables[0].Rows)
                                {
                                    Response validReq = new Response();
                                    validReq.ObjectID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                                    validReq.Message = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                                    validReqs.Add(validReq.ObjectID);
                                }

                                requirements = validReqs.ToArray();
                            }
                        }

                        string reqIdString = string.Empty;
                        int reqIdCount = 0;
                        foreach (int reqIDFromList in requirements)
                        {
                            List<RequirementItems> reqItemsPerReq = listReqItems.Where(v => v.RequirementID == reqIDFromList).ToList();
                            double priceWithoutTax = 0;
                            double price = 0;
                            foreach (RequirementItems item in reqItemsPerReq)
                            {
                                item.RevUnitDiscount = item.UnitDiscount;
                                priceWithoutTax += item.NetPrice * item.ProductQuantity;
                                price += item.NetPrice * item.ProductQuantity;
                            }

                            string vendorRemark = string.Empty;
                            if (reqItemsPerReq.Count > 0)
                            {
                                vendorRemark = reqItemsPerReq[0].VendorRemarks;
                            }

                            int revised = 0;
                            if (sheetName.Equals("BULK_MARGIN_VEND_BIDDING", StringComparison.InvariantCultureIgnoreCase))
                            {
                                revised = 1;
                                Response res = SaveRunningItemPrice(reqItemsPerReq, vendor, reqIDFromList, priceWithoutTax, price, 0, 0, 0, 0);
                                reqIdString += reqIDFromList + ",";
                                reqIdCount++;
                                if (!string.IsNullOrEmpty(res.ErrorMessage))
                                {
                                    response.ErrorMessage += res.ErrorMessage;
                                }
                            }
                            else
                            {
                                string from = string.Empty;
                                if (!sheetName.Equals("BULK_MARGIN_VEND_QUOTATION", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    from = sheetName;
                                }

                                Response res = UploadQuotation(reqItemsPerReq, vendor, reqIDFromList, sessionID, priceWithoutTax,
                                0, 0, price, "", "", "", "",
                                revised, "", 0, listRequirementTaxes, vendorRemark, "", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "", "", null,
                                0, 0, 0, 0, 0, "", 0, "", 0, "", "");

                                if (!string.IsNullOrEmpty(res.ErrorMessage) && res.ErrorMessage.Trim() != "reviced")
                                {
                                    response.ErrorMessage += res.ErrorMessage;
                                }
                            }
                        }

                        if (sheetName.Equals("BULK_MARGIN_VEND_BIDDING", StringComparison.InvariantCultureIgnoreCase))
                        {

                            reqIdString = reqIdString.Substring(0, reqIdString.Length - 1);
                            sd.Add("P_U_ID", vendor);
                            sd.Add("P_REQ_ID_STR", reqIdString);
                            sd.Add("P_REQ_ID_COUNT", reqIdCount);
                            ds = sqlHelper.SelectList("fwd_CalculateRanksBulk", sd);
                            foreach (int reqIDFromList in requirements)
                            {
                                SignalRObj signalRObj = new SignalRObj();
                                List<object> payLoad = new List<object>();
                                DataSet ds1 = GetRequirementDetailsHub(reqIDFromList);
                                if (ds1 != null && ds1.Tables.Count > 0 && ds1.Tables[0].Rows.Count > 0)
                                {
                                    DataRow row = ds1.Tables[0].Rows[0];
                                    userID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                                }

                                Requirement req = GetRequirementDataFilter(userID, sessionID, ds1);
                                payLoad.Add(req);
                                InvolvedParties involvedParties = new InvolvedParties();
                                involvedParties.RequirementID = req.RequirementID;
                                involvedParties.CustomerID = req.CustomerID;
                                involvedParties.SuperUserID = req.SuperUserID;
                                involvedParties.CallerID = CallerID;
                                involvedParties.CustCompID = req.CustCompID;
                                involvedParties.UserIDList = req.AuctionVendors.Select(x => x.VendorID).ToArray();
                                involvedParties.MethodName = "MakeBid";
                                req.Inv = involvedParties;
                                signalRObj.Inv = involvedParties;
                                foreach (int vendorID in involvedParties.UserIDList)
                                {
                                    req = GetRequirementDataFilter(vendorID, sessionID, ds1);
                                    payLoad.Add(req);
                                }

                                signalRObj.Inv = involvedParties;
                                signalRObj.PayLoad = payLoad.ToArray();
                                try
                                {
                                    var context = GlobalHost.ConnectionManager.GetHubContext<FwdRequirementHub>();
                                    context.Clients.Group(Utilities.GroupName + req.RequirementID).checkRequirement(signalRObj);
                                }
                                catch (Exception ex)
                                {

                                }
                            }
                        }
                    }
                }

                else if (sheetName.Equals("UNIT_PRICE_QUOTATION", StringComparison.InvariantCultureIgnoreCase))
                {
                    Requirement requirement1 = GetRequirementData(reqID, requirement.CustomerID, sessionID);
                    foreach (VendorDetails vd in requirement1.AuctionVendors)
                    {
                        List<RequirementItems> listReqItems = new List<RequirementItems>();
                        List<RequirementTaxes> listRequirementTaxes = new List<RequirementTaxes>();
                        listReqItems = items.Where(v => v.VendorID == vd.VendorID && v.RequirementID == reqID).ToList();
                        if (listReqItems.Count > 0)
                        {
                            double priceWithoutTax = 0;
                            double price = 0;
                            double freight = 0;
                            foreach (RequirementItems item in listReqItems)
                            {
                                item.RevUnitDiscount = item.UnitDiscount;
                                priceWithoutTax += item.ItemPrice;
                                price += item.ItemPrice;
                                freight += 0;
                            }
                            price += freight;
                            priceWithoutTax += freight;
                            string vendorRemark = string.Empty;

                            foreach (VendorDetails properties in listVendorProperties)
                            {
                                if (vd.VendorID == properties.VendorID)
                                {
                                    vendorRemark = properties.OtherProperties;
                                }
                            }

                            UploadQuotation(listReqItems, vd.VendorID, requirement.RequirementID, sessionID, priceWithoutTax,
                            0, freight, price, "", "", "", "",
                            0, "", 0, listRequirementTaxes, "", "", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "", "BULK_EXCEL", null,
                            0, 0, 0, 0, 0, "", 0, "", 0, "", "");
                        }
                    }
                }
                else
                {
                    if (requirement.RequirementID > 0)
                    {
                        Requirement oldReq = GetRequirementData(reqID, userID, sessionID);
                    }

                    string quotationItems = string.Empty;
                    foreach (RequirementItems quotation in items)
                    {
                        sd = new SortedDictionary<object, object>() { };
                        sd.Add("P_U_ID", userID);
                        sd.Add("P_REQ_ID", reqID);
                        sd.Add("P_ITEM_ID", quotation.ItemID);
                        sd.Add("P_PROD_ID", quotation.ProductIDorName);
                        sd.Add("P_PROD_NO", quotation.ProductNo);
                        sd.Add("P_DESCRIPTION", quotation.ProductDescription);
                        sd.Add("P_BRAND", quotation.ProductBrand);
                        sd.Add("P_C_GST", quotation.CGst);
                        sd.Add("P_S_GST", quotation.SGst);
                        sd.Add("P_I_GST", quotation.IGst);

                        if (requirement.IsDiscountQuotation == 0)
                        {
                            sd.Add("P_UNIT_MRP", 0);
                            sd.Add("P_UNIT_DISCOUNT", 0);

                            sd.Add("P_UNIT_PRICE", quotation.UnitPrice);
                            sd.Add("P_PRICE", quotation.ItemPrice);
                        }
                        else if (requirement.IsDiscountQuotation == 1)
                        {
                            sd.Add("P_UNIT_MRP", quotation.UnitMRP);
                            sd.Add("P_UNIT_DISCOUNT", quotation.UnitDiscount);

                            var UnitPrice = quotation.UnitMRP - ((quotation.UnitMRP / 100) * quotation.UnitDiscount);
                            double Price = (UnitPrice + (UnitPrice * ((quotation.SGst + quotation.CGst + quotation.IGst) / 100))) * quotation.ProductQuantity;

                            sd.Add("P_UNIT_PRICE", UnitPrice);
                            sd.Add("P_PRICE", Price);
                        }

                        ds = sqlHelper.SelectList("fwd_UploadQuotationsFromExcel", sd);
                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                        {
                            response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                            response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                        }
                    }
                }

                string quotationTaxes = string.Empty;
                if (!sheetName.Equals("BULK_REQ_MARGIN_QUOTATION", StringComparison.InvariantCultureIgnoreCase))
                {
                    Requirement req = GetRequirementDataOfflinePrivate(requirement.RequirementID, requirement.CustomerID, requirement.SessionID);
                    InvolvedParties involvedParties = new InvolvedParties();
                    involvedParties.RequirementID = requirement.RequirementID;
                    involvedParties.CustomerID = requirement.CustomerID;
                    involvedParties.SuperUserID = requirement.SuperUserID;
                    involvedParties.UserIDList = requirement.AuctionVendors.Select(x => x.VendorID).ToArray();
                    involvedParties.CallerID = CallerID;
                    var context = GlobalHost.ConnectionManager.GetHubContext<FwdRequirementHub>();
                    involvedParties.MethodName = "UploadQuotation";
                    involvedParties.CustCompID = requirement.CustCompID;
                    req.Inv = involvedParties;
                    //context.Clients.Group(Utilities.GroupName + requirement.RequirementID).checkRequirement(req);
                    context.Clients.Group(Utilities.GroupName + req.RequirementID + Utilities.UserIDComponent + req.CustomerID).checkRequirement(req);
                    context.Clients.Group(Utilities.GroupName + req.RequirementID + Utilities.UserIDComponent + req.SuperUserID).checkRequirement(req);
                    foreach (int vendorID in involvedParties.UserIDList)
                    {
                        req = GetRequirementData(reqID, vendorID, sessionID);
                        context.Clients.Group(Utilities.GroupName + req.RequirementID + Utilities.UserIDComponent + vendorID).checkRequirement(req);
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            response.SessionID = sessionID;
            UserInfo user = Utilities.GetUserNew(userID);
            response.UserInfo = new UserInfo();
            response.UserInfo = user;

            return response;
        }

        public Response SaveRunningItemPrice(List<RequirementItems> itemsList, int userID, int reqID, double price,
            double vendorBidPrice, double freightcharges,
            double revfreightCharges, double revpackingCharges, double revinstallationCharges)
        {
            Response response = new Response();
            try
            {
                string queryValues = string.Empty;
                int count = 0;
                string delete = string.Format("DELETE FROM fwd_runningitempricetemp WHERE REQ_ID = {0} AND U_ID  = {1};", reqID, userID);
                string query = delete + "INSERT INTO fwd_runningitempricetemp(ITEM_ID, REQ_ID, U_ID, REV_UNIT_PRICE, " +
                    "REV_ITEM_PRICE, REVICED_PRICE, VEND_REVISED_TOTAL_PRICE, " +
                    "VEND_FREIGHT, REV_UNIT_DISCOUNT,ITEM_REV_FREIGHT_CHARGES," +
                    "REV_FREIGHT_CHARGES, REV_PACKING_CHARGES, REV_INSTALLATION_CHARGES, " +
                    "ProductQuotationTemplateJson) Values";

                foreach (RequirementItems item in itemsList)
                {
                    double prevRevCostPrice = (item.UnitMRP * 100 * 100) / ((item.RevUnitDiscount * 100) + 10000 + (item.RevUnitDiscount * (item.CGst + item.SGst + item.IGst)) + ((item.CGst + item.SGst + item.IGst) * 100));
                    prevRevCostPrice = Math.Round(prevRevCostPrice, decimal_round);
                    //if (prevRevCostPrice > item.RevCostPrice)
                    //{
                    //    continue;
                    //}


                    queryValues = queryValues + string.Format("({0},{1},'{2}',{3},{4},{5},{6},{7},{8},{9}," +
                        "{10},{11},{12}," +
                        "'{13}'),",
                        item.ItemID, reqID, userID, item.RevUnitPrice, item.RevItemPrice, price, vendorBidPrice, freightcharges, item.RevUnitDiscount,
                        0,
                        revfreightCharges, revpackingCharges, revinstallationCharges,
                        item.ProductQuotationTemplateJson);
                    count++;

                }
                response.ObjectID = count;
                if (queryValues.Length > 0)
                {
                    queryValues = queryValues.Substring(0, queryValues.Length - 1);
                    queryValues = queryValues + ";";
                    query = query + queryValues;
                    sqlHelper.ExecuteQuery(query);

                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_U_ID", userID);
                    sd.Add("P_REQ_ID", reqID);
                    sqlHelper.SelectList("fwd_SaveRunningItemPriceBulk", sd);
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response UpdateStatus(int reqid, int userid, string status, string type, string sessionID)
        {

            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {
                ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqid);
                sd.Add("P_STATUS", status);

                DataSet ds = sqlHelper.SelectList("fwd_UpdateStatus", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    int custID = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][1].ToString()) : -1;
                    int selcVendID = ds.Tables[0].Rows[0][2] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][2].ToString()) : -1;
                    if (response.ObjectID > 0)
                    {
                        Requirement req = GetRequirementDataOfflinePrivate(reqid, custID, sessionID);
                        RequirementPO reqPO = new RequirementPO();// GetPODetails(reqid, custID, req.SelectedVendorID, sessionID);

                        List<UserInfo> users = new List<UserInfo>();
                        if (type != "")
                        {
                            users = GetUsersInvolved(reqid, type);

                        }
                        foreach (UserInfo user in users)
                        {
                            string body = string.Empty;
                            string subject = string.Empty;
                            string sms = string.Empty;
                            if (user.UserType == Utilities.GetEnumDesc<PRMRoles>(PRMRoles.CUSTOMER.ToString()))
                            {
                                //status = status.ToUpper();
                                if (status == Utilities.GetEnumDesc<PRMStatus>(PRMStatus.POSent.ToString()))
                                {
                                    body = GenerateEmailBody("POGeneratedemail");
                                    body = string.Format(GenerateEmailBody("POGeneratedemail"), user.FirstName, user.LastName, reqid, reqPO.POID);
                                    subject = string.Format(GenerateEmailBody("POGeneratedSubj"), reqid, reqPO.POID);
                                    sms = string.Format(GenerateEmailBody("POGeneratedsms"), user.FirstName, user.LastName, reqid, reqPO.POID);



                                    UserInfo superUser = Utilities.GetSuperUser(custID);
                                    string subUserID = custID.ToString();

                                    string bodySuper = string.Format(GenerateEmailBody("POGeneratedemail"), superUser.FirstName, superUser.LastName, reqid, reqPO.POID);
                                    string subjectSuper = string.Format(GenerateEmailBody("POGeneratedSubj"), reqid, reqPO.POID);

                                    string smsSuper = string.Format(GenerateEmailBody("POGeneratedsms"), superUser.FirstName, superUser.LastName, reqid, reqPO.POID);



                                    if (superUser.UserID != subUserID)
                                    {
                                        SendEmail(superUser.Email + "," + superUser.AltEmail, subjectSuper, bodySuper, reqid, 0, req.Module, sessionID).ConfigureAwait(false);
                                        smsSuper = smsSuper.Replace("<br/>", "");
                                        // SendSMS(string.Empty, superUser.PhoneNum + "," + superUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(smsSuper.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), reqid, 0, req.Module, sessionID).ConfigureAwait(false);
                                    }

                                }
                                else if (status == Utilities.GetEnumDesc<PRMStatus>(PRMStatus.POAccepted.ToString()))
                                {
                                    body = GenerateEmailBody("POAcceptedemailCustomer");
                                    body = string.Format(GenerateEmailBody("POAcceptedemailCustomer"), user.FirstName, user.LastName, reqid, reqPO.POID, req.SelectedVendor.VendorName);
                                    subject = string.Format(GenerateEmailBody("POAcceptedCustSubj"), reqid);
                                    sms = string.Format(GenerateEmailBody("POAcceptedsmsCustomer"), user.FirstName, user.LastName, reqid, reqPO.POID, req.SelectedVendor.VendorName);


                                    UserInfo superUser = Utilities.GetSuperUser(custID);
                                    string subUserID = custID.ToString();

                                    string bodySuper = string.Format(GenerateEmailBody("POAcceptedemailCustomer"), superUser.FirstName, superUser.LastName, reqid, reqPO.POID, req.SelectedVendor.VendorName);
                                    string subjectSuper = string.Format(GenerateEmailBody("POAcceptedCustSubj"), reqid);

                                    string smsSuper = string.Format(GenerateEmailBody("POAcceptedsmsCustomer"), superUser.FirstName, superUser.LastName, reqid, reqPO.POID, req.SelectedVendor.VendorName);


                                    if (superUser.UserID != subUserID)
                                    {
                                        SendEmail(superUser.Email + "," + superUser.AltEmail, subjectSuper, bodySuper, reqid, 0, req.Module, sessionID).ConfigureAwait(false);
                                        smsSuper = smsSuper.Replace("<br/>", "");
                                        // SendSMS(string.Empty, superUser.PhoneNum + "," + superUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(smsSuper.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), reqid, 0, req.Module, sessionID).ConfigureAwait(false);
                                    }


                                }
                                else if (status == Utilities.GetEnumDesc<PRMStatus>(PRMStatus.MaterialDispatched.ToString()))
                                {
                                    body = GenerateEmailBody("MaterialDispatchedemailCustomer");
                                    body = string.Format(GenerateEmailBody("MaterialDispatchedemailCustomer"), user.FirstName, user.LastName, reqid, reqPO.POID, req.SelectedVendor.VendorName);
                                    subject = string.Format(GenerateEmailBody("MaterialDispatchedemailCustSubj"), reqid);
                                    sms = string.Format(GenerateEmailBody("MaterialDispatchedCustSMS"), user.FirstName, user.LastName, reqid, reqPO.POID, req.SelectedVendor.VendorName);


                                    UserInfo superUser = Utilities.GetSuperUser(custID);
                                    string subUserID = custID.ToString();

                                    string bodySuper = string.Format(GenerateEmailBody("MaterialDispatchedemailCustomer"), superUser.FirstName, superUser.LastName, reqid, reqPO.POID, req.SelectedVendor.VendorName);
                                    string subjectSuper = string.Format(GenerateEmailBody("MaterialDispatchedemailCustSubj"), reqid);

                                    string smsSuper = string.Format(GenerateEmailBody("MaterialDispatchedCustSMS"), superUser.FirstName, superUser.LastName, reqid, reqPO.POID, req.SelectedVendor.VendorName);


                                    if (superUser.UserID != subUserID)
                                    {
                                        SendEmail(superUser.Email + "," + superUser.AltEmail, subjectSuper, bodySuper, reqid, 0, req.Module, sessionID).ConfigureAwait(false);
                                        smsSuper = smsSuper.Replace("<br/>", "");
                                        //  SendSMS(string.Empty, superUser.PhoneNum + "," + superUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(smsSuper.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), reqid, 0, req.Module, sessionID).ConfigureAwait(false);
                                    }



                                }
                                else if (status == Utilities.GetEnumDesc<PRMStatus>(PRMStatus.MaterialReceived.ToString()))
                                {
                                    body = GenerateEmailBody("MaterialReceivedemailCustomer");
                                    body = string.Format(GenerateEmailBody("MaterialReceivedemailCustomer"), user.FirstName, user.LastName, reqid, reqPO.POID);
                                    subject = string.Format(GenerateEmailBody("MaterialReceivedCustSubj"), reqid);
                                    sms = string.Format(GenerateEmailBody("MaterialReceivedSMSCustomer"), user.FirstName, user.LastName, reqid, reqPO.POID);


                                    UserInfo superUser = Utilities.GetSuperUser(custID);
                                    string subUserID = custID.ToString();

                                    string bodySuper = string.Format(GenerateEmailBody("MaterialReceivedemailCustomer"), superUser.FirstName, superUser.LastName, reqid, reqPO.POID);
                                    string subjectSuper = string.Format(GenerateEmailBody("MaterialReceivedCustSubj"), reqid);

                                    string smsSuper = string.Format(GenerateEmailBody("MaterialReceivedSMSCustomer"), superUser.FirstName, superUser.LastName, reqid, reqPO.POID);


                                    if (superUser.UserID != subUserID)
                                    {
                                        SendEmail(superUser.Email + "," + superUser.AltEmail, subjectSuper, bodySuper, reqid, 0, req.Module, sessionID).ConfigureAwait(false);
                                        smsSuper = smsSuper.Replace("<br/>", "");
                                        //  SendSMS(string.Empty, superUser.PhoneNum + "," + superUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(smsSuper.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), reqid, 0, req.Module, sessionID).ConfigureAwait(false);
                                    }


                                }
                                else if (status == Utilities.GetEnumDesc<PRMStatus>(PRMStatus.PaymentPROCESSING.ToString()))
                                {
                                    body = GenerateEmailBody("PaymentProcessingemailCustomer");
                                    body = string.Format(GenerateEmailBody("PaymentProcessingemailCustomer"), user.FirstName, user.LastName, reqid, reqPO.POID);
                                    subject = string.Format(GenerateEmailBody("PaymentProcessingCustSubj"), reqid);
                                    sms = string.Format(GenerateEmailBody("PaymentProcessingSMSCustomer"), user.FirstName, user.LastName, reqid, reqPO.POID);


                                    UserInfo superUser = Utilities.GetSuperUser(custID);
                                    string subUserID = custID.ToString();

                                    string bodySuper = string.Format(GenerateEmailBody("PaymentProcessingemailCustomer"), superUser.FirstName, superUser.LastName, reqid, reqPO.POID);
                                    string subjectSuper = string.Format(GenerateEmailBody("PaymentProcessingCustSubj"), reqid);

                                    string smsSuper = string.Format(GenerateEmailBody("PaymentProcessingSMSCustomer"), superUser.FirstName, superUser.LastName, reqid, reqPO.POID);


                                    if (superUser.UserID != subUserID)
                                    {
                                        SendEmail(superUser.Email + "," + superUser.AltEmail, subjectSuper, bodySuper, reqid, 0, req.Module, sessionID).ConfigureAwait(false);
                                        smsSuper = smsSuper.Replace("<br/>", "");
                                        // SendSMS(string.Empty, superUser.PhoneNum + "," + superUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(smsSuper.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), reqid, 0, req.Module, sessionID).ConfigureAwait(false);
                                    }


                                }
                                else if (status == Utilities.GetEnumDesc<PRMStatus>(PRMStatus.PaymentReceived.ToString()))
                                {
                                    body = GenerateEmailBody("PaymentAcknowledgedemailCustomer");
                                    body = string.Format(GenerateEmailBody("PaymentAcknowledgedemailCustomer"), user.FirstName, user.LastName, reqid, reqPO.POID, req.SelectedVendor.VendorName);
                                    subject = string.Format(GenerateEmailBody("PaymentAcknowledgedCustSubj"), reqid);
                                    sms = string.Format(GenerateEmailBody("PaymentAcknowledgedSMS"), user.FirstName, user.LastName, reqid, reqPO.POID, req.SelectedVendor.VendorName);


                                    UserInfo superUser = Utilities.GetSuperUser(custID);
                                    string subUserID = custID.ToString();

                                    string bodySuper = string.Format(GenerateEmailBody("PaymentAcknowledgedemailCustomer"), superUser.FirstName, superUser.LastName, reqid, reqPO.POID, req.SelectedVendor.VendorName);
                                    string subjectSuper = string.Format(GenerateEmailBody("PaymentAcknowledgedCustSubj"), reqid);

                                    string smsSuper = string.Format(GenerateEmailBody("PaymentAcknowledgedSMS"), superUser.FirstName, superUser.LastName, reqid, reqPO.POID, req.SelectedVendor.VendorName);


                                    if (superUser.UserID != subUserID)
                                    {
                                        SendEmail(superUser.Email + "," + superUser.AltEmail, subjectSuper, bodySuper, reqid, 0, req.Module, sessionID).ConfigureAwait(false);
                                        smsSuper = smsSuper.Replace("<br/>", "");
                                        //  SendSMS(string.Empty, superUser.PhoneNum + "," + superUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(smsSuper.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), reqid, 0, req.Module, sessionID).ConfigureAwait(false);
                                    }


                                }

                                if (body != string.Empty && subject != string.Empty)
                                {
                                    SendEmail(user.Email + "," + user.AltEmail, subject, body, reqid, 0, req.Module, sessionID).ConfigureAwait(false);
                                    sms = sms.Replace("<br/>", "");
                                    // SendSMS(string.Empty, user.PhoneNum + "," + user.AltPhoneNum, System.Web.HttpUtility.UrlEncode(sms.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), reqid, 0, req.Module, sessionID).ConfigureAwait(false);
                                }


                            }
                            else if (user.UserType == Utilities.GetEnumDesc<PRMRoles>(PRMRoles.VENDOR.ToString()))
                            {



                                UserInfo vendor = Utilities.GetUserNew(selcVendID);

                                if (status == Utilities.GetEnumDesc<PRMStatus>(PRMStatus.POSent.ToString()))
                                {
                                    Requirement reqTrue = GetRequirementData(reqid, custID, sessionID);
                                    if (reqTrue.IsTabular)
                                    {
                                        var index = 1;
                                        foreach (RequirementItems item in reqTrue.ListRequirementItems)
                                        {
                                            vendor = Utilities.GetUserNew(item.SelectedVendorID);
                                            if (vendor.UserID == item.SelectedVendorID.ToString())
                                            {

                                                //reqPO = GetPODetails(reqid, custID, item.SelectedVendorID, sessionID);
                                                body = GenerateEmailBody("POSentemailVendor");
                                                body = string.Format(GenerateEmailBody("POSentemailVendor"), vendor.FirstName, vendor.LastName, reqid, (reqPO.POID + "/" + index));
                                                subject = string.Format(GenerateEmailBody("POSentVendSubj"), reqid);
                                                sms = string.Format(GenerateEmailBody("POSentsmsVendor"), vendor.FirstName, vendor.LastName, reqid, (reqPO.POID + "/" + index));

                                                SendEmail(vendor.Email + "," + vendor.AltEmail, subject, body, reqid, item.SelectedVendorID, req.Module, sessionID).ConfigureAwait(false);

                                                sms = sms.Replace("<br/>", "");
                                                //  SendSMS(string.Empty, vendor.PhoneNum + "," + vendor.AltPhoneNum, System.Web.HttpUtility.UrlEncode(sms.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), reqid, item.SelectedVendorID, req.Module, sessionID).ConfigureAwait(false);
                                                index++;

                                            }


                                        }
                                    }
                                    else
                                    {
                                        body = GenerateEmailBody("POSentemailVendor");
                                        body = string.Format(GenerateEmailBody("POSentemailVendor"), vendor.FirstName, vendor.LastName, reqid, reqPO.POID);
                                        subject = string.Format(GenerateEmailBody("POSentVendSubj"), reqid);
                                        sms = string.Format(GenerateEmailBody("POSentsmsVendor"), vendor.FirstName, vendor.LastName, reqid, reqPO.POID);
                                    }

                                }
                                else if (status == Utilities.GetEnumDesc<PRMStatus>(PRMStatus.POAccepted.ToString()))
                                {
                                    body = GenerateEmailBody("POSentemailVendor");
                                    body = string.Format(GenerateEmailBody("POAcceptedemailVendor"), vendor.FirstName, vendor.LastName, reqid, reqPO.POID);
                                    subject = string.Format(GenerateEmailBody("POAcceptedVendorSubj"), reqid);
                                    sms = string.Format(GenerateEmailBody("POAcceptedsmsVendor"), vendor.FirstName, vendor.LastName, reqid, reqPO.POID);
                                }
                                else if (status == Utilities.GetEnumDesc<PRMStatus>(PRMStatus.MaterialDispatched.ToString()))
                                {
                                    body = GenerateEmailBody("MaterialDispatchedemailVendor");
                                    body = string.Format(GenerateEmailBody("MaterialDispatchedemailVendor"), vendor.FirstName, vendor.LastName, reqid, reqPO.POID);
                                    subject = string.Format(GenerateEmailBody("MaterialDispatchedVendorSubj"), reqid);
                                    sms = string.Format(GenerateEmailBody("MaterialDispatchedVendorSMS"), vendor.FirstName, vendor.LastName, reqid, reqPO.POID);
                                }
                                else if (status == Utilities.GetEnumDesc<PRMStatus>(PRMStatus.MaterialReceived.ToString()))
                                {
                                    body = GenerateEmailBody("MaterialReceivedemailVendor");
                                    body = string.Format(GenerateEmailBody("MaterialReceivedemailVendor"), vendor.FirstName, vendor.LastName, reqid, reqPO.POID);
                                    subject = string.Format(GenerateEmailBody("MaterialReceivedVendSubj"), reqid);
                                    sms = string.Format(GenerateEmailBody("MaterialReceivedSMSVendor"), vendor.FirstName, vendor.LastName, reqid, reqPO.POID);
                                }
                                else if (status == Utilities.GetEnumDesc<PRMStatus>(PRMStatus.PaymentPROCESSING.ToString()))
                                {
                                    body = GenerateEmailBody("PaymentProcessingemailVendor");
                                    body = string.Format(GenerateEmailBody("PaymentProcessingemailVendor"), vendor.FirstName, vendor.LastName, reqid, reqPO.POID);
                                    subject = string.Format(GenerateEmailBody("PaymentProcessingVendSubj"), reqid);
                                    sms = string.Format(GenerateEmailBody("PaymentProcessingSMS"), vendor.FirstName, vendor.LastName, reqid, reqPO.POID);
                                }
                                else if (status == "PAYMENT RELEASED")
                                {
                                    body = GenerateEmailBody("PaymentProcessingemailVendor");
                                    body = string.Format(GenerateEmailBody("PaymentProcessingemailVendor"), vendor.FirstName, vendor.LastName, reqid, reqPO.POID);
                                    subject = string.Format(GenerateEmailBody("PaymentProcessingVendSubj"), reqid);
                                    sms = string.Format(GenerateEmailBody("PaymentProcessingSMS"), vendor.FirstName, vendor.LastName, reqid, reqPO.POID);
                                }

                                if (body != string.Empty && subject != string.Empty)
                                {
                                    SendEmail(vendor.Email + "," + vendor.AltEmail, subject, body, reqid, 0, req.Module, sessionID).ConfigureAwait(false);

                                    sms = sms.Replace("<br/>", "");
                                    // SendSMS(string.Empty, vendor.PhoneNum + "," + vendor.AltPhoneNum, System.Web.HttpUtility.UrlEncode(sms.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), reqid, 0, req.Module, sessionID).ConfigureAwait(false);
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }
            return response;
        }

        public Response ItemWiseSelectVendor(int userID, int vendorID, int reqID, int itemID, string sessionID)
        {
            Response response = new Response();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                Requirement req = GetRequirementDataOfflinePrivate(reqID, userID, sessionID);
                int isValidSession = ValidateSession(sessionID);
                sd.Add("P_U_ID", userID);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_ITEM_ID", itemID);
                sd.Add("P_VENDOR_ID", vendorID);
                DataSet ds = sqlHelper.SelectList("fwd_ItemWiseSelectVendor", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    if (response.ObjectID > 0)
                    {
                        response.UserInfo = Utilities.GetUserNew(vendorID);
                        response.ObjectID = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Requirement GetReqData(int reqID, string sessionID)
        {
            Requirement requirement = new Requirement();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("fwd_pc_GetReqData", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    string[] arr = new string[] { };
                    byte[] test = new byte[] { };
                    requirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                    requirement.Description = row["REQ_DESC"] != DBNull.Value ? Convert.ToString(row["REQ_DESC"]) : string.Empty;
                    requirement.RequirementID = reqID;

                    List<VendorDetails> vendorDetails = new List<VendorDetails>();
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        foreach (DataRow row1 in ds.Tables[1].Rows)
                        {
                            VendorDetails vendor = new VendorDetails();
                            vendor.PO = new RequirementPO();
                            vendor.VendorID = row1["U_ID"] != DBNull.Value ? Convert.ToInt32(row1["U_ID"]) : 0;
                            vendorDetails.Add(vendor);
                        }
                    }

                    int productSNo = 0;
                    List<RequirementItems> ListRequirementItems = new List<RequirementItems>();
                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        foreach (DataRow row2 in ds.Tables[2].Rows)
                        {
                            RequirementItems RequirementItems = new RequirementItems();
                            RequirementItems.ProductSNo = productSNo++;
                            RequirementItems.ItemID = row2["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_ID"]) : 0;
                            RequirementItems.RequirementID = row2["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row2["REQ_ID"]) : 0;
                            RequirementItems.ProductQuantity = row2["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row2["QUANTITY"]) : 0;
                            RequirementItems.OthersBrands = row2["OTHER_BRAND"] != DBNull.Value ? Convert.ToString(row2["OTHER_BRAND"]) : string.Empty;
                            RequirementItems.ProductImageID = row2["IMAGE_ID"] != DBNull.Value ? Convert.ToInt32(row2["IMAGE_ID"]) : 0;
                            RequirementItems.IsDeleted = row2["IS_DELETED"] != DBNull.Value ? Convert.ToInt32(row2["IS_DELETED"]) : 0;
                            RequirementItems.ProductQuantityIn = row2["QUANTITY_IN"] != DBNull.Value ? Convert.ToString(row2["QUANTITY_IN"]) : string.Empty;
                            RequirementItems.SelectedVendorID = row2["SELECTED_VENDOR_ID"] != DBNull.Value ? Convert.ToInt32(row2["SELECTED_VENDOR_ID"]) : 0;

                            RequirementItems.ProductIDorName = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                            RequirementItems.ProductNo = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                            RequirementItems.ProductDescription = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                            RequirementItems.ProductBrand = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;
                            ListRequirementItems.Add(RequirementItems);
                        }
                        requirement.ItemSNoCount = productSNo;
                        requirement.ListRequirementItems = ListRequirementItems;
                    }

                    List<RequirementItems> ListQuotations = new List<RequirementItems>();
                    if (ds.Tables[3].Rows.Count > 0)
                    {
                        foreach (DataRow row3 in ds.Tables[3].Rows)
                        {
                            RequirementItems Quotation = new RequirementItems();
                            Quotation.ItemID = row3["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row3["ITEM_ID"]) : 0;
                            Quotation.ItemPrice = row3["PRICE"] != DBNull.Value ? Convert.ToDouble(row3["PRICE"]) : 0;
                            Quotation.OthersBrands = row3["OTHER_BRANDS"] != DBNull.Value ? Convert.ToString(row3["OTHER_BRANDS"]) : string.Empty; ;
                            Quotation.RevItemPrice = row3["REVICED_PRICE"] != DBNull.Value ? Convert.ToDouble(row3["REVICED_PRICE"]) : 0; ;
                            Quotation.UnitPrice = row3["UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row3["UNIT_PRICE"]) : 0; ;
                            Quotation.RevUnitPrice = row3["REV_UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row3["REV_UNIT_PRICE"]) : 0; ;
                            Quotation.CGst = row3["C_GST"] != DBNull.Value ? Convert.ToDouble(row3["C_GST"]) : 0; ;
                            Quotation.SGst = row3["S_GST"] != DBNull.Value ? Convert.ToDouble(row3["S_GST"]) : 0; ;
                            Quotation.IGst = row3["I_GST"] != DBNull.Value ? Convert.ToDouble(row3["I_GST"]) : 0; ;
                            Quotation.UnitMRP = row3["UNIT_MRP"] != DBNull.Value ? Convert.ToDouble(row3["UNIT_MRP"]) : 0; ;
                            Quotation.UnitDiscount = row3["UNIT_DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row3["UNIT_DISCOUNT"]) : 0; ;
                            Quotation.RevUnitDiscount = row3["REV_UNIT_DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row3["REV_UNIT_DISCOUNT"]) : 0; ;
                            ListQuotations.Add(Quotation);
                        }
                    }

                    foreach (var item in requirement.ListRequirementItems)
                    {
                        foreach (var quote in ListQuotations)
                        {
                            if (item.ItemID == quote.ItemID)
                            {
                                item.ItemPrice = quote.ItemPrice;
                                item.OthersBrands = quote.OthersBrands;
                                item.RevItemPrice = quote.RevItemPrice;
                                item.UnitPrice = quote.UnitPrice;
                                item.RevUnitPrice = quote.RevUnitPrice;
                                item.CGst = quote.CGst;
                                item.SGst = quote.SGst;
                                item.IGst = quote.IGst;
                                item.UnitMRP = quote.UnitMRP;
                                item.UnitDiscount = quote.UnitDiscount;
                                item.RevUnitDiscount = quote.RevUnitDiscount;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Requirement req = new Requirement();
                req.ErrorMessage = ex.Message;
                return req;
            }

            return requirement;
        }

        public Response SavePriceCap(int reqID, List<RequirementItems> listReqItems,
            double price, int isDiscountQuotation, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {
                foreach (var item in listReqItems)
                {
                    sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_REQ_ID", item.RequirementID);
                    sd.Add("P_ITEM_ID", item.ItemID);
                    sd.Add("P_PRICE", item.ItemPrice);
                    sd.Add("P_REVICED_PRICE", item.RevItemPrice);
                    sd.Add("P_UNIT_PRICE", item.UnitPrice);
                    sd.Add("P_REV_UNIT_PRICE", item.RevUnitPrice);
                    sd.Add("P_C_GST", item.CGst);
                    sd.Add("P_S_GST", item.SGst);
                    sd.Add("P_I_GST", item.IGst);
                    sd.Add("P_UNIT_MRP", item.UnitMRP);
                    sd.Add("P_UNIT_DISCOUNT", item.UnitDiscount);
                    sd.Add("P_REV_UNIT_DISCOUNT", item.RevUnitDiscount);
                    sqlHelper.SelectList("fwd_pc_SavePriceCapItemPrices", sd);
                }

                sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_PRICE", price);
                sd.Add("P_IS_DISCOUNT_QUOTATION", isDiscountQuotation);
                DataSet ds = sqlHelper.SelectList("fwd_pc_SavePriceCap", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }
        #endregion Services

        #region publicservices
        public async Task SendEmail(string To, string Subject, string Body, int reqID, int vendorID, string module, string sessionID = null, Attachment attachment = null, List<Attachment> ListAttachment = null, DateTime? startTime = null, long duration = 0, string reminderType = "", string cc = "")
        {
            string clientType = ConfigurationManager.AppSettings["SMTP_CLIENT_TYPE"].ToString();
            if (!clientType.Equals("SENDGRID", StringComparison.InvariantCultureIgnoreCase))
            {
                await SendSMTPEmail(To, Subject, Body, reqID, vendorID, module, sessionID, attachment, ListAttachment, startTime, duration, reminderType, cc);
            }
            else
            {
                //await SendGridEmail(To, Subject, Body, reqID, vendorID, module, sessionID, attachment, ListAttachment, startTime, duration, reminderType, cc);
            }
        }

        public User GetReqAlternateCommunications(int reqID, int vendorID)
        {
            User user = new User();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_VENDOR_ID", vendorID);
                IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
                DataSet ds = sqlHelper.SelectList("fwd_GetAlternateCommunications", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        user.AltPhoneNum += row["U_PHONE"] != DBNull.Value ? row["U_PHONE"].ToString() : string.Empty;
                        user.AltPhoneNum += ",";
                        user.AltEmail += row["U_EMAIL"] != DBNull.Value ? row["U_EMAIL"].ToString() : string.Empty;
                        user.AltEmail += ",";
                    }
                }
            }
            catch (Exception ex)
            {
                user.ErrorMessage = ex.Message;
            }
            return user;
        }
        public Requirement GetRequirementDataOffline(int reqID, int userID, string sessionID)
        {
            Requirement requirement = GetRequirementDataOfflinePrivate(reqID, userID, sessionID);
            return requirement;
        }
        public Response DeleteRequirement(int reqID, int userID, string sessionID, string reason)
        {
            int CallerID = userID;
            string ScreenName = "DELETE_REQUIREMENT";
            Response response = new Response();
            string msgStep = "Default";
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                Requirement req = GetRequirementDataOfflinePrivate(reqID, userID, sessionID);
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_U_ID", userID);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_REASON", reason);
                DataSet ds = sqlHelper.SelectList("fwd_DeleteRequirement", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    if (response.ObjectID > 0)
                    {
                        InvolvedParties involvedParties = new InvolvedParties();
                        involvedParties.RequirementID = req.RequirementID;
                        involvedParties.CustomerID = req.CustomerID;
                        involvedParties.SuperUserID = req.SuperUserID;
                        involvedParties.UserIDList = req.AuctionVendors.Select(x => x.VendorID).ToArray();
                        involvedParties.CallerID = CallerID;
                        var context = GlobalHost.ConnectionManager.GetHubContext<RequirementHub>();
                        involvedParties.MethodName = "DeleteRequirement";
                        involvedParties.CustCompID = req.CustCompID;
                        req.Inv = involvedParties;
                        //context.Clients.Group(Utilities.GroupName + req.RequirementID).checkRequirement(req);
                        context.Clients.Group(Utilities.GroupName + req.RequirementID + Utilities.UserIDComponent + req.CustomerID).checkRequirement(req);
                        context.Clients.Group(Utilities.GroupName + req.RequirementID + Utilities.UserIDComponent + req.SuperUserID).checkRequirement(req);
                        foreach (int vendorID in involvedParties.UserIDList)
                        {
                            req = GetRequirementData(reqID, vendorID, sessionID);
                            context.Clients.Group(Utilities.GroupName + req.RequirementID + Utilities.UserIDComponent + vendorID).checkRequirement(req);
                        }

                        msgStep = "Stored Procedure Executed";

                        req.Module = ScreenName;

                        UserInfo user = GetUser(userID);
                        string body = GenerateEmailBody("DeleteRequirementemailCustomer");
                        body = String.Format(body, user.FirstName, user.LastName, reqID);
                        msgStep = "-->>Step 1";
                        SendEmail(user.Email + "," + user.AltEmail, "Req Number: " + req.RequirementNumber + " Title: " + req.Title + " - Requirement Deleted Successfully", body, reqID, Convert.ToInt32(user.UserID), req.Module, sessionID).ConfigureAwait(false);


                        string body2 = GenerateEmailBody("DeleteRequirementsms");
                        body2 = String.Format(body2, user.FirstName, user.LastName, reqID);
                        body2 = body2.Replace("<br/>", "");
                       
                        UserInfo superUser = Utilities.GetSuperUser(req.CustomerID);
                        string bodySuper = GenerateEmailBody("DeleteRequirementemailCustomer");
                        bodySuper = String.Format(bodySuper, superUser.FirstName, superUser.LastName, reqID);
                        string body2Super = GenerateEmailBody("DeleteRequirementsms");
                        body2Super = String.Format(body2Super, superUser.FirstName, superUser.LastName, reqID);
                        body2Super = body2Super.Replace("<br/>", "");
                        string subUserID = req.CustomerID.ToString();
                        if (superUser.UserID != subUserID)
                        {
                            SendEmail(superUser.Email + "," + superUser.AltEmail, "Req Number: " + req.RequirementNumber + " Title: " + req.Title + " - Requirement Deleted Successfully", bodySuper, reqID, req.CustomerID, req.Module, sessionID).ConfigureAwait(false);
                        }

                        Response res1 = RemoveVendorFromAuction(involvedParties.UserIDList.ToList(), reqID, sessionID);
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message + msgStep;
            }

            return response;
        }
        public Requirement GetRequirementDataOfflinePrivate(int reqID, int userID, string sessionID)
        {
            Requirement requirement = new Requirement();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_EXCLUDE_PRICE_CAP", 0);
                DataSet ds = sqlHelper.SelectList("fwd_GetRequirementData", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    string[] arr = new string[] { };
                    byte[] test = new byte[] { };
                    requirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                    var tempReqType = row["REQ_TYPE"] != DBNull.Value ? Convert.ToString(row["REQ_TYPE"]) : string.Empty;
                    requirement.IsRFP = tempReqType == "2";
                    requirement.Description = row["REQ_DESC"] != DBNull.Value ? Convert.ToString(row["REQ_DESC"]) : string.Empty;
                    requirement.Category = row["REQ_CATEGORY"] != DBNull.Value ? Convert.ToString(row["REQ_CATEGORY"]).Split(',') : arr;
                    requirement.PostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                    requirement.Urgency = row["REQ_URGENCY"] != DBNull.Value ? Convert.ToString(row["REQ_URGENCY"]) : string.Empty;
                    requirement.Budget = row["REQ_BUDGET"] != DBNull.Value ? Convert.ToString(row["REQ_BUDGET"]) : string.Empty;
                    requirement.DeliveryLocation = row["REQ_DELIVERY_LOC"] != DBNull.Value ? Convert.ToString(row["REQ_DELIVERY_LOC"]) : string.Empty;
                    requirement.Taxes = row["REQ_TAXES"] != DBNull.Value ? Convert.ToString(row["REQ_TAXES"]) : string.Empty;
                    requirement.PaymentTerms = row["REQ_PAYMENT_TERMS"] != DBNull.Value ? Convert.ToString(row["REQ_PAYMENT_TERMS"]) : string.Empty;
                    requirement.Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                    requirement.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                    requirement.CustomerID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                    requirement.CustFirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                    requirement.CustLastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                    requirement.DeliveryTime = row["REQ_DELIVERY_TIME"] != DBNull.Value ? Convert.ToString(row["REQ_DELIVERY_TIME"]) : string.Empty;
                    requirement.RequirementID = reqID;
                    int isPOSent = row["IS_PO_SENT"] != DBNull.Value ? Convert.ToInt32(row["IS_PO_SENT"]) : 0;
                    //requirement.AttachmentName = row["REQ_ATTACHMENT"] != DBNull.Value ? Convert.ToBase64String((byte[])row["REQ_ATTACHMENT"]) : string.Empty;
                    string fileName = row["REQ_ATTACHMENT"] != DBNull.Value ? Convert.ToString(row["REQ_ATTACHMENT"]) : string.Empty;
                    string URL = fileName != string.Empty ? fileName : string.Empty;
                    requirement.AttachmentName = URL;
                    requirement.EndTime = row["END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["END_TIME"]) : DateTime.MaxValue;
                    requirement.SuperUserID = row["SUPER_U_ID"] != DBNull.Value ? Convert.ToInt32(row["SUPER_U_ID"]) : requirement.CustomerID;
                    requirement.SelectedVendorID = row["SELECTED_VENDOR_ID"] != DBNull.Value ? Convert.ToInt32(row["SELECTED_VENDOR_ID"]) : requirement.CustomerID;
                    requirement.IsStopped = row["IS_STOPPED"] != DBNull.Value ? (Convert.ToInt32(row["IS_STOPPED"]) == 1 ? true : false) : false;
                    requirement.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                    requirement.LotId = row["LOT_ID"] != DBNull.Value ? Convert.ToInt32(row["LOT_ID"]) : 0;
                    requirement.CustomerCompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                    requirement.IsTechScoreReq = row["TECH_SCORE_REQ"] != DBNull.Value ? Convert.ToInt32(row["TECH_SCORE_REQ"]) : 0;
                    requirement.Currency = row["REQ_CURRENCY"] != DBNull.Value ? Convert.ToString(row["REQ_CURRENCY"]) : string.Empty;
                    requirement.TimeZone = row["REQ_TIMEZONE"] != DBNull.Value ? Convert.ToString(row["REQ_TIMEZONE"]) : string.Empty;

                    requirement.CustCompID = row["CUST_COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["CUST_COMP_ID"]) : 0;

                    requirement.IsQuotationPriceLimit = row["IS_QUOTATION_PRICE_LIMIT"] != DBNull.Value ? (Convert.ToInt32(row["IS_QUOTATION_PRICE_LIMIT"]) == 1 ? true : false) : false;
                    requirement.QuotationPriceLimit = row["QUOTATION_PRICE_LIMIT"] != DBNull.Value ? Convert.ToDouble(row["QUOTATION_PRICE_LIMIT"]) : 0;
                    requirement.NoOfQuotationReminders = row["NO_OF_QUOTATION_REMINDERS"] != DBNull.Value ? Convert.ToInt32(row["NO_OF_QUOTATION_REMINDERS"]) : 0;
                    requirement.RemindersTimeInterval = row["REMINDERS_TIME_INTERVAL"] != DBNull.Value ? Convert.ToInt32(row["REMINDERS_TIME_INTERVAL"]) : 0;

                    requirement.IsDiscountQuotation = row["IS_DISCOUNT_QUOTATION"] != DBNull.Value ? Convert.ToInt32(row["IS_DISCOUNT_QUOTATION"]) : 0;
                    requirement.IsRevUnitDiscountEnable = row["IS_REV_UNIT_DISCOUNT_ENABLE"] != DBNull.Value ? Convert.ToInt32(row["IS_REV_UNIT_DISCOUNT_ENABLE"]) : 0;
                    requirement.AuditVersion = row["AUDIT_VERSION"] != DBNull.Value ? Convert.ToInt32(row["AUDIT_VERSION"]) : 1;
                    requirement.AuditComments = row["AUDIT_COMMENTS"] != DBNull.Value ? Convert.ToString(row["AUDIT_COMMENTS"]) : string.Empty;
                    requirement.BiddingType = row["BIDDING_TYPE"] != DBNull.Value ? Convert.ToString(row["BIDDING_TYPE"]) : string.Empty;
                    requirement.RequirementNumber = row["REQ_NUMBER"] != DBNull.Value ? Convert.ToString(row["REQ_NUMBER"]) : string.Empty;

                    NegotiationSettings NegotiationSettings = new NegotiationSettings();
                    NegotiationSettings.MinReductionAmount = row["UD_MIN_REDUCE_AMOUNT"] != DBNull.Value ? Convert.ToDouble(row["UD_MIN_REDUCE_AMOUNT"]) : 0;
                    NegotiationSettings.RankComparision = row["UD_VENDOR_COMPARISION_MIN_AMOUNT"] != DBNull.Value ? Convert.ToDouble(row["UD_VENDOR_COMPARISION_MIN_AMOUNT"]) : 0;
                    NegotiationSettings.NegotiationDuration = row["UD_NEGOTIATION_DURATION"] != DBNull.Value ? Convert.ToString(row["UD_NEGOTIATION_DURATION"]) : string.Empty;
                    int tempDisableLocalGSTFeature = row["DISABLE_LOCAL_GST_FEATURE"] != DBNull.Value ? Convert.ToUInt16(row["DISABLE_LOCAL_GST_FEATURE"]) : 0;
                    NegotiationSettings.DisableLocalGSTFeature = tempDisableLocalGSTFeature > 0 ? true : false;
                    requirement.NegotiationSettings = NegotiationSettings;
                    DateTime now = DateTime.UtcNow;
                    if (requirement.EndTime != DateTime.MaxValue && requirement.EndTime > now && requirement.StartTime < now)
                    {
                        DateTime date = Convert.ToDateTime(requirement.EndTime);
                        long diff = Convert.ToInt64((date - now).TotalSeconds);
                        requirement.TimeLeft = diff;
                        requirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString());

                    }
                    else if (requirement.StartTime != DateTime.MaxValue && requirement.StartTime > now)
                    {
                        DateTime date = Convert.ToDateTime(requirement.StartTime);
                        long diff = Convert.ToInt64((date - now).TotalSeconds);
                        requirement.TimeLeft = diff;
                        requirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString());
                    }
                    else if (requirement.EndTime < now)
                    {
                        requirement.TimeLeft = -1;
                    }
                    else if (requirement.StartTime == DateTime.MaxValue)
                    {
                        requirement.TimeLeft = -1;
                        requirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.UNCONFIRMED.ToString());
                    }
                    List<VendorDetails> vendorDetails = new List<VendorDetails>();
                    int rank = 1;
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        foreach (DataRow row1 in ds.Tables[1].Rows)
                        {
                            VendorDetails vendor = new VendorDetails();

                            vendor.VendorID = row1["U_ID"] != DBNull.Value ? Convert.ToInt32(row1["U_ID"]) : 0;
                            vendor.compID = row1["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row1["COMP_ID"]) : 0;
                            vendor.IS_QUOT_LATE = row1["IS_QUOT_LATE"] != DBNull.Value ? Convert.ToInt32(row1["IS_QUOT_LATE"]) : 0;
                            vendor.VendorName = row1["VENDOR_NAME"] != DBNull.Value ? Convert.ToString(row1["VENDOR_NAME"]) : string.Empty;
                            vendor.InitialPrice = row1["VEND_INIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["VEND_INIT_PRICE"]) : 0;
                            vendor.RunningPrice = row1["VEND_RUN_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["VEND_RUN_PRICE"]) : 0;
                            vendor.City = row1["UAD_CITY"] != DBNull.Value ? Convert.ToString(row1["UAD_CITY"]) : string.Empty;
                            vendor.Taxes = row1["TAXES"] != DBNull.Value ? Convert.ToDouble(row1["TAXES"]) : 0;
                            vendor.TotalInitialPrice = row1["VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["VEND_TOTAL_PRICE"]) : 0;
                            vendor.TotalRunningPrice = row1["VEND_TOTAL_PRICE_RUNNING"] != DBNull.Value ? Convert.ToDouble(row1["VEND_TOTAL_PRICE_RUNNING"]) : 0;
                            vendor.Rating = row1["RATING"] != DBNull.Value ? Convert.ToDouble(row1["RATING"]) : 0;
                            vendor.TechnicalScore = row1["TECHNICAL_SCORE"] != DBNull.Value ? Convert.ToDecimal(row1["TECHNICAL_SCORE"]) : 0;
                            vendor.Taxes = row1["VEND_TAXES"] != DBNull.Value ? Convert.ToInt32(row1["VEND_TAXES"]) : 0;
                            vendor.CompanyName = row1["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row1["COMPANY_NAME"]) : string.Empty;
                            vendor.Warranty = row1["WARRANTY"] != DBNull.Value ? Convert.ToString(row1["WARRANTY"]) : string.Empty;
                            vendor.Payment = row1["PAYMENT"] != DBNull.Value ? Convert.ToString(row1["PAYMENT"]) : string.Empty;
                            vendor.Duration = row1["DURATION"] != DBNull.Value ? Convert.ToString(row1["DURATION"]) : string.Empty;
                            vendor.Validity = row1["VALIDITY"] != DBNull.Value ? Convert.ToString(row1["VALIDITY"]) : string.Empty;
                            vendor.OtherProperties = row1["OTHER_PROPERTIES"] != DBNull.Value ? Convert.ToString(row1["OTHER_PROPERTIES"]) : string.Empty;
                            vendor.IsQuotationRejected = row1["IS_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToInt32(row1["IS_QUOTATION_REJECTED"]) : 0;
                            vendor.QuotationRejectedComment = row1["QUOTATION_REJECTED_REASON"] != DBNull.Value ? Convert.ToString(row1["QUOTATION_REJECTED_REASON"]) : string.Empty;
                            vendor.PO = new RequirementPO();
                            vendor.PO.POLink = row1["PO_ATT_ID"] != DBNull.Value ? Convert.ToString(row1["PO_ATT_ID"]) : "0";
                            string fileName1 = row1["QUOTATION_URL"] != DBNull.Value ? Convert.ToString(row1["QUOTATION_URL"]) : string.Empty;
                            vendor.Rating = row1["RATING"] != DBNull.Value ? Convert.ToDouble(row1["RATING"]) : 0;
                            string URL1 = fileName1 != string.Empty ? fileName1 : string.Empty;
                            vendor.QuotationUrl = URL1;
                            string fileName2 = row1["REV_QUOTATION_URL"] != DBNull.Value ? Convert.ToString(row1["REV_QUOTATION_URL"]) : string.Empty;
                            string URL2 = fileName2 != string.Empty ? fileName2 : string.Empty;
                            vendor.RevQuotationUrl = URL2;
                            vendor.RevPrice = row1["REV_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["REV_PRICE"]) : 0;
                            vendor.RevVendorTotalPrice = row1["REV_VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["REV_VEND_TOTAL_PRICE"]) : 0;
                            vendor.RevVendorTotalPriceNoTax = row1["REV_VEND_TOTAL_PRICE_NO_TAX"] != DBNull.Value ? Convert.ToDouble(row1["REV_VEND_TOTAL_PRICE_NO_TAX"]) : 0;
                            vendor.IsRevQuotationRejected = row1["IS_REV_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToInt32(row1["IS_REV_QUOTATION_REJECTED"]) : 0;
                            vendor.RevQuotationRejectedComment = row1["REV_QUOTATION_REJECTED_REASON"] != DBNull.Value ? Convert.ToString(row1["REV_QUOTATION_REJECTED_REASON"]) : string.Empty;
                            vendor.LastActiveTime = row1["LAST_ACTIVE_TIME"] != DBNull.Value ? Convert.ToString(row1["LAST_ACTIVE_TIME"]) : string.Empty;
                            vendor.Discount = row1["DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row1["DISCOUNT"]) : 0;
                            vendor.ReductionPercentage = row1["REDUCTION_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row1["REDUCTION_PERCENTAGE"]) : 0;
                            vendor.Vendor = new User();
                            vendor.Vendor.Email = row1["U_EMAIL"] != DBNull.Value ? Convert.ToString(row1["U_EMAIL"]) : string.Empty;
                            vendor.Vendor.PhoneNum = row1["U_PHONE"] != DBNull.Value ? Convert.ToString(row1["U_PHONE"]) : string.Empty;
                            vendor.PO.MaterialDispachmentLink = row1["MAT_DIS_LINK"] != DBNull.Value ? Convert.ToInt32(row1["MAT_DIS_LINK"]) : 0;
                            vendor.PO.MaterialReceivedLink = row1["MR_LINK"] != DBNull.Value ? Convert.ToInt32(row1["MR_LINK"]) : 0;
                            vendor.LandingPrice = row1["LANDING_PRICE"] != DBNull.Value ? Convert.ToDecimal(row1["LANDING_PRICE"]) : 0;
                            vendor.RevLandingPrice = row1["REV_LANDING_PRICE"] != DBNull.Value ? Convert.ToDecimal(row1["REV_LANDING_PRICE"]) : 0;
                            vendor.SumOfMargin = row1["PARAM_SUM_MARGIN"] != DBNull.Value ? Convert.ToDecimal(row1["PARAM_SUM_MARGIN"]) : 0;
                            vendor.SumOfInitialMargin = row1["PARAM_SUM_INITIAL_MARGIN"] != DBNull.Value ? Convert.ToDecimal(row1["PARAM_SUM_INITIAL_MARGIN"]) : 0;
                            vendor.MultipleAttachments = row1["MULTIPLE_ATTACHMENTS"] != DBNull.Value ? Convert.ToString(row1["MULTIPLE_ATTACHMENTS"]) : string.Empty;
                            vendor.GstNumber = row1["GST_NUMBER"] != DBNull.Value ? Convert.ToString(row1["GST_NUMBER"]) : string.Empty;
                            vendor.LandingPrice = Math.Round(vendor.LandingPrice, decimal_round);
                            vendor.RevLandingPrice = Math.Round(vendor.RevLandingPrice, decimal_round);
                            vendor.ReductionPercentage = Math.Round(vendor.ReductionPercentage, decimal_round);
                            vendor.DIFFERENTIAL_FACTOR = row1["DIFFERENTIAL_FACTOR"] != DBNull.Value ? Convert.ToDecimal(row1["DIFFERENTIAL_FACTOR"]) : 0;
                            vendor.DF_REV_VENDOR_TOTAL_PRICE = Convert.ToDecimal(vendor.RevVendorTotalPrice) + vendor.DIFFERENTIAL_FACTOR;
                            vendor.DF_REV_VENDOR_TOTAL_PRICE_NO_TAX = Convert.ToDecimal(vendor.RevVendorTotalPriceNoTax) + vendor.DIFFERENTIAL_FACTOR;
                            vendor.AuctionModifiedDate = row1["AUCTION_MODIFIED_DATE"] != DBNull.Value ? Convert.ToDateTime(row1["AUCTION_MODIFIED_DATE"]) : DateTime.MaxValue;
                            vendor.INCO_TERMS = row1["INCO_TERMS"] != DBNull.Value ? Convert.ToString(row1["INCO_TERMS"]) : string.Empty;
                            vendor.SurrogateComments = row1["SURROGATE_COMMENTS"] != DBNull.Value ? Convert.ToString(row1["SURROGATE_COMMENTS"]) : string.Empty;
                            vendor.VendorCurrencyFactor = row1["CURRENCY_FACTOR"] != DBNull.Value ? Convert.ToDecimal(row1["CURRENCY_FACTOR"]) : 1;
                            vendor.Rank = rank;

                            vendorDetails.Add(vendor);
                            rank++;
                        }
                    }


                    if (requirement.IsDiscountQuotation == 2)
                    {
                        decimal maxInitialMargin = 0;
                        decimal maxClosedMargin = 0;

                        try
                        {
                            maxInitialMargin = vendorDetails.Where(vd => vd.CompanyName != "PRICE_CAP" && vd.CompanyName != "DEFAULT_VENDOR" && vd.IsQuotationRejected == 0).Max(vd => vd.SumOfInitialMargin);
                            maxClosedMargin = vendorDetails.Where(vd => vd.CompanyName != "PRICE_CAP" && vd.CompanyName != "DEFAULT_VENDOR" && vd.IsQuotationRejected == 0).Max(vd => vd.SumOfMargin);
                        }
                        catch
                        {
                            maxInitialMargin = 0;
                            maxClosedMargin = 0;
                        }


                        requirement.Savings = Convert.ToDouble(maxClosedMargin - maxInitialMargin);
                        requirement.Savings = Math.Round(requirement.Savings, decimal_round);
                    }

                    requirement.AuctionVendors = vendorDetails;
                    List<VendorDetails> v2 = new List<VendorDetails>();
                    if (requirement.StartTime > DateTime.UtcNow)
                    {
                        v2 = vendorDetails.Where(v => v.IsQuotationRejected == 0).Where(v => v.TotalRunningPrice > 0).OrderByDescending(v => v.TotalRunningPrice).ThenBy(p => p.AuctionModifiedDate).ToList();
                        v2.AddRange(vendorDetails.Where(v => v.IsQuotationRejected != 0).Where(v => v.TotalRunningPrice > 0).OrderByDescending(v => v.TotalRunningPrice).ThenBy(p => p.AuctionModifiedDate).ToList());
                        v2.AddRange(vendorDetails.Where(v => v.InitialPrice == 0).ToList());

                    }
                    else
                    {
                        v2 = vendorDetails.Where(v => v.IsQuotationRejected == 0).Where(v => v.TotalRunningPrice > 0).OrderByDescending(v => v.TotalRunningPrice).ThenBy(p => p.AuctionModifiedDate).ToList();
                        v2.AddRange(vendorDetails.Where(v => v.IsQuotationRejected != 0).Where(v => v.TotalRunningPrice > 0).OrderByDescending(v => v.TotalRunningPrice).ThenBy(p => p.AuctionModifiedDate).ToList());
                        v2.AddRange(vendorDetails.Where(v => v.TotalRunningPrice == 0).ToList());

                    }
                    if (v2.Count > 0)
                    {
                        VendorDetails vendor1 = v2[0];
                        if (vendor1.TotalRunningPrice == 0)
                        {
                            requirement.Price = vendor1.TotalInitialPrice;
                        }
                        else if (vendor1.TotalInitialPrice < vendor1.TotalRunningPrice && vendor1.TotalInitialPrice != 0)
                        {
                            requirement.Price = vendor1.TotalInitialPrice;
                        }
                        else
                        {
                            requirement.Price = vendor1.TotalRunningPrice;
                        }
                    }

                    List<VendorDetails> tv2 = v2.Where(v => v.IsQuotationRejected == 0).OrderByDescending(v => DoInclusiveTax(v, requirement.InclusiveTax)).ThenBy(p => p.AuctionModifiedDate).ToList();
                    foreach (VendorDetails vendor in tv2)
                    {
                        if (vendor.DF_REV_VENDOR_TOTAL_PRICE > 0)
                        {
                            vendor.Rank = v2.IndexOf(vendor) + 1;
                        }
                    }

                    List<VendorDetails> notApprovedVendors = v2.Where(v => v.IsQuotationRejected != 0).ToList();
                    foreach (VendorDetails vendor in notApprovedVendors)
                    {
                        tv2.Add(vendor);
                    }
                    v2 = tv2;
                    if (tv2 != null && tv2.Count > 0) { } else { tv2 = v2; }
                    if (requirement.CustomerID == userID || requirement.SuperUserID == userID || requirement.CustomerReqAccess == true)
                    {
                        requirement.AuctionVendors = tv2;
                    }
                    else
                    {
                        requirement.AuctionVendors = v2.Where(v => v.VendorID == userID).OrderByDescending(v => DoInclusiveTax(v, requirement.InclusiveTax)).ThenBy(p => p.AuctionModifiedDate).ToList();
                    }

                    requirement.SelectedVendor = requirement.AuctionVendors.FirstOrDefault(v => v.VendorID == requirement.SelectedVendorID);
                    requirement.CustomerReqAccess = false;
                    if (ds.Tables[5].Rows.Count > 0)
                    {
                        foreach (DataRow row5 in ds.Tables[5].Rows)
                        {
                            int reqCustomerId = row5["U_ID"] != DBNull.Value ? Convert.ToInt32(row5["U_ID"]) : 0;

                            if (userID == reqCustomerId)
                            {
                                requirement.CustomerReqAccess = true;
                            }
                        }
                    }

                    int productSNo = 0;
                    string CATEGORY_ID = string.Empty;
                    List<RequirementItems> ListRequirementItems = new List<RequirementItems>();
                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        foreach (DataRow row2 in ds.Tables[2].Rows)
                        {
                            RequirementItems RequirementItems = new RequirementItems();
                            RequirementItems.ProductSNo = productSNo++;
                            RequirementItems.ItemID = row2["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_ID"]) : 0;
                            RequirementItems.RequirementID = row2["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row2["REQ_ID"]) : 0;
                            RequirementItems.ProductIDorName = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                            RequirementItems.ProductNo = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                            RequirementItems.ProductDescription = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                            RequirementItems.ProductQuantity = row2["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row2["QUANTITY"]) : 0;
                            RequirementItems.ProductBrand = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;
                            RequirementItems.OthersBrands = row2["OTHER_BRAND"] != DBNull.Value ? Convert.ToString(row2["OTHER_BRAND"]) : string.Empty;
                            RequirementItems.ProductImageID = row2["IMAGE_ID"] != DBNull.Value ? Convert.ToInt32(row2["IMAGE_ID"]) : 0;
                            RequirementItems.IsDeleted = row2["IS_DELETED"] != DBNull.Value ? Convert.ToInt32(row2["IS_DELETED"]) : 0;
                            RequirementItems.ProductQuantityIn = row2["QUANTITY_IN"] != DBNull.Value ? Convert.ToString(row2["QUANTITY_IN"]) : string.Empty;
                            RequirementItems.CeilingPrice = row2.Table.Columns.Contains("CEILING_PRICE") && row2["CEILING_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["CEILING_PRICE"]) : 0;
                            RequirementItems.IsCoreProductCategory = row2["IS_CORE_CAT"] != DBNull.Value ? Convert.ToInt32(row2["IS_CORE_CAT"]) : 0;
                            CATEGORY_ID = row2["CATEGORY_ID"] != DBNull.Value ? Convert.ToString(row2["CATEGORY_ID"]) : string.Empty;
                            RequirementItems.CATEGORY_ID = CATEGORY_ID;
                            ListRequirementItems.Add(RequirementItems);
                        }
                    }


                    if (ds.Tables[3].Rows.Count > 0)
                    {
                        foreach (DataRow row2 in ds.Tables[3].Rows)
                        {

                            int uID = row2["U_ID"] != DBNull.Value ? Convert.ToInt32(row2["U_ID"]) : 0;
                            int itemid = row2["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_ID"]) : 0;

                            double price = row2["PRICE"] != DBNull.Value ? Convert.ToDouble(row2["PRICE"]) : 0;
                            string name = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                            string no = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                            string des = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                            string brand = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;
                            int isCoreProductCategory = row2["IS_CORE_CAT"] != DBNull.Value ? Convert.ToInt32(row2["IS_CORE_CAT"]) : 0;
                            double ceilingPrice = row2["CEILING_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["CEILING_PRICE"]) : 0;

                            if (uID == userID || requirement.CustomerReqAccess == true)
                            {
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ItemPrice = price;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ProductIDorName = name;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ProductNo = no;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ProductDescription = des;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ProductBrand = brand;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().IsCoreProductCategory = isCoreProductCategory;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().CATEGORY_ID = CATEGORY_ID;
                                if (ceilingPrice > 0)
                                {
                                    ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().CeilingPrice = ceilingPrice;
                                }
                            }
                        }
                    }
                    requirement.ListRequirementItems = ListRequirementItems;
                    if (requirement.IsDiscountQuotation == 1 || requirement.IsDiscountQuotation == 0 || requirement.IsDiscountQuotation == 2)
                    {
                        tv2 = v2.Where(v => v.IsQuotationRejected == 0).OrderByDescending(v => DoInclusiveTax(v, requirement.InclusiveTax)).ThenBy(p => p.AuctionModifiedDate).ToList();
                        foreach (VendorDetails vendor in tv2)
                        {
                            if (vendor.DF_REV_VENDOR_TOTAL_PRICE > 0)
                            {
                                vendor.Rank = v2.IndexOf(vendor) + 1;
                            }
                        }
                        foreach (VendorDetails vendor in notApprovedVendors)
                        {
                            tv2.Add(vendor);
                        }
                        v2 = tv2;
                        if (tv2 != null && tv2.Count > 0) { } else { tv2 = v2; }
                        if (requirement.CustomerID == userID || requirement.SuperUserID == userID || requirement.CustomerReqAccess == true)
                        {
                            requirement.AuctionVendors = tv2;
                        }
                        else
                        {
                            requirement.AuctionVendors = v2.Where(v => v.VendorID == userID).OrderByDescending(v => DoInclusiveTax(v, requirement.InclusiveTax)).ThenBy(p => p.AuctionModifiedDate).ToList();
                        }
                    }

                    int taxSNo = 0;
                    List<RequirementTaxes> ListRequirementTaxes = new List<RequirementTaxes>();
                    if (ds.Tables[4].Rows.Count > 0)
                    {
                        foreach (DataRow row4 in ds.Tables[4].Rows)
                        {
                            RequirementTaxes requirementTaxes = new RequirementTaxes();
                            requirementTaxes.TaxSNo = taxSNo++;
                            int uID = row4["U_ID"] != DBNull.Value ? Convert.ToInt32(row4["U_ID"]) : 0;
                            if (uID == userID || requirement.CustomerReqAccess == true)
                            {
                                requirementTaxes.TaxID = row4["QT_ID"] != DBNull.Value ? Convert.ToInt32(row4["QT_ID"]) : 0;
                                requirementTaxes.TaxName = row4["TAX_NAME"] != DBNull.Value ? Convert.ToString(row4["TAX_NAME"]) : string.Empty;
                                requirementTaxes.TaxPercentage = row4["TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row4["TAX_PERCENTAGE"]) : 0;
                                requirementTaxes.IsTaxDeleted = row4["IS_TAX_DELETED"] != DBNull.Value ? Convert.ToInt32(row4["IS_TAX_DELETED"]) : 0;

                                ListRequirementTaxes.Add(requirementTaxes);
                            }
                        }

                        requirement.TaxSNoCount = taxSNo;
                    }

                    requirement.ListRequirementTaxes = ListRequirementTaxes;
                }
            }
            catch (Exception ex)
            {
                Requirement req = new Requirement();
                req.ErrorMessage = ex.Message;
                return req;
            }

            return requirement;
        }

        public Requirement GetRequirementDataFilter(int userID, string sessionID, DataSet ds)
        {
            Requirement requirement = new Requirement();
            requirement.EntityID = userID;
            try
            {
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    logger.Debug("TESTINGIN2");
                    DataRow row = ds.Tables[0].Rows[0];
                    string[] arr = new string[] { };
                    byte[] test = new byte[] { };
                    requirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                    requirement.RequirementNumber = row["REQ_NUMBER"] != DBNull.Value ? Convert.ToString(row["REQ_NUMBER"]) : string.Empty;
                    requirement.BiddingType = row["BIDDING_TYPE"] != DBNull.Value ? Convert.ToString(row["BIDDING_TYPE"]) : string.Empty;
                    var tempReqType = row["REQ_TYPE"] != DBNull.Value ? Convert.ToString(row["REQ_TYPE"]) : string.Empty;
                    requirement.ProjectDetails = row["PROJECT_DETAILS"] != DBNull.Value ? Convert.ToString(row["PROJECT_DETAILS"]) : string.Empty;
                    requirement.ProjectName = row["PROJECT_NAME"] != DBNull.Value ? Convert.ToString(row["PROJECT_NAME"]) : string.Empty;
                    requirement.IsRFP = tempReqType == "2";
                    requirement.Description = row["REQ_DESC"] != DBNull.Value ? Convert.ToString(row["REQ_DESC"]) : string.Empty;
                    requirement.Category = row["REQ_CATEGORY"] != DBNull.Value ? Convert.ToString(row["REQ_CATEGORY"]).Split(',') : arr;
                    requirement.Subcategories = row["REQ_SUBCATEGORIES"] != DBNull.Value ? Convert.ToString(row["REQ_SUBCATEGORIES"]) : string.Empty;
                    requirement.PostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                    logger.Debug("TESTINGIN1");
                    requirement.Urgency = row["REQ_URGENCY"] != DBNull.Value ? Convert.ToString(row["REQ_URGENCY"]) : string.Empty;
                    requirement.Budget = row["REQ_BUDGET"] != DBNull.Value ? Convert.ToString(row["REQ_BUDGET"]) : string.Empty;
                    requirement.DeliveryLocation = row["REQ_DELIVERY_LOC"] != DBNull.Value ? Convert.ToString(row["REQ_DELIVERY_LOC"]) : string.Empty;
                    requirement.Taxes = row["REQ_TAXES"] != DBNull.Value ? Convert.ToString(row["REQ_TAXES"]) : string.Empty;
                    requirement.PaymentTerms = row["REQ_PAYMENT_TERMS"] != DBNull.Value ? Convert.ToString(row["REQ_PAYMENT_TERMS"]) : string.Empty;
                    requirement.Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                    string Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                    requirement.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                    logger.Debug("TESTINGIN3");
                    requirement.CustomerID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                    requirement.IsTechScoreReq = row["TECH_SCORE_REQ"] != DBNull.Value ? Convert.ToInt32(row["TECH_SCORE_REQ"]) : 0;

                    requirement.CustFirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                    requirement.CustLastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                    requirement.DeliveryTime = row["REQ_DELIVERY_TIME"] != DBNull.Value ? Convert.ToString(row["REQ_DELIVERY_TIME"]) : string.Empty;
                    requirement.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                    int isPOSent = row["IS_PO_SENT"] != DBNull.Value ? Convert.ToInt32(row["IS_PO_SENT"]) : 0;
                    //requirement.AttachmentName = row["REQ_ATTACHMENT"] != DBNull.Value ? Convert.ToBase64String((byte[])row["REQ_ATTACHMENT"]) : string.Empty;
                    string fileName = row["REQ_ATTACHMENT"] != DBNull.Value ? Convert.ToString(row["REQ_ATTACHMENT"]) : string.Empty;

                    string POLink = row["PURCHASE_ORDER_LINK"] != DBNull.Value ? (row["PURCHASE_ORDER_LINK"].ToString()) : string.Empty;
                    requirement.POLink = !string.IsNullOrEmpty(POLink) ? POLink : string.Empty;
                    string URL = fileName != string.Empty ? fileName : string.Empty;
                    requirement.AttachmentName = fileName;
                    requirement.EndTime = row["END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["END_TIME"]) : DateTime.MaxValue;
                    logger.Debug("TESTINGIN5");
                    requirement.InclusiveTax = row["REQ_INCLUSIVE_TAX"] != DBNull.Value ? (Convert.ToInt32(row["REQ_INCLUSIVE_TAX"]) == 1 ? true : false) : false;
                    requirement.IncludeFreight = row["REQ_INCLUDE_FREIGHT"] != DBNull.Value ? (Convert.ToInt32(row["REQ_INCLUDE_FREIGHT"]) == 1 ? true : false) : false;
                    requirement.SuperUserID = row["SUPER_U_ID"] != DBNull.Value ? Convert.ToInt32(row["SUPER_U_ID"]) : requirement.CustomerID;
                    requirement.SelectedVendorID = row["SELECTED_VENDOR_ID"] != DBNull.Value ? Convert.ToInt32(row["SELECTED_VENDOR_ID"]) : requirement.CustomerID;
                    requirement.CustomerCompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                    requirement.CustomerCompanyId = row["CUST_COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["CUST_COMP_ID"]) : 0;
                    requirement.MinReduceAmount = row["MIN_REDUCE_AMOUNT"] != DBNull.Value ? Convert.ToDouble(row["MIN_REDUCE_AMOUNT"]) : 0;
                    requirement.QuotationFreezTime = row["QUOTATION_FREEZ_TIME"] != DBNull.Value ? Convert.ToDateTime(row["QUOTATION_FREEZ_TIME"]) : DateTime.MaxValue;
                    logger.Debug("TESTINGIN16");
                    requirement.IsTabular = row["REQ_IS_TABULAR"] != DBNull.Value ? (Convert.ToInt32(row["REQ_IS_TABULAR"]) == 1 ? true : false) : false;
                    requirement.ReqComments = row["REQ_COMMENTS"] != DBNull.Value ? Convert.ToString(row["REQ_COMMENTS"]) : string.Empty;
                    logger.Debug("TESTINGIN8");
                    requirement.ExpStartTime = row["EXP_START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["EXP_START_TIME"]) : DateTime.MaxValue;
                    logger.Debug("TESTINGIN17");
                    if (requirement.MinReduceAmount > 0)
                    {
                        requirement.MinBidAmount = requirement.MinReduceAmount;
                    }
                    else
                    {
                        requirement.MinBidAmount = 0;
                        //requirement.MinBidAmount = Math.Round(Convert.ToDouble(requirement.Budget) / 100, decimal_round);
                    }

                    requirement.CustomerCompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                    requirement.IsStopped = row["IS_STOPPED"] != DBNull.Value ? (Convert.ToInt32(row["IS_STOPPED"]) == 1 ? true : false) : false;
                    requirement.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                    requirement.Currency = row["REQ_CURRENCY"] != DBNull.Value ? Convert.ToString(row["REQ_CURRENCY"]) : string.Empty;
                    requirement.TimeZone = row["REQ_TIMEZONE"] != DBNull.Value ? Convert.ToString(row["REQ_TIMEZONE"]) : string.Empty;

                    //requirement.IsNegotiationEnded = row["IS_NEGOTIATION_ENDED"] != DBNull.Value ? (Convert.ToInt32(row["IS_NEGOTIATION_ENDED"]) == 1 ? true : false) : false;
                    requirement.IsNegotiationEnded = row["IS_NEGOTIATION_ENDED"] != DBNull.Value ? Convert.ToInt32(row["IS_NEGOTIATION_ENDED"]) : 0;

                    requirement.NegotiationDuration = row["NEGOTIATION_DURATION"] != DBNull.Value ? Convert.ToString(row["NEGOTIATION_DURATION"]) : string.Empty;
                    requirement.MinVendorComparision = row["VENDOR_COMPARISION_MIN_AMOUNT"] != DBNull.Value ? Convert.ToDouble(row["VENDOR_COMPARISION_MIN_AMOUNT"]) : 0;

                    NegotiationSettings NegotiationSettings = new NegotiationSettings();
                    NegotiationSettings.MinReductionAmount = row["UD_MIN_REDUCE_AMOUNT"] != DBNull.Value ? Convert.ToDouble(row["UD_MIN_REDUCE_AMOUNT"]) : 0;
                    NegotiationSettings.RankComparision = row["UD_VENDOR_COMPARISION_MIN_AMOUNT"] != DBNull.Value ? Convert.ToDouble(row["UD_VENDOR_COMPARISION_MIN_AMOUNT"]) : 0;
                    NegotiationSettings.NegotiationDuration = row["UD_NEGOTIATION_DURATION"] != DBNull.Value ? Convert.ToString(row["UD_NEGOTIATION_DURATION"]) : string.Empty;
                    int tempDisableLocalGSTFeature = row["DISABLE_LOCAL_GST_FEATURE"] != DBNull.Value ? Convert.ToUInt16(row["DISABLE_LOCAL_GST_FEATURE"]) : 0;
                    NegotiationSettings.DisableLocalGSTFeature = tempDisableLocalGSTFeature > 0 ? true : false;

                    requirement.ReqPDF = row["REQ_PDF"] != DBNull.Value ? Convert.ToInt32(row["REQ_PDF"]) : 0;
                    requirement.ReqPDFCustomer = row["REQ_PDF_CUSTOMER"] != DBNull.Value ? Convert.ToInt32(row["REQ_PDF_CUSTOMER"]) : 0;

                    requirement.ReportReq = row["REPORT_REQ"] != DBNull.Value ? Convert.ToInt32(row["REPORT_REQ"]) : 0;
                    requirement.ReportItemWise = row["REPORT_ITEM_WISE"] != DBNull.Value ? Convert.ToInt32(row["REPORT_ITEM_WISE"]) : 0;

                    requirement.ReqType = row["PARAM_REQ_TYPE"] != DBNull.Value ? Convert.ToString(row["PARAM_REQ_TYPE"]) : string.Empty;
                    requirement.PriceCapValue = row["PARAM_PRICE_CAP_VALUE"] != DBNull.Value ? Convert.ToDouble(row["PARAM_PRICE_CAP_VALUE"]) : 0;

                    requirement.CustCompID = row["CUST_COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["CUST_COMP_ID"]) : 0;

                    requirement.IsQuotationPriceLimit = row["IS_QUOTATION_PRICE_LIMIT"] != DBNull.Value ? (Convert.ToInt32(row["IS_QUOTATION_PRICE_LIMIT"]) == 1 ? true : false) : false;
                    requirement.QuotationPriceLimit = row["QUOTATION_PRICE_LIMIT"] != DBNull.Value ? Convert.ToInt32(row["QUOTATION_PRICE_LIMIT"]) : 0;
                    requirement.NoOfQuotationReminders = row["NO_OF_QUOTATION_REMINDERS"] != DBNull.Value ? Convert.ToInt32(row["NO_OF_QUOTATION_REMINDERS"]) : 0;
                    requirement.RemindersTimeInterval = row["REMINDERS_TIME_INTERVAL"] != DBNull.Value ? Convert.ToInt32(row["REMINDERS_TIME_INTERVAL"]) : 0;

                    requirement.SuperUser = new User();
                    requirement.PostedUser = new User();
                    requirement.PostedUser.UserInfo = new UserInfo();

                    requirement.SuperUser.FirstName = row["SUPER_USER_NAME"] != DBNull.Value ? Convert.ToString(row["SUPER_USER_NAME"]) : string.Empty;
                    requirement.SuperUser.Email = row["SUPER_USER_EMAIL"] != DBNull.Value ? Convert.ToString(row["SUPER_USER_EMAIL"]) : string.Empty;
                    requirement.SuperUser.PhoneNum = row["SUPER_USER_PHONE"] != DBNull.Value ? Convert.ToString(row["SUPER_USER_PHONE"]) : string.Empty;

                    requirement.PostedUser.Email = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                    requirement.PostedUser.PhoneNum = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
                    requirement.PostedUser.FirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                    requirement.PostedUser.LastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                    requirement.PostedUser.UserInfo.Address = row["ADDRESS"] != DBNull.Value ? Convert.ToString(row["ADDRESS"]) : string.Empty;

                    requirement.IsUnitPriceBidding = row["IS_UNIT_PRICE_BIDDING"] != DBNull.Value ? Convert.ToInt32(row["IS_UNIT_PRICE_BIDDING"]) : 0;

                    requirement.MaterialDispachmentLink = row["MAT_DIS_LINK"] != DBNull.Value ? Convert.ToInt32(row["MAT_DIS_LINK"]) : 0;
                    requirement.MaterialReceivedLink = row["MR_LINK"] != DBNull.Value ? Convert.ToInt32(row["MR_LINK"]) : 0;

                    requirement.IndentID = row["INDENT_ID"] != DBNull.Value ? Convert.ToInt32(row["INDENT_ID"]) : 0;

                    requirement.IsDiscountQuotation = row["IS_DISCOUNT_QUOTATION"] != DBNull.Value ? Convert.ToInt32(row["IS_DISCOUNT_QUOTATION"]) : 0;
                    requirement.IsRevUnitDiscountEnable = row["IS_REV_UNIT_DISCOUNT_ENABLE"] != DBNull.Value ? Convert.ToInt32(row["IS_REV_UNIT_DISCOUNT_ENABLE"]) : 0;

                    requirement.ContactDetails = row["CONTACT_DETAILS"] != DBNull.Value ? Convert.ToString(row["CONTACT_DETAILS"]) : string.Empty;
                    requirement.GeneralTC = row["GENERAL_TC"] != DBNull.Value ? Convert.ToString(row["GENERAL_TC"]) : string.Empty;

                    requirement.ContractStartTime = row["CONT_START_TIME"] != DBNull.Value ? Convert.ToString(row["CONT_START_TIME"]) : string.Empty;

                    requirement.ContractEndTime = row["CONT_END_TIME"] != DBNull.Value ? Convert.ToString(row["CONT_END_TIME"]) : string.Empty;
                    requirement.IsContract = row["IS_CONTRACT"] != DBNull.Value ? (Convert.ToInt32(row["IS_CONTRACT"]) == 1 ? true : false) : false;

                    //#CB-0-2018-12-05
                    requirement.IS_CB_ENABLED = row["IS_CB_ENABLED"] != DBNull.Value ? (Convert.ToInt32(row["IS_CB_ENABLED"]) == 1 ? true : false) : false;

                    requirement.CB_END_TIME = row["CB_END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["CB_END_TIME"]) : DateTime.MinValue;
                    logger.Debug("TESTINGIN19");
                    requirement.CB_STOP_QUOTATIONS = row["CB_STOP_QUOTATIONS"] != DBNull.Value ? (Convert.ToInt32(row["CB_STOP_QUOTATIONS"]) == 1 ? true : false) : false;
                    requirement.IS_CB_COMPLETED = row["IS_CB_COMPLETED"] != DBNull.Value ? (Convert.ToInt32(row["IS_CB_COMPLETED"]) == 1 ? true : false) : false;

                    requirement.LAST_BID_ID = row["LAST_BID_ID"] != DBNull.Value ? Convert.ToInt32(row["LAST_BID_ID"]) : 0;

                    requirement.IS_CB_NO_REGRET = row["IS_CB_NO_REGRET"] != DBNull.Value ? (Convert.ToInt32(row["IS_CB_NO_REGRET"]) == 1 ? true : false) : false;

                    requirement.LotId = row["LOT_ID"] != DBNull.Value ? Convert.ToInt32(row["LOT_ID"]) : 0;
                    requirement.AuditVersion = row["AUDIT_VERSION"] != DBNull.Value ? Convert.ToInt32(row["AUDIT_VERSION"]) : 1;
                    requirement.AuditComments = row["AUDIT_COMMENTS"] != DBNull.Value ? Convert.ToString(row["AUDIT_COMMENTS"]) : string.Empty;

                    requirement.NegotiationSettings = NegotiationSettings;
                    DateTime now = DateTime.UtcNow;
                    requirement.DATETIME_NOW = now;
                    DateTime DATE_CB_END_TIME = Convert.ToDateTime(requirement.CB_END_TIME);
                    logger.Debug("TESTINGIN20");
                    long DIFF_CB_END_TIME = Convert.ToInt64((DATE_CB_END_TIME - now).TotalSeconds);

                    if (DIFF_CB_END_TIME > 0)
                    {
                        requirement.CB_TIME_LEFT = DIFF_CB_END_TIME;
                    }
                    else
                    {
                        requirement.CB_TIME_LEFT = 0;
                        requirement.CB_END_TIME = now;
                    }



                    if (requirement.EndTime != DateTime.MaxValue && requirement.EndTime > now && requirement.StartTime < now)
                    {
                        DateTime date = Convert.ToDateTime(requirement.EndTime);
                        logger.Debug("TESTINGIN21");
                        long diff = Convert.ToInt64((date - now).TotalSeconds);
                        requirement.TimeLeft = diff;
                        requirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString());
                    }
                    else if (requirement.StartTime != DateTime.MaxValue && requirement.StartTime > now)
                    {
                        DateTime date = Convert.ToDateTime(requirement.StartTime);
                        logger.Debug("TESTINGIN22");
                        long diff = Convert.ToInt64((date - now).TotalSeconds);
                        requirement.TimeLeft = diff;
                        requirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString());
                    }
                    else if (requirement.EndTime < now)
                    {
                        requirement.TimeLeft = -1;
                        if ((Status == Utilities.GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString()) || Status == Utilities.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString())) && requirement.EndTime < now)
                        {
                            requirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.NegotiationEnded.ToString());
                            EndNegotiation(requirement.RequirementID, requirement.CustomerID, sessionID);
                        }
                        else
                        {
                            requirement.Status = Status;
                        }
                        //if (isPOSent == 0 && requirement.Status != GetEnumDesc<PRMStatus>(PRMStatus.POGenerated.ToString()))
                        //{
                        //    requirement.Status = "CLOSED";
                        //}
                    }
                    else if (requirement.StartTime == DateTime.MaxValue)
                    {
                        requirement.TimeLeft = -1;
                        requirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.UNCONFIRMED.ToString());
                    }
                    if (Status == Utilities.GetEnumDesc<PRMStatus>(PRMStatus.DELETED.ToString()))
                    {
                        requirement.TimeLeft = -1;
                        requirement.Status = Status;
                    }


                    if (DIFF_CB_END_TIME > 0)
                    {
                        requirement.CB_TIME_LEFT = DIFF_CB_END_TIME;
                        requirement.TimeLeft = DIFF_CB_END_TIME;
                    }
                    else
                    {
                        requirement.CB_TIME_LEFT = 0;
                        requirement.CB_END_TIME = now;
                    }


                    List<VendorDetails> vendorDetails = new List<VendorDetails>();
                    int rank = 1;
                    bool isUserVendor = false;
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        var companyCurrencies = new List<CurrencyFactors>();
                        if (requirement.CustomerCompanyId > 0)
                        {
                            companyCurrencies = this.GetCurrencyFactors(requirement.CustomerCompanyId);
                            if (companyCurrencies == null)
                            {
                                companyCurrencies = new List<CurrencyFactors>();
                            }
                        }

                        foreach (DataRow row1 in ds.Tables[1].Rows)
                        {
                            VendorDetails vendor = new VendorDetails();

                            vendor.PO = new RequirementPO();

                            vendor.ListCurrencies = new List<KeyValuePair>();

                            vendor.VendorID = row1["U_ID"] != DBNull.Value ? Convert.ToInt32(row1["U_ID"]) : 0;
                            vendor.compID = row1["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row1["COMP_ID"]) : 0;
                            vendor.IS_QUOT_LATE = row1["IS_QUOT_LATE"] != DBNull.Value ? Convert.ToInt32(row1["IS_QUOT_LATE"]) : 0;
                            vendor.VendorName = row1["VENDOR_NAME"] != DBNull.Value ? Convert.ToString(row1["VENDOR_NAME"]) : string.Empty;
                            vendor.InitialPrice = row1["VEND_INIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["VEND_INIT_PRICE"]) : 0;
                            vendor.RunningPrice = row1["VEND_RUN_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["VEND_RUN_PRICE"]) : 0;
                            vendor.TotalInitialPrice = row1["VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["VEND_TOTAL_PRICE"]) : 0;
                            vendor.TotalRunningPrice = row1["VEND_TOTAL_PRICE_RUNNING"] != DBNull.Value ? Convert.ToDouble(row1["VEND_TOTAL_PRICE_RUNNING"]) : 0;
                            vendor.City = row1["UAD_CITY"] != DBNull.Value ? Convert.ToString(row1["UAD_CITY"]) : string.Empty;
                            vendor.Taxes = row1["TAXES"] != DBNull.Value ? Convert.ToDouble(row1["TAXES"]) : 0;
                            vendor.Rating = row1["RATING"] != DBNull.Value ? Convert.ToDouble(row1["RATING"]) : 0;
                            //vendor.DeliveryTime = row1["DELIVERY_TIME"] != DBNull.Value ? Convert.ToString(row1["DELIVERY_TIME"]) : string.Empty;

                            vendor.Taxes = row1["VEND_TAXES"] != DBNull.Value ? Convert.ToInt32(row1["VEND_TAXES"]) : 0;
                            vendor.CompanyName = row1["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row1["COMPANY_NAME"]) : string.Empty;

                            //vendor.InitialPriceWithOutTaxFreight = row1["VEND_INIT_PRICE_WITHOUT_TAX"] != DBNull.Value ? Convert.ToDouble(row1["VEND_INIT_PRICE_WITHOUT_TAX"]) : 0;
                            //vendor.VendorFreight = row1["VEND_FREIGHT"] != DBNull.Value ? Convert.ToDouble(row1["VEND_FREIGHT"]) : 0;
                            vendor.TechnicalScore = row1["TECHNICAL_SCORE"] != DBNull.Value ? Convert.ToDecimal(row1["TECHNICAL_SCORE"]) : 0;
                            vendor.Warranty = row1["WARRANTY"] != DBNull.Value ? Convert.ToString(row1["WARRANTY"]) : string.Empty;
                            vendor.Payment = row1["PAYMENT"] != DBNull.Value ? Convert.ToString(row1["PAYMENT"]) : string.Empty;
                            vendor.Duration = row1["DURATION"] != DBNull.Value ? Convert.ToString(row1["DURATION"]) : string.Empty;
                            vendor.Validity = row1["VALIDITY"] != DBNull.Value ? Convert.ToString(row1["VALIDITY"]) : string.Empty;

                            vendor.OtherProperties = row1["OTHER_PROPERTIES"] != DBNull.Value ? Convert.ToString(row1["OTHER_PROPERTIES"]) : string.Empty;

                            vendor.IsQuotationRejected = row1["IS_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToInt32(row1["IS_QUOTATION_REJECTED"]) : 0;
                            vendor.QuotationRejectedComment = row1["QUOTATION_REJECTED_REASON"] != DBNull.Value ? Convert.ToString(row1["QUOTATION_REJECTED_REASON"]) : string.Empty;
                            //vendor.PO = GetPODetails(reqID, requirement.CustomerID, vendor.VendorID, sessionID);
                            vendor.PO = new RequirementPO();

                            vendor.PO.POLink = row1["PO_ATT_ID"] != DBNull.Value ? Convert.ToString(row1["PO_ATT_ID"]) : "0";

                            string fileName1 = row1["QUOTATION_URL"] != DBNull.Value ? Convert.ToString(row1["QUOTATION_URL"]) : string.Empty;
                            string URL1 = fileName1 != string.Empty ? fileName1 : string.Empty;
                            vendor.QuotationUrl = URL1;

                            string fileName2 = row1["REV_QUOTATION_URL"] != DBNull.Value ? Convert.ToString(row1["REV_QUOTATION_URL"]) : string.Empty;
                            string URL2 = fileName2 != string.Empty ? fileName2 : string.Empty;
                            vendor.RevQuotationUrl = URL2;

                            //  REV_QUOTATION_URL, REV_PRICE, REV_TAX, REV_VEND_FREIGHT, REV_VEND_TOTAL_PRICE

                            vendor.RevPrice = row1["REV_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["REV_PRICE"]) : 0;
                            //vendor.RevVendorFreight = row1["REV_VEND_FREIGHT"] != DBNull.Value ? Convert.ToDouble(row1["REV_VEND_FREIGHT"]) : 0;
                            vendor.RevVendorTotalPrice = row1["REV_VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["REV_VEND_TOTAL_PRICE"]) : 0;
                            vendor.RevVendorTotalPriceNoTax = row1["REV_VEND_TOTAL_PRICE_NO_TAX"] != DBNull.Value ? Convert.ToDouble(row1["REV_VEND_TOTAL_PRICE_NO_TAX"]) : 0;

                            vendor.IsRevQuotationRejected = row1["IS_REV_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToInt32(row1["IS_REV_QUOTATION_REJECTED"]) : 0;
                            vendor.RevQuotationRejectedComment = row1["REV_QUOTATION_REJECTED_REASON"] != DBNull.Value ? Convert.ToString(row1["REV_QUOTATION_REJECTED_REASON"]) : string.Empty;
                            //vendor.LastActiveTime = row1["LAST_ACTIVE_TIME"] != DBNull.Value ? Convert.ToString(row1["LAST_ACTIVE_TIME"]) : string.Empty;
                            //vendor.LastActiveTime = GetLastActiveTime(sessionID, vendor.VendorID);
                            vendor.LastActiveTime = row1["LAST_ACTIVE_TIME"] != DBNull.Value ? Convert.ToString(row1["LAST_ACTIVE_TIME"]) : string.Empty;
                            vendor.Discount = row1["DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row1["DISCOUNT"]) : 0;
                            vendor.ReductionPercentage = row1["REDUCTION_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row1["REDUCTION_PERCENTAGE"]) : 0;

                            vendor.Vendor = new User();

                            vendor.Vendor.Email = row1["U_EMAIL"] != DBNull.Value ? Convert.ToString(row1["U_EMAIL"]) : string.Empty;
                            vendor.Vendor.PhoneNum = row1["U_PHONE"] != DBNull.Value ? Convert.ToString(row1["U_PHONE"]) : string.Empty;

                            vendor.PO.MaterialDispachmentLink = row1["MAT_DIS_LINK"] != DBNull.Value ? Convert.ToInt32(row1["MAT_DIS_LINK"]) : 0;
                            vendor.PO.MaterialReceivedLink = row1["MR_LINK"] != DBNull.Value ? Convert.ToInt32(row1["MR_LINK"]) : 0;

                            vendor.LandingPrice = row1["LANDING_PRICE"] != DBNull.Value ? Convert.ToDecimal(row1["LANDING_PRICE"]) : 0;
                            vendor.RevLandingPrice = row1["REV_LANDING_PRICE"] != DBNull.Value ? Convert.ToDecimal(row1["REV_LANDING_PRICE"]) : 0;
                            vendor.SumOfMargin = row1["PARAM_SUM_MARGIN"] != DBNull.Value ? Convert.ToDecimal(row1["PARAM_SUM_MARGIN"]) : 0;
                            vendor.SumOfInitialMargin = row1["PARAM_SUM_INITIAL_MARGIN"] != DBNull.Value ? Convert.ToDecimal(row1["PARAM_SUM_INITIAL_MARGIN"]) : 0;
                            vendor.MultipleAttachments = row1["MULTIPLE_ATTACHMENTS"] != DBNull.Value ? Convert.ToString(row1["MULTIPLE_ATTACHMENTS"]) : string.Empty;
                            vendor.GstNumber = row1["GST_NUMBER"] != DBNull.Value ? Convert.ToString(row1["GST_NUMBER"]) : string.Empty;
                            //vendor.PackingCharges = row1["PACKING_CHARGES"] != DBNull.Value ? Convert.ToDouble(row1["PACKING_CHARGES"]) : 0;
                            //vendor.PackingChargesTaxPercentage = row1["PACKING_CHARGES_TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row1["PACKING_CHARGES_TAX_PERCENTAGE"]) : 0;
                            //vendor.PackingChargesWithTax = row1["PACKING_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row1["PACKING_CHARGES_WITH_TAX"]) : 0;
                            //vendor.InstallationCharges = row1["INSTALLATION_CHARGES"] != DBNull.Value ? Convert.ToDouble(row1["INSTALLATION_CHARGES"]) : 0;
                            //vendor.InstallationChargesTaxPercentage = row1["INSTALLATION_CHARGES_TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row1["INSTALLATION_CHARGES_TAX_PERCENTAGE"]) : 0;
                            //vendor.InstallationChargesWithTax = row1["INSTALLATION_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row1["INSTALLATION_CHARGES_WITH_TAX"]) : 0;
                            //vendor.RevpackingCharges = row1["REV_PACKING_CHARGES"] != DBNull.Value ? Convert.ToDouble(row1["REV_PACKING_CHARGES"]) : 0;
                            //vendor.RevinstallationCharges = row1["REV_INSTALLATION_CHARGES"] != DBNull.Value ? Convert.ToDouble(row1["REV_INSTALLATION_CHARGES"]) : 0;
                            vendor.VendorCurrency = row1["CURR_NAME"] != DBNull.Value ? Convert.ToString(row1["CURR_NAME"]) : string.Empty;
                            vendor.SelectedVendorCurrency = row1["SELECTED_VENDOR_CURRENCY"] != DBNull.Value ? Convert.ToString(row1["SELECTED_VENDOR_CURRENCY"]) : string.Empty;
                            vendor.VendorCurrencyFactor = row1["CURRENCY_FACTOR"] != DBNull.Value ? Convert.ToDecimal(row1["CURRENCY_FACTOR"]) : 1;
                            // vendor.IsCurrency = row1["IS_CURRENCY"] != DBNull.Value ? (Convert.ToInt32(row1["IS_CURRENCY"]) == 1 ? true : false) : false;
                            //vendor.RevinstallationChargesWithTax = row1["REV_INSTALLATION_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row1["REV_INSTALLATION_CHARGES_WITH_TAX"]) : 0;
                            //vendor.RevpackingChargesWithTax = row1["REV_PACKING_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row1["REV_PACKING_CHARGES_WITH_TAX"]) : 0;
                            // vendor.IsCurrency = row1["IS_CURRENCY"] != DBNull.Value ? (Convert.ToInt32(row1["IS_CURRENCY"]) == 1 ? true : false) : false;

                            //vendor.FREIGHT_CHARGES = row1["FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row1["FREIGHT_CHARGES"]) : 0;
                            //vendor.FREIGHT_CHARGES_TAX_PERCENTAGE = row1["FREIGHT_CHARGES_TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row1["FREIGHT_CHARGES_TAX_PERCENTAGE"]) : 0;
                            //vendor.FREIGHT_CHARGES_WITH_TAX = row1["FREIGHT_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row1["FREIGHT_CHARGES_WITH_TAX"]) : 0;
                            //vendor.REV_FREIGHT_CHARGES = row1["REV_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row1["REV_FREIGHT_CHARGES"]) : 0;
                            //vendor.REV_FREIGHT_CHARGES_WITH_TAX = row1["REV_FREIGHT_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row1["REV_FREIGHT_CHARGES_WITH_TAX"]) : 0;

                            //#CB-0-2018-12-05
                            vendor.RevPriceCB = row1["REV_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row1["REV_PRICE_CB"]) : 0;
                            vendor.RevVendorTotalPriceCB = row1["REV_VEND_TOTAL_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row1["REV_VEND_TOTAL_PRICE_CB"]) : 0;
                            vendor.TotalRunningPriceCB = row1["VEND_TOTAL_PRICE_RUNNING_CB"] != DBNull.Value ? Convert.ToDouble(row1["VEND_TOTAL_PRICE_RUNNING_CB"]) : 0;
                            //vendor.RevpackingChargesCB = row1["REV_PACKING_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row1["REV_PACKING_CHARGES_CB"]) : 0;
                            //vendor.RevinstallationChargesCB = row1["REV_INSTALLATION_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row1["REV_INSTALLATION_CHARGES_CB"]) : 0;
                            //vendor.VendorFreightCB = row1["VEND_FREIGHT_CB"] != DBNull.Value ? Convert.ToDouble(row1["VEND_FREIGHT_CB"]) : 0;
                            //vendor.RevVendorFreightCB = row1["REV_VEND_FREIGHT_CB"] != DBNull.Value ? Convert.ToDouble(row1["REV_VEND_FREIGHT_CB"]) : 0;
                            //vendor.REV_FREIGHT_CHARGES_CB = row1["REV_FREIGHT_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row1["REV_FREIGHT_CHARGES_CB"]) : 0;

                            //#CB-0-2018-12-05
                            vendor.FREEZE_CB = row1["FREEZE_CB"] != DBNull.Value ? (Convert.ToInt32(row1["FREEZE_CB"]) == 1 ? true : false) : false;
                            vendor.VEND_FREEZE_CB = row1["VEND_FREEZE_CB"] != DBNull.Value ? (Convert.ToInt32(row1["VEND_FREEZE_CB"]) == 1 ? true : false) : false;
                            vendor.CB_BID_COMMENTS = row1["CB_BID_COMMENTS"] != DBNull.Value ? Convert.ToString(row1["CB_BID_COMMENTS"]) : string.Empty;

                            vendor.INCO_TERMS = row1["INCO_TERMS"] != DBNull.Value ? Convert.ToString(row1["INCO_TERMS"]) : string.Empty;

                            vendor.LandingPrice = Math.Round(vendor.LandingPrice, decimal_round);
                            vendor.RevLandingPrice = Math.Round(vendor.RevLandingPrice, decimal_round);
                            //vendor.LastActiveTime = GetLastActiveTime(sessionID, vendor.VendorID);
                            vendor.LastActiveTime = row1["LAST_ACTIVE_TIME"] != DBNull.Value ? Convert.ToString(row1["LAST_ACTIVE_TIME"]) : string.Empty;
                            vendor.ReductionPercentage = Math.Round(vendor.ReductionPercentage, decimal_round);
                            vendor.DIFFERENTIAL_FACTOR = row1["DIFFERENTIAL_FACTOR"] != DBNull.Value ? Convert.ToDecimal(row1["DIFFERENTIAL_FACTOR"]) : 0;
                            vendor.DF_REV_VENDOR_TOTAL_PRICE = Convert.ToDecimal(vendor.RevVendorTotalPrice) + vendor.DIFFERENTIAL_FACTOR;
                            vendor.DF_REV_VENDOR_TOTAL_PRICE_NO_TAX = Convert.ToDecimal(vendor.RevVendorTotalPriceNoTax) + vendor.DIFFERENTIAL_FACTOR;
                            logger.Debug("TESTINGIN24.1");
                            vendor.AuctionModifiedDate = row1["AUCTION_MODIFIED_DATE"] != DBNull.Value ? Convert.ToDateTime(row1["AUCTION_MODIFIED_DATE"]) : DateTime.MaxValue;
                            vendor.SurrogateComments = row1["SURROGATE_COMMENTS"] != DBNull.Value ? Convert.ToString(row1["SURROGATE_COMMENTS"]) : string.Empty;
                            logger.Debug("TESTINGIN24");
                            KeyValuePair reqCurrency = new KeyValuePair();
                            reqCurrency.Value = requirement.Currency;
                            reqCurrency.Key1 = "REQ_CURRENCY";

                            KeyValuePair vendorCurrency = new KeyValuePair();
                            vendorCurrency.Value = vendor.VendorCurrency;
                            vendorCurrency.Key1 = "VENDOR_CURRENCY";

                            vendor.ListCurrencies.Add(reqCurrency);

                            if (reqCurrency.Value == vendorCurrency.Value)
                            {
                                //vendor.SelectedVendorCurrency = reqCurrency.Value;
                            }
                            else
                            {
                                vendor.ListCurrencies.Add(vendorCurrency);
                            }

                            if (companyCurrencies != null && companyCurrencies.Count > 0)
                            {
                                foreach (var currency in companyCurrencies)
                                {
                                    if (!vendor.ListCurrencies.Any(c => c.Value == currency.CurrencyCode))
                                    {
                                        vendor.ListCurrencies.Add(new KeyValuePair() { Value = currency.CurrencyCode, Key1 = "COMPANY_CURRENCY" });
                                    }
                                }
                            }

                            vendorDetails.Add(vendor);
                        }
                    }


                    if (requirement.IsDiscountQuotation == 2)
                    {

                        decimal maxInitialMargin = 0;
                        decimal maxClosedMargin = 0;

                        try
                        {
                            maxInitialMargin = vendorDetails.Where(vd => vd.CompanyName != "PRICE_CAP" && vd.CompanyName != "DEFAULT_VENDOR" && vd.IsQuotationRejected == 0).Max(vd => vd.SumOfInitialMargin);
                            maxClosedMargin = vendorDetails.Where(vd => vd.CompanyName != "PRICE_CAP" && vd.CompanyName != "DEFAULT_VENDOR" && vd.IsQuotationRejected == 0).Max(vd => vd.SumOfMargin);
                        }
                        catch
                        {
                            maxInitialMargin = 0;
                            maxClosedMargin = 0;
                        }


                        requirement.Savings = Convert.ToDouble(maxClosedMargin - maxInitialMargin);
                        requirement.Savings = Math.Round(requirement.Savings, decimal_round);
                    }



                    //requirement.AuctionVendors = vendorDetails;

                    requirement.CustomerReqAccess = false;

                    if (ds.Tables[5].Rows.Count > 0)
                    {
                        foreach (DataRow row5 in ds.Tables[5].Rows)
                        {
                            int reqCustomerId = row5["U_ID"] != DBNull.Value ? Convert.ToInt32(row5["U_ID"]) : 0;

                            if (userID == reqCustomerId)
                            {
                                requirement.CustomerReqAccess = true;
                            }
                        }
                    }

                    List<VendorDetails> v2 = new List<VendorDetails>();
                    if (requirement.StartTime > DateTime.UtcNow)
                    {
                        v2 = vendorDetails.Where(v => v.IsQuotationRejected == 0).Where(v => v.TotalInitialPrice > 0).OrderByDescending(v => ((requirement.InclusiveTax ? v.DF_REV_VENDOR_TOTAL_PRICE : v.DF_REV_VENDOR_TOTAL_PRICE_NO_TAX) * v.VendorCurrencyFactor)).ThenBy(p => p.AuctionModifiedDate).ToList();
                        v2.AddRange(vendorDetails.Where(v => v.IsQuotationRejected != 0).Where(v => v.TotalInitialPrice > 0).OrderByDescending(v => ((requirement.InclusiveTax ? v.DF_REV_VENDOR_TOTAL_PRICE : v.DF_REV_VENDOR_TOTAL_PRICE_NO_TAX) * v.VendorCurrencyFactor)).ThenBy(p => p.AuctionModifiedDate).ToList());
                        v2.AddRange(vendorDetails.Where(v => v.InitialPrice == 0).ToList());
                    }
                    else
                    {
                        v2 = vendorDetails.Where(v => v.IsQuotationRejected == 0).Where(v => v.TotalInitialPrice > 0).OrderByDescending(v => ((requirement.InclusiveTax ? v.DF_REV_VENDOR_TOTAL_PRICE : v.DF_REV_VENDOR_TOTAL_PRICE_NO_TAX) * v.VendorCurrencyFactor)).ThenBy(p => p.AuctionModifiedDate).ToList();
                        v2.AddRange(vendorDetails.Where(v => v.IsQuotationRejected != 0).Where(v => v.TotalInitialPrice > 0).OrderByDescending(v => ((requirement.InclusiveTax ? v.DF_REV_VENDOR_TOTAL_PRICE : v.DF_REV_VENDOR_TOTAL_PRICE_NO_TAX) * v.VendorCurrencyFactor)).ThenBy(p => p.AuctionModifiedDate).ToList());
                        v2.AddRange(vendorDetails.Where(v => v.InitialPrice == 0).ToList());
                    }
                    if (v2.Count > 0)
                    {
                        VendorDetails vendor1 = v2[0];
                        if (vendor1.TotalRunningPrice == 0)
                        {
                            requirement.Price = vendor1.TotalInitialPrice;
                        }
                        else if (vendor1.TotalInitialPrice < vendor1.TotalRunningPrice && vendor1.TotalInitialPrice != 0)
                        {
                            requirement.Price = vendor1.TotalInitialPrice;
                        }
                        else
                        {
                            requirement.Price = vendor1.TotalRunningPrice;
                        }
                    }

                    List<VendorDetails> tv2 = v2.Where(v => v.IsQuotationRejected == 0).OrderByDescending(v => DoInclusiveTax(v, requirement.InclusiveTax)).ThenBy(p => p.AuctionModifiedDate).ToList();
                    foreach (VendorDetails vendor in tv2.Where(p => p.DF_REV_VENDOR_TOTAL_PRICE > 0))
                    {
                        vendor.Rank = v2.IndexOf(vendor) + 1;
                    }
                    List<VendorDetails> notApprovedVendors = v2.Where(v => v.IsQuotationRejected != 0).ToList();
                    foreach (VendorDetails vendor in notApprovedVendors)
                    {
                        tv2.Add(vendor);
                    }
                    v2 = tv2;
                    if (tv2 != null && tv2.Count > 0) { } else { tv2 = v2; }
                    if (requirement.CustomerID == userID || requirement.SuperUserID == userID || requirement.CustomerReqAccess == true)
                    {
                        requirement.AuctionVendors = tv2.OrderBy(x => x.Rank == 0).ThenBy(x => x.Rank).ToList();
                    }
                    else
                    {
                        requirement.AuctionVendors = v2.Where(v => v.VendorID == userID).OrderByDescending(v => DoInclusiveTax(v, requirement.InclusiveTax)).ThenBy(p => p.AuctionModifiedDate).ToList();
                    }

                    requirement.SelectedVendor = requirement.AuctionVendors.FirstOrDefault(v => v.VendorID == requirement.SelectedVendorID);
                    int productSNo = 0;
                    string CATEGORY_ID = string.Empty;
                    List<RequirementItems> ListRequirementItems = new List<RequirementItems>();
                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        foreach (DataRow row2 in ds.Tables[2].Rows)
                        {
                            RequirementItems RequirementItems = new RequirementItems();
                            RequirementItems.ProductSNo = productSNo++;
                            RequirementItems.ItemID = row2["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_ID"]) : 0;
                            RequirementItems.RequirementID = row2["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row2["REQ_ID"]) : 0;
                            RequirementItems.ProductIDorName = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                            RequirementItems.ProductNo = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                            RequirementItems.ProductDescription = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                            RequirementItems.ProductQuantity = row2["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row2["QUANTITY"]) : 0;
                            RequirementItems.ProductBrand = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;
                            RequirementItems.OthersBrands = row2["OTHER_BRAND"] != DBNull.Value ? Convert.ToString(row2["OTHER_BRAND"]) : string.Empty;
                            RequirementItems.ProductImageID = row2["IMAGE_ID"] != DBNull.Value ? Convert.ToInt32(row2["IMAGE_ID"]) : 0;
                            RequirementItems.IsDeleted = row2["IS_DELETED"] != DBNull.Value ? Convert.ToInt32(row2["IS_DELETED"]) : 0;
                            RequirementItems.ProductQuantityIn = row2["QUANTITY_IN"] != DBNull.Value ? Convert.ToString(row2["QUANTITY_IN"]) : string.Empty;
                            RequirementItems.SelectedVendorID = row2["SELECTED_VENDOR_ID"] != DBNull.Value ? Convert.ToInt32(row2["SELECTED_VENDOR_ID"]) : 0;

                            RequirementItems.ProductIDorNameCustomer = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                            RequirementItems.ProductNoCustomer = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                            RequirementItems.ProductDescriptionCustomer = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                            RequirementItems.ProductBrandCustomer = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;
                            RequirementItems.ItemMinReduction = row2["ITEM_MIN_REDUCTION"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_MIN_REDUCTION"]) : 0;
                            RequirementItems.ItemLastPrice = row2["LAST_ITEM_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["LAST_ITEM_PRICE"]) : 0;
                            RequirementItems.ITEM_LEVEL_LEAST_PRICE = row2["ITEM_LEVEL_LEAST_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_LEVEL_LEAST_PRICE"]) : 0;
                            RequirementItems.I_LLP_DETAILS = row2["I_LLP_DETAILS"] != DBNull.Value ? Convert.ToString(row2["I_LLP_DETAILS"]) : string.Empty;
                            RequirementItems.CatalogueItemID = row2["CATALOGUE_ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["CATALOGUE_ITEM_ID"]) : 0;
                            RequirementItems.ITEM_PR_NUMBER = row2["ITEM_PR_NUMBER"] != DBNull.Value ? Convert.ToString(row2["ITEM_PR_NUMBER"]) : string.Empty;
                            RequirementItems.IS_GMP_PR_ITEM = row2["IS_GMP_PR_ITEM"] != DBNull.Value ? Convert.ToInt32(row2["IS_GMP_PR_ITEM"]) : 0;
                            RequirementItems.ITEM_PLANTS = row2["ITEM_PLANTS"] != DBNull.Value ? Convert.ToString(row2["ITEM_PLANTS"]) : string.Empty;
                            RequirementItems.ITEM_PR_QUANTITY = row2["ITEM_PR_QUANTITY"] != DBNull.Value ? Convert.ToDecimal(row2["ITEM_PR_QUANTITY"]) : 0;
                            RequirementItems.PR_WBS_CODE = row2["PR_WBS_CODE"] != DBNull.Value ? Convert.ToString(row2["PR_WBS_CODE"]) : string.Empty;
                            RequirementItems.HsnCode = row2["HSN_CODE"] != DBNull.Value ? Convert.ToString(row2["HSN_CODE"]) : string.Empty;
                            RequirementItems.CeilingPrice = row2.Table.Columns.Contains("CEILING_PRICE") && row2["CEILING_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["CEILING_PRICE"]) : 0;
                            RequirementItems.HsnCodeCustomer = row2["HSN_CODE"] != DBNull.Value ? Convert.ToString(row2["HSN_CODE"]) : string.Empty;
                            RequirementItems.ProductDeliveryDetails = row2["DELIERY_DETAILS"] != DBNull.Value ? Convert.ToString(row2["DELIERY_DETAILS"]) : string.Empty;
                            RequirementItems.IsCoreProductCategory = row2["IS_CORE_CAT"] != DBNull.Value ? Convert.ToInt32(row2["IS_CORE_CAT"]) : 0;
                            CATEGORY_ID = row2["CATEGORY_ID"] != DBNull.Value ? Convert.ToString(row2["CATEGORY_ID"]) : string.Empty;
                            RequirementItems.CATEGORY_ID = CATEGORY_ID;

                            ListRequirementItems.Add(RequirementItems);
                        }
                        requirement.ItemSNoCount = productSNo;
                    }

                    if (ds.Tables[3].Rows.Count > 0)
                    {
                        foreach (DataRow row2 in ds.Tables[3].Rows)
                        {
                            int uID = row2["U_ID"] != DBNull.Value ? Convert.ToInt32(row2["U_ID"]) : 0;
                            int itemid = row2["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_ID"]) : 0;

                            double price = row2["PRICE"] != DBNull.Value ? Convert.ToDouble(row2["PRICE"]) : 0;
                            string name = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                            string no = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                            string des = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                            string brand = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;

                            double unitprice = row2["UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["UNIT_PRICE"]) : 0;
                            double revunitprice = row2["REV_UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["REV_UNIT_PRICE"]) : 0;

                            double revItemPrice = row2["REVICED_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["REVICED_PRICE"]) : 0;

                            double cgst = row2["C_GST"] != DBNull.Value ? Convert.ToDouble(row2["C_GST"]) : 0;
                            double sgst = row2["S_GST"] != DBNull.Value ? Convert.ToDouble(row2["S_GST"]) : 0;
                            double igst = row2["I_GST"] != DBNull.Value ? Convert.ToDouble(row2["I_GST"]) : 0;

                            double unitmrp = row2["UNIT_MRP"] != DBNull.Value ? Convert.ToDouble(row2["UNIT_MRP"]) : 0;
                            double unitdiscount = row2["UNIT_DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row2["UNIT_DISCOUNT"]) : 0;
                            double revunitdiscount = row2["REV_UNIT_DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row2["REV_UNIT_DISCOUNT"]) : 0;
                            string otherBrands = row2["OTHER_BRANDS"] != DBNull.Value ? Convert.ToString(row2["OTHER_BRANDS"]) : string.Empty;
                            string vendorUnits = row2["VENDOR_UNITS"] != DBNull.Value ? Convert.ToString(row2["VENDOR_UNITS"]) : string.Empty;
                            bool isRegret = row2["IS_REGRET"] != DBNull.Value ? (Convert.ToInt32(row2["IS_REGRET"]) > 0 ? true : false) : false;
                            string regretComments = row2["REGRET_COMMENTS"] != DBNull.Value ? Convert.ToString(row2["REGRET_COMMENTS"]) : string.Empty;
                            string itemLevelInitialComments = row2["ITEM_LEVEL_INITIALCOMMENT"] != DBNull.Value ? Convert.ToString(row2["ITEM_LEVEL_INITIALCOMMENT"]) : string.Empty;
                            string itemLevelRevComments = row2["ITEM_LEVEL_REVCOMMENT"] != DBNull.Value ? Convert.ToString(row2["ITEM_LEVEL_REVCOMMENT"]) : string.Empty;
                            double itemFreightCharges = row2["ITEM_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_FREIGHT_CHARGES"]) : 0;
                            double itemFreightTAX = row2["ITEM_FREIGHT_TAX"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_FREIGHT_TAX"]) : 0;
                            double itemRevFreightCharges = row2["ITEM_REV_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_REV_FREIGHT_CHARGES"]) : 0;

                            //#CB-0-2018-12-05
                            double revunitpriceCB = row2["REV_UNIT_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row2["REV_UNIT_PRICE_CB"]) : 0;
                            double revItemPriceCB = row2["REVICED_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row2["REVICED_PRICE_CB"]) : 0;
                            double revunitdiscountCB = row2["REV_UNIT_DISCOUNT_CB"] != DBNull.Value ? Convert.ToDouble(row2["REV_UNIT_DISCOUNT_CB"]) : 0;
                            double itemRevFreightChargesCB = row2["ITEM_REV_FREIGHT_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_REV_FREIGHT_CHARGES_CB"]) : 0;

                            double LAST_BID_REV_UNIT_PRICE = row2["LAST_BID_REV_UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["LAST_BID_REV_UNIT_PRICE"]) : 0;

                            string ProductQuotationTemplateJson = row2["ProductQuotationTemplateJson"] != DBNull.Value ? Convert.ToString(row2["ProductQuotationTemplateJson"]) : string.Empty;
                            string HsnCode = row2["HSN_CODE"] != DBNull.Value ? Convert.ToString(row2["HSN_CODE"]) : string.Empty;
                            int catalogueItemID = row2["CATALOGUE_ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["CATALOGUE_ITEM_ID"]) : 0;
                            int itemRank = row2["ITEM_RANK"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_RANK"]) : 0;
                            int isItemQuotationRejected = row2["IS_ITEM_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToInt32(row2["IS_ITEM_QUOTATION_REJECTED"]) : -1;
                            string itemQuotationRejectedComment = row2["ITEM_QUOTATION_REJECTED_COMMENT"] != DBNull.Value ? Convert.ToString(row2["ITEM_QUOTATION_REJECTED_COMMENT"]) : string.Empty;
                            int isCoreProductCategory = row2["IS_CORE_CAT"] != DBNull.Value ? Convert.ToInt32(row2["IS_CORE_CAT"]) : 0;
                            double ceilingPrice = row2["CEILING_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["CEILING_PRICE"]) : 0;

                            if (uID == userID)
                            {
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ItemPrice = price;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ProductIDorName = name;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ProductNo = no;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ProductDescription = des;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ProductBrand = brand;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().RevItemPrice = revItemPrice;

                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().UnitPrice = unitprice;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().RevUnitPrice = revunitprice;

                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().CGst = cgst;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().SGst = sgst;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().IGst = igst;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().Gst = cgst + sgst + igst;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().UnitMRP = unitmrp;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().UnitDiscount = unitdiscount;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().RevUnitDiscount = revunitdiscount;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().VendorUnits = vendorUnits;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().IsRegret = isRegret;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().RegretComments = regretComments;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ItemLevelInitialComments = itemLevelInitialComments;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ItemLevelRevComments = itemLevelRevComments;
                                //ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ItemFreightCharges = itemFreightCharges;
                                //ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ItemFreightTAX = itemFreightTAX;
                                //ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ItemRevFreightCharges = itemRevFreightCharges;

                                //#CB-0-2018-12-05
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().RevUnitPriceCB = revunitpriceCB;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().RevItemPriceCB = revItemPriceCB;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().RevUnitDiscountCB = revunitdiscountCB;
                                //ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ItemRevFreightChargesCB = itemRevFreightChargesCB;

                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().LAST_BID_REV_UNIT_PRICE = LAST_BID_REV_UNIT_PRICE;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().LAST_BID_REDUCTION_PRICE = revunitprice - LAST_BID_REV_UNIT_PRICE;

                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ProductQuotationTemplateJson = ProductQuotationTemplateJson;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().HsnCode = HsnCode;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().CatalogueItemID = catalogueItemID;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ItemRank = itemRank;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().IsItemQuotationRejected = isItemQuotationRejected;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ItemQuotationRejectedComment = itemQuotationRejectedComment;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().IsCoreProductCategory = isCoreProductCategory;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().CATEGORY_ID = CATEGORY_ID;
                                if (ceilingPrice > 0)
                                {
                                    ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().CeilingPrice = ceilingPrice;
                                }
                            }
                        }
                    }

                    requirement.ListRequirementItems = ListRequirementItems;
                    foreach (VendorDetails V in vendorDetails)
                    {
                        int productSNo1 = 0;

                        List<RequirementItems> ListRequirementItems1 = new List<RequirementItems>();
                        if (ds.Tables[2].Rows.Count > 0)
                        {
                            foreach (DataRow row2 in ds.Tables[2].Rows)
                            {
                                RequirementItems RequirementItems = new RequirementItems();
                                RequirementItems.ProductSNo = productSNo1++;
                                RequirementItems.ItemID = row2["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_ID"]) : 0;
                                RequirementItems.RequirementID = row2["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row2["REQ_ID"]) : 0;
                                RequirementItems.ProductIDorName = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                                RequirementItems.ProductNo = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                                RequirementItems.ProductDescription = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                                RequirementItems.ProductQuantity = row2["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row2["QUANTITY"]) : 0;
                                RequirementItems.ProductBrand = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;
                                RequirementItems.OthersBrands = row2["OTHER_BRAND"] != DBNull.Value ? Convert.ToString(row2["OTHER_BRAND"]) : string.Empty;
                                RequirementItems.ProductImageID = row2["IMAGE_ID"] != DBNull.Value ? Convert.ToInt32(row2["IMAGE_ID"]) : 0;
                                RequirementItems.IsDeleted = row2["IS_DELETED"] != DBNull.Value ? Convert.ToInt32(row2["IS_DELETED"]) : 0;
                                RequirementItems.ProductQuantityIn = row2["QUANTITY_IN"] != DBNull.Value ? Convert.ToString(row2["QUANTITY_IN"]) : string.Empty;
                                RequirementItems.SelectedVendorID = row2["SELECTED_VENDOR_ID"] != DBNull.Value ? Convert.ToInt32(row2["SELECTED_VENDOR_ID"]) : 0;

                                RequirementItems.ProductIDorNameCustomer = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                                RequirementItems.ProductNoCustomer = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                                RequirementItems.ProductDescriptionCustomer = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                                RequirementItems.ProductBrandCustomer = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;
                                RequirementItems.ItemMinReduction = row2["ITEM_MIN_REDUCTION"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_MIN_REDUCTION"]) : 0;
                                RequirementItems.ItemLastPrice = row2["LAST_ITEM_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["LAST_ITEM_PRICE"]) : 0;
                                RequirementItems.ITEM_LEVEL_LEAST_PRICE = row2["ITEM_LEVEL_LEAST_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_LEVEL_LEAST_PRICE"]) : 0;
                                RequirementItems.I_LLP_DETAILS = row2["I_LLP_DETAILS"] != DBNull.Value ? Convert.ToString(row2["I_LLP_DETAILS"]) : string.Empty;
                                RequirementItems.CatalogueItemID = row2["CATALOGUE_ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["CATALOGUE_ITEM_ID"]) : 0;
                                RequirementItems.ITEM_PR_NUMBER = row2["ITEM_PR_NUMBER"] != DBNull.Value ? Convert.ToString(row2["ITEM_PR_NUMBER"]) : string.Empty;
                                RequirementItems.IS_GMP_PR_ITEM = row2["IS_GMP_PR_ITEM"] != DBNull.Value ? Convert.ToInt32(row2["IS_GMP_PR_ITEM"]) : 0;
                                RequirementItems.ITEM_PLANTS = row2["ITEM_PLANTS"] != DBNull.Value ? Convert.ToString(row2["ITEM_PLANTS"]) : string.Empty;
                                RequirementItems.ITEM_PR_QUANTITY = row2["ITEM_PR_QUANTITY"] != DBNull.Value ? Convert.ToDecimal(row2["ITEM_PR_QUANTITY"]) : 0;
                                RequirementItems.PR_WBS_CODE = row2["PR_WBS_CODE"] != DBNull.Value ? Convert.ToString(row2["PR_WBS_CODE"]) : string.Empty;
                                RequirementItems.HsnCode = row2["HSN_CODE"] != DBNull.Value ? Convert.ToString(row2["HSN_CODE"]) : string.Empty;
                                RequirementItems.HsnCodeCustomer = row2["HSN_CODE"] != DBNull.Value ? Convert.ToString(row2["HSN_CODE"]) : string.Empty;
                                RequirementItems.IsCoreProductCategory = row2["IS_CORE_CAT"] != DBNull.Value ? Convert.ToInt32(row2["IS_CORE_CAT"]) : 0;
                                RequirementItems.ProductDeliveryDetails = row2["DELIERY_DETAILS"] != DBNull.Value ? Convert.ToString(row2["DELIERY_DETAILS"]) : string.Empty;
                                //V_CATEGORY_ID = row2["CATEGORY_ID"] != DBNull.Value ? Convert.ToString(row2["CATEGORY_ID"]) : string.Empty;
                                //RequirementItems.CATEGORY_ID = V_CATEGORY_ID;
                                ListRequirementItems1.Add(RequirementItems);
                            }
                        }

                        if (ds.Tables[3].Rows.Count > 0)
                        {
                            foreach (DataRow row2 in ds.Tables[3].Rows)
                            {
                                int uID = row2["U_ID"] != DBNull.Value ? Convert.ToInt32(row2["U_ID"]) : 0;
                                int itemid = row2["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_ID"]) : 0;

                                double price = row2["PRICE"] != DBNull.Value ? Convert.ToDouble(row2["PRICE"]) : 0;
                                string name = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                                string no = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                                string des = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                                string brand = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;

                                double unitprice = row2["UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["UNIT_PRICE"]) : 0;
                                double revunitprice = row2["REV_UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["REV_UNIT_PRICE"]) : 0;

                                double revItemPrice = row2["REVICED_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["REVICED_PRICE"]) : 0;

                                double cgst = row2["C_GST"] != DBNull.Value ? Convert.ToDouble(row2["C_GST"]) : 0;
                                double sgst = row2["S_GST"] != DBNull.Value ? Convert.ToDouble(row2["S_GST"]) : 0;
                                double igst = row2["I_GST"] != DBNull.Value ? Convert.ToDouble(row2["I_GST"]) : 0;

                                double unitmrp = row2["UNIT_MRP"] != DBNull.Value ? Convert.ToDouble(row2["UNIT_MRP"]) : 0;
                                double unitdiscount = row2["UNIT_DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row2["UNIT_DISCOUNT"]) : 0;
                                double revunitdiscount = row2["REV_UNIT_DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row2["REV_UNIT_DISCOUNT"]) : 0;
                                string otherBrands = row2["OTHER_BRANDS"] != DBNull.Value ? Convert.ToString(row2["OTHER_BRANDS"]) : string.Empty;
                                string vendorUnits = row2["VENDOR_UNITS"] != DBNull.Value ? Convert.ToString(row2["VENDOR_UNITS"]) : string.Empty;
                                bool isRegret = row2["IS_REGRET"] != DBNull.Value ? (Convert.ToInt32(row2["IS_REGRET"]) > 0 ? true : false) : false;
                                string regretComments = row2["REGRET_COMMENTS"] != DBNull.Value ? Convert.ToString(row2["REGRET_COMMENTS"]) : string.Empty;
                                string itemLevelInitialComments = row2["ITEM_LEVEL_INITIALCOMMENT"] != DBNull.Value ? Convert.ToString(row2["ITEM_LEVEL_INITIALCOMMENT"]) : string.Empty;
                                string itemLevelRevComments = row2["ITEM_LEVEL_REVCOMMENT"] != DBNull.Value ? Convert.ToString(row2["ITEM_LEVEL_REVCOMMENT"]) : string.Empty;
                                double itemFreightCharges = row2["ITEM_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_FREIGHT_CHARGES"]) : 0;
                                double itemFreightTAX = row2["ITEM_FREIGHT_TAX"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_FREIGHT_TAX"]) : 0;
                                double itemRevFreightCharges = row2["ITEM_REV_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_REV_FREIGHT_CHARGES"]) : 0;

                                //#CB-0-2018-12-05
                                double revunitpriceCB = row2["REV_UNIT_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row2["REV_UNIT_PRICE_CB"]) : 0;
                                double revItemPriceCB = row2["REVICED_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row2["REVICED_PRICE_CB"]) : 0;
                                double revunitdiscountCB = row2["REV_UNIT_DISCOUNT_CB"] != DBNull.Value ? Convert.ToDouble(row2["REV_UNIT_DISCOUNT_CB"]) : 0;
                                double itemRevFreightChargesCB = row2["ITEM_REV_FREIGHT_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_REV_FREIGHT_CHARGES_CB"]) : 0;

                                double LAST_BID_REV_UNIT_PRICE = row2["LAST_BID_REV_UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["LAST_BID_REV_UNIT_PRICE"]) : 0;

                                string ProductQuotationTemplateJson = row2["ProductQuotationTemplateJson"] != DBNull.Value ? Convert.ToString(row2["ProductQuotationTemplateJson"]) : string.Empty;
                                string HsnCode = row2["HSN_CODE"] != DBNull.Value ? Convert.ToString(row2["HSN_CODE"]) : string.Empty;
                                int catalogueItemID = row2["CATALOGUE_ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["CATALOGUE_ITEM_ID"]) : 0;
                                int itemRank = row2["ITEM_RANK"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_RANK"]) : 0;
                                int isItemQuotationRejected = row2["IS_ITEM_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToInt32(row2["IS_ITEM_QUOTATION_REJECTED"]) : -1;
                                string itemQuotationRejectedComment = row2["ITEM_QUOTATION_REJECTED_COMMENT"] != DBNull.Value ? Convert.ToString(row2["ITEM_QUOTATION_REJECTED_COMMENT"]) : string.Empty;
                                int isCoreProductCategory = row2["IS_CORE_CAT"] != DBNull.Value ? Convert.ToInt32(row2["IS_CORE_CAT"]) : 0;
                                double ceilingPrice = row2["CEILING_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["CEILING_PRICE"]) : 0;
                                if (ceilingPrice <= 0)
                                {
                                    ceilingPrice = ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().CeilingPrice;
                                }

                                if (uID == V.VendorID)
                                {
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ItemPrice = price;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ProductIDorName = name;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ProductNo = no;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ProductDescription = des;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ProductBrand = brand;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().RevItemPrice = revItemPrice;

                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().UnitPrice = unitprice;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().RevUnitPrice = revunitprice;

                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().CGst = cgst;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().SGst = sgst;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().IGst = igst;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().Gst = cgst + sgst + igst;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().UnitMRP = unitmrp;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().UnitDiscount = unitdiscount;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().RevUnitDiscount = revunitdiscount;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().VendorUnits = vendorUnits;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().IsRegret = isRegret;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().RegretComments = regretComments;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ItemLevelInitialComments = itemLevelInitialComments;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ItemLevelRevComments = itemLevelRevComments;
                                    //ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ItemFreightCharges = itemFreightCharges;
                                    //ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ItemFreightTAX = itemFreightTAX;
                                    //ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ItemRevFreightCharges = itemRevFreightCharges;

                                    //#CB-0-2018-12-05
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().RevUnitPriceCB = revunitpriceCB;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().RevItemPriceCB = revItemPriceCB;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().RevUnitDiscountCB = revunitdiscountCB;
                                    //ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ItemRevFreightChargesCB = itemRevFreightChargesCB;

                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().LAST_BID_REV_UNIT_PRICE = LAST_BID_REV_UNIT_PRICE;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().LAST_BID_REDUCTION_PRICE = revunitprice - LAST_BID_REV_UNIT_PRICE;

                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ProductQuotationTemplateJson = ProductQuotationTemplateJson;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().HsnCode = HsnCode;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().CatalogueItemID = catalogueItemID;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ItemRank = itemRank;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().IsItemQuotationRejected = isItemQuotationRejected;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ItemQuotationRejectedComment = itemQuotationRejectedComment;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().IsCoreProductCategory = isCoreProductCategory;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().CeilingPrice = ceilingPrice;
                                    //ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().CATEGORY_ID = V_CATEGORY_ID;
                                }
                            }
                        }
                        V.ListRequirementItems = ListRequirementItems1;

                    }

                    if (requirement.IsDiscountQuotation == 2)
                    {
                    }

                    if (requirement.IsDiscountQuotation == 1 || requirement.IsDiscountQuotation == 0)
                    {
                        foreach (VendorDetails vendor in v2.Where(v => v.IsQuotationRejected == 0).OrderByDescending(v => DoInclusiveTax(v, requirement.InclusiveTax)).ThenBy(p => p.AuctionModifiedDate).ToList())
                        {
                            vendor.Rank = vendor.Rank;
                        }
                        if (requirement.CustomerID == userID || requirement.SuperUserID == userID || requirement.CustomerReqAccess == true)
                        {
                            requirement.AuctionVendors = v2.OrderBy(x => x.Rank == 0).ThenBy(x => x.Rank).ToList();
                        }
                        else
                        {
                            requirement.AuctionVendors = v2.Where(v => v.VendorID == userID).OrderByDescending(v => DoInclusiveTax(v, requirement.InclusiveTax)).ThenBy(p => p.AuctionModifiedDate).ToList();
                        }
                        tv2 = v2.Where(v => v.IsQuotationRejected == 0).OrderByDescending(v => DoInclusiveTax(v, requirement.InclusiveTax)).ThenBy(p => p.AuctionModifiedDate).ToList();
                    }
                    else
                    {
                        v2 = v2.OrderByDescending(v => v.SumOfMargin).ToList();

                        List<VendorDetails> approvedV2 = new List<VendorDetails>();

                        approvedV2 = v2.Where(v => v.IsQuotationRejected == 0 && v.SumOfMargin > 0).OrderByDescending(v => v.SumOfMargin * (v.VendorCurrencyFactor)).ThenBy(p => p.AuctionModifiedDate).ToList();

                        foreach (VendorDetails vendor in v2)
                        {

                            if (vendor.IsQuotationRejected == 0 && vendor.SumOfMargin > 0)
                            {
                                vendor.Rank = approvedV2.IndexOf(vendor) + 1;
                            }
                        }

                        if (requirement.CustomerID == userID || requirement.SuperUserID == userID || requirement.CustomerReqAccess == true)
                        {
                            v2 = v2.Where(v => v.IsQuotationRejected == 0).OrderBy(v => v.Rank).ToList();

                            foreach (VendorDetails vendor in requirement.AuctionVendors)
                            {
                                if (vendor.IsQuotationRejected != 0)
                                {
                                    v2.Add(vendor);
                                }
                            }
                        }
                        else
                        {
                            v2 = v2.Where(v => v.VendorID == userID).OrderByDescending(v => v.SumOfMargin).ToList();
                        }

                        requirement.AuctionVendors = v2;
                    }

                    int taxSNo = 0;

                    List<RequirementTaxes> ListRequirementTaxes = new List<RequirementTaxes>();
                    if (ds.Tables[4].Rows.Count > 0)
                    {
                        foreach (DataRow row4 in ds.Tables[4].Rows)
                        {
                            RequirementTaxes requirementTaxes = new RequirementTaxes();
                            requirementTaxes.TaxSNo = taxSNo++;
                            int uID = row4["U_ID"] != DBNull.Value ? Convert.ToInt32(row4["U_ID"]) : 0;
                            if (uID == userID || requirement.CustomerReqAccess == true)
                            {
                                requirementTaxes.TaxID = row4["QT_ID"] != DBNull.Value ? Convert.ToInt32(row4["QT_ID"]) : 0;
                                requirementTaxes.TaxName = row4["TAX_NAME"] != DBNull.Value ? Convert.ToString(row4["TAX_NAME"]) : string.Empty;
                                requirementTaxes.TaxPercentage = row4["TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row4["TAX_PERCENTAGE"]) : 0;
                                requirementTaxes.IsTaxDeleted = row4["IS_TAX_DELETED"] != DBNull.Value ? Convert.ToInt32(row4["IS_TAX_DELETED"]) : 0;

                                ListRequirementTaxes.Add(requirementTaxes);
                            }
                        }
                        requirement.TaxSNoCount = taxSNo;
                    }
                    requirement.ListRequirementTaxes = ListRequirementTaxes;


                    List<LotAuctions> lotAuctions = new List<LotAuctions>();
                    if (ds.Tables[6].Rows.Count > 0)
                    {
                        foreach (DataRow row6 in ds.Tables[6].Rows)
                        {
                            LotAuctions lotAuction = new LotAuctions();

                            lotAuction.ReqId = row6["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row6["REQ_ID"]) : 0;

                            lotAuction.ReqTitle = row6["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row6["REQ_TITLE"]) : string.Empty;


                            lotAuctions.Add(lotAuction);
                        }
                    }
                    requirement.LotAuctions = lotAuctions;

                }
            }
            catch (Exception ex)
            {
                logger.Error("ERROR", ex);
                Requirement req = new Requirement();
                req.ErrorMessage = ex.Message;
                return req;
            }

            return requirement;
        }

        public Response EndNegotiation(int reqID, int userID, string sessionID)
        {
            int CallerID = userID;
            Response response = new Response();
            string ScreenName = "END_NEGOTIATION";
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                Requirement req = GetRequirementDataOfflinePrivate(reqID, userID, sessionID);
                sd.Add("P_U_ID", userID);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("fwd_EndNegotiation", sd);
                Response response1 = new Response();
                response = Utilities.GetSessionId(req.CustomerID);
                response1 = response;

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    if (response.ObjectID > 0)
                    {
                        InvolvedParties involvedParties = new InvolvedParties();
                        involvedParties.RequirementID = req.RequirementID;
                        involvedParties.CustomerID = req.CustomerID;
                        involvedParties.SuperUserID = req.SuperUserID;
                        involvedParties.UserIDList = req.AuctionVendors.Where(x => x.VendorName != "PRICE CAP").Select(x => x.VendorID).ToArray();
                        int value = req.AuctionVendors.Where(a => a.VendorName != "PRICE CAP" && a.TotalInitialPrice > 0).OrderByDescending(a => Convert.ToDouble(DoInclusiveTax(a, req.InclusiveTax))).ThenBy(a => a.AuctionModifiedDate).Select(a => a.VendorID).FirstOrDefault();
                        involvedParties.UserRunngPriceList = req.AuctionVendors.Where(vend => vend.VendorID == value).Select(a => Convert.ToDouble(DoInclusiveTax(a, req.InclusiveTax))).ToArray();
                        involvedParties.VendorName = req.AuctionVendors.Where(vend => vend.VendorID == value).Select(x => x.VendorName).ToArray();
                        involvedParties.MethodName = "EndNegotiation";
                        involvedParties.CallerID = CallerID;
                        involvedParties.CustCompID = req.CustCompID;
                        req.Inv = involvedParties;
                        var context = GlobalHost.ConnectionManager.GetHubContext<FwdRequirementHub>();
                        context.Clients.Group(Utilities.GroupName + req.RequirementID + Utilities.UserIDComponent + req.CustomerID).checkRequirement(req);
                        context.Clients.Group(Utilities.GroupName + req.RequirementID + Utilities.UserIDComponent + req.SuperUserID).checkRequirement(req);
                        foreach (int vendorID in involvedParties.UserIDList)
                        {
                            req = GetRequirementData(reqID, vendorID, sessionID);
                            context.Clients.Group(Utilities.GroupName + req.RequirementID + Utilities.UserIDComponent + vendorID).checkRequirement(req);
                        }

                        Requirement requirement = GetRequirementDataOfflinePrivate(reqID, userID, sessionID);

                        UserInfo customer = GetUser(requirement.CustomerID);

                        int id1 = involvedParties.UserIDList[0];
                        double UserRunngPriceList = Math.Round(involvedParties.UserRunngPriceList[0], decimal_round);
                        string VendorName = involvedParties.VendorName[0];
                        double SavedAmount = Math.Round(requirement.Savings, decimal_round);
                        DateTime endTime = (DateTime)requirement.EndTime;

                        requirement.Module = ScreenName;

                        if (endTime.Date == DateTime.UtcNow.Date)
                        {
                            string body = GenerateEmailBody("CustomeremailForEndNegotiation");
                            body = String.Format(body, customer.FirstName, customer.LastName, requirement.Title, VendorName, UserRunngPriceList, SavedAmount, reqID);

                            SendEmail(customer.Email + "," + customer.AltEmail, "Req Number: " + requirement.RequirementNumber + " Title: " + requirement.Title + " - Negotiation is completed successfully", body, reqID, requirement.CustomerID, requirement.Module, sessionID).ConfigureAwait(false);

                            string body1 = GenerateEmailBody("CustomersmsForEndNegotiation");
                            body1 = String.Format(body1, customer.FirstName, customer.LastName, requirement.Title, VendorName, UserRunngPriceList, SavedAmount, reqID);
                            body1 = body1.Replace("<br/>", "");
                            UserInfo superUser = Utilities.GetSuperUser(requirement.CustomerID);

                            string bodySuper = GenerateEmailBody("CustomeremailForEndNegotiation");
                            bodySuper = String.Format(bodySuper, superUser.FirstName, superUser.LastName, requirement.Title, VendorName, UserRunngPriceList, SavedAmount, reqID);

                            string body2Super = GenerateEmailBody("CustomersmsForEndNegotiation");
                            body2Super = String.Format(body2Super, superUser.FirstName, superUser.LastName, requirement.Title, VendorName, UserRunngPriceList, SavedAmount, reqID);
                            body2Super = body2Super.Replace("<br/>", "");

                            string bodyTelegram = GenerateEmailBody("EndNegotiationTelegramsms");
                            bodyTelegram = String.Format(bodyTelegram, superUser.Institution, superUser.FirstName, superUser.LastName, requirement.Title, requirement.Description, SavedAmount);
                            bodyTelegram = bodyTelegram.Replace("<br/>", "");

                            TelegramMsg tgMsg = new TelegramMsg();
                            tgMsg.Message = "END NEGOTITATION: Req ID : " + reqID + (bodyTelegram.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]);

                            string subUserID = req.CustomerID.ToString();
                            foreach (int id in involvedParties.UserIDList)
                            {
                                UserInfo user = GetUser(id);

                                User altUser = GetReqAlternateCommunications(reqID, id);
                                if (id != id1)
                                {
                                    string body2 = GenerateEmailBody("VendoremailForEndNegotiation");
                                    body2 = String.Format(body2, user.FirstName, user.LastName, requirement.Title, reqID);

                                    SendEmail(user.Email + "," + user.AltEmail + "," + altUser.AltEmail, "Req Number: " + requirement.RequirementNumber + " Title: " + requirement.Title + " - Negotiation is completed successfully", body2, reqID, id, requirement.Module, response1.Message).ConfigureAwait(false);

                                    string body3 = GenerateEmailBody("VendorsmsForEndNegotiation");
                                    body3 = String.Format(body3, user.FirstName, user.LastName, requirement.Title, reqID);

                                    body3 = body3.Replace("<br/>", "");
                                }
                                else
                                {

                                    string body2 = GenerateEmailBody("Vendor1emailForEndNegotiation");
                                    body2 = String.Format(body2, user.FirstName, user.LastName, requirement.Title, UserRunngPriceList, reqID);
                                    SendEmail(user.Email + "," + user.AltEmail + "," + altUser.AltEmail, "Req Number: " + requirement.RequirementNumber + " Title: " + requirement.Title + " - Negotiation is completed successfully", body2, reqID, Convert.ToInt32(user.UserID), requirement.Module, response1.Message).ConfigureAwait(false);

                                    string body3 = GenerateEmailBody("Vendor1smsForEndNegotiation");
                                    body3 = String.Format(body3, user.FirstName, user.LastName, requirement.Title, UserRunngPriceList, reqID);
                                    body3 = body3.Replace("<br/>", "");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public DataSet GetRequirementDetailsHub(int reqID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_REQ_ID", reqID);
            sd.Add("P_EXCLUDE_PRICE_CAP", 0);
            DataSet ds = sqlHelper.SelectList("fwd_GetRequirementData", sd);
            return ds;
        }

        public Response RestartNegotiation(int reqID, int userID, string sessionID)
        {
            int CallerID = userID;
            string ScreenName = "RESTART_NEGOTIATION";
            Response response = new Response();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("fwd_AddTimeToNegotiation", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    List<UserInfo> users = GetUsersInvolved(reqID, "ALLVENDORS");
                    Requirement req1 = GetRequirementDataOffline(reqID, userID, sessionID);
                    foreach (UserInfo user in users)
                    {
                        User altUser = GetReqAlternateCommunications(reqID, Convert.ToInt32(user.UserID));
                        if (user.UserType == "VENDOR")
                        {
                            string body = GenerateEmailBody("NegotiationRestartEmail");
                            body = String.Format(body, user.FirstName, user.LastName, reqID, req1.Title);
                            SendEmail(user.Email + "," + user.AltEmail, "Negotiation has restarted for the Requirement Number" + req1.RequirementNumber, body, reqID, Convert.ToInt32(user.UserID), ScreenName, sessionID).ConfigureAwait(false);
                            string message = GenerateEmailBody("NegotiationRestartSms");
                            message = String.Format(message, user.FirstName, user.LastName, reqID, req1.StartTime, req1.Title);
                            message = message.Replace("<br/>", "");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response StopBids(int reqID, int userID, string sessionID)
        {
            Response response = new Response();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_U_ID", userID);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("fwd_StopBids", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response UpdateAuctionStart(int reqID, int userID, DateTime date, string sessionID, string negotiationDuration, NegotiationSettings NegotiationSettings)
        {

            string moreInfo = string.Empty;
            Response response = new Response();
            try
            {
                if (ConfigurationManager.AppSettings["DatabaseProvider"] == "MSSQL")
                {
                    NegotiationSettings.NegotiationDuration = NegotiationSettings.NegotiationDuration.Replace(".0", "").Replace("0 ", "").Trim();
                }

                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                Utilities.ValidateSession(sessionID);
                List<Attachment> vendorAttachments = new List<Attachment>();
                negotiationDuration = "0 0:35";
                negotiationDuration = negotiationDuration + ":00.0";
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_NEW_TIME", date);
                sd.Add("P_U_ID", userID);
                sd.Add("P_MIN_REDUCE_AMOUNT", NegotiationSettings.MinReductionAmount);
                sd.Add("P_VENDOR_COMPARISION_MIN_AMOUNT", NegotiationSettings.RankComparision);
                sd.Add("P_NEGOTIATION_DURATION", NegotiationSettings.NegotiationDuration);
                DataSet ds = sqlHelper.SelectList("fwd_UpdateAuctionStart", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = ds.Tables[0].Rows[0][1].ToString();
                    if (response.ErrorMessage != string.Empty)
                    {
                        response.TimeLeft = -1;
                    }
                    else
                    {
                        long diff = Convert.ToInt64((date - DateTime.UtcNow).TotalSeconds);
                        response.TimeLeft = diff;
                    }
                }

                Attachment PRM360_VENDOR_USER_MANUAL = null;
                Requirement requirement = GetRequirementData(reqID, userID, sessionID);
                string attchID = "2";
                if (requirement.IsDiscountQuotation == 2)
                {
                    attchID = "3";
                }

                string watchers = string.Empty;
                if (requirement.CustomerEmails != null && requirement.CustomerEmails.Count > 0)
                {
                    watchers = string.Join(";", requirement.CustomerEmails.Select(e => e.Mail));
                }

                PRMForwardNotifications prmNotifications = new PRMForwardNotifications();
                Task.Factory.StartNew(() => prmNotifications.AuctionStartNotification(reqID, userID, sessionID, vendorAttachments, watchers));
            }
            catch (Exception ex)
            {
                response.ErrorMessage = moreInfo + ex.Message + ex.StackTrace;
            }

            return response;
        }

        public RequirementLite GetRequirementDetails(int requirementId)
        {
            RequirementLite requirement = new RequirementLite();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_REQ_ID", requirementId);
            DataSet ds = sqlHelper.SelectList("fwd_GetRequirementDataLite", sd);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                CORE.DataNamesMapper<RequirementLite> mapper1 = new CORE.DataNamesMapper<RequirementLite>();
                requirement = mapper1.Map(ds.Tables[0]).FirstOrDefault();
            }

            if (ds != null && ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
            {
                CORE.DataNamesMapper<VendorDetailsLite> mapper1 = new CORE.DataNamesMapper<VendorDetailsLite>();
                requirement.AuctionVendors = new List<VendorDetailsLite>();
                requirement.AuctionVendors = mapper1.Map(ds.Tables[1]).ToList();
            }

            if (ds != null && ds.Tables.Count > 2 && ds.Tables[2].Rows.Count > 0)
            {
                CORE.DataNamesMapper<RequirementItemsLite> mapper1 = new CORE.DataNamesMapper<RequirementItemsLite>();
                requirement.RequirementItems = new List<RequirementItemsLite>();
                requirement.RequirementItems = mapper1.Map(ds.Tables[2]).ToList();
            }

            if (ds != null && ds.Tables.Count > 3 && ds.Tables[3].Rows.Count > 0)
            {
                CORE.DataNamesMapper<RequirementItemsLite> mapper1 = new CORE.DataNamesMapper<RequirementItemsLite>();
                var vendorItems = new List<RequirementItemsLite>();

                vendorItems = mapper1.Map(ds.Tables[3]).ToList();
                if (requirement.RequirementItems != null && requirement.RequirementItems.Count > 0)
                {
                    foreach (var vendorItem in vendorItems)
                    {
                        var requirementItem = requirement.RequirementItems.First(r => r.ITEM_ID == vendorItem.ITEM_ID);
                        vendorItem.ItemMinReduction = requirementItem != null ? requirementItem.ItemMinReduction : 0;
                        vendorItem.ItemLastPrice = requirementItem != null ? requirementItem.ItemLastPrice : 0;
                    }
                }

                foreach (var vendor in requirement.AuctionVendors)
                {
                    vendor.RequirementItems = new List<RequirementItemsLite>();
                    vendor.RequirementItems = vendorItems.Where(v => v.U_ID == vendor.VendorID).ToList();
                }
            }

            return requirement;
        }

        public List<Requirement> GetActiveLeads(int userID, string sessionID)
        {
            List<Requirement> myAuctions = new List<Requirement>();
            try
            {
                Utilities.ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("fwd_GetActiveLeads", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        Requirement requirement = new Requirement();
                        string[] arr = new string[] { };
                        byte[] next = new byte[] { };
                        requirement.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : -1;


                        requirement.CustomerID = userID;
                        requirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                        requirement.BiddingType = row["BIDDING_TYPE"] != DBNull.Value ? Convert.ToString(row["BIDDING_TYPE"]) : string.Empty;
                        requirement.EndTime = row["END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["END_TIME"]) : DateTime.MaxValue;
                        if (requirement.EndTime == null)
                        {
                            requirement.EndTime = DateTime.MaxValue;
                        }
                        requirement.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                        if (requirement.StartTime == null)
                        {
                            requirement.StartTime = DateTime.MaxValue;
                        }
                        requirement.PostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                        requirement.Price = row["VEND_MIN_PRICE"] != DBNull.Value ? Convert.ToDouble(row["VEND_MIN_PRICE"]) : 0;
                        double RunPrice = row["VEND_MIN_RUN_PRICE"] != DBNull.Value ? Convert.ToDouble(row["VEND_MIN_RUN_PRICE"]) : 0;
                        DateTime start = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                        if (start < DateTime.UtcNow && RunPrice > 0)
                        {
                            requirement.Price = RunPrice;
                        }
                        string POLink = row["PURCHASE_ORDER_LINK"] != DBNull.Value ? (row["PURCHASE_ORDER_LINK"].ToString()) : string.Empty;
                        requirement.Description = row["REQ_DESC"] != DBNull.Value ? Convert.ToString(row["REQ_DESC"]) : string.Empty;
                        requirement.Category = row["REQ_CATEGORY"] != DBNull.Value ? Convert.ToString(row["REQ_CATEGORY"]).Split(',') : arr;
                        requirement.Urgency = row["REQ_URGENCY"] != DBNull.Value ? Convert.ToString(row["REQ_URGENCY"]) : string.Empty;
                        requirement.Budget = row["REQ_BUDGET"] != DBNull.Value ? Convert.ToString(row["REQ_BUDGET"]) : string.Empty;
                        requirement.DeliveryLocation = row["REQ_DELIVERY_LOC"] != DBNull.Value ? Convert.ToString(row["REQ_DELIVERY_LOC"]) : string.Empty;
                        requirement.Taxes = row["REQ_TAXES"] != DBNull.Value ? Convert.ToString(row["REQ_TAXES"]) : string.Empty;
                        requirement.PaymentTerms = row["REQ_PAYMENT_TERMS"] != DBNull.Value ? Convert.ToString(row["REQ_PAYMENT_TERMS"]) : string.Empty;
                        requirement.Currency = row["REQ_CURRENCY"] != DBNull.Value ? Convert.ToString(row["REQ_CURRENCY"]) : string.Empty;
                        string Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                        //requirement.IsClosed = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                        requirement.CustomerID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                        requirement.SelectedVendorID = row["SELECTED_VENDOR_ID"] != DBNull.Value ? Convert.ToInt32(row["SELECTED_VENDOR_ID"]) : 0;
                        //requirement.Attachment = row["REQ_ATTACHMENT"] != DBNull.Value ? (byte[])row["REQ_ATTACHMENT"] : next;

                        requirement.QuotationFreezTime = row["QUOTATION_FREEZ_TIME"] != DBNull.Value ? Convert.ToDateTime(row["QUOTATION_FREEZ_TIME"]) : DateTime.MaxValue;
                        requirement.IsDiscountQuotation = row["IS_DISCOUNT_QUOTATION"] != DBNull.Value ? Convert.ToInt32(row["IS_DISCOUNT_QUOTATION"]) : 0;

                        requirement.CustomerCompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                        requirement.IsRevUnitDiscountEnable = row["IS_REV_UNIT_DISCOUNT_ENABLE"] != DBNull.Value ? Convert.ToInt32(row["IS_REV_UNIT_DISCOUNT_ENABLE"]) : 0;

                        requirement.IS_CB_ENABLED = row["IS_CB_ENABLED"] != DBNull.Value ? (Convert.ToInt32(row["IS_CB_ENABLED"]) == 1 ? true : false) : false;
                        requirement.CB_END_TIME = row["CB_END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["CB_END_TIME"]) : DateTime.UtcNow;
                        requirement.CB_STOP_QUOTATIONS = row["CB_STOP_QUOTATIONS"] != DBNull.Value ? (Convert.ToInt32(row["CB_STOP_QUOTATIONS"]) == 1 ? true : false) : false;
                        requirement.IS_CB_COMPLETED = row["IS_CB_COMPLETED"] != DBNull.Value ? (Convert.ToInt32(row["IS_CB_COMPLETED"]) == 1 ? true : false) : false;
                        requirement.IS_CB_NO_REGRET = row["IS_CB_NO_REGRET"] != DBNull.Value ? (Convert.ToInt32(row["IS_CB_NO_REGRET"]) == 1 ? true : false) : false;
                        requirement.RequirementNumber = row["REQ_NUMBER"] != DBNull.Value ? Convert.ToString(row["REQ_NUMBER"]) : string.Empty;
                        int isQuotationRejected = row["IS_DISCOUNT_QUOTATION"] != DBNull.Value ? Convert.ToInt32(row["IS_DISCOUNT_QUOTATION"]) : -1;

                        requirement.QuotationNotSubmitted = row["QUOTATION_URL"] != DBNull.Value ? Convert.ToString(row["QUOTATION_URL"]) : string.Empty;
                        requirement.PRNumbers = row["PR_NUMBERS"] != DBNull.Value ? Convert.ToString(row["PR_NUMBERS"]) : string.Empty;
                        var prStatus = row["REQ_PR_STATUS"] != DBNull.Value ? Convert.ToString(row["REQ_PR_STATUS"]) : string.Empty;
                        requirement.TotalCount = 0;

                        DateTime now = DateTime.UtcNow;
                        if (requirement.EndTime != DateTime.MaxValue && requirement.EndTime > now && requirement.StartTime < now)
                        {
                            DateTime date = Convert.ToDateTime(requirement.EndTime);
                            long diff = Convert.ToInt64((date - now).TotalSeconds);
                            requirement.TimeLeft = diff;
                            requirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString());

                        }
                        else if (requirement.StartTime != DateTime.MaxValue && requirement.StartTime > now)
                        {
                            DateTime date = Convert.ToDateTime(requirement.StartTime);
                            long diff = Convert.ToInt64((date - now).TotalSeconds);
                            requirement.TimeLeft = diff;
                            requirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString());
                        }
                        else if (requirement.EndTime < now)
                        {
                            requirement.TimeLeft = -1;
                            requirement.Status = Status;
                        }
                        else if (requirement.StartTime == DateTime.MaxValue)
                        {
                            requirement.TimeLeft = -1;
                            requirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.UNCONFIRMED.ToString());
                        }

                        if (requirement.IS_CB_ENABLED)
                        {
                            if (requirement.CB_END_TIME > now && isQuotationRejected == 0)
                            {
                                requirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString());
                            }
                        }

                        if (requirement.Status == "UNCONFIRMED" && requirement.QuotationNotSubmitted.Contains("QUOTATION_NOT_SUBMITTED"))
                        {
                            requirement.Status = "NEW";
                        }
                        else if (requirement.Status == "UNCONFIRMED" && !requirement.QuotationNotSubmitted.Contains("QUOTATION_NOT_SUBMITTED"))
                        {
                            requirement.Status = "WIP";
                        }

                        if (!string.IsNullOrEmpty(requirement.PRNumbers) && requirement.Status == "Negotiation Ended")
                        {
                            if (!string.IsNullOrEmpty(prStatus) && prStatus.Equals("CLOSED", StringComparison.InvariantCultureIgnoreCase))
                            {
                                requirement.Status = "Closed";
                            }
                        }

                        requirement.AuctionVendors = new List<VendorDetails>();
                        requirement.CustFirstName = string.Empty;
                        requirement.CustLastName = string.Empty;
                        //requirement.Status = string.Empty;
                        requirement.SessionID = string.Empty;
                        requirement.ErrorMessage = string.Empty;

                        myAuctions.Add(requirement);
                    }
                }

            }
            catch (Exception ex)
            {
                Requirement auction = new Requirement();
                auction.ErrorMessage = ex.Message;

                myAuctions.Add(auction);
            }

            return myAuctions;
        }

        public List<ConsolidatedReport> GetConsolidatedReports(string sessionID, string from, string to, int userID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<ConsolidatedReport> consolidatedReports = new List<ConsolidatedReport>();
            List<RequirementUsersByRank> revisedReport = new List<RequirementUsersByRank>();
            List<RequirementUsersByRank> initialReport = new List<RequirementUsersByRank>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                if (string.IsNullOrEmpty(from))
                {
                    //from = DateTime.MinValue.ToString();
                    from = DateTime.UtcNow.AddDays(-30).ToString();
                }

                if (string.IsNullOrEmpty(to))
                {
                    //to = DateTime.MaxValue.ToString();
                    to = DateTime.UtcNow.ToString();
                }

                sd.Add("P_FROM_DATE", from);
                sd.Add("P_TO_DATE", to);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("fwd_rp_GetConsolidatedReports", sd);

                #region Revised Details (Table 1)

                if (ds.Tables[1].Rows.Count > 0)
                {
                    foreach (DataRow row1 in ds.Tables[1].Rows)
                    {
                        RequirementUsersByRank revised = new RequirementUsersByRank();
                        revised.UID = row1["U_ID"] != DBNull.Value ? Convert.ToInt32(row1["U_ID"]) : 0;
                        revised.RequirementID = row1["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row1["REQ_ID"]) : 0;
                        revised.RevBasePrice = row1["REV_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["REV_BASE_PRICE"]) : 0;
                        revised.InitBasePrice = row1["INIT_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["INIT_BASE_PRICE"]) : 0;
                        revised.RevVendTotalPrice = row1["REV_VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["REV_VEND_TOTAL_PRICE"]) : 0;
                        revised.Rank = row1["RN"] != DBNull.Value ? Convert.ToInt32(row1["RN"]) : 0;
                        //revised.Quantity = row1["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row1["QUANTITY"]) : 0;
                        revised.CompanyName = row1["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row1["COMPANY_NAME"]) : string.Empty;
                        revised.SelectedVendorCurrency = row1["SELECTED_VENDOR_CURRENCY"] != DBNull.Value ? Convert.ToString(row1["SELECTED_VENDOR_CURRENCY"]) : string.Empty;

                        revisedReport.Add(revised);
                    }
                }

                #endregion Revised Details

                #region initial details (table 2)

                // list<initialdetails> initialdetails = new list<initialdetails>();

                if (ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow row2 in ds.Tables[2].Rows)
                    {
                        RequirementUsersByRank initial = new RequirementUsersByRank();

                        //U_ID, REQ_ID, REV_BASE_PRICE, INIT_BASE_PRICE, RN, QUANTITY, COMPANY_NAME
                        initial.UID = row2["U_ID"] != DBNull.Value ? Convert.ToInt32(row2["U_ID"]) : 0;
                        initial.RequirementID = row2["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row2["REQ_ID"]) : 0;
                        initial.RevBasePrice = row2["REV_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["REV_BASE_PRICE"]) : 0;
                        initial.InitBasePrice = row2["INIT_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["INIT_BASE_PRICE"]) : 0;
                        initial.VendTotalPrice = row2["VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["VEND_TOTAL_PRICE"]) : 0;
                        initial.Rank = row2["RN"] != DBNull.Value ? Convert.ToInt32(row2["RN"]) : 0;
                        initial.Quantity = row2["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row2["QUANTITY"]) : 0;
                        // initial.CompanyName = row2["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row2["COMPANY_NAME"]) : string.Empty;

                        initialReport.Add(initial);
                    }
                }

                #endregion requirement items

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {

                        ConsolidatedReport report = new ConsolidatedReport();
                        report.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                        report.RequirementNumber = row["REQ_NUMBER"] != DBNull.Value ? Convert.ToString(row["REQ_NUMBER"]) : string.Empty;
                        report.UID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                        report.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                        report.ReqPostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                        report.QuotationFreezTime = row["QUOTATION_FREEZ_TIME"] != DBNull.Value ? Convert.ToDateTime(row["QUOTATION_FREEZ_TIME"]) : DateTime.MaxValue;
                        report.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                        report.EndTime = row["END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["END_TIME"]) : DateTime.MaxValue;
                        report.IsNegotiationEnded = row["IS_NEGOTIATION_ENDED"] != DBNull.Value ? Convert.ToInt32(row["IS_NEGOTIATION_ENDED"]) : 0;
                        report.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                        report.Closed = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                        string Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                        report.NoOfVendorsInvited = row["NoOfVendorsInvited"] != DBNull.Value ? Convert.ToInt32(row["NoOfVendorsInvited"]) : 0;
                        report.NoOfvendorsParticipated = row["NoOfvendorsParticipated"] != DBNull.Value ? Convert.ToInt32(row["NoOfvendorsParticipated"]) : 0;
                        report.ReqCategory = row["REQ_CATEGORY"] != DBNull.Value ? Convert.ToString(row["REQ_CATEGORY"]) : string.Empty;
                        report.RequirementCurrency = row["REQ_CURRENCY"] != DBNull.Value ? Convert.ToString(row["REQ_CURRENCY"]) : string.Empty;
                        report.PR_ID = row["PR_ID"] != DBNull.Value ? Convert.ToString(row["PR_ID"]) : string.Empty;
                        report.PRNumbers = row["PR_NUMBERS"] != DBNull.Value ? Convert.ToString(row["PR_NUMBERS"]) : string.Empty;

                        DateTime now = DateTime.UtcNow;
                        report.DATETIME_NOW = now;
                        DateTime DATE_CB_END_TIME = Convert.ToDateTime(report.CB_END_TIME);
                        long DIFF_CB_END_TIME = Convert.ToInt64((DATE_CB_END_TIME - now).TotalSeconds);

                        if (DIFF_CB_END_TIME > 0)
                        {
                            report.CB_TIME_LEFT = DIFF_CB_END_TIME;
                        }
                        else
                        {
                            report.CB_TIME_LEFT = 0;
                            report.CB_END_TIME = now;
                        }


                        if (report.EndTime != DateTime.MaxValue && report.EndTime > now && report.StartTime < now)
                        {
                            DateTime date = Convert.ToDateTime(report.EndTime);
                            long diff = Convert.ToInt64((date - now).TotalSeconds);
                            report.TimeLeft = diff;
                            report.Closed = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString());
                        }
                        else if (report.StartTime != DateTime.MaxValue && report.StartTime > now)
                        {
                            DateTime date = Convert.ToDateTime(report.StartTime);
                            long diff = Convert.ToInt64((date - now).TotalSeconds);
                            report.TimeLeft = diff;
                            report.Closed = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString());
                        }
                        else if (report.EndTime < now)
                        {
                            report.TimeLeft = -1;
                            if ((Status == Utilities.GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString()) || Status == Utilities.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString())) && report.EndTime < now)
                            {
                                report.Closed = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.NegotiationEnded.ToString());
                            }
                            else
                            {
                                report.Closed = Status;
                            }
                        }
                        else if (report.StartTime == DateTime.MaxValue)
                        {
                            report.TimeLeft = -1;
                            report.Closed = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.UNCONFIRMED.ToString());
                        }
                        if (Status == Utilities.GetEnumDesc<PRMStatus>(PRMStatus.DELETED.ToString()))
                        {
                            report.TimeLeft = -1;
                            report.Closed = Status;
                        }


                        if (DIFF_CB_END_TIME > 0)
                        {
                            report.CB_TIME_LEFT = DIFF_CB_END_TIME;
                            report.TimeLeft = DIFF_CB_END_TIME;
                        }
                        else
                        {
                            report.CB_TIME_LEFT = 0;
                            report.CB_END_TIME = now;
                        }

                        RequirementUsersByRank InitialUser = initialReport.Where(a => a.Rank == 1 && a.RequirementID == report.RequirementID).FirstOrDefault();
                        RequirementUsersByRank RevisedUserL1 = revisedReport.Where(a => a.Rank == 1 && a.RequirementID == report.RequirementID).FirstOrDefault();
                        RequirementUsersByRank RevisedUserL2 = revisedReport.Where(a => a.Rank == 2 && a.RequirementID == report.RequirementID).FirstOrDefault();

                        #region Initial L1 START
                        if (InitialUser != null)
                        {
                            report.IL1_UID = InitialUser.UID;
                            report.IL1_RevBasePrice = InitialUser.RevBasePrice;
                            report.IL1_InitBasePrice = InitialUser.InitBasePrice;
                            report.IL1_L1InitialBasePrice = InitialUser.L1InitialBasePrice;
                            report.IL1_VendTotalPrice = InitialUser.VendTotalPrice;
                            report.IL1_Rank = InitialUser.Rank;
                            report.IL1_Quantity = InitialUser.Quantity;
                            report.IL1_CompanyName = InitialUser.CompanyName;
                            report.IL1_SelectedVendorCurrency = InitialUser.SelectedVendorCurrency;
                        }

                        #endregion Initial L1 END

                        #region Revised RL1 START
                        if (RevisedUserL1 != null)
                        {
                            report.RL1_UID = RevisedUserL1.UID;
                            report.RL1_RevBasePrice = RevisedUserL1.RevBasePrice;
                            report.RL1_InitBasePrice = RevisedUserL1.InitBasePrice;
                            report.RL1_L1InitialBasePrice = RevisedUserL1.L1InitialBasePrice;
                            report.RL1_RevVendTotalPrice = RevisedUserL1.RevVendTotalPrice;
                            report.RL1_Rank = RevisedUserL1.Rank;
                            report.RL1_Quantity = RevisedUserL1.Quantity;
                            report.RL1_CompanyName = RevisedUserL1.CompanyName;
                            report.RL1_SelectedVendorCurrency = RevisedUserL1.SelectedVendorCurrency;
                        }

                        #endregion Revised RL1 END

                        #region Revised RL2 START
                        if (RevisedUserL2 != null)
                        {
                            report.RL2_UID = RevisedUserL2.UID;
                            report.RL2_RevBasePrice = RevisedUserL2.RevBasePrice;
                            report.RL2_InitBasePrice = RevisedUserL2.InitBasePrice;
                            report.RL2_L1InitialBasePrice = RevisedUserL2.L1InitialBasePrice;
                            report.RL2_RevVendTotalPrice = RevisedUserL2.RevVendTotalPrice;
                            report.RL2_Rank = RevisedUserL2.Rank;
                            report.RL2_Quantity = RevisedUserL2.Quantity;
                            report.RL2_CompanyName = RevisedUserL2.CompanyName;
                            report.RL2_SelectedVendorCurrency = RevisedUserL2.SelectedVendorCurrency;
                            #endregion Revised RL2 END
                        }

                        report.BasePriceSavings = report.RL1_RevVendTotalPrice - report.IL1_VendTotalPrice;
                        if (report.BasePriceSavings > 0)
                        {
                            report.BasePriceSavings = Math.Round(report.BasePriceSavings, decimal_round);
                        }
                        else
                        {
                            report.BasePriceSavings = 0;
                        }

                        report.SavingsPercentage = ((report.RL1_RevVendTotalPrice / report.IL1_VendTotalPrice) * 100) - 100;
                        if (report.SavingsPercentage > 0)
                        {
                            report.SavingsPercentage = Math.Round(report.SavingsPercentage, decimal_round);
                        }
                        else
                        {
                            report.SavingsPercentage = 0;
                        }

                        //report.SavingsPercentage = Math.Round(report.SavingsPercentage, decimal_round);
                        //report.RevisedUserL1 = RevisedUserL1;
                        //report.RevisedUserL2 = RevisedUserL2;

                        double factor = 1;// Utilities.GetRequirementCurrencyFactor(report.RequirementID);
                        report.Savings = Utilities.RoundValue(report.Savings / factor);
                        report.L1RevPrice = Utilities.RoundValue(report.L1RevPrice / factor);
                        report.L2RevPrice = Utilities.RoundValue(report.L2RevPrice / factor);
                        report.L1InitialBasePrice = Utilities.RoundValue(report.L1InitialBasePrice / factor);
                        report.L2InitialBasePrice = Utilities.RoundValue(report.L2InitialBasePrice / factor);
                        report.InitialL1BasePrice = Utilities.RoundValue(report.InitialL1BasePrice / factor);
                        report.IL1_RevBasePrice = Utilities.RoundValue(report.IL1_RevBasePrice / factor);
                        report.IL1_InitBasePrice = Utilities.RoundValue(report.IL1_InitBasePrice / factor);
                        report.IL1_L1InitialBasePrice = Utilities.RoundValue(report.IL1_L1InitialBasePrice / factor);
                        report.IL1_VendTotalPrice = Utilities.RoundValue(report.IL1_VendTotalPrice / factor);
                        report.RL1_RevBasePrice = Utilities.RoundValue(report.RL1_RevBasePrice / factor);
                        report.RL1_InitBasePrice = Utilities.RoundValue(report.RL1_InitBasePrice / factor);
                        report.RL1_L1InitialBasePrice = Utilities.RoundValue(report.RL1_L1InitialBasePrice / factor);
                        report.RL1_RevVendTotalPrice = Utilities.RoundValue(report.RL1_RevVendTotalPrice / factor);
                        report.RL2_RevBasePrice = Utilities.RoundValue(report.RL2_RevBasePrice / factor);
                        report.RL2_InitBasePrice = Utilities.RoundValue(report.RL2_InitBasePrice / factor);
                        report.RL2_L1InitialBasePrice = Utilities.RoundValue(report.RL2_L1InitialBasePrice / factor);
                        report.RL2_RevVendTotalPrice = Utilities.RoundValue(report.RL2_RevVendTotalPrice / factor);
                        report.BasePriceSavings = Utilities.RoundValue(report.BasePriceSavings / factor);
                        report.L1BasePrice = Utilities.RoundValue(report.L1BasePrice / factor);
                        report.L2BasePrice = Utilities.RoundValue(report.L2BasePrice / factor);

                        consolidatedReports.Add(report);
                    }

                    consolidatedReports = consolidatedReports.Where(r => r.NoOfVendorsInvited > 0).ToList();
                }
            }
            catch (Exception ex)
            {
                ConsolidatedReport recuirementerror = new ConsolidatedReport();
                recuirementerror.ErrorMessage = ex.Message;
                consolidatedReports.Add(recuirementerror);
            }

            return consolidatedReports;
        }

        public ExcelRequirement GetReqReportForExcel(int reqID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            ExcelRequirement excelrequirement = new ExcelRequirement();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("fwd_rp_GetReqReportForExcel", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[3].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[3].Rows[0];
                    excelrequirement.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                    excelrequirement.RequirementNumber = row["REQ_NUMBER"] != DBNull.Value ? Convert.ToString(row["REQ_NUMBER"]) : string.Empty;
                    excelrequirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                    excelrequirement.RequirementCurrency = row["REQ_CURRENCY"] != DBNull.Value ? Convert.ToString(row["REQ_CURRENCY"]) : string.Empty;
                    excelrequirement.PostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.UtcNow;
                    excelrequirement.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.UtcNow;
                    excelrequirement.CurrentDate = Utilities.GetDate();
                    excelrequirement.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                    excelrequirement.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                    excelrequirement.IS_CB_ENABLED = row["IS_CB_ENABLED"] != DBNull.Value ? Convert.ToInt32(row["IS_CB_ENABLED"]) : 0;
                    excelrequirement.IS_CB_COMPLETED = row["IS_CB_COMPLETED"] != DBNull.Value ? Convert.ToInt32(row["IS_CB_COMPLETED"]) : 0;
                    excelrequirement.PostedByFirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                    excelrequirement.PostedByLastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                    excelrequirement.Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                    excelrequirement.IsTechScoreReq = row["TECH_SCORE_REQ"] != DBNull.Value ? Convert.ToInt32(row["TECH_SCORE_REQ"]) : 0;
                    excelrequirement.IsDiscountQuotation = row["IS_DISCOUNT_QUOTATION"] != DBNull.Value ? Convert.ToInt32(row["IS_DISCOUNT_QUOTATION"]) : 0;
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[4].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[4].Rows[0];
                    excelrequirement.PreSavings = row["PRE_SAVING"] != DBNull.Value ? Convert.ToDouble(row["PRE_SAVING"]) : 0;
                    excelrequirement.PostSavings = row["REV_SAVING"] != DBNull.Value ? Convert.ToDouble(row["REV_SAVING"]) : 0;
                }

                List<ExcelVendorDetails> ListRV = new List<ExcelVendorDetails>();
                int rank = 1;
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        ExcelVendorDetails RV = new ExcelVendorDetails();

                        #region VENDOR DETAILS
                        RV.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                        RV.TechnicalScore = row["TECHNICAL_SCORE"] != DBNull.Value ? Convert.ToDecimal(row["TECHNICAL_SCORE"]) : 0;
                        RV.VendorName = row["VENDOR_NAME"] != DBNull.Value ? Convert.ToString(row["VENDOR_NAME"]) : string.Empty;
                        RV.VendorID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;

                        RV.VendorFreight = row["VEND_FREIGHT"] != DBNull.Value ? Convert.ToDouble(row["VEND_FREIGHT"]) : 0;
                        RV.RevVendorFreight = row["REV_VEND_FREIGHT"] != DBNull.Value ? Convert.ToDouble(row["REV_VEND_FREIGHT"]) : 0;
                        RV.TotalInitialPrice = row["VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row["VEND_TOTAL_PRICE"]) : 0;
                        RV.RevVendorTotalPrice = row["REV_VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REV_VEND_TOTAL_PRICE"]) : 0;
                        RV.Warranty = row["WARRANTY"] != DBNull.Value ? Convert.ToString(row["WARRANTY"]) : string.Empty;
                        RV.Validity = row["VALIDITY"] != DBNull.Value ? Convert.ToString(row["VALIDITY"]) : string.Empty;
                        RV.OtherProperties = row["OTHER_PROPERTIES"] != DBNull.Value ? Convert.ToString(row["OTHER_PROPERTIES"]) : string.Empty;
                        RV.Duration = row["DURATION"] != DBNull.Value ? Convert.ToString(row["DURATION"]) : string.Empty;
                        RV.Payment = row["PAYMENT"] != DBNull.Value ? Convert.ToString(row["PAYMENT"]) : string.Empty;
                        RV.GSTNumber = row["GST_NUMBER"] != DBNull.Value ? Convert.ToString(row["GST_NUMBER"]) : string.Empty;
                        RV.SelectedVendorCurrency = row["SELECTED_VENDOR_CURRENCY"] != DBNull.Value ? Convert.ToString(row["SELECTED_VENDOR_CURRENCY"]) : string.Empty;


                        RV.FREIGHT_CHARGES = row["FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["FREIGHT_CHARGES"]) : 0; //*
                        RV.FREIGHT_CHARGES_TAX_PERCENTAGE = row["FREIGHT_CHARGES_TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row["FREIGHT_CHARGES_TAX_PERCENTAGE"]) : 0;
                        RV.FREIGHT_CHARGES_WITH_TAX = row["FREIGHT_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["FREIGHT_CHARGES_WITH_TAX"]) : 0;
                        RV.REV_FREIGHT_CHARGES = row["REV_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["REV_FREIGHT_CHARGES"]) : 0;
                        RV.REV_FREIGHT_CHARGES_WITH_TAX = row["REV_FREIGHT_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["REV_FREIGHT_CHARGES_WITH_TAX"]) : 0; //*
                        RV.RevVendorFreightCB = row["REV_FREIGHT_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_FREIGHT_CHARGES_CB"]) : 0;

                        RV.InstallationCharges = row["INSTALLATION_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["INSTALLATION_CHARGES"]) : 0; //*
                        RV.InstallationChargesTaxPercentage = row["INSTALLATION_CHARGES_TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row["INSTALLATION_CHARGES_TAX_PERCENTAGE"]) : 0;
                        RV.InstallationChargesWithTax = row["INSTALLATION_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["INSTALLATION_CHARGES_WITH_TAX"]) : 0;
                        RV.RevinstallationCharges = row["REV_INSTALLATION_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["REV_INSTALLATION_CHARGES"]) : 0; //*
                        RV.RevinstallationChargesWithTax = row["REV_INSTALLATION_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["REV_INSTALLATION_CHARGES_WITH_TAX"]) : 0;
                        RV.RevinstallationChargesCB = row["REV_INSTALLATION_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_INSTALLATION_CHARGES_CB"]) : 0;
                        RV.RevinstallationChargesWithTaxCB = RV.RevinstallationChargesCB + ((RV.RevinstallationChargesCB / 100) * (RV.InstallationChargesTaxPercentage));

                        RV.PackingCharges = row["PACKING_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["PACKING_CHARGES"]) : 0; //*
                        RV.PackingChargesTaxPercentage = row["PACKING_CHARGES_TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row["PACKING_CHARGES_TAX_PERCENTAGE"]) : 0;
                        RV.PackingChargesWithTax = row["PACKING_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["PACKING_CHARGES_WITH_TAX"]) : 0;
                        RV.RevpackingCharges = row["REV_PACKING_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["REV_PACKING_CHARGES"]) : 0; //*
                        RV.RevpackingChargesWithTax = row["REV_PACKING_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["REV_PACKING_CHARGES_WITH_TAX"]) : 0;
                        RV.RevpackingChargesCB = row["REV_PACKING_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_PACKING_CHARGES_CB"]) : 0;
                        RV.RevpackingChargesWithTaxCB = RV.RevpackingChargesCB + ((RV.RevpackingChargesCB / 100) * (RV.PackingChargesTaxPercentage));

                        RV.RevVendorTotalPriceCB = row["REV_VEND_TOTAL_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_VEND_TOTAL_PRICE_CB"]) : 0;

                        RV.INCO_TERMS = row["INCO_TERMS"] != DBNull.Value ? Convert.ToString(row["INCO_TERMS"]) : string.Empty;
                        RV.PayLoadFactor = 0;
                        RV.RevPayLoadFactor = 0;
                        RV.RevChargeAny = 0;
                        RV.IsQuotationRejected = row["IS_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToInt32(row["IS_QUOTATION_REJECTED"]) : 0;
                        RV.IsRevQuotationRejected = row["IS_REV_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToInt32(row["IS_REV_QUOTATION_REJECTED"]) : 0;
                        RV.IS_QUOT_LATE = row["IS_QUOT_LATE"] != DBNull.Value ? Convert.ToInt32(row["IS_QUOT_LATE"]) : 0;
                        RV.VendorCurrencyFactor = row["CURRENCY_FACTOR"] != DBNull.Value ? Convert.ToDecimal(row["CURRENCY_FACTOR"]) : 1;
                        RV.Vendor = new User();
                        RV.Vendor.VendorCode = row["VENDOR_CODE"] != DBNull.Value ? Convert.ToString(row["VENDOR_CODE"]) : string.Empty;
                        #endregion VENDOR DETAILS

                        ListRV.Add(RV);
                    }

                    foreach (ExcelVendorDetails vendor in ListRV.Where(v => v.IsQuotationRejected == 0).OrderBy(v => v.TotalInitialPrice).ToList())
                    {
                        vendor.Rank = ListRV.IndexOf(vendor) + 1;
                    }
                }

                List<ExcelRequirementItems> ListRITemp = new List<ExcelRequirementItems>();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[1].Rows)
                    {
                        ExcelRequirementItems RI = new ExcelRequirementItems();
                        #region ITEM DETAILS
                        RI.ItemID = row["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row["ITEM_ID"]) : 0;
                        RI.ProductIDorName = row["PROD_ID"] != DBNull.Value ? Convert.ToString(row["PROD_ID"]) : string.Empty;
                        RI.ProductNo = row["PROD_NO"] != DBNull.Value ? Convert.ToString(row["PROD_NO"]) : string.Empty;
                        RI.HsnCode = row["HSN_CODE"] != DBNull.Value ? Convert.ToString(row["HSN_CODE"]) : string.Empty;
                        RI.ProductDescription = row["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row["DESCRIPTION"]) : string.Empty;
                        RI.ProductCode = row["PROD_CODE"] != DBNull.Value ? Convert.ToString(row["PROD_CODE"]) : string.Empty;
                        RI.ProductQuantity = row["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row["QUANTITY"]) : 0;
                        RI.ProductQuantityIn = row["QUANTITY_IN"] != DBNull.Value ? Convert.ToString(row["QUANTITY_IN"]) : string.Empty;
                        RI.ProductBrand = row["BRAND"] != DBNull.Value ? Convert.ToString(row["BRAND"]) : string.Empty;
                        RI.OthersBrands = row["OTHER_BRAND"] != DBNull.Value ? Convert.ToString(row["OTHER_BRAND"]) : string.Empty;
                        RI.ItemLastPrice = row["LAST_ITEM_PRICE"] != DBNull.Value ? Convert.ToDouble(row["LAST_ITEM_PRICE"]) : 0;
                        RI.I_LLP_DETAILS = row["I_LLP_DETAILS"] != DBNull.Value ? Convert.ToString(row["I_LLP_DETAILS"]) : string.Empty;
                        RI.ITEM_L1_PRICE = row["ITEM_L1_PRICE"] != DBNull.Value ? Convert.ToDouble(row["ITEM_L1_PRICE"]) : 0;
                        RI.ITEM_L1_COMPANY_NAME = row["ITEM_L1_COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["ITEM_L1_COMPANY_NAME"]) : string.Empty;
                        RI.ITEM_L2_PRICE = row["ITEM_L2_PRICE"] != DBNull.Value ? Convert.ToDouble(row["ITEM_L2_PRICE"]) : 0;
                        RI.ITEM_L2_COMPANY_NAME = row["ITEM_L2_COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["ITEM_L2_COMPANY_NAME"]) : string.Empty;
                        RI.ProductQuotationTemplateJson = row["PRODUCT_QUOTATION_JSON"] != DBNull.Value ? Convert.ToString(row["PRODUCT_QUOTATION_JSON"]) : string.Empty;
                        RI.ITEM_PR_NUMBER = row["ITEM_PR_NUMBER"] != DBNull.Value ? Convert.ToString(row["ITEM_PR_NUMBER"]) : string.Empty;
                        RI.PR_WBS_CODE = row["PR_WBS_CODE"] != DBNull.Value ? Convert.ToString(row["PR_WBS_CODE"]) : string.Empty;
                        RI.IsCore = row["IS_CORE"] != DBNull.Value ? Convert.ToInt32(row["IS_CORE"]) : 0;
                        RI.QtyDistributed = 0;
                        RI.CatalogueItemID = row["CATALOGUE_ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row["CATALOGUE_ITEM_ID"]) : 0;
                        if (!string.IsNullOrEmpty(RI.ProductQuotationTemplateJson))
                        {
                            RI.ProductQuotationTemplateArray = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CATALOG.ProductQuotationTemplate>>(RI.ProductQuotationTemplateJson);
                        }

                        RI.ReqVendors = ListRV.Select(item => (ExcelVendorDetails)item.Clone()).ToList().ToArray();
                        #endregion ITEM DETAILS

                        ListRITemp.Add(RI);
                    }
                }

                List<ExcelQuotationPrices> ListQ = new List<ExcelQuotationPrices>();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[2].Rows)
                    {
                        ExcelQuotationPrices Q = new ExcelQuotationPrices();

                        #region QUOTATION DETAILS
                        Q.VendorID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                        Q.ItemID = row["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row["ITEM_ID"]) : 0;
                        Q.UnitPrice = row["UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row["UNIT_PRICE"]) : 0;
                        Q.RevUnitPrice = row["REV_UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REV_UNIT_PRICE"]) : 0;
                        Q.CGst = row["C_GST"] != DBNull.Value ? Convert.ToDouble(row["C_GST"]) : 0;
                        Q.SGst = row["S_GST"] != DBNull.Value ? Convert.ToDouble(row["S_GST"]) : 0;
                        Q.IGst = row["I_GST"] != DBNull.Value ? Convert.ToDouble(row["I_GST"]) : 0;
                        Q.ItemPrice = row["PRICE"] != DBNull.Value ? Convert.ToDouble(row["PRICE"]) : 0;
                        Q.RevItemPrice = row["REVICED_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REVICED_PRICE"]) : 0;
                        Q.IsRegret = row["IS_REGRET"] != DBNull.Value ? (Convert.ToInt32(row["IS_REGRET"]) > 0 ? true : false) : false;
                        Q.RegretComments = row["REGRET_COMMENTS"] != DBNull.Value ? Convert.ToString(row["REGRET_COMMENTS"]) : string.Empty;
                        Q.ItemLevelInitialComments = row["ITEM_LEVEL_INITIALCOMMENT"] != DBNull.Value ? Convert.ToString(row["ITEM_LEVEL_INITIALCOMMENT"]) : string.Empty;
                        Q.ItemLevelRevComments = row["ITEM_LEVEL_REVCOMMENT"] != DBNull.Value ? Convert.ToString(row["ITEM_LEVEL_REVCOMMENT"]) : string.Empty;
                        Q.VendorUnits = row["VENDOR_UNITS"] != DBNull.Value ? Convert.ToString(row["VENDOR_UNITS"]) : string.Empty;
                        Q.VendorBrand = row["BRAND"] != DBNull.Value ? Convert.ToString(row["BRAND"]) : string.Empty;
                        Q.ItemFreightCharges = row["ITEM_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["ITEM_FREIGHT_CHARGES"]) : 0;
                        Q.ItemRevFreightCharges = row["ITEM_REV_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["ITEM_REV_FREIGHT_CHARGES"]) : 0;
                        Q.ItemFreightTAX = row["ITEM_FREIGHT_TAX"] != DBNull.Value ? Convert.ToDouble(row["ITEM_FREIGHT_TAX"]) : 0;
                        Q.RevItemPriceCB = row["REVICED_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row["REVICED_PRICE_CB"]) : 0;
                        Q.RevUnitPriceCB = row["REV_UNIT_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_UNIT_PRICE_CB"]) : 0;
                        Q.UnitDiscount = row["UNIT_DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row["UNIT_DISCOUNT"]) : 0;
                        Q.RevUnitDiscount = row["REV_UNIT_DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row["REV_UNIT_DISCOUNT"]) : 0;
                        Q.ProductQuotationTemplateJson = row["ProductQuotationTemplateJson"] != DBNull.Value ? Convert.ToString(row["ProductQuotationTemplateJson"]) : string.Empty;
                        if (!string.IsNullOrEmpty(Q.ProductQuotationTemplateJson))
                        {
                            Q.ProductQuotationTemplateArray = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CATALOG.ProductQuotationTemplate>>(Q.ProductQuotationTemplateJson);
                        }


                        // ITEM LEVEL RANKING //
                        int is_quotation_rejected = row["IS_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToInt32(row["IS_QUOTATION_REJECTED"]) : 0;
                        Q.ItemRank = 0;
                        if (is_quotation_rejected <= 0 && Q.RevUnitPrice > 0)
                        {
                            Q.ItemRank = 1;// row2["ITEM_RANK"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_RANK"]) : 0;
                        }
                        // ITEM LEVEL RANKING //

                        #endregion QUOTATION DETAILS

                        ListQ.Add(Q);

                    }
                }

                // ITEM LEVEL RANKING //
                ListQ = ListQ.OrderBy(q => q.ItemID).ThenByDescending(q => q.ItemRank).ThenBy(q => q.RevUnitPrice).ToList();
                int rank1 = 1;
                int prevItemId = 0;
                double prevRevUnitPrice = 0;
                foreach (var temp in ListQ)
                {
                    if (temp.ItemRank > 0)
                    {
                        if (prevItemId == temp.ItemID && temp.RevUnitPrice > 0 && prevRevUnitPrice == temp.RevUnitPrice)
                        {
                            temp.ItemRank = rank1;
                        }

                        if (prevItemId == temp.ItemID && temp.RevUnitPrice > 0 && temp.RevUnitPrice > prevRevUnitPrice)
                        {
                            temp.ItemRank = rank1 + 1;
                            rank1 = rank1 + 1;
                        }

                        if (prevItemId != temp.ItemID)
                        {
                            temp.ItemRank = 1;
                            rank1 = 1;
                        }
                    }

                    prevItemId = temp.ItemID;
                    prevRevUnitPrice = temp.RevUnitPrice;
                }
                // ITEM LEVEL RANKING //

                for (int i = 0; i < ListRITemp.Count; i++)
                {
                    for (int v = 0; v < ListRITemp[i].ReqVendors.Length; v++)
                    {
                        ListRITemp[i].ReqVendors[v].QuotationPrices = new ExcelQuotationPrices();
                        try
                        {
                            ListRITemp[i].ReqVendors[v].QuotationPrices = ListQ.Where(quotation => quotation.VendorID == ListRITemp[i].ReqVendors[v].VendorID && quotation.ItemID == ListRITemp[i].ItemID).FirstOrDefault();
                        }
                        catch
                        {
                            ListRITemp[i].ReqVendors[v].QuotationPrices = new ExcelQuotationPrices();
                        }
                    }
                }

                excelrequirement.ReqItems = ListRITemp.ToArray();

                foreach (ExcelRequirementItems item in excelrequirement.ReqItems)
                {
                    if (item.ReqVendors != null)
                    {
                        List<ExcelVendorDetails> vendors = new List<ExcelVendorDetails>();
                        vendors = item.ReqVendors.Where(v => v.RevVendorTotalPrice > 0).OrderBy(v => v.RevVendorTotalPrice).ToList();
                        item.ReqVendors = vendors.ToArray();
                    }
                }

                //CurrencyConversion
                double factor = 1;
                excelrequirement.Savings = Utilities.RoundValue(excelrequirement.Savings / factor);
                excelrequirement.PreSavings = Utilities.RoundValue(excelrequirement.PreSavings / factor);
                excelrequirement.PostSavings = Utilities.RoundValue(excelrequirement.PostSavings / factor);

                if (excelrequirement.ReqVendors != null && excelrequirement.ReqVendors.Length > 0)
                {
                    foreach (var vendor in excelrequirement.ReqVendors)
                    {
                        vendor.InitialPrice = Utilities.RoundValue(vendor.InitialPrice / factor);
                        vendor.RunningPrice = Utilities.RoundValue(vendor.RunningPrice / factor);
                        vendor.TotalInitialPrice = Utilities.RoundValue(vendor.TotalInitialPrice / factor);
                        vendor.TotalRunningPrice = Utilities.RoundValue(vendor.TotalRunningPrice / factor);
                        vendor.InitialPriceWithOutTaxFreight = Utilities.RoundValue(vendor.InitialPriceWithOutTaxFreight / factor);
                        vendor.VendorFreight = Utilities.RoundValue(vendor.VendorFreight / factor);
                        vendor.RevPrice = Utilities.RoundValue(vendor.RevPrice / factor);
                        vendor.RevVendorTotalPrice = Utilities.RoundValue(vendor.RevVendorTotalPrice / factor);
                        vendor.Discount = Utilities.RoundValue(vendor.Discount / factor);
                        vendor.marginRankDiscount = Utilities.RoundValue(vendor.marginRankDiscount / factor);
                        vendor.marginRankRevDiscount = Utilities.RoundValue(vendor.marginRankRevDiscount / factor);

                        vendor.RevVendorFreightCB = Utilities.RoundValue(vendor.RevVendorFreightCB / factor);
                        vendor.VendorFreightCB = Utilities.RoundValue(vendor.VendorFreightCB / factor);
                        vendor.RevinstallationChargesCB = Utilities.RoundValue(vendor.RevinstallationChargesCB / factor);
                        vendor.RevinstallationChargesWithTaxCB = Utilities.RoundValue(vendor.RevinstallationChargesWithTaxCB / factor);
                        vendor.RevpackingChargesCB = Utilities.RoundValue(vendor.RevpackingChargesCB / factor);
                        vendor.RevpackingChargesWithTaxCB = Utilities.RoundValue(vendor.RevpackingChargesWithTaxCB / factor);
                        vendor.RevVendorTotalPriceCB = Utilities.RoundValue(vendor.RevVendorTotalPriceCB / factor);
                    }
                }

                if (excelrequirement.ReqItems != null && excelrequirement.ReqItems.Length > 0)
                {
                    foreach (var item in excelrequirement.ReqItems)
                    {
                        item.ItemPrice = Utilities.RoundValue(item.ItemPrice / factor);
                        item.RevisedItemPrice = Utilities.RoundValue(item.RevisedItemPrice / factor);
                        item.RevisedInitialprice = Utilities.RoundValue(item.RevisedInitialprice / factor);
                        item.RevisedVendorBidPrice = Utilities.RoundValue(item.RevisedVendorBidPrice / factor);
                        item.RevItemPrice = Utilities.RoundValue(item.RevItemPrice / factor);
                        item.UnitPrice = Utilities.RoundValue(item.UnitPrice / factor);
                        item.RevUnitPrice = Utilities.RoundValue(item.RevUnitPrice / factor);
                        item.UnitMRP = Utilities.RoundValue(item.UnitMRP / factor);
                        item.UnitDiscount = Utilities.RoundValue(item.UnitDiscount / factor);
                        item.RevUnitDiscount = Utilities.RoundValue(item.RevUnitDiscount / factor);
                        item.CostPrice = Utilities.RoundValue(item.CostPrice / factor);

                        item.CostPrice = Utilities.RoundValue(item.CostPrice / factor);
                        item.NetPrice = Utilities.RoundValue(item.NetPrice / factor);
                        item.MarginAmount = Utilities.RoundValue(item.MarginAmount / factor);

                        item.ItemLastPrice = Utilities.RoundValue(item.ItemLastPrice / factor);
                        item.ITEM_L1_PRICE = Utilities.RoundValue(item.ITEM_L1_PRICE / factor);
                        item.ITEM_L2_PRICE = Utilities.RoundValue(item.ITEM_L2_PRICE / factor);

                        foreach (var vendor in item.ReqVendors)
                        {
                            vendor.InitialPrice = Utilities.RoundValue(vendor.InitialPrice / factor);
                            vendor.RunningPrice = Utilities.RoundValue(vendor.RunningPrice / factor);
                            vendor.TotalInitialPrice = Utilities.RoundValue(vendor.TotalInitialPrice / factor);
                            vendor.TotalRunningPrice = Utilities.RoundValue(vendor.TotalRunningPrice / factor);
                            vendor.InitialPriceWithOutTaxFreight = Utilities.RoundValue(vendor.InitialPriceWithOutTaxFreight / factor);
                            vendor.VendorFreight = Utilities.RoundValue(vendor.VendorFreight / factor);
                            vendor.RevPrice = Utilities.RoundValue(vendor.RevPrice / factor);
                            vendor.RevVendorTotalPrice = Utilities.RoundValue(vendor.RevVendorTotalPrice / factor);
                            vendor.Discount = Utilities.RoundValue(vendor.Discount / factor);
                            vendor.marginRankDiscount = Utilities.RoundValue(vendor.marginRankDiscount / factor);
                            vendor.marginRankRevDiscount = Utilities.RoundValue(vendor.marginRankRevDiscount / factor);
                            vendor.QuotationPrices.ItemPrice = Utilities.RoundValue(vendor.QuotationPrices.ItemPrice / factor);
                            vendor.QuotationPrices.RevisedItemPrice = Utilities.RoundValue(vendor.QuotationPrices.RevisedItemPrice / factor);
                            vendor.QuotationPrices.RevisedInitialprice = Utilities.RoundValue(vendor.QuotationPrices.RevisedInitialprice / factor);
                            vendor.QuotationPrices.RevisedVendorBidPrice = Utilities.RoundValue(vendor.QuotationPrices.RevisedVendorBidPrice / factor);
                            vendor.QuotationPrices.RevItemPrice = Utilities.RoundValue(vendor.QuotationPrices.RevItemPrice / factor);
                            vendor.QuotationPrices.UnitPrice = Utilities.RoundValue(vendor.QuotationPrices.UnitPrice / factor);
                            vendor.QuotationPrices.RevUnitPrice = Utilities.RoundValue(vendor.QuotationPrices.RevUnitPrice / factor);
                            vendor.QuotationPrices.UnitMRP = Utilities.RoundValue(vendor.QuotationPrices.UnitMRP / factor);
                            vendor.QuotationPrices.UnitDiscount = Utilities.RoundValue(vendor.QuotationPrices.UnitDiscount / factor);
                            vendor.QuotationPrices.RevUnitDiscount = Utilities.RoundValue(vendor.QuotationPrices.RevUnitDiscount / factor);
                            vendor.QuotationPrices.CostPrice = Utilities.RoundValue(vendor.QuotationPrices.CostPrice / factor);
                            vendor.QuotationPrices.NetPrice = Utilities.RoundValue(vendor.QuotationPrices.NetPrice / factor);
                            vendor.QuotationPrices.MarginAmount = Utilities.RoundValue(vendor.QuotationPrices.MarginAmount / factor);
                            vendor.QuotationPrices.CostPrice = Utilities.RoundValue(vendor.QuotationPrices.CostPrice / factor);

                            vendor.RevVendorFreightCB = Utilities.RoundValue(vendor.RevVendorFreightCB / factor);
                            vendor.VendorFreightCB = Utilities.RoundValue(vendor.VendorFreightCB / factor);
                            vendor.RevinstallationChargesCB = Utilities.RoundValue(vendor.RevinstallationChargesCB / factor);
                            vendor.RevinstallationChargesWithTaxCB = Utilities.RoundValue(vendor.RevinstallationChargesWithTaxCB / factor);
                            vendor.RevpackingChargesCB = Utilities.RoundValue(vendor.RevpackingChargesCB / factor);
                            vendor.RevpackingChargesWithTaxCB = Utilities.RoundValue(vendor.RevpackingChargesWithTaxCB / factor);
                            vendor.RevVendorTotalPriceCB = Utilities.RoundValue(vendor.RevVendorTotalPriceCB / factor);
                        }
                    }
                }
                //^CurrencyConversion

            }
            catch (Exception ex)
            {
                excelrequirement.ErrorMessage = ex.Message;
            }

            return excelrequirement;
        }

        public PriceComparison GetPriceComparisonPreNegotiation(int reqID, int userID, string sessionID)
        {
            PriceComparison requirement = new PriceComparison();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                int isValidSession = this.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("fwd_GetPriceComparisonPreNegotiation", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        DataRow row1 = ds.Tables[1].Rows[0];
                        requirement.MinQuotationPrice = row1["VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["VEND_TOTAL_PRICE"]) : 0;
                    }
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    requirement.requirement = GetRequirementData(reqID, userID, sessionID);
                    requirement.requirement.AuctionVendors = requirement.requirement.AuctionVendors.Where(v => v.IsQuotationRejected != 1).Where(v => v.InitialPrice > 0).OrderByDescending(v => (Convert.ToDecimal(v.InitialPrice) * v.VendorCurrencyFactor)).ThenBy(p => p.AuctionModifiedDate).Where(v => v.CompanyName != "PRICE_CAP").ToList();
                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        requirement.PriceCompareObject = new List<PriceCompareObject>();
                        foreach (DataRow row2 in ds.Tables[2].Rows)
                        {
                            PriceCompareObject prCompObj = new PriceCompareObject();
                            prCompObj.ItemID = row2["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_ID"]) : 0;
                            prCompObj.MinPriceBidder = row2["LEAST_ITEM_BIDDER"] != DBNull.Value ? Convert.ToInt32(row2["LEAST_ITEM_BIDDER"]) : 0;
                            prCompObj.MinPrice = row2["MIN_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["MIN_PRICE"]) : 0;

                            prCompObj.MinUnitPrice = row2["UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["UNIT_PRICE"]) : 0;

                            prCompObj.LeastBidderPrice = row2["LEAST_BIDDER_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["LEAST_BIDDER_PRICE"]) : 0;

                            prCompObj.LeastBidderUnitPrice = row2["LEAST_BIDDER_UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["LEAST_BIDDER_UNIT_PRICE"]) : 0;


                            string vendorNames = string.Empty;
                            prCompObj.VendorName = vendorNames;
                            prCompObj.ProductName = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                            prCompObj.ProductDescription = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;

                            prCompObj.Quantity = row2["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row2["QUANTITY"]) : 0;
                            prCompObj.Units = row2["UNITS"] != DBNull.Value ? Convert.ToString(row2["UNITS"]) : string.Empty;



                            if (ds.Tables[2].Rows.Count > 0)
                            {

                                int L1_U_ID = requirement.requirement.AuctionVendors[0].VendorID;
                                if (requirement.requirement.AuctionVendors[0].CompanyName == "PRICE_CAP")
                                {
                                    L1_U_ID = requirement.requirement.AuctionVendors[1].VendorID;
                                }

                                List<PriceCompareObject> listPriceComparision = new List<PriceCompareObject>();
                                List<PriceCompareObject> listPriceComparisionFilterd = new List<PriceCompareObject>();
                                foreach (DataRow row3 in ds.Tables[3].Rows)
                                {
                                    PriceCompareObject priceComparisionObject = new PriceCompareObject();

                                    priceComparisionObject.QuotationID = row3["QUOT_ID"] != DBNull.Value ? Convert.ToInt32(row3["QUOT_ID"]) : 0;
                                    priceComparisionObject.UserID = row3["U_ID"] != DBNull.Value ? Convert.ToInt32(row3["U_ID"]) : 0;
                                    priceComparisionObject.ItemID = row3["item_id"] != DBNull.Value ? Convert.ToInt32(row3["item_id"]) : 0;
                                    priceComparisionObject.MinPrice = row3["REVICED_PRICE"] != DBNull.Value ? Convert.ToDouble(row3["REVICED_PRICE"]) : 0;
                                    priceComparisionObject.Brand = row3["brand"] != DBNull.Value ? Convert.ToString(row3["brand"]) : string.Empty;
                                    priceComparisionObject.Duration = row3["DURATION"] != DBNull.Value ? Convert.ToString(row3["DURATION"]) : string.Empty;
                                    priceComparisionObject.Payment = row3["PAYMENT"] != DBNull.Value ? Convert.ToString(row3["PAYMENT"]) : string.Empty;
                                    priceComparisionObject.VendorName = row3["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row3["COMPANY_NAME"]) : string.Empty;
                                    priceComparisionObject.TechnicalScore = row3["TECHNICAL_SCORE"] != DBNull.Value ? Convert.ToDecimal(row3["TECHNICAL_SCORE"]) : 0;
                                    priceComparisionObject.OtherProperties = row3["OTHER_PROPERTIES"] != DBNull.Value ? Convert.ToString(row3["OTHER_PROPERTIES"]) : string.Empty;

                                    priceComparisionObject.MinUnitPrice = row3["UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row3["UNIT_PRICE"]) : 0;


                                    listPriceComparision.Add(priceComparisionObject);
                                }

                                listPriceComparisionFilterd = listPriceComparision.Where(v => v.ItemID == prCompObj.ItemID).Where(v => v.MinPrice == prCompObj.MinPrice).OrderByDescending(v => v.MinPrice).ToList();



                                foreach (PriceCompareObject priceComparisionObject in listPriceComparisionFilterd)
                                {
                                    foreach (VendorDetails vd in requirement.requirement.AuctionVendors)
                                    {
                                        if (priceComparisionObject.UserID == vd.VendorID)
                                        {
                                            prCompObj.VendorName += priceComparisionObject.VendorName + ",";
                                            prCompObj.Brand += priceComparisionObject.Brand + ",";
                                            prCompObj.Duration += priceComparisionObject.Duration + ",";
                                            prCompObj.Payment += priceComparisionObject.Payment + ",";
                                            prCompObj.OtherProperties += priceComparisionObject.OtherProperties + ",";
                                        }

                                    }
                                }
                                if (!string.IsNullOrEmpty(prCompObj.VendorName))
                                {
                                    prCompObj.VendorName = prCompObj.VendorName.Substring(0, prCompObj.VendorName.Length - 1);
                                }
                                if (!string.IsNullOrEmpty(prCompObj.Brand))
                                {
                                    prCompObj.Brand = prCompObj.Brand.Substring(0, prCompObj.Brand.Length - 1);
                                }
                                if (!string.IsNullOrEmpty(prCompObj.Duration))
                                {
                                    prCompObj.Duration = prCompObj.Duration.Substring(0, prCompObj.Duration.Length - 1);
                                }
                                if (!string.IsNullOrEmpty(prCompObj.Payment))
                                {
                                    prCompObj.Payment = prCompObj.Payment.Substring(0, prCompObj.Payment.Length - 1);
                                }
                                if (!string.IsNullOrEmpty(prCompObj.OtherProperties))
                                {
                                    prCompObj.OtherProperties = prCompObj.OtherProperties.Substring(0, prCompObj.OtherProperties.Length - 1);
                                }





                                foreach (PriceCompareObject priceComparisionObject in listPriceComparision)
                                {
                                    if (L1_U_ID == priceComparisionObject.UserID && prCompObj.ItemID == priceComparisionObject.ItemID)
                                    {
                                        prCompObj.BrandL1 = priceComparisionObject.Brand;
                                        prCompObj.DurationL1 = priceComparisionObject.Duration;
                                        prCompObj.PaymentL1 = priceComparisionObject.Payment;
                                        prCompObj.OtherPropertiesL1 = priceComparisionObject.OtherProperties;
                                    }
                                }
                            }

                            if (ds.Tables[4].Rows.Count > 0)
                            {
                                foreach (DataRow row1 in ds.Tables[4].Rows)
                                {
                                    int vendorID = row1["U_ID"] != DBNull.Value ? Convert.ToInt32(row1["U_ID"]) : 0;
                                    decimal techEvalScore = row1["TOTAL_MARKS"] != DBNull.Value ? Convert.ToInt32(row1["TOTAL_MARKS"]) : 0;
                                    requirement.requirement.AuctionVendors.Where(v => v.VendorID == vendorID).FirstOrDefault().TechEvalScore = techEvalScore;
                                }
                            }

                            requirement.PriceCompareObject.Add(prCompObj);
                        }
                    }
                }

                //CurrencyConversion
                double factor = 1;// Utilities.GetRequirementCurrencyFactor(reqID);
                requirement.MinQuotationPrice = Utilities.RoundValue(requirement.MinQuotationPrice / factor);
                if (requirement.PriceCompareObject != null && requirement.PriceCompareObject.Count > 0)
                {
                    foreach (var priceObj in requirement.PriceCompareObject)
                    {
                        priceObj.MinPrice = Utilities.RoundValue(priceObj.MinPrice / factor);
                        priceObj.LeastBidderPrice = Utilities.RoundValue(priceObj.LeastBidderPrice / factor);
                        priceObj.MinUnitPrice = Utilities.RoundValue(priceObj.MinUnitPrice / factor);
                        priceObj.LeastBidderUnitPrice = Utilities.RoundValue(priceObj.LeastBidderUnitPrice / factor);
                    }
                }
                //^CurrencyConversion
            }
            catch (Exception ex)
            {
                requirement.ErrorMessage = ex.Message;
            }

            return requirement;
        }

        public Response VendorReminders(int reqID, int userID, string message, int[] vendorIDs, string[] vendorCompanyNames, string sessionID, string requestType)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            string VendorUID = string.Empty;
            string VendorCompanies = string.Empty;
            try
            {
                // ValidateSession(sessionID, null);
                Requirement requirement = GetRequirementData(reqID, userID, sessionID);
                UserDetails customer = Utilities.GetUserDetails(userID);
                string ScreenName = "";
                foreach (int vendorID in vendorIDs)
                {
                    UserDetails vendor = Utilities.GetUserDetails(vendorID);
                    User altUser = GetReqAlternateCommunications(reqID, vendorID);
                    if (requestType == "REMINDER")
                    {
                        ScreenName = "VENDOR_REMINDER";
                        string emailBody = string.Empty;
                        emailBody = GenerateEmailBody("VendoremailForReqReminder");
                        emailBody = String.Format(emailBody, vendor.FirstName, vendor.LastName, reqID, requirement.Title, message, customer.FirstName, customer.LastName, customer.CompanyName);
                        SendEmail(vendor.Email + "," + vendor.AltEmail + "," + altUser.AltEmail, "Req Number: " + requirement.RequirementNumber + " Title: " + requirement.Title + " - Gentle Reminder from Customer Regarding Requirement : " + requirement.Title, emailBody, reqID, vendorID, ScreenName, sessionID).ConfigureAwait(false);

                        string body2 = GenerateEmailBody("VendorsmsForReqReminder");
                        body2 = String.Format(body2, vendor.FirstName, vendor.LastName, reqID, requirement.Title, message, customer.FirstName, customer.LastName, customer.CompanyName);
                        body2 = body2.Replace("<br/>", "");
                        //   SendSMS(string.Empty, vendor.PhoneNum + "," + vendor.AltPhoneNum + "," + altUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body2.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), reqID, vendorID, ScreenName, sessionID).ConfigureAwait(false);
                    }
                    else if (requestType == "INTRO")
                    {
                        ScreenName = "VENDOR_INTRODUCTION";
                        string emailBody = string.Empty;
                        emailBody = GenerateEmailBody("VendoremailForIntro");
                        emailBody = String.Format(emailBody, vendor.FirstName, vendor.LastName, reqID, requirement.Title, message, customer.FirstName, customer.LastName, customer.CompanyName);
                        SendEmail(vendor.Email + "," + vendor.AltEmail + "," + altUser.AltEmail, "Req Number: " + requirement.RequirementNumber + " Title: " + requirement.Title + " - Gentle Reminder from Customer Regarding Requirement : " + requirement.Title, emailBody, reqID, vendorID, ScreenName, sessionID).ConfigureAwait(false);

                        string body2 = GenerateEmailBody("VendorsmsForIntro");
                        body2 = String.Format(body2, vendor.FirstName, vendor.LastName, reqID, requirement.Title, message, customer.FirstName, customer.LastName, customer.CompanyName);
                        body2 = body2.Replace("<br/>", "");
                        //  SendSMS(string.Empty, vendor.PhoneNum + "," + vendor.AltPhoneNum + "," + altUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body2.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), reqID, vendorID, ScreenName, sessionID).ConfigureAwait(false);
                    }


                    VendorUID += vendorID + ",";
                }

                VendorUID = VendorUID.Substring(0, VendorUID.Length - 1);
                foreach (string vendorCompanyName in vendorCompanyNames)
                {
                    VendorCompanies += vendorCompanyName + ",";
                }

                VendorCompanies = VendorCompanies.Substring(0, VendorCompanies.Length - 1);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_U_ID", userID);
                sd.Add("P_REMINDER_MESSAGE", message);
                sd.Add("P_VENDOR_ID", VendorUID);
                sd.Add("P_VENDOR_COMPANY", VendorCompanies);
                sd.Add("P_REQUEST_TYPE", requestType);
                DataSet ds = sqlHelper.SelectList("fwd_SaveVendorReminders", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public List<Reminders> GetVendorReminders(int reqID, int userID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<Reminders> ListReminders = new List<Reminders>();
            try
            {
                Utilities.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("fwd_GetVendorReminders", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        Reminders reminders = new Reminders();
                        reminders.VendorIDs = row["VENDOR_ID"] != DBNull.Value ? Convert.ToString(row["VENDOR_ID"]) : string.Empty;
                        reminders.ReminderMessage = row["REMINDER_MESSAGE"] != DBNull.Value ? Convert.ToString(row["REMINDER_MESSAGE"]) : string.Empty;
                        reminders.sentOn = row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]) : DateTime.MaxValue;
                        reminders.VendorCompanies = row["VENDOR_COMPANY"] != DBNull.Value ? Convert.ToString(row["VENDOR_COMPANY"]) : string.Empty;
                        reminders.RequestType = row["REQUEST_TYPE"] != DBNull.Value ? Convert.ToString(row["REQUEST_TYPE"]) : string.Empty;

                        ListReminders.Add(reminders);
                    }
                }
            }
            catch (Exception ex)
            {
                Reminders reminders = new Reminders();
                reminders.ErrorMessage = ex.Message;
                ListReminders.Add(reminders);
            }

            return ListReminders;
        }

        public List<Requirement> GetVendorRequirements(int userID, int negotiationStarted, string sessionID)
        {
            return new List<Requirement>();
        }

        public string GetTemplates(string template, int compID, int userID, int reqID, string fromDate, string toDate, int templateid, string sessionID)
        {
            PRMServices service = new PRMServices();
            int maxRows = 500;
            MemoryStream ms = new MemoryStream();
            byte[] contents = null;

            
            #region vendorquotation
            if (template.ToUpper().Contains("MARGIN_QUOTATION"))
            {
                Requirement requirement = GetRequirementData(reqID, userID, sessionID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("MARGIN_QUOTATION");
                wsSheet1.Cells["A1"].Value = "VENDOR_ID";
                wsSheet1.Cells["B1"].Value = "VENDOR_NAME";
                wsSheet1.Cells["C1"].Value = "HSN_CODE";
                wsSheet1.Cells["D1"].Value = "BRAND";
                wsSheet1.Cells["E1"].Value = "QUANTITY";
                wsSheet1.Cells["F1"].Value = "COST_PRICE";
                wsSheet1.Cells["G1"].Value = "GST";
                wsSheet1.Cells["H1"].Value = "MRP";
                wsSheet1.Cells["I1"].Value = "NET_PRICE";
                wsSheet1.Cells["J1"].Value = "MARGIN_AMOUNT";
                wsSheet1.Cells["K1"].Value = "MARGIN_PERC";
                wsSheet1.Cells["L1"].Value = "ITEM_ID";
                wsSheet1.Cells["M1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["N1"].Value = "PRODUCT_NAME";
                wsSheet1.Cells["O1"].Value = "UNITS";
                wsSheet1.Cells["P1"].Value = "VENDOR_REMARK";
                wsSheet1.Cells["Q1"].Value = "REQ_ID";
                wsSheet1.Cells["R1"].Value = "TITLE";
                wsSheet1.Cells["S1"].Value = "POSTED_DATE";
                wsSheet1.Cells["T1"].Value = "MANUFACTURER_NAME";

                #region excelStyling
                wsSheet1.Cells["K2:K6000"].Style.Numberformat.Format = "0.00000000";
                wsSheet1.Column(1).Hidden = true;
                wsSheet1.Column(12).Hidden = true;
                wsSheet1.Column(13).Hidden = true;
                wsSheet1.Cells["A:K"].AutoFitColumns();
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#F2DCDB");
                wsSheet1.Cells["I:I"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["I:I"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["J:J"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["J:J"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["K:K"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["K:K"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                var calcColumns = wsSheet1.Cells["I:K"];
                var calcColumnsFont = calcColumns.Style.Font;
                calcColumnsFont.Bold = true;
                calcColumnsFont.Italic = true;
                #endregion excelStyling

                wsSheet1.Cells["I2:I6000"].Formula = "IF(A2 > 0, F2*(1+(G2/100)), \"\")";
                wsSheet1.Cells["J2:J6000"].Formula = "IF(A2 > 0, H2 - I2, \"\")";
                wsSheet1.Cells["K2:K6000"].Formula = "IFERROR(J2/I2*100,\"\")";
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                Dictionary<int, Requirement> vendorsDictionary = new Dictionary<int, Requirement>();
                foreach (VendorDetails vd in requirement.AuctionVendors)
                {

                    Requirement vendorReq = GetRequirementData(reqID, vd.VendorID, sessionID);
                    vendorsDictionary.Add(vd.VendorID, vendorReq);
                }

                int index = 2;
                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    Requirement req = GetRequirementData(entry.Value.RequirementID, entry.Key, sessionID);

                    string vendorRemarks = "";

                    foreach (var vendor in entry.Value.AuctionVendors)
                    {
                        if (vendor.VendorID == entry.Key)
                        {
                            vendorRemarks = vendor.OtherProperties;
                        }
                    }

                    foreach (var item in entry.Value.ListRequirementItems)
                    {
                        var gst = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["A" + index].Value = entry.Key;
                        wsSheet1.Cells["B" + index].Value = entry.Value.AuctionVendors[0].CompanyName;
                        wsSheet1.Cells["L" + index].Value = item.ItemID;
                        wsSheet1.Cells["C" + index].Value = item.ProductNo;
                        wsSheet1.Cells["D" + index].Value = item.ProductBrand;
                        wsSheet1.Cells["E" + index].Value = item.ProductQuantity;
                        wsSheet1.Cells["H" + index].Value = item.UnitMRP;
                        wsSheet1.Cells["G" + index].Value = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["M" + index].Value = 0;
                        wsSheet1.Cells["N" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["O" + index].Value = item.ProductQuantityIn;
                        wsSheet1.Cells["P" + index].Value = vendorRemarks;
                        wsSheet1.Cells["Q" + index].Value = req.RequirementID;
                        wsSheet1.Cells["R" + index].Value = req.Title;
                        wsSheet1.Cells["S" + index].Value = req.PostedOn.ToString();
                        wsSheet1.Cells["T" + index].Value = item.OthersBrands;
                        var costPrice = (item.UnitMRP * 100 * 100) / ((item.UnitDiscount * 100) + 10000 + (item.UnitDiscount * gst) + (gst * 100));
                        wsSheet1.Cells["F" + index].Value = Math.Round(costPrice, decimal_round);
                        index++;
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion

            #region revvendorquotation
            if (template.ToUpper().Contains("MARGIN_REV_QUOTATION"))
            {
                Requirement requirement = GetRequirementData(reqID, userID, sessionID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("MARGIN_REV_QUOTATION");
                wsSheet1.Cells["A1"].Value = "VENDOR_ID";
                wsSheet1.Cells["B1"].Value = "VENDOR_NAME";
                wsSheet1.Cells["C1"].Value = "HSN_CODE";
                wsSheet1.Cells["D1"].Value = "BRAND";
                wsSheet1.Cells["E1"].Value = "QUANTITY";
                wsSheet1.Cells["F1"].Value = "COST_PRICE";
                wsSheet1.Cells["G1"].Value = "GST";
                wsSheet1.Cells["H1"].Value = "MRP";
                wsSheet1.Cells["I1"].Value = "NET_PRICE";
                wsSheet1.Cells["J1"].Value = "MARGIN_AMOUNT";
                wsSheet1.Cells["K1"].Value = "MARGIN_PERC";
                wsSheet1.Cells["L1"].Value = "ITEM_ID";
                wsSheet1.Cells["M1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["N1"].Value = "PRODUCT_NAME";
                wsSheet1.Cells["O1"].Value = "UNITS";
                wsSheet1.Cells["P1"].Value = "VENDOR_REMARK";
                wsSheet1.Cells["Q1"].Value = "REQ_ID";
                wsSheet1.Cells["R1"].Value = "TITLE";
                wsSheet1.Cells["S1"].Value = "POSTED_DATE";
                wsSheet1.Cells["T1"].Value = "MANUFACTURER_NAME";

                #region excelStyling
                wsSheet1.Cells["K2:K6000"].Style.Numberformat.Format = "0.00000000";
                wsSheet1.Column(1).Hidden = true;
                wsSheet1.Column(12).Hidden = true;
                wsSheet1.Column(13).Hidden = true;
                wsSheet1.Cells["A:K"].AutoFitColumns();
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#F2DCDB");
                wsSheet1.Cells["I:I"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["I:I"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["J:J"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["J:J"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["K:K"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["K:K"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                var calcColumns = wsSheet1.Cells["I:K"];
                var calcColumnsFont = calcColumns.Style.Font;
                calcColumnsFont.Bold = true;
                calcColumnsFont.Italic = true;
                #endregion excelStyling

                wsSheet1.Cells["I2:I6000"].Formula = "IF(A2 > 0, F2*(1+(G2/100)), \"\")";
                wsSheet1.Cells["J2:J6000"].Formula = "IF(A2 > 0, H2 - I2, \"\")";
                wsSheet1.Cells["K2:K6000"].Formula = "IFERROR(J2/I2*100,\"\")";
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                Dictionary<int, Requirement> vendorsDictionary = new Dictionary<int, Requirement>();
                foreach (VendorDetails vd in requirement.AuctionVendors)
                {
                    Requirement vendorReq = GetRequirementData(reqID, vd.VendorID, sessionID);
                    vendorsDictionary.Add(vd.VendorID, vendorReq);
                }

                int index = 2;
                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    Requirement req = GetRequirementData(entry.Value.RequirementID, entry.Key, sessionID);
                    string vendorRemarks = string.Empty;
                    foreach (var vendor in entry.Value.AuctionVendors)
                    {
                        if (vendor.VendorID == entry.Key)
                        {
                            vendorRemarks = vendor.OtherProperties;
                        }
                    }

                    foreach (var item in entry.Value.ListRequirementItems)
                    {
                        var gst = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["A" + index].Value = entry.Key;
                        wsSheet1.Cells["B" + index].Value = entry.Value.AuctionVendors[0].CompanyName;
                        wsSheet1.Cells["L" + index].Value = item.ItemID;
                        wsSheet1.Cells["C" + index].Value = item.ProductNo;
                        wsSheet1.Cells["D" + index].Value = item.ProductBrand;
                        wsSheet1.Cells["E" + index].Value = item.ProductQuantity;
                        wsSheet1.Cells["H" + index].Value = item.UnitMRP;
                        wsSheet1.Cells["G" + index].Value = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["M" + index].Value = 0;
                        wsSheet1.Cells["N" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["O" + index].Value = item.ProductQuantityIn;
                        wsSheet1.Cells["P" + index].Value = vendorRemarks;
                        wsSheet1.Cells["Q" + index].Value = req.RequirementID;
                        wsSheet1.Cells["R" + index].Value = req.Title;
                        wsSheet1.Cells["S" + index].Value = req.PostedOn.ToString(); ;
                        wsSheet1.Cells["T" + index].Value = item.OthersBrands;
                        var costPrice = 100 * item.UnitMRP / (100 + gst) * (1 + (item.UnitDiscount / 100));
                        wsSheet1.Cells["F" + index].Value = Math.Round(costPrice, decimal_round);
                        index++;
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion

            #region unitprice
            #region vendorquotation
            if (template.ToUpper().Contains("UNIT_PRICE_QUOTATION"))
            {
                Requirement requirement = GetRequirementData(reqID, userID, sessionID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("UNIT_PRICE_QUOTATION");
                wsSheet1.Cells["A1"].Value = "VENDOR_ID";
                wsSheet1.Cells["B1"].Value = "VENDOR_NAME";
                wsSheet1.Cells["C1"].Value = "PRICE";
                wsSheet1.Cells["D1"].Value = "GST";
                wsSheet1.Cells["E1"].Value = "FREIGHT";
                wsSheet1.Cells["F1"].Value = "TOTAL_PRICE";
                wsSheet1.Cells["G1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["H1"].Value = "PRODUCT_ID";
                wsSheet1.Cells["I1"].Value = "PRODUCT_NAME";
                wsSheet1.Cells["J1"].Value = "PRODUCT_NO";
                wsSheet1.Cells["K1"].Value = "BRAND";
                wsSheet1.Cells["L1"].Value = "QUANTITY";
                wsSheet1.Cells["M1"].Value = "UNITS";
                wsSheet1.Cells["N1"].Value = "DESCRIPTION";
                wsSheet1.Cells["O1"].Value = "REQUIREMENT_ID";
                wsSheet1.Cells["F2:F6000"].Formula = "IF(A2 > 0, L2*C2*(1+(D2/100)), \"\")";
                wsSheet1.Cells["A:N"].AutoFitColumns();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                Dictionary<int, Requirement> vendorsDictionary = new Dictionary<int, Requirement>();
                foreach (VendorDetails vd in requirement.AuctionVendors)
                {
                    Requirement vendorReq = GetRequirementData(reqID, vd.VendorID, sessionID);
                    vendorsDictionary.Add(vd.VendorID, vendorReq);
                }

                int index = 2;
                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    string vendorRemarks = string.Empty;
                    foreach (var vendor in entry.Value.AuctionVendors)
                    {
                        if (vendor.VendorID == entry.Key)
                        {
                            vendorRemarks = vendor.OtherProperties;
                        }
                    }

                    foreach (var item in entry.Value.ListRequirementItems)
                    {
                        var gst = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["A" + index].Value = entry.Key;
                        wsSheet1.Cells["B" + index].Value = entry.Value.AuctionVendors[0].CompanyName;
                        wsSheet1.Cells["C" + index].Value = item.RevUnitPrice;
                        wsSheet1.Cells["D" + index].Value = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["E" + index].Value = 0;//entry.Value.AuctionVendors[0].RevVendorFreight;
                        //wsSheet1.Cells["F" + index].Value = item.RevItemPrice;
                        wsSheet1.Cells["G" + index].Value = 0;
                        wsSheet1.Cells["H" + index].Value = item.ItemID;
                        wsSheet1.Cells["I" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["J" + index].Value = item.ProductNo;
                        wsSheet1.Cells["K" + index].Value = item.ProductBrand;
                        wsSheet1.Cells["L" + index].Value = item.ProductQuantity;
                        wsSheet1.Cells["M" + index].Value = item.ProductQuantityIn;
                        wsSheet1.Cells["N" + index].Value = item.ProductDescription;
                        wsSheet1.Cells["O" + index].Value = item.RequirementID;
                        index++;
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion  
            if (template.ToUpper().Contains("UNIT_QUOTATION"))
            {
                List<models.PRMTemplateFields> temlateFields = null;
                PRMCustomFieldService fieldService = new PRMCustomFieldService();
                Requirement requirement = GetRequirementData(reqID, userID, sessionID);
                var reqSettings = GetRequirementSettings(reqID, sessionID);
                if (reqSettings != null && reqSettings.Any(s => s.REQ_SETTING == "TEMPLATE_ID"))
                {
                    temlateFields = fieldService.GetTemplateFieldsTemp(Convert.ToInt32(reqSettings.First(s => s.REQ_SETTING == "TEMPLATE_ID").REQ_SETTING_VALUE), string.Empty);
                }

                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("UNIT_QUOTATION_" + reqID);
                wsSheet1.Cells["A1"].Value = "REQUIREMENT_ID";
                wsSheet1.Cells["B1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["C1"].Value = "PRODUCT_ID";
                wsSheet1.Cells["D1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_CODE")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_CODE").FIELD_LABEL : "ProductCode";
                wsSheet1.Cells["E1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_NAME")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_NAME").FIELD_LABEL : "ProductName";
                wsSheet1.Cells["F1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_NUMBER")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_NUMBER").FIELD_LABEL : "ProductNumber";
                wsSheet1.Cells["G1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_MAKE_MODEL")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_MAKE_MODEL").FIELD_LABEL : "Make";
                wsSheet1.Cells["H1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_ITEMDETAILS")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_ITEMDETAILS").FIELD_LABEL : "Description"; ;
                wsSheet1.Cells["I1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_QUANTITY")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_QUANTITY").FIELD_LABEL : "QTY";
                wsSheet1.Cells["J1"].Value = "UNIT_PRICE";

                if (requirement.IsDiscountQuotation == 1)
                {
                    wsSheet1.Cells["K1"].Value = "UNIT_DISCOUNT";
                    wsSheet1.Cells["L1"].Value = "TAX";
                    wsSheet1.Cells["M1"].Value = "TOTAL_PRICE";
                    wsSheet1.Cells["N1"].Value = "UOM";
                    wsSheet1.Cells["O1"].Value = "Regret";
                    wsSheet1.Cells["P1"].Value = "Regret Comments";
                }
                else
                {
                    wsSheet1.Cells["K1"].Value = "TAX";
                    wsSheet1.Cells["L1"].Value = "TOTAL_PRICE";
                    wsSheet1.Cells["M1"].Value = "UOM";
                    wsSheet1.Cells["N1"].Value = "Regret";
                    wsSheet1.Cells["O1"].Value = "Regret Comments";
                }

                Dictionary<int, Requirement> vendorsDictionary = new Dictionary<int, Requirement>();
                foreach (VendorDetails vd in requirement.AuctionVendors)
                {
                    Requirement vendorReq = GetRequirementData(reqID, vd.VendorID, sessionID);
                    vendorsDictionary.Add(vd.VendorID, vendorReq);
                }

                var listReqItemsTemp = vendorsDictionary[userID].ListRequirementItems;
                if (listReqItemsTemp != null && listReqItemsTemp.Count > 0)
                {
                    maxRows = 0;
                    listReqItemsTemp = listReqItemsTemp.Where(item => item.IsCoreProductCategory > 0).ToList();
                    maxRows = listReqItemsTemp.Count + 1;
                    foreach (RequirementItems reqItem in listReqItemsTemp)
                    {
                        List<CATALOG.ProductQuotationTemplate> jsonToList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CATALOG.ProductQuotationTemplate>>(reqItem.ProductQuotationTemplateJson);
                        if (jsonToList != null && jsonToList.Count > 0)
                        {
                            maxRows += jsonToList.Where(subItems => !subItems.NAME.Equals("Total", StringComparison.InvariantCultureIgnoreCase)).Count();
                        }
                    }
                }

                if (requirement.IsDiscountQuotation == 1)
                {
                    wsSheet1.Cells[$"M2:M{maxRows}"].Formula = "IF(J2 > 0, (J2-((J2*K2)/100))*(1+((L2)/100))*I2, \"0\")";
                }
                else
                {
                    wsSheet1.Cells[$"L2:L{maxRows}"].Formula = "IF(J2 > 0, J2*I2*(1+((K2)/100)), \"0\")";
                }
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A1:P1" : "A1:O1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A1:P1" : "A1:O1"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#006c4d"));//GREEN
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A1:P1" : "A1:O1"].Style.Font.Color.SetColor(Color.White);
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A1:P1" : "A1:O1"].Style.Font.Bold = true;
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A1:P1" : "A1:O1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A1:P1" : "A1:O1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                wsSheet1.Row(1).Height = 60;
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A:P" : "A:O"].AutoFitColumns();
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A:P" : "A:O"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A:P" : "A:O"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A:P" : "A:O"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A:P" : "A:O"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                wsSheet1.Cells[$"J2:J{maxRows}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells[$"J2:J{maxRows}"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));

                if (requirement.IsDiscountQuotation == 1)
                {
                    wsSheet1.Cells[$"L2:L{maxRows}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    wsSheet1.Cells[$"L2:L{maxRows}"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));
                    wsSheet1.Cells[$"K2:K{maxRows}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    wsSheet1.Cells[$"K2:K{maxRows}"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));
                }
                else
                {
                    wsSheet1.Cells[$"K2:K{maxRows}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    wsSheet1.Cells[$"K2:K{maxRows}"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));
                }
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;

                int index = 2;
                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    int parentRowNumber = 2;
                    foreach (var item in entry.Value.ListRequirementItems)
                    {
                        if (item.IsCoreProductCategory <= 0)
                        {
                            continue;
                        }

                        List<CATALOG.ProductQuotationTemplate> jsonToList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CATALOG.ProductQuotationTemplate>>(item.ProductQuotationTemplateJson);
                        wsSheet1.Cells["A" + index].Value = item.RequirementID;
                        wsSheet1.Cells["B" + index].Value = 0;
                        wsSheet1.Cells["C" + index].Value = item.ItemID;
                        wsSheet1.Cells["D" + index].Value = item.ProductCode;
                        wsSheet1.Cells["E" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["F" + index].Value = item.ProductNo;
                        wsSheet1.Cells["G" + index].Value = item.ProductBrand;
                        wsSheet1.Cells["H" + index].Value = item.ProductDescription;
                        wsSheet1.Cells["I" + index].Value = item.ProductQuantity;
                        if (requirement.IsDiscountQuotation == 1)
                        {
                            wsSheet1.Cells["J" + index].Value = item.UnitMRP;
                            wsSheet1.Cells["K" + index].Value = item.RevUnitDiscount;
                            wsSheet1.Cells["L" + index].Value = item.IGst > 0 ? item.IGst : (item.CGst + item.SGst);
                            wsSheet1.Cells["N" + index].Value = item.ProductQuantityIn;
                            wsSheet1.Cells["O" + index].Value = item.IsRegret;
                            wsSheet1.Cells["P" + index].Value = item.RegretComments;
                        }
                        else
                        {
                            wsSheet1.Cells["J" + index].Value = item.RevUnitPrice;
                            wsSheet1.Cells["K" + index].Value = item.IGst > 0 ? item.IGst : (item.CGst + item.SGst);
                            wsSheet1.Cells["M" + index].Value = item.ProductQuantityIn;
                            wsSheet1.Cells["N" + index].Value = item.IsRegret;
                            wsSheet1.Cells["O" + index].Value = item.RegretComments;
                        }

                        if (requirement.IsDiscountQuotation == 1)
                        {
                            wsSheet1.Cells["K" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            wsSheet1.Cells["K" + index].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));//Editable

                            var unitDiscountValidation = wsSheet1.DataValidations.AddDecimalValidation("K" + index);
                            unitDiscountValidation.ShowErrorMessage = true;
                            unitDiscountValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                            unitDiscountValidation.ErrorTitle = "Invalid Unit Discount";
                            unitDiscountValidation.Error = "Enter Value greater than zero.";
                            unitDiscountValidation.Operator = ExcelDataValidationOperator.greaterThanOrEqual;
                            unitDiscountValidation.Formula.Value = 0;
                        }
                        var sgstDiscValidation = wsSheet1.DataValidations.AddDecimalValidation(requirement.IsDiscountQuotation == 1 ? "L" + index : "K" + index);
                        sgstDiscValidation.ShowErrorMessage = true;
                        sgstDiscValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                        sgstDiscValidation.ErrorTitle = "Invalid Tax";
                        sgstDiscValidation.Error = "Invalid Tax value should 0 and 99.";
                        sgstDiscValidation.Operator = ExcelDataValidationOperator.between;
                        sgstDiscValidation.Formula.Value = 0;
                        sgstDiscValidation.Formula2.Value = 99;
                        var unitPriceValidation = wsSheet1.DataValidations.AddDecimalValidation("J" + index);
                        unitPriceValidation.ShowErrorMessage = true;
                        unitPriceValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                        unitPriceValidation.ErrorTitle = "Invalid Unit Price";
                        unitPriceValidation.Error = "Enter Value greater than zero.";
                        unitPriceValidation.Operator = ExcelDataValidationOperator.greaterThan;
                        unitPriceValidation.Formula.Value = 0;

                        var qtyValidation = wsSheet1.DataValidations.AddDecimalValidation("I" + index);
                        qtyValidation.ShowErrorMessage = true;
                        qtyValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                        qtyValidation.ErrorTitle = "Invalid Quantity";
                        qtyValidation.Error = "You cannot modify Quantity";
                        qtyValidation.Operator = ExcelDataValidationOperator.equal;
                        qtyValidation.Formula.Value = 0;


                        index++;
                        if (jsonToList != null && jsonToList.Count > 0)
                        {
                            if (jsonToList.Any(j => j.HAS_PRICE > 0))
                            {
                                //var unitPriceValidation = wsSheet1.DataValidations.AddDecimalValidation("H" + parentRowNumber);
                                //unitPriceValidation.ShowErrorMessage = true;
                                //unitPriceValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                                //unitPriceValidation.ErrorTitle = "Invalid Unit Price";
                                //unitPriceValidation.Error = "Enter Value greater than zero.";
                                //unitPriceValidation.Operator = ExcelDataValidationOperator.equal;
                                //unitPriceValidation.Formula.Value = 0;
                            }

                            foreach (var subItem in jsonToList)
                            {

                                if (!subItem.NAME.Equals("Total", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? $"A{parentRowNumber}:O{parentRowNumber}" : $"A{parentRowNumber}:N{parentRowNumber}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? $"A{parentRowNumber}:O{parentRowNumber}" : $"A{parentRowNumber}:N{parentRowNumber}"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#f2f2f2"));//Editable
                                    wsSheet1.Cells["C" + index].Value = $"{item.ItemID}-{subItem.T_ID}";
                                    wsSheet1.Cells["E" + index].Value = subItem.NAME;
                                    wsSheet1.Cells["H" + index].Value = subItem.SPECIFICATION;
                                    wsSheet1.Cells["I" + index].Value = subItem.HAS_QUANTITY > 0 ? subItem.CONSUMPTION : 1;

                                    if (subItem.HAS_SPECIFICATION > 0)
                                    {
                                        wsSheet1.Cells["H" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        wsSheet1.Cells["H" + index].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));//Editable
                                    }

                                    if (subItem.HAS_QUANTITY <= 0 || true)
                                    {
                                        wsSheet1.Cells["I" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        wsSheet1.Cells["I" + index].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2F2F2"));//NON-Editable
                                        var subItemValicationRule = wsSheet1.DataValidations.AddDecimalValidation("I" + index);
                                        subItemValicationRule.ShowErrorMessage = true;
                                        subItemValicationRule.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                                        subItemValicationRule.ErrorTitle = "Invalid Quantity for this Item";
                                        subItemValicationRule.Error = "You cannot modify Qantity.";
                                        subItemValicationRule.Operator = ExcelDataValidationOperator.equal;
                                        subItemValicationRule.Formula.Value = Convert.ToDouble(subItem.HAS_QUANTITY > 0 ? subItem.CONSUMPTION : 1);

                                        //wsSheet1.Cells["G" + index].Style.Locked = true;
                                    }
                                    //else
                                    //{
                                    //    wsSheet1.Cells["G" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    //    wsSheet1.Cells["G" + index].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));//Editable
                                    //}

                                    if (subItem.HAS_PRICE > 0)
                                    {
                                        wsSheet1.Cells["J" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        wsSheet1.Cells["J" + index].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));//Editable
                                        var unitPriceValidation1 = wsSheet1.DataValidations.AddDecimalValidation("J" + index);
                                        unitPriceValidation1.ShowErrorMessage = true;
                                        unitPriceValidation1.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                                        unitPriceValidation1.ErrorTitle = "Invalid Unit Price";
                                        unitPriceValidation1.Error = "Enter Value greater than zero.";
                                        unitPriceValidation1.Operator = ExcelDataValidationOperator.greaterThan;
                                        unitPriceValidation1.Formula.Value = 0;
                                    }
                                    else
                                    {

                                        wsSheet1.Cells["J" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        wsSheet1.Cells["J" + index].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2F2F2"));//NON-Editable
                                        var subItemValicationRule = wsSheet1.DataValidations.AddDecimalValidation("J" + index);
                                        subItemValicationRule.ShowErrorMessage = true;
                                        subItemValicationRule.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                                        subItemValicationRule.ErrorTitle = "Invalid Price for this Item";
                                        subItemValicationRule.Error = "You cannot modify Price.";
                                        subItemValicationRule.Operator = ExcelDataValidationOperator.equal;
                                        subItemValicationRule.Formula.Value = 0;

                                        wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "M" + index : "L" + index].Value = 0;
                                    }

                                    if (subItem.HAS_TAX > 0)
                                    {
                                        wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "L" + index : "K" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "L" + index : "K" + index].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));//Editable

                                        var sgstValidation = wsSheet1.DataValidations.AddDecimalValidation(requirement.IsDiscountQuotation == 1 ? $"L{index}" : $"K{index}");
                                        sgstValidation.ShowErrorMessage = true;
                                        sgstValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                                        sgstValidation.ErrorTitle = "Invalid Tax";
                                        sgstValidation.Error = "Invalid Tax value should 0 and 99.";
                                        sgstValidation.Operator = ExcelDataValidationOperator.between;
                                        sgstValidation.Formula.Value = 0;
                                        sgstValidation.Formula2.Value = 99;
                                    }
                                    else
                                    {

                                        wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "L" + index : "K" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "L" + index : "K" + index].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2F2F2"));//NON-Editable
                                        //wsSheet1.DataValidations.Remove(wsSheet1.DataValidations["I" + index]);
                                        var subItemValicationRule = wsSheet1.DataValidations.AddDecimalValidation(requirement.IsDiscountQuotation == 1 ? "L" + index : "K" + index);
                                        subItemValicationRule.ShowErrorMessage = true;
                                        subItemValicationRule.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                                        subItemValicationRule.ErrorTitle = "Invalid Tax for this Item";
                                        subItemValicationRule.Error = "You cannot modify Tax.";
                                        subItemValicationRule.Operator = ExcelDataValidationOperator.equal;
                                        subItemValicationRule.Formula.Value = 0;
                                    }

                                    wsSheet1.Cells["J" + index].Value = subItem.BULK_PRICE;
                                    wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "L" + index : "K" + index].Value = subItem.TAX;

                                    if (subItem.HAS_PRICE > 0)
                                    {

                                        if (requirement.IsDiscountQuotation == 1)
                                        {
                                            wsSheet1.Cells["M" + parentRowNumber].Style.Locked = true;
                                            wsSheet1.Cells["M" + parentRowNumber].Formula = $"SUM(M{parentRowNumber + 1}:M{index})*I{parentRowNumber}";

                                            wsSheet1.Cells["J" + parentRowNumber].Style.Locked = true;
                                            wsSheet1.Cells["J" + parentRowNumber].Formula = $"SUM(M{parentRowNumber + 1}:M{index})";
                                            //"IF(H2 > 0, H2*G2*(1+((I2)/100)), \"0\")";
                                            //wsSheet1.Cells["M" + index].Formula = $"IF(J{index} > 0, J{index}*I{index}*(1+((L{index})/100)), \"0\")";
                                            wsSheet1.Cells["M" + index].Formula = $"IF(J{index} > 0, (J{index}-((J{index}*K{index})/100))*(1+((L{index})/100))*I{index}, \"0\")";
                                        }
                                        else
                                        {
                                            wsSheet1.Cells["L" + parentRowNumber].Style.Locked = true;
                                            wsSheet1.Cells["L" + parentRowNumber].Formula = $"SUM(L{parentRowNumber + 1}:L{index})*I{parentRowNumber}";

                                            wsSheet1.Cells["J" + parentRowNumber].Style.Locked = true;
                                            wsSheet1.Cells["J" + parentRowNumber].Formula = $"SUM(L{parentRowNumber + 1}:L{index})";
                                            //"IF(H2 > 0, H2*G2*(1+((I2)/100)), \"0\")";
                                            wsSheet1.Cells["L" + index].Formula = $"IF(J{index} > 0, J{index}*I{index}*(1+((K{index})/100)), \"0\")";
                                        }

                                    }

                                    if (requirement.IsDiscountQuotation == 1)
                                    {
                                        wsSheet1.Cells["M" + index].Formula = $"IF(J{index} > 0, (J{index}-((J{index}*K{index})/100))*(1+((L{index})/100))*I{index}, \"0\")";

                                        wsSheet1.Cells["K" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        wsSheet1.Cells["K" + index].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2F2F2"));
                                    }

                                    index++;
                                }
                            }

                            if (!jsonToList.Any(j => j.HAS_PRICE > 0))
                            {
                                wsSheet1.Cells["J" + parentRowNumber].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                wsSheet1.Cells["J" + parentRowNumber].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));//Editable

                                if (requirement.IsDiscountQuotation == 1)
                                {

                                    wsSheet1.Cells["K" + parentRowNumber].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    wsSheet1.Cells["K" + parentRowNumber].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));
                                    wsSheet1.Cells["L" + parentRowNumber].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    wsSheet1.Cells["L" + parentRowNumber].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));//Editable   
                                }
                                else
                                {
                                    wsSheet1.Cells["K" + parentRowNumber].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    wsSheet1.Cells["K" + parentRowNumber].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));//Editable   
                                }



                            }



                        }

                        parentRowNumber = index;
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }

            if (template.ToUpper().Contains("UNIT_BIDDING"))
            {
                List<models.PRMTemplateFields> temlateFields = null;
                PRMCustomFieldService fieldService = new PRMCustomFieldService();
                Requirement requirement = GetRequirementData(reqID, userID, sessionID);
                var reqSettings = GetRequirementSettings(reqID, sessionID);
                if (reqSettings != null && reqSettings.Any(s => s.REQ_SETTING == "TEMPLATE_ID"))
                {
                    temlateFields = fieldService.GetTemplateFieldsTemp(Convert.ToInt32(reqSettings.First(s => s.REQ_SETTING == "TEMPLATE_ID").REQ_SETTING_VALUE), string.Empty);
                }

                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("UNIT_BIDDING_" + reqID);
                wsSheet1.Cells["A1"].Value = "REQUIREMENT_ID";
                wsSheet1.Cells["B1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["C1"].Value = "PRODUCT_ID";
                wsSheet1.Cells["D1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_CODE")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_CODE").FIELD_LABEL : "ProductCode";
                wsSheet1.Cells["E1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_NAME")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_NAME").FIELD_LABEL : "ProductName";
                wsSheet1.Cells["F1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_NUMBER")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_NUMBER").FIELD_LABEL : "ProductNumber";
                wsSheet1.Cells["G1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_MAKE_MODEL")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_MAKE_MODEL").FIELD_LABEL : "Make";
                wsSheet1.Cells["H1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_ITEMDETAILS")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_ITEMDETAILS").FIELD_LABEL : "Description";
                wsSheet1.Cells["I1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_QUANTITY")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_QUANTITY").FIELD_LABEL : "QTY";
                wsSheet1.Cells["J1"].Value = "QUOTATION_UNIT_PRICE";
                if (requirement.IsDiscountQuotation == 1)
                {
                    wsSheet1.Cells["K1"].Value = "UNIT_DISCOUNT";
                    wsSheet1.Cells["L1"].Value = "TAX";
                    wsSheet1.Cells["M1"].Value = "REV.UNIT_DISCOUNT";
                    wsSheet1.Cells["N1"].Value = "REV_ITEM_PRICE";
                }
                else
                {
                    wsSheet1.Cells["K1"].Value = "TAX";
                    wsSheet1.Cells["L1"].Value = "REV.UNIT_PRICE";
                    wsSheet1.Cells["M1"].Value = "REV_ITEM_PRICE";
                }


                Dictionary<int, Requirement> vendorsDictionary = new Dictionary<int, Requirement>();
                foreach (VendorDetails vd in requirement.AuctionVendors)
                {
                    Requirement vendorReq = GetRequirementData(reqID, vd.VendorID, sessionID);
                    vendorsDictionary.Add(vd.VendorID, vendorReq);
                }

                var listReqItemsTemp = vendorsDictionary[userID].ListRequirementItems;
                if (listReqItemsTemp != null && listReqItemsTemp.Count > 0)
                {
                    maxRows = 0;
                    listReqItemsTemp = listReqItemsTemp.Where(item => item.IsCoreProductCategory > 0).ToList();
                    maxRows = listReqItemsTemp.Count + 1;
                    foreach (RequirementItems reqItem in listReqItemsTemp)
                    {
                        List<CATALOG.ProductQuotationTemplate> jsonToList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CATALOG.ProductQuotationTemplate>>(reqItem.ProductQuotationTemplateJson);
                        if (jsonToList != null && jsonToList.Count > 0)
                        {
                            maxRows += jsonToList.Where(subItems => !subItems.NAME.Equals("Total", StringComparison.InvariantCultureIgnoreCase)).Count();
                        }
                    }
                }


                if (requirement.IsDiscountQuotation == 1)
                {
                    wsSheet1.Cells[$"N2:N{maxRows}"].Formula = "IF(I2 > 0, I2 *(J2-((J2*M2)/100))*(1+((L2)/100)), \"0\")";
                }
                else
                {
                    wsSheet1.Cells[$"M2:M{maxRows}"].Formula = "IF(L2 > 0, L2*I2*(1+((K2)/100)), \"0\")";
                    wsSheet1.Cells[$"L2:L{maxRows}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    wsSheet1.Cells[$"L2:L{maxRows}"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));
                }


                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A1:N1" : "A1:M1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A1:N1" : "A1:M1"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#006c4d"));//GREEN
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A1:N1" : "A1:M1"].Style.Font.Color.SetColor(Color.White);
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A1:N1" : "A1:M1"].Style.Font.Bold = true;
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A1:N1" : "A1:M1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A1:N1" : "A1:M1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                wsSheet1.Row(1).Height = 60;
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A:N" : "A:M"].AutoFitColumns();
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A:N" : "A:M"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A:N" : "A:M"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A:N" : "A:M"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? "A:N" : "A:M"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;

                int index = 2;
                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    int parentRowNumber = 2;
                    foreach (var item in entry.Value.ListRequirementItems)
                    {
                        if (item.IsItemQuotationRejected == 1 || item.IsRegret || item.IsCoreProductCategory <= 0)
                        {
                            continue;
                        }

                        List<CATALOG.ProductQuotationTemplate> jsonToList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CATALOG.ProductQuotationTemplate>>(item.ProductQuotationTemplateJson);

                        var tax = item.IGst > 0 ? item.IGst : (item.CGst + item.SGst);
                        wsSheet1.Cells["A" + index].Value = reqID;
                        wsSheet1.Cells["B" + index].Value = 0;
                        wsSheet1.Cells["C" + index].Value = item.ItemID;
                        wsSheet1.Cells["D" + index].Value = item.ProductCode;
                        wsSheet1.Cells["E" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["F" + index].Value = item.ProductNo;
                        wsSheet1.Cells["G" + index].Value = item.ProductBrand;
                        wsSheet1.Cells["H" + index].Value = item.ProductDescription;
                        wsSheet1.Cells["I" + index].Value = item.ProductQuantity;
                        if (requirement.IsDiscountQuotation == 1)
                        {
                            wsSheet1.Cells["J" + index].Value = item.UnitMRP;
                            wsSheet1.Cells["K" + index].Value = item.UnitDiscount;
                            wsSheet1.Cells["L" + index].Value = tax;
                            wsSheet1.Cells["M" + index].Value = item.RevUnitDiscount;
                        }
                        else
                        {
                            wsSheet1.Cells["J" + index].Value = item.UnitPrice;
                            wsSheet1.Cells["K" + index].Value = tax;
                            wsSheet1.Cells["L" + index].Value = item.RevUnitPrice;
                        }


                        var quantityDiscValidationRule = wsSheet1.DataValidations.AddDecimalValidation("I" + index);
                        quantityDiscValidationRule.ShowErrorMessage = true;
                        quantityDiscValidationRule.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                        quantityDiscValidationRule.ErrorTitle = "Invalid Qunatity for this Item";
                        quantityDiscValidationRule.Error = "You cannot modify Qunatity.";
                        quantityDiscValidationRule.Operator = ExcelDataValidationOperator.equal;
                        quantityDiscValidationRule.Formula.Value = 0;
                        var unitPriceDiscValidationRule = wsSheet1.DataValidations.AddDecimalValidation("J" + index);
                        unitPriceDiscValidationRule.ShowErrorMessage = true;
                        unitPriceDiscValidationRule.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                        unitPriceDiscValidationRule.ErrorTitle = "Invalid Price for this Item";
                        unitPriceDiscValidationRule.Error = "You cannot modify Price.";
                        unitPriceDiscValidationRule.Operator = ExcelDataValidationOperator.equal;
                        unitPriceDiscValidationRule.Formula.Value = 0;
                        if (requirement.IsDiscountQuotation == 1)
                        {

                            wsSheet1.Cells["M" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            wsSheet1.Cells["M" + index].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));//Editable

                            wsSheet1.Cells["N" + index].Formula = $"IF(I{index} > 0, I{index} *(J{index}-((J{index}*M{index})/100))*(1+((L{index})/100)), \"0\")";


                            var unitdiscountValidationRule = wsSheet1.DataValidations.AddDecimalValidation("K" + index);
                            unitdiscountValidationRule.ShowErrorMessage = true;
                            unitdiscountValidationRule.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                            unitdiscountValidationRule.ErrorTitle = "Invalid Discount for this Item";
                            unitdiscountValidationRule.Error = "You cannot modify Discount.";
                            unitdiscountValidationRule.Operator = ExcelDataValidationOperator.equal;
                            unitdiscountValidationRule.Formula.Value = 0;
                            var sgstDiscValidationRule = wsSheet1.DataValidations.AddDecimalValidation("L" + index);
                            sgstDiscValidationRule.ShowErrorMessage = true;
                            sgstDiscValidationRule.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                            sgstDiscValidationRule.ErrorTitle = "Invalid Tax";
                            sgstDiscValidationRule.Error = "You cannot modify Tax";
                            sgstDiscValidationRule.Operator = ExcelDataValidationOperator.equal;
                            sgstDiscValidationRule.Formula.Value = 0;
                            sgstDiscValidationRule.Formula2.Value = 99;
                            var revunitDiscountValidation = wsSheet1.DataValidations.AddDecimalValidation("M" + index);
                            revunitDiscountValidation.ShowErrorMessage = true;
                            revunitDiscountValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                            revunitDiscountValidation.ErrorTitle = "Invalid Rev. Unit Discount";
                            revunitDiscountValidation.Error = "Enter Value greater than " + item.UnitDiscount.ToString();
                            revunitDiscountValidation.Operator = ExcelDataValidationOperator.greaterThanOrEqual;
                            revunitDiscountValidation.Formula.Value = Convert.ToDouble(item.UnitDiscount);
                        }
                        else
                        {
                            wsSheet1.Cells["L" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            wsSheet1.Cells["L" + index].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));//Editable
                            var sgstDiscValidationRule = wsSheet1.DataValidations.AddDecimalValidation("K" + index);
                            sgstDiscValidationRule.ShowErrorMessage = true;
                            sgstDiscValidationRule.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                            sgstDiscValidationRule.ErrorTitle = "Invalid Tax";
                            sgstDiscValidationRule.Error = "You cannot modify Tax";
                            sgstDiscValidationRule.Operator = ExcelDataValidationOperator.equal;
                            sgstDiscValidationRule.Formula.Value = 0;
                            sgstDiscValidationRule.Formula2.Value = 99;
                            var revunitPriceValidationRule = wsSheet1.DataValidations.AddDecimalValidation("L" + index);
                            revunitPriceValidationRule.ShowErrorMessage = true;
                            revunitPriceValidationRule.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                            revunitPriceValidationRule.ErrorTitle = "Invalid Rev. Unit Price";
                            revunitPriceValidationRule.Error = "Enter Value greater than " + item.UnitPrice.ToString();
                            revunitPriceValidationRule.Operator = ExcelDataValidationOperator.greaterThan;
                            revunitPriceValidationRule.Formula.Value = Convert.ToDouble(item.UnitPrice);
                        }
                        index++;

                        if (jsonToList != null && jsonToList.Count > 0)
                        {
                            if (jsonToList.Any(j => j.HAS_PRICE > 0))
                            {
                                //var unitPriceValidation = wsSheet1.DataValidations.AddDecimalValidation("H" + parentRowNumber);
                                //unitPriceValidation.ShowErrorMessage = true;
                                //unitPriceValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                                //unitPriceValidation.ErrorTitle = "Invalid Unit Price";
                                //unitPriceValidation.Error = "Enter Value greater than zero.";
                                //unitPriceValidation.Operator = ExcelDataValidationOperator.equal;
                                //unitPriceValidation.Formula.Value = 0;
                            }

                            foreach (var subItem in jsonToList)
                            {

                                if (!subItem.NAME.Equals("Total", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? $"A{parentRowNumber}:N{parentRowNumber}" : $"A{parentRowNumber}:M{parentRowNumber}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    wsSheet1.Cells[requirement.IsDiscountQuotation == 1 ? $"A{parentRowNumber}:N{parentRowNumber}" : $"A{parentRowNumber}:M{parentRowNumber}"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#f2f2f2"));//Editable
                                    wsSheet1.Cells["C" + index].Value = $"{item.ItemID}-{subItem.T_ID}";
                                    wsSheet1.Cells["E" + index].Value = subItem.NAME;
                                    wsSheet1.Cells["H" + index].Value = subItem.SPECIFICATION;
                                    wsSheet1.Cells["I" + index].Value = subItem.HAS_QUANTITY > 0 ? subItem.CONSUMPTION : 1;

                                    wsSheet1.Cells["I" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    wsSheet1.Cells["I" + index].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2F2F2"));//NON-Editable
                                    var subItemValicationRule = wsSheet1.DataValidations.AddDecimalValidation("I" + index);
                                    subItemValicationRule.ShowErrorMessage = true;
                                    subItemValicationRule.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                                    subItemValicationRule.ErrorTitle = "Invalid Quantity for this Item";
                                    subItemValicationRule.Error = "You cannot modify Qantity.";
                                    subItemValicationRule.Operator = ExcelDataValidationOperator.equal;
                                    subItemValicationRule.Formula.Value = subItem.HAS_QUANTITY > 0 ? Convert.ToDouble(subItem.CONSUMPTION) : 1;

                                    wsSheet1.Cells["J" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    wsSheet1.Cells["J" + index].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2F2F2"));
                                    var unitPriceValidation = wsSheet1.DataValidations.AddDecimalValidation("J" + index);
                                    unitPriceValidation.ShowErrorMessage = true;
                                    unitPriceValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                                    unitPriceValidation.ErrorTitle = "Invalid Unit Price";
                                    unitPriceValidation.Error = "Enter Value greater than zero.";
                                    unitPriceValidation.Operator = ExcelDataValidationOperator.equal;
                                    unitPriceValidation.Formula.Value = Convert.ToDouble(subItem.BULK_PRICE);



                                    if (requirement.IsDiscountQuotation == 1)
                                    {
                                        wsSheet1.Cells["L" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        wsSheet1.Cells["L" + index].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2F2F2"));
                                        var subItemTaxValicationRule = wsSheet1.DataValidations.AddDecimalValidation("L" + index);
                                        subItemTaxValicationRule.ShowErrorMessage = true;
                                        subItemTaxValicationRule.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                                        subItemTaxValicationRule.ErrorTitle = "Invalid Tax for this Item";
                                        subItemTaxValicationRule.Error = "You cannot modify Tax.";
                                        subItemTaxValicationRule.Operator = ExcelDataValidationOperator.equal;
                                        subItemTaxValicationRule.Formula.Value = Convert.ToDouble(subItem.TAX);

                                        wsSheet1.Cells["N" + index].Value = subItem.REV_UNIT_PRICE;
                                        wsSheet1.Cells["J" + index].Value = subItem.BULK_PRICE;
                                        wsSheet1.Cells["M" + index].Value = subItem.REV_BULK_PRICE;
                                        wsSheet1.Cells["L" + index].Value = subItem.TAX;

                                        wsSheet1.Cells["N" + parentRowNumber].Formula = $"IF(I{parentRowNumber} > 0, I{parentRowNumber} *(J{parentRowNumber}-((J{parentRowNumber}*M{parentRowNumber})/100))*(1+((L{parentRowNumber})/100)), \"0\")";
                                        wsSheet1.Cells["M" + parentRowNumber].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        wsSheet1.Cells["M" + parentRowNumber].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));
                                    }
                                    else
                                    {
                                        wsSheet1.Cells["K" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        wsSheet1.Cells["K" + index].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2F2F2"));

                                        var subItemTaxValicationRule = wsSheet1.DataValidations.AddDecimalValidation("K" + index);
                                        subItemTaxValicationRule.ShowErrorMessage = true;
                                        subItemTaxValicationRule.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                                        subItemTaxValicationRule.ErrorTitle = "Invalid Tax for this Item";
                                        subItemTaxValicationRule.Error = "You cannot modify Tax.";
                                        subItemTaxValicationRule.Operator = ExcelDataValidationOperator.equal;
                                        subItemTaxValicationRule.Formula.Value = Convert.ToDouble(subItem.TAX);

                                        wsSheet1.Cells["M" + index].Value = subItem.REV_UNIT_PRICE;
                                        wsSheet1.Cells["J" + index].Value = subItem.BULK_PRICE;
                                        wsSheet1.Cells["L" + index].Value = subItem.REV_BULK_PRICE;
                                        wsSheet1.Cells["K" + index].Value = subItem.TAX;
                                        if (subItem.HAS_PRICE > 0 && subItem.REV_BULK_PRICE > 0)
                                        {
                                            wsSheet1.Cells["L" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            wsSheet1.Cells["L" + index].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));//Editable
                                            var revunitPriceValidation = wsSheet1.DataValidations.AddDecimalValidation("L" + index);
                                            revunitPriceValidation.ShowErrorMessage = true;
                                            revunitPriceValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                                            revunitPriceValidation.ErrorTitle = "Invalid Rev. Unit Price";
                                            revunitPriceValidation.Error = "Enter Value less than " + subItem.REV_BULK_PRICE.ToString();
                                            revunitPriceValidation.Operator = ExcelDataValidationOperator.lessThan;
                                            revunitPriceValidation.Formula.Value = Convert.ToDouble(subItem.REV_BULK_PRICE);
                                        }
                                        else
                                        {
                                            wsSheet1.Cells["L" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            wsSheet1.Cells["L" + index].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2F2F2"));//NON-Editable
                                            var revUnitPriceValidationRule = wsSheet1.DataValidations.AddDecimalValidation("L" + index);
                                            revUnitPriceValidationRule.ShowErrorMessage = true;
                                            revUnitPriceValidationRule.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                                            revUnitPriceValidationRule.ErrorTitle = "Invalid Price for this Item";
                                            revUnitPriceValidationRule.Error = "You cannot modify Price.";
                                            revUnitPriceValidationRule.Operator = ExcelDataValidationOperator.equal;
                                            revUnitPriceValidationRule.Formula.Value = 0;
                                        }

                                        if (subItem.HAS_PRICE > 0)
                                        {
                                            wsSheet1.Cells["M" + parentRowNumber].Style.Locked = true;
                                            wsSheet1.Cells["M" + parentRowNumber].Formula = $"SUM(M{parentRowNumber + 1}:M{index})*I{parentRowNumber}";
                                            wsSheet1.Cells["M" + index].Formula = $"IF(L{index} > 0, L{index}*I{index}*(1+((K{index})/100)), \"0\")";

                                            wsSheet1.Cells["L" + parentRowNumber].Style.Locked = true;
                                            wsSheet1.Cells["L" + parentRowNumber].Formula = $"SUM(M{parentRowNumber + 1}:M{index})";
                                        }
                                        else
                                        {
                                            wsSheet1.Cells["M" + parentRowNumber].Formula = $"IF(L{parentRowNumber} > 0, L{parentRowNumber}*I{parentRowNumber}*(1+((K{parentRowNumber})/100)), \"0\")";
                                        }
                                    }



                                    index++;
                                }


                            }



                            if (!jsonToList.Any(j => j.HAS_PRICE > 0))
                            {
                                //wsSheet1.Cells["G" + parentRowNumber].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                //wsSheet1.Cells["G" + parentRowNumber].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));//Editable
                                if (requirement.IsDiscountQuotation == 1)
                                {
                                    wsSheet1.Cells["L" + parentRowNumber].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    wsSheet1.Cells["L" + parentRowNumber].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2F2F2"));//Non-Editable
                                }
                                else
                                {
                                    wsSheet1.Cells["L" + parentRowNumber].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    wsSheet1.Cells["L" + parentRowNumber].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));//Editable


                                }



                            }
                        }

                        parentRowNumber = index;
                    }
                }
                if (requirement.IsDiscountQuotation == 1)
                {
                    wsSheet1.Column(7).Hidden = true;
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #region 
            if (template.ToUpper().Contains("UNIT_PRICE_BIDDING_DATA"))
            {
                Requirement requirement = GetRequirementData(reqID, userID, sessionID);
                requirement.AuctionVendors = requirement.AuctionVendors.Where(v => v.CompanyName != "PRICE_CAP" && v.IsQuotationRejected != 1).ToList();
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("UNIT_PRICE_BIDDING_DATA");

                wsSheet1.Cells["A1"].Value = "VENDOR NAME";
                wsSheet1.Cells["B1"].Value = "Vendor Rank";
                wsSheet1.Cells["C1"].Value = "Product Name";
                wsSheet1.Cells["D1"].Value = "UNIT";
                wsSheet1.Cells["E1"].Value = "QUANTITY";
                wsSheet1.Cells["F1"].Value = "Quoted Unit Price";
                wsSheet1.Cells["G1"].Value = "Quoted GST";
                wsSheet1.Cells["H1"].Value = "Final Bid Price";
                wsSheet1.Cells["I1"].Value = "Total Quoted Price(inc GST)";
                wsSheet1.Cells["J1"].Value = "Total Bid Price(inc GST)";
                wsSheet1.Cells["K1"].Value = "Earnings %";
                wsSheet1.Cells["L1"].Value = "Earning Value";
                wsSheet1.Cells["M1"].Value = "Vendor wise total earning %";
                wsSheet1.Cells["N1"].Value = "Vendor wise total earning Value";

                var calcColumns = wsSheet1.Cells["A1:N1"];
                wsSheet1.Cells.AutoFitColumns();
                var calcColumnsFont = calcColumns.Style.Font;
                calcColumns.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                //calcColumns.Style.Border.Bottom.Color.SetColor(Color.Red);
                calcColumns.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                calcColumns.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(51, 122, 183));
                calcColumnsFont.Bold = true;
                //calcColumnsFont.Size = 16;
                calcColumnsFont.Color.SetColor(Color.White);

                //calcColumnsFont.Italic = true;


                wsSheet1.Cells["A:N"].AutoFitColumns();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                foreach (var entry in requirement.AuctionVendors)
                {
                    entry.savingsPercentage = 0;
                    entry.savings = 0;

                    foreach (var item in entry.ListRequirementItems)
                    {
                        //K
                        item.savingsPercentage = Math.Round(
                            (((item.RevItemPrice - item.ItemPrice)
                            / (item.ItemPrice)) * 100),
                            decimal_round);
                        //L
                        item.savings = Math.Round((item.RevItemPrice - item.ItemPrice), decimal_round);

                    }

                    //M
                    entry.savingsPercentage = Math.Round(
                        (((entry.RevVendorTotalPrice - entry.TotalInitialPrice)
                        / (entry.TotalInitialPrice)) * 100),
                        2);

                    //N
                    entry.savings = Math.Round((entry.RevVendorTotalPrice - entry.TotalInitialPrice), decimal_round);

                }




                int index = 2;
                foreach (var entry in requirement.AuctionVendors)
                {
                    var from = index;
                    var to = index;

                    foreach (var item in entry.ListRequirementItems)
                    {
                        var gst = item.CGst + item.SGst + item.IGst;

                        wsSheet1.Cells["A" + index].Value = entry.CompanyName;
                        if (from == to)
                        {
                            wsSheet1.Cells["B" + index].Value = entry.Rank;
                        }
                        wsSheet1.Cells["C" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["D" + index].Value = item.ProductQuantityIn;
                        wsSheet1.Cells["E" + index].Value = item.ProductQuantity;
                        wsSheet1.Cells["F" + index].Value = Math.Round(item.UnitPrice * Convert.ToDouble(entry.VendorCurrencyFactor), decimal_round);
                        wsSheet1.Cells["G" + index].Value = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["H" + index].Value = Math.Round(item.RevUnitPrice * Convert.ToDouble(entry.VendorCurrencyFactor), decimal_round);
                        wsSheet1.Cells["I" + index].Value = Math.Round(item.ItemPrice * Convert.ToDouble(entry.VendorCurrencyFactor), decimal_round);
                        wsSheet1.Cells["J" + index].Value = Math.Round(item.RevItemPrice * Convert.ToDouble(entry.VendorCurrencyFactor), decimal_round);
                        wsSheet1.Cells["K" + index].Value = item.savingsPercentage;
                        wsSheet1.Cells["L" + index].Value = Math.Round(item.savings * Convert.ToDouble(entry.VendorCurrencyFactor), decimal_round);
                        wsSheet1.Cells["M" + index].Value = entry.savingsPercentage;
                        wsSheet1.Cells["N" + index].Value = Math.Round(entry.savings * Convert.ToDouble(entry.VendorCurrencyFactor), decimal_round);
                        wsSheet1.Cells.AutoFitColumns();

                        index++;
                        to = index;
                    }
                    wsSheet1.Cells["B" + from + ":B" + (to - 1)].Merge = true;
                    wsSheet1.Cells["B" + from + ":B" + (to - 1)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    wsSheet1.Cells["B" + from + ":B" + (to - 1)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    wsSheet1.Cells["B" + from + ":B" + (to - 1)].Style.Font.Size = 16;

                    wsSheet1.Cells["M" + from + ":M" + (to - 1)].Merge = true;
                    wsSheet1.Cells["M" + from + ":M" + (to - 1)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    wsSheet1.Cells["M" + from + ":M" + (to - 1)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    wsSheet1.Cells["M" + from + ":M" + (to - 1)].Style.Font.Size = 16;

                    wsSheet1.Cells["N" + from + ":N" + (to - 1)].Merge = true;
                    wsSheet1.Cells["N" + from + ":N" + (to - 1)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    wsSheet1.Cells["N" + from + ":N" + (to - 1)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    wsSheet1.Cells["N" + from + ":N" + (to - 1)].Style.Font.Size = 16;


                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion
            #endregion

            #region bulk margin quotations
            if (template.ToUpper().Contains("BULK_MARGIN_VEND_QUOTATION"))
            {
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("BULK_MARGIN_VEND_QUOTATION");
                wsSheet1.Cells["A1"].Value = "VENDOR_ID";
                wsSheet1.Cells["B1"].Value = "VENDOR_NAME";
                wsSheet1.Cells["C1"].Value = "HSN_CODE";
                wsSheet1.Cells["D1"].Value = "BRAND";
                wsSheet1.Cells["E1"].Value = "QUANTITY";
                wsSheet1.Cells["F1"].Value = "COST_PRICE";
                wsSheet1.Cells["G1"].Value = "GST";
                wsSheet1.Cells["H1"].Value = "MRP";
                wsSheet1.Cells["I1"].Value = "NET_PRICE";
                wsSheet1.Cells["J1"].Value = "MARGIN_AMOUNT";
                wsSheet1.Cells["K1"].Value = "MARGIN_PERC";
                wsSheet1.Cells["L1"].Value = "ITEM_ID";
                wsSheet1.Cells["M1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["N1"].Value = "PRODUCT_NAME";
                wsSheet1.Cells["O1"].Value = "UNITS";
                wsSheet1.Cells["P1"].Value = "VENDOR_REMARK";
                wsSheet1.Cells["Q1"].Value = "REQ_ID";
                wsSheet1.Cells["R1"].Value = "TITLE";
                wsSheet1.Cells["S1"].Value = "POSTED_DATE";
                wsSheet1.Cells["T1"].Value = "MANUFACTURER_NAME";
                wsSheet1.Cells["U1"].Value = "QUANTITY_ORIG";
                wsSheet1.Cells["V1"].Value = "QUOTATION_STATUS";
                #region excelStyling
                wsSheet1.Cells["K2:K6000"].Style.Numberformat.Format = "0.00000000";
                wsSheet1.Column(1).Hidden = true;
                wsSheet1.Column(12).Hidden = true;
                wsSheet1.Column(13).Hidden = true;
                wsSheet1.Column(21).Hidden = true;
                wsSheet1.Cells["A:K"].AutoFitColumns();
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#F2DCDB");
                wsSheet1.Cells["I:I"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["I:I"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["J:J"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["J:J"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["K:K"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["K:K"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                var calcColumns = wsSheet1.Cells["I:K"];
                var calcColumnsFont = calcColumns.Style.Font;
                calcColumnsFont.Bold = true;
                calcColumnsFont.Italic = true;
                #endregion excelStyling

                wsSheet1.Cells["I2:I6000"].Formula = "IF(A2 > 0, F2*(1+(G2/100)), \"\")";
                wsSheet1.Cells["J2:J6000"].Formula = "IF(A2 > 0, H2 - I2, \"\")";
                wsSheet1.Cells["K2:K6000"].Formula = "IFERROR(J2/I2*100,\"\")";
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                int index = 2;
                List<Requirement> requirementList = GetVendorRequirements(userID, 14, sessionID);
                foreach (Requirement req in requirementList)
                {
                    if (req.AuctionVendors.Count == 0)
                    {
                        continue;
                    }
                    if (req.PostedOn >= DateTime.UtcNow.AddMonths(-1) && req.IsDiscountQuotation == 2 && (req.StartTime == null || req.StartTime > DateTime.UtcNow))
                    {
                        if (req.RequirementID > 0 && req.PostedOn >= DateTime.UtcNow.AddMonths(-1))
                        {
                            foreach (var item in req.ListRequirementItems)
                            {
                                var gst = item.CGst + item.SGst + item.IGst;
                                wsSheet1.Cells["A" + index].Value = userID;
                                wsSheet1.Cells["B" + index].Value = req.AuctionVendors[0].CompanyName;
                                wsSheet1.Cells["L" + index].Value = item.ItemID;
                                wsSheet1.Cells["C" + index].Value = item.ProductNo;
                                wsSheet1.Cells["D" + index].Value = item.ProductBrand;
                                wsSheet1.Cells["E" + index].Value = item.ProductQuantity;
                                wsSheet1.Cells["H" + index].Value = item.UnitMRP;
                                wsSheet1.Cells["G" + index].Value = item.CGst + item.SGst + item.IGst;
                                wsSheet1.Cells["M" + index].Value = 0;
                                wsSheet1.Cells["N" + index].Value = item.ProductIDorName;
                                wsSheet1.Cells["O" + index].Value = item.ProductQuantityIn;
                                wsSheet1.Cells["P" + index].Value = req.AuctionVendors[0].OtherProperties;
                                wsSheet1.Cells["Q" + index].Value = req.RequirementID;
                                wsSheet1.Cells["R" + index].Value = req.Title;
                                wsSheet1.Cells["S" + index].Value = req.PostedOn.ToString(); ;
                                wsSheet1.Cells["T" + index].Value = item.OthersBrands;
                                wsSheet1.Cells["U" + index].Value = item.ProductQuantity;
                                wsSheet1.Cells["V" + index].Value = req.AuctionVendors[0].IsQuotationRejected == -1 ? "Pending/Not Uploaded" : (req.AuctionVendors[0].IsQuotationRejected == 0 ? "Approved" : "Rejected");
                                var costPrice = (item.UnitMRP * 100 * 100) / ((item.UnitDiscount * 100) + 10000 + (item.UnitDiscount * gst) + (gst * 100));
                                wsSheet1.Cells["F" + index].Value = Math.Round(costPrice, decimal_round);
                                index++;
                            }
                        }
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion

            #region bulk margin bidding
            if (template.ToUpper().Contains("BULK_MARGIN_VEND_BIDDING"))
            {
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("BULK_MARGIN_VEND_BIDDING");
                wsSheet1.Cells["A1"].Value = "REQ_ID";
                wsSheet1.Cells["B1"].Value = "REQ_TITLE";
                wsSheet1.Cells["C1"].Value = "POSTED_DATE";
                wsSheet1.Cells["D1"].Value = "NEGOTIATION_DATE";
                wsSheet1.Cells["E1"].Value = "VENDOR_ID";
                wsSheet1.Cells["F1"].Value = "VENDOR_NAME";
                wsSheet1.Cells["G1"].Value = "HSN_CODE";
                wsSheet1.Cells["H1"].Value = "MANUFACTURER_NAME";
                wsSheet1.Cells["I1"].Value = "BRAND";
                wsSheet1.Cells["J1"].Value = "QUANTITY";
                wsSheet1.Cells["K1"].Value = "COST_PRICE";
                wsSheet1.Cells["L1"].Value = "REV_COST_PRICE";
                wsSheet1.Cells["M1"].Value = "DIFFERENCE";
                wsSheet1.Cells["N1"].Value = "GST";
                wsSheet1.Cells["O1"].Value = "MRP";
                wsSheet1.Cells["P1"].Value = "REV_NET_PRICE";
                wsSheet1.Cells["Q1"].Value = "REV_MARGIN_AMOUNT";
                wsSheet1.Cells["R1"].Value = "REV_MARGIN_PERC";
                wsSheet1.Cells["S1"].Value = "ITEM_ID";
                wsSheet1.Cells["T1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["U1"].Value = "TOTAL_INIT_PRICE";
                wsSheet1.Cells["V1"].Value = "REV_TOTAL_PRICE";
                wsSheet1.Cells["W1"].Value = "PRODUCT_NAME";
                wsSheet1.Cells["X1"].Value = "UNITS";
                wsSheet1.Cells["Y1"].Value = "VENDOR_REMARK";
                wsSheet1.Cells["Z1"].Value = "RANK";
                wsSheet1.Cells["AA1"].Value = "REV_COST_PRICE_ORIG";

                #region excelStyling                

                wsSheet1.Cells["R2:R6000"].Style.Numberformat.Format = "0.00000000";
                wsSheet1.Cells["A:AA"].AutoFitColumns();
                wsSheet1.Column(10).Hidden = true;
                wsSheet1.Column(11).Hidden = true;
                wsSheet1.Column(14).Hidden = true;
                wsSheet1.Column(15).Hidden = true;
                wsSheet1.Column(21).Hidden = true;
                wsSheet1.Column(27).Hidden = true;
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#F2DCDB");
                Color colFromHex1 = System.Drawing.ColorTranslator.FromHtml("#DAF7A6");
                wsSheet1.Cells["P:P"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["P:P"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["Q:Q"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["Q:Q"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["R:R"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["R:R"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["L:L"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["L:L"].Style.Fill.BackgroundColor.SetColor(colFromHex1);
                var calcColumns = wsSheet1.Cells["P:R"];
                var calcColumnsFont = calcColumns.Style.Font;
                calcColumnsFont.Bold = true;
                calcColumnsFont.Italic = true;
                #endregion excelStyling

                wsSheet1.Cells["M2:M6000"].Formula = "IF(E2 > 0, ABS(K2 - L2), \"\")";
                wsSheet1.Cells["P2:P6000"].Formula = "IF(E2 > 0, L2*(1+(N2/100)), \"\")";
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                List<Requirement> requirementList = GetVendorRequirements(userID, 1, sessionID);
                int index = 2;
                foreach (Requirement req in requirementList)
                {
                    if (req.AuctionVendors.Count > 0 && (req.AuctionVendors[0].IsQuotationRejected == 1 || req.AuctionVendors[0].IsRevQuotationRejected == 1))
                    {
                        continue;
                    }
                    if (req.PostedOn >= DateTime.UtcNow.AddMonths(-1) && req.IsDiscountQuotation == 2 && (req.StartTime < DateTime.UtcNow && req.EndTime > DateTime.UtcNow))
                    {
                        if (req.AuctionVendors == null)
                        {
                            continue;
                        }
                        foreach (var item in req.ListRequirementItems)
                        {
                            var gst = item.CGst + item.SGst + item.IGst;
                            var revCostPrice = (item.UnitMRP * 100 * 100) / ((item.RevUnitDiscount * 100) + 10000 + (item.RevUnitDiscount * gst) + (gst * 100));
                            wsSheet1.Cells["A" + index].Value = req.RequirementID; // entry.Key;
                            wsSheet1.Cells["B" + index].Value = req.Title;  // entry.Value.AuctionVendors[0].CompanyName;
                            wsSheet1.Cells["C" + index].Value = req.PostedOn.ToString();  // entry.Value.AuctionVendors[0].CompanyName;
                            wsSheet1.Cells["D" + index].Value = req.StartTime.ToString();  // entry.Value.AuctionVendors[0].CompanyName;
                            wsSheet1.Cells["E" + index].Value = userID;  // entry.Value.AuctionVendors[0].CompanyName;
                            wsSheet1.Cells["F" + index].Value = req.AuctionVendors[0].CompanyName;  // entry.Value.AuctionVendors[0].CompanyName;

                            wsSheet1.Cells["G" + index].Value = item.ProductNo;
                            wsSheet1.Cells["H" + index].Value = item.OthersBrands;
                            wsSheet1.Cells["I" + index].Value = item.ProductBrand;
                            wsSheet1.Cells["J" + index].Value = item.ProductQuantity;
                            wsSheet1.Cells["L" + index].Value = Math.Round(revCostPrice, decimal_round);

                            wsSheet1.Cells["N" + index].Value = item.CGst + item.SGst + item.IGst;
                            wsSheet1.Cells["O" + index].Value = item.UnitMRP;
                            wsSheet1.Cells["S" + index].Value = item.ItemID;
                            wsSheet1.Cells["T" + index].Value = 0;
                            wsSheet1.Cells["U" + index].Value = req.AuctionVendors[0].TotalInitialPrice;
                            wsSheet1.Cells["V" + index].Value = req.AuctionVendors[0].RevVendorTotalPrice;
                            wsSheet1.Cells["W" + index].Value = item.ProductIDorName;
                            wsSheet1.Cells["X" + index].Value = item.ProductQuantityIn;
                            wsSheet1.Cells["Y" + index].Value = req.AuctionVendors[0].OtherProperties;
                            wsSheet1.Cells["Z" + index].Value = req.AuctionVendors[0].Rank > 0 ? req.AuctionVendors[0].Rank.ToString() : "NA";
                            wsSheet1.Cells["AA" + index].Value = Math.Round(revCostPrice, decimal_round);

                            if (req.IsRevUnitDiscountEnable == 0)
                            {
                                wsSheet1.Cells["Q" + index].Value = 0;
                                wsSheet1.Cells["R" + index].Value = 0;
                            }
                            if (req.IsRevUnitDiscountEnable == 1)
                            {
                                wsSheet1.Cells["Q" + index].Formula = "IF(E" + index + " > 0, ABS(O" + index + " - P" + index + "), \"\")";
                                wsSheet1.Cells["R" + index].Formula = "IFERROR(Q" + index + "/P" + index + "*100,\"\")";
                            }

                            var costPrice = (item.UnitMRP * 100 * 100) / ((item.UnitDiscount * 100) + 10000 + (item.UnitDiscount * gst) + (gst * 100));
                            wsSheet1.Cells["K" + index].Value = Math.Round(costPrice, decimal_round);
                            index++;
                        }
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion

            #region REQUIREMENT_SAVE
            if (template.ToUpper().Contains("REQUIREMENT_SAVE"))
            {
                List<models.PRMTemplateFields> temlateFields = null;
                PRMCustomFieldService fieldService = new PRMCustomFieldService();
                Requirement requirement = new Requirement();
                if (reqID > 0)
                {
                    requirement = GetRequirementData(reqID, userID, sessionID);
                }

                if (templateid > 0)
                {
                    temlateFields = fieldService.GetTemplateFieldsTemp(templateid, string.Empty);
                }

                PRMCatalogService catalogService = new PRMCatalogService();
                var companyUnits = service.GetCompanyConfiguration(compID, "ITEM_UNITS", sessionID);
                List<Product> products = catalogService.GetProducts(compID, sessionID);
                products = products.Where(p => (p.IsValid > 0 && !string.IsNullOrEmpty(p.ProductCode))).ToList();
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("REQUIREMENT_SAVE");
                ExcelWorksheet wsSheet2 = ExcelPkg.Workbook.Worksheets.Add("REQUIREMENT_SAVE_CONFIG");
                wsSheet1.Cells["A1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_CODE")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_CODE").FIELD_LABEL : "ProductCode";
                wsSheet1.Cells["B1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_NAME")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_NAME").FIELD_LABEL : "ProductName";
                wsSheet1.Cells["C1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_NUMBER")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_NUMBER").FIELD_LABEL : "ProductNumber";
                wsSheet1.Cells["D1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_QUANTITY")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_QUANTITY").FIELD_LABEL : "Quantity";
                wsSheet1.Cells["E1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_UNITS")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_UNITS").FIELD_LABEL : "Units";
                wsSheet1.Cells["F1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_MAKE_MODEL")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_MAKE_MODEL").FIELD_LABEL : "Make";
                wsSheet1.Cells["G1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_DELIVERY_DETAILS")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_DELIVERY_DETAILS").FIELD_LABEL : "DeliveryDetails";
                wsSheet1.Cells["H1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_ITEMDETAILS")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_ITEMDETAILS").FIELD_LABEL : "Description";
                wsSheet1.Cells["A:H"].AutoFitColumns();
                var validation = wsSheet1.DataValidations.AddListValidation($"E2:E{maxRows}");
                validation.ShowErrorMessage = true;
                validation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                validation.ErrorTitle = "Invalid Units";
                validation.Error = "Invalid Units Selected";

                var quantityPriceValidation = wsSheet1.DataValidations.AddDecimalValidation($"D2:D{maxRows}");
                quantityPriceValidation.ShowErrorMessage = true;
                quantityPriceValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                quantityPriceValidation.ErrorTitle = "Invalid Quantity";
                quantityPriceValidation.Error = "Enter Value greater than zero.";
                quantityPriceValidation.Operator = ExcelDataValidationOperator.greaterThan;
                quantityPriceValidation.Formula.Value = 0;
                wsSheet1.Cells[$"A2:A{maxRows}"].Style.Numberformat.Format = "@";

                int count = 1;
                foreach (var unit in companyUnits)
                {
                    wsSheet2.Cells["A" + count.ToString()].Value = unit.ConfigValue;
                    count++;
                }

                validation.Formula.ExcelFormula = "REQUIREMENT_SAVE_CONFIG!$A1:$A" + (count - 1).ToString();

                //for (int i = 2; i <= maxRows; i++)
                //{
                //    wsSheet1.Cells["B" + (i)].Formula = $"IFNA(INDIRECT(\"REQUIREMENT_SAVE_CONFIG!C\" & MATCH(A{i}, REQUIREMENT_SAVE_CONFIG!$B:$B,0 )),\"\")";
                //    wsSheet1.Cells["C" + (i)].Formula = $"IFNA(INDIRECT(\"REQUIREMENT_SAVE_CONFIG!D\" & MATCH(A{i}, REQUIREMENT_SAVE_CONFIG!$B:$B,0 )),\"\")";
                //}


                if (requirement != null && requirement.ListRequirementItems != null && requirement.ListRequirementItems.Count > 0)
                {
                    int index = 2;
                    foreach (var item in requirement.ListRequirementItems)
                    {
                        if (item.IsCoreProductCategory <= 0)
                        {
                            continue;
                        }

                        wsSheet1.Cells["A" + index].Value = item.ProductCode;
                        wsSheet1.Cells["B" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["C" + index].Value = item.ProductNo;
                        wsSheet1.Cells["D" + index].Value = item.ProductQuantity;
                        wsSheet1.Cells["E" + index].Value = item.ProductQuantityIn;
                        wsSheet1.Cells["F" + index].Value = item.ProductBrand;
                        wsSheet1.Cells["G" + index].Value = item.ProductDeliveryDetails;
                        wsSheet1.Cells["H" + index].Value = item.ProductDescriptionCustomer;
                        index++;
                    }
                }


                //var productValidation = wsSheet1.DataValidations.AddListValidation($"A2:A{maxRows}");
                //productValidation.ShowErrorMessage = true;
                //productValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                //productValidation.ErrorTitle = "Invalid Product";
                //productValidation.Error = "Invalid Product entered";



                count = 1;
                foreach (var product in products)
                {
                    wsSheet2.Cells["B" + count.ToString()].Value = product.ProductCode;
                    wsSheet2.Cells["C" + count.ToString()].Value = product.ProductName;
                    wsSheet2.Cells["D" + count.ToString()].Value = product.ProductNo;
                    count++;
                }

                // productValidation.Formula.ExcelFormula = "=REQUIREMENT_SAVE_CONFIG!$B1:$B" + (count - 1).ToString();


                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                wsSheet2.Protection.IsProtected = false;
                wsSheet2.Protection.AllowSelectLockedCells = false;
                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion

            #region CEILING_PRICE
            if (template.ToUpper().Contains("CEILING_DETAILS"))
            {
                List<models.PRMTemplateFields> temlateFields = null;
                PRMCustomFieldService fieldService = new PRMCustomFieldService();
                Requirement requirement = GetRequirementData(reqID, userID, sessionID);
                var reqSettings = GetRequirementSettings(reqID, sessionID);
                if (reqSettings != null && reqSettings.Any(s => s.REQ_SETTING == "TEMPLATE_ID"))
                {
                    temlateFields = fieldService.GetTemplateFieldsTemp(Convert.ToInt32(reqSettings.First(s => s.REQ_SETTING == "TEMPLATE_ID").REQ_SETTING_VALUE), string.Empty);
                }

                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("CEILING_DETAILS");
                wsSheet1.Cells["A1"].Value = "PRODUCT_ID";
                wsSheet1.Cells["B1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_CODE")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_CODE").FIELD_LABEL : "ProductCode";
                wsSheet1.Cells["C1"].Value = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_NAME")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_NAME").FIELD_LABEL : "ProductName";
                wsSheet1.Cells["D1"].Value = "UNIT_PRICE";
                wsSheet1.Cells["E1"].Value = "TAX";
                wsSheet1.Cells["F1"].Value = "CEILING_PRICE";
                wsSheet1.Cells["G1"].Value = "REQUIREMENT_ID";
                wsSheet1.Cells["H1"].Value = "USER_ID";
                wsSheet1.Cells["I1"].Value = "COMPANY_NAME";
                wsSheet1.Cells["J1"].Value = "USER_TYPE";

                maxRows = requirement.ListRequirementItems.Count + 1;
                wsSheet1.Cells["A1:J1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["A1:J1"].Style.Font.Color.SetColor(Color.White);
                wsSheet1.Cells["A1:J1"].Style.Font.Bold = true;
                wsSheet1.Cells["A1:J1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                wsSheet1.Cells["A1:J1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                wsSheet1.Row(1).Height = 60;
                wsSheet1.Cells["A:J"].AutoFitColumns();
                wsSheet1.Cells["A:J"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                wsSheet1.Cells["A:J"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                wsSheet1.Cells["A:J"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                wsSheet1.Cells["A:J"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;

                var requirementItems = requirement.ListRequirementItems;
                string companyName = requirement.CustomerCompanyName;
                string userType = "CUSTOMER";
                if (requirement.AuctionVendors != null && requirement.AuctionVendors.Count > 0 && requirement.AuctionVendors.Any(v => v.VendorID == userID))
                {
                    requirementItems = requirement.AuctionVendors.First(v => v.VendorID == userID).ListRequirementItems;
                    companyName = requirement.AuctionVendors.First(v => v.VendorID == userID).CompanyName;
                    userType = "VENDOR";
                }

                int index = 2;
                foreach (var item in requirement.ListRequirementItems)
                {
                    if (item.IsCoreProductCategory <= 0)
                    {
                        continue;
                    }

                    wsSheet1.Cells["A" + index].Value = item.ItemID;
                    wsSheet1.Cells["B" + index].Value = item.ProductCode;
                    wsSheet1.Cells["C" + index].Value = item.ProductIDorName;
                    wsSheet1.Cells["D" + index].Value = item.UnitPrice;
                    wsSheet1.Cells["E" + index].Value = item.IGst > 0 ? item.IGst : (item.CGst + item.SGst);
                    wsSheet1.Cells["F" + index].Value = item.CeilingPrice;
                    wsSheet1.Cells["G" + index].Value = item.RequirementID;
                    wsSheet1.Cells["H" + index].Value = userID;
                    wsSheet1.Cells["I" + index].Value = companyName;
                    wsSheet1.Cells["J" + index].Value = userType;
                    index++;
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion
            return Convert.ToBase64String(ms.ToArray());
        }

        public LiveBidding[] GetLiveBiddingReport(int reqID, int count, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            if (count <= 0)
            {
                count = 10;
            }

            List<LiveBidding> details = new List<LiveBidding>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_COUNT", count);
                DataSet ds = sqlHelper.SelectList("fwd_rp_GetLiveBiddingReport", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        double factor = 1;
                        string vendorCurrency = row["SELECTED_VENDOR_CURRENCY"] != DBNull.Value ? row["SELECTED_VENDOR_CURRENCY"].ToString() : string.Empty;
                        string requirementCurrency = row["REQ_CURRENCY"] != DBNull.Value ? row["REQ_CURRENCY"].ToString() : string.Empty;
                        LiveBidding detail = ReportUtility.GetLiveBiddingReportObject(row, count, factor);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                details.Clear();
                LiveBidding error = new LiveBidding();
                error.ErrorMessage = ex.Message;
                details.Add(error);
            }

            return details.ToArray();
        }

        public ReportsRequirement GetReqDetails(int reqID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            ReportsRequirement reportsrequirement = new ReportsRequirement();
            try
            {
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("fwd_rp_GetReqDetails", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    reportsrequirement.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                    reportsrequirement.CustomerID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                    reportsrequirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                    reportsrequirement.PostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                    reportsrequirement.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                    reportsrequirement.EndTime = row["END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["END_TIME"]) : DateTime.MaxValue;
                    reportsrequirement.NegotiationDuration = row["NEGOTIATION_DURATION"] != DBNull.Value ? Convert.ToString(row["NEGOTIATION_DURATION"]) : string.Empty;
                    reportsrequirement.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                    reportsrequirement.Savings = reportsrequirement.Savings;
                    reportsrequirement.IsTabular = row["REQ_IS_TABULAR"] != DBNull.Value ? (Convert.ToInt32(row["REQ_IS_TABULAR"]) == 1 ? true : false) : false;
                    reportsrequirement.IsUnitPriceBidding = row["IS_UNIT_PRICE_BIDDING"] != DBNull.Value ? Convert.ToInt32(row["IS_UNIT_PRICE_BIDDING"]) : 0;
                    reportsrequirement.NoOfVendorsInvited = row["NO_OF_VENDORS_INVITED"] != DBNull.Value ? Convert.ToInt32(row["NO_OF_VENDORS_INVITED"]) : 0;
                    reportsrequirement.NoOfVendorsParticipated = row["NO_OF_VENDORS_PARTICIPATED"] != DBNull.Value ? Convert.ToInt32(row["NO_OF_VENDORS_PARTICIPATED"]) : 0;
                }
            }
            catch (Exception ex)
            {

            }

            return reportsrequirement;
        }

        public ItemWiseReport[] GetItemWiseReport(int reqID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<ItemWiseReport> details = new List<ItemWiseReport>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("fwd_rp_GetItemWiseReport", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    var factor = 1;// Utilities.GetRequirementCurrencyFactor(reqID);
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        ItemWiseReport detail = ReportUtility.GetItemWiseReportObject(row, factor);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                details.Clear();
                ItemWiseReport error = new ItemWiseReport();
                error.ErrorMessage = ex.Message;
                details.Add(error);
            }

            return details.ToArray();
        }

        public DeliveryTimeLine[] GetDeliveryTimeLineReport(int reqID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<DeliveryTimeLine> details = new List<DeliveryTimeLine>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("fwd_rp_GetDeliveryTimeLineReport", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    var factor = 1;
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        DeliveryTimeLine detail = ReportUtility.GetIDeliveryTimelineObject(row, Convert.ToDecimal(factor));
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                details.Clear();
                DeliveryTimeLine error = new DeliveryTimeLine();
                error.ErrorMessage = ex.Message;
                details.Add(error);
            }

            return details.ToArray();
        }

        public DeliveryTimeLine[] GetPaymentTermsReport(int reqID, string sessionID)
        {
            List<DeliveryTimeLine> details = new List<DeliveryTimeLine>();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("fwd_rp_GetPaymentTermsReport", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    var factor = 1;
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        DeliveryTimeLine detail = ReportUtility.GetIDeliveryTimelineObject(row, Convert.ToDecimal(factor));
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                details.Clear();
                DeliveryTimeLine error = new DeliveryTimeLine();
                error.ErrorMessage = ex.Message;
                details.Add(error);
            }

            return details.ToArray();
        }

        public List<CBPrices> GetCBPricesAudit(int reqID, int vendorID, string sessionID)
        {
            List<CBPrices> details = new List<CBPrices>();
            List<CBItemPrices> itemDetails = new List<CBItemPrices>();
            try
            {
                Utilities.ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };

                CORE.DataNamesMapper<CBPrices> mapper = new CORE.DataNamesMapper<CBPrices>();
                CORE.DataNamesMapper<CBItemPrices> mapperItemList = new CORE.DataNamesMapper<CBItemPrices>();

                string query = string.Format("call fwd_GetCBPricesAudit({0}, {1});", reqID, vendorID);

                var dataset = sqlHelper.ExecuteQuery(query);
                details = mapper.Map(dataset.Tables[0]).ToList();
                itemDetails = mapperItemList.Map(dataset.Tables[1]).ToList();


                foreach (CBPrices bid in details)
                {
                    bid.CBItemPricesList = new List<CBItemPrices>();
                    bid.CBItemPricesList = itemDetails.Where(i => i.CB_AH_ID == bid.CB_AH_ID).ToList();
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data GetCBPricesAudit");
            }

            return details;
        }

        public List<RequirementItems> GetItemBidHistory(int reqID, int userID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<Comment> bidHistory = new List<Comment>();
            List<RequirementItems> itemsList = new List<RequirementItems>();

            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("fwd_GetItemAudit", sd);

                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row1 in ds.Tables[1].Rows)
                    {
                        RequirementItems item = new RequirementItems();

                        item.ItemID = row1["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row1["ITEM_ID"]) : 0;
                        item.RequirementID = row1["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row1["REQ_ID"]) : 0;
                        item.ProductIDorName = row1["PROD_ID"] != DBNull.Value ? Convert.ToString(row1["PROD_ID"]) : string.Empty;
                        item.ProductNo = row1["PROD_NO"] != DBNull.Value ? Convert.ToString(row1["PROD_NO"]) : string.Empty;

                        itemsList.Add(item);
                    }
                }

                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        Comment history = new Comment();

                        history.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : -1;
                        history.ItemID = row["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row["ITEM_ID"]) : -1;
                        history.UserID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : -1;
                        history.FirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                        history.LastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                        history.CreatedTime = row["BID_TIME"] != DBNull.Value ? Convert.ToDateTime(row["BID_TIME"]) : DateTime.MaxValue;
                        history.BidAmount = row["REV_UNIT_PRICE"] != DBNull.Value ? Utilities.RoundValue(Convert.ToDouble(row["REV_UNIT_PRICE"])) : 0;
                        history.Currency = row["REQ_CURRENCY"] != DBNull.Value ? Convert.ToString(row["REQ_CURRENCY"]) : string.Empty;
                        history.RejectReson = row["COMMENTS"] != DBNull.Value ? Convert.ToString(row["COMMENTS"]) : string.Empty;
                        history.CompanyName = row["INSTITUTION"] != DBNull.Value ? Convert.ToString(row["INSTITUTION"]) : string.Empty;
                        history.ProductName = row["PRODUCT_NAME"] != DBNull.Value ? Convert.ToString(row["PRODUCT_NAME"]) : string.Empty;



                        bidHistory.Add(history);
                    }

                }

                foreach (RequirementItems items in itemsList)
                {
                    items.BidHistory = bidHistory.Where(bid => bid.ItemID == items.ItemID).ToList();
                }
            }
            catch (Exception ex)
            {
                Comment history = new Comment();
                history.ErrorMessage = ex.Message;
                bidHistory.Add(history);
            }

            return itemsList;
        }

        public List<RequirementItems> uploadRequirementItemsSaveExcel(int reqID, bool isrfp, int userID, int compId, string sessionID, byte[] requirementItemsAttachment, int templateid)
        {
            List<RequirementItems> items = new List<RequirementItems>();
            string sheetName = string.Empty;
            if (requirementItemsAttachment != null)
            {
                DataTable currentData = new DataTable();
                using (MemoryStream ms = new MemoryStream())
                {
                    ms.Write(requirementItemsAttachment, 0, requirementItemsAttachment.Length);
                    using (ExcelPackage package = new ExcelPackage(ms))
                    {
                        currentData = Utilities.WorksheetToDataTable(package.Workbook.Worksheets[1]);
                        sheetName = package.Workbook.Worksheets[1].Name;
                    }
                }

                if (sheetName.Equals("REQUIREMENT_SAVE", StringComparison.InvariantCultureIgnoreCase))
                {
                    List<models.PRMTemplateFields> temlateFields = null;
                    PRMCustomFieldService fieldService = new PRMCustomFieldService();
                    PRMCatalogService catalogService = new PRMCatalogService();
                    PRMServices prmService = new PRMServices();
                    List<CATALOG.Product> products = catalogService.GetProducts(compId, sessionID);
                    if (templateid > 0)
                    {
                        temlateFields = fieldService.GetTemplateFieldsTemp(templateid, string.Empty);
                    }

                    var companyUnits = prmService.GetCompanyConfiguration(compId, "ITEM_UNITS", sessionID);

                    string productCodeLabel = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_CODE")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_CODE").FIELD_LABEL : "ProductCode";
                    string productNameLabel = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_NAME")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_NAME").FIELD_LABEL : "ProductName";
                    string productNumberLabel = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_NUMBER")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_NUMBER").FIELD_LABEL : "ProductNumber";
                    string quantityLabel = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_QUANTITY")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_QUANTITY").FIELD_LABEL : "Quantity";
                    string unitsLabel = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_UNITS")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_UNITS").FIELD_LABEL : "Units";
                    string makeLabel = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_MAKE_MODEL")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_MAKE_MODEL").FIELD_LABEL : "Make";

                    string descriptionLabel = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_ITEMDETAILS")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_ITEMDETAILS").FIELD_LABEL : "Description";
                    string deliveryDetailsLabel = (temlateFields != null && temlateFields.Any(t => t.FIELD_NAME == "PRODUCT_DELIVERY_DETAILS")) ? temlateFields.First(t => t.FIELD_NAME == "PRODUCT_DELIVERY_DETAILS").FIELD_LABEL : "DeliveryDetails";
                    var rows = currentData.AsEnumerable().Where(r => (r[productCodeLabel] != null && r[productCodeLabel] != DBNull.Value && !string.IsNullOrEmpty(r[productCodeLabel].ToString())) ||
                    (r[productNameLabel] != null && r[productNameLabel] != DBNull.Value && !string.IsNullOrEmpty(r[productNameLabel].ToString()) && !r[productNameLabel].ToString().Equals("#NAME?", StringComparison.InvariantCultureIgnoreCase)));
                    foreach (DataRow row in rows)
                    {
                        try
                        {
                            Response response2 = new Response();
                            RequirementItems item = new RequirementItems();

                            int productId = 0;
                            string productCode = (row.IsNull(productCodeLabel) || row[productCodeLabel] == DBNull.Value || string.IsNullOrEmpty(row[productCodeLabel].ToString().Trim())) ? string.Empty : Convert.ToString(row[productCodeLabel].ToString().Trim());
                            string productName = (row.IsNull(productNameLabel) || row[productNameLabel] == DBNull.Value || string.IsNullOrEmpty(row[productNameLabel].ToString().Trim())) ? string.Empty : Convert.ToString(row[productNameLabel].ToString().Trim());
                            string productNumber = (row.IsNull(productNumberLabel) || row[productNumberLabel] == DBNull.Value || string.IsNullOrEmpty(row[productNumberLabel].ToString().Trim())) ? string.Empty : Convert.ToString(row[productNumberLabel].ToString().Trim());
                            double quantity = (row.IsNull(quantityLabel) || row[quantityLabel] == DBNull.Value || string.IsNullOrEmpty(row[quantityLabel].ToString().Trim())) ? 0 : Convert.ToDouble(row[quantityLabel].ToString().Trim());
                            string units = (row.IsNull(unitsLabel) || row[unitsLabel] == DBNull.Value || string.IsNullOrEmpty(row[unitsLabel].ToString().Trim())) ? string.Empty : Convert.ToString(row[unitsLabel].ToString().Trim());
                            string make = (row.IsNull(makeLabel) || row[makeLabel] == DBNull.Value || string.IsNullOrEmpty(row[makeLabel].ToString().Trim())) ? string.Empty : Convert.ToString(row[makeLabel].ToString().Trim());
                            string description = (row.IsNull(descriptionLabel) || row[descriptionLabel] == DBNull.Value || string.IsNullOrEmpty(row[descriptionLabel].ToString().Trim())) ? string.Empty : Convert.ToString(row[descriptionLabel].ToString().Trim());
                            string deliveryDetails = (row.IsNull(deliveryDetailsLabel) || row[deliveryDetailsLabel] == DBNull.Value || string.IsNullOrEmpty(row[deliveryDetailsLabel].ToString().Trim())) ? string.Empty : Convert.ToString(row[deliveryDetailsLabel].ToString().Trim());
                            var product = products.FirstOrDefault(p => (!string.IsNullOrEmpty(p.ProductCode) && p.ProductCode.Equals(productCode, StringComparison.InvariantCultureIgnoreCase)));

                            units = companyUnits.First(c => c.ConfigValue.Equals(units, StringComparison.InvariantCultureIgnoreCase)).ConfigValue;
                            if (product != null)
                            {
                                productId = product.ProductId;
                            }

                            if (productId > 0 || isrfp)
                            {
                                item.ProductCode = productCode;
                                item.ProductIDorName = product != null ? product.ProductName : productName;
                                item.ProductQuantity = quantity;
                                item.ProductNo = product != null ? product.ProductNo : productNumber;
                                item.HsnCode = product != null ? product.ProductHSNCode : "";
                                item.ProductQuantityIn = units;
                                item.CatalogueItemID = productId;
                                item.ProductBrand = make;
                                item.ProductDescription = description;
                                item.ProductDeliveryDetails = deliveryDetails;
                                items.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            RequirementItems err = new RequirementItems();
                            err.ErrorMessage = ex.Message + ex.StackTrace;
                            items.Add(err);
                        }
                    }
                }
            }

            return items;
        }

        public Dictionary<string, List<Audit>> GetRequirementAudit(int reqID, string sessionID)
        {
            Dictionary<string, List<Audit>> audits = new Dictionary<string, List<Audit>>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("fwd_GetRequirementAuditData", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    List<Audit> requirementAudits = new List<Audit>();
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        Audit audit = new Audit();
                        audit.DateCreated = row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]) : DateTime.MinValue;
                        audit.AuditBy = row["FULL_NAME"] != DBNull.Value ? Convert.ToString(row["FULL_NAME"]) : string.Empty;
                        audit.AuditComments = row["AUDIT_COMMENTS"] != DBNull.Value ? Convert.ToString(row["AUDIT_COMMENTS"]) : string.Empty;
                        audit.AuditVersion = row["AUDIT_VERSION"] != DBNull.Value ? Convert.ToString(row["AUDIT_VERSION"]) : string.Empty;

                        requirementAudits.Add(audit);
                    }

                    audits.Add("versions", requirementAudits);
                }

                if (ds != null && ds.Tables.Count > 1)
                {
                    List<Audit> requirementAudits = new List<Audit>();
                    foreach (DataRow row in ds.Tables[1].Rows)
                    {
                        Audit audit = new Audit();

                        audit.DateCreated = row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]) : DateTime.MinValue;
                        audit.AuditBy = row["FULL_NAME"] != DBNull.Value ? Convert.ToString(row["FULL_NAME"]) : string.Empty;
                        audit.ActionType = row["ACTION_TYPE"] != DBNull.Value ? Convert.ToString(row["ACTION_TYPE"]) : string.Empty;
                        audit.MoreDetails = row["ProductName"] != DBNull.Value ? Convert.ToString(row["ProductName"]) : string.Empty;
                        audit.ColumnNameDesc = row["COLUMN_NAME_DESC"] != DBNull.Value ? Convert.ToString(row["COLUMN_NAME_DESC"]) : string.Empty;
                        audit.ColumnName = row["COLUMN_NAME"] != DBNull.Value ? Convert.ToString(row["COLUMN_NAME"]) : string.Empty;
                        audit.OldValue = row["OLD_VALUE"] != DBNull.Value ? Convert.ToString(row["OLD_VALUE"]) : string.Empty;
                        audit.NewValue = row["NEW_VALUE"] != DBNull.Value ? Convert.ToString(row["NEW_VALUE"]) : string.Empty;
                        audit.AuditVersion = row["AUDIT_VERSION"] != DBNull.Value ? Convert.ToString(row["AUDIT_VERSION"]) : string.Empty;

                        requirementAudits.Add(audit);
                    }

                    audits.Add("details", requirementAudits);
                }
            }
            catch (Exception ex)
            {

            }

            return audits;
        }
        #endregion publicservices


        private Response SaveRequirementItems(RequirementItems RequirementItems, int reqID, List<VendorDetails> vendors, string sessionID, int userID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {
                string fileName = string.Empty;
                if (!string.IsNullOrEmpty(RequirementItems.AttachmentBase64))
                {
                    string[] stringSeparators = new string[] { "base64," };
                    string tempAttach = RequirementItems.AttachmentBase64;
                    if (RequirementItems.AttachmentBase64.ToLower().Contains("base64"))
                    {
                        tempAttach = RequirementItems.AttachmentBase64.Split(stringSeparators, StringSplitOptions.None)[1];
                    }

                    RequirementItems.ItemAttachment = Convert.FromBase64String(tempAttach);
                }

                if (RequirementItems.ItemAttachment != null && RequirementItems.ItemAttachment.Length > 0)
                {
                    long tick = DateTime.UtcNow.Ticks;
                    fileName = System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "req" + tick + "_user" + userID + "_" + RequirementItems.AttachmentName);
                    SaveFile(fileName, RequirementItems.ItemAttachment);

                    fileName = "req" + tick + "_user" + userID + "_" + RequirementItems.AttachmentName;
                    Response res = SaveAttachment(fileName);
                    if (res.ErrorMessage != "")
                    {
                        response.ErrorMessage = res.ErrorMessage;
                    }
                    fileName = res.ObjectID.ToString();
                }
                else if (RequirementItems.ProductImageID > 0)
                {
                    fileName = Convert.ToString(RequirementItems.ProductImageID);
                }

                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_ITEM_ID", RequirementItems.ItemID);
                sd.Add("P_PROD_ID", RequirementItems.ProductIDorName);
                sd.Add("P_PROD_NO", RequirementItems.ProductNo);
                sd.Add("P_PROD_CODE", RequirementItems.ProductCode);
                sd.Add("P_DESCRIPTION", RequirementItems.ProductDescription == null ? string.Empty : RequirementItems.ProductDescription.Replace("'", ""));
                // sd.Add("P_QUANTITY", RequirementItems.ProductQuantity);
                sd.Add("P_QUANTITY_IN", RequirementItems.ProductQuantityIn);
                sd.Add("P_BRAND", RequirementItems.ProductBrand == null ? string.Empty : RequirementItems.ProductBrand.Replace("'", ""));
                sd.Add("P_OTHER_BRAND", RequirementItems.OthersBrands == null ? string.Empty : RequirementItems.OthersBrands.Replace("'", ""));
                sd.Add("P_ITEM_MIN_REDUCTION", RequirementItems.ItemMinReduction);
                sd.Add("P_IMAGE_ID", fileName);
                sd.Add("P_U_ID", userID);
                sd.Add("P_IS_DELETED", RequirementItems.IsDeleted);
                sd.Add("P_LAST_ITEM_PRICE", RequirementItems.ItemLastPrice);
                sd.Add("P_I_LLP_DETAILS", RequirementItems.I_LLP_DETAILS);
                sd.Add("P_CATALOGUE_ITEM_ID", RequirementItems.CatalogueItemID);
                sd.Add("P_HSN_CODE", RequirementItems.HsnCode == null ? string.Empty : RequirementItems.HsnCode.Replace("'", ""));
                sd.Add("P_CEILING_PRICE", RequirementItems.CeilingPrice);

                string json = string.Empty;
                List<CATALOG.ProductQuotationTemplate> listObj = new List<CATALOG.ProductQuotationTemplate>();
                List<CATALOG.ProductQuotationTemplate> jsonToList = new List<CATALOG.ProductQuotationTemplate>();

                CATALOG.ProductQuotationTemplate obj1 = new CATALOG.ProductQuotationTemplate();
                obj1.NAME = "Total";
                obj1.IS_CALCULATED = 1;
                obj1.IS_VALID = 1;

                if (RequirementItems != null && RequirementItems.ProductQuotationTemplateJson != null && RequirementItems.ProductQuotationTemplateJson != "")
                {
                    jsonToList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CATALOG.ProductQuotationTemplate>>(RequirementItems.ProductQuotationTemplateJson);

                    jsonToList.Add(obj1);
                    json = JsonConvert.SerializeObject(jsonToList);
                }

                sd.Add("P_PRODUCT_QUOTATION_JSON", json);

                sd.Add("P_QUANTITY", RequirementItems.SplitEnabled ? 1 : RequirementItems.ProductQuantity);
                sd.Add("P_SPLIT_ENABLED", RequirementItems.SplitEnabled ? 1 : 0);
                sd.Add("P_FROMRANGE", RequirementItems.SplitEnabled ? RequirementItems.FromRange : 0);
                sd.Add("P_TORANGE", RequirementItems.SplitEnabled ? RequirementItems.ToRange : 0);
                sd.Add("P_REQ_QTY", RequirementItems.SplitEnabled ? RequirementItems.RequiredQuantity : 0);
                RequirementItems.ProductDeliveryDetails = !string.IsNullOrEmpty(RequirementItems.ProductDeliveryDetails) ? RequirementItems.ProductDeliveryDetails.Replace("'", "") : "";
                sd.Add("P_DELIERY_DETAILS", RequirementItems.ProductDeliveryDetails ?? string.Empty);
                DataSet ds = sqlHelper.SelectList("fwd_SaveRequirementItems", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }

                string vendorProductMapQuery = string.Empty;
                foreach (VendorDetails vendor in vendors)
                {
                    //if (vendor.IsFromGlobalSearch == 1 && RequirementItems.IsCoreProductCategory > 0)
                    if (RequirementItems.IsCoreProductCategory > 0)
                    {
                        vendorProductMapQuery += $"CALL cp_SaveVendorsToItems('{RequirementItems.CatalogueItemID}', {userID}, {vendor.VendorID});";
                    }
                }

                if (!string.IsNullOrEmpty(vendorProductMapQuery))
                {
                    sqlHelper.ExecuteNonQuery_IUD(vendorProductMapQuery);
                }

            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        private int ValidateSession(string sessionId)
        {
            return Utilities.ValidateSession(sessionId);
        }

        private string GenerateEmailBody(string TemplateName, string biddingType = "")
        {
            if (!string.IsNullOrEmpty(biddingType) && biddingType.Equals("TENDER", StringComparison.InvariantCultureIgnoreCase))
            {
                TemplateName = TemplateName + "Tender";
                return Utilities.GenerateEmailBodyForForwardAuction(TemplateName + "Tender");
            }
            else
            {
                return Utilities.GenerateEmailBodyForForwardAuction(TemplateName);
            }
        }

        private void SaveFile(string fileName, byte[] fileContent)
        {
            var allowedExtns = ConfigurationManager.AppSettings["SUPPORTED.FILE.EXT"].ToString().Split(',').ToList();
            var isValid = allowedExtns.Any(e => fileName.ToLower().Contains(e));

            if (isValid)
            {
                File.WriteAllBytes(fileName, fileContent);
            }
            else
            {
                logger.Warn("Unsupported file uploaded: " + fileName);
            }
        }

        private Response SaveAttachment(string path)
        {
            return Utilities.SaveAttachment(path);
        }

        private FileData DownloadFile(string path, string sessioID, byte[] attachment = null, string fileName = "")
        {
            return Utilities.DownloadFile(path, sessioID, attachment, fileName);

        }

        private Response LinkPRWithRFQ(string prIDS, int reqID, string prItemIds)
        {
            Response response = new Response();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PR_IDS", prIDS);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_PR_ITEM_IDS", prItemIds);
                DataSet ds = sqlHelper.SelectList("fwd_LinkRFQWithPRItems", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }
            }
            catch (Exception ex)
            {

            }

            return response;
        }

        private async Task SendSMTPEmail(string To, string Subject, string Body, int reqID, int vendorID, string module, string sessionID = null, Attachment attachment = null, List<Attachment> ListAttachment = null, DateTime? startTime = null, long duration = 0, string reminderType = "", string cc = "")
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(RemoteServerCertificateValidationCallback);
            System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["MAILHOST_USER"].ToString(), ConfigurationManager.AppSettings["MAILHOST_PWD"].ToString());
            SmtpClient smtpClient = new SmtpClient(ConfigurationManager.AppSettings["MAILHOST"].ToString(), Convert.ToInt32(ConfigurationManager.AppSettings["MAILHOST_PORT"]));
            MailMessage mail = new MailMessage();

            try
            {
                int compID1;
                Communication communication = new Communication();
                communication = Utilities.GetCommunicationDataForwardAuction("EMAIL_FROM", sessionID, reqID: reqID);
                if (communication.Company.SendEmails != 0 || string.IsNullOrEmpty(sessionID))
                {
                    IDatabaseHelper sqlHelper1 = DatabaseProvider.GetDatabaseProvider();
                    //Body = Body.Replace("COMPANY_LOGO_ID", communication.Company.CompanyID > 0 ? communication.Company.CompanyID.ToString() : "0");
                    string companyName = ConfigurationManager.AppSettings["COMPANY_NAME"].ToString();
                    Body = Body.Replace("COMPANY_NAME", string.Equals(module, "END_NEGOTIATION", StringComparison.InvariantCultureIgnoreCase) ? String.Empty : !string.IsNullOrEmpty(communication.Company.CompanyName) ? communication.Company.CompanyName : companyName + " TEAM");
                    Body = Body.Replace("USER_NAME", string.Equals(module, "END_NEGOTIATION", StringComparison.InvariantCultureIgnoreCase) ? String.Empty : !string.IsNullOrEmpty(communication.User.FirstName + " " + communication.User.LastName) ? communication.User.FirstName + " " + communication.User.LastName : "");
                    Body = Body.Replace("COMPANY_ADDRESS", string.Equals(module, "END_NEGOTIATION", StringComparison.InvariantCultureIgnoreCase) ? String.Empty : !string.IsNullOrEmpty(communication.CompanyAddress) ? communication.CompanyAddress : "");
                    Body = Utilities.replaceDomain(Body);
                    Subject = Utilities.replaceDomain(Subject);
                    smtpClient.Credentials = credentials;
                    smtpClient.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["MAILHOST_SSL_ENABLED"].ToString());
                    mail.From = new MailAddress(GetFromAddress(""), GetFromDisplayName(""));
                    mail.BodyEncoding = System.Text.Encoding.ASCII;
                    if (startTime != null && startTime.HasValue && !string.IsNullOrEmpty(reminderType))
                    {
                        List<string> emails = new List<string>();
                        emails = To.Split(new Char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList<string>().Distinct().ToList();
                        string fileName = "";
                        if (reminderType.Equals("QUOTATION_FREEZE"))
                        {
                            fileName = "Quotation Reminder";
                        }
                        else if (reminderType.Equals("NEGOTATION_START"))
                        {
                            fileName = "Negotiation Reminder";
                        }

                        TimeZoneInfo timeInfo = TimeZoneInfo.Utc;
                        string icsContent = Utilities.MeetingRequestString("no-reply@prm360.com", emails, Subject, Body, "On-Line", startTime.Value, startTime.Value.AddMinutes(duration), timeInfo);
                        byte[] calendarBytes = System.Text.Encoding.UTF8.GetBytes(icsContent.ToString());
                        Attachment calendarAttachment = new Attachment(new MemoryStream(calendarBytes), fileName + ".ics", "text/calendar");
                        mail.Attachments.Add(calendarAttachment);
                    }

                    if (attachment != null)
                    {
                        mail.Attachments.Add(attachment);
                    }

                    if (ListAttachment != null)
                    {
                        foreach (Attachment singleAttachment in ListAttachment)
                        {
                            if (singleAttachment != null)
                            {
                                mail.Attachments.Add(singleAttachment);
                            }
                        }
                    }

                    List<string> ToAddresses = To.Split(',').ToList<string>();
                    foreach (string address in ToAddresses)
                    {
                        if (!string.IsNullOrEmpty(address))
                        {
                            mail.To.Add(new MailAddress(address));
                        }
                    }

                    if (!string.IsNullOrEmpty(cc))
                    {
                        List<string> ccAddresses = cc.Split(';').ToList<string>();
                        foreach (string address in ccAddresses)
                        {
                            if (!string.IsNullOrEmpty(address))
                            {
                                mail.CC.Add(new MailAddress(address));
                            }
                        }
                    }

                    DateTime currentdate = DateTime.UtcNow;
                    var current_date = currentdate.ToString("yyyy/MM/dd HH:mm:ss");
                    string message_body = Body;
                    EmailLogs compID = Utilities.GetCompanyidByVendorid(vendorID);
                    compID1 = compID.CompID;
                    foreach (var item in mail.To)
                    {
                        SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                        message_body = message_body.Replace("<br/>", "");
                        message_body = message_body.Replace("<b>", "");
                        message_body = message_body.Replace("</b>", "");
                        sd.Add("P_REQ_ID", reqID);
                        sd.Add("P_USER_ID", vendorID);
                        sd.Add("P_USER", item.User);
                        sd.Add("P_ADDRESS", item.Address);
                        sd.Add("P_MESSAGE", message_body);
                        sd.Add("P_SUBJECT", Subject);
                        sd.Add("P_COMP_ID", compID1);
                        sd.Add("P_REQUIREMENT", module);
                        sd.Add("P_DATE_CREATED", current_date);
                        sd.Add("P_DATE_MODIFIED", current_date);
                        DataSet ds = sqlHelper1.SelectList("cp_saveEmailLogs", sd);
                    }

                    mail.Subject = Subject;
                    mail.IsBodyHtml = true;
                    mail.Body = Body;
                    mail.Subject = Subject;
                    mail.Body = Body;
                    mail.IsBodyHtml = true;
                    logger.Info("Send email Before");
                    smtpClient.Send(mail);
                    logger.Info("Send email after");
                    Response response = new Response();
                    response.ObjectID = 1;
                }
            }
            catch (Exception ex)
            {
                logger.Info(ex.Message);
                Response response = new Response();
                response.ErrorMessage = ex.Message;
            }
            finally
            {
                mail = null;
                smtpClient = null;
            }
        }

        private string GetFromAddress(string FromAdd = null)
        {
            string from = ConfigurationManager.AppSettings["FROMADDRESS"].ToString();
            if (!string.IsNullOrEmpty(FromAdd))
            {
                from = FromAdd;
            }

            if (from.ToLower().Contains("@yahoo"))
            {
                from = ConfigurationManager.AppSettings["FROMADDRESS"].ToString();
            }

            return from;
        }

        private string GetFromDisplayName(string DisplayName = null)
        {
            string from = ConfigurationManager.AppSettings["FROMDISPLAYNAME"].ToString();
            if (!string.IsNullOrEmpty(DisplayName))
            {
                from = DisplayName;
            }

            return from;
        }

        private bool RemoteServerCertificateValidationCallback(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        private Response SaveQuotationReminders(int reqID, DateTime reminderTime, string action)
        {
            Response response = new Response();

            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_REMINDER_TIME", reminderTime);
                sd.Add("P_ACTION", action);
                DataSet ds = sqlHelper.SelectList("fwd_SaveQuotationReminders", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                }
            }
            catch (Exception ex)
            {
                //response.ErrorMessage = ex.Message;
            }
            return response;
        }

        public DateTime QuotationReminders(DateTime quotationFreezTime, int timeInterval, int startTime, int endTime, DateTime createdQuotationFreezTime)
        {
            DateTime resopnseQuotationFreezTime = DateTime.UtcNow;
            try
            {
                quotationFreezTime = quotationFreezTime.AddHours(-timeInterval);

                if (quotationFreezTime.Hour >= startTime && quotationFreezTime.Hour <= endTime)
                {
                    resopnseQuotationFreezTime = quotationFreezTime;
                }
                else if (quotationFreezTime.Date == createdQuotationFreezTime.Date && createdQuotationFreezTime.Hour > startTime && createdQuotationFreezTime.Hour < endTime && quotationFreezTime.Hour <= startTime && (createdQuotationFreezTime.Hour - quotationFreezTime.Hour) == timeInterval)
                {
                    TimeSpan ts = new TimeSpan(startTime, 00, 0);
                    resopnseQuotationFreezTime = quotationFreezTime.Date + ts;
                }
                else if (quotationFreezTime.Date == createdQuotationFreezTime.Date && quotationFreezTime.Hour >= endTime)
                {
                    TimeSpan ts = new TimeSpan(endTime, 00, 0);
                    resopnseQuotationFreezTime = quotationFreezTime.Date + ts;
                }
                else
                {
                    int days = 1;

                    if (timeInterval > 24)
                    {
                        days = Convert.ToInt32(timeInterval / 24);
                    }

                    DateTime newDate = quotationFreezTime.AddDays(-(days));
                    TimeSpan ts = new TimeSpan(endTime, 00, 0);
                    resopnseQuotationFreezTime = newDate.Date + ts;
                }
            }
            catch (Exception ex)
            {

            }
            return resopnseQuotationFreezTime;
        }

        private List<int> GetItemsToRemove(Requirement requirement, Requirement oldReq)
        {
            List<int> oldItemsList = oldReq.ListRequirementItems.Select(x => x.ItemID).ToList();
            List<int> newItemsList = requirement.ListRequirementItems.Select(x => x.ItemID).ToList();
            return oldItemsList.Except(newItemsList).ToList();
        }

        private decimal DoInclusiveTax(VendorDetails vendor, bool value)
        {
            decimal price = value ? vendor.DF_REV_VENDOR_TOTAL_PRICE : vendor.DF_REV_VENDOR_TOTAL_PRICE_NO_TAX;
            price = price * (vendor.VendorCurrencyFactor);
            return price;
        }

        private List<int> GetVendorsToRemove(Requirement requirement, Requirement oldReq)
        {
            List<int> oldVendorList = oldReq.AuctionVendors.Select(x => x.VendorID).ToList();
            List<int> newVendorList = requirement.AuctionVendors.Select(x => x.VendorID).ToList();
            return oldVendorList.Except(newVendorList).ToList();
        }

        private Response RemoveItemFromAuction(List<int> items, int reqID, string sessionID, string templateName)
        {
            Response response = new Response();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                sd.Add("P_ITEMS", string.Join(", ", items));
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("fwd_RemoveItemFromAuction", sd);
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        private Response RemoveVendorFromAuction(List<int> users, int reqID, string sessionID)
        {
            Response response = new Response();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_USERS", string.Join(", ", users));
                DataSet ds = sqlHelper.SelectList("fwd_RemoveVendorFromAuction", sd);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    if (response.ObjectID > 0)
                    {
                        foreach (var id in users)
                        {
                            UserInfo user = GetUser(id);
                            string body = GenerateEmailBody("VendoremailForRemoval");
                            body = String.Format(body, user.FirstName, user.LastName, reqID);
                            SendEmail(user.Email + "," + user.AltEmail, "ReqID: " + reqID + " - Please ignore the following requirement", body, reqID, Convert.ToInt32(user.UserID), sessionID).ConfigureAwait(false);
                            string body2 = GenerateEmailBody("VendorsmsForRemoval");
                            body2 = String.Format(body2, user.FirstName, user.LastName, reqID);
                            body2 = body2.Replace("<br/>", "");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;

        }

        private UserInfo GetUser(int userID)
        {
            UserInfo user = new UserInfo();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("cp_GetUserData", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    user.UserID = userID.ToString();
                    user.FirstName = row["U_FNAME"] != DBNull.Value ? row["U_FNAME"].ToString() : string.Empty;
                    user.LastName = row["U_LNAME"] != DBNull.Value ? row["U_LNAME"].ToString() : string.Empty;
                    user.Email = row["U_EMAIL"] != DBNull.Value ? row["U_EMAIL"].ToString() : string.Empty;
                    user.Username = row["LG_LOGIN"] != DBNull.Value ? row["LG_LOGIN"].ToString() : string.Empty;
                    user.PhoneNum = row["U_PHONE"] != DBNull.Value ? row["U_PHONE"].ToString() : string.Empty;
                    user.AddressLine1 = row["UAD_LINE1"] != DBNull.Value ? row["UAD_LINE1"].ToString() : string.Empty;
                    user.AddressLine2 = row["UAD_LINE2"] != DBNull.Value ? row["UAD_LINE2"].ToString() : string.Empty;
                    user.AddressLine3 = row["UAD_LINE3"] != DBNull.Value ? row["UAD_LINE3"].ToString() : string.Empty;
                    user.AddressPhoneNum = row["UAD_PHONE"] != DBNull.Value ? row["UAD_PHONE"].ToString() : string.Empty;
                    user.City = row["UAD_CITY"] != DBNull.Value ? row["UAD_CITY"].ToString() : string.Empty;
                    user.State = row["UAD_STATE"] != DBNull.Value ? row["UAD_STATE"].ToString() : string.Empty;
                    user.Country = row["UAD_COUNTRY"] != DBNull.Value ? row["UAD_COUNTRY"].ToString() : string.Empty;
                    user.Extension1 = row["UAD_EXT1"] != DBNull.Value ? row["UAD_EXT1"].ToString() : string.Empty;
                    user.Extension2 = row["UAD_EXT2"] != DBNull.Value ? row["UAD_EXT2"].ToString() : string.Empty;
                    user.Institution = row["INSTITUTION"] != DBNull.Value ? row["INSTITUTION"].ToString() : string.Empty;
                    user.UserData1 = row["UD_DATA1"] != DBNull.Value ? row["UD_DATA1"].ToString() : string.Empty;
                    user.UserType = row["U_TYPE"] != DBNull.Value ? row["U_TYPE"].ToString() : string.Empty;
                    user.ZipCode = row["UAD_ZIP"] != DBNull.Value ? row["UAD_ZIP"].ToString() : string.Empty;
                    user.Birthday = row["U_BIRTHDATE"] != DBNull.Value ? Convert.ToDateTime(row["U_BIRTHDATE"]) : DateTime.MaxValue;
                    user.isOTPVerified = row["isOTPVerified"] != DBNull.Value ? Convert.ToInt32(row["isOTPVerified"]) : 0;
                    user.isEmailOTPVerified = row["isEmailOTPVerified"] != DBNull.Value ? Convert.ToInt32(row["isEmailOTPVerified"]) : 0;
                    user.CredentialsVerified = row["CREDENTIALS_VERIFIED"] != DBNull.Value ? Convert.ToInt32(row["CREDENTIALS_VERIFIED"]) : 0;
                    user.RegistrationScore = row["U_REG_SCORE"] != DBNull.Value ? Convert.ToInt32(row["U_REG_SCORE"]) : 0;
                    user.IsSuperUser = row["IS_SUPER_USER"] != DBNull.Value ? (Convert.ToInt32(row["IS_SUPER_USER"]) == 1 ? true : false) : false;
                    string ProfileFileUrl = row["U_PROFILE_PIC_URL"] != DBNull.Value ? row["U_PROFILE_PIC_URL"].ToString() : string.Empty;
                    user.ProfileFileUrl = string.IsNullOrEmpty(ProfileFileUrl) ? "" : Utilities.FILE_URL + ProfileFileUrl;
                    user.Currency = row["PARAM_USER_CURRENCY"] != DBNull.Value ? row["PARAM_USER_CURRENCY"].ToString() : string.Empty;
                    user.CompanyId = row["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["COMP_ID"]) : 0;

                    user.AltPhoneNum = row["U_ALTPHONE"] != DBNull.Value ? row["U_ALTPHONE"].ToString() : string.Empty;
                    user.AltEmail = row["U_ALTEMAIL"] != DBNull.Value ? row["U_ALTEMAIL"].ToString() : string.Empty;
                    user.WF_ID = row["WF_ID"] != DBNull.Value ? Convert.ToInt32(row["WF_ID"]) : 0;
                    // REGISTRATION //
                    user.REG_STATUS = row["REG_STATUS"] != DBNull.Value ? Convert.ToInt32(row["REG_STATUS"]) : 0;
                    user.SubUserLoginID = row["LG_LOGIN"] != DBNull.Value ? row["LG_LOGIN"].ToString() : string.Empty;
                    user.SubUserPassword = row["LG_PASSWORD"] != DBNull.Value ? row["LG_PASSWORD"].ToString() : string.Empty;
                    user.VendorDetails = new Register();
                    user.VendorDetails.Address1 = row["ADDRESS1"] != DBNull.Value ? row["ADDRESS1"].ToString() : string.Empty;
                    user.VendorDetails.Street1 = row["STREET1"] != DBNull.Value ? row["STREET1"].ToString() : string.Empty;
                    user.VendorDetails.Street2 = row["STREET2"] != DBNull.Value ? row["STREET2"].ToString() : string.Empty;
                    user.VendorDetails.Street3 = row["STREET3"] != DBNull.Value ? row["STREET3"].ToString() : string.Empty;
                    user.VendorDetails.Country = row["COUNTRY"] != DBNull.Value ? row["COUNTRY"].ToString() : string.Empty;
                    user.VendorDetails.State = row["STATE"] != DBNull.Value ? row["STATE"].ToString() : string.Empty;
                    user.VendorDetails.PinCode = row["PINCODE"] != DBNull.Value ? row["PINCODE"].ToString() : string.Empty;
                    user.VendorDetails.City = row["CITY"] != DBNull.Value ? row["CITY"].ToString() : string.Empty;
                    user.VendorDetails.BankName = row["BANKNAME"] != DBNull.Value ? row["BANKNAME"].ToString() : string.Empty;
                    user.VendorDetails.BankAddress = row["BANKADDRESS"] != DBNull.Value ? row["BANKADDRESS"].ToString() : string.Empty;
                    user.VendorDetails.accntNumber = row["ACCNTNUMBER"] != DBNull.Value ? row["ACCNTNUMBER"].ToString() : string.Empty;
                    user.VendorDetails.Ifsc = row["IFSC"] != DBNull.Value ? row["IFSC"].ToString() : string.Empty;
                    user.VendorDetails.cancelledCheque = row["CANCELLEDCHEQUE"] != DBNull.Value ? row["CANCELLEDCHEQUE"].ToString() : string.Empty;
                    user.VendorDetails.altMobile = row["ALTMOBILE"] != DBNull.Value ? row["ALTMOBILE"].ToString() : string.Empty;
                    user.VendorDetails.landLine = row["LANDLINE"] != DBNull.Value ? row["LANDLINE"].ToString() : string.Empty;
                    user.VendorDetails.salesPerson = row["SALESPERSON"] != DBNull.Value ? row["SALESPERSON"].ToString() : string.Empty;
                    user.VendorDetails.salesContact = row["SALESCONTACT"] != DBNull.Value ? row["SALESCONTACT"].ToString() : string.Empty;
                    user.VendorDetails.GstAttach = row["GST_ATTACHMENTS"] != DBNull.Value ? row["GST_ATTACHMENTS"].ToString() : string.Empty;
                    user.VendorDetails.PanAttach = row["PAN_ATTACHMENTS"] != DBNull.Value ? row["PAN_ATTACHMENTS"].ToString() : string.Empty;
                    user.VendorDetails.BussAttach = row["BUSINESS_ATTACHMENTS"] != DBNull.Value ? row["BUSINESS_ATTACHMENTS"].ToString() : string.Empty;
                    user.VendorDetails.BussinessDet = row["BUSINESS_DETAILS"] != DBNull.Value ? row["BUSINESS_DETAILS"].ToString() : string.Empty;
                    user.VendorDetails.MsmeAttach = row["OTHER_ATTACH"] != DBNull.Value ? row["OTHER_ATTACH"].ToString() : string.Empty;
                    user.VendorDetails.AdditionalVendorInformation = row["ADDITIONAL_VENDOR_INFORMATION"] != DBNull.Value ? row["ADDITIONAL_VENDOR_INFORMATION"].ToString() : string.Empty;
                    user.VendorDetails.CustomerCopyLetterAttach = row["CUSTOMER_COPY_ATTACHMENTS"] != DBNull.Value ? row["CUSTOMER_COPY_ATTACHMENTS"].ToString() : string.Empty;

                    user.PERSONAL_INFO_VALID = row["PERSONAL_INFO_VALID"] != DBNull.Value ? Convert.ToInt32(row["PERSONAL_INFO_VALID"]) : 0;
                    user.CONTACT_INFO_VALID = row["CONTACT_INFO_VALID"] != DBNull.Value ? Convert.ToInt32(row["CONTACT_INFO_VALID"]) : 0;
                    user.BANK_INFO_VALID = row["BANK_INFO_VALID"] != DBNull.Value ? Convert.ToInt32(row["BANK_INFO_VALID"]) : 0;
                    user.BUSINESS_INFO_VALID = row["BUSINESS_INFO_VALID"] != DBNull.Value ? Convert.ToInt32(row["BUSINESS_INFO_VALID"]) : 0;
                    user.CONFLICT_INFO_VALID = row["CONFLICT_INFO_VALID"] != DBNull.Value ? Convert.ToInt32(row["CONFLICT_INFO_VALID"]) : 0;
                    user.SAPUserId = row["SAP_USER_ID"] != DBNull.Value ? row["SAP_USER_ID"].ToString() : string.Empty;
                    // REGISTRATION //

                }
            }
            catch (Exception ex)
            {
                user.ErrorMessage = ex.Message;
            }

            CatalogueInfo catalogueInfo = GetCatalogueInfo(userID);
            user.IsCatalogueEnabled = catalogueInfo.IsCatalogueEnabled;
            user.CatalogueCompanyId = catalogueInfo.CatalogueCompanyId;
            user.CompanyRoundingDecimalSetting = decimal_round;
            return user;
        }

        private CatalogueInfo GetCatalogueInfo(int userID)
        {
            CatalogueInfo catalogueInfo = new CatalogueInfo();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("cm_GetCatalogueInfo", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    catalogueInfo.IsCatalogueEnabled = row["IS_CATALOGUE_ENABLED"] != DBNull.Value ? (Convert.ToInt32(row["IS_CATALOGUE_ENABLED"]) == 1 ? true : false) : false;
                    catalogueInfo.CatalogueCompanyId = row["CATALOGUE_COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["CATALOGUE_COMP_ID"]) : 0;
                }
            }
            catch (Exception ex)
            {

            }
            return catalogueInfo;
        }

        private Requirement GetRequirementDetails(int reqID, int userID, int excludePriceCap)
        {
            Requirement requirement = new Requirement();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_EXCLUDE_PRICE_CAP", excludePriceCap);
                DataSet ds = sqlHelper.SelectList("fwd_GetRequirementData", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    string[] arr = new string[] { };
                    byte[] test = new byte[] { };
                    requirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                    var tempReqType = row["REQ_TYPE"] != DBNull.Value ? Convert.ToString(row["REQ_TYPE"]) : string.Empty;
                    requirement.IsRFP = tempReqType == "2";
                    requirement.Description = row["REQ_DESC"] != DBNull.Value ? Convert.ToString(row["REQ_DESC"]) : string.Empty;
                    requirement.Category = row["REQ_CATEGORY"] != DBNull.Value ? Convert.ToString(row["REQ_CATEGORY"]).Split(',') : arr;
                    requirement.Subcategories = row["REQ_SUBCATEGORIES"] != DBNull.Value ? Convert.ToString(row["REQ_SUBCATEGORIES"]) : string.Empty;
                    requirement.PostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                    requirement.Urgency = row["REQ_URGENCY"] != DBNull.Value ? Convert.ToString(row["REQ_URGENCY"]) : string.Empty;
                    requirement.Budget = row["REQ_BUDGET"] != DBNull.Value ? Convert.ToString(row["REQ_BUDGET"]) : string.Empty;
                    requirement.DeliveryLocation = row["REQ_DELIVERY_LOC"] != DBNull.Value ? Convert.ToString(row["REQ_DELIVERY_LOC"]) : string.Empty;
                    requirement.Taxes = row["REQ_TAXES"] != DBNull.Value ? Convert.ToString(row["REQ_TAXES"]) : string.Empty;
                    requirement.PaymentTerms = row["REQ_PAYMENT_TERMS"] != DBNull.Value ? Convert.ToString(row["REQ_PAYMENT_TERMS"]) : string.Empty;
                    requirement.Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                    string Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                    requirement.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                    requirement.CustomerID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                    requirement.CustFirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                    requirement.CustLastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                    requirement.DeliveryTime = row["REQ_DELIVERY_TIME"] != DBNull.Value ? Convert.ToString(row["REQ_DELIVERY_TIME"]) : string.Empty;
                    requirement.RequirementID = reqID;
                    int isPOSent = row["IS_PO_SENT"] != DBNull.Value ? Convert.ToInt32(row["IS_PO_SENT"]) : 0;
                    string fileName = row["REQ_ATTACHMENT"] != DBNull.Value ? Convert.ToString(row["REQ_ATTACHMENT"]) : string.Empty;
                    string POLink = row["PURCHASE_ORDER_LINK"] != DBNull.Value ? (row["PURCHASE_ORDER_LINK"].ToString()) : string.Empty;
                    requirement.POLink = !string.IsNullOrEmpty(POLink) ? POLink : string.Empty;
                    string URL = fileName != string.Empty ? fileName : string.Empty;
                    requirement.AttachmentName = fileName;
                    requirement.EndTime = row["END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["END_TIME"]) : DateTime.MaxValue;
                    requirement.InclusiveTax = row["REQ_INCLUSIVE_TAX"] != DBNull.Value ? (Convert.ToInt32(row["REQ_INCLUSIVE_TAX"]) == 1 ? true : false) : false;
                    requirement.IncludeFreight = row["REQ_INCLUDE_FREIGHT"] != DBNull.Value ? (Convert.ToInt32(row["REQ_INCLUDE_FREIGHT"]) == 1 ? true : false) : false;
                    requirement.SuperUserID = row["SUPER_U_ID"] != DBNull.Value ? Convert.ToInt32(row["SUPER_U_ID"]) : requirement.CustomerID;
                    requirement.SelectedVendorID = row["SELECTED_VENDOR_ID"] != DBNull.Value ? Convert.ToInt32(row["SELECTED_VENDOR_ID"]) : requirement.CustomerID;
                    requirement.CustomerCompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                    requirement.CustomerCompanyId = row["CUST_COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["CUST_COMP_ID"]) : 0;
                    requirement.MinReduceAmount = row["MIN_REDUCE_AMOUNT"] != DBNull.Value ? Convert.ToDouble(row["MIN_REDUCE_AMOUNT"]) : 0;
                    requirement.QuotationFreezTime = row["QUOTATION_FREEZ_TIME"] != DBNull.Value ? Convert.ToDateTime(row["QUOTATION_FREEZ_TIME"]) : DateTime.MaxValue;
                    requirement.IsTabular = row["REQ_IS_TABULAR"] != DBNull.Value ? (Convert.ToInt32(row["REQ_IS_TABULAR"]) == 1 ? true : false) : false;
                    requirement.ReqComments = row["REQ_COMMENTS"] != DBNull.Value ? Convert.ToString(row["REQ_COMMENTS"]) : string.Empty;
                    requirement.ExpStartTime = row["EXP_START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["EXP_START_TIME"]) : DateTime.MaxValue;
                    requirement.LotId = row["LOT_ID"] != DBNull.Value ? Convert.ToInt32(row["LOT_ID"]) : 0;
                    requirement.IsTechScoreReq = row["TECH_SCORE_REQ"] != DBNull.Value ? Convert.ToInt32(row["TECH_SCORE_REQ"]) : 0;
                    requirement.DateModified = row["DATE_MODIFIED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_MODIFIED"]) : DateTime.MinValue;
                    //requirement.IsPOGenerated = isPOGenerated(reqID);
                    if (requirement.MinReduceAmount > 0)
                    {
                        requirement.MinBidAmount = requirement.MinReduceAmount;
                    }
                    else
                    {
                        requirement.MinBidAmount = 0;
                    }

                    requirement.CustomerCompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                    requirement.IsStopped = row["IS_STOPPED"] != DBNull.Value ? (Convert.ToInt32(row["IS_STOPPED"]) == 1 ? true : false) : false;
                    requirement.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                    requirement.Currency = row["REQ_CURRENCY"] != DBNull.Value ? Convert.ToString(row["REQ_CURRENCY"]) : string.Empty;
                    requirement.TimeZone = row["REQ_TIMEZONE"] != DBNull.Value ? Convert.ToString(row["REQ_TIMEZONE"]) : string.Empty;
                    requirement.IsNegotiationEnded = row["IS_NEGOTIATION_ENDED"] != DBNull.Value ? Convert.ToInt32(row["IS_NEGOTIATION_ENDED"]) : 0;
                    requirement.NegotiationDuration = row["NEGOTIATION_DURATION"] != DBNull.Value ? Convert.ToString(row["NEGOTIATION_DURATION"]) : string.Empty;
                    requirement.MinVendorComparision = row["VENDOR_COMPARISION_MIN_AMOUNT"] != DBNull.Value ? Convert.ToDouble(row["VENDOR_COMPARISION_MIN_AMOUNT"]) : 0;

                    NegotiationSettings NegotiationSettings = new NegotiationSettings();
                    NegotiationSettings.MinReductionAmount = row["UD_MIN_REDUCE_AMOUNT"] != DBNull.Value ? Convert.ToDouble(row["UD_MIN_REDUCE_AMOUNT"]) : 0;
                    NegotiationSettings.RankComparision = row["UD_VENDOR_COMPARISION_MIN_AMOUNT"] != DBNull.Value ? Convert.ToDouble(row["UD_VENDOR_COMPARISION_MIN_AMOUNT"]) : 0;
                    NegotiationSettings.NegotiationDuration = row["UD_NEGOTIATION_DURATION"] != DBNull.Value ? Convert.ToString(row["UD_NEGOTIATION_DURATION"]) : string.Empty;
                    int tempDisableLocalGSTFeature = row["DISABLE_LOCAL_GST_FEATURE"] != DBNull.Value ? Convert.ToUInt16(row["DISABLE_LOCAL_GST_FEATURE"]) : 0;
                    NegotiationSettings.DisableLocalGSTFeature = tempDisableLocalGSTFeature > 0 ? true : false;
                    requirement.ReqPDF = row["REQ_PDF"] != DBNull.Value ? Convert.ToInt32(row["REQ_PDF"]) : 0;
                    requirement.ReqPDFCustomer = row["REQ_PDF_CUSTOMER"] != DBNull.Value ? Convert.ToInt32(row["REQ_PDF_CUSTOMER"]) : 0;
                    requirement.ReportReq = row["REPORT_REQ"] != DBNull.Value ? Convert.ToInt32(row["REPORT_REQ"]) : 0;
                    requirement.ReportItemWise = row["REPORT_ITEM_WISE"] != DBNull.Value ? Convert.ToInt32(row["REPORT_ITEM_WISE"]) : 0;
                    requirement.ReqType = row["PARAM_REQ_TYPE"] != DBNull.Value ? Convert.ToString(row["PARAM_REQ_TYPE"]) : string.Empty;
                    requirement.PriceCapValue = row["PARAM_PRICE_CAP_VALUE"] != DBNull.Value ? Convert.ToDouble(row["PARAM_PRICE_CAP_VALUE"]) : 0;
                    requirement.CustCompID = row["CUST_COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["CUST_COMP_ID"]) : 0;
                    requirement.IsQuotationPriceLimit = row["IS_QUOTATION_PRICE_LIMIT"] != DBNull.Value ? (Convert.ToInt32(row["IS_QUOTATION_PRICE_LIMIT"]) == 1 ? true : false) : false;
                    requirement.QuotationPriceLimit = row["QUOTATION_PRICE_LIMIT"] != DBNull.Value ? Convert.ToDouble(row["QUOTATION_PRICE_LIMIT"]) : 0;
                    requirement.NoOfQuotationReminders = row["NO_OF_QUOTATION_REMINDERS"] != DBNull.Value ? Convert.ToInt32(row["NO_OF_QUOTATION_REMINDERS"]) : 0;
                    requirement.RemindersTimeInterval = row["REMINDERS_TIME_INTERVAL"] != DBNull.Value ? Convert.ToInt32(row["REMINDERS_TIME_INTERVAL"]) : 0;
                    requirement.SuperUser = new User();
                    requirement.PostedUser = new User();
                    requirement.PostedUser.UserInfo = new UserInfo();
                    requirement.SuperUser.FirstName = row["SUPER_USER_NAME"] != DBNull.Value ? Convert.ToString(row["SUPER_USER_NAME"]) : string.Empty;
                    requirement.SuperUser.Email = row["SUPER_USER_EMAIL"] != DBNull.Value ? Convert.ToString(row["SUPER_USER_EMAIL"]) : string.Empty;
                    requirement.SuperUser.PhoneNum = row["SUPER_USER_PHONE"] != DBNull.Value ? Convert.ToString(row["SUPER_USER_PHONE"]) : string.Empty;
                    requirement.PostedUser.Email = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                    requirement.PostedUser.PhoneNum = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
                    requirement.PostedUser.FirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                    requirement.PostedUser.LastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                    requirement.PostedUser.UserInfo.Address = row["ADDRESS"] != DBNull.Value ? Convert.ToString(row["ADDRESS"]) : string.Empty;
                    requirement.IsUnitPriceBidding = row["IS_UNIT_PRICE_BIDDING"] != DBNull.Value ? Convert.ToInt32(row["IS_UNIT_PRICE_BIDDING"]) : 0;
                    requirement.MaterialDispachmentLink = row["MAT_DIS_LINK"] != DBNull.Value ? Convert.ToInt32(row["MAT_DIS_LINK"]) : 0;
                    requirement.MaterialReceivedLink = row["MR_LINK"] != DBNull.Value ? Convert.ToInt32(row["MR_LINK"]) : 0;
                    requirement.IndentID = row["INDENT_ID"] != DBNull.Value ? Convert.ToInt32(row["INDENT_ID"]) : 0;
                    requirement.IsDiscountQuotation = row["IS_DISCOUNT_QUOTATION"] != DBNull.Value ? Convert.ToInt32(row["IS_DISCOUNT_QUOTATION"]) : 0;
                    requirement.ContactDetails = row["CONTACT_DETAILS"] != DBNull.Value ? Convert.ToString(row["CONTACT_DETAILS"]) : string.Empty;
                    requirement.GeneralTC = row["GENERAL_TC"] != DBNull.Value ? Convert.ToString(row["GENERAL_TC"]) : string.Empty;
                    requirement.IsRevUnitDiscountEnable = row["IS_REV_UNIT_DISCOUNT_ENABLE"] != DBNull.Value ? Convert.ToInt32(row["IS_REV_UNIT_DISCOUNT_ENABLE"]) : 0;

                    requirement.ContractStartTime = row["CONT_START_TIME"] != DBNull.Value ? Convert.ToString(row["CONT_START_TIME"]) : string.Empty;
                    requirement.ContractEndTime = row["CONT_END_TIME"] != DBNull.Value ? Convert.ToString(row["CONT_END_TIME"]) : string.Empty;
                    requirement.IsContract = row["IS_CONTRACT"] != DBNull.Value ? (Convert.ToInt32(row["IS_CONTRACT"]) == 1 ? true : false) : false;

                    //#CB-0-2018-12-05
                    requirement.IS_CB_ENABLED = row["IS_CB_ENABLED"] != DBNull.Value ? (Convert.ToInt32(row["IS_CB_ENABLED"]) == 1 ? true : false) : false;

                    requirement.CB_END_TIME = row["CB_END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["CB_END_TIME"]) : DateTime.MinValue;

                    requirement.CB_STOP_QUOTATIONS = row["CB_STOP_QUOTATIONS"] != DBNull.Value ? (Convert.ToInt32(row["CB_STOP_QUOTATIONS"]) == 1 ? true : false) : false;
                    requirement.IS_CB_COMPLETED = row["IS_CB_COMPLETED"] != DBNull.Value ? (Convert.ToInt32(row["IS_CB_COMPLETED"]) == 1 ? true : false) : false;

                    requirement.LAST_BID_ID = row["LAST_BID_ID"] != DBNull.Value ? Convert.ToInt32(row["LAST_BID_ID"]) : 0;

                    requirement.IsLockReq = row["LOCK_REQ"] != DBNull.Value ? Convert.ToInt32(row["LOCK_REQ"]) : 0;



                    requirement.LotCount = row["LOT_COUNT"] != DBNull.Value ? Convert.ToInt32(row["LOT_COUNT"]) : 0;
                    requirement.BiddingType = row["BIDDING_TYPE"] != DBNull.Value ? Convert.ToString(row["BIDDING_TYPE"]) : string.Empty;

                    requirement.IS_CB_NO_REGRET = row["IS_CB_NO_REGRET"] != DBNull.Value ? (Convert.ToInt32(row["IS_CB_NO_REGRET"]) == 1 ? true : false) : false;
                    requirement.AuditVersion = row["AUDIT_VERSION"] != DBNull.Value ? Convert.ToInt32(row["AUDIT_VERSION"]) : 1;
                    requirement.AuditComments = row["AUDIT_COMMENTS"] != DBNull.Value ? Convert.ToString(row["AUDIT_COMMENTS"]) : string.Empty;
                    requirement.ProjectName = row["PROJECT_NAME"] != DBNull.Value ? Convert.ToString(row["PROJECT_NAME"]) : string.Empty;
                    requirement.ProjectDetails = row["PROJECT_DETAILS"] != DBNull.Value ? Convert.ToString(row["PROJECT_DETAILS"]) : string.Empty;

                    requirement.PR_ID = row["PR_ID"] != DBNull.Value ? Convert.ToString(row["PR_ID"]) : string.Empty;
                    requirement.PRNumbers = row["PR_NUMBERS"] != DBNull.Value ? Convert.ToString(row["PR_NUMBERS"]) : string.Empty;
                    requirement.PRCreator = row["PR_CREATOR"] != DBNull.Value ? Convert.ToString(row["PR_CREATOR"]) : string.Empty;
                    requirement.PLANT_CODES = row["PLANT_CODES"] != DBNull.Value ? Convert.ToString(row["PLANT_CODES"]) : string.Empty;
                    requirement.PLANTS = row["PLANTS"] != DBNull.Value ? Convert.ToString(row["PLANTS"]) : string.Empty;
                    requirement.PURCHASE_GROUP_CODES = row["PURCHASE_GROUP_CODES"] != DBNull.Value ? Convert.ToString(row["PURCHASE_GROUP_CODES"]) : string.Empty;
                    requirement.RequirementNumber = row["REQ_NUMBER"] != DBNull.Value ? Convert.ToString(row["REQ_NUMBER"]) : string.Empty;
                    requirement.NegotiationSettings = NegotiationSettings;
                    DateTime now = DateTime.UtcNow;
                    requirement.DATETIME_NOW = now;
                    DateTime DATE_CB_END_TIME = Convert.ToDateTime(requirement.CB_END_TIME);
                    long DIFF_CB_END_TIME = Convert.ToInt64((DATE_CB_END_TIME - now).TotalSeconds);

                    if (DIFF_CB_END_TIME > 0)
                    {
                        requirement.CB_TIME_LEFT = DIFF_CB_END_TIME;
                    }
                    else
                    {
                        requirement.CB_TIME_LEFT = 0;
                        requirement.CB_END_TIME = now;
                    }


                    if (requirement.EndTime != DateTime.MaxValue && requirement.EndTime > now && requirement.StartTime < now)
                    {
                        DateTime date = Convert.ToDateTime(requirement.EndTime);
                        long diff = Convert.ToInt64((date - now).TotalSeconds);
                        requirement.TimeLeft = diff;
                        requirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString());
                    }
                    else if (requirement.StartTime != DateTime.MaxValue && requirement.StartTime > now)
                    {
                        DateTime date = Convert.ToDateTime(requirement.StartTime);
                        long diff = Convert.ToInt64((date - now).TotalSeconds);
                        requirement.TimeLeft = diff;
                        requirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString());
                    }
                    else if (requirement.EndTime < now)
                    {
                        requirement.TimeLeft = -1;
                        if ((Status == Utilities.GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString()) || Status == Utilities.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString())) && requirement.EndTime < now)
                        {
                            requirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.NegotiationEnded.ToString());
                            EndNegotiation(requirement.RequirementID, requirement.CustomerID, "");
                        }
                        else
                        {
                            requirement.Status = Status;
                        }
                    }
                    else if (requirement.StartTime == DateTime.MaxValue)
                    {
                        requirement.TimeLeft = -1;
                        requirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.UNCONFIRMED.ToString());
                    }
                    if (Status == Utilities.GetEnumDesc<PRMStatus>(PRMStatus.DELETED.ToString()))
                    {
                        requirement.TimeLeft = -1;
                        requirement.Status = Status;
                    }


                    if (DIFF_CB_END_TIME > 0)
                    {
                        requirement.CB_TIME_LEFT = DIFF_CB_END_TIME;
                        requirement.TimeLeft = DIFF_CB_END_TIME;
                    }
                    else
                    {
                        requirement.CB_TIME_LEFT = 0;
                        requirement.CB_END_TIME = now;
                    }

                    List<VendorDetails> vendorDetails = new List<VendorDetails>();
                    int rank = 1;
                    bool isUserVendor = false;
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        var companyCurrencies = new List<CurrencyFactors>();
                        if (requirement.CustomerCompanyId > 0)
                        {
                            companyCurrencies = this.GetCurrencyFactors(requirement.CustomerCompanyId);
                            if (companyCurrencies == null)
                            {
                                companyCurrencies = new List<CurrencyFactors>();
                            }
                        }

                        foreach (DataRow row1 in ds.Tables[1].Rows)
                        {
                            VendorDetails vendor = new VendorDetails();

                            vendor.PO = new RequirementPO();
                            vendor.ListCurrencies = new List<KeyValuePair>();
                            vendor.RequirementID = requirement.RequirementID;
                            vendor.VendorID = row1["U_ID"] != DBNull.Value ? Convert.ToInt32(row1["U_ID"]) : 0;
                            vendor.compID = row1["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row1["COMP_ID"]) : 0;
                            vendor.VendorCode = row1["VENDOR_CODE"] != DBNull.Value ? Convert.ToString(row1["VENDOR_CODE"]) : string.Empty;
                            vendor.PaymentTermCode = row1["PAYMENT_TERM_CODE"] != DBNull.Value ? Convert.ToString(row1["PAYMENT_TERM_CODE"]) : string.Empty;
                            vendor.PaymentTermDesc = row1["PAYMENT_TERM_DESC"] != DBNull.Value ? Convert.ToString(row1["PAYMENT_TERM_DESC"]) : string.Empty;
                            vendor.IS_QUOT_LATE = row1["IS_QUOT_LATE"] != DBNull.Value ? Convert.ToInt32(row1["IS_QUOT_LATE"]) : 0;
                            vendor.VendorName = row1["VENDOR_NAME"] != DBNull.Value ? Convert.ToString(row1["VENDOR_NAME"]) : string.Empty;
                            vendor.InitialPrice = row1["VEND_INIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["VEND_INIT_PRICE"]) : 0;
                            vendor.RunningPrice = row1["VEND_RUN_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["VEND_RUN_PRICE"]) : 0;
                            vendor.TotalInitialPrice = row1["VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["VEND_TOTAL_PRICE"]) : 0;

                            vendor.City = row1["UAD_CITY"] != DBNull.Value ? Convert.ToString(row1["UAD_CITY"]) : string.Empty;
                            vendor.Taxes = row1["TAXES"] != DBNull.Value ? Convert.ToDouble(row1["TAXES"]) : 0;
                            vendor.Rating = row1["RATING"] != DBNull.Value ? Convert.ToDouble(row1["RATING"]) : 0;
                            vendor.Taxes = row1["VEND_TAXES"] != DBNull.Value ? Convert.ToInt32(row1["VEND_TAXES"]) : 0;
                            vendor.CompanyName = row1["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row1["COMPANY_NAME"]) : string.Empty;
                            //vendor.InitialPriceWithOutTaxFreight = row1["VEND_INIT_PRICE_WITHOUT_TAX"] != DBNull.Value ? Convert.ToDouble(row1["VEND_INIT_PRICE_WITHOUT_TAX"]) : 0;
                            vendor.Warranty = row1["WARRANTY"] != DBNull.Value ? Convert.ToString(row1["WARRANTY"]) : string.Empty;
                            vendor.Payment = row1["PAYMENT"] != DBNull.Value ? Convert.ToString(row1["PAYMENT"]) : string.Empty;
                            vendor.Duration = row1["DURATION"] != DBNull.Value ? Convert.ToString(row1["DURATION"]) : string.Empty;
                            vendor.Validity = row1["VALIDITY"] != DBNull.Value ? Convert.ToString(row1["VALIDITY"]) : string.Empty;
                            vendor.OtherProperties = row1["OTHER_PROPERTIES"] != DBNull.Value ? Convert.ToString(row1["OTHER_PROPERTIES"]) : string.Empty;
                            vendor.IsQuotationRejected = row1["IS_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToInt32(row1["IS_QUOTATION_REJECTED"]) : 0;
                            vendor.TechnicalScore = row1["TECHNICAL_SCORE"] != DBNull.Value ? Convert.ToDecimal(row1["TECHNICAL_SCORE"]) : 0;
                            vendor.QuotationRejectedComment = row1["QUOTATION_REJECTED_REASON"] != DBNull.Value ? Convert.ToString(row1["QUOTATION_REJECTED_REASON"]) : string.Empty;
                            vendor.PO = new RequirementPO();
                            vendor.PO.POLink = row1["PO_ATT_ID"] != DBNull.Value ? Convert.ToString(row1["PO_ATT_ID"]) : "0";
                            string fileName1 = row1["QUOTATION_URL"] != DBNull.Value ? Convert.ToString(row1["QUOTATION_URL"]) : string.Empty;
                            string URL1 = fileName1 != string.Empty ? fileName1 : string.Empty;
                            vendor.QuotationUrl = URL1;
                            string fileName2 = row1["REV_QUOTATION_URL"] != DBNull.Value ? Convert.ToString(row1["REV_QUOTATION_URL"]) : string.Empty;
                            string URL2 = fileName2 != string.Empty ? fileName2 : string.Empty;
                            vendor.RevQuotationUrl = URL2;


                            vendor.RevPrice = row1["REV_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["REV_PRICE"]) : 0;
                            vendor.RevVendorTotalPrice = row1["REV_VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["REV_VEND_TOTAL_PRICE"]) : 0;
                            vendor.RevVendorTotalPriceNoTax = row1["REV_VEND_TOTAL_PRICE_NO_TAX"] != DBNull.Value ? Convert.ToDouble(row1["REV_VEND_TOTAL_PRICE_NO_TAX"]) : 0;
                            vendor.TotalRunningPrice = row1["VEND_TOTAL_PRICE_RUNNING"] != DBNull.Value ? Convert.ToDouble(row1["VEND_TOTAL_PRICE_RUNNING"]) : 0;
                            vendor.IsRevQuotationRejected = row1["IS_REV_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToInt32(row1["IS_REV_QUOTATION_REJECTED"]) : 0;
                            vendor.RevQuotationRejectedComment = row1["REV_QUOTATION_REJECTED_REASON"] != DBNull.Value ? Convert.ToString(row1["REV_QUOTATION_REJECTED_REASON"]) : string.Empty;
                            vendor.Discount = row1["DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row1["DISCOUNT"]) : 0;
                            vendor.ReductionPercentage = row1["REDUCTION_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row1["REDUCTION_PERCENTAGE"]) : 0;
                            vendor.Vendor = new User();
                            vendor.Vendor.Email = row1["U_EMAIL"] != DBNull.Value ? Convert.ToString(row1["U_EMAIL"]) : string.Empty;
                            vendor.Vendor.PhoneNum = row1["U_PHONE"] != DBNull.Value ? Convert.ToString(row1["U_PHONE"]) : string.Empty;
                            vendor.PO.MaterialDispachmentLink = row1["MAT_DIS_LINK"] != DBNull.Value ? Convert.ToInt32(row1["MAT_DIS_LINK"]) : 0;
                            vendor.PO.MaterialReceivedLink = row1["MR_LINK"] != DBNull.Value ? Convert.ToInt32(row1["MR_LINK"]) : 0;
                            vendor.LandingPrice = row1["LANDING_PRICE"] != DBNull.Value ? Convert.ToDecimal(row1["LANDING_PRICE"]) : 0;
                            vendor.RevLandingPrice = row1["REV_LANDING_PRICE"] != DBNull.Value ? Convert.ToDecimal(row1["REV_LANDING_PRICE"]) : 0;
                            vendor.TechEvalScore = row1["TECH_EVAL_SCORE"] != DBNull.Value ? Convert.ToDecimal(row1["TECH_EVAL_SCORE"]) : 0;
                            vendor.SumOfMargin = row1["PARAM_SUM_MARGIN"] != DBNull.Value ? Convert.ToDecimal(row1["PARAM_SUM_MARGIN"]) : 0;
                            vendor.SumOfInitialMargin = row1["PARAM_SUM_INITIAL_MARGIN"] != DBNull.Value ? Convert.ToDecimal(row1["PARAM_SUM_INITIAL_MARGIN"]) : 0;
                            vendor.MultipleAttachments = row1["MULTIPLE_ATTACHMENTS"] != DBNull.Value ? Convert.ToString(row1["MULTIPLE_ATTACHMENTS"]) : string.Empty;
                            vendor.GstNumber = row1["GST_NUMBER"] != DBNull.Value ? Convert.ToString(row1["GST_NUMBER"]) : string.Empty;
                            vendor.VendorCurrency = row1["CURR_NAME"] != DBNull.Value ? Convert.ToString(row1["CURR_NAME"]) : string.Empty;
                            vendor.SelectedVendorCurrency = row1["SELECTED_VENDOR_CURRENCY"] != DBNull.Value ? Convert.ToString(row1["SELECTED_VENDOR_CURRENCY"]) : string.Empty;
                            vendor.VendorCurrencyFactor = row1["CURRENCY_FACTOR"] != DBNull.Value ? Convert.ToDecimal(row1["CURRENCY_FACTOR"]) : 1;
                            vendor.RevPriceCB = row1["REV_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row1["REV_PRICE_CB"]) : 0;
                            vendor.RevVendorTotalPriceCB = row1["REV_VEND_TOTAL_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row1["REV_VEND_TOTAL_PRICE_CB"]) : 0;
                            vendor.TotalRunningPriceCB = row1["VEND_TOTAL_PRICE_RUNNING_CB"] != DBNull.Value ? Convert.ToDouble(row1["VEND_TOTAL_PRICE_RUNNING_CB"]) : 0;
                            vendor.FREEZE_CB = row1["FREEZE_CB"] != DBNull.Value ? (Convert.ToInt32(row1["FREEZE_CB"]) == 1 ? true : false) : false;
                            vendor.VEND_FREEZE_CB = row1["VEND_FREEZE_CB"] != DBNull.Value ? (Convert.ToInt32(row1["VEND_FREEZE_CB"]) == 1 ? true : false) : false;
                            vendor.CB_BID_COMMENTS = row1["CB_BID_COMMENTS"] != DBNull.Value ? Convert.ToString(row1["CB_BID_COMMENTS"]) : string.Empty;
                            vendor.INCO_TERMS = row1["INCO_TERMS"] != DBNull.Value ? Convert.ToString(row1["INCO_TERMS"]) : string.Empty;
                            vendor.LandingPrice = Math.Round(vendor.LandingPrice, decimal_round);
                            vendor.RevLandingPrice = Math.Round(vendor.RevLandingPrice, decimal_round);
                            vendor.LastActiveTime = row1["LAST_ACTIVE_TIME"] != DBNull.Value ? Convert.ToString(row1["LAST_ACTIVE_TIME"]) : string.Empty;
                            vendor.IsVendAckChecked = row1["IS_VEND_ACK_CHECKED"] != DBNull.Value ? Convert.ToString(row1["IS_VEND_ACK_CHECKED"]) : string.Empty;
                            vendor.ReductionPercentage = Math.Round(vendor.ReductionPercentage, decimal_round);
                            vendor.DIFFERENTIAL_FACTOR = row1["DIFFERENTIAL_FACTOR"] != DBNull.Value ? Convert.ToDecimal(row1["DIFFERENTIAL_FACTOR"]) : 0;
                            vendor.DF_REV_VENDOR_TOTAL_PRICE = Convert.ToDecimal(vendor.RevVendorTotalPrice) + vendor.DIFFERENTIAL_FACTOR;
                            vendor.DF_REV_VENDOR_TOTAL_PRICE_NO_TAX = Convert.ToDecimal(vendor.RevVendorTotalPriceNoTax) + vendor.DIFFERENTIAL_FACTOR;
                            vendor.AuctionModifiedDate = row1["AUCTION_MODIFIED_DATE"] != DBNull.Value ? Convert.ToDateTime(row1["AUCTION_MODIFIED_DATE"]) : DateTime.MaxValue;
                            vendor.SurrogateComments = row1["SURROGATE_COMMENTS"] != DBNull.Value ? Convert.ToString(row1["SURROGATE_COMMENTS"]) : string.Empty;
                            vendor.VENDOR_PLANT_CODE = row1["VENDOR_PLANT_CODE"] != DBNull.Value ? Convert.ToString(row1["VENDOR_PLANT_CODE"]) : string.Empty;

                            KeyValuePair reqCurrency = new KeyValuePair();
                            reqCurrency.Value = requirement.Currency;
                            reqCurrency.Key1 = "REQ_CURRENCY";

                            KeyValuePair vendorCurrency = new KeyValuePair();
                            vendorCurrency.Value = vendor.VendorCurrency;
                            vendorCurrency.Key1 = "VENDOR_CURRENCY";

                            vendor.ListCurrencies.Add(reqCurrency);

                            if (reqCurrency.Value == vendorCurrency.Value)
                            {
                                //vendor.SelectedVendorCurrency = reqCurrency.Value;
                            }
                            else
                            {
                                vendor.ListCurrencies.Add(vendorCurrency);
                            }

                            if (companyCurrencies != null && companyCurrencies.Count > 0)
                            {
                                foreach (var currency in companyCurrencies)
                                {
                                    if (!vendor.ListCurrencies.Any(c => c.Value == currency.CurrencyCode))
                                    {
                                        vendor.ListCurrencies.Add(new KeyValuePair() { Value = currency.CurrencyCode, Key1 = "COMPANY_CURRENCY" });
                                    }
                                }
                            }

                            vendorDetails.Add(vendor);
                        }
                    }

                    if (requirement.IsDiscountQuotation == 2)
                    {
                        decimal maxInitialMargin = 0;
                        decimal maxClosedMargin = 0;
                        try
                        {
                            maxInitialMargin = vendorDetails.Where(vd => vd.CompanyName != "PRICE_CAP" && vd.CompanyName != "DEFAULT_VENDOR" && vd.IsQuotationRejected == 0).Max(vd => vd.SumOfInitialMargin);
                            maxClosedMargin = vendorDetails.Where(vd => vd.CompanyName != "PRICE_CAP" && vd.CompanyName != "DEFAULT_VENDOR" && vd.IsQuotationRejected == 0).Max(vd => vd.SumOfMargin);
                        }
                        catch
                        {
                            maxInitialMargin = 0;
                            maxClosedMargin = 0;
                        }

                        requirement.Savings = Convert.ToDouble(maxClosedMargin - maxInitialMargin);
                        requirement.Savings = Math.Round(requirement.Savings, decimal_round);
                    }

                    requirement.CustomerReqAccess = false;
                    if (ds.Tables[5].Rows.Count > 0)
                    {
                        foreach (DataRow row5 in ds.Tables[5].Rows)
                        {
                            int reqCustomerId = row5["U_ID"] != DBNull.Value ? Convert.ToInt32(row5["U_ID"]) : 0;

                            if (userID == reqCustomerId)
                            {
                                requirement.CustomerReqAccess = true;
                            }
                        }
                    }

                    List<VendorDetails> v2 = new List<VendorDetails>();
                    if (requirement.StartTime > DateTime.UtcNow)
                    {
                        v2 = vendorDetails.Where(v => v.IsQuotationRejected == 0 && v.TotalInitialPrice > 0).OrderByDescending(v => ((requirement.InclusiveTax ? v.DF_REV_VENDOR_TOTAL_PRICE : v.DF_REV_VENDOR_TOTAL_PRICE_NO_TAX) * v.VendorCurrencyFactor)).ThenBy(p => p.AuctionModifiedDate).ToList();
                        v2.AddRange(vendorDetails.Where(v => v.IsQuotationRejected != 0 && v.TotalInitialPrice > 0).OrderByDescending(v => ((requirement.InclusiveTax ? v.DF_REV_VENDOR_TOTAL_PRICE : v.DF_REV_VENDOR_TOTAL_PRICE_NO_TAX) * v.VendorCurrencyFactor)).ThenBy(p => p.AuctionModifiedDate).ToList());
                        v2.AddRange(vendorDetails.Where(v => v.InitialPrice == 0).ToList());

                    }
                    else
                    {
                        v2 = vendorDetails.Where(v => v.IsQuotationRejected == 0 && v.TotalRunningPrice > 0).OrderByDescending(v => ((requirement.InclusiveTax ? v.DF_REV_VENDOR_TOTAL_PRICE : v.DF_REV_VENDOR_TOTAL_PRICE_NO_TAX) * v.VendorCurrencyFactor)).ThenBy(p => p.AuctionModifiedDate).ToList();
                        v2.AddRange(vendorDetails.Where(v => v.IsQuotationRejected != 0 && v.TotalRunningPrice > 0).OrderByDescending(v => ((requirement.InclusiveTax ? v.DF_REV_VENDOR_TOTAL_PRICE : v.DF_REV_VENDOR_TOTAL_PRICE_NO_TAX) * v.VendorCurrencyFactor)).ThenBy(p => p.AuctionModifiedDate).ToList());
                        v2.AddRange(vendorDetails.Where(v => v.TotalRunningPrice == 0).ToList());

                    }

                    if (v2.Count > 0)
                    {
                        VendorDetails vendor1 = v2[0];
                        if (vendor1.TotalRunningPrice == 0)
                        {
                            requirement.Price = vendor1.TotalInitialPrice;
                        }
                        else if (vendor1.TotalInitialPrice < vendor1.TotalRunningPrice && vendor1.TotalInitialPrice != 0)
                        {
                            requirement.Price = vendor1.TotalInitialPrice;
                        }
                        else
                        {
                            requirement.Price = vendor1.TotalRunningPrice;
                        }
                    }

                    List<VendorDetails> tv2 = v2.Where(v => v.IsQuotationRejected == 0).OrderByDescending(p => DoInclusiveTax(p, requirement.InclusiveTax)).ThenBy(p => p.AuctionModifiedDate).ToList();
                    foreach (VendorDetails vendor in tv2.Where(p => p.DF_REV_VENDOR_TOTAL_PRICE > 0))
                    {
                        vendor.Rank = v2.IndexOf(vendor) + 1;
                    }
                    List<VendorDetails> notApprovedVendors = v2.Where(v => v.IsQuotationRejected != 0).ToList();
                    foreach (VendorDetails vendor in notApprovedVendors)
                    {
                        tv2.Add(vendor);
                    }
                    v2 = tv2;
                    if (tv2 != null && tv2.Count > 0) { } else { tv2 = v2; }
                    if (requirement.CustomerID == userID || requirement.SuperUserID == userID || requirement.CustomerReqAccess == true)
                    {
                        requirement.AuctionVendors = tv2.OrderBy(x => x.Rank == 0).ThenBy(x => x.Rank).ToList();
                    }
                    else
                    {
                        requirement.AuctionVendors = v2.Where(v => v.VendorID == userID).OrderByDescending(p => DoInclusiveTax(p, requirement.InclusiveTax)).ThenBy(p => p.AuctionModifiedDate).ToList();
                    }

                    requirement.SelectedVendor = requirement.AuctionVendors.FirstOrDefault(v => v.VendorID == requirement.SelectedVendorID);
                    int productSNo = 0;
                    string CATEGORY_ID = string.Empty;
                    List<RequirementItems> ListRequirementItems = new List<RequirementItems>();
                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        foreach (DataRow row2 in ds.Tables[2].Rows)
                        {
                            RequirementItems RequirementItems = new RequirementItems();
                            RequirementItems.ProductSNo = productSNo++;
                            RequirementItems.ItemID = row2["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_ID"]) : 0;
                            RequirementItems.RequirementID = row2["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row2["REQ_ID"]) : 0;
                            RequirementItems.ProductIDorName = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                            RequirementItems.ProductNo = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                            RequirementItems.ProductDescription = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                            RequirementItems.ProductQuantity = row2["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row2["QUANTITY"]) : 0;
                            RequirementItems.ProductBrand = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;
                            RequirementItems.OthersBrands = row2["OTHER_BRAND"] != DBNull.Value ? Convert.ToString(row2["OTHER_BRAND"]) : string.Empty;
                            RequirementItems.ProductImageID = row2["IMAGE_ID"] != DBNull.Value ? Convert.ToInt32(row2["IMAGE_ID"]) : 0;
                            RequirementItems.IsDeleted = row2["IS_DELETED"] != DBNull.Value ? Convert.ToInt32(row2["IS_DELETED"]) : 0;
                            RequirementItems.ProductQuantityIn = row2["QUANTITY_IN"] != DBNull.Value ? Convert.ToString(row2["QUANTITY_IN"]) : string.Empty;
                            RequirementItems.SelectedVendorID = row2["SELECTED_VENDOR_ID"] != DBNull.Value ? Convert.ToInt32(row2["SELECTED_VENDOR_ID"]) : 0;
                            RequirementItems.ProductIDorNameCustomer = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                            RequirementItems.ProductNoCustomer = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                            RequirementItems.ProductDescriptionCustomer = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                            RequirementItems.ProductBrandCustomer = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;
                            RequirementItems.ItemMinReduction = row2["ITEM_MIN_REDUCTION"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_MIN_REDUCTION"]) : 0;
                            RequirementItems.ItemLastPrice = row2["LAST_ITEM_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["LAST_ITEM_PRICE"]) : 0;
                            RequirementItems.ITEM_LEVEL_LEAST_PRICE = row2["ITEM_LEVEL_LEAST_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_LEVEL_LEAST_PRICE"]) : 0;
                            RequirementItems.I_LLP_DETAILS = row2["I_LLP_DETAILS"] != DBNull.Value ? Convert.ToString(row2["I_LLP_DETAILS"]) : string.Empty;
                            RequirementItems.ProductCode = row2["PROD_CODE"] != DBNull.Value ? Convert.ToString(row2["PROD_CODE"]) : string.Empty;
                            RequirementItems.ProductQuotationTemplateJson = row2["PRODUCT_QUOTATION_JSON"] != DBNull.Value ? Convert.ToString(row2["PRODUCT_QUOTATION_JSON"]) : string.Empty;
                            RequirementItems.CatalogueItemID = row2["CATALOGUE_ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["CATALOGUE_ITEM_ID"]) : 0;
                            RequirementItems.ITEM_PR_NUMBER = row2["ITEM_PR_NUMBER"] != DBNull.Value ? Convert.ToString(row2["ITEM_PR_NUMBER"]) : string.Empty;
                            RequirementItems.IS_GMP_PR_ITEM = row2["IS_GMP_PR_ITEM"] != DBNull.Value ? Convert.ToInt32(row2["IS_GMP_PR_ITEM"]) : 0;
                            RequirementItems.ITEM_PLANTS = row2["ITEM_PLANTS"] != DBNull.Value ? Convert.ToString(row2["ITEM_PLANTS"]) : string.Empty;
                            RequirementItems.ITEM_PR_QUANTITY = row2["ITEM_PR_QUANTITY"] != DBNull.Value ? Convert.ToDecimal(row2["ITEM_PR_QUANTITY"]) : 0;
                            RequirementItems.PR_WBS_CODE = row2["PR_WBS_CODE"] != DBNull.Value ? Convert.ToString(row2["PR_WBS_CODE"]) : string.Empty;
                            RequirementItems.HsnCode = row2["HSN_CODE"] != DBNull.Value ? Convert.ToString(row2["HSN_CODE"]) : string.Empty;
                            RequirementItems.CeilingPrice = row2["CEILING_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["CEILING_PRICE"]) : 0;
                            RequirementItems.HsnCodeCustomer = row2["HSN_CODE"] != DBNull.Value ? Convert.ToString(row2["HSN_CODE"]) : string.Empty;
                            RequirementItems.IsCoreProductCategory = row2["IS_CORE_CAT"] != DBNull.Value ? Convert.ToInt32(row2["IS_CORE_CAT"]) : 0;
                            RequirementItems.ProductDeliveryDetails = row2["DELIERY_DETAILS"] != DBNull.Value ? Convert.ToString(row2["DELIERY_DETAILS"]) : string.Empty;
                            CATEGORY_ID = row2["CATEGORY_ID"] != DBNull.Value ? Convert.ToString(row2["CATEGORY_ID"]) : string.Empty;
                            RequirementItems.CATEGORY_ID = CATEGORY_ID;
                            ListRequirementItems.Add(RequirementItems);
                        }

                        requirement.ItemSNoCount = productSNo;
                    }

                    if (ds.Tables[3].Rows.Count > 0)
                    {
                        foreach (DataRow row2 in ds.Tables[3].Rows)
                        {
                            int uID = row2["U_ID"] != DBNull.Value ? Convert.ToInt32(row2["U_ID"]) : 0;
                            int itemid = row2["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_ID"]) : 0;

                            double price = row2["PRICE"] != DBNull.Value ? Convert.ToDouble(row2["PRICE"]) : 0;
                            string name = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                            string no = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                            string des = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                            string brand = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;
                            double unitprice = row2["UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["UNIT_PRICE"]) : 0;

                            double revunitprice = row2["REV_UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["REV_UNIT_PRICE"]) : 0;
                            double revItemPrice = row2["REVICED_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["REVICED_PRICE"]) : 0;
                            double revunitdiscount = row2["REV_UNIT_DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row2["REV_UNIT_DISCOUNT"]) : 0;
                            double itemRevFreightCharges = row2["ITEM_REV_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_REV_FREIGHT_CHARGES"]) : 0;

                            //#CB-0-2018-12-05
                            double revunitpriceCB = row2["REV_UNIT_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row2["REV_UNIT_PRICE_CB"]) : 0;
                            double revItemPriceCB = row2["REVICED_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row2["REVICED_PRICE_CB"]) : 0;
                            double revunitdiscountCB = row2["REV_UNIT_DISCOUNT_CB"] != DBNull.Value ? Convert.ToDouble(row2["REV_UNIT_DISCOUNT_CB"]) : 0;
                            double itemRevFreightChargesCB = row2["ITEM_REV_FREIGHT_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_REV_FREIGHT_CHARGES_CB"]) : 0;


                            double cgst = row2["C_GST"] != DBNull.Value ? Convert.ToDouble(row2["C_GST"]) : 0;
                            double sgst = row2["S_GST"] != DBNull.Value ? Convert.ToDouble(row2["S_GST"]) : 0;
                            double igst = row2["I_GST"] != DBNull.Value ? Convert.ToDouble(row2["I_GST"]) : 0;
                            double unitmrp = row2["UNIT_MRP"] != DBNull.Value ? Convert.ToDouble(row2["UNIT_MRP"]) : 0;
                            double unitdiscount = row2["UNIT_DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row2["UNIT_DISCOUNT"]) : 0;

                            string otherBrands = row2["OTHER_BRANDS"] != DBNull.Value ? Convert.ToString(row2["OTHER_BRANDS"]) : string.Empty;
                            string vendorUnits = row2["VENDOR_UNITS"] != DBNull.Value ? Convert.ToString(row2["VENDOR_UNITS"]) : string.Empty;
                            bool isRegret = row2["IS_REGRET"] != DBNull.Value ? (Convert.ToInt32(row2["IS_REGRET"]) > 0 ? true : false) : false;
                            string regretComments = row2["REGRET_COMMENTS"] != DBNull.Value ? Convert.ToString(row2["REGRET_COMMENTS"]) : string.Empty;
                            string itemLevelInitialComments = row2["ITEM_LEVEL_INITIALCOMMENT"] != DBNull.Value ? Convert.ToString(row2["ITEM_LEVEL_INITIALCOMMENT"]) : string.Empty;
                            string itemLevelRevComments = row2["ITEM_LEVEL_REVCOMMENT"] != DBNull.Value ? Convert.ToString(row2["ITEM_LEVEL_REVCOMMENT"]) : string.Empty;
                            double itemFreightCharges = row2["ITEM_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_FREIGHT_CHARGES"]) : 0;
                            double itemFreightTAX = row2["ITEM_FREIGHT_TAX"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_FREIGHT_TAX"]) : 0;

                            double LAST_BID_REV_UNIT_PRICE = row2["LAST_BID_REV_UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["LAST_BID_REV_UNIT_PRICE"]) : 0;

                            string ProductQuotationTemplateJson = row2["ProductQuotationTemplateJson"] != DBNull.Value ? Convert.ToString(row2["ProductQuotationTemplateJson"]) : string.Empty;
                            string HsnCode = row2["HSN_CODE"] != DBNull.Value ? Convert.ToString(row2["HSN_CODE"]) : string.Empty;
                            int catalogueItemID = row2["CATALOGUE_ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["CATALOGUE_ITEM_ID"]) : 0;
                            int itemRank = row2["ITEM_RANK"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_RANK"]) : 0;
                            int isItemQuotationRejected = row2["IS_ITEM_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToInt32(row2["IS_ITEM_QUOTATION_REJECTED"]) : -1;
                            string itemQuotationRejectedComment = row2["ITEM_QUOTATION_REJECTED_COMMENT"] != DBNull.Value ? Convert.ToString(row2["ITEM_QUOTATION_REJECTED_COMMENT"]) : string.Empty;
                            int isCoreProductCategory = row2["IS_CORE_CAT"] != DBNull.Value ? Convert.ToInt32(row2["IS_CORE_CAT"]) : 0;
                            double ceilingPrice = row2["CEILING_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["CEILING_PRICE"]) : 0;
                            //REV_UNIT_PRICE_CB, REVICED_PRICE_CB, REV_UNIT_DISCOUNT_CB, ITEM_REV_FREIGHT_CHARGES_CB, REV_ITEM_PRICE_CB




                            List<VendorDetails> tempV = new List<VendorDetails>();
                            tempV = v2.Where(v => v != null && v.VendorID == uID).ToList();
                            if (tempV.Count > 0)
                            {
                                v2.Where(v => v != null && v.VendorID == uID).FirstOrDefault().marginRankDiscount += unitdiscount;
                                v2.Where(v => v != null && v.VendorID == uID).FirstOrDefault().marginRankRevDiscount += revunitdiscount;
                            }

                            if (uID == userID)
                            {
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ItemPrice = price;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ProductIDorName = name;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ProductNo = no;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ProductDescription = des;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ProductBrand = brand;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().OthersBrands = otherBrands;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().UnitPrice = unitprice;

                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().RevUnitPrice = revunitprice;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().RevItemPrice = revItemPrice;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().RevUnitDiscount = revunitdiscount;
                                //ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ItemRevFreightCharges = itemRevFreightCharges;

                                //#CB-0-2018-12-05
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().RevUnitPriceCB = revunitpriceCB;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().RevItemPriceCB = revItemPriceCB;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().RevUnitDiscountCB = revunitdiscountCB;
                                //ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ItemRevFreightChargesCB = itemRevFreightChargesCB;


                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().CGst = cgst;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().SGst = sgst;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().IGst = igst;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().Gst = cgst + sgst + igst;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().UnitMRP = unitmrp;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().UnitDiscount = unitdiscount;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().VendorUnits = vendorUnits;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().IsRegret = isRegret;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().RegretComments = regretComments;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ItemLevelInitialComments = itemLevelInitialComments;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ItemLevelRevComments = itemLevelRevComments;
                                //ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ItemFreightCharges = itemFreightCharges;
                                //ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ItemFreightTAX = itemFreightTAX;

                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().LAST_BID_REV_UNIT_PRICE = LAST_BID_REV_UNIT_PRICE;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().LAST_BID_REDUCTION_PRICE = revunitprice - LAST_BID_REV_UNIT_PRICE;

                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ProductQuotationTemplateJson = ProductQuotationTemplateJson;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().HsnCode = HsnCode;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().CatalogueItemID = catalogueItemID;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ItemRank = itemRank;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().IsItemQuotationRejected = isItemQuotationRejected;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ItemQuotationRejectedComment = itemQuotationRejectedComment;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().IsCoreProductCategory = isCoreProductCategory;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().CATEGORY_ID = CATEGORY_ID;
                                if (ceilingPrice > 0)
                                {
                                    ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().CeilingPrice = ceilingPrice;
                                }
                            }
                        }
                    }

                    requirement.ListRequirementItems = ListRequirementItems;
                    string V_CATEGORY_ID = string.Empty;
                    foreach (VendorDetails V in vendorDetails)
                    {
                        int productSNo1 = 0;
                        List<RequirementItems> ListRequirementItems1 = new List<RequirementItems>();
                        if (ds.Tables[2].Rows.Count > 0)
                        {
                            foreach (DataRow row2 in ds.Tables[2].Rows)
                            {
                                RequirementItems RequirementItems = new RequirementItems();
                                RequirementItems.ProductSNo = productSNo1++;
                                RequirementItems.ItemID = row2["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_ID"]) : 0;
                                RequirementItems.RequirementID = row2["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row2["REQ_ID"]) : 0;
                                RequirementItems.ProductIDorName = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                                RequirementItems.ProductNo = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                                RequirementItems.ProductDescription = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                                RequirementItems.ProductQuantity = row2["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row2["QUANTITY"]) : 0;
                                RequirementItems.ProductBrand = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;
                                RequirementItems.OthersBrands = row2["OTHER_BRAND"] != DBNull.Value ? Convert.ToString(row2["OTHER_BRAND"]) : string.Empty;
                                RequirementItems.ProductDeliveryDetails = row2["DELIERY_DETAILS"] != DBNull.Value ? Convert.ToString(row2["DELIERY_DETAILS"]) : string.Empty;
                                RequirementItems.ProductImageID = row2["IMAGE_ID"] != DBNull.Value ? Convert.ToInt32(row2["IMAGE_ID"]) : 0;
                                RequirementItems.IsDeleted = row2["IS_DELETED"] != DBNull.Value ? Convert.ToInt32(row2["IS_DELETED"]) : 0;
                                RequirementItems.ProductQuantityIn = row2["QUANTITY_IN"] != DBNull.Value ? Convert.ToString(row2["QUANTITY_IN"]) : string.Empty;
                                RequirementItems.SelectedVendorID = row2["SELECTED_VENDOR_ID"] != DBNull.Value ? Convert.ToInt32(row2["SELECTED_VENDOR_ID"]) : 0;
                                RequirementItems.ProductIDorNameCustomer = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                                RequirementItems.ProductNoCustomer = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                                RequirementItems.ProductDescriptionCustomer = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                                RequirementItems.ProductBrandCustomer = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;
                                RequirementItems.ItemMinReduction = row2["ITEM_MIN_REDUCTION"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_MIN_REDUCTION"]) : 0;
                                RequirementItems.ItemLastPrice = row2["LAST_ITEM_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["LAST_ITEM_PRICE"]) : 0;
                                RequirementItems.ITEM_LEVEL_LEAST_PRICE = row2["ITEM_LEVEL_LEAST_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_LEVEL_LEAST_PRICE"]) : 0;
                                RequirementItems.I_LLP_DETAILS = row2["I_LLP_DETAILS"] != DBNull.Value ? Convert.ToString(row2["I_LLP_DETAILS"]) : string.Empty;
                                RequirementItems.ProductCode = row2["PROD_CODE"] != DBNull.Value ? Convert.ToString(row2["PROD_CODE"]) : string.Empty;
                                RequirementItems.ProductQuotationTemplateJson = row2["PRODUCT_QUOTATION_JSON"] != DBNull.Value ? Convert.ToString(row2["PRODUCT_QUOTATION_JSON"]) : string.Empty;
                                RequirementItems.CatalogueItemID = row2["CATALOGUE_ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["CATALOGUE_ITEM_ID"]) : 0;
                                RequirementItems.ITEM_PR_NUMBER = row2["ITEM_PR_NUMBER"] != DBNull.Value ? Convert.ToString(row2["ITEM_PR_NUMBER"]) : string.Empty;
                                RequirementItems.ITEM_PLANTS = row2["ITEM_PLANTS"] != DBNull.Value ? Convert.ToString(row2["ITEM_PLANTS"]) : string.Empty;
                                RequirementItems.IS_GMP_PR_ITEM = row2["IS_GMP_PR_ITEM"] != DBNull.Value ? Convert.ToInt32(row2["IS_GMP_PR_ITEM"]) : 0;
                                RequirementItems.ITEM_PR_QUANTITY = row2["ITEM_PR_QUANTITY"] != DBNull.Value ? Convert.ToDecimal(row2["ITEM_PR_QUANTITY"]) : 0;
                                RequirementItems.PR_WBS_CODE = row2["PR_WBS_CODE"] != DBNull.Value ? Convert.ToString(row2["PR_WBS_CODE"]) : string.Empty;
                                RequirementItems.HsnCode = row2["HSN_CODE"] != DBNull.Value ? Convert.ToString(row2["HSN_CODE"]) : string.Empty;
                                RequirementItems.HsnCodeCustomer = row2["HSN_CODE"] != DBNull.Value ? Convert.ToString(row2["HSN_CODE"]) : string.Empty;
                                RequirementItems.IsCoreProductCategory = row2["IS_CORE_CAT"] != DBNull.Value ? Convert.ToInt32(row2["IS_CORE_CAT"]) : 0;
                                //V_CATEGORY_ID = row2["CATEGORY_ID"] != DBNull.Value ? Convert.ToString(row2["CATEGORY_ID"]) : string.Empty;
                                //RequirementItems.CATEGORY_ID = V_CATEGORY_ID;
                                //RequirementItems.IsItemQuotationRejected = -1;
                                ListRequirementItems1.Add(RequirementItems);
                            }
                        }

                        if (ds.Tables[3].Rows.Count > 0)
                        {
                            foreach (DataRow row2 in ds.Tables[3].Rows)
                            {
                                int uID = row2["U_ID"] != DBNull.Value ? Convert.ToInt32(row2["U_ID"]) : 0;
                                int itemid = row2["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_ID"]) : 0;
                                double price = row2["PRICE"] != DBNull.Value ? Convert.ToDouble(row2["PRICE"]) : 0;
                                string name = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                                string no = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                                string des = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                                string brand = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;
                                double unitprice = row2["UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["UNIT_PRICE"]) : 0;
                                double revunitprice = row2["REV_UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["REV_UNIT_PRICE"]) : 0;
                                double revItemPrice = row2["REVICED_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["REVICED_PRICE"]) : 0;
                                double cgst = row2["C_GST"] != DBNull.Value ? Convert.ToDouble(row2["C_GST"]) : 0;
                                double sgst = row2["S_GST"] != DBNull.Value ? Convert.ToDouble(row2["S_GST"]) : 0;
                                double igst = row2["I_GST"] != DBNull.Value ? Convert.ToDouble(row2["I_GST"]) : 0;
                                double unitmrp = row2["UNIT_MRP"] != DBNull.Value ? Convert.ToDouble(row2["UNIT_MRP"]) : 0;
                                double unitdiscount = row2["UNIT_DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row2["UNIT_DISCOUNT"]) : 0;
                                double revunitdiscount = row2["REV_UNIT_DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row2["REV_UNIT_DISCOUNT"]) : 0;
                                string otherBrands = row2["OTHER_BRANDS"] != DBNull.Value ? Convert.ToString(row2["OTHER_BRANDS"]) : string.Empty;
                                string vendorUnits = row2["VENDOR_UNITS"] != DBNull.Value ? Convert.ToString(row2["VENDOR_UNITS"]) : string.Empty;
                                bool isRegret = row2["IS_REGRET"] != DBNull.Value ? (Convert.ToInt32(row2["IS_REGRET"]) > 0 ? true : false) : false;
                                string regretComments = row2["REGRET_COMMENTS"] != DBNull.Value ? Convert.ToString(row2["REGRET_COMMENTS"]) : string.Empty;
                                string itemLevelInitialComments = row2["ITEM_LEVEL_INITIALCOMMENT"] != DBNull.Value ? Convert.ToString(row2["ITEM_LEVEL_INITIALCOMMENT"]) : string.Empty;
                                string itemLevelRevComments = row2["ITEM_LEVEL_REVCOMMENT"] != DBNull.Value ? Convert.ToString(row2["ITEM_LEVEL_REVCOMMENT"]) : string.Empty;
                                double itemFreightCharges = row2["ITEM_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_FREIGHT_CHARGES"]) : 0;
                                double itemFreightTAX = row2["ITEM_FREIGHT_TAX"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_FREIGHT_TAX"]) : 0;
                                double itemRevFreightCharges = row2["ITEM_REV_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_REV_FREIGHT_CHARGES"]) : 0;

                                //#CB-0-2018-12-05
                                double revunitpriceCB = row2["REV_UNIT_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row2["REV_UNIT_PRICE_CB"]) : 0;
                                double revItemPriceCB = row2["REVICED_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row2["REVICED_PRICE_CB"]) : 0;
                                double revunitdiscountCB = row2["REV_UNIT_DISCOUNT_CB"] != DBNull.Value ? Convert.ToDouble(row2["REV_UNIT_DISCOUNT_CB"]) : 0;
                                double itemRevFreightChargesCB = row2["ITEM_REV_FREIGHT_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_REV_FREIGHT_CHARGES_CB"]) : 0;

                                double LAST_BID_REV_UNIT_PRICE = row2["LAST_BID_REV_UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["LAST_BID_REV_UNIT_PRICE"]) : 0;

                                string ProductQuotationTemplateJson = row2["ProductQuotationTemplateJson"] != DBNull.Value ? Convert.ToString(row2["ProductQuotationTemplateJson"]) : string.Empty;
                                string HsnCode = row2["HSN_CODE"] != DBNull.Value ? Convert.ToString(row2["HSN_CODE"]) : string.Empty;
                                int catalogueItemID = row2["CATALOGUE_ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["CATALOGUE_ITEM_ID"]) : 0;
                                int itemRank = row2["ITEM_RANK"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_RANK"]) : 0;
                                int isItemQuotationRejected = row2["IS_ITEM_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToInt32(row2["IS_ITEM_QUOTATION_REJECTED"]) : -1;
                                string itemQuotationRejectedComment = row2["ITEM_QUOTATION_REJECTED_COMMENT"] != DBNull.Value ? Convert.ToString(row2["ITEM_QUOTATION_REJECTED_COMMENT"]) : string.Empty;
                                int isCoreProductCategory = row2["IS_CORE_CAT"] != DBNull.Value ? Convert.ToInt32(row2["IS_CORE_CAT"]) : 0;
                                double ceilingPrice = row2["CEILING_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["CEILING_PRICE"]) : 0;
                                if (ceilingPrice <= 0)
                                {
                                    ceilingPrice = ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().CeilingPrice;
                                }
                                // string ProductCode = row2["PROD_CODE"] != DBNull.Value ? Convert.ToString(row2["PROD_CODE"]) : string.Empty;

                                if (uID == V.VendorID)
                                {
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ItemPrice = price;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ProductIDorName = name;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ProductNo = no;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ProductDescription = des;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ProductBrand = brand;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().RevItemPrice = revItemPrice;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().UnitPrice = unitprice;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().RevUnitPrice = revunitprice;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().CGst = cgst;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().SGst = sgst;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().IGst = igst;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().Gst = cgst + sgst + igst;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().UnitMRP = unitmrp;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().UnitDiscount = unitdiscount;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().RevUnitDiscount = revunitdiscount;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().VendorUnits = vendorUnits;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().IsRegret = isRegret;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().RegretComments = regretComments;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ItemLevelInitialComments = itemLevelInitialComments;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ItemLevelRevComments = itemLevelRevComments;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().RevUnitPriceCB = revunitpriceCB;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().RevItemPriceCB = revItemPriceCB;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().RevUnitDiscountCB = revunitdiscountCB;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().LAST_BID_REV_UNIT_PRICE = LAST_BID_REV_UNIT_PRICE;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().LAST_BID_REDUCTION_PRICE = revunitprice - LAST_BID_REV_UNIT_PRICE;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ProductQuotationTemplateJson = ProductQuotationTemplateJson;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().HsnCode = HsnCode;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().CatalogueItemID = catalogueItemID;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ItemRank = itemRank;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().IsItemQuotationRejected = isItemQuotationRejected;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().ItemQuotationRejectedComment = itemQuotationRejectedComment;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().IsCoreProductCategory = isCoreProductCategory;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<RequirementItems>().CeilingPrice = ceilingPrice;
                                }
                            }
                        }

                        V.ListRequirementItems = ListRequirementItems1;
                    }

                    if (requirement.IsDiscountQuotation == 1 || requirement.IsDiscountQuotation == 0)
                    {

                        foreach (VendorDetails vendor in v2.Where(v => v.IsQuotationRejected == 0).OrderByDescending(v => DoInclusiveTax(v, requirement.InclusiveTax)).ThenBy(p => p.AuctionModifiedDate).ToList())
                        {
                            vendor.Rank = vendor.Rank;
                        }

                        requirement.RequirementVendorsList = v2.Where(v => v.IsQuotationRejected == 0).OrderByDescending(v => DoInclusiveTax(v, requirement.InclusiveTax)).ThenBy(p => p.AuctionModifiedDate).ToList();
                        if (requirement.CustomerID == userID || requirement.SuperUserID == userID || requirement.CustomerReqAccess == true)
                        {
                            requirement.AuctionVendors = v2.OrderBy(x => x.Rank == 0).ThenBy(x => x.Rank).ToList();
                        }
                        else
                        {
                            requirement.AuctionVendors = v2.Where(v => v.VendorID == userID).OrderByDescending(v => DoInclusiveTax(v, requirement.InclusiveTax)).ThenBy(p => p.AuctionModifiedDate).ToList();
                        }

                        tv2 = v2.Where(v => v.IsQuotationRejected == 0).OrderByDescending(v => DoInclusiveTax(v, requirement.InclusiveTax)).ThenBy(p => p.AuctionModifiedDate).ToList();
                    }
                    else
                    {
                        v2 = v2.OrderByDescending(v => v.SumOfMargin).ToList();
                        List<VendorDetails> approvedV2 = new List<VendorDetails>();
                        approvedV2 = v2.Where(v => v.IsQuotationRejected == 0 && v.SumOfMargin > 0).OrderByDescending(v => v.SumOfMargin * (v.VendorCurrencyFactor)).ThenBy(p => p.AuctionModifiedDate).ToList();
                        foreach (VendorDetails vendor in v2)
                        {
                            if (vendor.IsQuotationRejected == 0 && vendor.SumOfMargin > 0)
                            {
                                vendor.Rank = approvedV2.IndexOf(vendor) + 1;
                            }
                        }

                        requirement.RequirementVendorsList = v2;

                        if (requirement.CustomerID == userID || requirement.SuperUserID == userID || requirement.CustomerReqAccess == true)
                        {
                            v2 = v2.Where(v => v.IsQuotationRejected == 0).OrderBy(v => v.Rank).ToList();
                            foreach (VendorDetails vendor in requirement.AuctionVendors)
                            {
                                if (vendor.IsQuotationRejected != 0)
                                {
                                    v2.Add(vendor);
                                }
                            }
                        }
                        else
                        {
                            v2 = v2.Where(v => v.VendorID == userID).OrderByDescending(v => v.SumOfMargin).ThenBy(p => p.AuctionModifiedDate).ToList();
                        }

                        requirement.AuctionVendors = v2;
                    }


                    int taxSNo = 0;
                    List<RequirementTaxes> ListRequirementTaxes = new List<RequirementTaxes>();
                    if (ds.Tables[4].Rows.Count > 0)
                    {
                        foreach (DataRow row4 in ds.Tables[4].Rows)
                        {
                            RequirementTaxes requirementTaxes = new RequirementTaxes();
                            requirementTaxes.TaxSNo = taxSNo++;
                            int uID = row4["U_ID"] != DBNull.Value ? Convert.ToInt32(row4["U_ID"]) : 0;
                            if (uID == userID || requirement.CustomerReqAccess == true)
                            {
                                requirementTaxes.TaxID = row4["QT_ID"] != DBNull.Value ? Convert.ToInt32(row4["QT_ID"]) : 0;
                                requirementTaxes.TaxName = row4["TAX_NAME"] != DBNull.Value ? Convert.ToString(row4["TAX_NAME"]) : string.Empty;
                                requirementTaxes.TaxPercentage = row4["TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row4["TAX_PERCENTAGE"]) : 0;
                                requirementTaxes.IsTaxDeleted = row4["IS_TAX_DELETED"] != DBNull.Value ? Convert.ToInt32(row4["IS_TAX_DELETED"]) : 0;
                                ListRequirementTaxes.Add(requirementTaxes);
                            }
                        }

                        requirement.TaxSNoCount = taxSNo;
                    }

                    requirement.ListRequirementTaxes = ListRequirementTaxes;

                    List<LotAuctions> lotAuctions = new List<LotAuctions>();
                    if (ds.Tables[6].Rows.Count > 0)
                    {
                        foreach (DataRow row6 in ds.Tables[6].Rows)
                        {
                            LotAuctions lotAuction = new LotAuctions();

                            lotAuction.ReqId = row6["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row6["REQ_ID"]) : 0;

                            lotAuction.ReqTitle = row6["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row6["REQ_TITLE"]) : string.Empty;


                            lotAuctions.Add(lotAuction);
                        }
                    }
                    requirement.LotAuctions = lotAuctions;


                    List<CustomerEmails> customerEmailsList = new List<CustomerEmails>();
                    if (ds.Tables[7].Rows.Count > 0)
                    {
                        foreach (DataRow row7 in ds.Tables[7].Rows)
                        {
                            CustomerEmails customerEmail = new CustomerEmails();

                            customerEmail.WtcID = row7["wtc_id"] != DBNull.Value ? Convert.ToInt32(row7["wtc_id"]) : 0;
                            customerEmail.UserID = row7["user_id"] != DBNull.Value ? Convert.ToInt32(row7["user_id"]) : 0;
                            customerEmail.Status = row7["status"] != DBNull.Value ? Convert.ToInt32(row7["status"]) : 0;
                            customerEmail.ReqId = row7["req_id"] != DBNull.Value ? Convert.ToInt32(row7["req_id"]) : 0;

                            customerEmail.Mail = row7["email_id"] != DBNull.Value ? Convert.ToString(row7["email_id"]) : string.Empty;


                            customerEmailsList.Add(customerEmail);
                        }
                    }
                    requirement.CustomerEmails = customerEmailsList.Where(m => m.Status == 1 && requirement.RequirementID == m.ReqId).ToList();


                }

                ds = new DataSet();
            }
            catch (Exception ex)
            {
                Requirement req = new Requirement();
                req.ErrorMessage = ex.Message;
                return req;
            }

            return requirement;
        }

        private List<CategoryObj> GetUserSubCategories(int userID, string sessionID)
        {
            List<CategoryObj> Categories = new List<CategoryObj>();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", userID);
                sd.Add("P_SESSION_ID", sessionID);
                DataSet ds = sqlHelper.SelectList("cp_GetUserSubCategoriesForUser", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        CategoryObj cat = new CategoryObj();
                        cat.ID = row["CAT_ID"] != DBNull.Value ? Convert.ToInt32(row["CAT_ID"]) : 0;
                        cat.Category = row["CATEGORY"] != DBNull.Value ? Convert.ToString(row["CATEGORY"]) : string.Empty;
                        cat.Subcategory = row["SUBCATEGORY"] != DBNull.Value ? Convert.ToString(row["SUBCATEGORY"]) : string.Empty;
                        Categories.Add(cat);
                    }
                }
            }
            catch (Exception ex)
            {
                CategoryObj c = new CategoryObj();
                c.ErrorMessage = ex.Message;
                Categories.Add(c);
            }

            return Categories;
        }

        private List<UserInfo> GetUsersInvolved(int reqid, string type, int userID = 0)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<UserInfo> list = new List<UserInfo>();
            try
            {
                sd.Add("P_REQ_ID", reqid);
                sd.Add("P_TYPE", type);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("fwd_GetUsersInvolved", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        UserInfo user = new UserInfo();
                        user.UserID = row["U_ID"] != DBNull.Value ? Convert.ToString(row["U_ID"]) : string.Empty;
                        user.FirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                        user.LastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                        user.PhoneNum = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
                        user.Email = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                        user.UserType = row["U_TYPE"] != DBNull.Value ? Convert.ToString(row["U_TYPE"]) : string.Empty;
                        user.PhoneID = row["U_PHONE_TOKEN"] != DBNull.Value ? Convert.ToString(row["U_TYPE"]) : string.Empty;
                        user.PhoneOS = row["U_PHONE_OS"] != DBNull.Value ? Convert.ToString(row["U_TYPE"]) : string.Empty;
                        user.AltPhoneNum = row["U_ALTPHONE"] != DBNull.Value ? row["U_ALTPHONE"].ToString() : string.Empty;
                        user.AltEmail = row["U_ALTEMAIL"] != DBNull.Value ? row["U_ALTEMAIL"].ToString() : string.Empty;
                        list.Add(user);
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return list;
        }

        private string GenerateReportRequirementHTML(Requirement requirement, double itemWiseSavings, UserDetails customer)
        {
            string vendorsListHtml = string.Empty;
            foreach (VendorDetails vendorDetails in requirement.AuctionVendors)
            {
                string vendorDetails_Rank = "N/A";
                if (vendorDetails.Rank > 0)
                {
                    vendorDetails_Rank = vendorDetails.Rank.ToString();
                }

                string vendorDetails_TotalInitialPrice = "N/A";
                if (vendorDetails.TotalInitialPrice > 0)
                {
                    vendorDetails_TotalInitialPrice = vendorDetails.TotalInitialPrice.ToString();
                }

                string vendorDetails_TotalRunningPrice = "N/A";
                if (vendorDetails.TotalRunningPrice > 0)
                {
                    vendorDetails_TotalRunningPrice = vendorDetails.TotalRunningPrice.ToString();
                }

                string vendorDetails_ReductionPercentage = "N/A";
                if (vendorDetails.ReductionPercentage > 0)
                {
                    vendorDetails_ReductionPercentage = vendorDetails.ReductionPercentage.ToString();
                }

                string vendorsList = string.Empty;
                vendorsList = GenerateEmailBody("ReportsReqVendors");
                vendorsList = String.Format(vendorsList, vendorDetails.CompanyName, vendorDetails.VendorName, vendorDetails_Rank, vendorDetails_TotalInitialPrice, vendorDetails_TotalRunningPrice, vendorDetails_ReductionPercentage);
                vendorsListHtml += vendorsList;
            }

            string reqItemsListHtml = string.Empty;
            string html = string.Empty;
            if (!requirement.IsTabular)
            {
                html = "ReportsRequirementText.html";
            }

            if (requirement.IsTabular)
            {
                foreach (RequirementItems reqItems in requirement.ListRequirementItems)
                {
                    string reqItemsList = string.Empty;
                    reqItemsList = GenerateEmailBody("ReportsReqItems");
                    reqItemsList = String.Format(reqItemsList,
                        reqItems.ProductIDorName,
                        reqItems.ProductNo,
                        reqItems.ProductDescription,
                        reqItems.ProductQuantity,
                        reqItems.ProductQuantityIn,
                        reqItems.ProductBrand,
                        reqItems.OthersBrands);
                    reqItemsListHtml += reqItemsList;
                }

                html = "ReportsTabularRequirementText.html";
                requirement.Description = reqItemsListHtml;
            }

            string requirement_NoOfVendorsInvited = "N/A";
            if (requirement.NoOfVendorsInvited > 0)
            {
                requirement_NoOfVendorsInvited = requirement.NoOfVendorsInvited.ToString();
            }

            string requirement_NoOfVendorsParticipated = "N/A";
            if (requirement.NoOfVendorsParticipated > 0)
            {
                requirement_NoOfVendorsParticipated = requirement.NoOfVendorsParticipated.ToString();
            }

            string requirement_PriceBeforeNegotiation = "N/A";
            if (requirement.PriceBeforeNegotiation > 0)
            {
                requirement_PriceBeforeNegotiation = requirement.PriceBeforeNegotiation.ToString();
            }

            string requirement_priceAfterNegotiation = "N/A";
            if (requirement.priceAfterNegotiation > 0)
            {
                requirement_priceAfterNegotiation = requirement.priceAfterNegotiation.ToString();
            }

            string requirement_Savings = "N/A";
            if (requirement.Savings > 0)
            {
                requirement_Savings = requirement.Savings.ToString();
            }

            string requirement_itemWiseSavings = "N/A";
            if (itemWiseSavings > 0)
            {
                requirement_itemWiseSavings = itemWiseSavings.ToString();
            }

            string filestring = File.ReadAllText(HttpContext.Current.Server.MapPath(Utilities.FILE_URL + html));
            if (string.IsNullOrEmpty(customer.LogoURL))
            {
                customer.LogoURL = "/img/logo.png";
            }

            customer.LogoURL = customer.LogoURL.Replace("/Services/auctionFiles//Services/auctionFiles/", "/Services/auctionFiles/");
            string htmlRows = string.Empty;
            try
            {
                htmlRows = String.Format(filestring,
                requirement.RequirementID.ToString(),
                requirement.Title.ToString(),
                requirement.PostedOn.ToString() + " (UTC + 0)",
                requirement_NoOfVendorsInvited, // 3
                requirement_NoOfVendorsParticipated,
                requirement_PriceBeforeNegotiation,
                requirement_priceAfterNegotiation,
                requirement_Savings.ToString(), // 7
                requirement_itemWiseSavings.ToString(),
                requirement.Description.ToString(), // 9
                requirement.ReqComments.ToString(),
                requirement.PaymentTerms.ToString(), //11
                requirement.DeliveryTime.ToString(),
                requirement.DeliveryLocation.ToString(),
                requirement.Urgency.ToString(),
                requirement.QuotationFreezTime.ToString(),
                requirement.Currency.ToString(), // 16
                vendorsListHtml.ToString(),
                "http://prm360.com" + customer.LogoURL,
                customer.CompanyName.ToString()
               );
            }
            catch
            {
            }

            return htmlRows;
        }

        private string GenerateReportRequirementItemstHTML(Requirement requirement, PriceComparison priceComparision, double itemWiseSavings, UserDetails customer)
        {
            string vendorsListHtml = string.Empty;
            foreach (VendorDetails vendorDetails in requirement.AuctionVendors)
            {
                string vendorsList = string.Empty;
                vendorsList = GenerateEmailBody("ReportsReqVendors");
                vendorsList = String.Format(vendorsList, vendorDetails.CompanyName, vendorDetails.VendorName, vendorDetails.Rank, vendorDetails.TotalInitialPrice, vendorDetails.TotalRunningPrice, vendorDetails.ReductionPercentage);
                vendorsListHtml += vendorsList;
            }

            string reqItemsListHtml = string.Empty;
            string reqItemsWiseListHtml = string.Empty;
            string html = string.Empty;
            if (!requirement.IsTabular)
            {
                html = "ReportsItemWise.html";
            }

            if (requirement.IsTabular)
            {
                foreach (RequirementItems reqItems in requirement.ListRequirementItems)
                {
                    string reqItemsList = string.Empty;
                    reqItemsList = GenerateEmailBody("ReportsReqItems");
                    reqItemsList = String.Format(reqItemsList, reqItems.ProductIDorName, reqItems.ProductNo, reqItems.ProductDescription, reqItems.ProductQuantity, reqItems.ProductQuantityIn, reqItems.ProductBrand, reqItems.OthersBrands);
                    reqItemsListHtml += reqItemsList;
                }

                html = "ReportsItemWise.html";
                requirement.Description = reqItemsListHtml;
                string L1CompanyName = requirement.AuctionVendors[0].CompanyName;
                if (priceComparision.PriceCompareObject != null)
                {
                    foreach (PriceCompareObject pco in priceComparision.PriceCompareObject)
                    {
                        string reqItemsListWise = string.Empty;
                        reqItemsListWise = GenerateEmailBody("ReportsReqItemsWise");
                        reqItemsListWise = String.Format(reqItemsListWise, pco.ProductName, pco.LeastBidderPrice, L1CompanyName,
                            pco.BrandL1, pco.DurationL1, pco.PaymentL1, pco.MinPrice, pco.VendorName, pco.Brand, pco.Duration,
                            pco.Payment, pco.LeastBidderUnitPrice, pco.MinUnitPrice, pco.OtherPropertiesL1, pco.OtherProperties);
                        reqItemsWiseListHtml += reqItemsListWise;
                    }
                }
            }

            string requirement_Savings = "N/A";
            if (requirement.Savings > 0)
            {
                requirement_Savings = requirement.Savings.ToString();
            }

            string requirement_itemWiseSavings = "N/A";
            if (itemWiseSavings > 0)
            {
                requirement_itemWiseSavings = itemWiseSavings.ToString();
            }

            string filestring = File.ReadAllText(HttpContext.Current.Server.MapPath(Utilities.FILE_URL + html));
            if (string.IsNullOrEmpty(customer.LogoURL))
            {
                customer.LogoURL = "/img/logo.png";
            }

            customer.LogoURL = customer.LogoURL.Replace("/Services/auctionFiles//Services/auctionFiles/", "/Services/auctionFiles/");
            string htmlRows = string.Empty;
            try
            {
                htmlRows = String.Format(filestring,
                requirement.RequirementID.ToString(),
                requirement.Title.ToString(),
                requirement.PostedOn.ToString() + " (UTC + 0)",
                requirement.NoOfVendorsInvited, // 3
                requirement.NoOfVendorsParticipated,
                requirement.PriceBeforeNegotiation,
                requirement.priceAfterNegotiation,
                reqItemsWiseListHtml.ToString(),
                requirement_Savings.ToString(),
                requirement_itemWiseSavings.ToString(),
                requirement.PaymentTerms.ToString(), //10
                requirement.DeliveryTime.ToString(),
                requirement.DeliveryLocation.ToString(),
                "http://prm360.com" + customer.LogoURL,
                customer.CompanyName.ToString()
               );
            }
            catch
            {
            }

            return htmlRows;
        }

        private static string SaveReqAttachments(int reqID, int userID, string attachmentID, int revised, string fileType)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_U_ID", userID);
                sd.Add("P_ATTACHMENT_ID", attachmentID);
                sd.Add("P_IS_REVISED", revised);
                sd.Add("P_FILE_TYPE", fileType);
                IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
                DataSet ds = sqlHelper.SelectList("fwd_SaveQuotationAttachment", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response.ErrorMessage;
        }
    }
}