﻿using PRM.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class GRNDetails : ResponseAudit
    {
        [DataMember] [DataNames("GRN_ROW_ID")] public int GRN_ROW_ID { get; set; }
        [DataMember] [DataNames("COMP_ID")] public int COMP_ID { get; set; }
        [DataMember] [DataNames("ACUTAL_DELIVERY_DATE")] public DateTime? ACUTAL_DELIVERY_DATE { get; set; }
        [DataMember] [DataNames("COMMENTS")] public string COMMENTS { get; set; }
        [DataMember] [DataNames("FRIEGHT_VALUE")] public decimal FRIEGHT_VALUE { get; set; }
        [DataMember] [DataNames("GATE_ENTRY_DATE")] public DateTime? GATE_ENTRY_DATE { get; set; }
        [DataMember] [DataNames("GATE_ENTRY_NUMBER")] public string GATE_ENTRY_NUMBER { get; set; }
        [DataMember] [DataNames("GR_BATCH_NO")] public string GR_BATCH_NO { get; set; }
        [DataMember] [DataNames("GR_REFERENCE")] public string GR_REFERENCE { get; set; }
        [DataMember] [DataNames("GRN_VALUE")] public decimal GRN_VALUE { get; set; }
        [DataMember] [DataNames("PRODUCT_ID")] public int PRODUCT_ID { get; set; }
        [DataMember] [DataNames("GRN_STATUS")] public string GRN_STATUS { get; set; }
        [DataMember] [DataNames("VENDOR_BATCH_NO")] public string VENDOR_BATCH_NO { get; set; }
        [DataMember] [DataNames("VENDOR_ID")] public int VENDOR_ID { get; set; }
        [DataMember] [DataNames("REQ_ID")] public int REQ_ID { get; set; }
        [DataMember] [DataNames("REQ_NUMBER")] public string REQ_NUMBER { get; set; }


        [DataMember] [DataNames("GRN_NUMBER")] public string GRN_NUMBER { get; set; }
        [DataMember] [DataNames("GRN_LINE_ITEM")] public string GRN_LINE_ITEM { get; set; }
        [DataMember] [DataNames("PRODUCT_CODE")] public string PRODUCT_CODE { get; set; }
        [DataMember] [DataNames("PRODUCT_NAME")] public string PRODUCT_NAME { get; set; }
        [DataMember] [DataNames("PO_NUMBER")] public string PO_NUMBER { get; set; }
        [DataMember] [DataNames("PO_LINE_ITEM")] public string PO_LINE_ITEM { get; set; }
        [DataMember] [DataNames("ITEM_RECEIVED_DATE")] public DateTime? ITEM_RECEIVED_DATE { get; set; }
        [DataMember] [DataNames("GRN_DATE")] public DateTime? GRN_DATE { get; set; }
        [DataMember] [DataNames("RECEIVED_QUANTITY")] public decimal RECEIVED_QUANTITY { get; set; }
        [DataMember] [DataNames("REJECTED_QUANTITY")] public decimal REJECTED_QUANTITY { get; set; }
        [DataMember] [DataNames("GRN_RESERVED_QTY")] public decimal GRN_RESERVED_QTY { get; set; }
        [DataMember] [DataNames("SUPPLIER_NAME")] public string SUPPLIER_NAME { get; set; }
        [DataMember] [DataNames("SUPPLIER_CODE")] public string SUPPLIER_CODE { get; set; }
        [DataMember] [DataNames("UOM")] public string UOM { get; set; }
        [DataMember] [DataNames("TOTAL_COUNT")] public int TOTAL_COUNT { get; set; }
    }
}