﻿using PRM.Core.Common;
using PRM.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PRMServices.models
{
    [DataContract]
    public class PRMTemplate : Entity
    {
        [DataMember]
        [DataNames("TEMPLATE_ID")]
        public int TEMPLATE_ID { get; set; }

        [DataMember]
        [DataNames("COMP_ID")]
        public int COMP_ID { get; set; }

        [DataMember]
        [DataNames("TEMPLATE_NAME")]
        public string TEMPLATE_NAME { get; set; }

        [DataMember]
        [DataNames("TEMPLATE_DESC")]
        public string TEMPLATE_DESC { get; set; }

        [DataMember]
        [DataNames("IS_DEFAULT")]
        public bool IS_DEFAULT { get; set; }
    }
}