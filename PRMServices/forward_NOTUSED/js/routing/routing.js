﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider
            .state('view-fwd-req', {
                url: '/view-fwd-req/:Id',
                templateUrl: 'forward/views/viewFwdReq.html'
            })
            .state('save-fwd-req', {
                url: '/save-fwd-req/:Id',
                templateUrl: 'forward/views/saveFwdReq.html',
                params: {
                    reqObj: null
                }
            })
            .state('req-fwd-List', {
                url: '/req-fwd-List',
                templateUrl: 'forward/views/reqFwdList.html'
            })
            .state('req-Fwd-Vendor-List', {
                url: '/req-Fwd-Vendor-List',
                templateUrl: 'forward/views/reqFwdVendorList.html'
            })
            .state('fwdcomparatives', {
                url: '/fwdcomparatives/:reqID',
                templateUrl: 'forward/views/fwdcomparatives.html'
            })

        }]);