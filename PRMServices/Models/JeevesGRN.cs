﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class JeevesGRN
    {
        [DataMember] [DataNames("PO_NUMBER")] public string PO_NUMBER { get; set; }
        [DataMember] [DataNames("PO_LINE_ITEM")] public string PO_LINE_ITEM { get; set; }
        [DataMember] [DataNames("TYPE_OF_STOCK")] public string TYPE_OF_STOCK { get; set; }
        [DataMember] [DataNames("GRN_NO")] public string GRN_NO { get; set; }
        [DataMember] [DataNames("GRN_STATUS")] public string GRN_STATUS { get; set; }
        [DataMember] [DataNames("INVOICE_DOC")] public string INVOICE_DOC { get; set; }
        [DataMember] [DataNames("PO_LINE_ITEMS")] public List<JeevesPOItems> PO_LINE_ITEMS { get; set; }
        [DataMember] [DataNames("FOC_REQ_NUM")] public string FOC_REQ_NUM { get; set; }
        [DataMember] [DataNames("SHIPPED_LOCATION_CODE")] public string SHIPPED_LOCATION_CODE { get; set; }
        [DataMember] [DataNames("VENDOR_SITE_CODE")] public string VENDOR_SITE_CODE { get; set; }
        [DataMember] [DataNames("INVOICE_NO")] public string INVOICE_NO { get; set; }
        [DataMember] [DataNames("INVOICE_TYPE")] public string INVOICE_TYPE { get; set; }
        [DataMember] [DataNames("INVOICE_DATE")] public string INVOICE_DATE { get; set; }
        [DataMember] [DataNames("GRN_DATE")] public string GRN_DATE { get; set; }
        [DataMember] [DataNames("INVOICE_AMOUNT")] public decimal INVOICE_AMOUNT { get; set; }
    }

    public class JeevesPOItems
    {

        [DataMember] [DataNames("PO_LINE_ITEM_DESC")] public string PO_LINE_ITEM_DESC { get; set; }
        [DataMember] [DataNames("PO_QTY")] public decimal PO_QTY { get; set; }
        [DataMember] [DataNames("RECEIVED_QTY")] public decimal RECEIVED_QTY { get; set; }
        [DataMember] [DataNames("LOST_QTY")] public decimal LOST_QTY { get; set; }
        [DataMember] [DataNames("BAD_QTY")] public decimal BAD_QTY { get; set; }

    }


}