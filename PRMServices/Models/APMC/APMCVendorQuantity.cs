﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class APMCVendorQuantity : Entity
    {
        [DataMember(Name = "apmcVendorQuantityID")]
        public int APMCVendorQuantityID { get; set; }

        [DataMember(Name = "apmcNegotiationID")]
        public int APMCNegotiationID { get; set; }

        [DataMember(Name = "vendorID")]
        public int VendorID { get; set; }

        [DataMember(Name = "quantity")]
        public double Quantity{ get; set; }

        [DataMember(Name = "price")]
        public double Price { get; set; }

        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "units")]
        public string Units { get; internal set; }

        [DataMember(Name = "dateCreated")]
        public DateTime DateCreated { get; internal set; }
    }
}