﻿using PRM.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models.Catalog
{
    [DataContract]
    public class Product : EntityExt
    {
        [DataMember(Name = "prodId")]
        public int ProductId { get; set; }

        [DataMember(Name = "totalProducts")]
        public int TotalProducts { get; set; }

        [DataMember(Name = "totalUserProducts")]
        public int TotalUserProducts { get; set; }

        [DataMember(Name = "compId")]
        public int CompanyId { get; set; }

        //[DataMember(Name = "prodCode")]
        //public string ProductCode { get; set; }


        string prodCode = string.Empty;
        [DataMember(Name = "prodCode")]
        public string ProductCode
        {
            get
            {
                return prodCode;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    prodCode = value;
                }
            }
        }

        [DataMember(Name = "prodName")]
        public string ProductName { get; set; }

        [DataMember(Name = "prodHSNCode")]
        public string ProductHSNCode { get; set; }

        [DataMember(Name = "prodNo")]
        public string ProductNo { get; set; }

        [DataMember(Name = "prodQty")]
        public string ProdQty { get; set; }

        [DataMember(Name = "prodDesc")]
        public string ProductDesc { get; set; }
        
        [DataMember(Name = "isValid")]
        public int IsValid { get; set; }

        [DataMember(Name = "prodAlternateUnits")]
        public string ProdAlternativeUnits { get; set; }

        [DataMember(Name = "unitConversion")]
        public string UnitConversion { get; set; }

        [DataMember(Name = "shelfLife")]
        public string ShelfLife { get; set; }

        [DataMember(Name = "productVolume")]
        public string ProductVolume { get; set; }

        [DataMember(Name = "isCoreProductCategory")]
        public int IsCoreProductCategory { get; set; } = 1;

        //[DataMember(Name = "prodSelected")]
        //public int ProductSelected { get; set; }

        //[DataMember(Name = "prodChecked")]
        //public bool ProductChecked { get; set; }

        [DataMember(Name = "VendorPartNumber")]
        public string VendorPartNumber { get; set; }

        [DataMember(Name = "productGST")]
        public decimal ProductGST { get; set; }

        [DataMember(Name = "prefferedBrand")]
        public string PrefferedBrand { get; set; }

        [DataMember(Name = "alternateBrand")]
        public string AlternateBrand { get; set; }

        [DataMember(Name = "listVendorDetails")]
        public List<ProductVendorDetails> ListVendorDetails { get; set; }

        [DataMember(Name = "multipleAttachments")]
        public List<FileUpload> MultipleAttachments { get; set; }


        string itemAttachments = string.Empty;
        [DataMember(Name = "itemAttachments")]
        public string ItemAttachments
        {
            get
            {
                return this.itemAttachments;
            }
            set
            {
                this.itemAttachments = value;
            }
        }

        [DataMember(Name = "contractManagement")]
        public List<ContractManagementDetails> ContractManagement { get; set; }

        [DataMember(Name = "CONTRACT_VENDOR")]  public int CONTRACT_VENDOR { get; set; }

        [DataMember(Name = "casNumber")]
        public string CasNumber { get; set; }

        [DataMember(Name = "mfcdCode")]
        public string MfcdCode { get; set; }

        [DataMember(Name = "categoryId")]
        public string CategoryId { get; set; }

        string productImage = string.Empty;
        [DataMember(Name = "productImage")]
        public string ProductImage
        {
            get
            {
                return productImage;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    productImage = value;
                }
            }
        }
        [DataMember(Name = "IsServiceProduct")] public string IsServiceProduct { get; set; }
        [DataMember(Name = "EPN")] public string EPN { get; set; }
        [DataMember(Name = "SERVICE_TYPE")] public string SERVICE_TYPE { get; set; }
        [DataMember(Name = "ITEM_REG_STATUS")] public string ITEM_REG_STATUS { get; set; }
        [DataMember(Name = "MODEL_ID")] public string MODEL_ID { get; set; }
        [DataMember(Name = "MODEL_NAME")] public string MODEL_NAME { get; set; }
        [DataMember(Name = "BRAND_PART_CODE")] public string BRAND_PART_CODE { get; set; }
        [DataMember(Name = "BRAND_PRODUCT_CATEGORY_CODE")] public string BRAND_PRODUCT_CATEGORY_CODE { get; set; }
        [DataMember(Name = "JSN")] public string JSN { get; set; }
        [DataMember(Name = "FSN")] public string FSN { get; set; }
        [DataMember(Name = "IMEI_NO")] public string IMEI_NO { get; set; }
        [DataMember(Name = "ESN_NO")] public string ESN_NO { get; set; }
        [DataMember(Name = "ITEM_SERIALLZED_STATUS")] public string ITEM_SERIALLZED_STATUS { get; set; }
        [DataMember(Name = "CUSTOMER_PRICE")] public decimal CUSTOMER_PRICE { get; set; }
        [DataMember(Name = "SF_PRICE")] public decimal SF_PRICE { get; set; }
        [DataMember(Name = "LANDING_PRICE")] public decimal LANDING_PRICE { get; set; }
        [DataMember(Name = "STOCK_TRANSPORTER_PRICE")] public decimal STOCK_TRANSPORTER_PRICE { get; set; }
        [DataMember(Name = "HANDLING_CHARGES")] public decimal HANDLING_CHARGES { get; set; }
        [DataMember(Name = "WARRANTY")] public string WARRANTY { get; set; }
        [DataMember(Name = "WARRANTY_COVERAGE")] public string WARRANTY_COVERAGE { get; set; }
        [DataMember(Name = "IS_CONSUMABLE")] public string IS_CONSUMABLE { get; set; }
        [DataMember(Name = "IS_CHARGABLE")] public string IS_CHARGABLE { get; set; }
        [DataMember(Name = "IS_RETURNABLE")] public string IS_RETURNABLE { get; set; }
        [DataMember(Name = "IS_HIGHVALUE")] public string IS_HIGHVALUE { get; set; }
        [DataMember(Name = "IS_SERIALIZED")] public string IS_SERIALIZED { get; set; }
        [DataMember(Name = "IS_REPLACEMENT")] public string IS_REPLACEMENT { get; set; }
        [DataMember(Name = "IS_ADVANCE_REPLACEMENT")] public string IS_ADVANCE_REPLACEMENT { get; set; }
        [DataMember(Name = "REPAIR_LEVEL")] public string REPAIR_LEVEL { get; set; }
        [DataMember(Name = "PRIORITY_LEVEL")] public string PRIORITY_LEVEL { get; set; }
        [DataMember(Name = "DISCOUNT")] public decimal DISCOUNT { get; set; }
        [DataMember(Name = "CONDITION")] public string CONDITION { get; set; }
        [DataMember(Name = "ENTITY")] public string ENTITY { get; set; }
        [DataMember(Name = "SUB_ENTITY")] public string SUB_ENTITY { get; set; }

        //SubEntity

        [DataMember(Name = "SE_ID")] public int SE_ID { get; set; }
        [DataMember(Name = "SUB_ENTITY_NAME")] public string SUB_ENTITY_NAME { get; set; }
        [DataMember(Name = "VENDORS_LIST")] public string VENDORS_LIST { get; set; }
        [DataMember(Name = "COMP_ID")] public  int COMP_ID { get; set; }
        [DataMember(Name = "VERTICAL")] public string VERTICAL { get; set; }

    }

    [DataContract]
    public class ContractManagementDetails
    {
        [DataMember(Name = "PC_ID")]
        public int PC_ID { get; set; }

        [DataMember(Name = "number")]
        public string Number { get; set; }

        [DataMember(Name = "value")]
        public decimal Value { get; set; }

        [DataMember(Name = "price")]
        public double Price { get; set; }

        [DataMember(Name = "ProductId")]
        public int ProductId { get; set; }

        [DataMember(Name = "ProductName")]
        public string ProductName { get; set; }

        [DataMember(Name = "ProductCode")]
        public string ProductCode { get; set; }

        [DataMember(Name = "U_ID")]
        public int U_ID { get; set; }

        [DataMember(Name = "document")]
        public string Document { get; set; }

        [DataMember(Name = "quantity")]
        public decimal Quantity { get; set; }

        [DataMember(Name = "availedQuantity")]
        public decimal AvailedQuantity { get; set; }

        [DataMember(Name = "vendorId")]
        public int VendorId { get; set; }

        [DataMember(Name = "startTime")]
        public DateTime? StartTime { get; set; }

        [DataMember(Name = "endTime")]
        public DateTime? EndTime { get; set; }

        [DataMember(Name = "isValid")]
        public int IsValid { get; set; }

        [DataMember(Name = "companyName")]
        public string CompanyName { get; set; }

        [DataMember(Name = "contractNumber")]
        public string ContractNumber { get; set; }

        [DataMember(Name = "contractValue")]
        public decimal ContractValue { get; set; }

        [DataMember(Name = "categories")]
        public string Categories { get; set; }

        [DataMember(Name = "totalNoOfContracts")]
        public int TotalNoOfContracts { get; set; }

        [DataMember(Name = "totalContractualValue")]
        public decimal TotalContractualValue { get; set; }

        [DataMember(Name = "contractsUtilization")]
        public decimal ContractsUtilization { get; set; }

        [DataMember(Name = "phoneNumber")]
        public int PhoneNumber { get; set; }

        [DataMember(Name = "supplierName")]
        public string SupplierName { get; set; }

        [DataMember(Name = "poNumber")]
        public string PONumber { get; set; }

        [DataMember(Name = "TARGET_VALUE")]
        public decimal TARGET_VALUE { get; set; }

        [DataMember(Name = "CURRENCY")]
        public string CURRENCY { get; set; }

        [DataMember(Name = "PAYMENT_TERMS")]
        public string PAYMENT_TERMS { get; set; }

        [DataMember(Name = "INCO_TERMS")]
        public string INCO_TERMS { get; set; }

        [DataMember(Name = "MANUFACTURER")]
        public string MANUFACTURER { get; set; }

        [DataMember(Name = "DELIVERY_ADDRESS")]
        public string DELIVERY_ADDRESS { get; set; }

        [DataMember(Name = "AGREEMENT_TYPE")]
        public string AGREEMENT_TYPE { get; set; }

        [DataMember(Name = "TAX_CODE")]
        public string TAX_CODE { get; set; }

        [DataMember(Name = "EXCH_RATE")]
        public decimal EXCH_RATE { get; set; }

        [DataMember(Name = "COMP_ID")]
        public int COMP_ID { get; set; }

        [DataMember(Name = "CREATED_BY")]
        public int CREATED_BY { get; set; }

        [DataMember(Name = "CREATED_DATE")]
        public DateTime? CREATED_DATE { get; set; }

        [DataMember(Name = "MODIFIED_BY")]
        public int MODIFIED_BY { get; set; }

        [DataMember(Name = "MODIFIED_DATE")]
        public DateTime? MODIFIED_DATE { get; set; }

        [DataMember(Name = "ADDRESS")]
        public string ADDRESS { get; set; }

        [DataMember(Name = "priceType")]
        public string PriceType { get; set; }

        [DataMember(Name = "selectedVendorCode")]
        public string SelectedVendorCode { get; set; }

        [DataMember(Name = "DELIVERY_TERMS")]
        public string DELIVERY_TERMS { get; set; }

        [DataMember(Name = "PLANT_CODE")]
        public string PLANT_CODE { get; set; }

        [DataMember(Name = "GENERAL_TERMS")]
        public string GENERAL_TERMS { get; set; }

        [DataMember(Name = "DELIVERY_FROM")]
        public string DELIVERY_FROM { get; set; }

        [DataMember(Name = "DELIVERY_AT")]
        public string DELIVERY_AT { get; set; }

        [DataMember(Name = "LEADTIME")]
        public string LEADTIME { get; set; }

        [DataMember(Name = "WARRANTY")]
        public string WARRANTY { get; set; }

        [DataMember(Name = "SPECIAL_INSTRUCTIONS")]
        public string SPECIAL_INSTRUCTIONS { get; set; }

        [DataMember(Name = "NET_PRICE")]
        public decimal NET_PRICE { get; set; }


        [DataMember(Name = "DISCOUNT")]
        public decimal DISCOUNT { get; set; }

        [DataMember(Name = "GST")]
        public decimal GST { get; set; }

        [DataMember(Name = "CUSTOM_DUTY")]
        public decimal CUSTOM_DUTY { get; set; }

        [DataMember(Name = "DISCOUNT_ELIGIBILITY")]
        public string DISCOUNT_ELIGIBILITY { get; set; }

        [DataMember(Name = "RECONCILIATION_TIMELINES")]
        public string RECONCILIATION_TIMELINES { get; set; }

        [DataMember(Name = "EXCHANGE_RATE")]
        public decimal EXCHANGE_RATE { get; set; }

        [DataMember(Name = "PO_CURRENCY")]
        public string PO_CURRENCY { get; set; }

        [DataMember(Name = "USER")]
        public int USER { get; set; }


        [DataMember(Name = "PRODUCT_CODE")]
        public string PRODUCT_CODE { get; set; }


        [DataMember(Name = "VENDOR_CODE")]
        public string VENDOR_CODE { get; set; }

        [DataMember(Name = "VendorPartNumber")]
        public string VendorPartNumber { get; set; }

    }

    [DataContract]
    public class ImportEntity : Entity
    {
        [DataMember(Name = "entityName")]
        public string EntityName
        {
            get;
            set;
        }

        [DataMember(Name = "userid")]
        public int UserID
        {
            get;
            set;
        }

        [DataMember(Name = "attachment")]
        public byte[] Attachment
        {
            get;
            set;
        }

        [DataMember(Name = "attachmentFileName")]
        public string AttachmentFileName
        {
            get;
            set;
        }
    }

    [DataContract]
    public class ProductFilters
    {
        [DataMember(Name = "filterType")]
        public string FilterType
        {
            get;
            set;
        }

        [DataMember(Name = "filterField")]
        public string FilterField
        {
            get;
            set;
        }

        [DataMember(Name = "filterCond")]
        public string FilterCond
        {
            get;
            set;
        }

        [DataMember(Name = "filterValue")]
        public string FilterValue
        {
            get;
            set;
        }

        [DataMember(Name = "filterFieldDataType")]
        public string filterFieldDataType
        {
            get;
            set;
        }
    }

    [DataContract]
    public class CompanyConfiguration : Entity
    {

        [DataMember(Name = "compConfigID")]
        public int CompConfigID { get; set; }

        [DataMember(Name = "compID")]
        public int CompID { get; set; }

        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "configKey")]
        public string ConfigKey { get; set; }

        [DataMember(Name = "configValue")]
        public string ConfigValue { get; set; }

        [DataMember(Name = "isValid")]
        public bool IsValid { get; set; }

        [DataMember(Name = "configText")]
        public string ConfigText { get; set; }

        

    }
}