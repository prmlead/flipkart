﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class OracleVendor
    {
        [DataMember] [DataNames("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
        [DataMember] [DataNames("VENDOR_CODE")] public string VENDOR_CODE { get; set; }
        [DataMember] [DataNames("GST_NUMBER")] public string GST_NUMBER { get; set; }
        [DataMember] [DataNames("PAN_NUMBER")] public string PAN_NUMBER { get; set; }
        [DataMember] [DataNames("EMAIL")] public string EMAIL { get; set; }
        [DataMember] [DataNames("PHONE_NUMBER")] public string PHONE_NUMBER { get; set; }
        [DataMember] [DataNames("CURRENCY")] public string CURRENCY { get; set; }
        [DataMember] [DataNames("CONTACT_NAME")] public string CONTACT_NAME { get; set; }
        [DataMember] [DataNames("CONTACT_DESIGNATION")] public string CONTACT_DESIGNATION { get; set; }

        [DataMember] [DataNames("BILLING_ADDRESS")] public string BILLING_ADDRESS { get; set; }
        [DataMember] [DataNames("BILLING_COUNTRY")] public string COUNTRY { get; set; }
        [DataMember] [DataNames("BILLING_CITY")] public string BILLING_CITY { get; set; }
        [DataMember] [DataNames("BILLING_STATE")] public string BILLING_STATE { get; set; }
        [DataMember] [DataNames("BILLING_REGION")] public string BILLING_REGION { get; set; }
        [DataMember] [DataNames("BILLING_PINCODE")] public string BILLING_PINCODE { get; set; }

        [DataMember] [DataNames("SHIPPING_ADDRESS")] public string SHIPPING_ADDRESS { get; set; }
        [DataMember] [DataNames("SHIPPING_COUNTRY")] public string SHIPPING_COUNTRY { get; set; }
        [DataMember] [DataNames("SHIPPING_REGION")] public string SHIPPING_REGION { get; set; }
        [DataMember] [DataNames("SHIPPING_STATE")] public string SHIPPING_STATE { get; set; }
        [DataMember] [DataNames("SHIPPING_CITY")] public string SHIPPING_CITY { get; set; }
        [DataMember] [DataNames("SHIPPING_PINCODE")] public string SHIPPING_PINCODE { get; set; }
        [DataMember] [DataNames("ENTITY_NAME")] public string ENTITY_NAME { get; set; }
        [DataMember] [DataNames("VENDOR_TYPE")] public string VENDOR_TYPE { get; set; }
        [DataMember] [DataNames("VENDOR_CATEGORY")] public string VENDOR_CATEGORY { get; set; }
        [DataMember] [DataNames("VENDOR_SITE_CODE")] public string VENDOR_SITE_CODE { get; set; }
        [DataMember] [DataNames("PAYMENT_METHOD")] public string PAYMENT_METHOD { get; set; }
        [DataMember] [DataNames("EMAIL_ALT1")] public string EMAIL_ALT1 { get; set; }
        [DataMember] [DataNames("VENDOR_STATUS")] public int VENDOR_STATUS { get; set; }
        [DataMember] [DataNames("VENDOR_SITE_STATUS")] public int VENDOR_SITE_STATUS { get; set; }
        [DataMember] [DataNames("VENDOR_INACTIVE_DATE")] public DateTime VENDOR_INACTIVE_DATE { get; set; }


    }



}