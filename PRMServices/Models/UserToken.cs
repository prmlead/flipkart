﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;


namespace PRMServices.Models
{
    [DataContract]
    public class UserToken : Entity
    {
        int objectId = 0;
        [DataMember(Name = "objectID")]
        public int ObjectID
        {
            get
            {
                return this.objectId;
            }
            set
            {
                this.objectId = value;
            }
        }

        string msg = string.Empty;
        [DataMember(Name = "message")]
        public string Message
        {
            get
            {
                return this.msg;
            }
            set
            {
                this.msg = value;
            }
        }


        string status = string.Empty;
        [DataMember(Name = "status")]
        public string Status
        {
            get
            {
                return this.status;
            }
            set
            {
                this.status = value;
            }
        }




        string remarksType = string.Empty;
        [DataMember(Name = "remarksType")]
        public string RemarksType
        {
            get
            {
                return this.remarksType;
            }
            set
            {
                this.remarksType = value;
            }
        }


        long tLeft = 0;
        [DataMember(Name = "timeLeft")]
        public long TimeLeft
        {
            get
            {
                return this.tLeft;
            }
            set
            {
                this.tLeft = value;
            }
        }


        string jwtToken = string.Empty;
        [DataMember(Name = "jwtToken")]
        public string JWTToken
        {
            get
            {
                return this.jwtToken;
            }
            set
            {
                this.jwtToken = value;
            }
        }

        int expiresIn = 0;
        [DataMember(Name = "expiresIn")]
        public int ExpiresIn
        {
            get
            {
                return this.expiresIn;
            }
            set
            {
                this.expiresIn = value;
            }
        }



    }
}