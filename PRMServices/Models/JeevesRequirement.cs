﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class JeevesRequirement
    {
        [DataMember] [DataNames("ENTITY")] public string ENTITY { get; set; }
        [DataMember] [DataNames("TITLE")] public string TITLE { get; set; }
        [DataMember] [DataNames("REQUIRED_QUANTITY")] public string REQUIRED_QUANTITY { get; set; }
        [DataMember] [DataNames("UOM")] public string UOM { get; set; }
        [DataMember] [DataNames("CASE_ID")] public string CASE_ID { get; set; }
        [DataMember] [DataNames("TYPE_OF_STOCK")] public string TYPE_OF_STOCK { get; set; }
        [DataMember] [DataNames("REQUIREMENT_LINE_ITEMS")] public List<JeevesRequirementItems> REQUIREMENT_LINE_ITEMS { get; set; }
        [DataMember] [DataNames("PRODUCT_CODE")] public string PRODUCT_CODE { get; set; }
        [DataMember] [DataNames("MODEL_NAME")] public string MODEL_NAME { get; set; }
        [DataMember] [DataNames("BRAND_PART_CODE")] public string BRAND_PART_CODE { get; set; }
        [DataMember] [DataNames("BRAND")] public string BRAND { get; set; }
        [DataMember] [DataNames("BRAND_PRODUCT_CATEGORY_CODE")] public string BRAND_PRODUCT_CATEGORY_CODE { get; set; }
        [DataMember] [DataNames("DELIVERY_LOCATION")] public string DELIVERY_LOCATION { get; set; }
        [DataMember] [DataNames("PAYMENT_TERMS")] public string PAYMENT_TERMS { get; set; }
        [DataMember] [DataNames("CONTACT_DETAILS")] public string CONTACT_DETAILS { get; set; }
        [DataMember] [DataNames("URGENCY")] public string URGENCY { get; set; }
        [DataMember] [DataNames("CURRENCY")] public string CURRENCY { get; set; }
        [DataMember] [DataNames("FREEZE_TIME")] public DateTime FREEZE_TIME { get; set; }
        [DataMember] [DataNames("EXP_DELIVERY_DATE")] public DateTime EXP_DELIVERY_DATE { get; set; }
        [DataMember] [DataNames("NEGOTIATION_TIME")] public DateTime NEGOTIATION_TIME { get; set; }
    }

    public class JeevesRequirementItems 
    {

        [DataMember] [DataNames("PRODUCT_CODE")] public string PRODUCT_CODE { get; set; }
        [DataMember] [DataNames("MODEL_NAME")] public string MODEL_NAME { get; set; }
        [DataMember] [DataNames("BRAND_PART_CODE")] public string BRAND_PART_CODE { get; set; }
        [DataMember] [DataNames("BRAND")] public string BRAND { get; set; }
        [DataMember] [DataNames("BRAND_PRODUCT_CATEGORY_CODE")] public string BRAND_PRODUCT_CATEGORY_CODE { get; set; }
        [DataMember] [DataNames("DELIVERY_LOCATION")] public string DELIVERY_LOCATION { get; set; }
        [DataMember] [DataNames("PAYMENT_TERMS")] public string PAYMENT_TERMS { get; set; }
        [DataMember] [DataNames("CONTACT_DETAILS")] public string CONTACT_DETAILS { get; set; }
        [DataMember] [DataNames("URGENCY")] public string URGENCY { get; set; }
        [DataMember] [DataNames("CURRENCY")] public string CURRENCY { get; set; }
        [DataMember] [DataNames("FREEZE_TIME")] public DateTime FREEZE_TIME { get; set; }
        [DataMember] [DataNames("EXP_DELIVERY_DATE")] public DateTime EXP_DELIVERY_DATE { get; set; }
        [DataMember] [DataNames("NEGOTIATION_TIME")] public DateTime NEGOTIATION_TIME { get; set; }
        [DataMember] [DataNames("ITEM_LEVEL_ATTACHMENTS")] public string ITEM_LEVEL_ATTACHMENTS { get; set; }

    }


}