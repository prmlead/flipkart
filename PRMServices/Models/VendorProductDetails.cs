﻿using PRM.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class VendorProductDetails 
    {
        [DataMember] [DataNames("VENDOR_SITE_CODE")] public string VENDOR_SITE_CODE { get; set; }
        [DataMember] [DataNames("VENDOR_CODE")] public string VENDOR_CODE { get; set; }
        [DataMember] [DataNames("PAYMENT_TERMS")] public string PAYMENT_TERMS { get; set; }
        [DataMember] [DataNames("CURRENCY_FACTOR")] public decimal CURRENCY_FACTOR { get; set; }
        [DataMember] [DataNames("PRODUCT_CODE")] public string PRODUCT_CODE { get; set; }
        [DataMember] [DataNames("PRODUCT_NAME")] public string PRODUCT_NAME { get; set; }
        [DataMember] [DataNames("ENTITY")] public string ENTITY { get; set; }
        [DataMember] [DataNames("TYPE_OF_STOCK")] public string TYPE_OF_STOCK { get; set; }
        [DataMember] [DataNames("BRAND")] public string BRAND { get; set; }
        [DataMember] [DataNames("BRAND_PART_CODE")] public string BRAND_PART_CODE { get; set; }
        [DataMember] [DataNames("CATEGORY")] public string CATEGORY { get; set; }
        [DataMember] [DataNames("MODEL_NAME")] public string MODEL_NAME { get; set; }
        [DataMember] [DataNames("UOM")] public string UOM { get; set; }
        [DataMember] [DataNames("CASE_ID")] public string CASE_ID { get; set; }
        [DataMember] [DataNames("HSN_CODE")] public string HSN_CODE { get; set; }
        [DataMember] [DataNames("DELIVERY_LOCATION")] public string DELIVERY_LOCATION { get; set; }
        [DataMember] [DataNames("EXP_DELIVERY_DATE")] public DateTime? EXP_DELIVERY_DATE { get; set; }
        [DataMember] [DataNames("AVAILABLE_QTY")] public decimal AVAILABLE_QTY { get; set; }
        [DataMember] [DataNames("REV_UNIT_PRICE")] public decimal REV_UNIT_PRICE { get; set; }
        [DataMember] [DataNames("C_GST")] public decimal C_GST { get; set; }
        [DataMember] [DataNames("S_GST")] public decimal S_GST { get; set; }
        [DataMember] [DataNames("I_GST")] public decimal I_GST { get; set; }
        [DataMember] [DataNames("ASSIGN_QTY")] public decimal ASSIGN_QTY { get; set; }
        [DataMember] [DataNames("VENDOR_ID")] public int VENDOR_ID { get; set; }
        [DataMember] [DataNames("QCS_ID")] public int QCS_ID { get; set; }
        [DataMember] [DataNames("PRODUCT_ID")] public int PRODUCT_ID { get; set; }
        [DataMember] [DataNames("ITEM_REQ_ID")] public int ITEM_REQ_ID { get; set; }
        [DataMember] [DataNames("CURRENCY")] public string CURRENCY { get; set; }



    }
}