﻿using PRMServices.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRMServices.Models.RealTimePrice
{
	public class RealTimePriceUploadModel
	{
        public int TypeId { get; set; }

        public FileUploadModel File { get; set; }
    }
}