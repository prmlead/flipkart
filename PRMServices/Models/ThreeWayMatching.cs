﻿using PRM.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class ThreeWayMatching : ResponseAudit
    {
       
        [DataMember] [DataNames("PO_NUMBER")] public string PO_NUMBER { get; set; }
        [DataMember] [DataNames("PO_LINE_ITEM")] public string PO_LINE_ITEM { get; set; }
        [DataMember] [DataNames("PRODUCT_NAME")] public string PRODUCT_NAME { get; set; }
        [DataMember] [DataNames("PO_RECEIVED_QTY")] public decimal PO_RECEIVED_QTY { get; set; }
        [DataMember] [DataNames("ORDER_QTY")] public decimal ORDER_QTY { get; set; }
        [DataMember] [DataNames("INVOICE_NUMBER")] public string INVOICE_NUMBER { get; set; }
        [DataMember] [DataNames("INVOICE_QTY")] public decimal INVOICE_QTY { get; set; }
        [DataMember] [DataNames("GRN_NUMBER")] public string GRN_NUMBER { get; set; }
        [DataMember] [DataNames("GRN_RECEIVED_QTY")] public decimal GRN_RECEIVED_QTY { get; set; }
        [DataMember] [DataNames("IS_ERROR")] public decimal IS_ERROR { get; set; }
        [DataMember] [DataNames("MESSAGE")] public string MESSAGE { get; set; }
        [DataMember] [DataNames("INVOICE_ID")] public int INVOICE_ID { get; set; }
        [DataMember] [DataNames("TOTAL_COUNT")] public int TOTAL_COUNT { get; set; }

    }
}