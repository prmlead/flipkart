﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models


//ClientID, CL_CompName, CL_Domain, DateCreated, DateModified, CreatedBy, ModifiedBy, IsValid, ClientType, ClientStatus, ClientDB
//OpenRequirements,TodayNegotiation,PendingQuatation,LiveNegotiation
{
    [DataContract]
    public class OpsClientReqInfo : Entity 
    {
        

        [DataMember(Name = "openRequirements")]
        public int OpenRequirements { get; set; }

        [DataMember(Name = "todayNegotiation")]
        public int TodayNegotiation { get; set; }

        [DataMember(Name = "pendingQuatation")]
        public int PendingQuatation { get; set; }

        [DataMember(Name = "liveNegotiation")]
        public int LiveNegotiation { get; set; }

        [DataMember(Name = "closedRequirements")]
        public int ClosedRequirements { get; set; }

        [DataMember(Name = "clientSavingsAchive")]
        public Double ClientSavingsAchive { get; set; }

        [DataMember(Name = "req_POSTED_ON")]
        public DateTime REQ_POSTED_ON
        {
            get
            {
                return DateTime.Now;
            }
            set { }
        }

        //[DataMember(Name = "modifiedBy")]
        //public int ModifiedBy { get; set; }

        //[DataMember(Name = "isValid")]
        //public int IsValid { get; set; }

        //[DataMember(Name = "clientType")]
        //public string ClientType { get; set; }

        //[DataMember(Name = "clientStatus")]
        //public string ClientStatus { get; set; }

        //[DataMember(Name = "clientDB")]
        //public string ClientDB { get; set; }

         [DataMember(Name = "vednorNeedTraining")]
        public int VednorNeedTraining { get; set; }



    }
}