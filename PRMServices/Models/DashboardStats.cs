﻿using PRM.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class DashboardStats : Entity
    {
        [DataMember(Name = "totalAuctions")]
        public int TotalAuctions { get; set; }

        [DataMember(Name = "totalAuctionsPending")]
        public int TotalAuctionsPending { get; set; }

        [DataMember(Name = "totalAuctionsCompleted")]
        public int TotalAuctionsCompleted { get; set; }

        [DataMember(Name = "totalTurnaroundTime")]
        public double TotalTurnaroundTime { get; set; }

        [DataMember(Name="totalTurnover")]
        public double TotalTurnover { get; set; }

        [DataMember(Name = "totalSaved")]
        public double TotalSaved { get; set; }

        [DataMember(Name = "totalOrders")]
        public int TotalOrders { get; set; }

        [DataMember(Name = "totalPos")]
        public int TotalPos { get; set; }

        [DataMember(Name = "requirementsList")]
        public List<Requirement> RequirementsList { get; set; }

        [DataMember(Name = "stats")]
        public List<CArrayKeyValue> CArrayKeyValue { get; set; }

        [DataMember(Name = "asnList")]
        public List<DispatchTrack> ASNList { get; set; }

        [DataMember(Name = "noOfIndentsReceived")]
        public int NoOfIndentsReceived { get; set; }

        [DataMember(Name = "noOfPendingRFQ")]
        public int NoOfPendingRFQ { get; set; }

        [DataMember(Name = "noOfClosedRFQ")]
        public int NoOfClosedRFQ { get; set; }


        [DataMember(Name = "TOTAL_RFQS")]
        public int TOTAL_RFQS { get; set; }

        [DataMember(Name = "TOTAL_OPEN_RFQS")]
        public int TOTAL_OPEN_RFQS { get; set; }

        [DataMember(Name = "TOTAL_RFPS")]
        public int TOTAL_RFPS { get; set; }

        [DataMember(Name = "TOTAL_OPEN_RFPS")]
        public int TOTAL_OPEN_RFPS { get; set; }
        [DataMember(Name = "TOTAL_POS")]
        public int TOTAL_POS { get; set; }
        [DataMember(Name = "TOTAL_PENDING_POS")]
        public int TOTAL_PENDING_POS { get; set; }

        [DataMember(Name = "TOTAL_CONTRACTS")]
        public int TOTAL_CONTRACTS { get; set; }
        [DataMember(Name = "TOTAL_PENDING_CONTRACTS")]
        public int TOTAL_PENDING_CONTRACTS { get; set; }
        [DataMember] [DataNames("REQ_URGENCY_LIST")] public List<ReqUrgencyList> REQ_URGENCY_LIST { get; set; }
        [DataMember] [DataNames("REQ_RANGE_LIST")] public List<ReqRangeList> REQ_RANGE_LIST { get; set; }
        [DataMember] [DataNames("MONTHLY_AVG_SAVINGS")] public List<MonthlyAvgSavings> MONTHLY_AVG_SAVINGS { get; set; }
        [DataMember] [DataNames("MONTHLY_AVG_UTILIZATION")] public List<MonthlyAvgUtilizaion> MONTHLY_AVG_UTILIZATION { get; set; }
        [DataMember] [DataNames("TRANSACTIONAL_TREND")] public List<TransactionalTrend> TRANSACTIONAL_TREND { get; set; }
        [DataMember] [DataNames("TOP_PERFORMER")] public List<TopPerformer> TOP_PERFORMER { get; set; }

    }


    public class ReqUrgencyList
    {
        [DataMember] [DataNames("REQ_URGENCY")] public string REQ_URGENCY { get; set; }
        [DataMember] [DataNames("COUNT")] public int COUNT { get; set; }
    }
    public class ReqRangeList
    {
        [DataMember] [DataNames("RangeVal")] public string RangeVal { get; set; }
        [DataMember] [DataNames("COUNT")] public int COUNT { get; set; }
    }
    public class MonthlyAvgSavings
    {
        [DataMember] [DataNames("MONTH")] public string MONTH { get; set; }
        [DataMember] [DataNames("YEAR")] public string YEAR { get; set; }
        [DataMember] [DataNames("QUOTATION_VALUE")] public decimal QUOTATION_VALUE { get; set; }
        [DataMember] [DataNames("SAVINGS")] public decimal SAVINGS { get; set; }
    }

    public class MonthlyAvgUtilizaion
    {
        [DataMember] [DataNames("MONTH_NAME")] public string MONTH_NAME { get; set; }
        [DataMember] [DataNames("MONTH_NUM")] public int MONTH_NUM { get; set; }
        [DataMember] [DataNames("REQ_COUNT")] public int REQ_COUNT { get; set; }
        [DataMember] [DataNames("REQ_STATUS")] public string REQ_STATUS { get; set; }
        [DataMember] [DataNames("MONTH_YEAR")] public string MONTH_YEAR { get; set; }
    }

    public class TransactionalTrend
    {
        [DataMember] [DataNames("MONTH_NAME")] public string MONTH_NAME { get; set; }
        [DataMember] [DataNames("MONTH_NUM")] public int MONTH_NUM { get; set; }
        [DataMember] [DataNames("MY_REQ_COUNT")] public int MY_REQ_COUNT { get; set; }
        [DataMember] [DataNames("TOTAL_REQ_COUNT")] public string TOTAL_REQ_COUNT { get; set; }
    }

    public class TopPerformer
    {
        [DataMember] [DataNames("U_ID")] public int U_ID { get; set; }
        [DataMember] [DataNames("NAME")] public string NAME { get; set; }
        [DataMember] [DataNames("REQ_COUNT")] public int REQ_COUNT { get; set; }
        [DataMember] [DataNames("TOTAL_COUNT")] public int TOTAL_COUNT { get; set; }

    }
}