﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class User : Entity
    {
        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "altUserID")]
        public int AltUserID { get; set; }

        string firstName = string.Empty;
        [DataMember(Name = "firstName")]
        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    firstName = value;
                }
            }
        }

        string lastName = string.Empty;
        [DataMember(Name = "lastName")]
        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    lastName = value;
                }
            }
        }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        string phoneNum = string.Empty;
        [DataMember(Name = "phoneNum")]
        public string PhoneNum
        {
            get
            {
                return phoneNum;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    phoneNum = value;
                }
            }
        }

        //[DataMember(Name = "altEmail")]
        //public string AltEmail { get; set; }

        //[DataMember(Name = "altPhoneNum")]
        //public string AltPhoneNum { get; set; }


        string altEmail = string.Empty;
        [DataMember(Name = "altEmail")]
        public string AltEmail
        {
            get
            {
                if (!string.IsNullOrEmpty(altEmail))
                { return altEmail; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { altEmail = value; }
            }
        }

        string altPhoneNum = string.Empty;
        [DataMember(Name = "altPhoneNum")]
        public string AltPhoneNum
        {
            get
            {
                if (!string.IsNullOrEmpty(altPhoneNum))
                { return altPhoneNum; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { altPhoneNum = value; }
            }
        }


        [DataMember(Name = "userInfo")]
        public UserInfo UserInfo { get; set; }

        [DataMember(Name = "additionalInfo")]
        public string AdditionalInfo { get; set; }

        [DataMember(Name = "vendorCode")]
        public string VendorCode { get; set; }

        [DataMember(Name = "regStatus")]
        public int Status { get; set; }


        [DataMember(Name = "regReason")]
        public string Reason { get; set; }

        [DataMember(Name = "regComment")]
        public string Comment { get; set; }

        [DataMember(Name = "regScore")]
        public decimal Score { get; set; }


        string approvedBy = string.Empty;
        [DataMember(Name = "approvedBy")]
        public string ApprovedBy
        {
            get
            {
                return approvedBy;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    approvedBy = value;
                }
            }
        }

        [DataMember(Name = "isOtpVerified")]
        public int IsOtpVerified { get; set; }

        [DataMember(Name = "isEmailVerified")]
        public int IsEmailVerified { get; set; }

        [DataMember(Name = "company")]
        public string Company { get; set; }

    }
}