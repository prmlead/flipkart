﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class BudgetCodeEntity : Entity
    {

        [DataMember(Name = "userID")]
        public int UserID { get; set; }



        [DataMember(Name = "deptID")]
        public int DeptID { get; set; }

        [DataMember(Name = "deptTypeID")]
        public int DeptTypeID { get; set; }

        string deptCode = string.Empty;
        [DataMember(Name = "deptCode")]
        public string DeptCode
        {
            get
            {
                if (!string.IsNullOrEmpty(deptCode))
                { return deptCode; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { deptCode = value; }
            }
        }


        [DataMember(Name = "workCatID")]
        public int WorkCatID { get; set; }

        string workCatCode = string.Empty;
        [DataMember(Name = "workCatCode")]
        public string WorkCatCode
        {
            get
            {
                if (!string.IsNullOrEmpty(workCatCode))
                { return workCatCode; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { workCatCode = value; }
            }
        }

        string workCatDesc = string.Empty;
        [DataMember(Name = "workCatDesc")]
        public string WorkCatDesc
        {
            get
            {
                if (!string.IsNullOrEmpty(workCatDesc))
                { return workCatDesc; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { workCatDesc = value; }
            }
        }


        [DataMember(Name = "budgetID")]
        public int BudgetID { get; set; }

        string budgetCode = string.Empty;
        [DataMember(Name = "budgetCode")]
        public string BudgetCode
        {
            get
            {
                if (!string.IsNullOrEmpty(budgetCode))
                { return budgetCode; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { budgetCode = value; }
            }
        }

        string budgetDesc = string.Empty;
        [DataMember(Name = "budgetDesc")]
        public string BudgetDesc
        {
            get
            {
                if (!string.IsNullOrEmpty(budgetDesc))
                { return budgetDesc; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { budgetDesc = value; }
            }
        }


        [DataMember(Name = "isValid")]
        public int IsValid { get; set; }


    }
}