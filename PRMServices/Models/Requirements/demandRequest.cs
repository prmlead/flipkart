﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class DemandRequest : Entity
    {
        
        string caseID = string.Empty;
        [DataMember(Name = "caseID")]
        public string CaseID
        {
            get
            {
                if (!string.IsNullOrEmpty(caseID))
                {
                    return caseID;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    caseID = value;
                }
            }
        }

        string productNo = string.Empty;
        [DataMember(Name = "productNo")]
        public string ProductNo
        {
            get
            {
                if (!string.IsNullOrEmpty(productNo))
                {
                    return productNo;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    productNo = value;
                }
            }
        }

        string productName = string.Empty;
        [DataMember(Name = "productName")]
        public string ProductName
        {
            get
            {
                if (!string.IsNullOrEmpty(productName))
                {
                    return productName;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    productName = value;
                }
            }
        }

        string productDesc = string.Empty;
        [DataMember(Name = "productDesc")]
        public string ProductDesc
        {
            get
            {
                if (!string.IsNullOrEmpty(productDesc))
                {
                    return productDesc;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    productDesc = value;
                }
            }
        }

        string brand = string.Empty;
        [DataMember(Name = "brand")]
        public string Brand
        {
            get
            {
                if (!string.IsNullOrEmpty(brand))
                {
                    return brand;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    brand = value;
                }
            }
        }

        string brandPartCode = string.Empty;
        [DataMember(Name = "brandPartCode")]
        public string BrandPartCode
        {
            get
            {
                if (!string.IsNullOrEmpty(brandPartCode))
                {
                    return brandPartCode;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    brandPartCode = value;
                }
            }
        }

        string modelName = string.Empty;
        [DataMember(Name = "modelName")]
        public string ModelName
        {
            get
            {
                if (!string.IsNullOrEmpty(modelName))
                {
                    return modelName;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    modelName = value;
                }
            }
        }

        string deliveryLocation = string.Empty;
        [DataMember(Name = "deliveryLocation")]
        public string DeliveryLocation
        {
            get
            {
                if (!string.IsNullOrEmpty(deliveryLocation))
                {
                    return deliveryLocation;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    deliveryLocation = value;
                }
            }
        }

        string entity = string.Empty;
        [DataMember(Name = "entity")]
        public string Entity
        {
            get
            {
                if (!string.IsNullOrEmpty(entity))
                {
                    return entity;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    entity = value;
                }
            }
        }

        string typeOfStock = string.Empty;
        [DataMember(Name = "typeOfStock")]
        public string TypeOfStock
        {
            get
            {
                if (!string.IsNullOrEmpty(typeOfStock))
                {
                    return typeOfStock;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    typeOfStock = value;
                }
            }
        }

        string category = string.Empty;
        [DataMember(Name = "category")]
        public string Category
        {
            get
            {
                if (!string.IsNullOrEmpty(category))
                {
                    return category;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    category = value;
                }
            }
        }



        [DataMember(Name = "requiredQuantity")]
        public decimal RequiredQuantity { get; set; }

        

        
        [DataMember(Name = "expDeliveryDate")]
        public DateTime? ExpDeliveryDate { get; set; }

        

    }
}