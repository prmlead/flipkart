﻿using System;
using System.Runtime.Serialization;
using PRM.Core.Common;
using PRMServices.Models;


namespace PRMServices.Models
{
    public class GstDetails : Entity
    {
        [DataMember(Name = "GST_ID")]
        public int GST_ID { get; set; }

        [DataMember(Name = "USER_ID")]
        public int USER_ID { get; set; }

        [DataMember(Name = "COMP_ID")]
        public int COMP_ID { get; set; }

        [DataMember(Name = "CGST")]
        public decimal CGST { get; set; }

        [DataMember(Name = "SGST")]
        public decimal SGST { get; set; }

        [DataMember(Name = "IGST")]
        public decimal IGST { get; set; }

        [DataMember(Name = "TAX_CATEGORY_NAME")]
        public string TAX_CATEGORY_NAME { get; set; }

        [DataMember(Name = "ENTITY")]
        public string ENTITY { get; set; }

        [DataMember(Name = "TAX_RATE")]
        public decimal TAX_RATE { get; set; }

        [DataMember(Name = "IS_RCM_APPLICABLE")]
        public string IS_RCM_APPLICABLE { get; set; }

        [DataMember(Name = "EWC")]
        public string EWC { get; set; }

        [DataMember(Name = "TAX_CATEGORY_TYPE")]
        public string TAX_CATEGORY_TYPE { get; set; }

    }
}