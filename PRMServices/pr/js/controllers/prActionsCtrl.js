﻿prmApp
    .controller('prActionsCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService",
        "storeService", "growlService", "PRMPRServices", "poService", "$rootScope", "catalogService",
        "fileReader", "workflowService",
        function ($scope, $stateParams, $log, $state, $window, userService, auctionsService,
            storeService, growlService, PRMPRServices, poService, $rootScope, catalogService,
            fileReader, workflowService) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.compID = userService.getUserCompanyId();
            //$scope.isSuperUser = userService.getUserObj().isSuperUser;
            $scope.prID = $stateParams.Id;
            $scope.sendcommunication = true;
            $scope.isFormdisabled = false;
            $scope.itemWorkflow = [];
            $scope.isWorkflowCompleted = false;
            $scope.IsUserApprover = false;
            $scope.currentStep = 0;
            $scope.WorkflowModule = 'PR';
            $scope.deptIDs = [];
            $scope.desigIDs = [];
            $scope.deptIDStr = '';
            $scope.desigIDStr = '';
            console.log($stateParams.reqId);
            $scope.reqId = $stateParams.reqId;
            $scope.qcsId = $stateParams.qcsId;
            $scope.prNumber = $stateParams.prNumber;
            $scope.flow = $stateParams.flow;
            $scope.prStatus = [
                {
                    display: 'PENDING',
                    value: 'PENDING'
                },
                {
                    display: 'COMPLETED',
                    value: 'COMPLETED'
                },
                {
                    display: 'CANCELLED',
                    value: 'CANCELLED'
                },
                {
                    display: 'REJECTED',
                    value: 'REJECTED'
                }
            ];


            $scope.isPRFormdisabled = true;
            $scope.onlyQuantitydisabled = false;

            $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
            if ($scope.ListUserDepartmentDesignations && $scope.ListUserDepartmentDesignations.length > 0) {
                $scope.ListUserDepartmentDesignations.forEach(function (item, index) {
                    $scope.deptIDs.push(item.deptID);
                    item.listDesignation.forEach(function (item1, index1) {
                        if (item1.isAssignedToUser && item1.isValid) {
                            $scope.desigIDs.push(item1.desigID);
                            $scope.desigIDs = _.union($scope.desigIDs, [item1.desigID]);
                        }
                    });
                });

                $scope.deptIDStr = $scope.deptIDs.join(',');
                $scope.desigIDStr = $scope.desigIDs.join(',');
            }
          
            $scope.savePr = function () {
                var params = {
                    "prdetails": $scope.PRDetails,
                    "sendcommunication": $scope.sendcommunication,
                    "sessionid": userService.getUserToken()
                };

                var ts = userService.toUTCTicks($scope.PRDetails.REQUEST_DATE);
                var m = moment(ts);
                var reqdate = new Date(m);
                var milliseconds = parseInt(reqdate.getTime() / 1000.0);
                params.prdetails.REQUEST_DATE = "/Date(" + milliseconds + "000++530)/";

                ts = userService.toUTCTicks($scope.PRDetails.REQUIRED_DATE);
                m = moment(ts);
                var reqrddate = new Date(m);
                milliseconds = parseInt(reqrddate.getTime() / 1000.0);
                params.prdetails.REQUIRED_DATE = "/Date(" + milliseconds + "000++530)/";
                
                PRMPRServices.savePRActions(params)
                    .then(function (response) {

                        if (response.errorMessage !== '') {
                            growlService.growl(response.errorMessage, "inverse");
                        } else {
                            growlService.growl("Saved Successfully.", "success");
                        }
                    });
            };


            $scope.getprdetails = function () {
                var params = {
                    "prid": $stateParams.Id,
                    "sessionid": userService.getUserToken()
                };
                PRMPRServices.getprdetails(params)
                    .then(function (response) {

                        $scope.PRDetails = response;
                        $scope.PRDetails.REQUEST_DATE = userService.toLocalDate($scope.PRDetails.REQUEST_DATE);
                        $scope.PRDetails.REQUIRED_DATE = userService.toLocalDate($scope.PRDetails.REQUIRED_DATE);
                        $scope.PRDetails.RELEASE_DATE = userService.toLocalDate($scope.PRDetails.RELEASE_DATE);
                        $scope.getItemWorkflow();
                    });
            };


            if ($stateParams.Id > 0) {
                $scope.getprdetails();
            }

            $scope.goToPrList = function (id) {
                var url = $state.href("list-pr");
                window.open(url, '_self');
            };

            $scope.getItemWorkflow = function () {
                workflowService.getItemWorkflow(0, $scope.prID, 'PR')
                    .then(function (response) {
                        $scope.itemWorkflow = response;
                        $scope.checkIsFormDisable();
                        if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                            $scope.currentStep = 0;
                            var count = 0;
                            $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {
                                if (!track.multipleAttachments) {
                                    track.multipleAttachments = [];
                                }

                                if (track.status === 'APPROVED') { track.statusNew = 'Approved'; }
                                if (track.status === 'HOLD') { track.statusNew = 'Hold'; }
                                if (track.status === 'PENDING') { track.statusNew = 'Pending'; }
                                if (track.status === 'REJECTED') { track.statusNew = 'Rejected'; }
                                if (track.status === 'APPROVED' || track.status === 'HOLD') {
                                    $scope.isFormdisabled = true;
                                }

                                if (track.status === 'APPROVED') {
                                    $scope.isWorkflowCompleted = true;
                                }
                                else {
                                    $scope.isWorkflowCompleted = false;
                                }

                                if (track.status === 'REJECTED' && !count) {
                                    count = count + 1;
                                }

                                if ((track.status === 'PENDING' || track.status === 'HOLD') && !count) {
                                    count = count + 1;
                                    $scope.IsUserApproverForStage(track.approverID);
                                    $scope.currentAccess = track.order;
                                }

                                if ((track.status === 'PENDING' || track.status === 'HOLD' || track.status === 'REJECTED') && !$scope.currentStep) {
                                    $scope.currentStep = track.order;
                                    return false;
                                }
                            });
                        }
                    });
            };

            $scope.isApproverDisable = function (index) {
                let disable = true;
                var previousStep = {};
                $scope.itemWorkflow[0].WorkflowTracks.forEach(function (step, stepIndex) {
                    if (index === stepIndex) {
                        if (stepIndex === 0) {
                            if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                                (step.status === 'PENDING' || step.status === 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                        else if (stepIndex > 0) {
                            if (previousStep.status === 'PENDING' || previousStep.status === 'HOLD' || previousStep.status === 'REJECTED') {
                                disable = true;
                            }
                            else if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                                (step.status === 'PENDING' || step.status === 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                    }
                    previousStep = step;
                });

                return disable;
            };

            $scope.updateTrack = function (step, status) {

                if (!$rootScope.isUserEntitled(591164)) {
                    swal("Error!", "Workflow Approval Denied", "error");
                    $state.go('home');
                    return;
                }

                $scope.disableAssignPR = true;
                $scope.commentsError = '';
                if (step.comments) {
                    step.comments = validateStringWithoutSpecialCharacters(step.comments);
                }

                var tempArray = $scope.itemWorkflow[0].WorkflowTracks[$scope.itemWorkflow[0].WorkflowTracks.length - 1];
                if (step.order === tempArray.order && status === 'APPROVED') {
                    $scope.disableAssignPR = false;
                } else {
                    $scope.disableAssignPR = true;
                }

                if ($scope.isReject) {
                    $scope.commentsError = 'Please Save Rejected Items/Qty';
                    return false;
                }

                if (status === 'REJECTED' && !step.comments) {
                    $scope.commentsError = 'Please enter comments';
                    return false;
                }

                step.status = status;
                step.sessionID = $scope.sessionID;
                step.modifiedBy = userService.getUserId();
                step.moduleName = $scope.WorkflowModule;
                step.subModuleName = '';
                step.subModuleID = 0;

                workflowService.SaveWorkflowTrack(step)
                    .then(function (response) {
                        if (response.errorMessage) {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            step.multipleAttachments = [];
                            $scope.getItemWorkflow();
                            $state.go('list-pr');
                        }
                    });
            };

            $scope.IsUserApproverForStage = function (approverID) {
                workflowService.IsUserApproverForStage(approverID, userService.getUserId())
                    .then(function (response) {
                        $scope.IsUserApprover = response;
                    });
            };

            $scope.checkIsFormDisable = function () {
                $scope.isFormdisabled = false;
                if ($scope.itemWorkflow.length === 0) {
                    $scope.isFormdisabled = true;
                } else {
                    if ((+$scope.PRDetails.CREATED_BY === +userService.getUserId() || +$scope.PRDetails.MODIFIED_BY === +userService.getUserId()) && $scope.itemWorkflow[0].WorkflowTracks.length > 0 &&
                        $scope.itemWorkflow[0].WorkflowTracks[0].status !== "APPROVED" && $scope.itemWorkflow[0].WorkflowTracks[0].order === 1 && $scope.itemWorkflow[0].workflowID > 0) {
                        $scope.isFormdisabled = true;
                    }
                }
            };

            $scope.isUserBelongsToDeptandDesig = function (deptID, desigID) {
                let isEligible = true;
                if ($scope.deptIDs.indexOf(deptID) !== -1 && $scope.desigIDs.indexOf(desigID) !== -1) {
                    isEligible = true;
                } else {
                    isEligible = false;
                }

                return isEligible;
            };

            $scope.navigateToPOForm = function () {

                if (!$rootScope.isUserEntitled(519951)) {
                    swal("Error!", "Create PO Access Denied", "error");
                    $state.go('home');
                    return;
                }
                $state.go(
                    'po-domestic-zsdm-rev',
                    { 'reqId': $scope.reqId, 'qcsId': $scope.qcsId, 'prNumber': $scope.prNumber, 'Id': 0, 'isEditable': 1, 'prId': $scope.prID }
                );
            };

            $scope.checkIsPODisable = function () {
                let doShow = false;
                var approverCount = 0;
                if ($scope.itemWorkflow.length > 0) {
                    approverCount = $scope.itemWorkflow[0].WorkflowTracks.length;
                    if ($scope.itemWorkflow[0].WorkflowTracks.length > 0 &&
                        $scope.itemWorkflow[0].WorkflowTracks[approverCount - 1].status === "APPROVED" && $scope.itemWorkflow[0].WorkflowTracks[approverCount - 1].order == approverCount && $scope.itemWorkflow[0].workflowID > 0) {
                        doShow = true;
                    }
                }

                return doShow;
            };

            $scope.removeAttach = function (attachmentObj, type) {
                if (type) {
                    if (attachmentObj && attachmentObj.fileID) {
                        $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {
                            track.multipleAttachments = track.multipleAttachments.filter(function (fileObj) {
                                return fileObj.fileID !== attachmentObj.fileID;
                            });
                        });
                    } else if (attachmentObj && attachmentObj.fileName) {
                        $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {
                            track.multipleAttachments = track.multipleAttachments.filter(function (fileObj) {
                                return fileObj.fileName !== attachmentObj.fileName;
                            });
                        });                    }
                }
            };

            //$scope.getFile1 = function (id, itemid, ext) {
            //    $scope.filesTemp = $("#" + id)[0].files;
            //    $scope.filesTemp = Object.values($scope.filesTemp);
            //    $scope.filesTemp.forEach(function (attach, attachIndex) {
            //        $scope.file = $("#" + id)[0].files[attachIndex];
            //        fileReader.readAsDataUrl($scope.file, $scope)
            //            .then(function (result) {
            //                var fileUpload = {
            //                    fileStream: [],
            //                    fileName: '',
            //                    fileID: 0
            //                };
            //                var bytearray = new Uint8Array(result);
            //                fileUpload.fileStream = $.makeArray(bytearray);
            //                fileUpload.fileName = attach.name;
            //                $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {
            //                    var ifExists1 = _.findIndex(track.multipleAttachments, function (attach) { return attach.fileName.toLowerCase() === fileUpload.fileName.toLowerCase(); });
            //                    if (ifExists1 < 0) {
            //                        track.multipleAttachments.push(fileUpload);
            //                    }
            //                });
            //            });
            //    });
            //};

            $scope.getFile = function () {
                $scope.progress = 0;
                let totalQCSAttachmentsSize = 0;
                //$scope.file = $("#attachement")[0].files[0];
                let multipleAttachments = $("#attachement")[0].files;
                multipleAttachments = Object.values(multipleAttachments);
                if (multipleAttachments && multipleAttachments.length > 0) {
                    multipleAttachments.forEach(function (item, index) {
                        totalQCSAttachmentsSize = totalQCSAttachmentsSize + item.size;
                    });
                }

                if (totalQCSAttachmentsSize > $scope.totalAttachmentMaxSize) {
                    swal({
                        title: "Attachment size!",
                        text: "Total Attachments size cannot exceed 6MB",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {
                            return;
                        });
                    return;
                }

                multipleAttachments.forEach(function (item, index) {
                    fileReader.readAsDataUrl(item, $scope)
                        .then(function (result) {
                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };

                            var bytearray = new Uint8Array(result);
                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = item.name;
                            $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {
                                var ifExists1 = _.findIndex(track.multipleAttachments, function (attach) { return attach.fileName.toLowerCase() === fileUpload.fileName.toLowerCase(); });
                                if (ifExists1 < 0) {
                                    track.multipleAttachments.push(fileUpload);
                                }
                            });
                        });
                });
            };


            function validateStringWithoutSpecialCharacters(string) {
                if (string) {
                    string = string.replace(/\'/gi, "");
                    string = string.replace(/\"/gi, "");
                    string = string.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                    string = string.replace(/(\r\n|\n|\r)/gm, "");
                    string = string.replace(/\t/g, '');
                    return string;
                }
            }
            $scope.showcreatePOButton = false;


            $scope.checkPOAvailable = function () {
                var params = {
                    prID: $scope.prID
                }
                
                PRMPRServices.checkPOAvailable(params)
                    .then(function (response) {

                        if (response.CheckPOAvailableResult.objectID == -1) {
                            $scope.showcreatePOButton = true;
                        }
                    });
            }

            $scope.checkPOAvailable();

            $scope.showApprovedDate = function (date) {
                return userService.toLocalDate(date);
            };
        }]);
