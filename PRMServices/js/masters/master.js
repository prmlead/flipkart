﻿prmApp.factory("PrmMasterService", ['httpServices', 'masterDomain', 'domain', 'userService', function (httpServices, masterDomain, domain, userService) {

    var factory = {};
    factory.insertContactDetails = function (data) {
        return httpServices.post(masterDomain + "contact_detail/insert", data)
    }
    factory.updateContactDetails = function (data) {
        return httpServices.post(masterDomain + "contact_detail/update", data)
    }
    factory.deleteContactDetails = function (DataCue) {
        return httpServices.post(masterDomain + "contact_detail/delete",DataCue)
    }

    factory.getByPageContactDetails = function (data) {
        return httpServices.post(masterDomain + "contact_detail/getbypage", data)
    }

    factory.getContactDetails = function (data) {
        return httpServices.post(masterDomain + "contact_detail/get", data)
    }

    factory.getByIdContactDetails = function (id) {
        return httpServices.get(masterDomain + "contact_detail/getbyid/"+ id)
    }


    factory.insertPaymentTerms = function (data) {
        return httpServices.post(masterDomain + "payment_term/insert", data)
    }
    factory.updatePaymentTerms = function (data) {
        return httpServices.post(masterDomain + "payment_term/update", data)
    }
    factory.deletePaymentTerms = function (id) {
        return httpServices.post(masterDomain + "payment_term/delete" , id)
    }

    factory.getByPagePaymentTerms = function (data) {
        return httpServices.post(masterDomain + "payment_term/getbypage", data)
    }

    factory.getPaymentTerms = function (data) {
        return httpServices.post(masterDomain + "payment_term/get", data)
    }

    factory.getByIdPaymentTerms = function (id) {
        return httpServices.get(masterDomain + "payment_term/getbyid/" + id)
    }

    factory.insertDeliveryTerms = function (data) {
        return httpServices.post(masterDomain + "delivery_term/insert", data)
    }
    factory.updateDeliveryTerms = function (data) {
        return httpServices.post(masterDomain + "delivery_term/update", data)
    }
    factory.deleteDeliveryTerms = function (id) {
        return httpServices.post(masterDomain + "delivery_term/delete" ,id)
    }

    factory.getByPageDeliveryTerms = function (data) {
        return httpServices.post(masterDomain + "delivery_term/getbypage", data)
    }

    factory.getDeliveryTerms = function (data) {
        return httpServices.post(masterDomain + "delivery_term/get", data)
    }

    factory.getByIdDeliveryTerms = function (id) {
        return httpServices.get(masterDomain + "delivery_term/getbyid/" + id)
    }

    factory.insertDeliveryLocations = function (data) {
        return httpServices.post(masterDomain + "delivery_location/insert", data)
    }
    factory.updateDeliveryLocations = function (data) {
        return httpServices.post(masterDomain + "delivery_location/update", data)
    }
    factory.deleteDeliveryLocations = function (id) {
        return httpServices.post(masterDomain + "delivery_location/delete" ,id)
    }

    factory.getByPageDeliveryLocations = function (data) {
        return httpServices.post(masterDomain + "delivery_location/getbypage", data)
    }

    factory.getDeliveryLocations = function (data) {
        return httpServices.post(masterDomain + "delivery_location/get", data)
    }

    factory.getByIdDeliveryLocations = function (id) {
        return httpServices.get(masterDomain + "delivery_location/getbyid/" + id)
    }


    factory.insertGeneralTerms = function (data) {
        return httpServices.post(masterDomain + "general_term/insert", data)
    }
    factory.updateGeneralTerms = function (data) {
        return httpServices.post(masterDomain + "general_term/update", data)
    }
    factory.deleteGeneralTerms = function (id) {
        return httpServices.post(masterDomain + "general_term/delete" , id)
    }

    factory.getByPageGeneralTerms = function (data) {
        return httpServices.post(masterDomain + "general_term/getbypage", data)
    }

    factory.getGeneralTerms = function (data) {
        return httpServices.post(masterDomain + "general_term/get", data)
    }

    factory.getByIdGeneralTerms = function (id) {
        return httpServices.get(masterDomain + "general_term/getbyid/" + id)
    }
    factory.getCompanyDepartments = function () {


        return httpServices.get(domain + 'getcompanydepartments?userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken())
    }

    factory.getCompanyusers = function () {


        return httpServices.get(domain + 'getsubuserdata?userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken())
    }


    return factory;

}])

prmApp.controller("MasterContactDetailController", ['$uibModal','$state','PrmMasterService', '$scope', 'SettingService', 'store', 'growlService', 'fileReader', 'realTimePriceService', 'AngularUtility', '$stateParams',
    function ($uibModal, $state, _prmMasterService, $scope, SettingService, store, growlService, fileReader, realTimePriceService, AngularUtility, $stateParams) {

    var _masterContactDetailCtrl = this;
    _masterContactDetailCtrl.createupdateview = function () {
      return  "views/masters/contactdetails/createorupdate.html"
    }
    _masterContactDetailCtrl.gridOpitions = {
        itemsByPage: 20,
        pageSizeOptions: [10, 20, 50, 100],
        displayedPages: 7,
        showFilter: false,
        data: [],
        totalItems: 0,
        loading: false,
        noData: true,
        searchTerm: "",
        enableFilter: true,
        columns: [
            {
                name: 'Log Level',
                field: 'LogLevelId',
                type: 'int',
            },
            {
                name: 'Short message',
                field: 'ShortMessage',
                type: 'string',

            },
            {
                name: 'Created On',
                field: 'CreatedOnUtc',
                type: 'date',
            },
            {
                name: 'IP Address',
                field: 'IpAddress',
                display: true,
                template: '<ip-address ng-model="row.LastIpAddress"></ip-address>'
            },
            {
                name: 'CustomerId',
                field: 'CustomerId',
                type: 'ObjectId',
            },

        ],
    }

    _masterContactDetailCtrl.ContactDetailModel = {
        AllowedDepartments: [],
        AllowedUsers: [],
        SelectedDepartments: [],
        SelectedUsers : [],
    }

    _masterContactDetailCtrl.UserList = [];
    _masterContactDetailCtrl.DepartmentList = [];
    _masterContactDetailCtrl.getDeparments = function () {
        _prmMasterService.getCompanyDepartments().then(function (response) {
            _masterContactDetailCtrl.DepartmentList = response;
            angular.forEach(_masterContactDetailCtrl.DepartmentList, function (item) {
                var index = _masterContactDetailCtrl.ContactDetailModel.AllowedDepartments.indexOf(item.deptID);
                if (index !== -1) {
                    item.Selected = true;
                }
            })


        })
    };

    _masterContactDetailCtrl.getCompanyusers = function () {
        _prmMasterService.getCompanyusers().then(function (response) {
            _masterContactDetailCtrl.UserList = response;
            var allowedUser = [];
            angular.forEach(_masterContactDetailCtrl.UserList, function (item) {
                var index = _masterContactDetailCtrl.ContactDetailModel.AllowedUsers.indexOf(parseInt(item.userID));
                if (index !== -1) {
                    item.Selected = true;
                }
            });
        })
    };



    _masterContactDetailCtrl.tableState = {};
    _masterContactDetailCtrl.getByPage = function (table) {
        if (table == undefined)
            return false;

        _masterContactDetailCtrl.tableState = table;

        var pager = AngularUtility.preparePager(table, _masterContactDetailCtrl.gridOpitions.columns);

        _prmMasterService.getByPageContactDetails(pager).then(function (response) {
            var _jsonResposne = JSON.parse(response)
            if (_jsonResposne.Data != undefined) {
                _masterContactDetailCtrl.gridOpitions.data = _jsonResposne.Data;
                _masterContactDetailCtrl.gridOpitions.totalItems = _jsonResposne.TotalCount;
                table.pagination.totalItemCount = _jsonResposne.TotalCount;
                table.pagination.numberOfPages = _jsonResposne.TotalPages;
                _masterContactDetailCtrl.gridOpitions.loading = false;
            }
            else {
                _masterContactDetailCtrl.gridOpitions.data = [];
            }


        })
    }

    _masterContactDetailCtrl.getById = function () {
        _prmMasterService.getByIdContactDetails($stateParams.id).then(function (response) {
            if (response != null) {
                _masterContactDetailCtrl.ContactDetailModel = response;
            }

        })
    }

    _masterContactDetailCtrl.getContactDetails = function () {
        var data = {
            Departments: [67],
            Projects:[]
        }
        _prmMasterService.getContactDetails(data).then(function (response) {

            console.log(response)
        })
    }

    _masterContactDetailCtrl.getContactDetails();


    _masterContactDetailCtrl.Create = function (form) {
        form.$submitted = true;
        if (form.$valid) {

            var model = angular.copy(_masterContactDetailCtrl.ContactDetailModel);

            if (model.LimitedToDepartment && model.SelectedDepartments.length == 0) {
                growlService.growl("please select aleast one department", "inverse");
                return false;
            }

            if (model.LimitedToUser && model.SelectedUsers.length == 0) {
                growlService.growl("please select aleast one user", "inverse");
                return false;

            }



            if (model.SelectedUsers != [] && model.SelectedUsers != undefined)
                model.AllowedUsers = model.SelectedUsers.map(function (item) { return item.userID })

            if (model.SelectedDepartments != [] && model.SelectedDepartments != undefined)
                model.AllowedDepartments = model.SelectedDepartments.map(function (item) { return item.deptID })

            _prmMasterService.insertContactDetails(model).then(function (response) {
                if (response.errorMessage == "") {
                    _masterContactDetailCtrl.backTo();

                    growlService.growl(response.message, "inverse");
                }
                else {
                    growlService.growl(response.message, "inverse");
                }
            })
        }
    }

    _masterContactDetailCtrl.Update = function (form) {
        form.$submitted = true;
        if (form.$valid) {

            var model = angular.copy(_masterContactDetailCtrl.ContactDetailModel);

            if (model.LimitedToDepartment && model.SelectedDepartments.length == 0) {
                growlService.growl("please select aleast one department", "inverse");
                return false;
            }

            if (model.LimitedToUser && model.SelectedUsers.length == 0) {
                growlService.growl("please select aleast one user", "inverse");
                return false;

            }

            if (model.SelectedUsers != [] && model.SelectedUsers != undefined)
            model.AllowedUsers = model.SelectedUsers.map(function (item) { return item.userID })

            if (model.SelectedDepartments != [] && model.SelectedDepartments != undefined)
            model.AllowedDepartments = model.SelectedDepartments.map(function (item) { return item.deptID })

            _prmMasterService.updateContactDetails(model).then(function (response) {
                if (response.errorMessage == "") {
                    _masterContactDetailCtrl.backTo();

                    growlService.growl(response.message, "inverse");
                }
                else {
                    growlService.growl(response.message, "inverse");
                }
            })
        }
    }

    _masterContactDetailCtrl.Delete = function (row) {
        _prmMasterService.deleteContactDetails({ id: row.Id }).then(function (response) {
            if (response.errorMessage == "") {
                _masterContactDetailCtrl.getByPage(_masterContactDetailCtrl.tableState)
            }
            else {
                growlService.growl(response.message, "inverse");
            }
        })
    }

    _masterContactDetailCtrl.edit = function (row) {
        $state.go("pages.master_contactdetail_edit", { id: row.Id})
    }
    _masterContactDetailCtrl.backTo = function () {
        $state.go("pages.master_contactdetails")
    }


    _masterContactDetailCtrl.add = function () {
        $state.go("pages.master_contactdetail_add")
    }


    }]);

prmApp.controller("MasterDeliveryLocationController", ['$uibModal', '$state', 'PrmMasterService', '$scope', 'SettingService', 'store', 'growlService', 'fileReader', 'realTimePriceService', 'AngularUtility', '$stateParams',
    function ($uibModal, $state, _prmMasterService, $scope, SettingService, store, growlService, fileReader, realTimePriceService, AngularUtility, $stateParams) {

        var _masterDeliveryLocationCtrl = this;
        _masterDeliveryLocationCtrl.createupdateview = function () {
            return "views/masters/DeliveryLocations/createorupdate.html"
        }
        _masterDeliveryLocationCtrl.gridOpitions = {
            itemsByPage: 20,
            pageSizeOptions: [10, 20, 50, 100],
            displayedPages: 7,
            showFilter: false,
            data: [],
            totalItems: 0,
            loading: false,
            noData: true,
            searchTerm: "",
            enableFilter: true,
            columns: [
                {
                    name: 'Log Level',
                    field: 'LogLevelId',
                    type: 'int',
                },
                {
                    name: 'Short message',
                    field: 'ShortMessage',
                    type: 'string',

                },
                {
                    name: 'Created On',
                    field: 'CreatedOnUtc',
                    type: 'date',
                },
                {
                    name: 'IP Address',
                    field: 'IpAddress',
                    display: true,
                    template: '<ip-address ng-model="row.LastIpAddress"></ip-address>'
                },
                {
                    name: 'CustomerId',
                    field: 'CustomerId',
                    type: 'ObjectId',
                },

            ],
        }

        _masterDeliveryLocationCtrl.DeliveryLocationModel = {
            AllowedDepartments: [],
            AllowedUsers : []
        }

        _masterDeliveryLocationCtrl.UserList = [];
        _masterDeliveryLocationCtrl.DepartmentList = [];
        _masterDeliveryLocationCtrl.getDeparments = function () {
            _prmMasterService.getCompanyDepartments().then(function (response) {
                _masterDeliveryLocationCtrl.DepartmentList = response;
                angular.forEach(_masterDeliveryLocationCtrl.DepartmentList, function (item) {
                    var index = _masterDeliveryLocationCtrl.DeliveryLocationModel.AllowedDepartments.indexOf(item.deptID);
                    if (index !== -1) {
                        item.Selected = true;
                    }
                })


            })
        };

        _masterDeliveryLocationCtrl.getCompanyusers = function () {
            _prmMasterService.getCompanyusers().then(function (response) {
                _masterDeliveryLocationCtrl.UserList = response;
                var allowedUser = [];
                angular.forEach(_masterDeliveryLocationCtrl.UserList, function (item) {
                    var index = _masterDeliveryLocationCtrl.DeliveryLocationModel.AllowedUsers.indexOf(parseInt(item.userID));
                    if (index !== -1) {
                        item.Selected = true;
                    }
                });

            })
        };

        _masterDeliveryLocationCtrl.tableState = {};
        _masterDeliveryLocationCtrl.getByPage = function (table) {
            if (table == undefined)
                return false;

            _masterDeliveryLocationCtrl.tableState = table;

            var pager = AngularUtility.preparePager(table, _masterDeliveryLocationCtrl.gridOpitions.columns);

            _prmMasterService.getByPageDeliveryLocations(pager).then(function (response) {
                var _jsonResposne = JSON.parse(response)
                if (_jsonResposne.Data != undefined) {
                    _masterDeliveryLocationCtrl.gridOpitions.data = _jsonResposne.Data;
                    _masterDeliveryLocationCtrl.gridOpitions.totalItems = _jsonResposne.TotalCount;
                    table.pagination.totalItemCount = _jsonResposne.TotalCount;
                    table.pagination.numberOfPages = _jsonResposne.TotalPages;
                    _masterDeliveryLocationCtrl.gridOpitions.loading = false;
                }
                else {
                    _masterDeliveryLocationCtrl.gridOpitions.data = [];
                }


            })
        }

        _masterDeliveryLocationCtrl.getById = function () {
            _prmMasterService.getByIdDeliveryLocations($stateParams.id).then(function (response) {
                if (response != null) {
                    _masterDeliveryLocationCtrl.DeliveryLocationModel = response
                }

            })
        }


        _masterDeliveryLocationCtrl.Create = function (form) {
            form.$submitted = true;
            if (form.$valid) {

                var model = angular.copy(_masterDeliveryLocationCtrl.DeliveryLocationModel);

                if (model.LimitedToDepartment && model.SelectedDepartments.length == 0) {
                    growlService.growl("please select aleast one department", "inverse");
                    return false;
                }

                if (model.LimitedToUser && model.SelectedUsers.length == 0) {
                    growlService.growl("please select aleast one user", "inverse");
                    return false;

                }

                if (model.SelectedUsers != [] && model.SelectedUsers != undefined)
                    model.AllowedUsers = model.SelectedUsers.map(function (item) { return item.userID })

                if (model.SelectedDepartments != [] && model.SelectedDepartments != undefined)
                    model.AllowedDepartments = model.SelectedDepartments.map(function (item) { return item.deptID })

                _prmMasterService.insertDeliveryLocations(model).then(function (response) {
                    if (response.errorMessage == "") {
                        _masterDeliveryLocationCtrl.backTo();

                        growlService.growl(response.message, "inverse");
                    }
                    else {
                        growlService.growl(response.message, "inverse");
                    }
                })
            }
        }

        _masterDeliveryLocationCtrl.Update = function (form) {
            form.$submitted = true;
            if (form.$valid) {

                var model = angular.copy(_masterDeliveryLocationCtrl.DeliveryLocationModel);


                if (model.LimitedToDepartment && model.SelectedDepartments.length == 0) {
                    growlService.growl("please select aleast one department", "inverse");
                    return false;
                }

                if (model.LimitedToUser && model.SelectedUsers.length == 0) {
                    growlService.growl("please select aleast one user", "inverse");
                    return false;

                }

                if (model.SelectedUsers != [] && model.SelectedUsers != undefined)
                    model.AllowedUsers = model.SelectedUsers.map(function (item) { return item.userID })

                if (model.SelectedDepartments != [] && model.SelectedDepartments != undefined)
                    model.AllowedDepartments = model.SelectedDepartments.map(function (item) { return item.deptID })

                _prmMasterService.updateDeliveryLocations(model).then(function (response) {
                    if (response.errorMessage == "") {
                        _masterDeliveryLocationCtrl.backTo();

                        growlService.growl(response.message, "inverse");
                    }
                    else {
                        growlService.growl(response.message, "inverse");
                    }
                })
            }
        }

        _masterDeliveryLocationCtrl.Delete = function (row) {
            _prmMasterService.deleteDeliveryLocations({ id: row.Id }).then(function (response) {
                if (response.errorMessage == "") {
                    _masterDeliveryLocationCtrl.getByPage(_masterDeliveryLocationCtrl.tableState)
                }
                else {
                    growlService.growl(response.message, "inverse");
                }
            })
        }

        _masterDeliveryLocationCtrl.edit = function (row) {
            $state.go("pages.master_deliverylocation_edit", { id: row.Id })
        }
        _masterDeliveryLocationCtrl.backTo = function () {
            $state.go("pages.master_deliverylocations")
        }


        _masterDeliveryLocationCtrl.add = function () {
            $state.go("pages.master_deliverylocation_add")
        }


    }]);

prmApp.controller("MasterGeneralTermController", ['$uibModal', '$state', 'PrmMasterService', '$scope', 'SettingService', 'store', 'growlService', 'fileReader', 'realTimePriceService', 'AngularUtility', '$stateParams',
    function ($uibModal, $state, _prmMasterService, $scope, SettingService, store, growlService, fileReader, realTimePriceService, AngularUtility, $stateParams) {

        var _masterGeneralTermCtrl = this;
        _masterGeneralTermCtrl.createupdateview = function () {
            return "views/masters/GeneralTerms/createorupdate.html"
        }
        _masterGeneralTermCtrl.gridOpitions = {
            itemsByPage: 20,
            pageSizeOptions: [10, 20, 50, 100],
            displayedPages: 7,
            showFilter: false,
            data: [],
            totalItems: 0,
            loading: false,
            noData: true,
            searchTerm: "",
            enableFilter: true,
            columns: [
                {
                    name: 'Log Level',
                    field: 'LogLevelId',
                    type: 'int',
                },
                {
                    name: 'Short message',
                    field: 'ShortMessage',
                    type: 'string',

                },
                {
                    name: 'Created On',
                    field: 'CreatedOnUtc',
                    type: 'date',
                },
                {
                    name: 'IP Address',
                    field: 'IpAddress',
                    display: true,
                    template: '<ip-address ng-model="row.LastIpAddress"></ip-address>'
                },
                {
                    name: 'CustomerId',
                    field: 'CustomerId',
                    type: 'ObjectId',
                },

            ],
        }

        _masterGeneralTermCtrl.GeneralTermModel = {
            AllowedDepartments: [],
            AllowedUsers : []
        }

        _masterGeneralTermCtrl.UserList = [];
        _masterGeneralTermCtrl.DepartmentList = [];
        _masterGeneralTermCtrl.getDeparments = function () {
            _prmMasterService.getCompanyDepartments().then(function (response) {
                _masterGeneralTermCtrl.DepartmentList = response;
                angular.forEach(_masterGeneralTermCtrl.DepartmentList, function (item) {
                    var index = _masterGeneralTermCtrl.GeneralTermModel.AllowedDepartments.indexOf(item.deptID);
                    if (index !== -1) {
                        item.Selected = true;
                    }
                })


            })
        };

        _masterGeneralTermCtrl.getCompanyusers = function () {
            _prmMasterService.getCompanyusers().then(function (response) {
                _masterGeneralTermCtrl.UserList = response;
                var allowedUser = [];
                angular.forEach(_masterGeneralTermCtrl.UserList, function (item) {
                    var index = _masterGeneralTermCtrl.GeneralTermModel.AllowedUsers.indexOf(parseInt(item.userID));
                    if (index !== -1) {
                        item.Selected = true;
                    }
                });

            })
        };

        _masterGeneralTermCtrl.tableState = {};
        _masterGeneralTermCtrl.getByPage = function (table) {
            if (table == undefined)
                return false;

            _masterGeneralTermCtrl.tableState = table;

            var pager = AngularUtility.preparePager(table, _masterGeneralTermCtrl.gridOpitions.columns);

            _prmMasterService.getByPageGeneralTerms(pager).then(function (response) {
                var _jsonResposne = JSON.parse(response)
                if (_jsonResposne.Data != undefined) {
                    _masterGeneralTermCtrl.gridOpitions.data = _jsonResposne.Data;
                    _masterGeneralTermCtrl.gridOpitions.totalItems = _jsonResposne.TotalCount;
                    table.pagination.totalItemCount = _jsonResposne.TotalCount;
                    table.pagination.numberOfPages = _jsonResposne.TotalPages;
                    _masterGeneralTermCtrl.gridOpitions.loading = false;
                }
                else {
                    _masterGeneralTermCtrl.gridOpitions.data = [];
                }


            })
        }

        _masterGeneralTermCtrl.getById = function () {
            _prmMasterService.getByIdGeneralTerms($stateParams.id).then(function (response) {
                if (response != null) {
                    _masterGeneralTermCtrl.GeneralTermModel = response
                }

            })
        }


        _masterGeneralTermCtrl.Create = function (form) {
            form.$submitted = true;
            if (form.$valid) {

                var model = angular.copy(_masterGeneralTermCtrl.GeneralTermModel);

                if (model.LimitedToDepartment && model.SelectedDepartments.length == 0) {
                    growlService.growl("please select aleast one department", "inverse");
                    return false;
                }

                if (model.LimitedToUser && model.SelectedUsers.length == 0) {
                    growlService.growl("please select aleast one user", "inverse");
                    return false;

                }

                if (model.SelectedUsers != [] && model.SelectedUsers != undefined)
                    model.AllowedUsers = model.SelectedUsers.map(function (item) { return item.userID })

                if (model.SelectedDepartments != [] && model.SelectedDepartments != undefined)
                    model.AllowedDepartments = model.SelectedDepartments.map(function (item) { return item.deptID })

                _prmMasterService.insertGeneralTerms(model).then(function (response) {
                    if (response.errorMessage == "") {
                        _masterGeneralTermCtrl.backTo();

                        growlService.growl(response.message, "inverse");
                    }
                    else {
                        growlService.growl(response.message, "inverse");
                    }
                })
            }
        }

        _masterGeneralTermCtrl.Update = function (form) {
            form.$submitted = true;
            if (form.$valid) {

                var model = angular.copy(_masterGeneralTermCtrl.GeneralTermModel);
                if (model.LimitedToDepartment && model.SelectedDepartments.length == 0) {
                    growlService.growl("please select aleast one department", "inverse");
                    return false;
                }

                if (model.LimitedToUser && model.SelectedUsers.length == 0) {
                    growlService.growl("please select aleast one user", "inverse");
                    return false;

                }

                if (model.SelectedUsers != [] && model.SelectedUsers != undefined)
                    model.AllowedUsers = model.SelectedUsers.map(function (item) { return item.userID })

                if (model.SelectedDepartments != [] && model.SelectedDepartments != undefined)
                    model.AllowedDepartments = model.SelectedDepartments.map(function (item) { return item.deptID })


                _prmMasterService.updateGeneralTerms(model).then(function (response) {
                    if (response.errorMessage == "") {
                        _masterGeneralTermCtrl.backTo();

                        growlService.growl(response.message, "inverse");
                    }
                    else {
                        growlService.growl(response.message, "inverse");
                    }
                })
            }
        }

        _masterGeneralTermCtrl.Delete = function (row) {
            _prmMasterService.deleteGeneralTerms({ id: row.Id }).then(function (response) {
                if (response.errorMessage == "") {
                    _masterGeneralTermCtrl.getByPage(_masterGeneralTermCtrl.tableState)
                }
                else {
                    growlService.growl(response.message, "inverse");
                }
            })
        }

        _masterGeneralTermCtrl.edit = function (row) {
            $state.go("pages.master_generalterm_edit", { id: row.Id })
        }
        _masterGeneralTermCtrl.backTo = function () {
            $state.go("pages.master_generalterms")
        }


        _masterGeneralTermCtrl.add = function () {
            $state.go("pages.master_generalterm_add")
        }


    }]);

prmApp.controller("MasterDeliveryTermController", ['$uibModal', '$state', 'PrmMasterService', '$scope', 'SettingService', 'store', 'growlService', 'fileReader', 'realTimePriceService', 'AngularUtility', '$stateParams',
    function ($uibModal, $state, _prmMasterService, $scope, SettingService, store, growlService, fileReader, realTimePriceService, AngularUtility, $stateParams) {

        var _masterDeliveryTermCtrl = this;
        _masterDeliveryTermCtrl.createupdateview = function () {
            return "views/masters/DeliveryTerms/createorupdate.html"
        }
        _masterDeliveryTermCtrl.gridOpitions = {
            itemsByPage: 20,
            pageSizeOptions: [10, 20, 50, 100],
            displayedPages: 7,
            showFilter: false,
            data: [],
            totalItems: 0,
            loading: false,
            noData: true,
            searchTerm: "",
            enableFilter: true,
            columns: [
                {
                    name: 'Log Level',
                    field: 'LogLevelId',
                    type: 'int',
                },
                {
                    name: 'Short message',
                    field: 'ShortMessage',
                    type: 'string',

                },
                {
                    name: 'Created On',
                    field: 'CreatedOnUtc',
                    type: 'date',
                },
                {
                    name: 'IP Address',
                    field: 'IpAddress',
                    display: true,
                    template: '<ip-address ng-model="row.LastIpAddress"></ip-address>'
                },
                {
                    name: 'CustomerId',
                    field: 'CustomerId',
                    type: 'ObjectId',
                },

            ],
        }

        _masterDeliveryTermCtrl.DeliveryTermModel = {
            AllowedDepartments: [],
            AllowedUsers : []
        }

        _masterDeliveryTermCtrl.UserList = [];
        _masterDeliveryTermCtrl.DepartmentList = [];
        _masterDeliveryTermCtrl.getDeparments = function () {
            _prmMasterService.getCompanyDepartments().then(function (response) {
                _masterDeliveryTermCtrl.DepartmentList = response;
                angular.forEach(_masterDeliveryTermCtrl.DepartmentList, function (item) {
                    var index = _masterDeliveryTermCtrl.DeliveryTermModel.AllowedDepartments.indexOf(item.deptID);
                    if (index !== -1) {
                        item.Selected = true;
                    }
                })


            })
        };

        _masterDeliveryTermCtrl.getCompanyusers = function () {
            _prmMasterService.getCompanyusers().then(function (response) {
                _masterDeliveryTermCtrl.UserList = response;
                var allowedUser = [];
                angular.forEach(_masterDeliveryTermCtrl.UserList, function (item) {
                    var index = _masterDeliveryTermCtrl.DeliveryTermModel.AllowedUsers.indexOf(parseInt(item.userID));
                    if (index !== -1) {
                        item.Selected = true;
                    }
                });

            })
        };

        _masterDeliveryTermCtrl.tableState = {};
        _masterDeliveryTermCtrl.getByPage = function (table) {
            if (table == undefined)
                return false;

            _masterDeliveryTermCtrl.tableState = table;

            var pager = AngularUtility.preparePager(table, _masterDeliveryTermCtrl.gridOpitions.columns);

            _prmMasterService.getByPageDeliveryTerms(pager).then(function (response) {
                var _jsonResposne = JSON.parse(response)
                if (_jsonResposne.Data != undefined) {
                    _masterDeliveryTermCtrl.gridOpitions.data = _jsonResposne.Data;
                    _masterDeliveryTermCtrl.gridOpitions.totalItems = _jsonResposne.TotalCount;
                    table.pagination.totalItemCount = _jsonResposne.TotalCount;
                    table.pagination.numberOfPages = _jsonResposne.TotalPages;
                    _masterDeliveryTermCtrl.gridOpitions.loading = false;
                }
                else {
                    _masterDeliveryTermCtrl.gridOpitions.data = [];
                }


            })
        }

        _masterDeliveryTermCtrl.getById = function () {
            _prmMasterService.getByIdDeliveryTerms($stateParams.id).then(function (response) {
                if (response != null) {
                    _masterDeliveryTermCtrl.DeliveryTermModel = response
                }

            })
        }


        _masterDeliveryTermCtrl.Create = function (form) {
            form.$submitted = true;
            if (form.$valid) {

                var model = angular.copy(_masterDeliveryTermCtrl.DeliveryTermModel);
                if (model.LimitedToDepartment && model.SelectedDepartments.length == 0) {
                    growlService.growl("please select aleast one department", "inverse");
                    return false;
                }

                if (model.LimitedToUser && model.SelectedUsers.length == 0) {
                    growlService.growl("please select aleast one user", "inverse");
                    return false;

                }

                if (model.SelectedUsers != [] && model.SelectedUsers != undefined)
                    model.AllowedUsers = model.SelectedUsers.map(function (item) { return item.userID })

                if (model.SelectedDepartments != [] && model.SelectedDepartments != undefined)
                    model.AllowedDepartments = model.SelectedDepartments.map(function (item) { return item.deptID })

                _prmMasterService.insertDeliveryTerms(model).then(function (response) {
                    if (response.errorMessage == "") {
                        _masterDeliveryTermCtrl.backTo();

                        growlService.growl(response.message, "inverse");
                    }
                    else {
                        growlService.growl(response.message, "inverse");
                    }
                })
            }
        }

        _masterDeliveryTermCtrl.Update = function (form) {
            form.$submitted = true;
            if (form.$valid) {

                var model = angular.copy(_masterDeliveryTermCtrl.DeliveryTermModel);

                if (model.LimitedToDepartment && model.SelectedDepartments.length == 0) {
                    growlService.growl("please select aleast one department", "inverse");
                    return false;
                }

                if (model.LimitedToUser && model.SelectedUsers.length == 0) {
                    growlService.growl("please select aleast one user", "inverse");
                    return false;

                }

                if (model.SelectedUsers != [] && model.SelectedUsers != undefined)
                    model.AllowedUsers = model.SelectedUsers.map(function (item) { return item.userID })

                if (model.SelectedDepartments != [] && model.SelectedDepartments != undefined)
                    model.AllowedDepartments = model.SelectedDepartments.map(function (item) { return item.deptID })

                _prmMasterService.updateDeliveryTerms(model).then(function (response) {
                    if (response.errorMessage == "") {
                        _masterDeliveryTermCtrl.backTo();

                        growlService.growl(response.message, "inverse");
                    }
                    else {
                        growlService.growl(response.message, "inverse");
                    }
                })
            }
        }

        _masterDeliveryTermCtrl.Delete = function (row) {
            _prmMasterService.deleteDeliveryTerms({ id: row.Id }).then(function (response) {
                if (response.errorMessage == "") {
                    _masterDeliveryTermCtrl.getByPage(_masterDeliveryTermCtrl.tableState)
                }
                else {
                    growlService.growl(response.message, "inverse");
                }
            })
        }

        _masterDeliveryTermCtrl.edit = function (row) {
            $state.go("pages.master_deliveryterm_edit", { id: row.Id })
        }
        _masterDeliveryTermCtrl.backTo = function () {
            $state.go("pages.master_deliveryterms")
        }


        _masterDeliveryTermCtrl.add = function () {
            $state.go("pages.master_deliveryterm_add")
        }


    }]);

prmApp.controller("MasterPaymentTermController", ['$uibModal', '$state', 'PrmMasterService', '$scope', 'SettingService', 'store', 'growlService', 'fileReader', 'realTimePriceService', 'AngularUtility', '$stateParams',
    function ($uibModal, $state, _prmMasterService, $scope, SettingService, store, growlService, fileReader, realTimePriceService, AngularUtility, $stateParams) {

        var _masterPaymentTermCtrl = this;
        _masterPaymentTermCtrl.createupdateview = function () {
            return "views/masters/PaymentTerms/createorupdate.html"
        }
        _masterPaymentTermCtrl.gridOpitions = {
            itemsByPage: 20,
            pageSizeOptions: [10, 20, 50, 100],
            displayedPages: 7,
            showFilter: false,
            data: [],
            totalItems: 0,
            loading: false,
            noData: true,
            searchTerm: "",
            enableFilter: true,
            columns: [
                {
                    name: 'Log Level',
                    field: 'LogLevelId',
                    type: 'int',
                },
                {
                    name: 'Short message',
                    field: 'ShortMessage',
                    type: 'string',

                },
                {
                    name: 'Created On',
                    field: 'CreatedOnUtc',
                    type: 'date',
                },
                {
                    name: 'IP Address',
                    field: 'IpAddress',
                    display: true,
                    template: '<ip-address ng-model="row.LastIpAddress"></ip-address>'
                },
                {
                    name: 'CustomerId',
                    field: 'CustomerId',
                    type: 'ObjectId',
                },

            ],
        }

        _masterPaymentTermCtrl.PaymentTermModel = {
            AllowedDepartments: [],
            AllowedUsers : []
        }


        _masterPaymentTermCtrl.UserList = [];
        _masterPaymentTermCtrl.DepartmentList = [];
        _masterPaymentTermCtrl.getDeparments = function () {
            _prmMasterService.getCompanyDepartments().then(function (response) {
                _masterPaymentTermCtrl.DepartmentList = response;
                angular.forEach(_masterPaymentTermCtrl.DepartmentList, function (item) {
                    var index = _masterPaymentTermCtrl.PaymentTermModel.AllowedDepartments.indexOf(item.deptID);
                    if (index !== -1) {
                        item.Selected = true;
                    }
                })


            })
        };

        _masterPaymentTermCtrl.getCompanyusers = function () {
            _prmMasterService.getCompanyusers().then(function (response) {
                _masterPaymentTermCtrl.UserList = response;
                var allowedUser = [];
                angular.forEach(_masterPaymentTermCtrl.UserList, function (item) {
                    var index = _masterPaymentTermCtrl.PaymentTermModel.AllowedUsers.indexOf(parseInt(item.userID));
                    if (index !== -1) {
                        item.Selected = true;
                    }
                });

            })
        };


        _masterPaymentTermCtrl.tableState = {};
        _masterPaymentTermCtrl.getByPage = function (table) {
            if (table == undefined)
                return false;

            _masterPaymentTermCtrl.tableState = table;

            var pager = AngularUtility.preparePager(table, _masterPaymentTermCtrl.gridOpitions.columns);

            _prmMasterService.getByPagePaymentTerms(pager).then(function (response) {
                var _jsonResposne = JSON.parse(response)
                if (_jsonResposne.Data != undefined) {
                    _masterPaymentTermCtrl.gridOpitions.data = _jsonResposne.Data;
                    _masterPaymentTermCtrl.gridOpitions.totalItems = _jsonResposne.TotalCount;
                    table.pagination.totalItemCount = _jsonResposne.TotalCount;
                    table.pagination.numberOfPages = _jsonResposne.TotalPages;
                    _masterPaymentTermCtrl.gridOpitions.loading = false;
                }
                else {
                    _masterPaymentTermCtrl.gridOpitions.data = [];
                }


            })
        }

        _masterPaymentTermCtrl.getById = function () {
            _prmMasterService.getByIdPaymentTerms($stateParams.id).then(function (response) {
                if (response != null) {
                    _masterPaymentTermCtrl.PaymentTermModel = response
                }

            })
        }


        _masterPaymentTermCtrl.Create = function (form) {
            form.$submitted = true;
            if (form.$valid) {


                var model = angular.copy(_masterPaymentTermCtrl.PaymentTermModel);

                if (model.LimitedToDepartment && model.SelectedDepartments.length == 0) {
                    growlService.growl("please select aleast one department", "inverse");
                    return false;
                }

                if (model.LimitedToUser && model.SelectedUsers.length == 0) {
                    growlService.growl("please select aleast one user", "inverse");
                    return false;

                }

                if (model.SelectedUsers != [] && model.SelectedUsers != undefined)
                    model.AllowedUsers = model.SelectedUsers.map(function (item) { return item.userID })

                if (model.SelectedDepartments != [] && model.SelectedDepartments != undefined)
                    model.AllowedDepartments = model.SelectedDepartments.map(function (item) { return item.deptID })

                _prmMasterService.insertPaymentTerms(model).then(function (response) {
                    if (response.errorMessage == "") {
                        _masterPaymentTermCtrl.backTo();

                        growlService.growl(response.message, "inverse");
                    }
                    else {
                        growlService.growl(response.message, "inverse");
                    }
                })
            }
        }

        _masterPaymentTermCtrl.Update = function (form) {
            form.$submitted = true;
            if (form.$valid) {

                var model = angular.copy(_masterPaymentTermCtrl.PaymentTermModel);

                if (model.LimitedToDepartment && model.SelectedDepartments.length == 0) {
                    growlService.growl("please select aleast one department", "inverse");
                    return false;
                }

                if (model.LimitedToUser && model.SelectedUsers.length == 0) {
                    growlService.growl("please select aleast one user", "inverse");
                    return false;

                }

                if (model.SelectedUsers != [] && model.SelectedUsers != undefined)
                    model.AllowedUsers = model.SelectedUsers.map(function (item) { return item.userID })

                if (model.SelectedDepartments != [] && model.SelectedDepartments != undefined)
                    model.AllowedDepartments = model.SelectedDepartments.map(function (item) { return item.deptID })


                _prmMasterService.updatePaymentTerms(model).then(function (response) {
                    if (response.errorMessage == "") {
                        _masterPaymentTermCtrl.backTo();

                        growlService.growl(response.message, "inverse");
                    }
                    else {
                        growlService.growl(response.message, "inverse");
                    }
                })
            }
        }

        _masterPaymentTermCtrl.Delete = function (row) {
            _prmMasterService.deletePaymentTerms({ id: row.Id }).then(function (response) {
                if (response.errorMessage == "") {
                    _masterPaymentTermCtrl.getByPage(_masterPaymentTermCtrl.tableState)
                }
                else {
                    growlService.growl(response.message, "inverse");
                }
            })
        }

        _masterPaymentTermCtrl.edit = function (row) {
            $state.go("pages.master_paymentterm_edit", { id: row.Id })
        }
        _masterPaymentTermCtrl.backTo = function () {
            $state.go("pages.master_paymentterms")
        }


        _masterPaymentTermCtrl.add = function () {
            $state.go("pages.master_paymentterm_add")
        }


    }]);