﻿prmApp
    // =========================================================================
    // Auction Details Services
    // =========================================================================
    .service('fwdauctionsService', ["$http", "fwddomain", "httpServices", function ($http, fwddomain, httpServices) {
        //var domain = 'http://182.18.169.32/services/';
        var fwdauctions = this;
        fwdauctions.getdate = function () {
            var date = new Date();
            var time = date.getTime();
            return $http({
                method: 'GET',
                url: fwddomain + 'getdate?time=' + time,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
                //data: params
            }).then(function (response) {
                var list = [];
                if (response && response.data) {
                    if (response.data.length > 0) {
                        list = response.data;
                    }
                    return list;
                } else {
                    //console.log(response.data[0].errorMessage);
                }
            }, function (result) {
                //console.log("date error");
            });
        }

        fwdauctions.isnegotiationended = function (reqid, sessionid) {
            let url = fwddomain + 'isnegotiationended?reqid=' + reqid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        fwdauctions.selectVendor = function (params) {
            var url = fwddomain + 'selectvendor';
            return httpServices.post(url, params);
        };

        fwdauctions.getauctions = function (params) {
            let url = fwddomain + 'getrunningauctions?section=' + params.section + '&userid=' + params.userid + '&sessionid=' + params.sessionid + '&limit=' + params.limit;
            return httpServices.get(url);
        };

        fwdauctions.rateVendor = function (params) {
            var url = fwddomain + 'userratings';
            return httpServices.post(url, params);
        };

        fwdauctions.updateStatus = function (params) {
            var url = fwddomain + 'updatestatus';
            return httpServices.post(url, params);
        };

        fwdauctions.uploadquotationsfromexcel = function (params) {
            var url = fwddomain + 'uploadquotationsfromexcel';
            return httpServices.post(url, params);
        };

        fwdauctions.itemwiseselectvendor = function (params) {
            var url = fwddomain + 'itemwiseselectvendor';
            return httpServices.post(url, params);
        };


        fwdauctions.updatedeliverdate = function (params) {
            var url = fwddomain + 'updateexpdelandpaydate';
            return httpServices.post(url, params);
        };

        fwdauctions.reqTechSupport = function (params) {
            var url = fwddomain + 'reqtechsupport';
            return httpServices.post(url, params);
        };

        fwdauctions.SaveRunningItemPrice = function (params) {
            var url = fwddomain + 'saverunningitemprice';
            return httpServices.post(url, params);
        };

        fwdauctions.updatepaymentdate = function (params) {
            var url = fwddomain + 'updateexpdelandpaydate';
            return httpServices.post(url, params);
        };


        fwdauctions.StartNegotiation = function (params) {
            var url = fwddomain + 'startNegotiation';
            return httpServices.post(url, params);
        };

        fwdauctions.RestartNegotiation = function (params) {
            var url = fwddomain + 'restartnegotiation';
            return httpServices.post(url, params);
        };

        fwdauctions.DeleteVendorFromAuction = function (params) {
            var url = fwddomain + 'deletevendorfromauction';
            return httpServices.post(url, params);
        };

        fwdauctions.generatepo = function (params) {
            var data = {
                reqPO: params
            };
            var url = fwddomain + 'generatepoforuser';
            return httpServices.post(url, data);
        };

        fwdauctions.vendorreminders = function (params) {
            var url = fwddomain + 'vendorreminders';
            return httpServices.post(url, params);
        };

        fwdauctions.SaveCompanyDepartment = function (params) {
            var url = fwddomain + 'savecompanydepartment';
            return httpServices.post(url, params);
        };

        fwdauctions.SaveCompanyDesignations = function (params) {
            var url = fwddomain + 'savecompanydesignations';
            return httpServices.post(url, params);
        };

        fwdauctions.DeleteDepartment = function (params) {
            var url = fwddomain + 'deletedepartment';
            return httpServices.post(url, params);
        };

        fwdauctions.DeleteDesignation = function (params) {
            var url = fwddomain + 'deletedesignation';
            return httpServices.post(url, params);
        };

        fwdauctions.GetCompanyDepartments = function (userid, sessionid) {
            var url = fwddomain + 'getcompanydepartments?userid=' + userid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        fwdauctions.GetCompanyDesignations = function (userid, sessionid) {
            var url = fwddomain + 'getcompanydesignations?userid=' + userid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        fwdauctions.GetUserDepartments = function (userid, sessionid) {
            var url = fwddomain + 'getuserdepartments?userid=' + userid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        fwdauctions.GetUserDesignations = function (userid, sessionid) {
            var url = fwddomain + 'getuserdesignations?userid=' + userid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };


        fwdauctions.GetUserDeptDesig = function (userid, sessionid) {
            var url = fwddomain + 'getuserdeptdesig?userid=' + userid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        fwdauctions.GetCompDeptDesig = function (compid, sessionid) {
            var url = fwddomain + 'getcompdeptdesig?compid=' + compid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        fwdauctions.SaveUserDeptDesig = function (params) {
            var url = fwddomain + 'saveuserdeptdesig';
            return httpServices.post(url, params);
        };

        fwdauctions.GetReqDeptDesig = function (userid, reqid, sessionid) {
            var url = fwddomain + 'getreqdeptdesig?userid=' + userid + '&reqid=' + reqid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        fwdauctions.SaveReqDeptDesig = function (params) {
            var url = fwddomain + 'savereqdeptdesig';
            return httpServices.post(url, params);
        };

        fwdauctions.SaveUserDepartments = function (params) {
            var url = fwddomain + 'saveuserdepartments';
            return httpServices.post(url, params);
        };

        fwdauctions.GetDepartmentUsers = function (deptid, userid, sessionid) {
            var url = fwddomain + 'getdepartmentusers?deptid=' + deptid + '&userid=' + userid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        fwdauctions.GetReqDepartments = function (userid, reqid, sessionid) {
            let url = fwddomain + 'getreqdepartments?userid=' + userid + '&reqid=' + reqid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        fwdauctions.SaveReqDepartments = function (params) {
            let url = fwddomain + 'savereqdepartments';
            return httpServices.post(url, params);
        };

        fwdauctions.AssignVendorToCompany = function (params) {
            let url = fwddomain + 'assignvendortocompany';
            return httpServices.post(url, params);
        };

        fwdauctions.GetCompanyConfiguration = function (compid, configkey, sessionid) {
            let url = fwddomain + 'getcompanyconfiguration?compid=' + compid + '&configkey=' + configkey + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        fwdauctions.GetIsAuthorized = function (userid, reqid, sessionid) {
            let url = fwddomain + 'getisauthorized?userid=' + userid + '&reqid=' + reqid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        fwdauctions.materialdispatch = function (params) {
            var data = {
                reqMat: params
            };
            let url = fwddomain + 'materialdispatch';
            return httpServices.post(url, data);
        };

        fwdauctions.materialReceived = function (params) {
            var data = {
                materialreceived: params
            };
            let url = fwddomain + 'materialreceived';
            return httpServices.post(url, data);
        };

        fwdauctions.paymentdetails = function (params) {
            var data = {
                paymentDets: params
            };
            let url = fwddomain + 'paymentdetails';
            return httpServices.post(url, data);
        };

        fwdauctions.generatePOinServer = function (params) {
            let url = fwddomain + 'generatepo';
            return httpServices.post(url, params);
        };

        fwdauctions.getDashboardStats = function (params) {
            let url = fwddomain + 'getdashboardstats?userid=' + params.userid + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        fwdauctions.getCategories = function (params) {
            let url = fwddomain + 'getcategories?userid=' + (params ? params : -1);
            return httpServices.get(url);
        };

        fwdauctions.getKeyValuePairs = function (params) {
            let url = fwddomain + 'getkeyvaluepairs?parameter=' + params;
            return httpServices.get(url);
        };

        fwdauctions.getmyAuctions = function (params) {
            let url = fwddomain + 'getmyauctions?userid=' + params.userid + "&sessionid=" + params.sessionid;
            return httpServices.get(url);
        };

        fwdauctions.getactiveleads = function (params) {
            let url = fwddomain + 'getactiveleads?userid=' + params.userid + "&sessionid=" + params.sessionid;
            return httpServices.get(url);
        };

        fwdauctions.getrequirementdata = function (params) {
            let url = fwddomain + 'getrequirementdata?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid;
            return httpServices.get(url);
        };

        fwdauctions.GetReportsRequirement = function (params) {
            let url = fwddomain + 'getreportsrequirement?reqid=' + params.reqid + "&userid=" + params.userid + "&sessionid=" + params.sessionid;
            return httpServices.get(url);
        };

        fwdauctions.GetVendorReminders = function (params) {
            let url = fwddomain + 'getvendorreminders?reqid=' + params.reqid + "&userid=" + params.userid + "&sessionid=" + params.sessionid;
            return httpServices.get(url);
        };

        fwdauctions.GetBidHistory = function (params) {
            let url = fwddomain + 'getbidhistory?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid;
            return httpServices.get(url);
        };

        fwdauctions.getpodetails = function (params) {
            let url = fwddomain + 'getpodetails?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid;
            return httpServices.get(url);
        };

        fwdauctions.getcomments = function (params) {
            let url = fwddomain + 'getcomments?reqid=' + params.reqid + "&userID=" + params.userid + "&sessionid=" + params.sessionid;
            return httpServices.get(url);
        };

        fwdauctions.getpricecomparison = function (params) {
            let url = fwddomain + 'getpricecomparison?reqid=' + params.reqid + "&userID=" + params.userid + "&sessionid=" + params.sessionid;
            return httpServices.get(url);
        };

        fwdauctions.GetPriceComparisonPreNegotiation = function (params) {
            let url = fwddomain + 'getpricecomparisonprenegotiation?reqid=' + params.reqid + "&userID=" + params.userid + "&sessionid=" + params.sessionid;
            return httpServices.get(url);
        };

        fwdauctions.savecomment = function (params) {
            var comment = {
                "com": {
                    "requirementID": params.reqID,
                    "firstName": "",
                    "lastName": "",
                    "userID": params.userID,
                    "commentText": params.commentText,
                    "replyCommentID": -1,
                    "commentID": -1,
                    "errorMessage": "",
                    "sessionID": params.sessionID
                }
            };

            let url = fwddomain + 'savecomment';
            return httpServices.post(url, comment);
        };


        fwdauctions.deleteAttachment = function (Attaachmentparams) {
            let url = fwddomain + 'deleteattachment';
            return httpServices.post(url, Attaachmentparams);
        };


        fwdauctions.postrequirementdata = function (params) {
            var myDate = new Date(); // Your timezone!
            var myEpoch = parseInt(myDate.getTime() / 1000);
            params.minBidAmount = 0;
            params.postedOn = "/Date(" + myEpoch + "000+0000)/";
            params.timeLeft = -1;
            params.price = -1;
            var requirement = {
                "requirement": {
                    "title": params.title,
                    "description": params.description,
                    "category": params.category,
                    "subcategories": params.subcategories,
                    "urgency": params.urgency,
                    "budget": params.budget,
                    "attachmentName": params.attachmentName,
                    "deliveryLocation": params.deliveryLocation,
                    "taxes": params.taxes,
                    "paymentTerms": params.paymentTerms,
                    "requirementID": params.requirementID,
                    "customerID": params.customerID,
                    "isClosed": params.isClosed,
                    "endTime": null,
                    "sessionID": params.sessionID,
                    "errorMessage": "",
                    "timeLeft": -1,
                    "price": -1,
                    "auctionVendors": params.auctionVendors,
                    "startTime": null,
                    "status": "",
                    "postedOn": "/Date(" + myEpoch + "000+0000)/",
                    "custLastName": params.customerLastname,
                    "custFirstName": params.customerFirstname,
                    "deliveryTime": params.deliveryTime,
                    "includeFreight": params.includeFreight,
                    "inclusiveTax": params.inclusiveTax,
                    "minBidAmount": 0,
                    "checkBoxEmail": params.checkBoxEmail,
                    "checkBoxSms": params.checkBoxSms,
                    "quotationFreezTime": params.quotationFreezTime,
                    "currency": params.currency,
                    "timeZoneID": params.timeZoneID,
                    "listRequirementItems": params.listRequirementItems,
                    "isTabular": params.isTabular,
                    "reqComments": params.reqComments,
                    "itemsAttachment": params.itemsAttachment,
                    "isSubmit": params.isSubmit,
                    "isQuotationPriceLimit": params.isQuotationPriceLimit,
                    "quotationPriceLimit": params.quotationPriceLimit,
                    "noOfQuotationReminders": params.noOfQuotationReminders,
                    "remindersTimeInterval": params.remindersTimeInterval,
                    "custCompID": params.custCompID,
                    "customerCompanyName": params.customerCompanyName,
                    "deleteQuotations": params.deleteQuotations,
                    "indentID": params.indentID,
                    "expStartTime": params.expStartTime
                }, "attachment": params.attachment
            };
            let url = fwddomain + 'requirementsave';
            return httpServices.post(url, requirement);
        };

        fwdauctions.updatebidtime = function (params) {
            let url = fwddomain + 'updatebidtime';
            return httpServices.post(url, params);
        };

        fwdauctions.makeabid = function (params) {
            let url = fwddomain + 'makeabid';
            return httpServices.post(url, params);
        };

        fwdauctions.uploadQuotation = function (params) {
            let url = fwddomain + 'uploadquotation';
            return httpServices.post(url, params);
        };

        fwdauctions.revquotationupload = function (params) {
            let url = fwddomain + 'revquotationupload';
            return httpServices.post(url, params);
        };

        fwdauctions.QuatationAprovel = function (params) {
            let url = fwddomain + 'quatationaprovel';
            return httpServices.post(url, params);
        };

        fwdauctions.savepricecap = function (params) {
            let url = fwddomain + 'savepricecap';
            return httpServices.post(url, params);
        };

        fwdauctions.GetCompanyLeads = function (params) {
            let url = fwddomain + 'getcompanyleads?userid=' + params.userid + '&searchstring=' + params.searchstring + '&searchtype=' + params.searchtype + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        fwdauctions.updateauctionstart = function (params) {
            let url = fwddomain + 'updateauctionstart';
            return httpServices.post(url, params);
        };

        fwdauctions.GetMaterialReceivedData = function (params) {
            let url = fwddomain + 'getmaterialreceiveddata?reqid=' + params.reqid + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        fwdauctions.SaveCompanyCategories = function (params) {
            let url = fwddomain + 'savecompanycategories';
            return httpServices.post(url, params);
        };

        fwdauctions.GetCompanyCategories = function (compID, sessionID) {
            let url = fwddomain + 'getcompanycategories?compid=' + compID + '&sessionid=' + sessionID;
            return httpServices.get(url);
        };

        fwdauctions.newDetails = function (params) {
            let url = fwddomain + 'savealternatecontacts';
            return httpServices.post(url, params);
        };

        fwdauctions.getalternatecontacts = function (userid, compid, sessionID) {
            let url = fwddomain + 'getalternatecontacts?userid=' + userid + '&compid=' + compid + '&sessionid=' + sessionID;
            return httpServices.get(url);
        };

        fwdauctions.SaveRequirementTerms = function (params) {
            let url = fwddomain + 'saverequirementterms';
            return httpServices.post(url, params);
        };

        fwdauctions.GetRequirementTerms = function (userid, reqid, sessionID) {
            let url = fwddomain + 'getrequirementterms?userid=' + userid + '&reqid=' + reqid + '&sessionid=' + sessionID;
            return httpServices.get(url);
        };

        fwdauctions.DeleteRequirementTerms = function (params) {
            let url = fwddomain + 'deleterequirementterms';
            return httpServices.post(url, params);
        };

        fwdauctions.saveCIJ = function (params) {
            let url = fwddomain + 'savecij';
            return httpServices.post(url, params);
        };

        fwdauctions.GetCIJ = function (cijid, sessionid) {
            let url = fwddomain + 'getcij?cijid=' + cijid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        fwdauctions.GetIndentDetails = function (indentID, sessionid) {
            let url = fwddomain + 'getindentdetails?indentid=' + indentID + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        fwdauctions.SaveIndentDetails = function (params) {
            let url = fwddomain + 'saveindentdetails';
            return httpServices.post(url, params);
        };

        fwdauctions.GetCIJList = function (compid, sessionid) {
            let url = fwddomain + 'getcijlist?compid=' + compid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        fwdauctions.GetIndentList = function (compid, sessionid) {
            let url = fwddomain + 'getindentlist?compid=' + compid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        fwdauctions.saveFileUpload = function (params) {
            let url = fwddomain + 'savefileupload';
            return httpServices.post(url, params);
        };

        fwdauctions.GetUserDetails = function (userid, sessionid) {
            let url = fwddomain + 'getuserinfo?userid=' + userid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        return fwdauctions;
    }]);