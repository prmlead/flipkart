prmApp

    .service('rfqService', ["rfqDomain", "userService", "httpServices", function (rfqDomain, userService, httpServices) {
        //var domain = 'http://182.18.169.32/services/';
        var rfqService = this;
        
        //storeService.savestore = function (params) {
        //    let url = storeDomain + 'savestore';
        //    return httpServices.post(url, params);
        //};
        rfqService.GetRFQCIJList = function (userid, sessionid) {

            var url = rfqDomain + 'getrfqcijlist?userid=' + userid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        rfqService.GetRFQCreators = function (indentid) {

            var url = rfqDomain + 'getrfqcreators?indentid=' + indentid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        rfqService.GetRFQIndentList = function (userid, sessionid) {

            var url = rfqDomain + 'getrfqindentlist?userid=' + userid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

       
        return rfqService;
    }]);