﻿prmApp
    // =========================================================================
    // COMMON FORMS
    // =========================================================================

    .controller('threeWayMatchingCtrl', ["$rootScope", "$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMPOService", "poService",
        "PRMCustomFieldService", "fileReader", "PRMUploadServices","PRMJeevesOracleService",
        function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPOService, poService, PRMCustomFieldService, fileReader, PRMUploadServices, PRMJeevesOracleService) {


            /*PAGINATION CODE*/
            
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSizeMatch = 10;
            $scope.maxSize = 5;
            $scope.userId = userService.getUserId();

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
                $scope.getThreeWayMatchingList(($scope.currentPage - 1), 10, $scope.filters.searchKeyword);
            };

            $scope.compId = userService.getUserCompanyId();
            $scope.isVendor = (userService.getUserType() == "VENDOR") ? true : false;
            $scope.userID = userService.getUserId();

            $scope.filters = {
                searchKeyword: '',
                //fromDate: moment().subtract(30, "days").format("YYYY-MM-DD"),
                toDate: moment().format('YYYY-MM-DD'),
            }


            /*PAGINATION CODE*/
            $scope.loadServices = false;
            $scope.setFilters = function (currentPage) {
                if ($scope.loadServices) {
                    $scope.getThreeWayMatchingList(0, 10, $scope.filters.searchKeyword);
                }
            };
            $scope.filterByDate = function () {
                $scope.getThreeWayMatchingList(0, 10, $scope.filters.searchKeyword);
            };

            $scope.threeWayMatchingList = [];
            $scope.totalItems = 0;

            $scope.getThreeWayMatchingList = function (recordsFetchFrom, pageSize, searchString) {
               var params = {
                    "compid": $scope.compId,
                    "userId": $scope.userId,
                    "search": searchString ? searchString : "",
                    "fromDate": "2000-01-01",
                    "toDate": $scope.filters.toDate,
                    "PageSize": recordsFetchFrom * pageSize,
                    "NumberOfRecords": pageSize,
                    "sessionID": userService.getUserToken()
                };
                $scope.pageSizeTemp = (params.PageSize + 1);
                $scope.NumberOfRecords = ((recordsFetchFrom + 1) * pageSize);

                PRMPOService.getThreeWayMatchingList(params)
                    .then(function (response) {
                        $scope.loadServices = true;
                        if (response && response.length > 0) {
                            $scope.threeWayMatchingList = response;
                            $scope.totalItems = $scope.threeWayMatchingList[0].TOTAL_COUNT;
                            $scope.threeWayMatchingList.forEach(function (item) {
                                if (item.IS_ERROR == 0) {
                                    item.displayColor = '';
                                } else {
                                    item.displayColor = 'orangered';
                                }
                            })
                        } else {
                            $scope.threeWayMatchingList = [];
                            $scope.totalItems = 0;
                        }
                    });
            };



            $scope.processOracleFiles = function () {

                
                PRMJeevesOracleService.GetInvoiceOracleDetails()
                    .then(function (response) {
                        if(response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("Saved Successfully.", "success");
                        }
                    });

            }
            $scope.invoiceDetails = [];

            $scope.getInvoiceHeaderLevelList = function (search) {
                $scope.params = {
                    "compID": !$scope.isVendor ? +$scope.compId : 0,
                    "uID": !$scope.isVendor ? 0 : +$scope.userID,
                    "fromDate": "2000-01-01",
                    "toDate": $scope.filters.toDate,
                    "search": search
                }

                PRMPOService.getInvoiceHeaderLevelList($scope.params)
                    .then(function (response) {
                        $scope.invoiceDetails = response;
                        if ($scope.invoiceDetails.length > 0) {
                            
                        }
                    });

            }
            $scope.getThreeWayMatchingList(0, 10, $scope.filters.searchKeyword);


            $scope.viewGRN = function (grnNo) {
                var url = $state.href('list-ViewGRN', { "grnNo": grnNo, "isGRN": false });

                $window.open(url, '_blank');
            };

            $scope.viewInvoice = function (invoice) {
                var url = $state.href("createInvoice", { "poNumber": invoice.PO_NUMBER, "invoiceNumber": invoice.INVOICE_NUMBER, "invoiceID": invoice.INVOICE_ID });
                $window.open(url, '_blank');
            };


        }]);