﻿prmApp

    .controller('myIndentsCtrl', ["$scope", "$http", "$state", "domain", "$filter", "$window", "$log", "$stateParams", "$timeout", "auctionsService", "fwdauctionsService", "userService", "SignalRFactory", "fileReader", "growlService", "workflowService",
        function ($scope, $http, $state, domain, $filter, $window, $log, $stateParams, $timeout, auctionsService, fwdauctionsService, userService, SignalRFactory, fileReader, growlService, workflowService) {

            $scope.compID = userService.getUserCompanyId();
            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();

            $scope.indentType = 'CP';

            $scope.indentStatus = 0;

            $scope.GetMyIndents = function () {
                workflowService.GetMyIndents($scope.userID)
                    .then(function (response) {
                        $scope.listIndents = response;

                        $scope.changeFunction('CP');

                        console.log($scope.listIndents);


                    })
            }

            $scope.GetMyIndents();


            $scope.cancelClick = function () {
                $window.history.back();
            }

            $scope.changeFunction = function (val) {
                $scope.listIndentsFilter = [];

                $scope.listIndents.forEach(function (item, index) {
                    if (item.reqID == 0 && val == 'CP') {
                        $scope.listIndentsFilter.push(item);
                    }
                    if (item.reqID > 0 && val == 'CC') {
                        $scope.listIndentsFilter.push(item);
                    }

                    if (item.reqID == 0 && item.userID == $scope.userID && val == 'MP') {
                        $scope.listIndentsFilter.push(item);
                    }
                    if (item.reqID > 0 && item.userID == $scope.userID && val == 'MC') {
                        $scope.listIndentsFilter.push(item);
                    }
                })
            }


            $scope.goToIndent = function (id) {
                var url = $state.href('materialsPurchaseIndent', { "indentID": id });
                window.open(url, '_blank');
            }


            $scope.goToIndentView = function (id) {
                var url = $state.href('materialPurchaseIndentView', { "indentID": id });
                window.open(url, '_blank');
            }


            $scope.goToReq = function (id) {
                var url = $state.href('view-requirement', { "Id": id });
                window.open(url, '_blank');
            }


        }]);