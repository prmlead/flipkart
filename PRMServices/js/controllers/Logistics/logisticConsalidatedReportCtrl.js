﻿
prmApp
    .controller('logisticConsalidatedReportCtrl', ["$timeout", "$uibModal", "$state", "$window", "$scope", "growlService", "userService", "auctionsService", "fwdauctionsService", "$http", "domain", "$rootScope", "fileReader", "$filter", "$log", "reportingService",
        function ($timeout, $uibModal, $state, $window, $scope, growlService, userService, auctionsService, fwdauctionsService, $http, domain, $rootScope, fileReader, $filter, $log, reportingService) {
            $scope.formRequest = {};
            $scope.formRequest.isForwardBidding = false;
            $scope.logisticConsalidatedReport = [];
            /*pagination code*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 8;

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
            };

            //$scope.reportFromDate = '';
            //$scope.reportToDate = '';

            $scope.reportToDate = moment().format('YYYY-MM-DD');
            $scope.reportFromDate = moment().subtract(30, "days").format("YYYY-MM-DD");
           
           
            $scope.getLogisticConsalidatedReport = function () {
                $scope.errMessage = '';
                
                

                reportingService.getLogisticConsalidatedReport($scope.reportFromDate, $scope.reportToDate)
                    .then(function (response) {
                        $scope.logisticConsalidatedReport = response;
                       
                        $scope.totalItems = $scope.logisticConsalidatedReport.length;
                        $scope.logisticConsalidatedReport.forEach(function (item, index) {
                            item.quotationFreezTime = $scope.GetDateconverted(item.quotationFreezTime);
                            item.reqPostedOn = $scope.GetDateconverted(item.reqPostedOn);
                            item.startTime = $scope.GetDateconverted(item.startTime);

                            if (String(item.startTime).includes('9999')) {
                                item.startTime = '';
                            }

                        })

                    })




            }

            $scope.getLogisticConsalidatedReport();

            $scope.GetReport = function () {
                //alasql.fn.handleDate = function (date) {
                //    return new moment(date).format("MM/DD/YYYY");
                //}
                
                alasql('SELECT requirementID as [Requirement ID],title as [Requirement Title],reqPostedOn as [Posted On], invoiceNumber as [Invoice Number], ' +
                    'invoiceDate as [Invoice Date], consigneeName as [Consignee Name], destination as Destination, ' +
                    'productName as [Product Name], shippingMode as [Shipping Mode AIR/SEA], ' +
                    'qty as [Quantity], chargeableWt as [Chargeable Wt], dgFee as [DG Fee], ams as [AMS], misc as[MISC], menzinesCharges as [Menzines Charges], ' +
                    'chaCharges as [CHA Charges],savings as Savings, freightBidAmount as [Freight/Bid Amount], grandTotalas [Grand Total]' +
                    'INTO XLSX(?, { headers: true, sheetid: "LogisticConsolidatedReport", style: "font-size:15px;background:#233646;color:#FFF;" }) FROM ? ',
                    ["LogisticConsolidatedReport.xlsx", $scope.logisticConsalidatedReport]);

            }
            $scope.GetDateconverted = function (dateBefore) {
                if (dateBefore) {
                    return new moment(dateBefore).format("DD-MM-YYYY HH:mm");
                }
            };


        }]);