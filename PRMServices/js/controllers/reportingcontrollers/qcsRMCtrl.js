﻿prmApp
    .controller('qcsRMCtrl', ["$scope", "$state", "$log", "$stateParams", "userService", "auctionsService", "$window",
        "$timeout", "reportingService", "growlService",
        function ($scope, $state, $log, $stateParams, userService, auctionsService, $window,
            $timeout, reportingService, growlService) {

            $scope.reqId = $stateParams.reqID;
            $scope.itemid = $stateParams.itemID;
            $scope.qcsID = $stateParams.qcsID;
            $scope.isUOMDifferent = false;
            $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;
            if (!$scope.isCustomer) {
                $state.go('home');
            };

            $scope.userId = userService.getUserId();
            $scope.sessionid = userService.getUserToken();

            $scope.editForm = false;

            $scope.tableName1 = '1.PIPELINE STOCK(U / M)';
            $scope.tableName2 = '2.ARRIVAL SCHEDULE ( ON ORDER & PLANNED QTY )';
            $scope.tableName3 = '3.CONSUMPTION PLANNED & CLOSING INVENTORIES';
            $scope.tableName4 = '4.COST COMPARISION';
            $scope.tableName5 = '5.BENCHMARK PRICE (LAST 3 MONTHS & CURRENT)';
            $scope.tableName6 = '6.FINAL RECOMMENDATION:  ANNUAL / SPECIFIC REQUIREMENT :(KG)';

            $scope.listRequirementTaxes = [];
            $scope.listRequirementTaxeslength = $scope.listRequirementTaxes.length;
            $scope.uomDetails = [];

            $scope.Plant = 0;
            $scope.Transit = 0;
            $scope.Port = 0;
            $scope.OnOrder = 0;
            $scope.TotalCover = 0;
            $scope.MANUFACTURER = '';
            $scope.PAYMENT_TERMS = '';
            $scope.DELIVERY = '';
            $scope.GST = 0;
            $scope.GSTVAL = 0;
            $scope.total = 0;
            $scope.NOMPRICE = 0;
            $scope.GST_VALUE = 12;
            $scope.PAYMENT_TERMS_VALUE = 60;
            $scope.SOB_TOTAL_PERCENTAGE = 0;

            $scope.MONTH = '';
            $scope.MONTH1 = '';
            $scope.MONTH2 = '';
            $scope.PRE_MONTH1 = '';
            $scope.PRE_MONTH2 = '';
            $scope.PRE_MONTH3 = '';
            $scope.CURRENCY_USD = 68;
            $scope.color = '';

            $scope.NOM_VALUE_LEAST = [];

            $scope.NOM_VALUE_LEAST_VALUE = 0;

            $scope.RMQCSValues = function (manufacturer, clearance, inventory, lcbg, intrestoncredit) {

                $scope.QCSDetails = {
                    QCS_ID: 0,
                    U_ID: userService.getUserId(),
                    QCS_CODE: '',
                    PO_CODE: '',
                    RECOMMENDATIONS: '',
                    UNIT_CODE: '',
                    MANUFACTURER: manufacturer,
                    CLEARANCE: clearance,
                    INVENTORY_CARRYING_COST: inventory,
                    LC_BG_COST: lcbg,
                    INTEREST_ON_CREDIT: intrestoncredit
                }


            }


            $scope.currentMonth = function () {
                var month = new Array();
                month[0] = "JANUARY";
                month[1] = "FEBRUARY";
                month[2] = "MARCH";
                month[3] = "APRIL";
                month[4] = "MAY";
                month[5] = "JUNE";
                month[6] = "JULY";
                month[7] = "AUGUST";
                month[8] = "SEPTEMBER";
                month[9] = "OCTOBER";
                month[10] = "NOVEMBOR";
                month[11] = "DECEMBER";
                // var t = $scope.ReqReportForExcel.DATETIME_NOW;
                var ts = $scope.ReqReportForExcel.DATETIME_NOW;
                var m = moment(ts);
                var d = new Date(m);
                var n = month[d.getMonth()];
                $scope.MONTH = n;

                var M1 = month[d.getMonth() + 1];
                $scope.MONTH1 = M1;

                var M2 = month[d.getMonth() + 2];
                $scope.MONTH2 = M2;

                var PreviousMonth1 = month[d.getMonth() - 1];
                $scope.PRE_MONTH1 = PreviousMonth1;

                var PreviousMonth2 = month[d.getMonth() - 2];
                $scope.PRE_MONTH2 = PreviousMonth2;

                var PreviousMonth3 = month[d.getMonth() - 3];
                $scope.PRE_MONTH3 = PreviousMonth3;


            };


            $scope.handleUOMItems = function (item) {
                if (item) {
                    item.reqVendors.forEach(function (vendor, index) {
                        var uomDetailsObj = {
                            itemId: item.itemID,
                            vendorId: vendor.vendorID,
                            containsUOMItem: false
                        }

                        if (vendor.quotationPrices) {
                            uomDetailsObj.containsUOMItem = item.productQuantityIn.toUpperCase() != vendor.quotationPrices.vendorUnits.toUpperCase();
                        }

                        $scope.uomDetails.push(uomDetailsObj);
                    });
                }
            }

            $scope.containsUOMITems = function (vendorId) {
                var filteredUOMitems = _.filter($scope.uomDetails, function (uomItem) { return uomItem.vendorId == vendorId && uomItem.containsUOMItem == true; });
                if (filteredUOMitems && filteredUOMitems.length > 0) {
                    return true;
                }

                return false;
            }

            $scope.ReqReportForExcel = [];
            $scope.GetRMQCSReportForExcel = function () {
                reportingService.GetRMQCSReportForExcel($scope.reqId, $scope.itemid, userService.getUserToken())
                    .then(function (response) {
                        $scope.ReqReportForExcel = response;
                        // // // #INTERNATIONALIZATION-0-2019-02-12-0-2019-02-12
                        $scope.ReqReportForExcel.currentDate = userService.toLocalDate($scope.ReqReportForExcel.currentDate);
                        $scope.ReqReportForExcel.postedOn = userService.toLocalDate($scope.ReqReportForExcel.postedOn);
                        $scope.ReqReportForExcel.startTime = userService.toLocalDate($scope.ReqReportForExcel.startTime);

                        $scope.currentMonth();

                        $scope.ReqReportForExcel.reqItems.forEach(function (item, itemIndex) {



                            $scope.handleUOMItems(item)



                            item.reqVendors.forEach(function (vendor, vendorindex) {

                                vendor.UP = vendor.quotationPrices.unitPrice;
                                vendor.RUP = vendor.quotationPrices.revUnitPrice;
                                vendor.RUPCB = vendor.quotationPrices.revUnitPriceCB;

                                if ($scope.ReqReportForExcel.status == "Negotiation Ended") {

                                    vendor.UNIT_PRICE = vendor.RUP;

                                } else if ($scope.ReqReportForExcel.status == "NOTSTARTED" || $scope.ReqReportForExcel.status == "UNCONFIRMED") {
                                    vendor.UNIT_PRICE = vendor.UP;
                                }
                                //else if ($scope.ReqReportForExcel.status == "Negotiation Ended" && $scope.ReqReportForExcel.IS_CB_ENABLED == 1) {
                                //    vendor.UNIT_PRICE = vendor.RUPCB;
                                //}

                                vendor.FC = vendor.freightChargesWithTax
                                vendor.RFC = vendor.revfreightChargesWithTax

                                vendor.DRUM_REALISATION = 2;
                                if ($scope.ReqReportForExcel.status == "Negotiation Ended") {
                                    if (item.QCS_ITEM_ID == 0) {

                                        vendor.FREIGHT = (parseFloat(vendor.RFC));
                                    }
                                } else if ($scope.ReqReportForExcel.status == "NOTSTARTED" || $scope.ReqReportForExcel.status == "UNCONFIRMED") {
                                    if (item.QCS_ITEM_ID == 0) {

                                        vendor.FREIGHT = (parseFloat(vendor.FC));
                                    }
                                }


                                if (item.QCS_ITEM_ID > 0) {
                                    $scope.GST_VALUE = item.GST_VALUE;
                                    $scope.PAYMENT_TERMS_VALUE = item.PAYMENT_TERMS_VALUE;

                                    item.CURRENT_MONTH_ORDER = item.ON_ORDER;
                                }



                            })

                            //#region ITEM_LEVEL
                            item.reqVendors.forEach(function (vendor, vendorIndex) {
                                if (vendor.quotationPrices) {
                                    vendor.quotationPrices.Gst = vendor.quotationPrices.cGst + vendor.quotationPrices.sGst + vendor.quotationPrices.iGst;
                                    vendor.quotationPrices.revGstAmount = 0;
                                    vendor.quotationPrices.revGstAmount = ((vendor.quotationPrices.revUnitPrice / 100) * (vendor.quotationPrices.Gst));
                                    vendor.quotationPrices.revItemPriceWithoutTax = 0;
                                    vendor.quotationPrices.revItemPriceWithoutTax = ((vendor.quotationPrices.revUnitPrice) * (item.productQuantity));

                                    vendor.packingChargesGstAmount = ((vendor.revpackingCharges / 100) * (vendor.packingChargesTaxPercentage));
                                    vendor.freightChargesGstAmount = ((vendor.revfreightCharges / 100) * (vendor.freightChargesTaxPercentage));
                                    vendor.installationChargesGstAmount = ((vendor.revinstallationCharges / 100) * (vendor.installationChargesTaxPercentage));

                                }
                            })
                            //#endregion ITEM_LEVEL

                            //#region SUB_TOTAL(Overall) and GST_AMOUNT(Item Level)

                            item.reqVendors.forEach(function (vendor, vendorIndex) {


                                //#region SUB_TOTAL_WITHOUT_TAX
                                if (item && item.reqVendors && item.reqVendors[vendorIndex] && item.reqVendors[vendorIndex].quotationPrices && item.reqVendors[vendorIndex].quotationPrices.revitemPrice) {
                                    if ($scope.ReqReportForExcel.reqItems[0].reqVendors[vendorIndex].revVendorSubTotalWithoutGST > 0) { } else { $scope.ReqReportForExcel.reqItems[0].reqVendors[vendorIndex].revVendorSubTotalWithoutGST = 0 }
                                    $scope.ReqReportForExcel.reqItems[0].reqVendors[vendorIndex].revVendorSubTotalWithoutGST += item.reqVendors[vendorIndex].quotationPrices.revUnitPrice * item.productQuantity;
                                }
                                //#endregion SUB_TOTAL_WITHOUT_TAX

                                //#region GST_AMOUNT_WITH_QUANTITY
                                if (item && item.reqVendors && item.reqVendors[vendorIndex] && item.reqVendors[vendorIndex].quotationPrices && item.reqVendors[vendorIndex].quotationPrices.revGstAmount) {
                                    if ($scope.ReqReportForExcel.reqItems[0].reqVendors[vendorIndex].revVendorGstAmountWithQuantity > 0) { } else { $scope.ReqReportForExcel.reqItems[0].reqVendors[vendorIndex].revVendorGstAmountWithQuantity = 0 }
                                    $scope.ReqReportForExcel.reqItems[0].reqVendors[vendorIndex].revVendorGstAmountWithQuantity += item.reqVendors[vendorIndex].quotationPrices.revGstAmount * item.productQuantity;
                                }
                                //#endregion GST_AMOUNT_WITH_QUANTITY



                            })

                            //#endregion SUB_TOTAL(Overall) and GST_AMOUNT(Item Level)

                            //#region VENDOR_NOM(SUB_TOTAL+FREIGHT+INSTALLATION+PACKING)
                            $scope.ReqReportForExcel.reqItems[0].reqVendors.forEach(function (vendor, vendorIndex) {
                                vendor.NOM = vendor.revVendorSubTotalWithoutGST +
                                    vendor.revpackingCharges +
                                    vendor.revfreightCharges +
                                    vendor.revinstallationCharges;
                            })
                            //#endregion VENDOR_NOM(SUB_TOTAL+FREIGHT+INSTALLATION+PACKING)

                            //#region VENDOR_GST_AMOUNT
                            $scope.ReqReportForExcel.reqItems[0].reqVendors.forEach(function (vendor, vendorIndex) {
                                vendor.totalTAX = vendor.revVendorGstAmountWithQuantity +
                                    vendor.packingChargesGstAmount +
                                    vendor.freightChargesGstAmount +
                                    vendor.installationChargesGstAmount;
                            })
                            //#endregion VENDOR_GST_AMOUNT

                            $scope.ReqReportForExcel.reqItems[0].reqVendors.forEach(function (vendor, vendorIndex) {



                                $scope.RMCalculations($scope.GST_VALUE, $scope.PAYMENT_TERMS_VALUE);

                                //vendor.FREIGHT = (parseFloat(vendor.revfreightChargesWithTax));

                                //vendor.GST = (parseFloat(vendor.quotationPrices.iGst) + parseFloat(vendor.quotationPrices.cGst) + parseFloat(vendor.quotationPrices.sGst));

                                //vendor.GST_AMOUNT = (((vendor.quotationPrices.unitPrice) * (vendor.GST)) / 100);

                                //vendor.total = (parseFloat(vendor.quotationPrices.unitPrice) + 0 + vendor.GST_AMOUNT + vendor.CLEARANCE + vendor.FREIGHT + vendor.INVENTORY_CARRYING_COST);

                                //vendor.IGST_MODEVAT = vendor.GST_AMOUNT;

                                //vendor.LANDED_COST = ((vendor.total) - (vendor.IGST_MODEVAT) - (vendor.DRUM_REALISATION));

                                //vendor.PRICE_NOM = vendor.LANDED_COST;



                                //vendor.INTEREST_ON_CREDIT = ((vendor.total) * (((vendor.total) / 100) * ($scope.GST_VALUE))) / (365 * 60);

                                //vendor.NET_COST_WITH_FIM_IMPACT = (vendor.LANDED_COST - vendor.INTEREST_ON_CREDIT);

                                //vendor.NET_COST_W_F_I = vendor.NET_COST_WITH_FIM_IMPACT;

                                //vendor.TOTAL_NOM_VALUE = ((parseFloat(vendor.quotationPrices.unitPrice) + parseFloat(vendor.CLEARANCE) + vendor.FREIGHT + parseFloat(vendor.INVENTORY_CARRYING_COST)) * (parseFloat($scope.ReqReportForExcel.reqItems[0].productQuantity)));

                                //vendor.TOTAL_VALUE_CASH_FLOW = ((parseFloat(vendor.quotationPrices.unitPrice) + vendor.GST_AMOUNT + vendor.FREIGHT) * ($scope.ReqReportForExcel.reqItems[0].productQuantity));
                            })

                            // $scope.NOMPRICE = item.reqVendors[0].PRICE_NOM;
                        });
                    });
            }

            $scope.GetRMQCSReportForExcel();

            $scope.doPrint = false;

            $scope.printReport = function () {
                $scope.doPrint = true;
                $timeout(function () {
                    $window.print();
                    $scope.doPrint = false
                }, 1000);
            }


            $scope.htmlToCanvasSaveLoading = false;

            $scope.htmlToCanvasSave = function (format) {
                $scope.htmlToCanvasSaveLoading = true;
                setTimeout(function () {
                    try {
                        var name = "Comparisions-ReqID-" + $scope.reqId + "." + format;
                        var canvas = document.createElement("canvas");
                        if (format == 'pdf') {
                            document.getElementById("widget").style["display"] = "";
                        }
                        else {
                            document.getElementById("widget").style["display"] = "inline-block";
                        }

                        html2canvas($("#widget"), {
                            onrendered: function (canvas) {
                                theCanvas = canvas;

                                const a = document.createElement("a");
                                a.style = "display: none";
                                a.href = canvas.toDataURL();

                                // Add Image to HTML
                                //document.body.appendChild(canvas);

                                /* Save As PDF */
                                if (format == 'pdf') {
                                    var imgData = canvas.toDataURL();
                                    var pdf = new jsPDF();
                                    pdf.addImage(imgData, 'JPEG', 0, 0);
                                    pdf.save(name);
                                }
                                else {
                                    a.download = name;
                                    a.click();
                                }

                                // Clean up 
                                //document.body.removeChild(canvas);
                                document.getElementById("widget").style["display"] = "";
                                $scope.htmlToCanvasSaveLoading = false;
                            }
                        });
                    }
                    catch (err) {
                        document.getElementById("widget").style["display"] = "";
                        $scope.htmlToCanvasSaveLoading = false;
                    }
                    finally {

                    }
                }, 500);

            };

            $scope.RMCalculations = function (gstv, ptv, vID) {
                $scope.TOTAL_PERCENTAGE = 0;
                $scope.ReqReportForExcel.reqItems[0].reqVendors.forEach(function (vend, vendindex) {

                    $scope.TOTAL_PERCENTAGE += parseFloat(vend.SOB);

                    if ($scope.TOTAL_PERCENTAGE > 100) {
                        if (vID == vend.vendorID) {
                            growlService.growl('SOB Quantity exceeded please verify again', "inverse");
                            vend.SOB = 0;
                        }

                        return;
                    }

                })


                $scope.ReqReportForExcel.reqItems[0].GST_VALUE = gstv;
                $scope.ReqReportForExcel.reqItems[0].PAYMENT_TERMS_VALUE = ptv;

                if ($scope.ReqReportForExcel.reqItems[0].GST_VALUE > 100) {

                    growlService.growl('Please enter less then 100', "inverse");
                    $scope.ReqReportForExcel.reqItems[0].GST_VALUE = 0;
                    return;
                }
                $scope.NOM_VALUE_LEAST = [];
                $scope.ReqReportForExcel.reqItems[0].reqVendors.forEach(function (vendor, vendorindex) {


                    vendor.color = '';


                    //$scope.GSTVAL = 12;

                    vendor.GST = (parseFloat(vendor.quotationPrices.iGst) + parseFloat(vendor.quotationPrices.cGst) + parseFloat(vendor.quotationPrices.sGst));

                    vendor.GST_AMOUNT = (((vendor.UNIT_PRICE) * (vendor.GST)) / 100);

                    vendor.CLEARANCE_TEMP = parseFloat(vendor.CLEARANCE);

                    if (vendor.selectedVendorCurrency == 'INR') {
                        vendor.CONVERTED_PRICE = parseFloat(vendor.UNIT_PRICE);
                    }
                    else if (vendor.selectedVendorCurrency == 'USD') {

                        vendor.CONVERTED_PRICE = (parseFloat(vendor.UNIT_PRICE) * $scope.ReqReportForExcel.CURRENCY_RATE_US);

                    } else if (vendor.selectedVendorCurrency == 'EURO') {
                        vendor.CONVERTED_PRICE = (parseFloat(vendor.UNIT_PRICE) * $scope.ReqReportForExcel.CURRENCY_RATE_EURO);
                    }

                    vendor.INVENTORY_CARRYING_COST_TEMP = parseFloat(vendor.INVENTORY_CARRYING_COST);

                    vendor.total = (vendor.CONVERTED_PRICE + vendor.GST_AMOUNT + (vendor.CLEARANCE_TEMP) + parseFloat(vendor.FREIGHT) + vendor.INVENTORY_CARRYING_COST_TEMP);

                    vendor.IGST_MODEVAT = vendor.GST_AMOUNT;

                    vendor.LANDED_COST = ((vendor.total) - (vendor.IGST_MODEVAT) - (vendor.DRUM_REALISATION));

                    vendor.PRICE_NOM = vendor.LANDED_COST;



                    vendor.INTEREST_ON_CREDIT = ((vendor.total) * (($scope.ReqReportForExcel.reqItems[0].GST_VALUE) / 100)) / 365 * ($scope.ReqReportForExcel.reqItems[0].PAYMENT_TERMS_VALUE);

                    vendor.NET_COST_WITH_FIM_IMPACT = (vendor.LANDED_COST - vendor.INTEREST_ON_CREDIT);

                    vendor.NET_COST_W_F_I = vendor.NET_COST_WITH_FIM_IMPACT;

                    vendor.TOTAL_NOM_VALUE = ((vendor.CONVERTED_PRICE + parseFloat(vendor.CLEARANCE_TEMP) + parseFloat(vendor.FREIGHT) + vendor.INVENTORY_CARRYING_COST_TEMP) * (parseFloat($scope.ReqReportForExcel.reqItems[0].productQuantity)));

                    vendor.TOTAL_VALUE_CASH_FLOW = ((vendor.CONVERTED_PRICE + vendor.GST_AMOUNT + parseFloat(vendor.FREIGHT)) * ($scope.ReqReportForExcel.reqItems[0].productQuantity));


                    //vendor.GST_VALUE = gstValue;
                    //vendor.INTEREST_ON_CREDIT = ((vendor.total) * (((vendor.total) / 100) * (vendor.GST_VALUE))) / (365 * 60);

                    //vendor.NET_COST_WITH_FIM_IMPACT = (vendor.LANDED_COST - vendor.INTEREST_ON_CREDIT);

                    //$scope.GSTVAL = gstValue;




                    $scope.SOB_CAL_PERCENTAGE = ($scope.ReqReportForExcel.reqItems[0].productQuantity * (vendor.SOB / 100));

                    vendor.CAL_QTY = $scope.SOB_CAL_PERCENTAGE;
                    vendor.SOB_QTY = vendor.CAL_QTY;

                    $scope.NOM_VALUE_LEAST.push(vendor.LANDED_COST);


                })
                $scope.leastVendor();
            }

            $scope.leastVendor = function (VID) {

                $scope.NOM_VALUE_LEAST_VALUE = _.min($scope.NOM_VALUE_LEAST);
                $scope.ReqReportForExcel.reqItems[0].reqVendors.forEach(function (vendor, vendorindex) {




                    // $scope.valu = _.groupBy($scope.NOM_VALUE_LEAST);

                    console.log($scope.NOM_VALUE_LEAST_VALUE);
                    console.log(vendor.LANDED_COST);


                    if ($scope.NOM_VALUE_LEAST_VALUE == vendor.LANDED_COST) {

                        $scope.landedNom = vendor.LANDED_COST;

                        vendor.color = 'LightGreen';
                        $scope.color = 'LightGreen';

                    }

                })

            }


            $scope.RMQCSCalculations = function (Plant, Transit, Port, OnOrder, Manufacturer, vendID, paymentT, deliverL, gstValue) {



                $scope.ReqReportForExcel.reqItems.forEach(function (item1) {

                    item1.CURRENT_MONTH_ORDER = item1.ON_ORDER;

                    item1.TOTAL_COVER = (parseFloat(item1.PLANT) + parseFloat(item1.TRANSIT) + parseFloat(item1.PORT) + parseFloat(item1.ON_ORDER));

                    item1.TOTAL_ARRIVAL_PLAN = (parseFloat(item1.CURRENT_MONTH_ORDER) + parseFloat(item1.PROPOSAL));

                    item1.TOTAL_ARRIVAL_PLAN1 = (parseFloat(item1.FUTURE_MONTH_ORDER1) + parseFloat(item1.PROPOSAL1));

                    item1.TOTAL_ARRIVAL_PLAN2 = (parseFloat(item1.FUTURE_MONTH_ORDER2) + parseFloat(item1.PROPOSAL2));

                    item1.CLOSING_INVENTORIES = ((item1.TOTAL_ARRIVAL_PLAN + parseFloat(item1.PLANT)) - item1.CONSUMPTION_PLAN);

                    item1.CLOSING_INVENTORIES1 = ((item1.TOTAL_ARRIVAL_PLAN1 + parseFloat(item1.CLOSING_INVENTORIES)) - item1.CONSUMPTION_PLAN1);

                    item1.CLOSING_INVENTORIES2 = ((item1.TOTAL_ARRIVAL_PLAN2 + parseFloat(item1.CLOSING_INVENTORIES1)) - item1.CONSUMPTION_PLAN2);


                    item1.reqVendors.forEach(function (vend) {
                        if (vend.vendorID == vendID) {
                            //vend.MANUFACTURER = Manufacturer;
                            // vend.PAYMENT_TERMS = vend.payment;
                            //vend.DELIVERY = deliverL;



                        }




                        //vend.GST_VALUE = gstValue;
                        //vend.INTEREST_ON_CREDIT = ((vend.total) * (((vend.total) / 100) * (vend.GST_VALUE))) / (365 * 60);

                        //vend.NET_COST_WITH_FIM_IMPACT = (vend.LANDED_COST - vend.INTEREST_ON_CREDIT);

                        //$scope.GSTVAL = gstValue;
                    })

                })
            }
            // $scope.RMQCSCalculations();

            $scope.GetQCSPrices = function (vendID1) {

                $scope.ReqReportForExcel.reqItems.forEach(function (item2) {

                    item2.reqVendors.forEach(function (vend1) {
                        if (vend1.vendorID == vendID1) {
                            vend1.MANUFACTURER = Manufacturer;

                        }



                    })

                })

            }

            $scope.getTotalTax = function (quotation) {
                if (quotation) {
                    if (!quotation.cGst || quotation.cGst == undefined) {
                        quotation.cGst = 0;
                    }

                    if (!quotation.sGst || quotation.sGst == undefined) {
                        quotation.sGst = 0;
                    }

                    if (!quotation.iGst || quotation.iGst == undefined) {
                        quotation.iGst = 0;
                    }

                    return quotation.cGst + quotation.sGst + quotation.iGst;

                } else {
                    return 0;
                }
            }

            $scope.SaveRMQCSDetails = function () {



                if ($scope.ReqReportForExcel.reqItems[0].QCS_CODE == '' || $scope.ReqReportForExcel.reqItems[0].QCS_CODE == null || $scope.ReqReportForExcel.reqItems[0].QCS_CODE == undefined) {
                    growlService.growl('Please enter QCS Number.', "inverse");
                    return;
                }




                var ts = userService.toUTCTicks($scope.ReqReportForExcel.currentDate);
                var m = moment(ts);
                var currentDate = new Date(m);
                var milliseconds = parseInt(currentDate.getTime() / 1000.0);
                $scope.ReqReportForExcel.currentDate = "/Date(" + milliseconds + "000+0530)/";

                var ts = userService.toUTCTicks($scope.ReqReportForExcel.postedOn);
                var m = moment(ts);
                var postedOn = new Date(m);
                var milliseconds = parseInt(postedOn.getTime() / 1000.0);
                $scope.ReqReportForExcel.postedOn = "/Date(" + milliseconds + "000+0530)/";

                var ts = userService.toUTCTicks($scope.ReqReportForExcel.startTime);
                var m = moment(ts);
                var startTime = new Date(m);
                var milliseconds = parseInt(startTime.getTime() / 1000.0);
                $scope.ReqReportForExcel.startTime = "/Date(" + milliseconds + "000+0530)/";


                var params = {
                    "rmqcsdetails": $scope.ReqReportForExcel,
                    "reqId": $scope.reqId,
                    "itemid": $scope.itemid,
                    "U_ID": userService.getUserId(),
                    "sessionid": userService.getUserToken()
                };

                reportingService.SaveRMQCSDetails(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("Details saved successfully.", "success");
                            //$scope.goToQCSList($scope.reqId);
                            location.reload();
                            $scope.editForm = false;
                        }
                    })
            };

            $scope.GetQCSDetails = function () {
                var params = {
                    "qcsid": $stateParams.qcsID,
                    "sessionid": userService.getUserToken()
                };
                reportingService.GetQCSDetails(params)
                    .then(function (response) {
                        $scope.QCSDetails = response;
                    })
            };

            if ($stateParams.qcsID > 0) {
                $scope.GetQCSDetails();
            };

            $scope.goToQCSList = function (reqID) {
                var url = $state.href("list-qcs", { "reqID": $scope.reqId });
                window.open(url, '_self');
            };

        }]);