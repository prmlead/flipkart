﻿prmApp
    .controller('getPendingQuotationsCtrl', ["$rootScope", "$scope", "$stateParams", "$log", "$state", "$window", "userService", "growlService",
        "reportingService", "$location", "$element", "$timeout",
        function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, growlService, reportingService, $location, $element, $timeout) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;
            $scope.compId = userService.getUserCompanyId();
            
            
            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;
            $scope.filters = {
                "fromDate": moment().add('days', -30).format('YYYY-MM-DD'),
                "toDate": moment().format('YYYY-MM-DD')
            };


            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
                $scope.GetList(($scope.currentPage - 1), 10, '');
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };
            /*PAGINATION CODE*/
            $scope.tableColumns = [];
            $scope.GetList = function (recordsFetchFrom, pageSize, searchString, downloadToExcel) {

                var params =
                {
                    "uId": +$scope.userID,
                    "companyId": +$scope.compId,
                    "sessionid": userService.getUserToken(),
                    "fromDate": $scope.filters.fromDate,
                    "toDate": $scope.filters.toDate,
                    "PageSize": recordsFetchFrom * pageSize,
                    "NumberOfRecords": pageSize
                };

                reportingService.GetPendingQuotationsReport(params)
                    .then(function (response) {
                        if (response) {
                            var arr = JSON.parse(response).Table;
                            if (arr && arr.length > 0) {
                                $scope.totalItems = arr[0].TOTAL_COUNT;
                                arr.forEach(a => delete a.TOTAL_COUNT);
                                $scope.tableColumnsTemp = angular.copy(_.keys(arr[0]));
                                $scope.tableColumnsTemp.forEach(function (item, index) {
                                    item = item.replaceAll("_", " ");
                                    var foundIndex = _.findIndex($scope.tableColumns, function (val) { return val === item; });
                                    if (foundIndex <= -1) {
                                        $scope.tableColumns.push(item);
                                    }
                                });
                                $scope.rows = arr;
                                arr.forEach(function (item, index) {
                                    var obj = angular.copy(_.values(item));
                                    if (obj) {
                                        item.tableValues = [];
                                        obj.forEach(function (value, valueIndex) {
                                            item.tableValues.push(value);
                                        });
                                    }
                                });
                            } else {
                                var arr = JSON.parse(response);
                                $scope.rows = [];
                                $scope.totalItems = 0;
                                $scope.tableColumns = [];
                                arr = arr.filter(function (a) { return a !== 'TOTAL_COUNT' });
                                $scope.tableColumnsTemp = arr;
                                $scope.tableColumnsTemp.forEach(function (item, index) {
                                    item = item.replaceAll("_", " ");
                                    $scope.tableColumns.push(item);
                                });
                            }

                            if (downloadToExcel) {
                                $scope.excelRows = [];
                                $scope.excelColumnsTemp = [];
                                $scope.excelColumns = [];
                                $scope.rows.forEach(function (rowItem, rowIndex) {
                                    var newObj = Object.assign({}, rowItem);
                                    delete newObj.tableValues;
                                    $scope.excelColumnsTemp.push(newObj);
                                });
                                if ($scope.excelColumnsTemp && $scope.excelColumnsTemp.length > 0) {
                                    Object.keys($scope.excelColumnsTemp[0]).forEach(function (key, index) {
                                        var value = key + ' as [' + key.replaceAll("_", " ") + ']';
                                        $scope.excelColumns.push(value);
                                    });
                                    $scope.excelColumns = $scope.excelColumns.join(',');

                                    $scope.excelColumnsTemp.forEach(function (item, index) {
                                        $scope.excelRows.push(item);
                                    });
                                }
                                execute();
                                $scope.rows = arr.slice(0, 10);
                            }
                        } else {
                            if ($scope.tableColumnsTemp && $scope.tableColumnsTemp.length > 0) {
                                $scope.tableColumnsTemp.forEach(function (item, index) {
                                    item = item.replaceAll("_", " ");
                                    var foundIndex = _.findIndex($scope.tableColumns, function (val) { return val === item; });
                                    if (foundIndex <= -1) {
                                        $scope.tableColumns.push(item);
                                    }
                                });
                            }
                            $scope.rows = [];
                            $scope.totalItems = 0;
                            swal("Status!", "No Records Found..!", "error");
                        }
                    });
            };

            $scope.GetList(0, 10, '');

            $scope.downloadToExcel = function()
            {
                $scope.GetList(0, 0, '', true);
            }

            function execute()
            {
                var mystyle = {
                    headers: true,
                    column: { style: { Font: { Color: "#FFFFFF" }, Interior: { Color: "#4F81BD", Pattern: "Solid" }, Alignment: { Horizontal: "Center" } } },
                    row: { style: { Alignment: { Horizontal: "Center" } } }
                };
                alasql('SELECT '+ $scope.excelColumns +' INTO XLSXML("Get Pending Quotations.xls",?) FROM ?', [mystyle, $scope.excelRows]);
                $scope.downloadExcel = false;
            }

        }]);