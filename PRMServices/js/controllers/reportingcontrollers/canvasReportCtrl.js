﻿prmApp
    .controller('canvasReportCtrl', ["$scope", "$http", "$state", "$window", "$timeout", "domain", "$filter", "$stateParams", "$timeout", "$log", "auctionsService", "userService", "reportingService",
        function ($scope, $http, $state, $window, $timeout, domain, $filter, $stateParams, $timeout, $log, auctionsService, userService, reportingService) {

            $scope.htmlToCanvasSaveLoading = false;

            $scope.htmlToCanvasSave = function (format) {
                $scope.htmlToCanvasSaveLoading = true;

                setTimeout(function () {
                    try {
                        var name = "Comparisions-ReqID-" + $scope.reqId + "." + format;
                        var canvas = document.createElement("canvas");
                        if (format == 'pdf') {
                            document.getElementById("widget").style["display"] = "";
                        }
                        else {
                            document.getElementById("widget").style["display"] = "inline-block";
                        }

                        html2canvas($("#widget"), {
                            onrendered: function (canvas) {
                                theCanvas = canvas;

                                const a = document.createElement("a");
                                a.style = "display: none";
                                a.href = canvas.toDataURL();

                                // Add Image to HTML
                                //document.body.appendChild(canvas);

                                /* Save As PDF */
                                if (format == 'pdf') {
                                    var imgData = canvas.toDataURL();
                                    var pdf = new jsPDF();
                                    pdf.addImage(imgData, 'JPEG', 0, 0);
                                    pdf.save(name);
                                }
                                else {
                                    a.download = name;
                                    a.click();
                                }

                                // Clean up 
                                //document.body.removeChild(canvas);
                                document.getElementById("widget").style["display"] = "";
                                $scope.htmlToCanvasSaveLoading = false;
                            }
                        });
                    }
                    catch (err) {
                        document.getElementById("widget").style["display"] = "";
                        $scope.htmlToCanvasSaveLoading = false;
                    }
                    finally {

                    }
                }, 500);

            };



            $scope.scrolll = function (type) {
                var elmnt = document.getElementById("scrollDiv");

                if (type == 'scrollTop') {
                    elmnt.scrollTop = elmnt.scrollTop + 10;
                }
                if (type == 'scrollBottom') {
                    elmnt.scrollTop = elmnt.scrollTop - 10;
                }
                if (type == 'scrollLeft') {
                    elmnt.scrollLeft = elmnt.scrollLeft + 10;
                }
                if (type == 'scrollRight') {
                    elmnt.scrollLeft = elmnt.scrollLeft - 10;
                }
                
                //document.getElementById("divtab").innerHTML = "Horizontally: " + 10 + "px<br>Vertically: " + 10 + "px";
            }



    }]);