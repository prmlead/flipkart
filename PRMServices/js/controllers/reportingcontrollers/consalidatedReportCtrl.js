﻿
prmApp
    .controller('consalidatedReportCtrl', ["$timeout", "$uibModal", "$state", "$window", "$scope", "growlService", "userService", "auctionsService", "fwdauctionsService", "$http", "domain", "$rootScope", "fileReader", "$filter", "$log", "reportingService",
        function ($timeout, $uibModal, $state, $window, $scope, growlService, userService, auctionsService, fwdauctionsService, $http, domain, $rootScope, fileReader, $filter, $log, reportingService) {
            $scope.formRequest = {};
            $scope.formRequest.isForwardBidding = false;
            $scope.consalidatedReport = [];
            $scope.isVendor = (userService.getUserType() == "VENDOR") ? true : false;

            /*pagination code*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 8;
            $scope.revisedUserL1 = [];


            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
            };

            /* CLIENT STATUS MAPPING TO PRM STATUS */
            $scope.isCustomer = userService.getUserType();
            $scope.prmStatus = function (type, status) {
                return userService.NegotiationStatus(type, status);
            };
            /* CLIENT STATUS MAPPING TO PRM STATUS */

            //#region ListUserDepartmentDesignations

            $scope.deptIDs = [];
            $scope.desigIDs = [];
            $scope.deptIDStr = '';
            $scope.desigIDStr = '';
            $scope.SelectedDeptId = 0;
            if (!$scope.isVendor) {
                $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
                if (userService.getSelectedUserDepartmentDesignation()) {
                    $scope.SelectedUserDepartmentDesignation = userService.getSelectedUserDepartmentDesignation();
                    $scope.SelectedDeptId = $scope.SelectedUserDepartmentDesignation.deptID;
                }

                if ($scope.ListUserDepartmentDesignations && $scope.ListUserDepartmentDesignations.length > 0) {
                    $scope.ListUserDepartmentDesignations.forEach(function (item, index) {
                        $scope.deptIDs.push(item.deptID);
                        item.listDesignation.forEach(function (item1, index1) {
                            if (item1.isAssignedToUser && item1.isValid) {
                                $scope.desigIDs.push(item1.desigID);
                                $scope.desigIDs = _.union($scope.desigIDs, [item1.desigID]);
                            }
                        });
                    });
                    $scope.deptIDStr = $scope.deptIDs.join(',');
                    $scope.desigIDStr = $scope.desigIDs.join(',');
                }
            }

            //$scope.reportFromDate = '';
            //$scope.reportToDate = '';

            $scope.reportToDate = moment().format('YYYY-MM-DD');
            $scope.reportFromDate = moment().subtract(30, "days").format("YYYY-MM-DD");

            $scope.filterDate = {
                reqToDate: '',
                reqFromDate: ''
            };

            $scope.filterDate.reqToDate = moment().format('YYYY-MM-DD');
            $scope.filterDate.reqFromDate = moment().subtract(30, "days").format("YYYY-MM-DD");

            $scope.getConsalidatedReport = function () {
                $scope.errMessage = '';

                //var ts = userService.toUTCTicks($scope.reportFromDate);
                //var m = moment(ts);
                //var quotationDate = new Date(m);
                //var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                //var reportFromDate = "/Date(" + milliseconds + "000+0530)/";

                //var ts = userService.toUTCTicks($scope.reportToDate);
                //var m = moment(ts);
                //var quotationDate = new Date(m);
                //var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                //var reportToDate = "/Date(" + milliseconds + "000+0530)/";

                reportingService.getConsolidatedReport($scope.filterDate.reqFromDate, $scope.filterDate.reqToDate, $scope.deptIDStr)
                    .then(function (response) {
                        $scope.consalidatedReport = response;

                        $scope.totalItems = $scope.consalidatedReport.length;
                        $scope.consalidatedReport.forEach(function (item, index) {
                            item.closed = $scope.prmStatus($scope.isCustomer, item.closed);
                            item.quotationFreezTime = $scope.GetDateconverted(item.quotationFreezTime);
                            item.reqPostedOn = $scope.GetDateconverted(item.reqPostedOn);
                            item.startTime = $scope.GetDateconverted(item.startTime);

                            if (String(item.startTime).includes('9999')) {
                                item.startTime = '';
                            }
                        });
                    });
            };

            $scope.getConsalidatedReport();

            $scope.GetReport = function () {
                
                

                alasql('SELECT requirementNumber as [Requirement Number],title as [Requirement Title],prNumbers as [PR Numbers],POSTED_BY_USER as [Posted By],reqCategory as [Category], ' +
                    'closed as Status, ' +
                    'reqPostedOn as [Posted On], quotationFreezTime as [Freez Time], startTime as [Negotiation scheduled time],IL1_vendTotalPrice as [Initial Least Price], ' +
                    'RL1_companyName as [L1 Company Name],  RL1_revVendTotalPrice as [L1 Rev Price], ' + 
                    'RL2_companyName as [L2 Company Name],  RL2_revVendTotalPrice as [L2 Rev Price],  ' +
                    'basePriceSavings as [Savings],savingsPercentage as [Savings %] ' +
                    'INTO XLSX(?, { headers: true, sheetid: "ConsolidatedReport", style: "font-size:15px;background:#233646;color:#FFF;" }) FROM ? ',
                    ["ConsolidatedReport.xlsx", $scope.consalidatedReport]);


            }
            $scope.GetDateconverted = function (dateBefore) {
                if (dateBefore) {
                    return userService.toLocalDate(dateBefore);
                }
            };


        }]);