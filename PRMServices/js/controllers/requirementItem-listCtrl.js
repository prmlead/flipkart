﻿prmApp
    .controller('requirementItem-ListCtrl', ["$rootScope", "$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMPRServices",
        "PRMCustomFieldService", "catalogService", "$location", "PRMUploadServices", "$element", "fileReader","reportingService",
        function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPRServices, PRMCustomFieldService, catalogService, $location, PRMUploadServices, $element, fileReader, reportingService) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;
            $scope.compId = userService.getUserCompanyId();

            $scope.reqLevel = 1;
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;
            $scope.filters = {
                searchKeyword: '',
            }

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
                $scope.getrequirementItemsList(($scope.currentPage - 1), 10);
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };

            $scope.filterDate = {
                reqToDate: '',
                reqFromDate: ''
            };


            $scope.filterDate.reqToDate = moment().format('YYYY-MM-DD');
            $scope.filterDate.reqFromDate = moment().subtract(30, "days").format("YYYY-MM-DD");

            $scope.filterByDate = function () {
                $scope.getuserStatusReport(0, 0);

            };

            $scope.setFilters = function (currentPage,type) {


                if ($scope.loadServices) {
                    $scope.getrequirementItemsList(0, 10);
                }
                if (type == 'DATE_FILTER') {
                    $scope.GetitemsListfilters();
                }
            };

            $scope.filtersList = {
                requirementList :[],
                prNumberList :[],
                caseIdList :[],
                skuIdList :[],
                brandList :[],
                categoryList :[],
                reqStatusList :[],
                entityList :[],
                creatorList : []
            };


            $scope.filters = {
                reqNumber: {},
                prNumber: {},
                caseId: {},
                sku: {},
                brand: {},
                category: {},
                reqStatus: {},
                entity: {},
                creatorName: {},
            };

     

            $scope.getDate = function (value) {
                return userService.toLocalDate(value);
            }
            $scope.tableColumns = [];
            $scope.rows = [];
           
            $scope.getrequirementItemsList = function (recordsFetchFrom, pageSize, downloadToExcel) {


                var fromDate, toDate,reqNumbers, prNumbers, caseIds, skus, brands, categorys, creatorName, reqstatus, entity;

                if (_.isEmpty($scope.filterDate.reqFromDate)) {
                    fromDate = '';
                } else {
                    fromDate = $scope.filterDate.reqFromDate;
                }

                if (_.isEmpty($scope.filterDate.reqToDate)) {
                    toDate = '';
                } else {
                    toDate = $scope.filterDate.reqToDate;
                }

                if (_.isEmpty($scope.filters.reqNumber)) {
                    reqNumber = '';
                } else if ($scope.filters.reqNumber && $scope.filters.reqNumber.length > 0) {
                    var reqNumbers = _($scope.filters.reqNumber)
                        .filter(item => item.name)
                        .map('id')
                        .value();
                    reqNumber = reqNumbers.join(',');
                }

                if (_.isEmpty($scope.filters.caseId)) {
                    caseId = '';
                } else if ($scope.filters.caseId && $scope.filters.caseId.length > 0) {
                    var caseIds = _($scope.filters.caseId)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    caseId = caseIds.join(',');
                }


                if (_.isEmpty($scope.filters.prNumber)) {
                    prNumber = '';
                } else if ($scope.filters.prNumber && $scope.filters.prNumber.length > 0) {
                    var prNumbers = _($scope.filters.prNumber)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    prNumber = prNumbers.join(',');
                }


                if (_.isEmpty($scope.filters.sku)) {
                    sku = '';
                } else if ($scope.filters.sku && $scope.filters.sku.length > 0) {
                    var skus = _($scope.filters.sku)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    sku = skus.join(',');
                }

                if (_.isEmpty($scope.filters.brand)) {
                    brand = '';
                } else if ($scope.filters.brand && $scope.filters.brand.length > 0) {
                    var brands = _($scope.filters.brand)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    brand = brands.join(',');
                }

                if (_.isEmpty($scope.filters.category)) {
                    category = '';
                } else if ($scope.filters.category && $scope.filters.category.length > 0) {
                    var categorys = _($scope.filters.category)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    category = categorys.join(',');
                }

                if (_.isEmpty($scope.filters.reqStatus)) {
                    status = '';
                } else if ($scope.filters.reqStatus && $scope.filters.reqStatus.length > 0) {
                    var reqstatus = _($scope.filters.reqStatus)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    status = reqstatus.join(',');
                }

              


                if (_.isEmpty($scope.filters.creatorName)) {
                    creatorName = '';
                } else if ($scope.filters.creatorName && $scope.filters.creatorName.length > 0) {
                    var creatorName = _($scope.filters.creatorName)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    creatorName = creatorName.join(',');
                }


                if (_.isEmpty($scope.filters.entity)) {
                    entity = '';
                } else if ($scope.filters.brand && $scope.filters.entity.length > 0) {
                    var entity = _($scope.filters.entity)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    entity = entity.join(',');
                }

                $scope.tableValues = [];
                auctionsService.getrequirementItemsList({
                    "userid": userService.getUserId(), "fromDate": fromDate, "toDate": toDate, "status": status, "search": '', "allBuyer": creatorName, "prNumber": prNumber,
                    "reqNumber": reqNumber, "caseID": caseId, "skuID": sku, "brand": brand, "category": category, "entity": entity, "page": recordsFetchFrom * pageSize, "pagesize": pageSize, "sessionid": userService.getUserToken()
                })
                    .then(function (response) {
                        $scope.loadServices = true;
                        if (response) {
                            $scope.arr = JSON.parse(response).Table;
                            if ($scope.arr && $scope.arr.length > 0) {
                                $scope.totalItems = $scope.arr[0].TOTAL_COUNT;
                                $scope.arr.forEach(a => delete a.TOTAL_COUNT);
                                if ($scope.tableColumns.length <= 0) {
                                    $scope.tableColumnsTemp = angular.copy(_.keys($scope.arr[0]));
                                    $scope.tableColumnsTemp.forEach(function (item, index) {
                                        item = item.replaceAll("_", " ");
                                        $scope.tableColumns.push(item);
                                    });
                                }

                                $scope.rows = $scope.arr;
                                $scope.arr.forEach(function (item, index) {
                                    if (item.Date) {
                                        item.Date = moment(item.Date).format("DD-MM-YYYY");
                                    }
                                    var obj = angular.copy(_.values(item));
                                    if (obj) {
                                        item.tableValues = [];
                                        obj.forEach(function (value, valueIndex) {
                                            item.tableValues.push(value);
                                        });
                                    }
                                });

                                if (downloadToExcel) {
                                    $scope.excelRows = [];
                                    $scope.excelColumnsTemp = [];
                                    $scope.excelColumns = [];
                                    $scope.rows.forEach(function (rowItem, rowIndex) {
                                        var newObj = Object.assign({}, rowItem);
                                        delete newObj.tableValues;
                                        $scope.excelColumnsTemp.push(newObj);
                                    });
                                    if ($scope.excelColumnsTemp && $scope.excelColumnsTemp.length > 0) {
                                        Object.keys($scope.excelColumnsTemp[0]).forEach(function (key, index) {
                                            var value = '[' + key.replaceAll(" ", "_") + ']' + ' as [' + key.replaceAll("_", " ") + ']';
                                            $scope.excelColumns.push(value);
                                        });
                                        $scope.excelColumns = $scope.excelColumns.join(',');

                                        $scope.excelColumnsTemp.forEach(function (item, index) {
                                            let newObj = {};

                                            for (let key in item) {
                                                let newKey = key.replace(/ /g, '_');  // Replace space with underscore
                                                newObj[newKey] = item[key];
                                            }
                                            $scope.excelRows.push(newObj);
                                        });
                                    }
                                    execute();
                                    $scope.rows = $scope.arr.slice(0, 10);
                                }

                               
                            }
                        } else {
                            $scope.rows = [];
                            $scope.arr = [];
                            $scope.totalItems = 0;
                        }


                        
                    })
                
            }
            $scope.getrequirementItemsList(0, 10);



            $scope.filterValues = [];

            $scope.GetitemsListfilters = function () {
                var params =
                {
                    "userID": userService.getUserId(),
                    "fromDate": $scope.filterDate.reqFromDate,
                    "toDate": $scope.filterDate.reqToDate,
                    "sessionid": userService.getUserToken(),
                    "type": 'RFQ'

                };

                let reqNumberListTemp = [];
                let prNumberListTemp = [];
                let caseIdListTemp = [];
                let skuIdListTemp = [];
                let brandListTemp = [];
                let categoryListTemp = [];
                let reqStatusListTemp = [];
                let entityListTemp = [];
                let creatorNameListTemp = [];
                $scope.loadServices = false;

                auctionsService.GetitemsListfilters(params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.filterValues = response;
                            if ($scope.filterValues && $scope.filterValues.length > 0) {
                                $scope.filterValues.forEach(function (item, index) {
                                    if (item.TYPE === 'REQ_NUMBER') {
                                        reqNumberListTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'PR_NUMBER') {
                                        prNumberListTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'CASE_ID') {
                                        caseIdListTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'SKU_ID') {
                                        skuIdListTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'BRAND') {
                                        brandListTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'CATEGORY') {
                                        categoryListTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'REQ_STATUS') {
                                        reqStatusListTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'ENTITY') {
                                        entityListTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'CREATOR_NAME') {
                                        creatorNameListTemp.push({ id: item.ID, name: item.NAME });
                                    }

                                });

                                $scope.filtersList.requirementList = reqNumberListTemp;
                                $scope.filtersList.prNumberList = prNumberListTemp;
                                $scope.filtersList.caseIdList = caseIdListTemp;
                                $scope.filtersList.skuIdList = skuIdListTemp;
                                $scope.filtersList.brandList = brandListTemp;
                                $scope.filtersList.categoryList = categoryListTemp;
                                $scope.filtersList.reqStatusList = reqStatusListTemp;
                                $scope.filtersList.entityList = entityListTemp;
                                $scope.filtersList.creatorList = creatorNameListTemp;
                            }
                        }
                        else {

                            $scope.filtersList = {
                                requirementList: [],
                                prNumberList: [],
                                caseIdList: [],
                                skuIdList: [],
                                brandList: [],
                                categoryList: [],
                                reqStatusList: [],
                                entityList: [],
                                creatorList: []
                            };
                        }
                    });

            };
            $scope.GetitemsListfilters();
            


            $scope.downloadToExcel = function () {
                $scope.getrequirementItemsList(0, 0, true);
            }


            function execute()
            {
                var mystyle = {
                    headers: true,
                    column: { style: { Font: { Color: "#FFFFFF" }, Interior: { Color: "#4F81BD", Pattern: "Solid" }, Alignment: { Horizontal: "Center" } } },
                    row: { style: { Alignment: { Horizontal: "Center" } } }
                };
                alasql('SELECT ' + $scope.excelColumns + ' INTO XLSXML("REQUIREMENT_ITEM_LEVEL.xls",?) FROM ?', [mystyle, $scope.excelRows]);
                $scope.downloadExcel = false;                
            }
           
            $scope.routereqlevel = function () {
                $state.go('pages.profile.profile-timeline');
            };       
          
        }]);