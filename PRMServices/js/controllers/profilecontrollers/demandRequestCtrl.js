﻿
prmApp
    .controller('demandRequestCtrl', ["$timeout", "$uibModal", "$state", "$window", "$scope", "growlService", "userService", "auctionsService", "fwdauctionsService", "$http", "domain", "$rootScope", "fileReader", "$filter", "$log", "reportingService", "$stateParams", "PRMUploadServices",
        function ($timeout, $uibModal, $state, $window, $scope, growlService, userService, auctionsService, fwdauctionsService, $http, domain, $rootScope, fileReader, $filter, $log, reportingService, $stateParams, PRMUploadServices) {
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;
            $scope.compId = userService.getUserCompanyId();
            $scope.hideButtons = false;
            if (window.location.href.contains("WEB"))
            {
                $scope.hideButtons = true;
            }

            $scope.processFiles = function () {
                var params = {
                    IS_PROCCESED: 0,
                    sessionID: userService.getUserToken()
                };

                PRMUploadServices.processFiles(params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            if (response[0].IS_SUCCESS) {
                                //swal("Status!", "Total Count : " + response[0].TOTAL_COUNT + " \n " + " Successfull Count : " + response[0].SUCCESS_COUNT + " \n " + " Failed Count : " + response[0].FAILED_COUNT, "warning");
                                $scope.getDemandRequest();
                            }

                            if (response[0].SUCCESS_COUNT > 0) {
                                $scope.getDemandRequest();
                            } else {

                            }
                        }
                    });

            };
            $scope.searchKeyword = '';

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
                $scope.getDemandRequest($scope.searchKeyword);

            };

            $scope.filterDate = {
                reqToDate: '',
                reqFromDate: ''
            };

            $scope.filterDate.reqToDate = moment().format('YYYY-MM-DD');
            $scope.filterDate.reqFromDate = moment().subtract(30, "days").format("YYYY-MM-DD");



            $scope.getDemandRequest = function (searchKeyword) {
                auctionsService.getDemandRequest({ "userid": userService.getUserId(), "fromDate": $scope.filterDate.reqFromDate, "toDate": $scope.filterDate.reqToDate, "search": searchKeyword ? searchKeyword : '', "sessionid": userService.getUserToken()})
                    .then(function (response) {
                        response.forEach(function (item, index) {
                            //item.expDeliveryDate = item.expDeliveryDate ? moment(item.expDeliveryDate).format("DD-MM-YYYY") : '-';
                            item.expDeliveryDate = String(item.expDeliveryDate).includes('9999') ? '-' : moment(item.expDeliveryDate).format("DD-MM-YYYY") ;
                        });
                        $scope.demandRequestList = response;
                        $scope.totalItems = $scope.demandRequestList.length;

                    });
            };

            $scope.getDemandRequest();

            //$scope.getDate = function (value) {
            //    return userService.toLocalDate(value);
            //}

            $scope.getDate = function (value) {
                var utcTemp = moment.tz(value, "UTC");
                return moment.utc(new moment(utcTemp).format("DD-MM-YYYY HH:mm"), "DD-MM-YYYY HH:mm").local().format('DD-MM-YYYY HH:mm');
            }

            $scope.exportToExcel = function (type) {
                PRMUploadServices.GetExcelTemplate(type);
            };

            $scope.showErrorPopup = false;
            $scope.rowErrors = [];


            $scope.uploadExcel = function (type, proceedFurther, fileName) {
                $scope.showErrorPopup = false;
                $scope.FILE_NAME = fileName ? fileName : $scope.FILE_NAME;
                var params = {
                    name: type,
                    compID: $scope.compId,
                    sessionID: userService.getUserToken(),
                    tableName: 'SAP_REQUIREMENT_DETAILS',
                    attachment: $scope.attachment,
                    proceedFurther: proceedFurther,
                    fileName: $scope.FILE_NAME
                };
                PRMUploadServices.uploadTemplate(params)
                    .then(function (response) {

                        if (response && response.length > 0) {
                            if (response[0].IS_SUCCESS) {
                                //If the sheet exceeds greater than 10 errors it is displayed in error logs
                                if (response.length <= 10) {
                                    var reasonString = _.map(response, 'REASON').join(",");
                                } else {
                                    var reasonString = 'Please check in the error logs.';
                                }
                                angular.element('#errorPopUp').modal('hide');
                                swal("Status!", "Total Count : " + response[0].TOTAL_COUNT + " \n " + " Successfull Count : " + response[0].SUCCESS_COUNT + " \n " + " Failed Count : " + response[0].FAILED_COUNT + " \n " + " Reason : " + reasonString, "warning");
                                $("#requirementclientupload").val(null);
                                $scope.setPage(1);
                            } else {
                                if (!response[0].IS_SUCCESS && proceedFurther === 1) {
                                    $scope.showErrorPopup = true;
                                    $scope.rowErrors = response;
                                    angular.element('#errorPopUp').modal('show');
                                    if (response[0].IS_NO_RECORDS) {
                                        swal("Status!", "Sheet is empty.. So cannot continue further ! Please validate the sheet again and upload.", "error");
                                    } else {
                                        swal("Status!", "All the records in this sheet contains errors.. So cannot continue further ! Please validate the sheet again and upload.", "error");
                                    }
                                    $("#requirementclientupload").val(null);
                                    angular.element('#errorPopUp').modal('hide');
                                } else {
                                    $scope.showErrorPopup = true;
                                    $scope.rowErrors = response;
                                    angular.element('#errorPopUp').modal('show');
                                    $("#requirementclientupload").val(null);
                                }
                            }
                        }else {

                            $scope.uploadExcel('REQUIREMENT_DETAILS', 1);
                        }
                    });
            };
            $scope.getFile1 = function (id, itemid, ext) {

                console.log(1)
                $scope.filesTemp = $("#" + id)[0].files;
                $scope.filesTemp = Object.values($scope.filesTemp);
                $scope.filesTemp.forEach(function (attach, attachIndex) {
                    $scope.file = $("#" + id)[0].files[attachIndex];

                    fileReader.readAsDataUrl($scope.file, $scope)
                        .then(function (result) {
                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };
                            var bytearray = new Uint8Array(result);
                            $scope.attachment = $.makeArray(bytearray);
                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = attach.name;

                            if (id == "requirementclientupload") {
                                $scope.uploadExcel('REQUIREMENT_DETAILS', 0, fileUpload.fileName);
                                $scope.file = [];
                                $scope.file.name = '';
                            }
                        });
                });
            };

            $scope.exportErrorDetails = function (type) {
                $state.go('moduleErrorList', { 'moduleName': type });
            };
           

            $scope.getModuleBasedErrorDetails = function () {
                $scope.modulErrorList = [];
                $scope.rowErrors = [];
                PRMUploadServices.getModuleBasedErrorDetails({ "MODULE": 'REQUIREMENT_DETAILS' }).then(function (response) {
                    $scope.modulErrorList = response;
                    $scope.rowErrors = response;
                    angular.element('#errorPopUp').modal('show');
                });
            };

            $scope.cancel = function () {
                $scope.getDemandRequest();
                angular.element('#errorPopUp').modal('hide');
            };

        }]);