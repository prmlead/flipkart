﻿prmApp
    .controller('workflowApprovalCtrl', function ($scope, $state, $stateParams, $filter, userService, growlService, workflowService, auctionsService, fileReader, $log) {
        $scope.compID = userService.getUserCompanyId();
        $scope.sessionID = $stateParams.sessionID;
        $scope.approverID = $stateParams.workflowTrackID;
        $scope.currentID = $stateParams.userID;
        $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;
        $scope.status = $stateParams.wfstatus;
        $scope.moduleID = $stateParams.moduleID;
        $scope.moduleName = $stateParams.module;
        $scope.WorkflowTrack = {};
        $scope.commentsError = '';

        $scope.getItemWorkflow = function () {
            workflowService.getItemWorkflow($stateParams.workflowID, $stateParams.moduleID, $stateParams.module)
                .then(function (response) {
                    $scope.itemWorkflow = response;
                    if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                        $scope.currentStep = 0;

                        var count = 0;
                        $scope.WorkflowTrack = $filter('filter')($scope.itemWorkflow[0].WorkflowTracks, { approverID: $scope.approverID })[0];
                        $scope.WorkflowTrack.status = $scope.status;
                        $scope.WorkflowTrack.modifiedBy = $scope.currentID;
                        $scope.WorkflowTrack.moduleID = $scope.moduleID;
                        $scope.WorkflowTrack.sessionID = $scope.sessionID;
                        $scope.WorkflowTrack.moduleName = $scope.moduleName;
                        $scope.WorkflowTrack.comments = '';
                    }
                });
        }

        $scope.getItemWorkflow();


        $scope.updateTrack = function (step) {
            $scope.commentsError = '';
            
            $scope.commentsError = '';
            if ($scope.WorkflowTrack.comments == '' && $scope.status == 'REJECTED') {
                $scope.commentsError = "Please enter comments";
                return;
            }
            workflowService.SaveWorkflowTrack(step)
                .then(function (response) {
                    console.log(response);
                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        growlService.growl("Action Saved Successfully.", "success");
                        $scope.logout();
                        $scope.closeWindow();
                    }
                })
        }

        $scope.logout = function () {
            userService.logout().then(function () {
                $scope.loggedIn = userService.isLoggedIn();
            });
        }

        $scope.IsUserApprover = false;


        $scope.functionResponse = false;
        $scope.IsUserApproverForStage = function () {
            workflowService.IsUserApproverForStage($scope.approverID, userService.getUserId())
                .then(function (response) {
                    $scope.IsUserApprover = response;
                });
        };

        $scope.closeWindow = function () {
            var Browser = navigator.appName;
            var indexB = Browser.indexOf('Explorer');

            if (indexB > 0) {
                var indexV = navigator.userAgent.indexOf('MSIE') + 5;
                var Version = navigator.userAgent.substring(indexV, indexV + 1);

                if (Version >= 7) {
                    window.open('', '_self', '');
                    window.close();
                }
                else if (Version == 6) {
                    window.opener = null;
                    window.close();
                }
                else {
                    window.opener = '';
                    window.close();
                }

            }
            else {
                window.open('', '_self', '');
                window.close();
            }
        };





        $scope.SaveEmailApproval = function () {
            $scope.updateTrack($scope.WorkflowTrack);
        };




    });