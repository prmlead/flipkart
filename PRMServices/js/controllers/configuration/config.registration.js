﻿prmApp.controller("RegistrationSettingsController", ['$uibModal', 'SettingService', 'store', 'growlService', function ($uibModal, SettingService, store, growlService) {
    var _regSettingsCtrl = this;
    _regSettingsCtrl.FieldList = []
    _regSettingsCtrl.addField = function () {
        $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'RegistrationSettingModal.html',
            controller: 'RegistrationSettingModalCtrl',
            controllerAs: '_modalCtrl',
            appendTo: 'body',
            resolve: {
                options: {
                    editMode: false
                }
            }
        }).result.then(function (data) {
            _regSettingsCtrl.getFields();

        });
    }

    _regSettingsCtrl.editField = function (item) {
        $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'RegistrationSettingModal.html',
            controller: 'RegistrationSettingModalCtrl',
            controllerAs: '_modalCtrl',
            appendTo: 'body',
            resolve: {
                options: {
                    editMode: true,
                    field: item
                }
            }
        }).result.then(function (data) {
            _regSettingsCtrl.getFields();
        });
    }

    _regSettingsCtrl.deleteField = function (item, index) {

        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this field!",
            icon: "warning",
            buttons: [
              'No, cancel it!',
              'Yes, I am sure!'
            ],
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, I am sure!",
            closeOnConfirm: true,
            dangerMode: true,
        }, function (isConfirm) {
            console.log(isConfirm)
            if (isConfirm) {
                SettingService.deleteRegistrationField(item.Id).then(function (response) {
                    if (response.errorMessage == "") {
                        _regSettingsCtrl.FieldList.splice(index, 1);
                        growlService.growl(response.message, "success", {
                            from: "bottom",
                            align : "left"
                        })
                    }
                    else {
                        growlService.growl(response.errorMessage, "error")

                    }
                })
            } else {
                swal("Cancelled", "Your imaginary file is safe :)", "error");
            }
        })



    }

    _regSettingsCtrl.getFields = function () {


        SettingService.getRegistrationFields().then(function (response) {
            if (response != null && response.length > 0) {
                _regSettingsCtrl.FieldList = response
            }
        })
    }
    _regSettingsCtrl.dragControlListeners = {
        containment: '#fieldList',
        accept: function (sourceItemHandleScope, destSortableScope) {
            return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id;
        }
    };

}]);

prmApp.controller("RegistrationSettingModalCtrl", ['$uibModalInstance', 'options', 'SettingService', 'store', 'growlService', function ($uibModalInstance, options, SettingService, store, growlService) {
    var _modalCtrl = this;
    _modalCtrl.Model = {
        Type: "",
        Required: true
    };
    _modalCtrl.options = options;
    _modalCtrl.TypeList = ["text", "number", "checkbox", "radio"]
    if (options.editMode) {
        _modalCtrl.Model = options.field
    }

    _modalCtrl.Create = function (form) {
        form.$submitted = true;
        if (form.$valid) {
            SettingService.insertRegistrationField(_modalCtrl.Model).then(function (response) {
                if (response.errorMessage == "") {
                    $uibModalInstance.close(_modalCtrl.Model);
                    growlService.growl(response.message, "inverse")
                }
                else {
                    growlService.growl(response.errorMessage, "inverse")

                }
            })

        }

    };
    _modalCtrl.Update = function (form) {
        form.$submitted = true;
        if (form.$valid) {
            SettingService.updateRegistrationField(_modalCtrl.Model).then(function (response) {
                if (response.errorMessage == "") {
                    $uibModalInstance.close(_modalCtrl.Model);
                    growlService.growl(response.message, "inverse")
                } else {
                    growlService.growl(response.errorMessage, "inverse")

                }
            })

        }
    };

    _modalCtrl.close = function () {
        $uibModalInstance.close();
    };

}]);