﻿prmApp
    .controller('createInvoiceCtrl', function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPOService, poService, PRMCustomFieldService, fileReader, $uibModal, $filter, workflowService, PRMJeevesOracleService) {
        $scope.userID = userService.getUserId();
        $scope.sessionID = userService.getUserToken();
        $scope.invoiceNumber = $stateParams.invoiceNumber;
        $scope.poNumber = $stateParams.poNumber;
        //$scope.asnCode = $stateParams.asnCode;
        $scope.invoiceID = $stateParams.invoiceID;
        $scope.isCustomer = userService.getUserType() === "CUSTOMER" ? true : false;
        $scope.isVendor = (userService.getUserType() == "VENDOR") ? true : false;
        $scope.compID = userService.getUserCompanyId();
        $scope.customerCompanyId = userService.getCustomerCompanyId();
        $scope.isSuperUser = userService.getUserObj().isSuperUser;
        $scope.selectedPODetails = [];
        $scope.selectedPODetails = [{
            itemAttachment: [],
            attachmentName: '',
            attachmentsArray:[]
        }]
        $scope.pendingPOItems = [{
            itemAttachment: [],
            attachmentName: '',
            attachmentsArray: []
        }]
        $scope.billedQty = 0;
        $scope.receivedQty = 0;
        $scope.rejectedQty = 0;
        $scope.remainingQty = 0;
        $scope.removeQty = 0;
        $scope.isError = false;
        $scope.ALTERNATIVE_UOM = '';
        $scope.ALTERNATIVE_UOM_QTY = '';
        $scope.SERVICE_CODE = '';
        $scope.SERVICE_DESCRIPTION = '';
        $scope.MISC_CHARGES = '';
        $scope.ACK_COMMENTS = '';
        $scope.maxDateMoment = moment();

        if ($scope.isCustomer) {
            $scope.SelectedDeptId = 0;
            $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
            if (userService.getSelectedUserDepartmentDesignation()) {
                $scope.SelectedUserDepartmentDesignation = userService.getSelectedUserDepartmentDesignation();
            }
            $scope.SelectedDeptId = $scope.SelectedUserDepartmentDesignation.deptID;
            $scope.UserLocation = $scope.SelectedUserDepartmentDesignation.userLocation;
        }

        $scope.filters = {
            fromDate: moment().subtract(30, "days").format("YYYY-MM-DD"),
            toDate: moment().format('YYYY-MM-DD'),
        }
        $scope.checkInvoiceUniqueResult = false;
        $scope.isSaveDisabled = false;

        
        $scope.getInvoiceList = function () {
            $scope.params = {
                "COMP_ID": $scope.isCustomer ? +$scope.compID : 0,
                "U_ID": $scope.isCustomer ? 0 : +$scope.userID,
                "fromDate": $scope.filters.fromDate,
                "toDate": $scope.filters.toDate,
                "PO_NUMBER": $scope.poNumber,
                "INVOICE_NUMBER": $scope.invoiceNumber
            }

            PRMPOService.getInvoiceList($scope.params)
                .then(function (response) {
                    $scope.filteredPendingPOsList.REMARKS = response[0].COMMENTS;
                    $scope.filteredPendingPOsList.INVOICE_TYPE_LOOKUP = response[0].INVOICE_TYPE;
                    $scope.filteredPendingPOsList.EXCHANGE_RATE = response[0].EXCHANGE_RATE;
                    $scope.filteredPendingPOsList.IRN_NO = response[0].IRN_NO;
                    $scope.filteredPendingPOsList.INVOICE_NUMBER = response[0].INVOICE_NUMBER;
                    $scope.filteredPendingPOsList.INVOICE_DATE_1 = new moment(response[0].INVOICE_DATE).format("DD-MM-YYYY");
                    $scope.filteredPendingPOsList.DUE_DATE_1 = new moment(response[0].DUE_DATE).format("DD-MM-YYYY");
                    $scope.filteredPendingPOsList.attachmentsArray = response[0].attachmentsArray;
                    $scope.filteredPendingPOsList.PO_TYPE = response[0].PO_TYPE;
                    $scope.filteredPendingPOsList.ACK_COMMENTS = response[0].ACK_COMMENTS;
                    $scope.filteredPendingPOsList.WF_STATUS = response[0].WF_STATUS;
                    $scope.filteredPendingPOsList.IS_ACK = response[0].IS_ACK ? response[0].IS_ACK : 0;
                    $scope.filteredPendingPOsList.INVOICE_COMMENTS = response[0].INVOICE_COMMENTS;
                    $scope.filteredPendingPOsList.ROW_ID = response[0].INVOICE_ROW_ID > 0 ? response[0].INVOICE_ROW_ID : $scope.invoiceID ;

                    $scope.isFormdisabled = true;
                    $scope.pendingPOItems = response;
                    $scope.pendingPOItems.forEach(function (item, index) {
                        item.isErrorWeightValidation = false;
                        item.isErrorGrossWeightValidation = false;
                        item.isErrorWeightValidationRemaining = false;
                        item.isErrorRemaining = false;
                        //item.AMOUNT = 0;
                        item.NATURE_OF_TRANSACTION = item.IGST > 0 ? 'Inter-State Purchase' : 'Intra-State Purchase';
                        item.AMOUNT = item.NET_PRICE;
                        //item.TAX = item.IGST + item.CGST + item.SGST;
                        item.INVOICE_QTY_TEMP = item.INVOICE_QTY;
                        item.TOTAL_INVOICE_LINE_ITEM_REMAINING_QTY_TEMP = item.TOTAL_INVOICE_LINE_ITEM_REMAINING_QTY;
                        item.HSN_CODE = _.filter($scope.pendingPOItemsTemp, function (o) { return o.productid == item.productid })[0].HSN_CODE;                        
                    });


                    if ($scope.isCustomer) {
                        $scope.getItemWorkflow('', $scope.filteredPendingPOsList.ROW_ID)
                    }

                    $scope.getSum();
                    console.log($scope.pendingPOItems);
                });
        }


        $scope.getPendingPOOverall = function () {
            $scope.billedQty = 0;
            $scope.receivedQty = 0;
            $scope.rejectedQty = 0;
            $scope.remainingQty = 0;
            $scope.removeQty = 0;

            var params = {
                "compid": $scope.isCustomer ? $scope.compID : 0,
                "uid": $scope.isCustomer ? 0 : +$scope.userID,
                "search": $scope.poNumber,
                "categoryid": '',
                "productid": '',
                "supplier": '',
                "postatus": '',
                "deliverystatus": '',
                "plant": '',
                "fromdate": '1970-01-01',
                "todate": '2100-01-01',
                "page": 0,
                "pagesize": 10,
                "ackStatus": '',
                "buyer": '',
                "purchaseGroup": '',
                "sessionid": userService.getUserToken()

            };

            $scope.pageSizeTemp = (params.page + 1);

            PRMPOService.getPOScheduleList(params)
                .then(function (response) {
                    $scope.pendingPOList = [];
                    $scope.filteredPendingPOsList = [];
                    if (response && response.length > 0) {
                        response.forEach(function (item, index) {
                            item.TRANSACTION_TYPE = item.CURRENCY == 'INR' ? 'Domestic' : 'International';
                            item.IS_RCM_APPLICABLE = 'N'; // item.GST_NUMBER != '' ? 'Y' : 'N'
                            item.REGISTERED_UNDER_GST = item.GST_NUMBER ? 'Y' : 'N';
                            item.PAYMENT_METHOD = 'EFT';
                            item.PAYGROUP_DESCRIPTION = 'PRM PURCHASE';
                            //item.TDS_CODE = 'PIBPL_TDS_194Q';
                            item.IRN_NO = '';
                            item.SUB_TOTAL = 0;
                            item.INVOICE_AMOUNT = 0;
                            item.INDENT_OF_PURCHASE = 'INPUTS';
                            item.TAX = 0;
                            //item.AMOUNT = item.NET_PRICE * item.INVOICE_QTY;
                            item.REMARKS = '';
                            item.INVOICE_TYPE_LOOKUP = 'Standard';
                            $scope.pendingPOList.push(item);

                        });

                    }

                    $scope.filteredPendingPOsList = $scope.pendingPOList[0];

                    if ($scope.filteredPendingPOsList.PO_NUMBER !== '') {
                        var params1 = {
                            "ponumber": $scope.poNumber,
                            "moredetails": 0,
                            "forasn": false
                        };
                        PRMPOService.getPOScheduleItems(params1)
                            .then(function (response) {
                                $scope.pendingPOItemsTemp = response;
                                $scope.pendingPOItems = response;
                                $scope.filteredPendingPOsList.TDS_CODE = response[0].ENTITY == 'Jeeves' ? 'F1 / JCS_TDS_194(Q)_0.1%' : 'F1_TDS_194(Q)_0.1%';
                                $scope.pendingPOItems.forEach(function (item, index) {
                                    //item.ORDER_QTY = item.RECEIVED_QTY; // PO Qty 
                                    item.NATURE_OF_TRANSACTION = item.IGST > 0 ? 'Inter-State Purchase' : 'Intra-State Purchase';
                                    //item.TAX = item.IGST + item.CGST + item.SGST;
                                    //$scope.getSum(item, 'QTY', index);
                                    item.AMOUNT = item.UNIT_PRICE_NO_TAX * item.INVOICE_QTY;

                                });

                                $scope.filteredPendingPOsList.TAX = _.sumBy($scope.pendingPOItems, function (o) { return ((o.AMOUNT) / 100 * (o.IGST + o.SGST + o.CGST)); });

                                //pendingPODetails.pendingPOItems = $scope.pendingPOItems;
                                if ($scope.pendingPOItems.length > 0 && $scope.invoiceID > 0) {
                                    $scope.getInvoiceList();
                                }

                            });
                    }
                });
        };


        $scope.getPendingPOOverall();

        $scope.createInvoice = function (poNumber) {
            var url = $state.href('vendorPoInvoices', { "poNumber": poNumber });
            $window.open(url, '_blank');
        };

        $scope.getSum = function (item) {
            $scope.filteredPendingPOsList.SUB_TOTAL = 0;
            $scope.filteredPendingPOsList.INVOICE_AMOUNT = 0;
            $scope.filteredPendingPOsList.TAX = 0;

            if (item) {
                $scope.pendingPOItems.forEach(function (item1, index) {
                    let itemTotalPrice = (item1.UNIT_PRICE_NO_TAX * item1.INVOICE_QTY);
                    $scope.filteredPendingPOsList.SUB_TOTAL = $scope.filteredPendingPOsList.SUB_TOTAL + itemTotalPrice;
                    $scope.filteredPendingPOsList.TAX = $scope.filteredPendingPOsList.TAX + (itemTotalPrice * ((item1.IGST + item1.CGST + item1.SGST) / 100));
                    item1.AMOUNT = itemTotalPrice + (itemTotalPrice * ((item1.IGST + item1.CGST + item1.SGST) / 100))
                    if ($scope.filteredPendingPOsList && $scope.filteredPendingPOsList.INVOICE_NUMBER && ($scope.filteredPendingPOsList.INVOICE_NUMBER.indexOf('INV_PP_PO_') >= 0 || $scope.filteredPendingPOsList.INVOICE_NUMBER.indexOf('INV_DM_PO_') >= 0)) {
                        $scope.filteredPendingPOsList.INVOICE_AMOUNT = $scope.filteredPendingPOsList.INVOICE_AMOUNT + item1.INVOICE_AMOUNT;
                    } else {
                        $scope.filteredPendingPOsList.INVOICE_AMOUNT = $scope.filteredPendingPOsList.INVOICE_AMOUNT + (item1.INVOICE_AMOUNT ? item1.INVOICE_AMOUNT : item1.AMOUNT);
                    }
                });
            } else {
                $scope.pendingPOItems.forEach(function (item1, index) {
                    let itemTotalPrice = (item1.UNIT_PRICE_NO_TAX * item1.INVOICE_QTY);
                    $scope.filteredPendingPOsList.SUB_TOTAL = $scope.filteredPendingPOsList.SUB_TOTAL + itemTotalPrice;
                    $scope.filteredPendingPOsList.TAX = $scope.filteredPendingPOsList.TAX + (itemTotalPrice * ((item1.IGST + item1.CGST + item1.SGST) / 100));

                    if ($scope.filteredPendingPOsList.INVOICE_NUMBER.indexOf('INV_PP_PO_') >= 0 || $scope.filteredPendingPOsList.INVOICE_NUMBER.indexOf('INV_DM_PO_') >= 0) {
                        $scope.filteredPendingPOsList.INVOICE_AMOUNT = $scope.filteredPendingPOsList.INVOICE_AMOUNT + item1.INVOICE_AMOUNT;
                    } else {
                        $scope.filteredPendingPOsList.INVOICE_AMOUNT = item1.INVOICE_AMOUNT;
                    }
                });
            }
        }

        $scope.isQtyError = function () {
            let isValid = false;
            isValid = _.some($scope.selectedPODetails, function (item) {
                return (parseFloat(item.INVOICE_QTY) > parseFloat(item.TOTAL_INVOICE_LINE_ITEM_REMAINING_QTY_TEMP));
            });
            return isValid;
        };

        $scope.areAllItemsValid = function () {
            let isValid = false;
            isValid = _.some($scope.selectedPODetails, function (item) {
                return item.isError;
            });
            return isValid;
        };


        $scope.saveInvoice = function (item) {
            $scope.isError = false;
            $scope.isErrorWeightValidation = false;
            $scope.isErrorWeightValidationRemaining = false;
            $scope.isErrorGrossWeightValidation = false;
            $scope.invoiceNumberError = false;
            $scope.invoiceDateError = false;
            $scope.dueDateError = false;
            $scope.invoiceTypeError = false;
            $scope.isErrorRemaining = false;

            //if ($scope.filteredPendingPOsList.INVOICE_TYPE_LOOKUP == '' || $scope.filteredPendingPOsList.INVOICE_TYPE_LOOKUP == undefined) {
            //    $scope.invoiceTypeError = true;
            //}

            if ($scope.filteredPendingPOsList.INVOICE_NUMBER == '' || $scope.filteredPendingPOsList.INVOICE_NUMBER == undefined) {
                $scope.invoiceNumberError = true;
            }

            if ($scope.filteredPendingPOsList.INVOICE_DATE_1 == '' || $scope.filteredPendingPOsList.INVOICE_DATE_1 == undefined) {
                $scope.invoiceDateError = true;
            }

            //if ($scope.filteredPendingPOsList.DUE_DATE_1 == '' || $scope.filteredPendingPOsList.DUE_DATE_1 == undefined) {
            //    $scope.dueDateError = true;
            //}

            if ($scope.checkInvoiceUniqueResult) {
                swal("Error!", "Invoice number already exists.", "error");
                return;
            }


            if ($scope.filteredPendingPOsList.PO_TYPE == 'Pre Payment') {

                if ($scope.filteredPendingPOsList.ACK_COMMENTS == '' || $scope.filteredPendingPOsList.ACK_COMMENTS == null || $scope.filteredPendingPOsList.ACK_COMMENTS == undefined) {
                    swal("Error!", "Please Enter Ack Comments.", "error");
                    return;
                }
               
            } else {

                if ($scope.filteredPendingPOsList.INVOICE_COMMENTS == '' || $scope.filteredPendingPOsList.INVOICE_COMMENTS == null || $scope.filteredPendingPOsList.INVOICE_COMMENTS == undefined) {
                    swal("Error!", "Please Enter Invoice Comments.", "error");
                    return;
                }

                if (!$scope.filteredPendingPOsList.attachmentsArray ||  $scope.filteredPendingPOsList.attachmentsArray.length <= 0 ) {
                    swal("Error!", "Please Select Atleast one  Attachments.", "error");
                    return;
                }

            }

            $scope.pendingPOItems.forEach(function (item) {
                if (parseFloat(item.TOTAL_INVOICE_LINE_ITEM_REMAINING_QTY) == 0 && item.INVOICE_QTY == 0) {
                    item.isErrorGrossWeightValidation = true;
                    $scope.isErrorGrossWeightValidation = true;
                }
               
                if ($scope.invoiceID == 0) {
                    if ((parseFloat(item.TOTAL_INVOICE_LINE_ITEM_REMAINING_QTY) !==0 && ((parseFloat(item.INVOICE_QTY) > parseFloat(item.TOTAL_INVOICE_LINE_ITEM_REMAINING_QTY))) || item.INVOICE_QTY == undefined)) {
                        item.isErrorWeightValidationRemaining = true;
                        $scope.isErrorWeightValidationRemaining = true;
                    }
                } else {
                    if ((parseFloat(item.TOTAL_INVOICE_LINE_ITEM_REMAINING_QTY) !== 0 && ((parseFloat(item.INVOICE_QTY) > (parseFloat(item.INVOICE_QTY_TEMP) + parseFloat(item.TOTAL_INVOICE_LINE_ITEM_REMAINING_QTY)))))) {
                        item.isErrorWeightValidation = true;
                        $scope.isErrorWeightValidation = true;
                    }

                }
            })
            //if ($scope.isErrorRemaining) {
            //    return;
            //}
            if ($scope.isErrorWeightValidation || $scope.isErrorWeightValidationRemaining || $scope.isErrorGrossWeightValidation || $scope.invoiceNumberError || $scope.invoiceDateError || $scope.dueDateError || $scope.invoiceTypeError) {
                return;
            }


            $scope.pendingPOItems.forEach(function (item,index) {
                var isError = '';
                item.PO_NUMBER = $scope.filteredPendingPOsList.PO_NUMBER;
                item.CUSTOMER_NAME = $scope.filteredPendingPOsList.VENDOR_COMPANY;
                item.VENDOR_CODE = $scope.filteredPendingPOsList.VENDOR_CODE;
                item.COMMENTS = $scope.filteredPendingPOsList.REMARKS;
                item.VENDOR_SITE_CODE = $scope.filteredPendingPOsList.VENDOR_SITE_CODE;
                item.CURRENCY = $scope.filteredPendingPOsList.CURRENCY;
                item.INVOICE_TYPE = $scope.filteredPendingPOsList.INVOICE_TYPE_LOOKUP;
                item.TRANSACTION_TYPE = $scope.filteredPendingPOsList.TRANSACTION_TYPE;
                item.ASN_NUMBER = $scope.filteredPendingPOsList.ASN_NUMBER;
                item.INDENT_OF_PURCHASE = $scope.filteredPendingPOsList.INDENT_OF_PURCHASE;
                item.REGISTERED_UNDER_GST = $scope.filteredPendingPOsList.REGISTERED_UNDER_GST;
                item.IS_RCM_APPLICABLE = $scope.filteredPendingPOsList.IS_RCM_APPLICABLE;
                item.EXCHANGE_RATE = $scope.filteredPendingPOsList.EXCHANGE_RATE;
                item.PAYMENT_TERMS = $scope.filteredPendingPOsList.PAYMENT_TERMS;
                item.PAYMENT_METHOD = $scope.filteredPendingPOsList.PAYMENT_METHOD;
                item.PAYGROUP_DESCRIPTION = $scope.filteredPendingPOsList.PAYGROUP_DESCRIPTION;
                item.IRN_NO = $scope.filteredPendingPOsList.IRN_NO;
                item.EWC = $scope.filteredPendingPOsList.EWC;
                item.INVOICE_ID = +$scope.invoiceID == 0 ? 0 : item.INVOICE_ID;
                item.INVOICE_NUMBER = $scope.filteredPendingPOsList.INVOICE_NUMBER;
                //item.INVOICE_TYPE = $scope.filteredPendingPOsList.INVOICE_TYPE;
                item.INVOICE_AMOUNT = $scope.filteredPendingPOsList.INVOICE_AMOUNT;
                item.TAX = $scope.filteredPendingPOsList.TAX;
                item.C_COMP_ID = +$scope.customerCompanyId;
                item.V_COMP_ID = +$scope.compID;
                item.SessionID = $scope.sessionID;
                item.VENDOR_ID = $scope.userID;
                item.attachmentsArray = $scope.filteredPendingPOsList.attachmentsArray;
                item.ACK_COMMENTS = $scope.filteredPendingPOsList.ACK_COMMENTS;
                item.INVOICE_COMMENTS = $scope.filteredPendingPOsList.INVOICE_COMMENTS;
                item.ROW_ID = $scope.filteredPendingPOsList.ROW_ID

                //item.IGST = $scope.filteredPendingPOsList.IGST;
                //item.CGST = $scope.filteredPendingPOsList.CGST;
                //item.SGST = $scope.filteredPendingPOsList.SGST;
            /*    item.attachmentsArray = $scope.selectedPODetails[index].attachmentsArray;*/
                item.isError = false;
                isError = $scope.isQtyError();
                if (isError) {
                    item.isError = true;
                }

                var ts = moment($scope.filteredPendingPOsList.INVOICE_DATE_1, "DD-MM-YYYY").valueOf();
                var m = moment(ts);
                var deliveryDate = new Date(m);
                var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                item.INVOICE_DATE = "/Date(" + milliseconds + "000+0530)/";

                var ts = moment($scope.filteredPendingPOsList.DUE_DATE_1, "DD-MM-YYYY").valueOf();
                var m = moment(ts);
                var deliveryDate = new Date(m);
                var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                item.DUE_DATE = "/Date(" + milliseconds + "000+0530)/";
            })

            if ($scope.areAllItemsValid()) {
                return;
            }

            var params = {
                poInvDet: $scope.pendingPOItems
            };

            PRMPOService.savePOInvoiceForm(params)
                .then(function (response) {

                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        growlService.growl("Saved Successfully.", "success");
                        $state.go("vendorPoInvoices", {});
                    }

                });
        }


        $scope.checkUserUniqueResult = function (idtype, inputvalue) {
            if (!$scope.checkInvoiceUniqueResult) {
                $scope.checkInvoiceUniqueResult = false;
            }
            if (inputvalue == "" || inputvalue == undefined) {
                $scope.isSaveDisabled = false;
                return false;
            }

            userService.checkUserUniqueResult(inputvalue, idtype).then(function (response) {
                if (idtype == "INVOICE") {
                    if ($scope.checkInvoiceUniqueResult = !response) {
                        $scope.checkInvoiceUniqueResult = !response;
                        $scope.isSaveDisabled = true;
                    } else {
                        $scope.isSaveDisabled = false;
                    }
                }
            });
        };
        $scope.isSaveDisabled = false;
        $scope.disableFirstApproverRejection = function () {
            $scope.isSaveDisabled = true;
            if ($scope.itemWorkflow && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                if ($scope.itemWorkflow[0].WorkflowTracks[0].createdBy === +$scope.userID && $scope.itemWorkflow[0].WorkflowTracks[0].status == 'REJECTED') {
                    $scope.isSaveDisabled = false;
                }
            }
        };


        $scope.isFormdisabled = false;

        if ($scope.isCustomer) {
            $scope.isFormdisabled = true;
            $scope.getItemWorkflow = function (submitInvoice, moduleId) {
                //let moduleId = rowID ? rowID : $scope.invoiceID;
                //if ($scope.invoiceNumber) {
                //    moduleId = +$scope.invoiceNumber.split('_')[2];
                //}

                workflowService.getItemWorkflow(0, moduleId, 'VENDOR_INVOICE')
                    .then(function (response) {
                        $scope.itemWorkflow = response;
                        if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                            $scope.currentStep = 0;

                            var count = 0;

                            $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {
                                if (!track.multipleAttachments) {
                                    track.multipleAttachments = [];
                                }

                                if (track.status === 'APPROVED') { track.statusNew = 'Approved'; }
                                if (track.status === 'HOLD') { track.statusNew = 'Hold'; }
                                if (track.status === 'PENDING') { track.statusNew = 'Pending'; }
                                if (track.status === 'REJECTED') { track.statusNew = 'Rejected'; }

                                if (track.status === 'APPROVED' || track.status === 'HOLD') {
                                    $scope.isFormdisabled = true;
                                }

                                if (track.status === 'APPROVED') {
                                    $scope.isWorkflowCompleted = true;
                                    $scope.orderInfo = track.order;
                                    $scope.assignToShow = track.status;

                                }
                                else {
                                    $scope.isWorkflowCompleted = false;
                                }

                                if (track.status === 'REJECTED' && count == 0) {
                                    count = count + 1;
                                }

                                if ((track.status === 'PENDING' || track.status === 'HOLD') && count == 0) {
                                    count = count + 1;
                                    $scope.currentAccess = track.order;
                                }

                                if ((track.status === 'PENDING' || track.status === 'HOLD' || track.status === 'REJECTED') && $scope.currentStep === 0) {
                                    $scope.currentStep = track.order;
                                    return false;
                                }
                            });

                            $scope.disableFirstApproverRejection();
                            if (submitInvoice && $scope.isSaveDisabled) {
                                $scope.params = {
                                    "invoicenumber": $scope.invoiceNumber,
                                    "sessionid": $scope.sessionID
                                };

                                PRMPOService.sendForInvoiceIntegration($scope.params)
                                    .then(function (response) {
                                        if (response && !response.errorMessage) {
                                            location.reload();
                                        } else {
                                            console.log(response.errorMessage);

                                            swal({
                                                title: "Failure",
                                                text: "Failed to send Invoice to Oracle",
                                                type: "error",
                                                showCancelButton: false,
                                                confirmButtonColor: "#DD6B55",
                                                confirmButtonText: "Ok",
                                                closeOnConfirm: true
                                            },
                                                function () {
                                                    return;
                                                });
                                        }                                        
                                    });
                            }
                        }
                    });
            };

            $scope.updateTrack = function (step, status) {
                $scope.disableAssignPR = true;
                $scope.commentsError = '';
                step.isLastApprover = false;
                if (step.comments != null || step.comments != "" || step.comments != undefined) {
                    step.comments = validateStringWithoutSpecialCharacters(step.comments);
                }
                var tempArray = $scope.itemWorkflow[0].WorkflowTracks[$scope.itemWorkflow[0].WorkflowTracks.length - 1];
                if (step.order == tempArray.order && status === 'APPROVED') {
                    $scope.disableAssignPR = false;
                    step.isLastApprover = true;
                } else {
                    $scope.disableAssignPR = true;
                }

                if ($scope.isReject) {
                    $scope.commentsError = 'Please Save Rejected Items/Qty';
                    return false;
                }

                if (status === 'REJECTED' && (step.comments == null || step.comments == "")) {
                    $scope.commentsError = 'Please enter comments';
                    return false;
                }

                step.status = status;
                step.sessionID = $scope.sessionID;
                step.modifiedBy = userService.getUserId();

                step.moduleName = 'VENDOR_INVOICE';

                step.subModuleName = '';
                step.subModuleID = 0;

                workflowService.SaveWorkflowTrack(step)
                    .then(function (response) {
                        if (response.errorMessage) {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            //$scope.getItemWorkflow(false);
                            location.reload();
                        }
                    });
            };

            //$scope.getItemWorkflow();

            function validateStringWithoutSpecialCharacters(string) {
                if (string) {
                    string = string.replace(/\'/gi, "");
                    string = string.replace(/\"/gi, "");
                    string = string.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                    string = string.replace(/(\r\n|\n|\r)/gm, "");
                    string = string.replace(/\t/g, '');
                    return string;
                }
            }

            $scope.isApproverDisable = function (index) {

                var disable = true;

                var previousStep = {};

                $scope.itemWorkflow[0].WorkflowTracks.forEach(function (step, stepIndex) {

                    if (index == stepIndex) {
                        if (stepIndex == 0) {
                            if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                                (step.status === 'PENDING' || step.status === 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                        else if (stepIndex > 0) {
                            if (previousStep.status === 'PENDING' || previousStep.status === 'HOLD' || previousStep.status === 'REJECTED') {
                                disable = true;
                            }
                            else if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                                (step.status === 'PENDING' || step.status === 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                    }
                    previousStep = step;
                });

                return disable;
            };


            $scope.deptIDs = [];
            $scope.desigIDs = [];
            $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
            if ($scope.ListUserDepartmentDesignations && $scope.ListUserDepartmentDesignations.length > 0) {
                $scope.ListUserDepartmentDesignations.forEach(function (item, index) {
                    $scope.deptIDs.push(item.deptID);
                    item.listDesignation.forEach(function (item1, index1) {
                        if (item1.isAssignedToUser && item1.isValid) {
                            $scope.desigIDs.push(item1.desigID);
                        }
                    });
                });
            }


            $scope.isUserBelongsToDeptandDesig = function (deptID, desigID) {
                var isEligible = true;

                if ($scope.deptIDs.indexOf(deptID) != -1 && $scope.desigIDs.indexOf(desigID) != -1) {
                    isEligible = true;
                } else {
                    isEligible = false;
                }

                return isEligible;
            };

            $scope.showApprovedDate = function (date) {
                return userService.toLocalDate(date);
            };
        }

        $scope.completeInvoice = function (item) {
            var params =
            {
                "INVOICE_NUMBER": item[0].INVOICE_NUMBER,
                "INVOICE_ID": +$scope.invoiceID,
                "uId": +$scope.userID,
                "sessionid": userService.getUserToken()
            };

            PRMPOService.completeInvoice(params)
                .then(function (response) {

                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        growlService.growl("Saved Successfully.", "success");
                        location.reload();
                    }
                });
        };
 
        $scope.totalAttachmentMaxSize = 6291456;
        $scope.totalASNSize = 0;
        $scope.totalASNItemSize = 0;

        $scope.getFile1 = function (id, itemid, ext) {
            $scope.filesTemp = $("#" + id)[0].files;
            $scope.filesTemp = Object.values($scope.filesTemp);
            $scope.showErrorDetails = [];
            $scope.totalASNSize = 0;
            if ($scope.filesTemp && $scope.filesTemp.length > 0) {
                $scope.filesTemp.forEach(function (item, index) {

                    if (ext && ext === 'pdf') {
                        $scope.totalASNSize = $scope.totalASNSize + item.size;
                        $scope.filesTemp.push(item);
                    } else {
                        if ($scope.showErrorDetails.length <= 0) {
                            $scope.showErrorDetails.push("Upload Only PDF Attachments.");
                        }
                    }
                });
            }

            if ($scope.showErrorDetails && $scope.showErrorDetails.length > 0) {
                swal("Error!", $scope.showErrorDetails[0], 'error');
                return;
            }

            if (($scope.totalASNSize + $scope.totalASNItemSize) > $scope.totalAttachmentMaxSize) {
                swal({
                    title: "Attachment size!",
                    text: "Total Attachments size cannot exceed 6MB",
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ok",
                    closeOnConfirm: true
                },
                    function () {
                        return;
                    });
                return;
            }

            $scope.filesTemp.forEach(function (attach, attachIndex) {
                $scope.file = $("#" + id)[0].files[attachIndex];

                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {
                        var fileUpload = {
                            fileStream: [],
                            fileName: '',
                            fileID: 0,
                            fileSize: 0
                        };
                        var bytearray = new Uint8Array(result);
                        fileUpload.fileSize = result.byteLength;
                        fileUpload.fileStream = $.makeArray(bytearray);
                        fileUpload.fileName = attach.name;
                        if (!$scope.filteredPendingPOsList.attachmentsArray) {
                            $scope.filteredPendingPOsList.attachmentsArray = [];
                        }

                        //$scope.filteredPendingPOsList.attachmentsArray = fileUpload;


                        var ifExists = _.findIndex($scope.filteredPendingPOsList.attachmentsArray, function (attach) { return attach.fileName.toLowerCase() === fileUpload.fileName.toLowerCase() });
                        if (ifExists <= -1 && $scope.filteredPendingPOsList.attachmentsArray.length < 1) {
                            $scope.filteredPendingPOsList.attachmentsArray.push(fileUpload);
                        } else {
                            $scope.filteredPendingPOsList.attachmentsArray = [];
                            $scope.filteredPendingPOsList.attachmentsArray.push(fileUpload);

                           // swal("Error!", "Cannot Upload More than 1 Attachmnets", 'error');
                        }

                    });
            })
        }

        $scope.removeAttach = function (index, item) {
            $scope.filteredPendingPOsList.attachmentsArray.splice(index, 1);
        }
        //$scope.removeAttach = function (index) {
        //    $scope.pendingPOItems[index].attachmentsArray.splice(index, 1);
        //}


        $scope.cancelInvoice = function () {

            var url = $state.href("vendorPoInvoices");
            $window.open(url, '_self');

        };

    });