prmApp
    .controller('dispatchTrackCtrl', ["$scope", "$state", "$stateParams", "userService", "auctionsService", "fileReader", "poService",
        function ($scope, $state, $stateParams, userService, auctionsService, fileReader, poService) {
        $scope.reqID = $stateParams.reqID;
        //$scope.poID = $stateParams.poID;
        //$scope.dTID = $stateParams.dTID;
            $scope.poOrderId = $stateParams.poOrderId;
            $scope.dCode = $stateParams.dCode;
       
        $scope.dispatchTrack = {};
        $scope.vendors = [];

        $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;


        $scope.getDispatchTrack = function () {
            poService.GetDispatchTrackList($scope.poOrderId, $scope.dCode)
                .then(function (response) {
                    $scope.dispatchTrack = response;

                    if ($scope.dispatchTrack) {
                        $scope.dispatchTrack.forEach(function (item, index) {
                            item.dispatchDate = new moment(item.dispatchDate).format("DD-MM-YYYY");
                            item.recivedDate = new moment(item.recivedDate).format("DD-MM-YYYY");

                            if (item.dispatchDate.indexOf('-9999') > -1) {
                                item.dispatchDate = "";
                            }
                            if (item.recivedDate.indexOf('-9999') > -1) {
                                item.recivedDate = "";
                            }

                            item.shipmentDate = new moment(item.shipmentDate).format("DD-MM-YYYY");
                            if (item.shipmentDate.indexOf('-9999') > -1) {
                                item.shipmentDate = "";
                            }

                            item.documentDate = new moment(item.documentDate).format("DD-MM-YYYY");
                            if (item.documentDate.indexOf('-9999') > -1) {
                                item.documentDate = "";
                            }

                            item.manufacDate = new moment(item.manufacDate).format("DD-MM-YYYY");
                            if (item.manufacDate.indexOf('-9999') > -1) {
                                item.manufacDate = "";
                            }

                            item.bestBeforeDate = new moment(item.bestBeforeDate).format("DD-MM-YYYY");
                            if (item.bestBeforeDate.indexOf('-9999') > -1) {
                                item.bestBeforeDate = "";
                            }

                        });
                    }
                    

                });
        }

        $scope.getDispatchTrack();




        $scope.saveDescPoInfo = function () {

            $scope.descPo.sessionID = userService.getUserToken();

            var ts = moment($scope.descPo.expectedDelivery, "DD-MM-YYYY").valueOf();
            var m = moment(ts);
            var deliveryDate = new Date(m);
            var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
            $scope.descPo.expectedDelivery = "/Date(" + milliseconds + "000+0530)/";

            var params = {
                'povendor': $scope.descPo
            }
            poService.saveDescPoInfo(params)
                .then(function (response) {
                    if (response.errorMessage == '') {
                        swal({
                            title: "Done!",
                            text: "Purchase Orders Generated Successfully.",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                            function () {
                                location.reload();
                            });
                    }
                })
        }


        $scope.getFile1 = function (id, doctype, ext) {
            $scope.progress = 0;
            $scope.file = $("#" + id)[0].files[0];
            $scope.docType = doctype + "." + ext;
            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {
                    if (id == "poFile") {
                        $scope.descPo.poFile = { "fileName": '', 'fileStream': null };
                        var bytearray = new Uint8Array(result);
                        $scope.descPo.poFile.fileStream = $.makeArray(bytearray);
                        $scope.descPo.poFile.fileName = $scope.file.name;
                    }

                });
        };


            $scope.goToDispatchTrackForm = function (purchaseID, dispatchCode) {
                //$state.go("reqTechSupport", { "reqId": $scope.reqId }, { reload: true, newtab: true });

                var url = $state.href("dtform", { reqID: $scope.reqID, "poOrderId": purchaseID, "dCode": dispatchCode });
                window.open(url, '_blank');
            };

            $scope.cancelPO = function () {
                //dispatchTrack[0].vendorPOObject.vendor
                if ($scope.isCustomer) {
                    $state.go("po-list", { "reqID": $scope.reqID, "vendorID": $scope.dispatchTrack[0].vendorPOObject.vendor.userID, "poID": 0 });
                } else if (!$scope.isCustomer) {
                    $state.go("po-list", { "reqID": $scope.reqID, "vendorID": userService.getUserId(), "poID": 0 });
                }
            };
    }]);