﻿prmApp
    .controller('pendingContractsCtrl', ["$rootScope", "$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMPRServices",
        "PRMCustomFieldService", "catalogService", "$location", "PRMUploadServices", "$element", "fileReader", "$timeout",
        function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPRServices, PRMCustomFieldService, catalogService, $location, PRMUploadServices, $element, fileReader, $timeout) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;
            $scope.compId = userService.getUserCompanyId();
            let requirementPRContacts = $rootScope.PRPendingContracts;
            //console.log(requirementPRContacts);


            $scope.exportItemsToExcel = function () {
                PRMUploadServices.GetExcelTemplate("CONTRACT_DETAILS");
            };


            $scope.getFile1 = function (id, itemid, ext) {
                $scope.filesTemp = $("#" + id)[0].files;
                $scope.filesTemp = Object.values($scope.filesTemp);
                $scope.filesTemp.forEach(function (attach, attachIndex) {
                    $scope.file = $("#" + id)[0].files[attachIndex];

                    fileReader.readAsDataUrl($scope.file, $scope)
                        .then(function (result) {
                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };
                            var bytearray = new Uint8Array(result);
                            $scope.attachment = $.makeArray(bytearray);
                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = attach.name;

                            if (id == "excelclientupload") {
                                $scope.uploadDetails('CONTRACT_DETAILS', 0, fileUpload.fileName);
                                $scope.file = [];
                                $scope.file.name = '';
                            } else {
                            }


                        });
                })
            };


            $scope.showErrorPopup = false;
            $scope.rowErrors = [];

            $scope.uploadDetails = function (type, proceedFurther, fileName) {
                $scope.showErrorPopup = false;
                $scope.FILE_NAME = fileName ? fileName : $scope.FILE_NAME;
                var params = {
                    name: type,
                    compID: $scope.compId,
                    sessionID: $scope.sessionID,
                    tableName: 'SAP_CONTRACT_DETAILS',
                    attachment: $scope.attachment,
                    proceedFurther: proceedFurther,
                    fileName: $scope.FILE_NAME
                };
                PRMUploadServices.uploadTemplate(params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            if (response[0].IS_SUCCESS) {
                                angular.element('#errorPopUp').modal('hide');
                                swal("Status!", "Total Count : " + response[0].TOTAL_COUNT + " \n " + " Successfull Count : " + response[0].SUCCESS_COUNT + " \n " + " Failed Count : " + response[0].FAILED_COUNT, "warning");
                                $("#excelclientupload").val(null);
                                $scope.GetMyPendingContracts(0, 10, '');
                            } else {
                                if (!response[0].IS_SUCCESS && proceedFurther === 1) {
                                    $scope.showErrorPopup = true;
                                    $scope.rowErrors = response;
                                    angular.element('#errorPopUp').modal('show');
                                    if (response[0].IS_NO_RECORDS) {
                                        swal("Status!", "Sheet is empty.. So cannot continue further ! Please validate the sheet again and upload.", "error");
                                    } else {
                                        swal("Status!", "Records in this sheet contains errors.. So cannot continue further ! Please validate the sheet again and upload.", "error");
                                    }
                                    $("#excelclientupload").val(null);
                                    angular.element('#errorPopUp').modal('hide');
                                } else {
                                    $scope.showErrorPopup = true;
                                    $scope.rowErrors = response;
                                    angular.element('#errorPopUp').modal('show');
                                    $("#excelclientupload").val(null);
                                }
                            }
                        } else {
                            $("#excelclientupload").val(null);
                            $scope.uploadDetails('CONTRACT_DETAILS', 1);
                        }
                    });

            };

            $scope.cancel = function () {
                angular.element('#errorPopUp').modal('hide');
            };

            $scope.exportErrorDetails = function (type) {
                $state.go('moduleErrorList', { 'moduleName': type });
            };

            $scope.getDate = function (value) {
                return userService.toLocalDate(value);
            }



            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;


            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
                $scope.GetMyPendingContracts(($scope.currentPage - 1), 10, '');
            };
            $scope.loadServices = false;
            $scope.setFilters = function (currentPage) {
                if ($scope.loadServices) {
                    $scope.GetMyPendingContracts(0, 10, $scope.filters.searchKeyword);
                }
            };
            $scope.filters = {};
            $scope.filters.toDate = moment().format('YYYY-MM-DD');
            $scope.filters.fromDate = moment().subtract(30, "days").format("YYYY-MM-DD");

            $scope.filterByDate = function () {
                $scope.getFilterValues();
                $scope.GetMyPendingContracts(0, 10, $scope.filters.searchKeyword);
            };

            /*PAGINATION CODE*/

            $scope.supplierArr = [];
            $scope.itemArr = [];
            $scope.catArr = [];

            $scope.GetMyPendingContracts = function (recordsFetchFrom, pageSize, searchString, downloadToExcel) {
                $scope.MyPendingContracts = [];
                var  numbers;
                if (_.isEmpty($scope.filters.contractNo)) {
                    numbers = '';
                } else if ($scope.filters.contractNo && $scope.filters.contractNo.length > 0) {
                    var f1 = _($scope.filters.contractNo)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    numbers = f1.join(',');
                }

                $scope.tableValues = [];

                $scope.rows = [];
                var params =
                {
                    "companyId": +$scope.compId,
                    "sessionid": userService.getUserToken(),
                    "searchString": searchString ? searchString : "",
                    "fromDate": $scope.filters.fromDate,
                    "toDate": $scope.filters.toDate,
                    "contractNo": numbers,
                    "fromExcel": downloadToExcel ? 1 :0 ,
                    "PageSize": recordsFetchFrom * pageSize,
                    "NumberOfRecords": pageSize
                };

                catalogService.GetMyPendingContracts(params)
                    .then(function (response) {
                        if (downloadToExcel) {
                            $scope.MyPendingContracts = [];
                            if (response && response.length > 0) {
                                response.forEach(function (item, index) {
                                    item.startTime = userService.toLocalDate(item.startTime).split(' ')[0];
                                    item.endTime = userService.toLocalDate(item.endTime).split(' ')[0];
                                    item.CREATED_DATE = userService.toLocalDate(item.CREATED_DATE).split(' ')[0];
                                    $scope.MyPendingContracts.push(item);
                                });
                                var mystyle = {
                                    headers: true,
                                    column: { style: { Font: { Color: "#FFFFFF" }, Interior: { Color: "#4F81BD", Pattern: "Solid" }, Alignment: { Horizontal: "Center" } } },
                                    row: { style: { Alignment: { Horizontal: "Center" } } }
                                };

                                alasql('SELECT contractNumber AS [Contract Number], startTime AS [Contract StartTime], endTime as [Contract Endtime], CREATED_DATE as [Date Created],' +
                                    'ProductCode as [Product Code ],companyName as [Company Name], VENDOR_CODE [Vendor Code ],DELIVERY_FROM as [Bill To],DELIVERY_AT as [Ship To ],' +
                                    'quantity as [Quantity],contractValue as [Price], PAYMENT_TERMS as [Payment Terms], CURRENCY as [Currency], DISCOUNT as [Discount], NET_PRICE as [Net Price]' +
                                    'INTO XLSX(?, { headers: true, sheetid: "CONTRACT_DETAILS", style: "font-size:15px;background:#233646;color:#FFF;" }) FROM ? ',
                                    ["Contract Details.xlsx", $scope.MyPendingContracts]);
                            }
                        } else {
                            $scope.MyPendingContracts = [];
                            $scope.filteredContractList = [];
                            if (response && response.length > 0) {
                                response.forEach(function (item, index) {
                                        item.startTime = userService.toLocalDate(item.startTime).split(' ')[0];
                                        item.endTime = userService.toLocalDate(item.endTime).split(' ')[0];
                                        item.CREATED_DATE = userService.toLocalDate(item.CREATED_DATE).split(' ')[0];
                                    $scope.MyPendingContracts.push(item);
                                });
                                $scope.filteredContractList = $scope.MyPendingContracts;
                                $scope.totalItems = $scope.filteredContractList[0].totalNoOfContracts;
                            } else {
                                $scope.filteredContractList = [];
                                $scope.totalItems = 0;
                            }
                        }
 
                    });
            };


            if (!requirementPRContacts) {
                $scope.GetMyPendingContracts(0, 10, '');
            }
            if (requirementPRContacts && requirementPRContacts.length > 0) {
                $scope.filters.searchKeyword = requirementPRContacts[0].PRODUCT_ID;
                $scope.GetMyPendingContracts(0, 0, $scope.filters.searchKeyword);
            }
            $scope.filtersList = {
                contractNoList: []
            };
            $scope.filterValues = [];
            $scope.loadServices = false;
            $scope.getFilterValues = function () {
                var params =
                {
                    "COMP_ID": $scope.compId,
                    'fromDate': $scope.filters.fromDate,
                    'toDate': $scope.filters.toDate,
                };

                let contractNoListTemp = [];
                let vendorListTemp = [];

                catalogService.getcontractFilterValues(params)
                    .then(function (response) {
                        $scope.loadServices = true;
                        if (response && response.length > 0) {
                            $scope.filterValues = response;
                            if ($scope.filterValues && $scope.filterValues.length > 0) {
                                $scope.filterValues.forEach(function (item, index) {
                                    if (item.TYPE === 'CONTRACT') {
                                        contractNoListTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'VENDOR') {
                                        vendorListTemp.push({ id: item.ID, name: item.NAME });
                                    }
                                });

                                $scope.filtersList.contractNoList = contractNoListTemp;
                                $scope.filtersList.vendorList = vendorListTemp;

                            }
                        }
                    });

            };
            $scope.getFilterValues();


            $scope.cancel = function () {
                angular.element('#errorPopUp').modal('hide');
            };


            $scope.downloadToExcel = function () {
                $scope.GetMyPendingContracts(0, 0, $scope.filters.searchKeyword, true);
            };

            $scope.EditDeleteRecord = function (contract) {
                swal({
                    title: "Are You Sure!",
                    text: 'Do You want to Delete the Contract Permanently',
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes",
                    closeOnConfirm: true
                }, function () {
                    contract.isValid = 0;
                    contract.startTime = null;
                    contract.endTime = null;
                    contract.CREATED_DATE = null;
                    var params = {
                        "contract": contract
                    };
                    catalogService.SaveProductContract(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                                growlService.growl("Contract has been deleted.", "success");
                                location.reload();
                            }
                        });
                });



            }

        }]);