﻿prmApp
    .controller('gstSettingsCtrl', ["$scope", "userService", "auctionsService", "growlService", "$http", "domain",
        "$location", "$anchorScroll",
        function ($scope, userService, auctionsService, growlService, $http, domain, $location, $anchorScroll) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.compID = userService.getUserCompanyId();
            $scope.gstFactors = [];
            $scope.gstFactorAudits = [];
            $scope.displayCard = 0;
            $scope.minDateMoment = moment().subtract(0, 'day');
            $scope.gst = {
                USER_ID: +$scope.userID,
                GST_ID: 0,
                ENTITY: '',
                COMP_ID: +$scope.compID,
                CGST: 0,
                SGST: 0,
                IGST: 0,
                TAX_CATEGORY_NAME: '',
                TAX_RATE: 0,
                CGST_DISABLE: false,
                SGST_DISABLE: false,
                IGST_DISABLE: false,
                IS_RCM_APPLICABLE : '',
                TAX_CATEGORY_TYPE : ''
            };

            //$scope.gstRCM = [{ 'configValue': 'Y' }, { 'configValue': 'N' }];

            //$scope.gstEWC = [{ 'configValue': 'Y' }, { 'configValue': 'N' }];

            $scope.catType = [{ 'configValue': 'RCM' }, { 'configValue': 'Recoverable' }, { 'configValue': 'Non Recoverable' }];

            $scope.gstCodeDisabled = false;
            $scope.startDateDisabled = false;
            $scope.gstEndDate = '';

            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.totalItemAudits = 0;
            $scope.currentPage = 1;
            $scope.currentPage1 = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;

            $scope.isCurrEdit = {
                isEdit: 0
            }

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };


            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };
            /*PAGINATION CODE*/


            $scope.currencies = [];
            $scope.addNewGstView = false;
            $scope.GetGstFactors = function () {
                auctionsService.GetGstFactors($scope.userID, $scope.sessionID, $scope.compID)
                    .then(function (response) {
                        $scope.gstFactors = [];
                        $scope.gstFactors = response;

                        $scope.gstFactors1 = $scope.gstFactors;
                        $scope.totalItems = $scope.gstFactors.length;
                    });
            };

            $scope.GetGstFactors();

            
            $scope.editGSTFactor = function (gst) {
                $scope.addNewGSTView = true;
                $scope.gst = gst;
                
            };

            $scope.changeGST = function (gst) {
                gst.SGST_DISABLE = false;
                gst.CGST_DISABLE = false;
                gst.IGST_DISABLE = false;
                if (gst.CGST > 0) {
                    gst.SGST = gst.CGST;
                    gst.IGST_DISABLE = true;
                } else if (gst.IGST > 0) {
                    gst.SGST_DISABLE = true;
                    gst.CGST_DISABLE = true;
                } else if (gst.SGST > 0) {
                    gst.CGST = gst.SGST;
                    gst.IGST_DISABLE = true;
                }
            }

            $scope.SaveGstFactor = function () {

                if (!$scope.gst.ENTITY) {
                    growlService.growl("Please select Entity.", 'inverse');
                    return;
                }

                //if (!$scope.gst.IS_RCM_APPLICABLE) {
                //    growlService.growl("Please select Is RCM Applicable.", 'inverse');
                //    return;
                //}

                //if (!$scope.gst.EWC) {
                //    growlService.growl("Please select EWC.", 'inverse');
                //    return;
                //}
                if (!$scope.gst.TAX_CATEGORY_NAME) {
                    growlService.growl("Please enter Tax Category Name.", 'inverse');
                    return;
                }
                if (!$scope.gst.TAX_CATEGORY_TYPE) {
                    growlService.growl("Please enter Tax Category Type.", 'inverse');
                    return;
                }
                //if (!$scope.gst.TAX_RATE) {
                //    growlService.growl("Please enter Tax rate.", 'inverse');
                //    return;
                //}


                $scope.gst.COMP_ID = $scope.compID;
                $scope.gst.USER_ID = $scope.userID;

                var params = {
                    "gstFactor": $scope.gst
                };

                auctionsService.SaveGstFactor(params)
                    .then(function (response) {
                        $scope.addNewGSTView = false;
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                            $scope.addNewGSTView = true;
                        }
                        else {
                            growlService.growl("GstFactor Saved Successfully.", "success");
                            //location.reload();
                            $scope.GetGstFactors();
                            $scope.gst = {
                                USER_ID: $scope.userID,
                                GST_ID: 0,
                                ENTITY: '',
                                COMP_ID: $scope.compID,
                                CGST: 0,
                                SGST: 0,
                                IGST: 0,
                                TAX_CATEGORY_NAME: '',
                                TAX_RATE: 0,
                                CGST_DISABLE: false,
                                SGST_DISABLE: false,
                                IGST_DISABLE: false,
                                //IS_RCM_APPLICABLE: '',
                                //EWC:'',
                                TAX_CATEGORY_TYPE:''
                            };
                        }
                    });


            };

            $scope.closeGstFactor = function () {
                $scope.addNewGSTView = false;
                $scope.gst = {
                    USER_ID: $scope.userID,
                    GST_ID: 0,
                    ENTITY: '',
                    COMP_ID: $scope.compID,
                    CGST: 0,
                    SGST: 0,
                    IGST: 0,
                    TAX_CATEGORY_NAME: '',
                    TAX_RATE: 0,
                    CGST_DISABLE: false,
                    SGST_DISABLE: false,
                    IGST_DISABLE: false,
                    //IS_RCM_APPLICABLE: '',
                    //EWC:'',
                    TAX_CATEGORY_TYPE: ''
                };
            };

            function scrollToElement(elementId) {
                $location.hash(elementId);
                $anchorScroll();
            }


            $scope.DeleteGSTFactor = function (gst) {

                var params = {
                    gstID: gst.GST_ID,
                    sessionID: $scope.sessionID
                };

                auctionsService.DeleteGSTFactor(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("GST Deleted Successfully.", "success");
                            $scope.GetGstFactors();
                            //$window.history.back();
                        }
                    });
                $scope.addnewdeptView = false;
            };

            
        }]);

