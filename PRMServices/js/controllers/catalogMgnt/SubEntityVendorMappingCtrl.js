﻿//(function () { 
prmApp.controller('SubEntityVendorMappingCtrl', ['$scope', '$state', '$rootScope','$window', '$stateParams', '$filter', 'catalogService', 'userService', 'growlService',
    function ($scope, $state, $rootScope,$window, $stateParams, $filter, catalogService, userService, growlService) {

        $scope.compId = userService.getUserCompanyId();
        $scope.userObj = userService.getUserObj();
        var loginUserData = userService.getUserObj();
        this.userId = userService.getUserId();
        $scope.isOTPVerified = loginUserData.isOTPVerified;
        $scope.isEmailOTPVerified = loginUserData.isEmailOTPVerified;
        $scope.currentPage = 1;
        $scope.currentPage2 = 1;
        $scope.itemsPerPage = 5;
        $scope.itemsPerPage2 = 5;
        $scope.maxSize = 5;
        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };
        $scope.page = 0;
        $scope.PageSize = 0;
        $scope.fetchRecordsFrom = $scope.page * $scope.PageSize;
        $scope.totalCount = 0;

        $scope.ProductVendors = [];
        $scope.count = 0;

        $scope.list = {
            seId:0,
            subEntity: '',
            companyvendors: []
        }

        $scope.GetCompanyVendors = function (IsPaging, searchString) {
            if (searchString) {
                $scope.fetchRecordsFrom = 0;
            }
            $scope.vendorList1 = [];
            $scope.isOTPVerified = loginUserData.isOTPVerified;
            $scope.isEmailOTPVerified = loginUserData.isEmailOTPVerified;
            $scope.params = { "userID": userService.getUserId(), "sessionID": userService.getUserToken(), "PageSize": $scope.fetchRecordsFrom, "NumberOfRecords": $scope.PageSize, "searchString": searchString ? searchString : "" };
            if ($scope.isOTPVerified && $scope.isEmailOTPVerified) {
                userService.GetCompanyVendors($scope.params)
                    .then(function (response) {
                        
                        response.forEach(function (item) {
                            item.companyVendorCodes = _.uniq(item.companyVendorCodes.split(',').map(function (item) { return item.trim() })).join(',');
                            //var vendorObj = {
                            //    "firstName": item.firstName,
                            //    "lastName": item.lastName,
                            //    "userID": item.userID
                            //}
                            //$scope.vendorList1.push(vendorObj);
                        })
                        $scope.vendorsList = response;
                        $scope.vendorsList = _.filter($scope.vendorsList, function (o) { return o.sapVendorCode != '' || null; });
                    });

            }
        };

        $scope.GetCompanyVendors();

        $scope.productSubEntities = [];

        $scope.GetProductSubEntities = function () {
            catalogService.GetProductSubEntities(userService.getUserCompanyId())
                .then(function (response) {
                    $scope.productSubEntities = response;
                    $scope.productSubEntities.forEach(function (item) {
                        if (item.VENDORS_LIST) {
                            item.vendorList = JSON.parse(item.VENDORS_LIST);
                            item.VENDOR_NAMES = item.vendorList.map(function (e) {
                                return e.u_fname + ' ' + e.u_lname + ' ' + e.VENDOR_CODE;
                            }).join(',');
                        }
                        
                    })
                    $scope.totalItems = $scope.productSubEntities.length;
                })
        }

        $scope.GetProductSubEntities();

        $scope.editSubEntity = function (sub) {
            $scope.addnewEntityView = true;
            $scope.list.companyvendors = [];
            $scope.list.subEntity = sub.SUB_ENTITY_NAME;
            //$scope.list.companyvendors = sub.vendorList;
            $scope.list.companyvendors = $scope.vendorsList.filter(function (obj) {
                return sub.vendorList.some(function (obj2) {
                    return obj2.u_id == obj.userID;
                });
            });
            $scope.list.seId = sub.SE_ID;
            //console.log(uniqueResultTwo)
            //$scope.list.companyvendors = [];
        }

        $scope.closeEditSubEntity = function () {
            $scope.addnewEntityView = false;
            $scope.list = {
                seId: 0,
                subEntity: '',
                companyvendors: []
            }
        };

        $scope.saveProductSubEntity = function (list) {

            if ($scope.list.subEntity == undefined || $scope.list.subEntity == "") {
                growlService.growl("please Enter SunEntity.", 'inverse');
                return;
            }
            if ($scope.list.companyvendors == undefined || $scope.list.companyvendors == "") {
                growlService.growl("please select vendors.", 'inverse');
                return;
            }
            

            else if (_.isEmpty(list.companyvendors)) {
                vendor = '';
            } else if (list.companyvendors && list.companyvendors.length > 0) {
                var vendors = _(list.companyvendors)
                    .filter(item => item.userID)
                    .map('userID')
                    .value();
                vendor = vendors.join(',');
            }
            var params = {
                'seId': list.seId,
                'companyId': $scope.compId,
                'subEntityName': list.subEntity,
                'vendorId': vendor,
                'userId': userService.getUserId(),
                'seessionId': userService.getUserToken()
            }
            catalogService.saveProductSubEntity(params)
                .then(function (response) {
                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");

                    }
                    else if (response.errorMessage == 'Sub Entity Already exists') {
                        growlService.growl("Sub Entity Already exists");
                    }
                    else {
                        growlService.growl("Added Successfully.", "success");
                        $scope.list = {
                            seId: 0,
                            subEntity: '',
                            companyvendors: []
                          }
                $scope.GetProductSubEntities();
                         }  
                });
        }
        
    }]);
