﻿prmApp
    .controller('listSubEntityCtrl', ["$rootScope", "$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMPRServices",
        "PRMCustomFieldService", "catalogService", "$location", "PRMUploadServices", "$element", "fileReader","$timeout",
        function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPRServices, PRMCustomFieldService, catalogService, $location, PRMUploadServices, $element, fileReader, $timeout) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;
            $scope.compId = userService.getUserCompanyId();
            


            $scope.exportItemsToExcel = function () {
                PRMUploadServices.GetExcelTemplate("SUB_ENTITY_DETAILS");
            };


            $scope.getFile1 = function (id, itemid, ext) {
                $scope.filesTemp = $("#" + id)[0].files;
                $scope.filesTemp = Object.values($scope.filesTemp);
                $scope.filesTemp.forEach(function (attach, attachIndex) {
                    $scope.file = $("#" + id)[0].files[attachIndex];

                    fileReader.readAsDataUrl($scope.file, $scope)
                        .then(function (result) {
                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };
                            var bytearray = new Uint8Array(result);
                            $scope.attachment = $.makeArray(bytearray);
                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = attach.name;

                            if (id == "excelclientupload") {
                                $scope.uploadDetails('SUB_ENTITY_DETAILS', 0, fileUpload.fileName);
                                $scope.file = [];
                                $scope.file.name = '';
                            } else {
                            }


                        });
                })
            };


            $scope.showErrorPopup = false;
            $scope.rowErrors = [];

            $scope.uploadDetails = function (type, proceedFurther, fileName) {
                $scope.showErrorPopup = false;
                $scope.FILE_NAME = fileName ? fileName : $scope.FILE_NAME;
                var params = {
                    name: type,
                    compID: $scope.compId,
                    sessionID: $scope.sessionID,
                    tableName: 'SAP_SUB_ENTITY_DETAILS',
                    attachment: $scope.attachment,
                    proceedFurther: proceedFurther,
                    fileName: $scope.FILE_NAME
                };
                PRMUploadServices.uploadTemplate(params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            if (response[0].IS_SUCCESS) {
                                angular.element('#errorPopUp').modal('hide');
                                swal("Status!", "Total Count : " + response[0].TOTAL_COUNT + " \n " + " Successfull Count : " + response[0].SUCCESS_COUNT + " \n " + " Failed Count : " + response[0].FAILED_COUNT, "warning");
                                $("#excelclientupload").val(null);
                                $scope.GetSubEntityList(0, 10, '');
                            } else {
                                if (!response[0].IS_SUCCESS && proceedFurther === 1) {
                                    $scope.showErrorPopup = true;
                                    $scope.rowErrors = response;
                                    angular.element('#errorPopUp').modal('show');
                                    if (response[0].IS_NO_RECORDS) {
                                        swal("Status!", "Sheet is empty.. So cannot continue further ! Please validate the sheet again and upload.", "error");
                                    } else {
                                        swal("Status!", "All the records in this sheet contains errors.. So cannot continue further ! Please validate the sheet again and upload.", "error");
                                    }
                                    $("#excelclientupload").val(null);
                                    angular.element('#errorPopUp').modal('hide');
                                } else {
                                    $scope.showErrorPopup = true;
                                    $scope.rowErrors = response;
                                    angular.element('#errorPopUp').modal('show');
                                    $("#excelclientupload").val(null);
                                }
                            }
                        } else {
                            $("#excelclientupload").val(null);
                            $scope.uploadDetails('SUB_ENTITY_DETAILS', 1);
                        }
                    });

            };

            $scope.cancel = function () {
                angular.element('#errorPopUp').modal('hide');
            };

            $scope.exportErrorDetails = function (type) {
                $state.go('moduleErrorList', { 'moduleName': type });
            };

            $scope.getDate = function (value) {
                return userService.toLocalDate(value);
            }



            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;


            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
                $scope.GetSubEntityList(($scope.currentPage - 1), 10, '');
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };
            /*PAGINATION CODE*/
            $scope.tableColumns = [];
            $scope.GetSubEntityList = function (recordsFetchFrom, pageSize, searchString,downloadToExcel) {

                var params =
                {
                    "companyId": +$scope.compId,
                    "sessionid": userService.getUserToken(),
                    "searchString": searchString ? searchString : "",
                    "PageSize": recordsFetchFrom * pageSize,
                    "NumberOfRecords": pageSize
                };

                catalogService.GetSubEntityList(params)
                    .then(function (response) {
                        if (response) {
                            var arr = JSON.parse(response).Table;
                            if (arr && arr.length > 0) {
                                $scope.totalItems = arr[0].TOTAL_COUNT;
                                arr.forEach(a => delete a.TOTAL_COUNT);
                                //arr.forEach(a => delete a.VENDORS_FIRST_LIST);
                                //arr.forEach(a => delete a.VENDORS_SECOND_LIST);
                                $scope.tableColumnsTemp = angular.copy(_.keys(arr[0]));
                                $scope.tableColumnsTemp.forEach(function (item, index) {
                                    item = item.replaceAll("_", " ");
                                    var foundIndex = _.findIndex($scope.tableColumns, function (val) { return val === item; });
                                    if (foundIndex <= -1)
                                    {
                                        $scope.tableColumns.push(item);
                                    }
                                });
                                $scope.rows = arr;
                                arr.forEach(function (item, index) {
                                    if (item.FIRST_PRIORITY_VENDORS) {
                                        var arr = item.FIRST_PRIORITY_VENDORS.split(',');
                                        item.FIRST_PRIORITY_VENDORS = _.uniqBy(arr).join(',');
                                    }
                                    if (item.SECOND_PRIORITY_VENDORS) {
                                        var arr1 = item.SECOND_PRIORITY_VENDORS.split(',');
                                        item.SECOND_PRIORITY_VENDORS = _.uniqBy(arr1).join(',');
                                    }
                                    var obj = angular.copy(_.values(item));
                                    if (obj) {
                                        item.tableValues = [];
                                        obj.forEach(function (value, valueIndex) {
                                            item.tableValues.push(value);
                                        });
                                    }
                                });
                            } else {
                                var arr = JSON.parse(response);
                                arr = arr.filter(function (a) { return a !== 'TOTAL_COUNT' });
                                $scope.tableColumnsTemp = arr;
                                $scope.tableColumnsTemp.forEach(function (item, index) {
                                    item = item.replaceAll("_", " ");
                                    $scope.tableColumns.push(item);
                                });
                            }

                            if (downloadToExcel)
                            {
                                $scope.excelRows = [];
                                $scope.excelColumnsTemp = [];
                                $scope.excelColumns = [];
                                $scope.rows.forEach(function (rowItem, rowIndex) {
                                    var newObj = Object.assign({},rowItem);
                                    delete newObj.tableValues;
                                    $scope.excelColumnsTemp.push(newObj);
                                });
                                if ($scope.excelColumnsTemp && $scope.excelColumnsTemp.length > 0) {
                                    Object.keys($scope.excelColumnsTemp[0]).forEach(function (key, index) {
                                        var value = key + ' as [' + key.replaceAll("_", " ") + ']';
                                        $scope.excelColumns.push(value);
                                    });
                                    $scope.excelColumns = $scope.excelColumns.join(',');

                                    $scope.excelColumnsTemp.forEach(function (item, index) {
                                        $scope.excelRows.push(item);
                                    });
                                }
                                execute();
                                $scope.rows = arr.slice(0,10);
                            }
                        } else {
                            if ($scope.tableColumnsTemp && $scope.tableColumnsTemp.length > 0) {
                                $scope.tableColumnsTemp.forEach(function (item, index) {
                                    item = item.replaceAll("_", " ");
                                    var foundIndex = _.findIndex($scope.tableColumns, function (val) { return val === item; });
                                    if (foundIndex <= -1) {
                                        $scope.tableColumns.push(item);
                                    }
                                });
                            }
                            swal("Status!", "No Records Found..! Please Upload Some Data.", "error");
                        }
                    });
            };

            $scope.GetSubEntityList(0, 10, '');

            $scope.showEdit = false;
            $scope.editEachRecord = function (eachRow)
            {
                eachRow.companyvendors = [];
                eachRow.companyvendors1 = [];
                $scope.GetCompanyVendors(0,'',eachRow);
            };

            $scope.GetCompanyVendors = function (IsPaging, searchString, eachRow) {
                if (searchString) {
                    $scope.fetchRecordsFrom = 0;
                }
                $scope.vendorList1 = [];
                $scope.params = { "userID": userService.getUserId(), "sessionID": userService.getUserToken(), "PageSize": 0, "NumberOfRecords": 0, "searchString": searchString ? searchString : "" };
                userService.GetCompanyVendors($scope.params)
                    .then(function (response) {

                        response.forEach(function (item) {
                            item.companyVendorCodes = _.uniq(item.companyVendorCodes.split(',').map(function (item) { return item.trim() })).join(',');
                        })
                        $scope.showEdit = true;
                        $scope.vendorsList = response;
                        $scope.vendorsList = _.filter($scope.vendorsList, function (o) { return o.sapVendorCode != '' || null; });
                        $scope.rowValue = {};
                        catalogService.GetSubEntity({ "SE_NUMBER": eachRow.SE_NUMBER, "COMP_ID": +$scope.compId, "sessionid": userService.getUserToken()})
                            .then(function (response) {
                                if (response) {
                                    var arr = JSON.parse(response).Table;

                                    var a  = arr[0].FIRST_PRIORITY_VENDORS;
                                    var b = arr[0].SECOND_PRIORITY_VENDORS;

                                    eachRow.FIRST_PRIORITY_VENDORS_TEMP = JSON.parse(a);
                                    eachRow.SECOND_PRIORITY_VENDORS_TEMP = JSON.parse(b);

                                    if (eachRow.FIRST_PRIORITY_VENDORS_TEMP) {
                                        eachRow.companyvendors = $scope.vendorsList.filter(function (obj) {
                                            return eachRow.FIRST_PRIORITY_VENDORS_TEMP.some(function (obj2) {
                                                return obj2.u_id == obj.userID;
                                            });
                                        });
                                    }

                                    if (eachRow.SECOND_PRIORITY_VENDORS_TEMP) {
                                        eachRow.companyvendors1 = $scope.vendorsList.filter(function (obj) {
                                            return eachRow.SECOND_PRIORITY_VENDORS_TEMP.some(function (obj2) {
                                                return obj2.u_id == obj.userID;
                                            });
                                        });
                                    }
                                    //delete eachRow.tableHeaders;
                                    //delete eachRow.tableValues;
                                    //delete eachRow.$$hashKey;
                                    $scope.editRecord = eachRow;
                                }
                            });
                    });
            };

            $scope.EditDeleteRecord = function (type,val)
            {
                $scope.firstVendorIds = '';
                $scope.secondVendorIds = '';
                if (type === 'DELETE')
                {
                    $scope.editRecord = val;
                    $scope.editRecord.companyvendors = '';
                    $scope.editRecord.companyvendors1 = '';
                }

                if (_.isEmpty($scope.editRecord.companyvendors)){
                    $scope.firstVendorIds = '';
                } else if ($scope.editRecord.companyvendors && $scope.editRecord.companyvendors.length > 0) {
                    var vendors = _($scope.editRecord.companyvendors)
                        .filter(item => item.userID)
                        .map('userID')
                        .value();
                    $scope.firstVendorIds = vendors.join(',');
                }

                if (_.isEmpty($scope.editRecord.companyvendors1)) {
                    $scope.secondVendorIds = '';
                } else if ($scope.editRecord.companyvendors1 && $scope.editRecord.companyvendors1.length > 0) {
                    var vendors = _($scope.editRecord.companyvendors1)
                        .filter(item => item.userID)
                        .map('userID')
                        .value();
                    $scope.secondVendorIds = vendors.join(',');
                }

                var params =
                {
                    "SE_NUMBER": $scope.editRecord.SE_NUMBER,
                    "FIRST_VENDORS": $scope.firstVendorIds,
                    "SECOND_VENDORS": $scope.secondVendorIds,
                    "COMP_ID": +$scope.compId,
                    "type": type,
                    "sessionid": userService.getUserToken()
                };
                let isConfirmed = true;
                if (type === 'DELETE')
                {
                    isConfirmed = false;
                    swal({
                        title: "Are You Sure!",
                        text: "Do You want to delete the sub entity",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes",
                        closeOnConfirm: true
                    }, function (isConfirm) {
                        if (!isConfirm) {
                            isConfirmed = false;
                        } else {
                            isConfirmed = true;
                            process(isConfirmed, params);
                        }
                    });
                }
                process(isConfirmed, params);
            };

            function process(isConfirmed, params) {
                if (isConfirmed) {
                    catalogService.EditDeleteSubEntity(params)
                        .then(function (response) {
                            $scope.GetSubEntityList(0, 10, '');
                            $scope.showEdit = false;
                        });
                }
            }

            $scope.cancel = function () {
                $scope.showEdit = false;
            };


            $scope.downloadToExcel = function()
            {
                $scope.GetSubEntityList(0, 0, '', true);
            }

            function execute()
            {
                var mystyle = {
                    headers: true,
                    column: { style: { Font: { Color: "#FFFFFF" }, Interior: { Color: "#4F81BD", Pattern: "Solid" }, Alignment: { Horizontal: "Center" } } },
                    row: { style: { Alignment: { Horizontal: "Center" } } }
                };
                alasql('SELECT '+ $scope.excelColumns +' INTO XLSXML("Sub Entity Report.xls",?) FROM ?', [mystyle, $scope.excelRows]);//BRAND,SE_NUMBER as [SE NUMBER]
                $scope.downloadExcel = false;
            }

        }]);