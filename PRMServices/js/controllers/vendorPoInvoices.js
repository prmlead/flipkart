﻿prmApp
    .controller('vendorPoInvoicesCtrl', function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPOService, poService, PRMCustomFieldService, fileReader, $uibModal, $filter) {

        console.log('1234');

        $scope.isVendor = (userService.getUserType() == "VENDOR") ? true : false;
        $scope.userID = userService.getUserId();
        $scope.compID = userService.getUserCompanyId();
        //$scope.customerCompanyId = userService.getCustomerCompanyId();

        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.maxSize = 5;
        $scope.invoiceDetails = [];
        $scope.searchKeyword = '';
        $scope.filterStatus = '';

        $scope.isCustomer = userService.getUserType() === "CUSTOMER" ? true : false;
        $scope.setPage = function (pageNo,status) {
            $scope.currentPage = pageNo;
            if (status) {
                $scope.filterStatus = status
            } 
            $scope.getInvoiceHeaderLevelList($scope.searchKeyword, $scope.filterStatus);
        };
        $scope.reqStatus = 'ALL';


        $scope.pageChanged = function () {
            ////console.log('Page changed to: ' + $scope.currentPage);
        };


        $scope.filters = {
            searchKeyword: '',
            type: '',
            status: '',
            fromDate: moment().subtract(30, "days").format("YYYY-MM-DD"),
            toDate: moment().format('YYYY-MM-DD'),
            statusfilter: [],
        }

        

        //$scope.getInvoiceList = function () {
        //    $scope.params = {
        //        "COMP_ID": !$scope.isVendor ? +$scope.compID : 0,
        //        "U_ID": !$scope.isVendor ? 0 : +$scope.userID,
        //        "fromDate": $scope.filters.fromDate,
        //        "toDate": $scope.filters.toDate,
        //        "PO_NUMBER": '',
        //        "INVOICE_NUMBER": ''
        //    }

        //    PRMPOService.getInvoiceList($scope.params)
        //        .then(function (response) {
        //            $scope.invoiceDetails = response;
        //            $scope.invoiceDetails.forEach(function (item) {
        //                item.INVOICE_DATE = new moment(item.INVOICE_DATE).format("DD-MM-YYYY");
        //            })
        //            $scope.totalItems = $scope.invoiceDetails.length;

        //        });
        //}
        //$scope.getInvoiceList();


        $scope.getInvoiceHeaderLevelList = function (search,status) {
            $scope.params = {
                "compID": !$scope.isVendor ? +$scope.compID : 0,
                "uID": !$scope.isVendor ? 0 : +$scope.userID,
                "fromDate": $scope.filters.fromDate,
                "toDate": $scope.filters.toDate,
                "search": search
            }

            PRMPOService.getInvoiceHeaderLevelList($scope.params)
                .then(function (response) {
                    $scope.invoiceDetails = response;
                    $scope.filterArray = [];
                    if (status == 'ALL') {
                        $scope.filterStatus = status;
                    }
                    
                    $scope.invoiceDetails.forEach(function (item) {
                        item.INVOICE_DATE = new moment(item.INVOICE_DATE).format("DD-MM-YYYY");
                    })
                    if (status !== 'ALL' && $scope.isCustomer) {
                        $scope.filterArray = $scope.invoiceDetails.filter(function (item) {
                            if (item.INVOICE_STATUS) {
                                return item.INVOICE_STATUS.toLowerCase() === status.toLowerCase();
                            }
                        });

                        $scope.invoiceDetails = $scope.filterArray;
                    }



                    $scope.totalItems = $scope.invoiceDetails.length;

                });
        }
        $scope.getInvoiceHeaderLevelList('', 'ALL');



        $scope.createInvoice = function (invoice) {
            var url = $state.href("createInvoice", { "poNumber": invoice.PO_NUMBER, "invoiceNumber": invoice.INVOICE_NUMBER, "invoiceID": invoice.INVOICE_ID });
            $window.open(url, '_blank');
        };

        $scope.deleteInvoice = function (invoiceNumber) {
            $scope.invoice = invoiceNumber;
            swal({
                title: "Delete!",
                text: "Are you sure to delete the Invoice?",
                type: "error",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ok",
                closeOnConfirm: true
            },
                function () {
                    $scope.params = {
                        "invoiceNumber": $scope.invoice,
                        "sessionID": userService.getUserToken()
                    }
                    PRMPOService.DeleteInvoice($scope.params)
                        .then(function (response) {
                            if (response.errorMessage == "") {
                               // growlService.growl("Invoice Deleted Successfully.", "success");
                                location.reload();
                            }
                        });
                });
        }

        $scope.sendForIntegration = function (invoicenumber) {

            $scope.params = {
                "invoicenumber": invoicenumber,
                "sessionid": userService.getUserToken()
            }
            PRMPOService.sendForInvoiceIntegration($scope.params)
                .then(function (response) {

                });

        }



    });