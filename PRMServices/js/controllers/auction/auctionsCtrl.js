prmApp
    // =========================================================================
    // Auction Tiles
    // =========================================================================
    .controller('auctionsCtrl', ["$timeout", "$state", "$scope", "$log", "growlService", "userService", "auctionsService", "$http", "$rootScope",
        "SignalRFactory", "signalRHubName", "logisticServices", "$filter", "store", "poService", "workflowService",
        function ($timeout, $state, $scope, $log, growlService, userService, auctionsService, $http, $rootScope, $filter, SignalRFactory, signalRHubName, logisticServices, store, poService,
            workflowService) {
            $scope.myHotAuctionsLoaded = false;
            $scope.sessionid = userService.getUserToken();
            $scope.isVendor = (userService.getUserType() == "VENDOR") ? true : false;
            $scope.totalPendingApprovals = 0;

            //#region ListUserDepartmentDesignations
            if (!$scope.isVendor) {
                $scope.SelectedDeptId = 0;
                $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
                if ($scope.ListUserDepartmentDesignations && $scope.ListUserDepartmentDesignations.length > 0) {
                    $scope.ListUserDepartmentDesignations.unshift({ deptCode: 'ALL', compID: 0, deptID: 0, deptTypeID: 1000, listDesignation: [], isValid: true });
                }

                if (userService.getSelectedUserDepartmentDesignation()) {
                    $scope.SelectedUserDepartmentDesignation = userService.getSelectedUserDepartmentDesignation();
                }
                $scope.SelectedDeptId = $scope.SelectedUserDepartmentDesignation.deptID;
            };

            $scope.changeUserDepartmentDesignation = function () {
                $scope.SelectedUserDepartmentDesignation = $scope.ListUserDepartmentDesignations.filter(function (udd) {
                    return (udd.deptID == $scope.SelectedDeptId);
                });

                $scope.SelectedUserDepartmentDesignation = $scope.SelectedUserDepartmentDesignation[0];
                store.set('SelectedUserDepartmentDesignation', $scope.SelectedUserDepartmentDesignation);

                console.log(userService.getSelectedUserDepartmentDesignation().deptID);
                $scope.getMiniItems();

            };
            //#endregion ListUserDepartmentDesignations

            /* CLIENT STATUS MAPPING TO PRM STATUS */
            $scope.isCustomer = userService.getUserType();
            $scope.prmStatus = function (type, status) {
                return userService.NegotiationStatus(type, status);
            };
            /* CLIENT STATUS MAPPING TO PRM STATUS */

            $scope.compID = userService.getUserCompanyId();
            // console.log('------------------------------------------$scope.compID');
            //console.log($scope.compID);
            //console.log('------------------------------------------$scope.compID');
            //if ($scope.compID == 0) {
            //    location.reload();
            //}

            console.log('-----------------------------------------------$scope.compID');
            console.log($scope.compID);
            console.log('$scope.compID-----------------------------------------------');

            /*$scope.myAuctionsLoaded = false;*/
            $scope.todaysAuctionsLoaded = false;
            $scope.myHotAuctionsLoaded = false;
            $scope.scheduledAuctionsLoaded = false;
            /*$scope.myAuctions = [];*/
            /*$scope.myAuctions = [];*/
            $scope.todaysAuctionsLoaded = false;
            $scope.scheduledAuctionsLoaded = false;
            $scope.hotLimit = 4;
            $scope.todaysLimit = 4;
            $scope.scheduledLimit = 4;
            $scope.loggedIn = userService.isLoggedIn();
            $scope.isVendor = (userService.getUserType() == "VENDOR") ? true : false;
            $log.info(userService.getUserType());
            $scope.scheduledAuctionsMessage = $scope.todayAuctionsMessage = $scope.hotAuctionsMessage = "Loading data, please wait.";

            $scope.urgencyChartShow = false;
            $scope.categoryChartShow = false;
            $scope.quotationRangeChartShow = false;
            $scope.monthlyDetailsChartShow = false;
            $scope.requirementDetailsChartShow = false;
            $scope.transactionDetailsChartShow = false;


            $scope.reqCategory = {
                credits: {
                    enabled: false
                },
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                    width: 470,
                    height: 300
                },
                title: {
                    text: 'Pending Requirements by Category'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y}</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.y}',
                            style: {
                                color: 'black'
                            }
                        }
                    }
                    // Clickable dashboard //
                    ,
                    events: {
                        click: function (event) {
                            $scope.goToRequirements('UNCONFIRMED', event.point.name);
                        }
                    }
                    // Clickable dashboard //
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: []
                }]
            };


            $scope.monthlyDetails = {
                credits: {
                    enabled: false
                },
                chart: {
                    type: 'line',
                    width: 450,
                    height: 300
                },
                title: {
                    text: 'Monthly Average Savings'
                },
                subtitle: {
                    text: 'Source: Requirements'
                },
                xAxis: {
                    categories: []
                },
                yAxis: {
                    title: {
                        text: 'Price (INR)'
                    }
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                series: [{
                    name: 'Requirement Value',
                    data: []
                }, {
                    name: 'Savings Achieved',
                    data: []
                }]
            };;




            $scope.reduceTime = function (timerId, time) {
                addCDSeconds(timerId, time);
            };

            $scope.stopBid = function (item) {
                $scope.myHotAuctions[0].TimeLeft = 60;
            };

            $scope.options = {
                loop: true,
                dots: false,
                margin: 10,
                autoplay: true,
                autoplayTimeout: 2000,
                autoplayHoverPause: true,
                responsive: {
                    0: {
                        items: 1,
                    },
                    600: {
                        items: 3,
                    },
                    1000: {
                        items: 4,
                        loop: false
                    }
                }
            };
            $scope.data = {
                userID: 1,
                sessionID: 1,
                section: "CURRENT"
            };

            $scope.myHotAuctions = [];
            $scope.scheduledAuctions = [];
            $scope.todaysAuctions = [];
            $log.info($scope.loggedIn);



            $scope.currentUserObj = userService.getUserObj();
            $log.info($scope.currentUserObj);

            $scope.totalAuctionsPendingPercentage = 0;
            $scope.totalAuctionsCompletedPercentage = 0;
            $scope.totalTurnaroundTime = 0;
            $scope.totalTurnaroundTimeArray = [];
            $scope.liveAuctionsTableLimit = 50;

            $scope.totalSavedAmount = 0;
            $scope.totalSavedPercentage = 0;
            $scope.totalTurnover = 0;

            $scope.D = 0;
            $scope.H = 0;
            $scope.M = 0;

            var today = moment();
            $scope.filter = {
                dashboardFromDate: today.add('days', -30).format('YYYY-MM-DD'),
                dashboardToDate: moment().format('YYYY-MM-DD')
            };

            $scope.userDepartments = [];

            $scope.loadUserDepartments = function () {
                auctionsService.GetUserDepartments(userService.getUserId(), userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.userDepartments = response;
                            $log.info('$scope.userDepartments');
                            $log.info($scope.userDepartments);
                        }

                    })
            };

            $scope.getMiniItems = function () {
                if ($scope.loggedIn) {
                    //$scope.loadUserDepartments();

                    var params = {
                        userid: userService.getUserId(),
                        sessionid: userService.getUserToken(),
                        fromDate: $scope.filter.dashboardFromDate,
                        toDate: $scope.filter.dashboardToDate,
                        filterDepts: $scope.isVendor ? 0 : $scope.SelectedDeptId
                    };

                    auctionsService.getDashboardStatsNew(params)
                        .then(function (response) {
                            $scope.dashboardStats = response;
                            var factor = Math.pow(10, 2);

                            $scope.totalSavedAmount = $scope.dashboardStats.totalSaved;
                            $scope.totalSavedAmount = (Math.round($scope.totalSavedAmount * factor) / factor);


                            $scope.totalTurnover = $scope.dashboardStats.totalTurnover;
                            $scope.totalTurnover = Math.round($scope.totalTurnover * factor) / factor;

                            $scope.totalSavedPercentage = (($scope.dashboardStats.totalSaved / $scope.dashboardStats.totalTurnover) * 100);
                            $scope.totalSavedPercentage = Math.round($scope.totalSavedPercentage * factor) / factor;


                            //$scope.dashboardStats.totalTurnover = parseFloat($scope.dashboardStats.totalTurnover).toFixed(2);
                            //$scope.dashboardStats.totalTurnover = parseInt($scope.dashboardStats.totalTurnover);

                            //if (userService.getUserType() == "CUSTOMER") {
                            //    $scope.dashboardStats.totalSaved = parseFloat($scope.dashboardStats.totalSaved).toFixed(2);
                            //    $scope.dashboardStats.totalSaved = parseInt($scope.dashboardStats.totalSaved);
                            //}

                            $scope.totalOpenRFQsPercentage = parseFloat(($scope.dashboardStats.TOTAL_OPEN_RFQS / $scope.dashboardStats.TOTAL_RFQS) * 100).toFixed(2);
                            $scope.totalTurnaroundTime = parseFloat($scope.dashboardStats.totalTurnaroundTime / 60).toFixed(2);

                            $scope.D = parseInt($scope.dashboardStats.totalTurnaroundTime / (24 * 60));
                            $scope.H = parseInt(($scope.dashboardStats.totalTurnaroundTime % (24 * 60)) / 60);
                            $scope.M = parseInt(($scope.dashboardStats.totalTurnaroundTime % (24 * 60)) % 60);

                            $scope.totalTurnaroundTimeArray = $scope.totalTurnaroundTime.toString().split('.');

                            $scope.totalTurnaroundTimePercentage = parseFloat((($scope.dashboardStats.totalTurnaroundTime / (60 * 24)) / $scope.dashboardStats.totalAuctionsCompleted) * 100).toFixed(2);
                          

                            if ($scope.dashboardStats && $scope.dashboardStats.REQ_RANGE_LIST && $scope.dashboardStats.REQ_RANGE_LIST.length > 0) {
                                let data = [];
                                $scope.quotationRange.series[0].data = data;
                                $scope.dashboardStats.REQ_RANGE_LIST.forEach(function (keyVal, index) {
                                    let statObj = {};
                                    statObj.name = keyVal.RangeVal;
                                    statObj.y = keyVal.COUNT;
                                    data.push(statObj);
                                })
                                if (angular.element('#quotationRange').length > 0) {
                                    Highcharts.chart('quotationRange', $scope.quotationRange);
                                }
                                $scope.quotationRangeChartShow = true;

                            } else {
                                $scope.quotationRange.series[0].data = [];
                                if (angular.element('#quotationRange').length <= 0) {
                                    Highcharts.chart('quotationRange', $scope.quotationRange);
                                }
                                $scope.quotationRangeChartShow = false;
                            };

                            if ($scope.dashboardStats.REQ_URGENCY_LIST && $scope.dashboardStats.REQ_URGENCY_LIST.length > 0) {
                                let data = [];
                                $scope.reqUrgency.series[0].data = [];
                                $scope.dashboardStats.REQ_URGENCY_LIST.forEach(function (keyVal, index) {
                                    let statObj = {};
                                    statObj.name = keyVal.REQ_URGENCY.split(' (')[0];
                                    statObj.y = keyVal.COUNT;
                                    data.push(statObj);
                                });
                                $scope.reqUrgency.series[0].data = data;
                                if (angular.element('#reqUrgency').length > 0) {
                                    Highcharts.chart('reqUrgency', $scope.reqUrgency);
                                }
                                $scope.urgencyChartShow = true;
                            } else {
                                $scope.reqUrgency.series[0].data = [];
                                if (angular.element('#reqUrgency').length <= 0) {
                                    Highcharts.chart('reqUrgency', $scope.reqUrgency);
                                }
                                $scope.urgencyChartShow = false;

                            }


                            if ($scope.dashboardStats.MONTHLY_AVG_UTILIZATION && $scope.dashboardStats.MONTHLY_AVG_UTILIZATION.length > 0) {
                                let data = [];
                                let data2 = [];
                                $scope.requirementDetails.xAxis.categories = [];
                                $scope.requirementDetails.series[0].data = [];
                                $scope.requirementDetails.series[1].data = [];
                                $scope.dashboardStats.MONTHLY_AVG_UTILIZATION.forEach(function (keyVal, index) {
                                    if (keyVal.REQ_STATUS == 'Requirements Pending') {
                                        var obj =
                                        {
                                            "name": keyVal.MONTH_NAME,
                                            "count": keyVal.REQ_COUNT
                                        };
                                        data.push(obj.count);
                                        $scope.requirementDetails.xAxis.categories.push(keyVal.MONTH_NAME);
                                    } else if (keyVal.REQ_STATUS == 'Requirements posted') {
                                        var obj2 =
                                        {
                                            "name": keyVal.MONTH_NAME,
                                            "count": keyVal.REQ_COUNT
                                        };
                                        data2.push(obj2.count);
                                    }

                                });

                                $scope.requirementDetails.series[0].data = data;
                                $scope.requirementDetails.series[1].data = data2;
                                if (angular.element('#requirement').length > 0) {
                                    Highcharts.chart('requirement', $scope.requirementDetails);
                                }
                                $scope.requirementDetailsChartShow = true;
                            } else {
                                $scope.requirementDetails.xAxis.categories = [];
                                $scope.requirementDetails.series[0].data = [];
                                $scope.requirementDetails.series[1].data = [];
                                if (angular.element('#requirement').length <=0) {
                                    Highcharts.chart('requirement', $scope.requirementDetails);
                                }
                                $scope.requirementDetailsChartShow = false;
                            }

                            if ($scope.dashboardStats.MONTHLY_AVG_SAVINGS && $scope.dashboardStats.MONTHLY_AVG_SAVINGS.length > 0) {
                                $scope.monthlyDetailsChartShow = false;
                                $scope.monthlyDetails.xAxis.categories = [];
                                $scope.monthlyDetails.series[0].data = [];
                                $scope.monthlyDetails.series[1].data = [];
                                $scope.dashboardStats.MONTHLY_AVG_SAVINGS.forEach(function (keyVal, index) {
                                    $scope.monthlyDetails.xAxis.categories.push(keyVal.MONTH.toString());
                                    $scope.monthlyDetails.series[0].data.push(keyVal.QUOTATION_VALUE);
                                    $scope.monthlyDetails.series[1].data.push(keyVal.SAVINGS);
                                });
                                if (angular.element('#savings').length > 0) {
                                    Highcharts.chart('savings', $scope.monthlyDetails);
                                }
                                $scope.monthlyDetailsChartShow = true;
                            } else {
                                $scope.monthlyDetails.xAxis.categories = [];
                                $scope.monthlyDetails.series[0].data = [];
                                $scope.monthlyDetails.series[1].data = [];
                                if (angular.element('#savings').length<=0) {
                                    Highcharts.chart('savings', $scope.monthlyDetails);
                                }
                                $scope.monthlyDetailsChartShow = false;

                            }

                            if ($scope.dashboardStats.TRANSACTIONAL_TREND && $scope.dashboardStats.TRANSACTIONAL_TREND.length > 0) {
                                $scope.transactionDetailsChartShow = false;
                                $scope.transactionDetails.xAxis.categories = [];
                                $scope.transactionDetails.series[0].data = [];
                                $scope.transactionDetails.series[1].data = [];
                                $scope.dashboardStats.TRANSACTIONAL_TREND.forEach(function (keyVal, index) {
                                    $scope.transactionDetails.xAxis.categories.push(keyVal.MONTH_NAME.toString());
                                    $scope.transactionDetails.series[0].data.push(keyVal.TOTAL_REQ_COUNT);
                                    $scope.transactionDetails.series[1].data.push(keyVal.MY_REQ_COUNT);
                                });
                                if (angular.element('#transaction').length > 0) {
                                    Highcharts.chart('transaction', $scope.transactionDetails);
                                }
                                $scope.transactionDetailsChartShow = true;
                            } else {
                                $scope.transactionDetailsChartShow = false;
                                $scope.transactionDetails.xAxis.categories = [];
                                $scope.transactionDetails.series[0].data = [];
                                $scope.transactionDetails.series[1].data = [];
                                if (angular.element('#transaction').length <= 0) {
                                    Highcharts.chart('transaction', $scope.transactionDetails);
                                }

                            }
                        });
                }
            };

            $scope.purchaseOrders = [];
            $scope.poNumbers = [];
            $scope.deliverySchedulePoNumbers = [];
            $scope.deliverySchedulesStr = "";
            $scope.deliverySchedulesStrTemp = "";


            $scope.totalPurchaseOrdersCount = 0;
            $scope.totaldeliverySchedulesCount = 0;
            $scope.getPurchaseOrders = function () {
                poService.getAssignedPO()
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.purchaseOrders = response;
                            $scope.totalPurchaseOrdersCount = $scope.purchaseOrders[0].TOTAL_COUNT;

                            $scope.purchaseOrders.forEach(function (item, index) {
                                $scope.poNumbers.push(item.PURCHASE_ORDER_ID);
                            });
                            $scope.deliverySchedulesStr = "";
                            $scope.deliverySchedulesStrTemp = "";

                            $scope.deliverySchedulesStrTemp = $scope.poNumbers;
                            $scope.deliverySchedulesStr = $scope.deliverySchedulesStrTemp.join(',');
                            if ($scope.deliverySchedulesStr && $scope.deliverySchedulesStr != null) {
                                $scope.getDeliverySchedules($scope.deliverySchedulesStr);
                            }
                        }
                    });
            };

            $scope.getDeliverySchedules = function (poNumbers) {
                poService.getDeliverySchedules(poNumbers)
                    .then(function (response) {
                        $scope.deliverySchedules = response;

                        $scope.deliverySchedules.forEach(function (item, index) {
                            item.DELIVERY_DATE = userService.toLocalDate(item.DELIVERY_DATE);
                        });
                        $scope.totaldeliverySchedulesCount = $scope.deliverySchedules[0].TOTAL_COUNT;
                    });
            };
            
            $scope.getMiniItems();

            if ($scope.isVendor) {
                $scope.getPurchaseOrders();
            }
           

            $scope.addItem = function () {
                var obj = {
                    auctionTimerId: 1000,
                    title: "test",
                    price: 11244354,
                    bids: 24,
                    auctionEnds: '123465'
                }
                $scope.myHotAuctions.push(obj);
            }



            $scope.reduceTime = function (timerId, time) {
                addCDSeconds(timerId, time);
            }

            $scope.stopBid = function (item) {
                $scope.myHotAuctions[0].TimeLeft = 60;
            }

            $scope.changeScheduledAuctionsLimit = function () {
                $scope.scheduledLimit = 8;
                $scope.getMiniItems();
            }

            $scope.changeHotAuctionsLimit = function () {
                $scope.hotLimit = 8;
                $scope.getMiniItems();
            }

            $scope.changeTodaysAuctionsLimit = function () {
                $scope.todaysLimit = 8;
                $scope.getMiniItems();
            }

            $scope.totalItems = 0;
            $scope.totalLeads = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 5;

            $scope.currentPage2 = 1;
            $scope.itemsPerPage2 = 5;

            $scope.maxSize = 5; //Number of pager buttons to show

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
                //console.log('Page changed to: ' + $scope.currentPage);
            };


            $scope.myAuctionsLoaded = false;
            $scope.myAuctions = [];
            $scope.myActiveLeads = [];

            $scope.myActiveLeadsEnded = [];
            $scope.myActiveLeadsNotStarted = [];

            var loginUserData = userService.getUserObj();



            $scope.totalLeads = 0;
            $scope.totalLeads2 = 0;
            $scope.myLogistics = [];

            // Clickable dashboard //
            $scope.goToRequirements = function (status, priority) {
                $state.go('pages.profile.profile-timeline', { 'userId': userService.getUserId(), "filters": { status: status, priority: priority } });
            };
            // Clickable dashboard //
            $scope.goToPos = function () {
                //$state.go('pendingpos', { 'userId': userService.getUserId() });
                $state.go('list-pendingPO');
            };
            $scope.goToPendingContracts = function () {
                $state.go('pendingcontracts', { 'userId': userService.getUserId() });
            };
            $scope.goToPendingApprovals = function () {
                $state.go('workflow-pending-approvals', { 'userId': userService.getUserId() });
            };

            $scope.goToConsolidateReport = function () {
                // $state.go('consalidated-report');
                var url = $state.href("consalidatedReport");
                window.open(url, '_self');
            };

            $scope.goToDispatchTrackForm = function (reqID, purchaseID, dispatchCode) {
                var url = $state.href("dtform", { reqID: reqID, "poOrderId": purchaseID, "dCode": dispatchCode });
                window.open(url, '_blank');
            };

            $scope.goToProjectStatus = function () {
                var url = $state.href("projectStatus");
                window.open(url, '_blank');
            };

            //Pending Approvals
            $scope.GetMyPendingApprovals = function () {
                $scope.totalPendingApprovals = 0;
                let listUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
                let deptIDStr = '';
                let desigIDStr = '';
                let desigIDs = [];
                let deptIDs = [];
                if (listUserDepartmentDesignations && listUserDepartmentDesignations.length > 0) {
                    listUserDepartmentDesignations.forEach(function (item, index) {
                        deptIDs.push(item.deptID);
                        item.listDesignation.forEach(function (item1, index1) {
                            if (item1.isAssignedToUser && item1.isValid) {
                                desigIDs.push(item1.desigID);
                                desigIDs = _.union(desigIDs, [item1.desigID]);
                            }
                        });
                    });

                    deptIDStr = deptIDs.join(',');
                    desigIDStr = desigIDs.join(',');
                }

                workflowService.GetMyPendingApprovals(deptIDStr, desigIDStr)
                    .then(function (response) {
                        if (response) {
                            $scope.totalPendingApprovals = response.length;
                        }
                    });
            };

            $scope.GetMyPendingApprovals();
            //^Pending Approvals

            //Customer Chart Data
            $scope.chartData = {};

            $scope.getChartData = function () {

                var params = {
                    userid: userService.getUserId(),
                    sessionid: userService.getUserToken(),
                    fromDate: $scope.filter.dashboardFromDate,
                    toDate: $scope.filter.dashboardToDate,
                    filterDepts: $scope.isVendor ? 0 : $scope.SelectedDeptId
                };
                auctionsService.getCustomerDashboard(params)
                    .then(function (response) {
                        $scope.chartData = response;

                        if (response && response.categorySpendSeries) {
                            if (response.categorySpendSeries.length > 0) {
                                data = [];
                                response.categorySpendSeries.forEach(function (keyVal, index) {
                                    var statObj = {};
                                    statObj.name = keyVal.name;
                                    statObj.y = keyVal.data1;
                                    data.push(statObj);
                                });

                                $scope.categorySpendDetails.series[0].data = data;
                                $scope.categoriesSpendDetailsChartShow = true;
                            }
                        }
                    
                    });
            };




            $scope.requirementDetails = {
                credits: {
                    enabled: false
                },
                chart: {
                    type: 'column',
                    width: 550,
                    height: 300
                },
                title: {
                    text: 'Monthly Average Utilization'
                },
                subtitle: {
                    text: 'Source: Requirements'
                },
                xAxis: {
                    categories: []
                },
                yAxis: {

                    title: {
                        text: 'Requriments'
                    }

                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                series: [{
                    name: 'Requirement Pending',
                    data: []
                }, {
                    name: 'Requirement Posted',
                    data: []
                }
                ]
            };

            $scope.reqUrgency = {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                    width: 320,
                    height: 300
                },
                title: {
                    text: 'Pending Requirements by Priority'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y}</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.y}',
                            style: {
                                color: 'black'
                            }
                        }
                    }
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: []
                }],
                credits: {
                    enabled: false
                }
            }

            $scope.quotationRange = {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                    width: 320,
                    height: 300
                },
                title: {
                    text: 'Pending Requirements by Value'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y}</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.y}',
                            style: {
                                color: 'black'
                            }
                        }
                    }
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: []
                }],
                credits: {
                    enabled: false
                }
            };

            $scope.transactionDetails = {
                credits: {
                    enabled: false
                },
                chart: {
                    type: 'areaspline',
                    width: 450,
                    height: 300
                },
                title: {
                    text: ' My Transactional Trend'
                },
                subtitle: {
                    text: 'Source: My Transactional Trend Vs Organizational Trend'
                },
                xAxis: {
                    categories: []
                },
                yAxis: {
                    title: {
                        text: 'RFQ counts'
                    }

                },

                plotOptions: {
                    areaspline: {
                        lineWidth: 5,
                        states: {
                            hover: {
                                lineWidth: 3
                            }
                        },
                        marker: {
                            enabled: false,
                            states: {
                                hover: {
                                    enabled: true
                                }
                            }
                        },
                    }
                },
                series: [{
                    name: 'Organizational Trend',
                    data: []
                }, {
                    name: 'My Transactional Trend',
                    data: []
                }],
                navigation: {
                    menuItemStyle: {
                        fontSize: '10px'
                    }
                }
            };
           
        }]);