﻿using System;
using System.Collections.Generic;
using System.Data;
using PRMServices.Models;
using PRMServices.SQLHelper;

namespace PRMServices.Common
{
    public class TechEvalUtility
    {
        public static Questionnaire GetQuestionnaireObject(DataRow row)
        {
            Questionnaire detail = new Questionnaire();
            detail.EvalID = Convert.ToInt32(row.GetColumnValue("EVAL_ID", detail.EvalID.GetType()));
            detail.ReqID = Convert.ToInt32(row.GetColumnValue("REQ_ID", detail.ReqID.GetType()));
            detail.UserID = Convert.ToInt32(row.GetColumnValue("U_ID", detail.UserID.GetType()));
            detail.Title = Convert.ToString(row.GetColumnValue("QUEST_TITLE", detail.Title.GetType()));
            detail.Description = Convert.ToString(row.GetColumnValue("QUEST_DESC", detail.Description.GetType()));

            if (detail.StartDate == null)
            {
                detail.StartDate = DateTime.MinValue;
            }
            if (detail.EndDate == null)
            {
                detail.EndDate = DateTime.MaxValue;
            }

            detail.StartDate = Convert.ToDateTime(row.GetColumnValue("QUEST_START_DATE", detail.StartDate.GetType()));
            detail.EndDate = Convert.ToDateTime(row.GetColumnValue("QUEST_END_DATE", detail.EndDate.GetType()));
            detail.Category = Convert.ToString(row.GetColumnValue("QUEST_CATEGORY", detail.Description.GetType()));

            detail.TotalMarks = Convert.ToDouble(row.GetColumnValue("TOTAL_MARKS", detail.TotalMarks.GetType()));
            detail.QualifyingMarks = Convert.ToDouble(row.GetColumnValue("QUALIFYING_SCORE", detail.QualifyingMarks.GetType()));
            detail.IsApproved = Convert.ToInt32(row.GetColumnValue("IS_APPROVED", detail.IsApproved.GetType()));
            detail.IsSentToVendor = Convert.ToInt32(row.GetColumnValue("IS_EVAL_SENT", detail.IsApproved.GetType()));

            return detail;
        }

        public static Question GetQuestion(DataRow row)
        {
            Question detail = new Question();
            detail.EvalID = Convert.ToInt32(row.GetColumnValue("EVAL_ID", detail.EvalID.GetType()));
            detail.QuestionID = Convert.ToInt32(row.GetColumnValue("Q_ID", detail.QuestionID.GetType()));
            detail.Text = Convert.ToString(row.GetColumnValue("Q_TEXT", detail.Text.GetType()));
            detail.Options = Convert.ToString(row.GetColumnValue("Q_OPTIONS", detail.Options.GetType()));
            detail.Type = Convert.ToString(row.GetColumnValue("Q_TYPE", detail.Type.GetType()));
            detail.AttachmentID = Convert.ToInt32(row.GetColumnValue("Q_ATTACH_ID", detail.AttachmentID.GetType()));
            detail.Marks = Convert.ToDecimal(row.GetColumnValue("Q_MARKS", detail.Marks.GetType()));

            return detail;
        }

        public static Answer GetAnswer(DataRow row)
        {
            Answer detail = new Answer();
            detail.Vendor = new UserDetails();
            detail.AnswerID = Convert.ToInt32(row.GetColumnValue("RES_ID", detail.AnswerID.GetType()));
            detail.Vendor.UserID = Convert.ToInt32(row.GetColumnValue("U_ID", detail.AnswerID.GetType()));
            detail.AnswerText = Convert.ToString(row.GetColumnValue("RES_TEXT", detail.AnswerText.GetType()));
            detail.ResComments = Convert.ToString(row.GetColumnValue("RES_COMMENTS", detail.ResComments.GetType()));
            detail.UserMarks = Convert.ToDecimal(row.GetColumnValue("RES_MARKS", detail.UserMarks.GetType()));
            detail.AttachmentID = Convert.ToInt32(row.GetColumnValue("RES_ATTACH_ID", detail.AttachmentID.GetType()));
            
            return detail;
        }

        public static Answer GetVendorReport(DataRow row)
        {
            Answer detail = new Answer();
            detail.Vendor = new UserDetails();
            detail.Vendor.UserID = Convert.ToInt32(row.GetColumnValue("U_ID", detail.Vendor.UserID.GetType()));
            detail.Vendor.FirstName = Convert.ToString(row.GetColumnValue("U_FNAME", detail.Vendor.FirstName.GetType()));
            detail.Vendor.LastName = Convert.ToString(row.GetColumnValue("U_LNAME", detail.Vendor.LastName.GetType()));
            detail.Vendor.CompanyName = Convert.ToString(row.GetColumnValue("COMPANY_NAME", detail.Vendor.CompanyName.GetType()));
            detail.UserMarks = Convert.ToDecimal(row.GetColumnValue("RES_MARKS", detail.UserMarks.GetType()));

            return detail;
        }

        public static TechEval GetTechEvalReport(DataRow row)
        {
            TechEval detail = new TechEval();
            detail.Vendor = new UserDetails();
            detail.Vendor.FirstName = string.Empty;
            detail.Vendor.LastName = string.Empty;
            detail.Vendor.CompanyName = string.Empty;
            detail.Vendor.UserID = Convert.ToInt32(row.GetColumnValue("U_ID", detail.Vendor.UserID.GetType()));
            detail.Vendor.FirstName = Convert.ToString(row.GetColumnValue("U_FNAME", detail.Vendor.FirstName.GetType()));
            detail.Vendor.LastName = Convert.ToString(row.GetColumnValue("U_LNAME", detail.Vendor.LastName.GetType()));
            detail.Vendor.CompanyName = Convert.ToString(row.GetColumnValue("COMPANY_NAME", detail.Vendor.CompanyName.GetType()));


            detail.ReqID = Convert.ToInt32(row.GetColumnValue("REQ_ID", detail.ReqID.GetType()));
            detail.EvalID = Convert.ToInt32(row.GetColumnValue("EVAL_ID", detail.EvalID.GetType()));
            detail.TotalMarks = Convert.ToDecimal(row.GetColumnValue("TOTAL_MARKS", detail.TotalMarks.GetType()));
            detail.IsApproved = Convert.ToInt32(row.GetColumnValue("IS_APPROVED", detail.IsApproved.GetType()));
            detail.DateModified = Convert.ToDateTime(row.GetColumnValue("DATE_MODIFIED", detail.DateModified.GetType()));
            detail.DateCreated = Convert.ToDateTime(row.GetColumnValue("DATE_CREATED", detail.DateModified.GetType()));
            detail.ModifiedBY = Convert.ToInt32(row.GetColumnValue("MODIFIED_BY", detail.ModifiedBY.GetType()));
            detail.Comments = Convert.ToString(row.GetColumnValue("COMMENTS", detail.Comments.GetType()));

            detail.IsAnswered = Convert.ToInt32(row.GetColumnValue("IS_ANSWERED", detail.IsAnswered.GetType()));

            

            return detail;
        }

        public static DataSet SaveAnswerEntity(Answer answer)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_RES_ID", answer.AnswerID);
            sd.Add("P_Q_ID", answer.QuestionDetails.QuestionID);
            sd.Add("P_U_ID", answer.Vendor.UserID);
            sd.Add("P_RES_TEXT", answer.AnswerText);
            sd.Add("P_RES_COMMENTS", answer.ResComments);
            sd.Add("P_RES_ATTACH_ID", answer.AttachmentID);
            sd.Add("P_MARKS", answer.UserMarks);
            sd.Add("P_EVAL_ID", answer.QuestionDetails.EvalID);
            DataSet ds = DatabaseProvider.GetDatabaseProvider().SelectList("te_RecordResponse", sd);
            return ds;
        }

        public static DataSet SaveQuestionnaireEntity(Questionnaire questionnaire)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_EVAL_ID", questionnaire.EvalID);
            sd.Add("P_REQ_ID", questionnaire.ReqID);
            sd.Add("P_U_ID", questionnaire.CreatedBy);
            sd.Add("P_QUEST_TITLE", questionnaire.Title);
            sd.Add("P_QUEST_DESC", questionnaire.Description);
            sd.Add("P_QUEST_START_DATE", questionnaire.StartDate);
            sd.Add("P_QUEST_END_DATE", questionnaire.EndDate);
            sd.Add("P_CATEGORY", questionnaire.Category);
            sd.Add("P_IS_EVAL_SENT", questionnaire.IsSentToVendor);
            sd.Add("P_TOTAL_MARKS", questionnaire.TotalMarks);
            sd.Add("P_QUALIFYING_MARKS", questionnaire.QualifyingMarks);
            DataSet ds = DatabaseProvider.GetDatabaseProvider().SelectList("te_CreateQuestionnaire", sd);
            return ds;
        }

        public static DataSet AssignQuestionnaireEntity(Questionnaire questionnaire)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_EVAL_ID", questionnaire.EvalID);
            sd.Add("P_REQ_ID", questionnaire.ReqID);
            DataSet ds = DatabaseProvider.GetDatabaseProvider().SelectList("te_AssignQuestionnaire", sd);
            return ds;
        }

        public static DataSet SaveVendorForQuestionnaireEntity(Questionnaire questionnaire, VendorDetails vendor)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_EVAL_ID", questionnaire.EvalID);
            sd.Add("P_U_ID", vendor.VendorID);
            DataSet ds = DatabaseProvider.GetDatabaseProvider().SelectList("te_SaveVendorForQuestionnaire", sd);
            return ds;
        }

        public static DataSet SaveQuestionEntity(Question question)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_EVAL_ID", question.EvalID);
            sd.Add("P_Q_ID", question.QuestionID);
            sd.Add("P_U_ID", question.CreatedBy);
            sd.Add("P_Q_TYPE", question.Type);
            sd.Add("P_Q_TEXT", question.Text);
            sd.Add("P_Q_OPTIONS", question.Options);
            sd.Add("P_Q_ATTACH_ID", question.AttachmentID);
            sd.Add("P_Q_MARKS", question.Marks);
            DataSet ds = DatabaseProvider.GetDatabaseProvider().SelectList("te_CreateQuestion", sd);
            return ds;
        }

        public static DataSet SaveTechEvalEntity(TechEval techeval)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_REQ_ID", techeval.ReqID);
            sd.Add("P_EVAL_ID", techeval.EvalID);            
            sd.Add("P_U_ID", techeval.Vendor.UserID);
            sd.Add("P_IS_APPROVED", techeval.IsApproved);
            sd.Add("P_TOTAL_MARKS", techeval.TotalMarks);
            sd.Add("P_USER", techeval.ModifiedBY);
            sd.Add("P_COMMENTS", techeval.Comments);
            DataSet ds = DatabaseProvider.GetDatabaseProvider().SelectList("te_SaveTechEvaluation", sd);
            return ds;
        }

        public static VendorDetails GetUserDetails(DataRow row)
        {
            VendorDetails detail = new VendorDetails();
            detail.VendorID = Convert.ToInt32(row.GetColumnValue("U_ID", detail.VendorID.GetType()));
            detail.VendorName = Convert.ToString(row.GetColumnValue("U_FNAME", detail.VendorName.GetType()));
            detail.VendorName += " " + Convert.ToString(row.GetColumnValue("U_LNAME", detail.VendorName.GetType()));
            detail.CompanyName = Convert.ToString(row.GetColumnValue("COMPANY_NAME", detail.CompanyName.GetType()));

            return detail;
        }
    }
}