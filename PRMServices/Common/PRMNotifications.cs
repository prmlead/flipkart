﻿using System;
using System.Collections.Generic;
using PRMServices.Models;
using System.Net.Mail;
using System.ComponentModel;
using System.Xml;
using System.IO;

namespace PRMServices.Common
{
    public class PRMNotifications
    {
        public void AuctionStartNotification(int reqID, int userID, string sessionID, List<Attachment> vendorAttachments, string watchers)
        {
            PRMServices prm = new PRMServices();
            string ScreenName = "NEGOTIATION_START_TIME";
            
            Requirement req = prm.GetRequirementDataOffline(reqID, userID, sessionID);
            req.Module = ScreenName;
            req.StartTime = Utilities.toLocal(req.StartTime);
            foreach (VendorDetails vendor in req.AuctionVendors)
            {
                string body = Utilities.GenerateEmailBody("VendoremailForAuctionStartUpdate"); 
                UserInfo user = prm.GetUserNew(vendor.VendorID, sessionID);
                User altUser = prm.GetAlternateCommunications(reqID, vendor.VendorID, sessionID);
                body = String.Format(body, user.FirstName, user.LastName, req.RequirementNumber, req.StartTime, req.Title, req.RequirementID);
                prm.SendEmail(user.Email + "," + user.AltEmail + "," + altUser.AltEmail, "Negotiation Start time has been updated for Req Number: " + req.RequirementNumber + " Title:  " + req.Title, body, reqID, vendor.VendorID, req.Module, sessionID, null, vendorAttachments, req.StartTime, 15, "NEGOTATION_START", string.Empty).ConfigureAwait(false);
                string message = Utilities.GenerateEmailBody("VendorsmsForAuctionStartUpdate");
                message = String.Format(message, user.FirstName, user.LastName, reqID, req.StartTime, req.Title);
                message = message.Replace("<br/>", "");
              //  prm.SendSMS(string.Empty, user.PhoneNum + "," + user.AltPhoneNum + "," + altUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(message.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), reqID, vendor.VendorID,req.Module, sessionID).ConfigureAwait(false);
                PushNotifications push1 = new PushNotifications();
                push1.Message = message.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0];
                push1.MessageType = ScreenName;
                push1.RequirementID = req.RequirementID;
                prm.SendPushNotificationAsync(push1, user).ConfigureAwait(false);
            }

            if (req.Status != Utilities.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString()))
            {
                string body1 = Utilities.GenerateEmailBody("CustomeremailForAuctionStartUpdate");
                UserInfo user1 = prm.GetUserNew(userID, sessionID);
                body1 = String.Format(body1, user1.FirstName, user1.LastName, req.RequirementNumber, req.StartTime, req.Title, req.RequirementID);
                prm.SendEmail(user1.Email + "," + user1.AltEmail, "Negotiation Start time has been updated for Req Number: " + req.RequirementNumber + " Title: " + req.Title, body1, reqID,Convert.ToInt32(user1.UserID), req.Module, sessionID, null, null, req.StartTime, 15, "NEGOTATION_START", watchers).ConfigureAwait(false);

                string message1 = Utilities.GenerateEmailBody("CustomersmsForAuctionStartUpdate");
                message1 = String.Format(message1, user1.FirstName, user1.LastName, req.RequirementNumber, req.StartTime, req.Title);
                message1 = message1.Replace("<br/>", "");
              //  prm.SendSMS(string.Empty, user1.PhoneNum + "," + user1.AltPhoneNum, System.Web.HttpUtility.UrlEncode(message1), reqID, Convert.ToInt32(user1.UserID), req.Module).ConfigureAwait(false);


                UserInfo superUser = prm.GetSuperUser(userID, sessionID);

                string bodySuper = Utilities.GenerateEmailBody("CustomeremailForAuctionStartUpdate");
                bodySuper = String.Format(bodySuper, superUser.FirstName, superUser.LastName, req.RequirementNumber, req.StartTime, req.Title, req.RequirementID);

                string body2Super = Utilities.GenerateEmailBody("CustomersmsForAuctionStartUpdate");
                body2Super = String.Format(body2Super, superUser.FirstName, superUser.LastName, req.RequirementNumber, req.StartTime, req.Title);
                body2Super = body2Super.Replace("<br/>", "");
                string subUserID = req.CustomerID.ToString();               
                string bodyTelegram = Utilities.GenerateEmailBody("UpdateStartTimeTelegramsms");
                bodyTelegram = String.Format(bodyTelegram, req.CustomerCompanyName, req.CustFirstName, req.CustLastName, req.Title, req.Description, req.StartTime);
                bodyTelegram = bodyTelegram.Replace("<br/>", "");

                TelegramMsg tgMsg = new TelegramMsg();
                tgMsg.Message = "Negotiation Start time has been updated FOR THE REQ ID:" + req.RequirementNumber + (bodyTelegram.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]);
                tgMsg = prm.SendTelegramMsg(tgMsg);
            }
        }
    }
}