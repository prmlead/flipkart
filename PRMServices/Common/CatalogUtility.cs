﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using PRMServices.Models.Catalog;
using System.Reflection;
using PRMServices.SQLHelper;

namespace PRMServices
{
    public static class CatalogUtility
    {
        public static List<Category> GetCategoriesById(int id)
        {
            DataTable data = GetCategories("cp_getCategory");

            var result = (from row in data.AsEnumerable()
                          where row.Field<int>("CAT_ID") == id
                          select CreateItemFromRow<Category>(row)).ToList();

            return result;
        }

        public static DataTable GetCategories(string spName)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            DataSet ds = DatabaseProvider.GetDatabaseProvider().SelectList(spName, sd);
            return ds.Tables[0];
        }

        internal static DataSet GetResultSet(int prop_ID, string v, List<string> list1, List<string> list2)
        {
            throw new NotImplementedException();
        }

        public static T CreateItemFromRow<T>(DataRow row) where T : new()
        {
            T item = new T();
            SetItemFromRow(item, row);
            return item;
        }

        public static void SetItemFromRow<T>(T item, DataRow row) where T : new()
        {
            foreach (DataColumn c in row.Table.Columns)
            {
                PropertyInfo p = item.GetType().GetProperty(c.ColumnName);
                if (p != null && row[c] != DBNull.Value)
                {
                    p.SetValue(item, c.DataType.ToString() == "System.Int64" ? Convert.ToInt32(row[c]) : row[c], null);
                }
            }
        }
        public static DataSet GetResultSet(string spName, List<string> columnNames, List<object> values)
        {
            DataSet ds = new DataSet();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                if (columnNames != null && columnNames.Count > 0)
                {
                    for (int ctr = 0; ctr < columnNames.Count; ctr++)
                    {
                        sd.Add(columnNames[ctr], values[ctr]);
                    }
                }

                ds = DatabaseProvider.GetDatabaseProvider().SelectList(spName, sd);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return ds;
        }
    }
}