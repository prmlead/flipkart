﻿using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using System.ServiceModel.Activation;
using PRMServices.Models;
using PRMServices.SQLHelper;
using CORE = PRM.Core.Common;

namespace PRMServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMCatalogReportsService : IPRMCatalogReportsService
    {

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();

        #region Services

        public List<cr_UserDepartments> GetUserDepartments(int U_ID, int IS_SUPER_USER, string sessionid)
        {
            List<cr_UserDepartments> details = new List<cr_UserDepartments>();
            try
            {
                //Utilities.ValidateSession(sessionid, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", U_ID);
                sd.Add("P_IS_SUPER_USER", IS_SUPER_USER);
                CORE.DataNamesMapper<cr_UserDepartments> mapper = new CORE.DataNamesMapper<cr_UserDepartments>();
                var dataset = sqlHelper.SelectList("cr_GetUserDepartments", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<cr_DepartmentCategories> GetDepartmentCategories(int U_ID, string DEPT_IDS, string sessionid)
        {
            List<cr_DepartmentCategories> details = new List<cr_DepartmentCategories>();
            try
            {
                //Utilities.ValidateSession(sessionid, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", U_ID);
                sd.Add("P_DEPT_IDS", DEPT_IDS);
                CORE.DataNamesMapper<cr_DepartmentCategories> mapper = new CORE.DataNamesMapper<cr_DepartmentCategories>();
                var dataset = sqlHelper.SelectList("cr_GetDepartmentCategories", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<cr_DepartmentCategories> GetCategorySubcategories(int U_ID, string CAT_IDS, string sessionid)
        {
            List<cr_DepartmentCategories> details = new List<cr_DepartmentCategories>();
            try
            {
                //Utilities.ValidateSession(sessionid, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", U_ID);
                sd.Add("P_CAT_IDS", CAT_IDS);
                CORE.DataNamesMapper<cr_DepartmentCategories> mapper = new CORE.DataNamesMapper<cr_DepartmentCategories>();
                var dataset = sqlHelper.SelectList("cr_GetCategorySubcategories", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<cr_CategoryProducts> GetCategoryProducts(int U_ID, string CAT_IDS, string sessionid)
        {
            List<cr_CategoryProducts> details = new List<cr_CategoryProducts>();
            try
            {
                //Utilities.ValidateSession(sessionid, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", U_ID);
                sd.Add("P_CAT_IDS", CAT_IDS);
                CORE.DataNamesMapper<cr_CategoryProducts> mapper = new CORE.DataNamesMapper<cr_CategoryProducts>();
                var dataset = sqlHelper.SelectList("cr_GetCategoryProducts", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public cr_CategoryProducts GetProductAnalysis(int U_ID, int PROD_ID, string sessionid, int call_code)
        {
            cr_CategoryProducts details = new cr_CategoryProducts();
            try
            {
                //Utilities.ValidateSession(sessionid, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", U_ID);
                sd.Add("P_PROD_ID", PROD_ID);
                CORE.DataNamesMapper<cr_CategoryProducts> mapper = new CORE.DataNamesMapper<cr_CategoryProducts>();
                var dataset = new DataSet();
                if (call_code == 1)
                {
                    dataset = sqlHelper.SelectList("cr_GetProductAnalysis", sd);
                }
                else if (call_code == 2)
                {
                    dataset = sqlHelper.SelectList("cr_GetProductAnalysisCall2", sd);
                }
                details = mapper.Map(dataset.Tables[0]).FirstOrDefault();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<cr_VendorsAndOrders> GetVendorsAndOrders(int U_ID, int PROD_ID, string sessionid)
        {
            List<cr_VendorsAndOrders> details = new List<cr_VendorsAndOrders>();
            try
            {
                //Utilities.ValidateSession(sessionid, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", U_ID);
                sd.Add("P_PROD_ID", PROD_ID);
                CORE.DataNamesMapper<cr_VendorsAndOrders> mapper = new CORE.DataNamesMapper<cr_VendorsAndOrders>();
                var dataset = new DataSet();
                dataset = sqlHelper.SelectList("cr_GetVendorsAndOrders", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<cr_VendorsAndOrders> GetMostOrdersAndVendors(int U_ID, int PROD_ID, string sessionid)
        {
            List<cr_VendorsAndOrders> details = new List<cr_VendorsAndOrders>();
            try
            {
                //Utilities.ValidateSession(sessionid, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", U_ID);
                sd.Add("P_PROD_ID", PROD_ID);
                CORE.DataNamesMapper<cr_VendorsAndOrders> mapper = new CORE.DataNamesMapper<cr_VendorsAndOrders>();
                var dataset = new DataSet();
                dataset = sqlHelper.SelectList("cr_GetMostOrdersAndVendors", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<cr_VendorsAndOrders> GetProductRequirements(int U_ID, int PROD_ID, string sessionid)
        {
            List<cr_VendorsAndOrders> details = new List<cr_VendorsAndOrders>();
            try
            {
                //Utilities.ValidateSession(sessionid, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", U_ID);
                sd.Add("P_PROD_ID", PROD_ID);
                CORE.DataNamesMapper<cr_VendorsAndOrders> mapper = new CORE.DataNamesMapper<cr_VendorsAndOrders>();
                var dataset = new DataSet();
                dataset = sqlHelper.SelectList("cr_GetProductRequirements", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<cr_VendorsAndOrders> GetProductPriceHistory(int U_ID, int PROD_ID, string sessionid)
        {
            List<cr_VendorsAndOrders> details = new List<cr_VendorsAndOrders>();
            try
            {
                //Utilities.ValidateSession(sessionid, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", U_ID);
                sd.Add("P_PROD_ID", PROD_ID);
                CORE.DataNamesMapper<cr_VendorsAndOrders> mapper = new CORE.DataNamesMapper<cr_VendorsAndOrders>();
                var dataset = new DataSet();
                dataset = sqlHelper.SelectList("cr_GetProductPriceHistory", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        #endregion Services

    }
}