﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using PRMServices.Models;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMQCSService
    {
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getlivebiddingreport?reqid={reqID}&count={count}&sessionid={sessionID}")]
        LiveBidding[] GetLiveBiddingReport(int reqID, int count, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getitemwisereport?reqid={reqID}&sessionid={sessionID}")]
        ItemWiseReport[] GetItemWiseReport(int reqID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "deliverytimelinereport?reqid={reqID}&sessionid={sessionID}")]
        DeliveryTimeLine[] GetDeliveryTimeLineReport(int reqID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "paymenttermsreport?reqid={reqID}&sessionid={sessionID}")]
        DeliveryTimeLine[] GetPaymentTermsReport(int reqID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getreqdetails?reqid={reqID}&sessionid={sessionID}")]
        ReportsRequirement GetReqDetails(int reqID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "gettemplates?template={template}&compid={compID}&userid={userID}&reqid={reqID}&sessionid={sessionID}")]
        string GetTemplates(string template, int compID, int userID, int reqID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getreqreportforexcel?reqid={reqID}&sessionid={sessionID}")]
        ExcelRequirement GetReqReportForExcel(int reqID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getconsolidatedreports?from={from}&to={to}&userid={userID}&sessionid={sessionID}")]
        List<ConsolidatedReport> GetConsolidatedReports(string sessionID, string from, string to, int userID);


        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getLogisticConsolidatedreports?from={from}&to={to}&userid={userID}&sessionid={sessionID}")]
        List<LogisticConsolidatedReport> GetLogisticConsolidatedReports(string sessionID, string from, string to, int userID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getconsolidatedbasepricereports?from={from}&to={to}&userid={userID}&sessionid={sessionID}")]
        List<ConsolidatedBasePriceReports> GetConsolidatedBasePriceReports(string sessionID, string from, string to, int userID);
    }
}
