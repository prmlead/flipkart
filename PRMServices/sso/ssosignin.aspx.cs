﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PRMServices
{
    public partial class ssosignin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
                //FederatedAuthentication.WSFederationAuthenticationModule.RedirectToIdentityProvider("http://localhost:24983/", "http://localhost:26756/user", true);

                var samlEndpoint = ConfigurationManager.AppSettings["samlEndpoint"].ToString();
                var request = new AuthRequest(
                    ConfigurationManager.AppSettings["AppUnitQueID"].ToString(), //put your app's "unique ID" here
                    ConfigurationManager.AppSettings["prmland"].ToString() //assertion Consumer Url - the redirect URL where the provider will send authenticated users
                    );

                //generate the provider URL
                string url = request.GetRedirectUrl(samlEndpoint);

                //then redirect your user to the above "url" var
                //for example, like this:
                Response.Redirect(url);
                //return View();
        }
    }
}