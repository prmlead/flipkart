﻿using System;
using System.Text;
using System.IdentityModel.Services;
using System.Xml.Linq;
using System.Net;
using System.Linq;
using System.Configuration;

namespace PRMServices
{
    public partial class ssologin : System.Web.UI.Page
    {

        public string email = string.Empty;
        public string loginPage = string.Empty;
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        protected void Page_Load(object sender, EventArgs e)
        {

            //string script1 = string.Format("localStorage.setItem('ssoUser1', '{0}');", "prm360@.com");
            //ClientScript.RegisterClientScriptBlock(this.GetType(), "key", script1, true);

            string samlContent = string.Empty;
            logger.Info("Starting");
            Response.Write("Starting \n \n");

            //logger.Info("URL:" + Request.UrlReferrer);
            //Response.Write("URL:" + Request.UrlReferrer + " \n \n");

            //logger.Info("Request:" + Request.Form.Count);
            //Response.Write("Request:" + Request.Form.Count + " \n \n");

            foreach (string entry in Request.Form)
            {
                logger.Info("entry:" + entry);
                logger.Info(entry + "----" + Request.Form[entry]);
                //Response.Write(entry + "----" + Request.Form[entry] + " \n \n");

                if (entry.Equals("SAMLResponse", StringComparison.InvariantCultureIgnoreCase))
                {
                    samlContent = Request.Form[entry];
                }
            }


            if (!string.IsNullOrEmpty(samlContent))
            {
                byte[] samlData = Convert.FromBase64String(samlContent);
                //Response.Write("after base 64 \n \n");
                //logger.Info("after base 64:" + samlData);
                //Response.Write(samlData);

                // read back into a UTF string
                string samlAssertion = Encoding.UTF8.GetString(samlData);
                //Response.Write("saml assertion \n \n");
                //logger.Info("saml assertion:" + samlAssertion);
                //Response.Write(samlAssertion);

                //string xml = @"
                //<root>
                //  <child id='1'/>
                //  <child id='2'>
                //    <grandchild id='3' />
                //    <grandchild id='4' />
                //  </child>
                //</root>";
                //XDocument doc = XDocument.Parse(samlAssertion);

                //Response.Write("saml doc -------------------------->" + doc + " \n \n");
                //logger.Info("saml doc:   -------------------------->" + doc + " \n \n");

                //foreach (XElement element in doc.Descendants("AttributeValue"))
                //{
                //    Console.WriteLine(element);
                //    Response.Write("saml element -------------------------->" + element + " \n \n");
                //    logger.Info("saml element:   -------------------------->" + element + " \n \n");
                //}

                

                
                logger.Info("samlAssertion:   -------------------------->" + samlAssertion + " \n \n");
                email = ParseResponse(samlAssertion);

                Response.Write("Login email -------------------------->" + email + " \n \n");
                logger.Info("Login email:   -------------------------->" + email + " \n \n");

                //sessionStorage.setItem("lastname", "Smith");

                string script = string.Format("localStorage.setItem('ssoaduser', '{0}');", email);
                //ClientScript.RegisterClientScriptBlock(this.GetType(), "key", script, true);
                ClientScript.RegisterStartupScript(this.GetType(), "ssoUser", script, true);
                string virtualDir = string.IsNullOrEmpty(ConfigurationManager.AppSettings["WEBSITE_ENV"].ToString()) ? "" : "/" + ConfigurationManager.AppSettings["WEBSITE_ENV"].ToString();
                //Response.Redirect($"{ConfigurationManager.AppSettings["WEBSITE_URL".ToString()]}{virtualDir}/prm360.html#/login");
                loginPage = $"{ConfigurationManager.AppSettings["WEBSITE_URL".ToString()]}{virtualDir}/prm360.html#/login";
                Response.Write("Login localStorage -------------------------->" + email + " \n \n");
                logger.Info("Login localStorage:   -------------------------->" + email + " \n \n");
            }
        }

        public string ParseResponse(string xml) {

            logger.Info("SAML RESPONSE :" + xml);
            var email = string.Empty;
            try
            {
                bool isValidResponse = false;
                XDocument responseDoc = XDocument.Parse(xml);
                XNamespace pr = "urn:oasis:names:tc:SAML:2.0:protocol";
                XNamespace ast = "urn:oasis:names:tc:SAML:2.0:assertion";
                XElement status = responseDoc.Element(pr + "Response").Element(pr + "Status");
                string statusCode = (string)status.Element(pr + "StatusCode").Attribute("Value");
                //string statusMessage = (string)status.Element(pr + "StatusMessage");
                XElement attStatement = responseDoc.Element(pr + "Response").Element(ast + "Assertion").Elements().ToList<XElement>().Find(e => e.Name.LocalName == "AttributeStatement");
                XElement condition = (responseDoc.Element(pr + "Response").Element(ast + "Assertion").Elements().ToList<XElement>().Find(e => e.Name.LocalName == "Conditions"));
                email = ((System.Xml.Linq.XElement)((System.Xml.Linq.XContainer)attStatement.FirstNode).FirstNode).Value;
                if (condition != null)
                {
                    DateTime notBefore = Convert.ToDateTime(condition.FirstAttribute.Value);
                    DateTime notAfter = Convert.ToDateTime(condition.LastAttribute.Value);
                    if (DateTime.Now >= notBefore && DateTime.Now <= notAfter)
                    {
                        isValidResponse = true;
                    }
                }

                var attributeNode = ((System.Xml.Linq.XElement)attStatement.FirstNode).FirstAttribute;
                if (isValidResponse && attributeNode != null && !string.IsNullOrEmpty(attributeNode.Value) && attributeNode.Value.Equals("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress"))
                {
                    email = ((System.Xml.Linq.XElement)((System.Xml.Linq.XContainer)attStatement.FirstNode).FirstNode).Value;
                }
            }
            catch (Exception ex) {
                logger.Info("ParseResponse Error:   -------------------------->" + ex.Message + " \n \n");
            }
            logger.Info("ParseResponse email:   -------------------------->" + email + " \n \n");
            return email;
        }


        protected void Page_LoadComplete(object sender, EventArgs e)
        {
//            Response.Redirect("https://allcargo.prm360.com/WEBDEV/prm360.html#/login?ssoUser=" + );
        }


    }
}