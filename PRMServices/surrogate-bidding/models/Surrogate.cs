﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class Surrogate : Entity
    {
        [DataMember] [DataNames("VENDOR_ID")] public int VENDOR_ID { get; set; }
        [DataMember] [DataNames("VENDOR_NAME")] public string VENDOR_NAME { get; set; }
        [DataMember] [DataNames("CUSTOMER_ID")] public int CUSTOMER_ID { get; set; }
        [DataMember] [DataNames("CUSTOMER_NAME")] public string CUSTOMER_NAME { get; set; }
        [DataMember] [DataNames("COMMENTS")] public string COMMENTS { get; set; }
        [DataMember] [DataNames("IS_VALID")] public int IS_VALID { get; set; }
    }
}