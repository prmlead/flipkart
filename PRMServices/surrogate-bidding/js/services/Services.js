prmApp.constant('PRMSurrogateBidDomain', 'surrogate-bidding/svc/PRMSurrogateBid.svc/REST/');
prmApp.constant('signalRFwdHubName', 'fwdRequirementHub');
prmApp.service('PRMSurrogateBidService', ["PRMSurrogateBidDomain", "userService", "httpServices",
    function (PRMSurrogateBidDomain, userService, httpServices) {


        var PRMSurrogateBidService = this;


        PRMSurrogateBidService.GetSurrogates = function (params) {
            var url = PRMSurrogateBidDomain + 'getsurrogateusers?reqid=' + params.reqid + '&vendorid=' + params.vendorid + '&deptid=' + params.deptid + '&onlyvalid=true&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMSurrogateBidService.SaveSurrogateUsers = function (params) {
            let url = PRMSurrogateBidDomain + 'savesurrogateusers';
            return httpServices.post(url, params);
        };

        return PRMSurrogateBidService;

}]);