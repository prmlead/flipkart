﻿using PRM.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class POScheduleDetails : ResponseAudit
    {
        [DataMember] [DataNames("PO_NUMBER")] public string PO_NUMBER { get; set; }
        [DataMember] [DataNames("DELIVERY_DATE")] public DateTime? DELIVERY_DATE { get; set; }
        [DataMember] [DataNames("PO_RECEIPT_DATE")] public DateTime? PO_RECEIPT_DATE { get; set; }
        [DataMember] [DataNames("VENDOR_COMPANY")] public string VENDOR_COMPANY { get; set; }
        [DataMember] [DataNames("PO_STATUS")] public string PO_STATUS { get; set; }
        [DataMember] [DataNames("PO_QTY_STATUS")] public string PO_QTY_STATUS { get; set; }
        [DataMember] [DataNames("PO_STATUS")] public string PO_AMMEND_QTY_STATUS { get; set; }
        [DataMember] [DataNames("DELIVERY_COMPLETED")] public string DELIVERY_COMPLETED { get; set; }
        [DataMember] [DataNames("PO_CLOSED_DATE")] public DateTime? PO_CLOSED_DATE { get; set; }
        [DataMember] [DataNames("PLANT")] public string PLANT { get; set; }
        [DataMember] [DataNames("PLANT_NAME")] public string PLANT_NAME { get; set; }
        [DataMember] [DataNames("PLANT_LOCATION")] public string PLANT_LOCATION { get; set; }
        [DataMember] [DataNames("VENDOR_CODE")] public string VENDOR_CODE { get; set; }
        [DataMember] [DataNames("VENDOR_SITE_CODE")] public string VENDOR_SITE_CODE { get; set; }
        [DataMember] [DataNames("GST_NUMBER")] public string GST_NUMBER { get; set; }
        [DataMember] [DataNames("GST_ADDR")] public string GST_ADDR { get; set; }

        [DataMember] [DataNames("VENDOR_ID")] public int VENDOR_ID { get; set; }
        [DataMember] [DataNames("PAYMENT_TERMS")] public string PAYMENT_TERMS { get; set; }
        [DataMember] [DataNames("PAYMENT_TERMS_DESC")] public string PAYMENT_TERMS_DESC { get; set; }
        [DataMember] [DataNames("VENDOR_PRIMARY_EMAIL")] public string VENDOR_PRIMARY_EMAIL { get; set; }
        [DataMember] [DataNames("VENDOR_PRIMARY_PHONE_NUMBER")] public string VENDOR_PRIMARY_PHONE_NUMBER { get; set; }
        [DataMember] [DataNames("PO_CREATOR")] public string PO_CREATOR { get; set; }
        [DataMember] [DataNames("PO_CREATOR_NAME")] public string PO_CREATOR_NAME { get; set; }
        [DataMember] [DataNames("DOC_TYPE")] public string DOC_TYPE { get; set; }
        [DataMember] [DataNames("PO_RELEASE_DATE")] public DateTime? PO_RELEASE_DATE { get; set; }
        [DataMember] [DataNames("NET_PRICE")] public decimal NET_PRICE { get; set; }
        [DataMember] [DataNames("TOTAL_VALUE")] public decimal TOTAL_VALUE { get; set; }
        [DataMember] [DataNames("CURRENCY")] public string CURRENCY { get; set; }
        [DataMember] [DataNames("TOTAL_COUNT")] public int TOTAL_COUNT { get; set; }
        [DataMember] [DataNames("PO_DATE")] public DateTime? PO_DATE { get; set; }
        [DataMember] [DataNames("VENDOR_EXPECTED_DELIVERY_DATE")] public DateTime? VENDOR_EXPECTED_DELIVERY_DATE { get; set; }
        [DataMember] [DataNames("VENDOR_EXPECTED_DELIVERY_DATE_STRING")] public string VENDOR_EXPECTED_DELIVERY_DATE_STRING { get; set; }
        [DataMember] [DataNames("STATS_TOTAL_COUNT")] public int STATS_TOTAL_COUNT { get; set; }
        [DataMember] [DataNames("STATS_PO_AWAITING_RECEIPT")] public int STATS_PO_AWAITING_RECEIPT { get; set; }
        [DataMember] [DataNames("IS_PO_ACK")] public int IS_PO_ACK { get; set; }
        [DataMember] [DataNames("STATS_PO_NOT_INITIATED")] public int STATS_PO_NOT_INITIATED { get; set; }
        [DataMember] [DataNames("STATS_PO_PARTIAL_DELIVERY")] public int STATS_PO_PARTIAL_DELIVERY { get; set; }
        [DataMember] [DataNames("VENDOR_ACK_STATUS")] public string VENDOR_ACK_STATUS { get; set; }
        [DataMember] [DataNames("ASN_CODE")] public string ASN_CODE { get; set; }
        [DataMember] [DataNames("ASN_ID")] public int ASN_ID { get; set; }

        [DataMember] [DataNames("GRN_STATUS")] public string GRN_STATUS { get; set; }
        [DataMember] [DataNames("HEADER_TEXT")] public string HEADER_TEXT { get; set; }

        [DataMember(Name = "attachmentsArray")]
        public List<FileUpload> AttachmentsArray { get; set; }

        [DataMember(Name = "vendorAttachmentsArray")]
        public List<FileUpload> VendorAttachmentsArray { get; set; }
        [DataMember] [DataNames("ATTACHMENTS")] public string ATTACHMENTS { get; set; }
        [DataMember] [DataNames("VENDOR_ATTACHEMNTS")] public string VENDOR_ATTACHEMNTS { get; set; }
        [DataMember] [DataNames("INCO_TERMS")] public string INCO_TERMS { get; set; }
        [DataMember] [DataNames("ADDRESS")] public string ADDRESS { get; set; }
        [DataMember] [DataNames("VENDOR_ACK_REJECT_COMMENTS")] public string VENDOR_ACK_REJECT_COMMENTS { get; set; }

        [DataMember] [DataNames("SessionID")] public string SessionID { get; set; }

        [DataMember] [DataNames("EMAIL_SENT_DATE")] public DateTime? EMAIL_SENT_DATE { get; set; }
        [DataMember] [DataNames("DATE_CREATED")] public DateTime? DATE_CREATED { get; set; }
        [DataMember] [DataNames("DATE_MODIFIED")] public DateTime? DATE_MODIFIED { get; set; }
        [DataMember] [DataNames("STATUS")] public string STATUS { get; set; }
        [DataMember] [DataNames("BILL_TO_LOCATION")] public string BILL_TO_LOCATION { get; set; }
        [DataMember] [DataNames("SHIP_TO_LOCATION")] public string SHIP_TO_LOCATION { get; set; }
        [DataMember] [DataNames("VENDOR_GST_REGISTRATION")] public string VENDOR_GST_REGISTRATION { get; set; }
        [DataMember] [DataNames("EWC")] public string EWC { get; set; }
        [DataMember] [DataNames("REVISION_NUMBER")] public string REVISION_NUMBER { get; set; }

    }

    public class POScheduleDetailsItems : ResponseAudit
    {
        [DataMember] [DataNames("COMP_ID")] public int COMP_ID { get; set; }
        [DataMember] [DataNames("QCS_ID")] public int QCS_ID { get; set; }
        [DataMember] [DataNames("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
        [DataMember] [DataNames("TYPE_OF_STOCK")] public string TYPE_OF_STOCK { get; set; }
        [DataMember] [DataNames("ENTITY")] public string ENTITY { get; set; }
        [DataMember] [DataNames("USER")] public int USER { get; set; }
        [DataMember] [DataNames("VENDOR_ID")] public int VENDOR_ID { get; set; }
        [DataMember] [DataNames("VENDOR_CODE")] public string VENDOR_CODE { get; set; }
        [DataMember] [DataNames("BILL_TO_LOCATION")] public string BILL_TO_LOCATION { get; set; }
        [DataMember] [DataNames("SHIP_TO_LOCATION")] public string SHIP_TO_LOCATION { get; set; }
        [DataMember] [DataNames("VENDOR_GST_REGISTRATION")] public string VENDOR_GST_REGISTRATION { get; set; }
        [DataMember] [DataNames("GST_REGISTRATION")] public string GST_REGISTRATION { get; set; }
        [DataMember] [DataNames("VENDOR_SITE_CODE")] public string VENDOR_SITE_CODE { get; set; }
        [DataMember] [DataNames("DELIVERY_LOCATION")] public string DELIVERY_LOCATION { get; set; }
        [DataMember] [DataNames("PAYMENT_TERMS")] public string PAYMENT_TERMS { get; set; }
        [DataMember] [DataNames("PO_NUMBER")] public string PO_NUMBER { get; set; }
        [DataMember] [DataNames("PRODUCT_ID")] public string PRODUCT_ID { get; set; }
        [DataMember] [DataNames("PRODUCT_CODE")] public string PRODUCT_CODE { get; set; }
        [DataMember] [DataNames("PRODUCT_NAME")] public string PRODUCT_NAME { get; set; }
        [DataMember] [DataNames("CATEGORY_ID")] public string CATEGORY_ID { get; set; }        
        [DataMember] [DataNames("PO_MATERIAL_DESC")] public string PO_MATERIAL_DESC { get; set; }
        [DataMember] [DataNames("HSN_CODE")] public string HSN_CODE { get; set; }
        [DataMember] [DataNames("CASE_ID")] public string CASE_ID { get; set; }
        [DataMember] [DataNames("PO_LINE_ITEM")] public string PO_LINE_ITEM { get; set; }
        [DataMember] [DataNames("PR_ID")] public int PR_ID { get; set; }
        [DataMember] [DataNames("PR_ITEM_ID")] public int PR_ITEM_ID { get; set; }
        [DataMember] [DataNames("REQ_ID")] public int REQ_ID { get; set; }
        [DataMember] [DataNames("REQ_ITEM_ID")] public int REQ_ITEM_ID { get; set; }
        [DataMember] [DataNames("PR_LINE_ITEM")] public string PR_LINE_ITEM { get; set; }
        [DataMember] [DataNames("TAX_CODE")] public string TAX_CODE { get; set; }
        [DataMember] [DataNames("MAT_TYPE")] public string MAT_TYPE { get; set; }
        [DataMember] [DataNames("PR_QTY")] public decimal PR_QTY { get; set; }
        [DataMember] [DataNames("ACK_QTY")] public decimal ACK_QTY { get; set; }
        [DataMember] [DataNames("ACK_DATE")] public DateTime? ACK_DATE { get; set; }
        [DataMember] [DataNames("PR_RELEASE_DATE")] public DateTime? PR_RELEASE_DATE { get; set; }
        [DataMember] [DataNames("PO_RELEASE_DATE")] public DateTime? PO_RELEASE_DATE { get; set; }
        [DataMember] [DataNames("DELIVERY_DATE")] public DateTime? DELIVERY_DATE { get; set; }
        [DataMember] [DataNames("CITY")] public string CITY { get; set; }
        [DataMember] [DataNames("UOM")] public string UOM { get; set; }
        [DataMember] [DataNames("ALTERNATIVE_UOM")] public string ALTERNATIVE_UOM { get; set; }
        [DataMember] [DataNames("ALTERNATIVE_UOM_QTY")] public decimal ALTERNATIVE_UOM_QTY { get; set; }
        [DataMember] [DataNames("FREIGHT")] public decimal FREIGHT { get; set; }
        [DataMember] [DataNames("MISC_CHARGES")] public decimal MISC_CHARGES { get; set; }
        [DataMember] [DataNames("PACKING_CHARGES")] public decimal PACKING_CHARGES { get; set; }
        [DataMember] [DataNames("CATEGORY_CODE")] public string CATEGORY_CODE { get; set; }
        [DataMember] [DataNames("ITEM_TEXT_PO")] public string ITEM_TEXT_PO { get; set; }
        [DataMember] [DataNames("REL_IND")] public string REL_IND { get; set; }
        [DataMember] [DataNames("DOC_TYPE")] public string DOC_TYPE { get; set; }
        [DataMember] [DataNames("PO_CONTRACT")] public string PO_CONTRACT { get; set; }
        [DataMember] [DataNames("DELETED_INDICATOR")] public string DELETED_INDICATOR { get; set; }
        [DataMember] [DataNames("DELIVERY_COMPLETED")] public string DELIVERY_COMPLETED { get; set; }
        [DataMember] [DataNames("ORDER_QTY")] public decimal ORDER_QTY { get; set; }
        [DataMember] [DataNames("AVAILABLE_QTY")] public decimal AVAILABLE_QTY { get; set; }
        [DataMember] [DataNames("RECEIVED_QTY")] public decimal RECEIVED_QTY { get; set; }
        [DataMember] [DataNames("LAST_RECEIVED_DATE")] public DateTime? LAST_RECEIVED_DATE { get; set; }
        [DataMember] [DataNames("PO_ITEM_CHANGE_DATE")] public DateTime? PO_ITEM_CHANGE_DATE { get; set; }
        [DataMember] [DataNames("VALID_TO")] public DateTime? VALID_TO { get; set; }
        [DataMember] [DataNames("VALID_FROM")] public DateTime? VALID_FROM { get; set; }
        [DataMember] [DataNames("REJECTED_QTY")] public decimal REJECTED_QTY { get; set; }
        [DataMember] [DataNames("REMAINING_QTY")] public decimal REMAINING_QTY { get; set; }
        [DataMember] [DataNames("PENDING_QTY")] public decimal PENDING_QTY { get; set; }
        [DataMember] [DataNames("NET_PRICE")] public decimal NET_PRICE { get; set; }
        [DataMember] [DataNames("TOTAL_VALUE")] public decimal TOTAL_VALUE { get; set; }
        [DataMember] [DataNames("CURRENCY")] public string CURRENCY { get; set; }
        [DataMember] [DataNames("GRN_NUMBER")] public string GRN_NUMBER { get; set; }
        [DataMember] [DataNames("GRN_RECEIVED_QTY")] public decimal GRN_RECEIVED_QTY { get; set; }
        [DataMember] [DataNames("GRN_DELIVERY_DATE")] public DateTime? GRN_DELIVERY_DATE { get; set; }
        [DataMember] [DataNames("GRN_LINE_ITEM")] public string GRN_LINE_ITEM { get; set; }
        [DataMember] [DataNames("VENDOR_ACK_STATUS")] public string VENDOR_ACK_STATUS { get; set; }
        [DataMember] [DataNames("VENDOR_ACK_COMMENTS")] public string VENDOR_ACK_COMMENTS { get; set; }
        [DataMember] [DataNames("VENDOR_EXPECTED_DELIVERY_DATE")] public DateTime? VENDOR_EXPECTED_DELIVERY_DATE { get; set; }

        [DataMember] [DataNames("VENDOR_EXPECTED_DELIVERY_DATE_STRING")] public string VENDOR_EXPECTED_DELIVERY_DATE_STRING { get; set; }
        [DataMember] [DataNames("CGST")] public decimal CGST { get; set; }
        [DataMember] [DataNames("SGST")] public decimal SGST { get; set; }
        [DataMember] [DataNames("IGST")] public decimal IGST { get; set; }
        [DataMember] [DataNames("CESS")] public decimal CESS { get; set; }
        [DataMember] [DataNames("TCS")] public decimal TCS { get; set; }
        [DataMember] [DataNames("PO_CREATOR")] public string PO_CREATOR { get; set; }
        [DataMember] [DataNames("PO_CREATOR_NAME")] public string PO_CREATOR_NAME { get; set; }
        [DataMember] [DataNames("PR_NUMBER")] public string PR_NUMBER { get; set; }
        [DataMember] [DataNames("PR_REQUISITIONER")] public string PR_REQUISITIONER { get; set; }
        [DataMember] [DataNames("GRNItem")] public List<GRNItem> GRNItems { get; set; }
        [DataMember] [DataNames("ITEM_MAT_TEXT")] public string ITEM_MAT_TEXT { get; set; }
        [DataMember] [DataNames("ITEM_GROSS_PRICE")] public decimal ITEM_GROSS_PRICE { get; set; }
        [DataMember] [DataNames("ITEM_DISCOUNT_VALUE")] public decimal ITEM_DISCOUNT_VALUE { get; set; }
        [DataMember] [DataNames("ITEM_DISCOUNT_PERCENTAGE")] public decimal ITEM_DISCOUNT_PERCENTAGE { get; set; }

        [DataMember] [DataNames("VENDOR_ACK_REJECT_COMMENTS")] public string VENDOR_ACK_REJECT_COMMENTS { get; set; }

        [DataMember] [DataNames("REMAINING_NET_QTY")] public decimal REMAINING_NET_QTY { get; set; }


        [DataMember] [DataNames("UNIT_PRICE_NO_TAX")] public decimal UNIT_PRICE_NO_TAX { get; set; }
        [DataMember] [DataNames("UNIT_PRICE")] public decimal UNIT_PRICE { get; set; }
        [DataMember] [DataNames("ITEM_TOTAL_PRICE_NO_TAX")] public decimal ITEM_TOTAL_PRICE_NO_TAX { get; set; }
        [DataMember] [DataNames("ITEM_TOTAL_PRICE")] public decimal ITEM_TOTAL_PRICE { get; set; }
        [DataMember] [DataNames("DISCOUNT_VALUE")] public decimal DISCOUNT_VALUE { get; set; }
        [DataMember] [DataNames("DISCOUNT_PERC")] public decimal DISCOUNT_PERC { get; set; }
        [DataMember] [DataNames("GROUPED_ITEMS")] public POScheduleDetailsItems[] GROUPED_ITEMS { get; set; }
        [DataMember] [DataNames("INCO_TERMS")] public string INCO_TERMS { get; set; }
        [DataMember] [DataNames("HEADER_TEXT")] public string HEADER_TEXT { get; set; }
        [DataMember] [DataNames("WF_ID")] public int WF_ID { get; set; }
        [DataMember] [DataNames("BRAND")] public string BRAND { get; set; }
        [DataMember] [DataNames("BRAND_PART_CODE")] public string BRAND_PART_CODE { get; set; }
        [DataMember] [DataNames("MODEL_NAME")] public string MODEL_NAME { get; set; }
        [DataMember] [DataNames("TOTAL_INVOICE_LINE_ITEM_REMAINING_QTY")] public decimal TOTAL_INVOICE_LINE_ITEM_REMAINING_QTY { get; set; }
        [DataMember] [DataNames("INVOICE_QTY")] public decimal INVOICE_QTY { get; set; }
        [DataMember] [DataNames("PO_INV")] public List<POInvoice> PO_INV { get; set; }
        [DataMember] [DataNames("ITEM_SOURCE")] public int ITEM_SOURCE { get; set; }
        [DataMember] [DataNames("SHIPPING_WAY")] public int SHIPPING_WAY { get; set; }
        [DataMember] [DataNames("SVC_CENTER_CODE")] public string SVC_CENTER_CODE { get; set; }
        [DataMember] [DataNames("STATUS")] public string STATUS { get; set; }
        [DataMember] [DataNames("SKU")] public string SKU { get; set; }
        [DataMember] [DataNames("VENDOR_RFQ_GST_NUMBER")] public string VENDOR_RFQ_GST_NUMBER { get; set; }
        [DataMember] [DataNames("PO_STATUS")] public string PO_STATUS { get; set; }
        [DataMember] [DataNames("EWC")] public string EWC { get; set; }
        [DataMember] [DataNames("TAX_CATEGORY_TYPE")] public string TAX_CATEGORY_TYPE { get; set; }
        [DataMember] [DataNames("PO_TYPE")] public string PO_TYPE { get; set; }
        [DataMember] [DataNames("ADVANCE_AMOUNT")] public decimal ADVANCE_AMOUNT { get; set; }
        [DataMember] [DataNames("ADVANCE_AMOUNT_OVERALL")] public decimal ADVANCE_AMOUNT_OVERALL { get; set; }
        [DataMember] [DataNames("OLD_NET_PRICE")] public decimal OLD_NET_PRICE { get; set; }
        [DataMember] [DataNames("CREATED_BY")] public int CREATED_BY { get; set; }
        [DataMember] [DataNames("MODIFIED_BY")] public int MODIFIED_BY { get; set; }
        [DataMember] [DataNames("PREV_APPROVED")] public int PREV_APPROVED { get; set; }

    }

    [DataContract]
    public class GRNItem : ResponseAudit
    {
        [DataMember] [DataNames("GRN_NUMBER")] public string GRN_NUMBER { get; set; }
        [DataMember] [DataNames("GRN_LINE_ITEM")] public string GRN_LINE_ITEM { get; set; }
        [DataMember] [DataNames("GRN_RECEIVED_QTY")] public decimal GRN_RECEIVED_QTY { get; set; }
        [DataMember] [DataNames("PO_ORDER_QTY")] public decimal PO_ORDER_QTY { get; set; }
        [DataMember] [DataNames("GRN_REJECTED_QTY")] public decimal GRN_REJECTED_QTY { get; set; }
        [DataMember] [DataNames("GRN_DELIVERY_DATE")] public DateTime? GRN_DELIVERY_DATE { get; set; }
        [DataMember] [DataNames("PO_NUMBER")] public string PO_NUMBER { get; set; }
        [DataMember] [DataNames("PO_LINE_ITEM")] public string PO_LINE_ITEM { get; set; }
    }

    [DataContract]
    public class POInvoice : ResponseAudit
    {
        [DataMember] [DataNames("PO_NUMBER")] public string PO_NUMBER { get; set; }
        [DataMember] [DataNames("CURRENCY")] public string CURRENCY { get; set; }
        [DataMember] [DataNames("VENDOR_SITE_CODE")] public string VENDOR_SITE_CODE { get; set; }
        [DataMember] [DataNames("TRANSACTION_TYPE")] public string TRANSACTION_TYPE { get; set; }
        [DataMember] [DataNames("INDENT_OF_PURCHASE")] public string INDENT_OF_PURCHASE { get; set; }
        [DataMember] [DataNames("REGISTERED_UNDER_GST")] public string REGISTERED_UNDER_GST { get; set; }
        [DataMember] [DataNames("IS_RCM_APPLICABLE")] public string IS_RCM_APPLICABLE { get; set; }
        [DataMember] [DataNames("NATURE_OF_TRANSACTION")] public string NATURE_OF_TRANSACTION { get; set; }
        [DataMember] [DataNames("EXCHANGE_RATE")] public string EXCHANGE_RATE { get; set; }
        [DataMember] [DataNames("PAYMENT_TERMS")] public string PAYMENT_TERMS { get; set; }
        [DataMember] [DataNames("PAYMENT_METHOD")] public string PAYMENT_METHOD { get; set; }
        [DataMember] [DataNames("PAYGROUP_DESCRIPTION")] public string PAYGROUP_DESCRIPTION { get; set; }
        [DataMember] [DataNames("IRN_NO")] public string IRN_NO { get; set; }
        [DataMember] [DataNames("ENTITY")] public string ENTITY { get; set; }
        [DataMember] [DataNames("INVOICE_NUMBER")] public string INVOICE_NUMBER { get; set; }
        [DataMember] [DataNames("INVOICE_AMOUNT")] public decimal INVOICE_AMOUNT { get; set; }
        [DataMember] [DataNames("VENDOR_ID")] public int VENDOR_ID { get; set; }
        [DataMember] [DataNames("INVOICE_ID")] public int INVOICE_ID { get; set; }
        [DataMember] [DataNames("WF_ID")] public int WF_ID { get; set; }
        [DataMember] [DataNames("VENDOR_CODE")] public string VENDOR_CODE { get; set; }
        [DataMember] [DataNames("COMMENTS")] public string COMMENTS { get; set; }
        [DataMember] [DataNames("STATUS")] public string STATUS { get; set; }
        [DataMember] [DataNames("LOCATION")] public string LOCATION { get; set; }
        [DataMember] [DataNames("VENDOR_COMP_NAME")] public string VENDOR_COMP_NAME { get; set; }
        [DataMember] [DataNames("VENDOR_NAME")] public string VENDOR_NAME { get; set; }
        [DataMember] [DataNames("DATE_CREATED")] public DateTime? DATE_CREATED { get; set; }
        [DataMember] [DataNames("DATE_MODIFIED")] public DateTime? DATE_MODIFIED { get; set; }

        //[DataMember] [DataNames("attachmentsArray")] public List<FileUpload> AttachmentsArray { get; set; }

        [DataMember(Name = "attachmentsArray")]
        public List<FileUpload> AttachmentsArray { get; set; }

        [DataMember] [DataNames("itemAttachment")] public byte[] itemAttachment { get; set; }
        [DataMember] [DataNames("attachmentName")] public string attachmentName { get; set; }

        [DataMember] [DataNames("ATTACHMENTS")] public string ATTACHMENTS { get; set; }
        [DataMember] [DataNames("SessionID")] public string SessionID { get; set; }

        [DataMember] [DataNames("C_COMP_ID")] public int C_COMP_ID { get; set; }
        [DataMember] [DataNames("V_COMP_ID")] public int V_COMP_ID { get; set; }
        [DataMember] [DataNames("INV_TYPE")] public string INV_TYPE { get; set; }
        [DataMember] [DataNames("CUSTOMER_NAME")] public string CUSTOMER_NAME { get; set; }
        [DataMember] [DataNames("INVOICE_TYPE")] public string INVOICE_TYPE { get; set; }
        [DataMember] [DataNames("ASN_NUMBER")] public string ASN_NUMBER { get; set; }
        [DataMember] [DataNames("FROM_ADDRESS")] public string FROM_ADDRESS { get; set; }
        [DataMember] [DataNames("TO_ADDRESS")] public string TO_ADDRESS { get; set; }
        [DataMember] [DataNames("INVOICE_DATE")] public DateTime? INVOICE_DATE { get; set; }
        [DataMember] [DataNames("DUE_DATE")] public DateTime? DUE_DATE { get; set; }
        [DataMember] [DataNames("SUB_TOTAL")] public decimal SUB_TOTAL { get; set; }
        [DataMember] [DataNames("TAX")] public decimal TAX { get; set; }
        [DataMember] [DataNames("CGST")] public decimal CGST { get; set; }
        [DataMember] [DataNames("IGST")] public decimal IGST { get; set; }
        [DataMember] [DataNames("SGST")] public decimal SGST { get; set; }
        [DataMember] [DataNames("TOTAL")] public decimal TOTAL { get; set; }
        [DataMember] [DataNames("PO_LINE_ITEM")] public string PO_LINE_ITEM { get; set; }
        [DataMember] [DataNames("INVOICE_QTY")] public decimal INVOICE_QTY { get; set; }
        [DataMember] [DataNames("ORDER_QTY")] public decimal ORDER_QTY { get; set; }
        [DataMember] [DataNames("NET_PRICE")] public decimal NET_PRICE { get; set; }
        [DataMember] [DataNames("UNIT_PRICE")] public decimal UNIT_PRICE { get; set; }
        [DataMember] [DataNames("UNIT_PRICE_NO_TAX")] public decimal UNIT_PRICE_NO_TAX { get; set; }
        [DataMember] [DataNames("PRODUCT_CODE")] public string PRODUCT_CODE { get; set; }
        [DataMember] [DataNames("PRODUCT_NAME")] public string PRODUCT_NAME { get; set; }
        [DataMember] [DataNames("INVOICE_STATUS")] public string INVOICE_STATUS { get; set; }
        [DataMember] [DataNames("CONVERTED_PDFS")] public string CONVERTED_PDFS { get; set; }
        [DataMember] [DataNames("CONVERTED_PDFS_SYSTEM")] public string CONVERTED_PDFS_SYSTEM { get; set; }
        [DataMember] [DataNames("EWC")] public string EWC { get; set; }
        [DataMember] [DataNames("TOTAL_INVOICE_LINE_ITEM_REMAINING_QTY")] public decimal TOTAL_INVOICE_LINE_ITEM_REMAINING_QTY { get; set; }
        [DataMember] [DataNames("PO_INV")] public List<POInvoice> PO_INV { get; set; }
        [DataMember] [DataNames("TAX_CATEGORY_TYPE")] public string TAX_CATEGORY_TYPE { get; set; }
        [DataMember] [DataNames("PO_TYPE")] public string PO_TYPE { get; set; }
        [DataMember] [DataNames("IS_ACK")] public int IS_ACK { get; set; }
        [DataMember] [DataNames("ACK_COMMENTS")] public string ACK_COMMENTS { get; set; }
        [DataMember] [DataNames("INVOICE_COMMENTS")] public string INVOICE_COMMENTS { get; set; }
        [DataMember] [DataNames("CREATED_BY")] public int CREATED_BY { get; set; }
        [DataMember] [DataNames("WF_STATUS")] public string WF_STATUS { get; set; }
        [DataMember] [DataNames("ROW_ID")] public int ROW_ID { get; set; }
        [DataMember] [DataNames("INVOICE_ROW_ID")] public int INVOICE_ROW_ID { get; set; }




    }
    [DataContract]
    public class PORejection : ResponseAudit
    {
        [DataMember] [DataNames("VENDOR_EMAIL")] public string VENDOR_EMAIL { get; set; }
        [DataMember] [DataNames("CUSTOMER_EMAIL")] public string CUSTOMER_EMAIL { get; set; }
        [DataMember] [DataNames("VENDOR_NAME")] public string VENDOR_NAME { get; set; }
        [DataMember] [DataNames("CUSTOMER_NAME")] public string CUSTOMER_NAME { get; set; }
        [DataMember] [DataNames("PO_NUMBER")] public string PO_NUMBER { get; set; }

    }
}