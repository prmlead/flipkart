﻿prmApp
    .controller('poDomesticZSDMRevCtrl', ["$state", "$stateParams", "$scope", "$rootScope", "auctionsService", "userService", "$http", "$window", "domain",
        "fileReader", "growlService", "$log", "$filter", "ngDialog", "catalogService", "PRMPRServices", "PRMCustomFieldService", "$modal", "PRMPOServices", "reportingService", "PRMPOService", "$q", "workflowService",
        function ($state, $stateParams, $scope, $rootScope, auctionsService, userService, $http, $window, domain,
            fileReader, growlService, $log, $filter, ngDialog, catalogService, PRMPRServices, PRMCustomFieldService, $modal, PRMPOServices, reportingService, PRMPOService, $q, workflowService) {

            $scope.currentUserId = +userService.getUserId();
            $scope.isCustomer = userService.getUserType() === "CUSTOMER" ? true : false;
            $scope.isSuperUser = userService.getUserObj().isSuperUser;
            $scope.currentUserSessionId = userService.getUserToken();
            $scope.currentUsercompanyId = userService.getUserCompanyId();
            $scope.currentUserName = userService.getFirstname() + ' ' + userService.getLastname();
            $scope.currentUserSapId = userService.getSAPUserId();
            $scope.qcsId = $stateParams.qcsId;
            $scope.poNumber = $stateParams.Id;
            $scope.reqId = $stateParams.reqId;
            $scope.prNumber = $stateParams.prNumber ? $stateParams.prNumber : '';
            $scope.poQuotId = $stateParams.quotId;
            $scope.prID = $stateParams.prId;
            $scope.isEditable = $stateParams.isEditable;
            $scope.vendorAssignments = $stateParams.detailsObj;
            $scope.poRAWJSON = $stateParams.poRawJSON;
            let poRoundDigits = 2;
            $scope.POServiceStatusText = '';
            $scope.documentType = $stateParams.templateName;
            $scope.selectedVendors = [];
            $scope.currentUIVendor = {};
            $scope.currentUIPlant = {};
            $scope.formDetails = {};
            $scope.formDetails.items = [];
            $scope.reqDepartments = [];
            $scope.requirementPRItems = [];
            $scope.requirementPOItems = [];
            $scope.requirementVendorCodeInfo = [];
            $scope.qcsRequirementDetails = {};
            $scope.minDateMoment = +$scope.poNumber !== 0 ? '' : moment();
            $scope.isFormdisabled = false;

            $scope.array = [];
            $scope.array1 = {};


            $scope.formDetails.MEMORY_TYPE = 'X';
            $scope.formDetails.PR_NUMBER = '';
            $scope.formDetails.PO_CREATOR = $scope.currentUserSapId;//$scope.requirementDetails && $scope.requirementDetails.prCreator ? $scope.requirementDetails.prCreator : '';
            $scope.formDetails.INCO_TERMS1 = '';
            $scope.formDetails.INCO_TERMS2 = '';
            $scope.formDetails.DOC_TYPE = $scope.documentType;
            $scope.requirementPlants = [];
            $scope.quantityError = false;
            var SelectedVendorDetails;
            $scope.companyConfigList = [];
            $scope.companyConfigStr = '';
            $scope.companyConfigurationArr = ['GST_DETAILS_F1', 'GST_DETAILS_JEEVES'];
            $scope.companyConfigStr = $scope.companyConfigurationArr.join(',').toString();
            $scope.COMPANY_INCO_TERMS = [];
            $scope.MasterPOs = [];
            $scope.deptIDs = [];
            $scope.desigIDs = [];
            $scope.deptIDStr = '';
            $scope.desigIDStr = '';
            $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
            if ($scope.ListUserDepartmentDesignations && $scope.ListUserDepartmentDesignations.length > 0) {
                $scope.ListUserDepartmentDesignations.forEach(function (item, index) {
                    $scope.deptIDs.push(item.deptID);
                    item.listDesignation.forEach(function (item1, index1) {
                        if (item1.isAssignedToUser && item1.isValid) {
                            $scope.desigIDs.push(item1.desigID);
                            $scope.desigIDs = _.union($scope.desigIDs, [item1.desigID]);
                        }
                    });
                });
                $scope.deptIDStr = $scope.deptIDs.join(',');
                $scope.desigIDStr = $scope.desigIDs.join(',');
            }

            /*region start WORKFLOW*/
            $scope.workflowList = [];
            $scope.itemWorkflow = [];
            $scope.obj = {
                auctionVendors: []
            };
            $scope.objNew = {
                auctionVendors: []
            };
            $scope.workflowObj = {};
            $scope.workflowObj.workflowID = 0;
            $scope.currentStep = 0;
            $scope.orderInfo = 0;
            $scope.assignToShow = '';
            $scope.isWorkflowCompleted = false;
            $scope.WorkflowModule = 'PO';
            $scope.disableWFSelection = false;
            /*region end WORKFLOW*/

            //auctionsService.GetCompanyConfiguration($scope.currentUsercompanyId, 'INCO', userService.getUserToken())
            //    .then(function (unitResponse) {
            //        $scope.companyConfigList = unitResponse;
            //        $scope.companyConfigList.forEach(function (item, index) {
            //            if (item.configKey === 'INCO') {
            //                $scope.COMPANY_INCO_TERMS.push(item);
            //            }
            //        });
            //    });

            $scope.precisionRound = function (number) {
                var factor = Math.pow(10, poRoundDigits);
                return Math.round(number * factor) / factor;
            };

            $scope.addItem = function () {
                $scope.formDetails.items.push({
                    ITEM_OF_REQUESITION: '',
                    isDeleted: false
                });
            };

            $scope.deleteItem = function (item) {
                item.isDeleted = true;
            };

            $scope.pageNo = 1;
            $scope.nextpage = function (pageNo) {
                $scope.pageNo = $scope.pageNo + 1;
            };

            $scope.previouspage = function () {
                $scope.pageNo = $scope.pageNo - 1;

            };

            $scope.cancel = function () {
                $window.history.back();
            };
            $scope.showUpdateBtn = false;
            $scope.updateConfirm = function () {
                $scope.checkIsFormDisable();
                $scope.showUpdateBtn = true;
            }

            $scope.itemSourceList = [
                {
                    'key': 1,
                    'value':'Purchase'
                },
                {
                    'key': 21,
                    'value': 'Foc'
                }

            ]

            $scope.shippingWayList = [
                {
                    'key': 1,
                    'value': 'Air'
                },
                {
                    'key': 2,
                    'value': 'Sea'
                },
                {
                    'key': 3,
                    'value': 'Surface'
                },
                {
                    'key': 4,
                    'value': 'By Hand'
                }

            ]


            $scope.vendorsList = [];

            $scope.getPOscheduleDetails = function () {
                var params = {
                    "ponumber": $scope.poNumber
                };
                PRMPOService.getPODetails(params)
                    .then(function (response) {
                        $scope.reqId = response[0].REQ_ID;
                        $scope.qcsId = response[0].QCS_ID;
                        $scope.prID = response[0].PR_ID;
                        $scope.prNumber = response[0].PR_NUMBER;
                        $scope.vendorsList = response;
                        if ($scope.vendorsList && $scope.vendorsList.length > 0 && ($scope.vendorsList[0].PO_STATUS == 'CLOSED' || $scope.vendorsList[0].PO_STATUS == 'PO CANCELLED')) {
                            $scope.isFormdisabled = true;
                        }
                        $scope.getRequirementData();

                    });


            }

          

            $scope.getRequirementData = function () {
                auctionsService.getReportrequirementdata({ "reqid": $scope.reqId, "sessionid": userService.getUserToken(), 'userid': userService.getUserId(), 'excludePriceCap': 1 })
                    .then(function (response) {
                        $scope.requirementDetails = response;
                        $scope.requirementDetails.auctionVendors.forEach(function (vendor, idx) {
                            vendor.PAYMENT_TERMS = vendor.payment;
                        });
                        $scope.getVendorItemAssignments();

                    });
            };


            if ($scope.poNumber == 0) {
                $scope.getRequirementData();
            } else {
                $scope.getPOscheduleDetails();
            }


             $scope.MasterPOs = [];

            $scope.GetRequirementVendorDetails = function () {
                auctionsService.getRequirementVendorDetails($scope.reqId, $scope.currentUserSessionId)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.requirementVendorDetails = response;

                            $scope.requirementDetails.auctionVendors.forEach(function (vendor) {
                                vendor.COMPANY_NAME = vendor.companyName;
                                vendor.PO_RELEASE_DATE = moment().format('DD-MM-YYYY');
                                vendor.PO_DELIVERY_DATE = '';
                                vendor.SHIP_TO_LOCATION = '';
                                vendor.BILL_TO_LOCATION = '';
                                vendor.billToLocationError = false;
                                vendor.shipToLocationError = false;
                                vendor.poDeliveryDateError = false;
                                vendor.poReleaseDateError = false;
                                vendor.gstError = false;
                                vendor.vendorGstError = false;
                                vendor.NET_PRICE = 0;
                                vendor.NET_PRICE_NO_TAX = 0;
                                vendor.DEPARTMENT = '';
                                vendor.SVC_CENTER_CODE = 'F1-SMart';
                                vendor.GST_REGISTRATION = '';
                                vendor.PO_TYPE = vendor.PAYMENT_TERMS.toLowerCase().includes('advance') ? 'Pre Payment' : 'Standard';

                                $scope.requirementVendorDetails.forEach(function (item) {
                                    if (vendor.vendorID == item.vendorID) {
                                        vendor.VENDOR_CODE = item.vendorCode;
                                        //vendor.BILL_TO_LOCATION = item.billToLocation;
                                        //vendor.SHIP_TO_LOCATION = item.additionalVendorInformation ? JSON.parse(item.additionalVendorInformation)[0].SHIP_TO_LOCATION : '';
                                        //vendor.DELIVERY_LOCATION = item.deliveryLocation;
                                        vendor.VENDOR_SITE_CODE = item.vendorSiteCode;
                                    }

                                });
                                $scope.GetCompanyGSTInfo(vendor);

                            });
                            $scope.requirementDetails.listRequirementItems.forEach(function (item) {
                                $scope.requirementDetails.auctionVendors.forEach(function (vendor, idx) {
                                    vendor.SHIP_TO_LOCATION = item.PR_DELIVERY_LOCATION;
                                    vendor.listRequirementItems.forEach(function (item1) {
                                        if (item1.itemID == item.itemID) {
                                            item1.caseId = item.caseId;
                                            item1.deliveryLocation = item.PR_DELIVERY_LOCATION;
                                        }
                                    })
                                });
                            })
                            let lineItem = 10;

                            $scope.requirementDetails.auctionVendors.forEach(function (vendor, idx) {

                                var result = vendor.listRequirementItems.filter(function (o1) {
                                    return $scope.vendorAssignmentList.some(function (o2) {
                                        return o1.itemID === o2.ITEM_ID;// return the ones with equal id
                                    });
                                });
                                vendor.listRequirementItems = result;

                                
                                vendor.listRequirementItems.forEach(function (item, idx) {
                                    item.expDeliveryDate1 = $scope.GetDateconverted(item.expDeliveryDate1);
                                    if (String(item.expDeliveryDate1).includes('9999') || String(item.expDeliveryDate1).includes('1000') || String(item.expDeliveryDate1).includes('10000')) {
                                        item.expDeliveryDate1 = '-';
                                    }

                                    if (!item.isDeleted) {
                                        item.PR_NUMBER = $scope.prNumber;
                                        $scope.vendorAssignmentList.forEach(function (qty) {
                                            if (vendor.vendorID == qty.VENDOR_ID && item.itemID == qty.ITEM_ID) {
                                                item.qtyDistributedTemp = qty.ASSIGN_QTY;
                                                item.qtyDistributed = qty.ASSIGN_QTY;
                                            }

                                        })
                                        item.totalPOPriceWithoutTax = +(((+item.qtyDistributed) * item.revUnitPrice) * vendor.vendorCurrencyFactor).toFixed(2);
                                        item.totalPOPriceWithTax = +((((+item.qtyDistributed) * item.revUnitPrice) + (((+item.qtyDistributed) * item.revUnitPrice) * ((item.sGst + item.cGst + item.iGst) / 100))) * vendor.vendorCurrencyFactor).toFixed(2);
                                        item.revUnitPriceWithTax = item.revUnitPrice + (item.revUnitPrice * ((item.sGst + item.cGst + item.iGst) / 100));
                                        item.revUnitPriceTemp = item.revUnitPrice;
                                        item.qtyDistributedTemp = item.qtyDistributed;
                                        var lineItem = lineItemNum((idx + 1) * 10);
                                        item.ITEM_OF_REQUESITION = lineItem;
                                    }
                                });
                                vendor.NET_PRICE = _.sumBy(vendor.listRequirementItems, 'totalPOPriceWithTax');
                                vendor.NET_PRICE_NO_TAX = _.sumBy(vendor.listRequirementItems, 'totalPOPriceWithoutTax');
                                vendor.VENDOR_GST_REGISTRATION = vendor.gstNumber;
                                let formRequestDetails = $scope.filterSelectionChange(vendor);
                                   $scope.MasterPOs.push(formRequestDetails);
                            });

                        }
                        $scope.getProductsList();
                    });
            };

            $scope.getVendorItemAssignments = function () {
                $scope.qcsPOVendors = [];
                $scope.vendorAssignmentList = [];
                $scope.vendorAssignmentList1 = [];

                var params =
                {
                    qcsid: $scope.qcsId,
                    reqid: $scope.reqId,
                    userid: 0,
                    sessionid: userService.getUserToken()
                };

                auctionsService.getQCSVendorItemAssignments(params)
                    .then(function (response) {
                        $scope.vendorAssignmentList = response;
                        $scope.vendorAssignmentList1 = _.uniqBy(response, v => [v.VENDOR_ID, v.ITEM_ID].join());
                        //$scope.vendorAssignmentList1 = _.uniqBy(response, x => x.VENDOR_ID);

                        //var result = $scope.requirementDetails.auctionVendors.filter(function (o1) {
                        //    return $scope.vendorAssignmentList1.some(function (o2) {
                        //        return o1.vendorID === o2.VENDOR_ID;// return the ones with equal id
                        //    });
                        //});
                        var result = [];
                        $scope.requirementDetails.auctionVendors.forEach(function (vendor, vendorIndex) {
                            if (vendor.listRequirementItems && vendor.listRequirementItems.length > 0) {
                                
                                var assignedVendors = $scope.vendorAssignmentList1.filter(function (vendorItemAssignment) {
                                    return vendorItemAssignment.VENDOR_ID === vendor.vendorID;
                                });

                                if (assignedVendors && assignedVendors.length > 0)
                                {
                                    var newObj = Object.assign({}, vendor);// removing reference
                                    newObj.listRequirementItems = [];
                                    result.push(newObj);// push vendor
                                    //result[vendorIndex].listRequirementItems = [];

                                    assignedVendors.forEach(function (assignedVendorItem, assignedVendorItemIndex) {
                                        vendor.listRequirementItems.forEach(function (vendorItem, vendorItemIndex) {
                                            if (assignedVendorItem.ITEM_ID === vendorItem.itemID) {
                                                result.forEach(function (vendItem,vendIndex) {
                                                    if (vendItem.vendorID === assignedVendorItem.VENDOR_ID) {
                                                        vendItem.listRequirementItems.push(vendorItem);// push only vendor assigned qty items
                                                    }
                                                });
                                                //result[vendorIndex].listRequirementItems.push(vendorItem);// push only vendor assigned qty items
                                            }
                                            //vendorItem.prNumber = '';
                                            //vendorItem.prID = '';
                                            //vendorItem.qcsId = '';
                                            //if ($scope.vendorsList && $scope.vendorsList.length > 0)
                                            //{
                                            //    var tempItem = _.filter($scope.vendorsList, function (poItem) { return poItem.VENDOR_ID === vendor.vendorID && +poItem.PRODUCT_ID === vendorItem.catalogueItemID; });
                                            //    if (tempItem)
                                            //    {
                                            //        vendorItem.prID = tempItem[0].PR_ID;
                                            //        vendorItem.prNumber = tempItem[0].PR_NUMBER;
                                            //        vendorItem.qcsId = tempItem[0].QCS_ID;
                                            //    }
                                            //}
                                        });
                                    });
                                }
                                
                            }
                        });

                        $scope.requirementDetails.auctionVendors = result;

                        $scope.GetRequirementVendorDetails();



                    });
            };

            
            $scope.getProductsList = function () {
                $scope.productsList = $scope.requirementDetails.listRequirementItems.map(function (el) { return el.catalogueItemID; });

                $scope.productsList.forEach(function (item) {
                    catalogService.getproductbyid($scope.currentUsercompanyId, item)
                        .then(function (response) {//siva
                            $scope.requirementDetails.auctionVendors.forEach(function (vendor, idx) {
                                vendor.listRequirementItems.forEach(function (item1) {
                                    if (item1.catalogueItemID == item) {
                                        item1.brand = response.prefferedBrand;
                                        item1.brandPartCode = response.BRAND_PART_CODE;
                                        item1.entity = response.ENTITY;
                                        item1.model = response.MODEL_NAME;
                                    }
                                })
                                vendor.showF1Values = _.filter(vendor.listRequirementItems, function (o) { return o.entity == 'F1'; }).length > 0 ? true : false;
                                
                                if (!vendor.showF1Values) {
                                    //vendor.SVC_CENTER_CODE = '';
                                }
                            });
                            $scope.requirementDetails.listRequirementItems.forEach(function (item) {
                                $scope.requirementDetails.auctionVendors.forEach(function (vendor, idx) {
                                    if (vendor.showF1Values) {
                                        vendor.SHIP_TO_LOCATION = _.filter($scope.locationMasterF1, function (o) { return o.LOCATION_CODE == item.PR_DELIVERY_LOCATION })[0].LOCATION_SITE;
                                        vendor.SVC_CENTER_CODE = item.PR_DELIVERY_LOCATION;
                                    } else {
                                        //vendor.SHIP_TO_LOCATION = _.filter($scope.locationMasterJeeves, function (o) { return o.LOCATION_CODE == item.PR_DELIVERY_LOCATION })[0].LOCATION_SITE
                                        var filteredLocations = _.filter($scope.locationMasterJeeves, function (o) {return o.LOCATION_CODE == item.PR_DELIVERY_LOCATION;});
                                        if (filteredLocations && filteredLocations.length > 0) {
                                            vendor.SHIP_TO_LOCATION = filteredLocations[0].LOCATION_SITE;
                                        }
                                        vendor.SVC_CENTER_CODE = item.PR_DELIVERY_LOCATION;
                                    }
                                });
                            })
                        });

                    $scope.getcategories(item);
                });

                if (+$scope.poNumber !== 0) {
                    $scope.vendorAssignmentList2 = [];
                    $scope.vendorAssignmentList2 = _.uniqBy($scope.vendorsList, x => x.VENDOR_ID);

                    var result = $scope.requirementDetails.auctionVendors.filter(function (o1) {
                        return $scope.vendorAssignmentList2.some(function (o2) {
                            return o1.vendorID === o2.VENDOR_ID; // return the ones with equal id
                        });
                    });
                    $scope.requirementDetails.auctionVendors = result;

                    $scope.requirementDetails.auctionVendors.forEach(function (vendor) {
                        vendor.PO_RELEASE_DATE = $scope.GetDateconverted($scope.vendorsList[0].PO_RELEASE_DATE);
                        //vendor.DELIVERY_LOCATION = $scope.vendorsList[0].CITY;
                        vendor.PO_DELIVERY_DATE = $scope.GetDateconverted($scope.vendorsList[0].DELIVERY_DATE);
                        vendor.ITEM_SOURCE = $scope.vendorsList[0].ITEM_SOURCE;
                        //vendor.EWC = $scope.vendorsList[0].EWC;
                        vendor.TAX_CATEGORY_TYPE = $scope.vendorsList[0].TAX_CATEGORY_TYPE;
                        vendor.SHIPPING_WAY = $scope.vendorsList[0].SHIPPING_WAY;
                        vendor.SVC_CENTER_CODE = $scope.vendorsList[0].SVC_CENTER_CODE;
                        vendor.GST_REGISTRATION = $scope.vendorsList[0].GST_REGISTRATION;
                        vendor.VENDOR_GST_REGISTRATION = $scope.vendorsList[0].VENDOR_GST_REGISTRATION;
                        vendor.BILL_TO_LOCATION = $scope.vendorsList[0].BILL_TO_LOCATION;
                        vendor.SHIP_TO_LOCATION = $scope.vendorsList[0].SHIP_TO_LOCATION;
                        vendor.PO_NUMBER = $scope.vendorsList[0].PO_NUMBER;
                        vendor.WF_ID = $scope.vendorsList[0].WF_ID;
                        vendor.NET_PRICE = $scope.vendorsList[0].NET_PRICE;
                        vendor.ADVANCE_AMOUNT = $scope.vendorsList[0].ADVANCE_AMOUNT_OVERALL;
                        vendor.listRequirementItems = vendor.listRequirementItems.filter(function (o1) {
                            return $scope.vendorsList.some(function (o2) {
                                return o1.deliveryLocation === o2.CITY; // return the ones with equal id
                            });
                        });
                        vendor.listRequirementItems.forEach(function (item) { //siva
                            $scope.vendorsList.forEach(function (item1) {
                                if (vendor.vendorID == item1.VENDOR_ID) {
                                    if (item.catalogueItemID == +item1.PRODUCT_ID) {
                                        if (item.caseId && item1.CASE_ID) {
                                            if (item.caseId && item.caseId.trim() == item1.CASE_ID && item.catalogueItemID == +item1.PRODUCT_ID) {
                                                item.totalPOPriceWithTax = item1.ITEM_TOTAL_PRICE;
                                                item.totalPOPriceWithoutTax = item1.ITEM_TOTAL_PRICE_NO_TAX;
                                                item.revUnitPrice = item1.UNIT_PRICE_NO_TAX;
                                                item.qtyDistributed = item1.ORDER_QTY;
                                                //item.qtyDistributedTemp1 = item1.ORDER_QTY;//qtyDistributedTemp1 for validation
                                                item.poStatus = item1.PO_STATUS;
                                                item.PO_NUMBER = $scope.vendorsList[0].PO_NUMBER;
                                                item.advanceAmount = item1.ADVANCE_AMOUNT;
                                            }
                                        } else if (item.caseId == item1.CASE_ID || item.catalogueItemID == item1.PRODUCT_ID) {
                                            item.totalPOPriceWithTax = item1.ITEM_TOTAL_PRICE;
                                            item.totalPOPriceWithoutTax = item1.ITEM_TOTAL_PRICE_NO_TAX;
                                            item.revUnitPrice = item1.UNIT_PRICE_NO_TAX;
                                            item.qtyDistributed = item1.ORDER_QTY;
                                            //item.qtyDistributedTemp1 = item1.ORDER_QTY;
                                            item.poStatus = item1.PO_STATUS;
                                            item.PO_NUMBER = $scope.vendorsList[0].PO_NUMBER;
                                        }
                                    }
                                    
                                }
                            })

                        })

                    })


                    if ($scope.vendorsList[0].WF_ID) {
                        $scope.workflowObj.workflowID = $scope.vendorsList[0].WF_ID
                    }
                    
                    $scope.getItemWorkflow($scope.vendorsList[0].WF_ID, parseInt($scope.vendorsList[0].PO_NUMBER.split('_')[1]), 'PO', '');

                }
               
            }

            $scope.getItemDetails = function () {
                var params1 = {
                    "ponumber": $scope.poNumber,
                    "moredetails": 0,
                    "forasn": false
                };
                $scope.grnDetails = [];
                PRMPOService.getPOScheduleItems(params1)
                    .then(function (response) {
                        response.forEach(function (item) {
                            //item.GRNItems.forEach(function (item1) {
                            //    $scope.grnDetails.push(item1);
                            //})

                        })
                    });

            };

            $scope.getItemDetails();

            $scope.validation = function (item) {

                if (+item.qtyDistributed > item.qtyDistributedTemp) {
                    swal("Total Qty should be Less than or equal to PO Qty", '', "error");
                    item.qtyDistributed = item.qtyDistributedTemp ;
                    return;
                }
                if (item.revUnitPrice > item.revUnitPriceTemp) {
                    swal("Rate should be Less than or equal to PO Rate", '', "error");
                    item.revUnitPrice = item.revUnitPriceTemp;
                    return;
                }

                //if ($scope.grnDetails.length > 0) {
                //    var result = _.filter($scope.grnDetails, function (o) { return o.PO_LINE_ITEM == item.ITEM_OF_REQUESITION && o.PO_NUMBER == $scope.poNumber });
                //} else {
                //    var result = [{ 'GRN_RECEIVED_QTY': 0, 'PO_ORDER_QTY': item.qtyDistributedTemp }]
                //}
               
                //if ((result[0].GRN_RECEIVED_QTY > 0 && (+item.qtyDistributed < result[0].GRN_RECEIVED_QTY)) || (+item.qtyDistributed > result[0].PO_ORDER_QTY)) {
                //    item.qtyDistributed = 0;
                //    swal("Total Qty should be greater than or equal to GRN Qty and Less than or equal to PO Qty", '', "error");
                //    return;
                //}
                
            }

            $scope.advanceQtyDistribution = function (vendor) {

                //vendor.listRequirementItems.forEach(function (item) {
                //    item.advanceAmount = vendor.ADVANCE_AMOUNT / (vendor.listRequirementItems.length);
                //})

                var x = vendor.ADVANCE_AMOUNT;

                vendor.listRequirementItems.forEach(function (item) {
                    if (x > item.totalPOPriceWithTax) {
                        item.advanceAmount = item.totalPOPriceWithoutTax;
                    } else {
                        item.advanceAmount = x;
                    }
                    x = x - item.advanceAmount;

                });

            }


            function lineItemNum(num) {
                let maxNum = 5;
                return num.toString().padStart(maxNum, "0");
            };

            $scope.filterSelectionChange = function (vendor) {
                let formDetails = {};
                formDetails.items = [];
                formDetails.INCO_TERMS1 = '';
                formDetails.INCO_TERMS2 = '';
                let currentVendorObj = vendor;

                formDetails.VENDOR_ID = currentVendorObj.vendorID;
                formDetails.PO_CURRENCY = currentVendorObj.vendorCurrency;

                let totalVendorItems = 1;
                let packageTax = 0;
                let packageValWithTax = 0;
                let packageValWithOutTax = 0;

                let freightTax = 0;
                let freightValWithTax = 0;
                let freightValWithOutTax = 0;

                let miscTax = 0;
                let miscValWithTax = 0;
                let miscValWithOutTax = 0;


                //Packing & Forwarding charges
                var packaging = _.filter(currentVendorObj.listRequirementItems, function (reqItem) {
                    return reqItem.productCode === 'Packing & Forwarding charges';
                });

                if (packaging && packaging.length > 0) {
                    packageTax = packaging[0].iGst ? packaging[0].iGst : (packaging[0].sGst + packaging[0].cGst);
                    packageValWithTax = packaging[0].itemPrice;
                    packageValWithOutTax = packaging[0].unitPrice;
                }

                //Freight charges
                var freightCharges = _.filter(currentVendorObj.listRequirementItems, function (reqItem) {
                    return reqItem.productCode === 'Freight charges';
                });

                if (freightCharges && freightCharges.length > 0) {
                    freightTax = freightCharges[0].iGst ? freightCharges[0].iGst : (freightCharges[0].sGst + freightCharges[0].cGst);
                    freightValWithTax = freightCharges[0].itemPrice;
                    freightValWithOutTax = freightCharges[0].unitPrice;
                }

                //Miscellenous
                var miscellenous = _.filter(currentVendorObj.listRequirementItems, function (reqItem) {
                    return reqItem.productCode === 'Miscellenous';
                });

                if (miscellenous && miscellenous.length > 0) {
                    miscTax = miscellenous[0].iGst ? miscellenous[0].iGst : (miscellenous[0].sGst + miscellenous[0].cGst);
                    miscValWithTax = miscellenous[0].itemPrice;
                    miscValWithOutTax = miscellenous[0].unitPrice;
                }



                //Items Populate:
                totalVendorItems = vendor.listRequirementItems.length;
                vendor.listRequirementItems.forEach(function (vendorItem, idx) {
                    if (!vendorItem.isDeleted && +vendorItem.qtyDistributed > 0) {
                        let poItemObj = {
                            MPN_CODE: '',
                            Deliver_SCHEDULE: ''
                        };
                        var itemdetailsTemp = vendorItem;
                        poItemObj.CASE_ID = itemdetailsTemp.caseId;
                        poItemObj.DELIVERY_LOCATION = itemdetailsTemp.deliveryLocation;
                        poItemObj.PRODUCT_ID = itemdetailsTemp.catalogueItemID;
                        poItemObj.PO_LINE_ITEM = itemdetailsTemp.ITEM_OF_REQUESITION;
                        poItemObj.PO_CREATOR = $scope.currentUserName;
                        poItemObj.TAX_CODE = itemdetailsTemp.TAX_CODE;
                        poItemObj.VENDOR_EXPECTED_DELIVERY_DATE = $scope.convertDate(itemdetailsTemp.expDeliveryDate1);
                        poItemObj.MAT_TYPE = itemdetailsTemp.MAT_TYPE;
                        //poItemObj.ORDER_QTY = itemdetailsTemp.productQuantity;
                        //poItemObj.RECEIVED_QTY = +itemdetailsTemp.qtyDistributed;
                        poItemObj.ORDER_QTY = +itemdetailsTemp.qtyDistributed;
                        poItemObj.ADVANCE_AMOUNT = 0;
                        poItemObj.AVAILABLE_QTY = +itemdetailsTemp.availableQty;
                        poItemObj.REQ_ITEM_ID = itemdetailsTemp.itemID;
                        poItemObj.REQ_ID = itemdetailsTemp.REQ_ID;
                        poItemObj.QCS_ID = $scope.qcsId;
                        poItemObj.UNIT_PRICE_NO_TAX = itemdetailsTemp.revUnitPrice;
                        poItemObj.UNIT_PRICE = itemdetailsTemp.revUnitPriceWithTax;
                        poItemObj.ITEM_TOTAL_PRICE_NO_TAX = itemdetailsTemp.totalPOPriceWithoutTax;
                        poItemObj.ITEM_TOTAL_PRICE = itemdetailsTemp.totalPOPriceWithTax;
                        poItemObj.TYPE_OF_STOCK = itemdetailsTemp.productDeliveryDetails;
                        poItemObj.HSN_CODE = itemdetailsTemp.hsnCode;

                        let prItemObj = _.filter($scope.requirementPRItems, function (prItem) {
                            return prItem.PRODUCT_ID === itemdetailsTemp.catalogueItemID;
                        });

                        if ($scope.requirementDetails.isDiscountQuotation === 1) {
                            poItemObj.DISCOUNT_VALUE = $scope.precisionRound(itemdetailsTemp.revUnitDiscount, 2);

                        } else {
                            poItemObj.DISCOUNT_VALUE = $scope.precisionRound(parseFloat(((itemdetailsTemp.unitPrice - itemdetailsTemp.revUnitPrice) / itemdetailsTemp.unitPrice) * 100), 2);
                        }

                        if (!poItemObj.ORDER_QTY) {
                            poItemObj.quantityError = true;
                        }

                        if (prItemObj && prItemObj.length > 0) {
                            poItemObj.PR_NUMBER = prItemObj[0].PR_NUMBER;
                            poItemObj.PR_ITEM_ID = prItemObj[0].ITEM_ID;
                            poItemObj.CATEGORY_ID = prItemObj[0].CATEGORY_ID;
                            poItemObj.PR_ID = prItemObj[0].PR_ID;
                            poItemObj.ITEM_TEXT_PO = '';
                            poItemObj.DOC_TYPE = '';
                        }

                        //Packing & Forwarding charges
                        var packaging = _.filter(currentVendorObj.listRequirementItems, function (reqItem) {
                            return reqItem.productCode === 'Packing & Forwarding charges';
                        });

                        if (packaging && packaging.length > 0) {
                            poItemObj.PACKING_CHARGES = parseFloat((packageValWithTax / totalVendorItems).toFixed(2));
                        }

                        //Freight charges
                        var freightCharges = _.filter(currentVendorObj.listRequirementItems, function (reqItem) {
                            return reqItem.productCode === 'Freight charges';
                        });

                        if (freightCharges && freightCharges.length > 0) {
                            poItemObj.FREIGHT = parseFloat((freightValWithOutTax / totalVendorItems).toFixed(2));
                        }

                        //Miscellenous
                        var miscellenous = _.filter(currentVendorObj.listRequirementItems, function (reqItem) {
                            return reqItem.productCode === 'Miscellenous';
                        });

                        if (miscellenous && miscellenous.length > 0) {
                            poItemObj.MISC_CHARGES = parseFloat((miscValWithOutTax / totalVendorItems).toFixed(2));
                        }

                        formDetails.items.push(poItemObj);

                    }
                });

                return formDetails;
            };



            $scope.qtyDistributionCal = function (vendor, item) {

                item.totalPOPriceWithoutTax = +(((+item.qtyDistributed) * item.revUnitPrice) * vendor.vendorCurrencyFactor).toFixed(2);
                item.totalPOPriceWithTax = +((((+item.qtyDistributed) * item.revUnitPrice) + (((+item.qtyDistributed) * item.revUnitPrice) * ((item.sGst + item.cGst + item.iGst) / 100))) * vendor.vendorCurrencyFactor).toFixed(2);
                 vendor.NET_PRICE = _.sumBy(vendor.listRequirementItems, 'totalPOPriceWithTax');
                vendor.NET_PRICE_NO_TAX = _.sumBy(vendor.listRequirementItems, 'totalPOPriceWithoutTax');

            }


            $scope.poList = [];
            $scope.submitPOToSAP = function (isDraft) {
                $scope.poList = [];
                $scope.allData = [];
                
                $scope.mandateError = false;

                $scope.requirementDetails.auctionVendors.forEach(function (vendor) {
                    vendor.billToLocationError = false;
                    vendor.shipToLocationError = false;
                    vendor.poDeliveryDateError = false;
                    vendor.poReleaseDateError = false;
                    vendor.advanceAmountError = false;
                    vendor.gstError = false;
                    vendor.vendorGstError = false;
                    vendor.taxcategoryTypeError = false;


                    if (!vendor.showF1Values) {
                        var billToLocationDisplay = [];
                        billToLocationDisplay = $scope.locationMasterJeeves.filter(function (item) {
                            return vendor.BILL_TO_LOCATION == item.LOCATION_SITE;
                        })
                        var shipToLocationDisplay = [];
                        shipToLocationDisplay = $scope.locationMasterJeeves.filter(function (item) {
                            return vendor.SHIP_TO_LOCATION == item.LOCATION_SITE;
                        })
                    } else {
                        var billToLocationDisplay = [];
                        billToLocationDisplay = $scope.locationMasterF1.filter(function (item) {
                            return vendor.BILL_TO_LOCATION == item.LOCATION_SITE;
                        })
                        var shipToLocationDisplay = [];
                        shipToLocationDisplay = $scope.locationMasterF1.filter(function (item) {
                            return vendor.SHIP_TO_LOCATION == item.LOCATION_SITE;
                        })
                    }

                    if (vendor.BILL_TO_LOCATION == '' || vendor.BILL_TO_LOCATION == undefined || billToLocationDisplay.length == 0) {
                        vendor.billToLocationError = true;
                        $scope.mandateError = true;
                    }
                    if (vendor.SHIP_TO_LOCATION == '' || vendor.SHIP_TO_LOCATION == undefined || shipToLocationDisplay.length == 0) {
                        vendor.shipToLocationError = true;
                        $scope.mandateError = true;
                    }
                    if (vendor.GST_REGISTRATION == '' || vendor.GST_REGISTRATION == undefined) {
                        vendor.gstError = true;
                        $scope.mandateError = true;
                    }
                    if (vendor.VENDOR_GST_REGISTRATION == '' || vendor.VENDOR_GST_REGISTRATION == undefined) {
                        vendor.vendorGstError = true;
                        $scope.mandateError = true;
                    }
                    if (vendor.PO_DELIVERY_DATE == '' || vendor.PO_DELIVERY_DATE == undefined) {
                        vendor.poDeliveryDateError = true;
                        $scope.mandateError = true;
                    }
                    if (vendor.PO_RELEASE_DATE == '' || vendor.PO_RELEASE_DATE == undefined) {
                        vendor.poReleaseDateError = true;
                        $scope.mandateError = true;
                    }

                    if (+vendor.ADVANCE_AMOUNT > vendor.NET_PRICE_NO_TAX) {
                        vendor.advanceAmountError = true;
                        $scope.mandateError = true;
                    }
                    if (vendor.TAX_CATEGORY_TYPE == '' || vendor.TAX_CATEGORY_TYPE == undefined) {
                        vendor.taxcategoryTypeError = true;
                        $scope.mandateError = true;
                    }
                })

                if ($scope.workflowObj.workflowID <= 0) {
                    $scope.errorMessage = 'Please select the workflow';
                }

                if ($scope.mandateError || $scope.workflowObj.workflowID <= 0) {
                    return;
                }

                $scope.MasterPOs.forEach(function (item1, index1) {
                    $scope.requirementDetails.auctionVendors.forEach(function (item, index) {
                        if (item1.VENDOR_ID == item.vendorID) {
                            item1.VENDOR_ID = item.vendorID;
                            item1.VENDOR_CODE = item.VENDOR_CODE;
                            item1.NET_PRICE = item.NET_PRICE;
                            item1.PO_RELEASE_DATE = item.PO_RELEASE_DATE;
                            item1.DELIVERY_DATE = item.PO_DELIVERY_DATE;
                            //item1.DELIVERY_LOCATION = item.DELIVERY_LOCATION;
                            item1.DEPARTMENT = item.DEPARTMENT;
                            if (!item.showF1Values) {
                                item1.SHIP_TO_LOCATION = _.filter($scope.locationMasterJeeves, function (o) { return o.LOCATION_SITE == item.SHIP_TO_LOCATION })[0].LOCATION_NAME
                                item1.BILL_TO_LOCATION = _.filter($scope.locationMasterJeeves, function (o) { return o.LOCATION_SITE == item.BILL_TO_LOCATION })[0].LOCATION_NAME;
                            } else {
                                item1.SHIP_TO_LOCATION = _.filter($scope.locationMasterF1, function (o) { return o.LOCATION_SITE == item.SHIP_TO_LOCATION })[0].LOCATION_NAME
                                item1.BILL_TO_LOCATION = _.filter($scope.locationMasterF1, function (o) { return o.LOCATION_SITE == item.BILL_TO_LOCATION })[0].LOCATION_NAME;
                            }
                            item1.VENDOR_SITE_CODE = item.VENDOR_SITE_CODE;
                            //item1.EWC = item.EWC;
                            item1.TAX_CATEGORY_TYPE = item.TAX_CATEGORY_TYPE;
                            item1.ITEM_SOURCE = item.ITEM_SOURCE == undefined ? 0 : +item.ITEM_SOURCE;
                            item1.SHIPPING_WAY = item.SHIPPING_WAY == undefined ? 0 : +item.SHIPPING_WAY;
                            item1.SVC_CENTER_CODE = item.SVC_CENTER_CODE;
                            item1.GST_REGISTRATION = item.GST_REGISTRATION;
                            item1.VENDOR_GST_REGISTRATION = item.VENDOR_GST_REGISTRATION;
                            item1.PAYMENT_TERMS = item.PAYMENT_TERMS;
                            item1.PO_TYPE = item.PO_TYPE;
                            item1.REQ_ID = +$scope.reqId;
                            item1.QCS_ID = +$scope.qcsId;
                            item1.CURRENCY = item.selectedVendorCurrency;
                            item1.ADVANCE_AMOUNT_OVERALL = +item.ADVANCE_AMOUNT ? +item.ADVANCE_AMOUNT : 0;
                            //+item.ADVANCE_AMOUNT > 0 ? +item.ADVANCE_AMOUNT : 0;
                            //item1.ENTITY = item.ENTITY;
                            item.listRequirementItems.forEach(function (x) {
                                item1.items.forEach(function (y) {
                                    y.ENTITY = x.entity;
                                    if (y.REQ_ITEM_ID == x.itemID) {
                                        if (x.advanceAmount > 0) {
                                            y.ADVANCE_AMOUNT = x.advanceAmount;
                                        } else {
                                            y.ADVANCE_AMOUNT = 0;
                                        }
                                        if (x.qtyDistributedTemp > 0) {
                                            y.ORDER_QTY = +x.qtyDistributed;
                                        }
                                    }
                                })
                            })
                        }

                    });
                });

                $scope.MasterPOs.forEach(function (item1, index1) {
                    item1.items.forEach(function (item, index) {
                       
                        let tempItem = angular.copy(item);
                        tempItem.VENDOR_ID = item1.VENDOR_ID;
                        tempItem.VENDOR_CODE = item1.VENDOR_CODE;
                        tempItem.NET_PRICE = item1.NET_PRICE;


                        var ts = moment(item1.PO_RELEASE_DATE, "DD-MM-YYYY").valueOf();
                        var m = moment(ts);
                        var deliveryDate = new Date(m);
                        var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                        tempItem.PO_RELEASE_DATE = "/Date(" + milliseconds + "000+0530)/";

                        var ts = moment(item1.DELIVERY_DATE, "DD-MM-YYYY").valueOf();
                        var m = moment(ts);
                        var quotationDate = new Date(m);
                        var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                        tempItem.DELIVERY_DATE = "/Date(" + milliseconds + "000+0530)/";

                        //var ts = userService.toUTCTicks(item1.PO_DELIVERY_DATE);
                        //var m = moment(ts);
                        //var quotationDate = new Date(m);
                        //var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                        //tempItem.DELIVERY_DATE = "/Date(" + milliseconds + "000+0530)/";

                        //tempItem.DELIVERY_LOCATION = item1.DELIVERY_LOCATION;
                        tempItem.DEPARTMENT = item1.DEPARTMENT;
                        tempItem.SHIP_TO_LOCATION = item1.SHIP_TO_LOCATION;
                        tempItem.BILL_TO_LOCATION = item1.BILL_TO_LOCATION;
                        tempItem.VENDOR_SITE_CODE = item1.VENDOR_SITE_CODE;
                        tempItem.ITEM_SOURCE = item1.ITEM_SOURCE == undefined ? 0 : +item1.ITEM_SOURCE;
                        tempItem.SHIPPING_WAY = item1.SHIPPING_WAY == undefined ? 0 : +item1.SHIPPING_WAY;
                        tempItem.SVC_CENTER_CODE = item1.SVC_CENTER_CODE;
                        tempItem.PAYMENT_TERMS = item1.PAYMENT_TERMS;
                        tempItem.PO_TYPE = item1.PO_TYPE;
                        //tempItem.EWC = item1.EWC;
                        tempItem.TAX_CATEGORY_TYPE = item1.TAX_CATEGORY_TYPE;
                        tempItem.GST_REGISTRATION = item1.GST_REGISTRATION;
                        tempItem.VENDOR_GST_REGISTRATION = item1.VENDOR_GST_REGISTRATION;
                        tempItem.USER = $scope.currentUserId;
                        tempItem.COMP_ID = $scope.currentUsercompanyId;
                        tempItem.WF_ID = $scope.workflowObj.workflowID;
                        tempItem.REQ_ID = +$scope.reqId;
                        tempItem.PR_ID = $scope.prID;
                        tempItem.QCS_ID = +$scope.qcsId;
                        tempItem.CURRENCY = item1.CURRENCY;
                        tempItem.ADVANCE_AMOUNT_OVERALL = item1.ADVANCE_AMOUNT_OVERALL;
                        //tempItem.ENTITY = item1.ENTITY;
                        $scope.poList.push(tempItem);
                    });
                });
                let sapDataList = [];

                //$scope.poList.forEach(function (item) {
                //    $scope.requirementDetails.auctionVendors.forEach(function (item2) {
                //        if (item.VENDOR_ID == item2.vendorID) {
                //            item.RECEIVED_QTY = _.filter(item2.listRequirementItems, { 'catalogueItemID': item.PRODUCT_ID })[0].qtyDistributed;
                //            item.RECEIVED_QTY = +item.RECEIVED_QTY;
                //            item.ITEM_TOTAL_PRICE = _.filter(item2.listRequirementItems, { 'catalogueItemID': item.PRODUCT_ID })[0].totalPOPriceWithTax;
                //            item.ITEM_TOTAL_PRICE_NO_TAX = _.filter(item2.listRequirementItems, { 'catalogueItemID': item.PRODUCT_ID })[0].totalPOPriceWithoutTax;
                //        }
                //    })
                //})

                var params = {
                    "poItems": $scope.poList,
                    "sessionID": $scope.currentUserSessionId
                };

                
                PRMPOService.GeneratePRToPO(params)
                    .then(function (response) {
                        swal("PO Generated Successfully.", '', "success");
                        $state.go('list-pendingPO');
                        if ($scope.gstArray.length > 0) {
                            $scope.updateVendorGstDetails();
                        }

                    });
            };

            $scope.updatePODetails = function () {
                let poList = [];
                
                var result = _.filter($scope.requirementDetails.auctionVendors[0].listRequirementItems, function (o) { return +o.qtyDistributed == 0 });
                if (result.length > 0) {
                    swal("Error!", 'Please enter TOTAL Qty');
                    return;
                }

                if ($scope.workflowObj.workflowID == 0) {
                    swal("Error!", 'Please enter Workflow');
                    return;
                }
                $scope.requirementDetails.auctionVendors[0].listRequirementItems.forEach(function (item) {
                    item.WF_ID = $scope.workflowObj.workflowID;
                    item.USER = $scope.currentUserId;
                    item.ITEM_TOTAL_PRICE = item.totalPOPriceWithTax;
                    item.ITEM_TOTAL_PRICE_NO_TAX = item.totalPOPriceWithoutTax;
                    item.UNIT_PRICE_NO_TAX = +item.revUnitPrice;
                    item.UNIT_PRICE =  +item.revUnitPrice + (item.revUnitPrice * ((item.sGst + item.cGst + item.iGst) / 100));
                    item.NET_PRICE = $scope.requirementDetails.auctionVendors[0].NET_PRICE;
                    //item.RECEIVED_QTY = +item.qtyDistributed;
                    item.PO_LINE_ITEM = item.ITEM_OF_REQUESITION;
                    item.ORDER_QTY = +item.qtyDistributed;
                    item.AVAILABLE_QTY = +item.availableQty;
                    item.PAYMENT_TERMS = $scope.requirementDetails.auctionVendors[0].PAYMENT_TERMS;
                    item.BILL_TO_LOCATION = $scope.requirementDetails.auctionVendors[0].BILL_TO_LOCATION;
                    item.SHIP_TO_LOCATION = $scope.requirementDetails.auctionVendors[0].SHIP_TO_LOCATION;
                    item.VENDOR_SITE_CODE = $scope.requirementDetails.auctionVendors[0].VENDOR_SITE_CODE;
                 
                    $scope.requirementDetails.auctionVendors[0].ADVANCE_AMOUNT ? +$scope.requirementDetails.auctionVendors[0].ADVANCE_AMOUNT : 0;
                    item.TAX_CATEGORY_TYPE = $scope.requirementDetails.auctionVendors[0].TAX_CATEGORY_TYPE;
                    item.GST_REGISTRATION = $scope.requirementDetails.auctionVendors[0].GST_REGISTRATION;
                    item.SVC_CENTER_CODE = $scope.requirementDetails.auctionVendors[0].SVC_CENTER_CODE;
                    //item.PO_TYPE = $scope.requirementDetails.auctionVendors[0].PO_TYPE;
                    var ts = moment($scope.requirementDetails.auctionVendors[0].PO_DELIVERY_DATE, "DD-MM-YYYY").valueOf();
                    var m = moment(ts);
                    var quotationDate = new Date(m);
                    var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                    item.DELIVERY_DATE = "/Date(" + milliseconds + "000+0530)/";
                    poList.push({
                        'WF_ID': item.WF_ID, 'USER': item.USER, 'ITEM_TOTAL_PRICE': item.ITEM_TOTAL_PRICE, 'ITEM_TOTAL_PRICE_NO_TAX': item.ITEM_TOTAL_PRICE_NO_TAX,
                        'UNIT_PRICE_NO_TAX': item.UNIT_PRICE_NO_TAX,
                        'NET_PRICE': item.NET_PRICE, 'ORDER_QTY': item.ORDER_QTY, 'PO_LINE_ITEM': item.PO_LINE_ITEM, 'PO_NUMBER': item.PO_NUMBER, 'AVAILABLE_QTY': item.AVAILABLE_QTY,
                        'PAYMENT_TERMS': item.PAYMENT_TERMS, 'BILL_TO_LOCATION': item.BILL_TO_LOCATION, 'SHIP_TO_LOCATION': item.SHIP_TO_LOCATION, 'VENDOR_SITE_CODE': item.VENDOR_SITE_CODE,
                        'ADVANCE_AMOUNT': item.ADVANCE_AMOUNT, 'TAX_CATEGORY_TYPE': item.TAX_CATEGORY_TYPE, 'GST_REGISTRATION': item.GST_REGISTRATION,
                        'SVC_CENTER_CODE': item.SVC_CENTER_CODE, 'DELIVERY_DATE': item.DELIVERY_DATE, 'UNIT_PRICE': item.UNIT_PRICE
                    });

                })
                var params = {
                    "poItems": poList,
                    "sessionID": $scope.currentUserSessionId
                };

                PRMPOService.UpdatePODetails(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            swal({
                                title: "Error!",
                                text: response.errorMessage,
                                type: "error",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            },
                                function () {
                                    location.reload();
                                });
                        } else {
                            swal("Updated Successfully.", '', "success");
                            $state.go('list-pendingPO');
                        }

                    });
            }

            $scope.convertDate = function (date) {
                if (date) {
                    var ts = moment(date, "DD-MM-YYYY").valueOf();
                    var m = moment(ts);
                    var quotationDate = new Date(m);
                    var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                    return "/Date(" + milliseconds + "000+0530)/";
                } else {
                    return null;
                }
            };


            //$scope.detectLinks = function urlify(text) {
            //    if (text) {
            //        var urlRegex = /(https?:\/\/[^\s]+)/g;
            //        return text.replace(urlRegex, function (url) {
            //            return url;//'<a target="_blank" href="' + url + '">' + url + '</a>';
            //        });
            //    }
            //};
            ///*region start WORKFLOW*/

            $scope.getWorkflows = function () {
                workflowService.getWorkflowList()
                    .then(function (response) {
                        $scope.workflowList = [];
                        $scope.workflowListDeptWise = [];
                        $scope.workflowListTemp = response;
                        $scope.workflowListTemp.forEach(function (item, index) {
                            if (item.WorkflowModule == $scope.WorkflowModule) {
                                $scope.workflowList.push(item);
                                $scope.workflowListDeptWise.push(item);
                            }
                        });

                        if (userService.getUserObj().isSuperUser) {
                            $scope.workflowList = $scope.workflowList;
                        }
                        else {
                            $scope.workflowList = [];
                            $scope.workflowListDeptWise.forEach(function (wf, idx) {
                                $scope.deptIDs.forEach(function (dep) {
                                    if (dep == wf.deptID) {
                                        $scope.workflowList.push(wf);
                                    }
                                });
                            });
                        }
                    });
            };

            $scope.getWorkflows();

            $scope.getItemWorkflow = function (wfID, prID, type, itemID) {
                workflowService.getItemWorkflow(wfID, prID, type)
                    .then(function (response) {
                        $scope.itemWorkflow = response;
                        $scope.checkIsFormDisable();
                        if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                            $scope.currentStep = 0;

                            var count = 0;

                            $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {
                                if (!track.multipleAttachments) {
                                    track.multipleAttachments = [];
                                }

                                if (track.status === 'APPROVED') { track.statusNew = 'Approved'; }
                                if (track.status === 'HOLD') { track.statusNew = 'Hold'; }
                                if (track.status === 'PENDING') { track.statusNew = 'Pending'; }
                                if (track.status === 'REJECTED') { track.statusNew = 'Rejected'; }

                                //if (track.status === 'APPROVED' || track.status === 'HOLD') {
                                //    $scope.isFormdisabled = true;
                                //}

                                if (track.status === 'APPROVED') {
                                    $scope.isWorkflowCompleted = true;
                                    $scope.orderInfo = track.order;
                                    $scope.assignToShow = track.status;

                                }
                                else {
                                    $scope.isWorkflowCompleted = false;
                                }

                                if (track.status === 'REJECTED' && count == 0) {
                                    count = count + 1;
                                }

                                if ((track.status === 'PENDING' || track.status === 'HOLD') && count == 0) {
                                    count = count + 1;
                                    $scope.IsUserApproverForStage(track.approverID);
                                    $scope.currentAccess = track.order;
                                }

                                if ((track.status === 'PENDING' || track.status === 'HOLD' || track.status === 'REJECTED') && $scope.currentStep === 0) {
                                    $scope.currentStep = track.order;
                                    return false;
                                }
                            });
                        }
                    });
            };

            $scope.updateTrack = function (step, status) {

                if (!$rootScope.isUserEntitled(591164)) {
                    swal("Error!", "Workflow Approval Denied", "error");
                    $state.go('home');
                    return;
                }

                $scope.disableAssignPR = true;
                $scope.commentsError = '';
                step.isLastApprover = false;
                if (step.comments != null || step.comments != "" || step.comments != undefined) {
                    step.comments = validateStringWithoutSpecialCharacters(step.comments);
                }
                var tempArray = $scope.itemWorkflow[0].WorkflowTracks[$scope.itemWorkflow[0].WorkflowTracks.length - 1];
                if (step.order == tempArray.order && status === 'APPROVED') {
                    $scope.disableAssignPR = false;
                    step.isLastApprover = true;
                } else {
                    $scope.disableAssignPR = true;
                }

                if ($scope.isReject) {
                    $scope.commentsError = 'Please Save Rejected Items/Qty';
                    return false;
                }

                if (status === 'REJECTED' && (step.comments == null || step.comments == "")) {
                    $scope.commentsError = 'Please enter comments';
                    return false;
                }

                step.status = status;
                step.sessionID = $scope.currentUserSessionId;
                step.modifiedBy = userService.getUserId();

                step.moduleName = $scope.WorkflowModule;
                step.indNo = $scope.poNumber;

                //step.subModuleName = $scope.requirementDetails.title;
                //step.subModuleID = $scope.reqId;

                workflowService.SaveWorkflowTrack(step)
                    .then(function (response) {
                        if (response.errorMessage) {
                            swal({
                                title: "Error!",
                                text: response.errorMessage,
                                type: "error",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            },
                                function () {
                                    location.reload();
                                });
                        }
                        else {
                            $scope.getItemWorkflow();
                            location.reload();
                            //$state.go('approval-qcs-list');
                        }
                    });
            };

            $scope.assignWorkflow = function (moduleID) {
                workflowService.assignWorkflow(({ wID: $scope.workflowObj.workflowID, moduleID: moduleID, user: userService.getUserId(), sessionID: $scope.sessionID }))
                    .then(function (response) {
                        if (response.errorMessage) {
                            growlService.growl(response.errorMessage, "inverse");
                            $scope.isSaveDisable = false;
                        }
                        else {
                            //  $state.go('list-pr');
                        }
                    });
            };

            $scope.IsUserApprover = false;

            $scope.functionResponse = false;

            $scope.IsUserApproverForStage = function (approverID) {
                workflowService.IsUserApproverForStage(approverID, userService.getUserId())
                    .then(function (response) {
                        $scope.IsUserApprover = response;
                    });
            };

            $scope.isApproverDisable = function (index) {

                var disable = true;

                var previousStep = {};

                $scope.itemWorkflow[0].WorkflowTracks.forEach(function (step, stepIndex) {

                    if (index == stepIndex) {
                        if (stepIndex == 0) {
                            if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                                (step.status === 'PENDING' || step.status === 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                        else if (stepIndex > 0) {
                            if (previousStep.status === 'PENDING' || previousStep.status === 'HOLD' || previousStep.status === 'REJECTED') {
                                disable = true;
                            }
                            else if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                                (step.status === 'PENDING' || step.status === 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                    }
                    previousStep = step;
                });

                return disable;
            };

            /*region end WORKFLOW*/

            $scope.isUserBelongsToDeptandDesig = function (deptID, desigID) {
                var isEligible = true;

                if ($scope.deptIDs.indexOf(deptID) != -1 && $scope.desigIDs.indexOf(desigID) != -1) {
                    isEligible = true;
                } else {
                    isEligible = false;
                }

                return isEligible;
            };

            function validateStringWithoutSpecialCharacters(string) {
                if (string) {
                    string = string.replace(/\'/gi, "");
                    string = string.replace(/\"/gi, "");
                    string = string.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                    string = string.replace(/(\r\n|\n|\r)/gm, "");
                    string = string.replace(/\t/g, '');
                    return string;
                }
            };

            $scope.GetDateconverted = function (dateBefore) {
                if (dateBefore) {
                    return new moment(dateBefore).format("DD-MM-YYYY");
                }
            };

            $scope.gstMasterF1 = [];
            $scope.gstMasterJeeves = [];
            auctionsService.GetCompanyConfiguration(0, $scope.companyConfigStr, userService.getUserToken())
                .then(function (unitResponse) {

                    $scope.companyConfigList = unitResponse;
                    var countriesTemp = [];
                    $scope.companyConfigList.forEach(function (item, index) {
                        if (item.configKey == 'GST_DETAILS_F1') {
                            $scope.gstMasterF1.push(item);
                        } else if (item.configKey == 'GST_DETAILS_JEEVES') {
                            $scope.gstMasterJeeves.push(item);
                        } 
                    });

                });

            $scope.locationList = [];
            $scope.locationMasterF1 = [];
            $scope.locationMasterJeeves = [];
            $scope.GetOracleConfiguration = function () {

                $scope.tableColumns = [];
                $scope.tableValues = [];
                $scope.filteredArray = [];
                var params = {
                    "companyId": +$scope.currentUsercompanyId,
                    "type": 'LOCATION',
                    "sessionid": userService.getUserToken()
                };
                auctionsService.getOracleConfiguration(params)
                    .then(function (response) {
                        if (response) {
                            var arr = JSON.parse(response).Table;
                            $scope.locationList = arr;
                            $scope.locationList.forEach(function (item, index) {
                                var digit = item.LOCATION_CODE.toString()[0];

                                if (digit == '7') {
                                    $scope.locationMasterF1.push(item);
                                } else if (digit == '9') {
                                    $scope.locationMasterJeeves.push(item);
                                }
                            });
                            }
                        
                    });
            };
            $scope.GetOracleConfiguration();


            $scope.getcategories = function (item) {
                catalogService.GetProductSubCategories(item, 0, $scope.currentUsercompanyId)
                    .then(function (response) {
                        $scope.requirementDetails.auctionVendors.forEach(function (vendor, idx) {
                            vendor.listRequirementItems.forEach(function (item1) {
                                if (item1.catalogueItemID == item) {
                                    item1.category = _.filter(response, function (o) { return o.nodeChecked == true; })[0].catName;
                                }
                            })
                            //$scope.GetCompanyGSTInfo(vendor);
                        });
                    });
            };


            $scope.billLocationDisplay = [];
            $scope.shipLocationDisplay = [];
            $scope.locationSearch = function (value, vendor, type, name) {

                $scope.billLocationDisplay = [];
                $scope.shipLocationDisplay = [];
                
                if (value == '' || value == undefined) {
                    if (name == 'BILL_TO_LOCATION') {
                        vendor.showSelectedBillLocation = false;
                    } else {
                        vendor.showSelectedShipLocation = false;
                    }

                } else {
                    if (name == 'BILL_TO_LOCATION') {
                        vendor.showSelectedBillLocation = true;
                        if (!type) {
                            $scope.billLocationDisplay = $scope.locationMasterJeeves.filter(function (item) {
                                return (String(item.LOCATION_SITE.toLowerCase()).includes(value.toLowerCase()) == true);
                            })
                        } else {
                            $scope.billLocationDisplay = $scope.locationMasterF1.filter(function (item) {
                                return (String(item.LOCATION_SITE.toLowerCase()).includes(value.toLowerCase()) == true);
                            })
                        }
                    } else {
                        vendor.showSelectedShipLocation = true;
                        if (!type) {
                            $scope.shipLocationDisplay = $scope.locationMasterJeeves.filter(function (item) {
                                return (String(item.LOCATION_SITE.toLowerCase()).includes(value.toLowerCase()) == true);
                            })
                        } else {
                            $scope.shipLocationDisplay = $scope.locationMasterF1.filter(function (item) {
                                return (String(item.LOCATION_SITE.toLowerCase()).includes(value.toLowerCase()) == true);
                            })
                        }
                    }
                    
                    
                }
            }

            $scope.fillValue = function (value, vendor, type) {
                //vendor.SVC_CENTER_CODE = '';
                if (type == 'BILL_TO_LOCATION') {
                    vendor.BILL_TO_LOCATION = value;
                    vendor.showSelectedBillLocation = false;

                } else {
                    vendor.SHIP_TO_LOCATION = value;
                    vendor.showSelectedShipLocation = false;
                    var value = $scope.shipLocationDisplay.filter(function (item) {
                        return vendor.SHIP_TO_LOCATION == item.LOCATION_SITE;
                    });
                    vendor.SVC_CENTER_CODE = value[0].LOCATION_CODE;
                }
            }
            $scope.GetCompanyGSTInfo = function (vendor) {
                let params = {
                    companyId: vendor.compID,
                    sessionid: userService.getUserToken()
                };
                auctionsService.getCompanyGSTInfo(params)
                    .then(function (response) {
                        vendor.companyGSTInfo = _.uniqBy(response, 'gstNumber')

                    });
            };

            $scope.gstArray = [];
            $scope.gstArrayFinal = [];
            $scope.gstSelect = function (vendor) {
                if (_.find($scope.gstArray, ['VENDOR_ID', +vendor.vendorID]) != undefined) {
                    var test = _.remove($scope.gstArray, function (n) {
                        return n.VENDOR_ID == +vendor.vendorID;
                    });

                }
                if (vendor.VENDOR_GST_REGISTRATION.slice(0, 2) == vendor.GST_REGISTRATION.slice(0, 2)) {
                    vendor.listRequirementItems.forEach(function (item) {
                        
                        if (item.iGst > 0) {
                            $scope.gstArray.push({ 'REQ_ID': +$scope.reqId, 'VENDOR_ID': +vendor.vendorID, 'VENDOR_GST': vendor.VENDOR_GST_REGISTRATION, 'ITEM_ID': +item.itemID, 'CGST': (item.iGst / 2), 'SGST': (item.iGst / 2), 'IGST': 0 })
                        } 
                    })
                   
                } else {
                    vendor.listRequirementItems.forEach(function (item) {
                        if (item.iGst == 0) {
                            $scope.gstArray.push({ 'REQ_ID': +$scope.reqId, 'VENDOR_ID': +vendor.vendorID, 'VENDOR_GST': vendor.VENDOR_GST_REGISTRATION, 'ITEM_ID': +item.itemID, 'CGST': 0, 'SGST': 0, 'IGST': (item.cGst + item.sGst) })
                        }
                    })
                }
               

            };

            $scope.updateVendorGstDetails = function () {
                $scope.gstArrayFinal = _.uniq($scope.gstArray, function (obj) {
                    return obj.VENDOR_ID && obj.ITEM_ID;
                });

                var params = {
                    "gstArray": $scope.gstArrayFinal,
                    "sessionID": $scope.currentUserSessionId
                };

                PRMPOService.UpdateVendorGstDetails(params)
                    .then(function (response) {
                    });

            };


            $scope.rejectPODetails = function (status)
            {
                var params =
                {
                    "poNumber": $scope.poNumber,
                    "status": status,
                    "sessionId": userService.getUserToken(),
                };
                
                PRMPOService.rejectPODetails(params)
                    .then(function (response) {
                        if (response)
                        {
                            try
                            {
                                var arr = JSON.parse(response).Table;
                                if (arr && arr.length > 0) {
                                    var obj = arr[0];
                                    if (status == "CLOSED") {
                                        let url = "";
                                        if (obj.QCS_TYPE === 'DOMESTIC') {
                                            url = $state.href("cost-comparisions-qcs", { "reqID": obj.REQ_ID, "qcsID": obj.QCS_ID });
                                        } else {
                                            url = $state.href("import-qcs", { "reqID": obj.REQ_ID, "qcsID": obj.QCS_ID });
                                        }
                                        window.open(url, '_self');
                                    } else if (status == "PO CANCELLED") {
                                        $state.go('list-pendingPO');
                                    }

                                }
                            } catch (err)
                            {
                                //swal("Error!", response, "error");
                                swal({
                                    title: "Error!",
                                    text: response,
                                    type: "error",
                                    showCancelButton: false,
                                    confirmButtonColor: "#DD6B55",
                                    confirmButtonText: "Ok",
                                    closeOnConfirm: true
                                },
                                    function () {
                                        location.reload();
                                    });
                            }
                        }
                    });
            };
            $scope.showRejectBtn = false;

            $scope.checkIsFormDisable = function () {
                $scope.isFormdisabled = true;
                if ($scope.itemWorkflow.length == 0) {
                    $scope.isFormdisabled = true;
                } else {
                    if (($scope.vendorsList[0].CREATED_BY == +userService.getUserId() || $scope.vendorsList[0].MODIFIED_BY == +userService.getUserId() || ($scope.isSuperUser && $scope.isCustomer)) &&
                        $scope.itemWorkflow[0].WorkflowTracks.length > 0 && $scope.itemWorkflow[0].workflowID > 0 && ($scope.vendorsList[0].PO_STATUS != 'CLOSED' 
                        && $scope.vendorsList[0].PO_STATUS != 'PO CANCELLED')) {
                        const lastWorkflowTrack = $scope.itemWorkflow[0].WorkflowTracks[$scope.itemWorkflow[0].WorkflowTracks.length - 1];
                        if ($scope.itemWorkflow[0].WorkflowTracks[0].order == 1 && $scope.itemWorkflow[0].WorkflowTracks[0].status == "REJECTED") {
                            $scope.isFormdisabled = false;
                            $scope.showRejectBtn = true;
                        } else if (lastWorkflowTrack.status == "APPROVED") {
                            $scope.isFormdisabled = false;
                            $scope.showRejectBtn = false;
                        }
                        if ($scope.vendorsList[0].PREV_APPROVED ==1) {
                            $scope.showRejectBtn = false;
                        }
                    }
                }
            };

            $scope.routeBack = function () {
                window.history.back();
            }

        }]);