﻿prmApp
    .controller('poDomesticZSDMRevCtrl1', ["$state", "$stateParams", "$scope", "$rootScope", "auctionsService", "userService", "$http", "$window", "domain",
        "fileReader", "growlService", "$log", "$filter", "ngDialog", "catalogService", "PRMPRServices", "PRMCustomFieldService", "$modal", "PRMPOServices", "reportingService", "PRMPOService", "$q",
        function ($state, $stateParams, $scope, $rootScope, auctionsService, userService, $http, $window, domain,
            fileReader, growlService, $log, $filter, ngDialog, catalogService, PRMPRServices, PRMCustomFieldService, $modal, PRMPOServices, reportingService, PRMPOService, $q) {

            $scope.currentUserId = +userService.getUserId();
            $scope.isCustomer = $scope.customerType === "CUSTOMER" ? true : false;
            $scope.currentUserSessionId = userService.getUserToken();
            $scope.currentUsercompanyId = userService.getUserCompanyId();
            $scope.currentUserName = userService.getFirstname() + ' ' + userService.getLastname();
            $scope.currentUserSapId = userService.getSAPUserId();
            $scope.qcsId = $stateParams.qcsId;
            $scope.reqId = $stateParams.reqId;
            $scope.prNumber = $stateParams.prNumber ? $stateParams.prNumber : '';
            $scope.poQuotId = $stateParams.quotId;
            $scope.vendorAssignments = $stateParams.detailsObj;
            $scope.poRAWJSON = $stateParams.poRawJSON;
            let poRoundDigits = 2;
            $scope.POServiceStatusText = '';
            $scope.documentType = $stateParams.templateName;
            $scope.selectedVendors = [];
            $scope.currentUIVendor = {};
            $scope.currentUIPlant = {};
            $scope.formDetails = {};
            $scope.formDetails.items = [];
            $scope.reqDepartments = [];
            $scope.requirementPRItems = [];
            $scope.requirementPOItems = [];
            $scope.requirementVendorCodeInfo = [];
            $scope.qcsRequirementDetails = {};

            $scope.formDetails.MEMORY_TYPE = 'X';
            $scope.formDetails.PR_NUMBER = '';
            $scope.formDetails.PO_CREATOR = $scope.currentUserSapId;//$scope.requirementDetails && $scope.requirementDetails.prCreator ? $scope.requirementDetails.prCreator : '';
            $scope.formDetails.INCO_TERMS1 = '';
            $scope.formDetails.INCO_TERMS2 = '';
            $scope.formDetails.DOC_TYPE = $scope.documentType;
            $scope.requirementPlants = [];
            $scope.quantityError = false;
            var SelectedVendorDetails;            
            $scope.companyConfigList = [];
            $scope.COMPANY_INCO_TERMS = [];
            $scope.MasterPOs = [];
            auctionsService.GetCompanyConfiguration($scope.currentUsercompanyId, 'INCO', userService.getUserToken())
                .then(function (unitResponse) {
                    $scope.companyConfigList = unitResponse;
                    $scope.companyConfigList.forEach(function (item, index) {
                        if (item.configKey === 'INCO') {
                            $scope.COMPANY_INCO_TERMS.push(item);
                        }
                    });
                });

            $scope.precisionRound = function (number) {
                var factor = Math.pow(10, poRoundDigits);
                return Math.round(number * factor) / factor;
            };

            $scope.addItem = function () {
                $scope.formDetails.items.push({
                    ITEM_OF_REQUESITION: '',
                    isDeleted: false
                });
            };

            $scope.deleteItem = function (item) {
                item.isDeleted = true;
            };

            $scope.pageNo = 1;
            $scope.nextpage = function (pageNo) {
                $scope.pageNo = $scope.pageNo + 1;
            };

            $scope.previouspage = function () {
                $scope.pageNo = $scope.pageNo - 1;

            };

            $scope.cancel = function () {
                $window.history.back();
            };

            $scope.GetQCSDetails = function () {
                var params = {
                    "qcsid": $scope.qcsId,
                    "sessionid": userService.getUserToken()
                };

                reportingService.GetQCSDetails(params)
                    .then(function (response) {
                        if (response.REQ_JSON) {
                            $scope.qcsRequirementDetails = JSON.parse(response.REQ_JSON);
                            $scope.requirementDetails = JSON.parse(response.REQ_JSON);
                            console.log($scope.qcsRequirementDetails);
                            $scope.MasterPOs = [];                            
                            $scope.qcsRequirementDetails.auctionVendors.forEach(function (vendor, idx) {
                                $scope.formDetails.items = [];
                                let lineItem = 10;
                                vendor.listRequirementItems.forEach(function (item, idx) {
                                    if (!item.isDeleted) {
                                        item.PR_NUMBER = $scope.prNumber;
                                        item.totalPOPriceWithoutTax = (+item.qtyDistributed) * item.revUnitPrice;
                                        item.totalPOPriceWithTax = item.totalPOPriceWithoutTax + (item.totalPOPriceWithoutTax * ((item.sGst + item.cGst + item.iGst) / 100));
                                        item.revUnitPriceWithTax = item.revUnitPrice + (item.revUnitPrice * ((item.sGst + item.cGst + item.iGst) / 100));
                                        if (lineItem < 10) {
                                            item.ITEM_OF_REQUESITION = ('000' + lineItem.toString());
                                        }
                                        else if (lineItem < 100) {
                                            item.ITEM_OF_REQUESITION = ('00' + lineItem.toString());
                                        }
                                        else if (n < 1000) {
                                            item.ITEM_OF_REQUESITION = ('0' + lineItem.toString());
                                        }
                                        else {
                                            item.ITEM_OF_REQUESITION = (lineItem.toString());
                                        }

                                        lineItem = lineItem + 10;
                                    }
                                });

                                let formRequestDetails = $scope.filterSelectionChange(vendor);
                                $scope.MasterPOs.push(formRequestDetails);
                            });
                        }
                    });
            };

            $scope.filterSelectionChange = function (vendor) {
                let formDetails = {};
                formDetails.items = [];                
                formDetails.INCO_TERMS1 = '';
                formDetails.INCO_TERMS2 = '';
                let currentVendorObj = vendor;

                formDetails.VENDOR_ID = currentVendorObj.vendorID;
                formDetails.PO_CURRENCY = currentVendorObj.vendorCurrency;

                let totalVendorItems = 1;
                let packageTax = 0;
                let packageValWithTax = 0;
                let packageValWithOutTax = 0;

                let freightTax = 0;
                let freightValWithTax = 0;
                let freightValWithOutTax = 0;

                let miscTax = 0;
                let miscValWithTax = 0;
                let miscValWithOutTax = 0;


                //Packing & Forwarding charges
                var packaging = _.filter(currentVendorObj.listRequirementItems, function (reqItem) {
                    return reqItem.productCode === 'Packing & Forwarding charges';
                });

                if (packaging && packaging.length > 0) {
                    packageTax = packaging[0].iGst ? packaging[0].iGst : (packaging[0].sGst + packaging[0].cGst);
                    packageValWithTax = packaging[0].itemPrice;
                    packageValWithOutTax = packaging[0].unitPrice;
                }

                //Freight charges
                var freightCharges = _.filter(currentVendorObj.listRequirementItems, function (reqItem) {
                    return reqItem.productCode === 'Freight charges';
                });

                if (freightCharges && freightCharges.length > 0) {
                    freightTax = freightCharges[0].iGst ? freightCharges[0].iGst : (freightCharges[0].sGst + freightCharges[0].cGst);
                    freightValWithTax = freightCharges[0].itemPrice;
                    freightValWithOutTax = freightCharges[0].unitPrice;
                }

                //Miscellenous
                var miscellenous = _.filter(currentVendorObj.listRequirementItems, function (reqItem) {
                    return reqItem.productCode === 'Miscellenous';
                });

                if (miscellenous && miscellenous.length > 0) {
                    miscTax = miscellenous[0].iGst ? miscellenous[0].iGst : (miscellenous[0].sGst + miscellenous[0].cGst);
                    miscValWithTax = miscellenous[0].itemPrice;
                    miscValWithOutTax = miscellenous[0].unitPrice;
                }

                

                //Items Populate:
                totalVendorItems = vendor.listRequirementItems.length;
                vendor.listRequirementItems.forEach(function (vendorItem, idx) {
                    if (!vendorItem.isDeleted && +vendorItem.qtyDistributed > 0) {
                        let poItemObj = {
                            MPN_CODE: '',
                            Deliver_SCHEDULE: ''
                        };
                        var itemdetailsTemp = vendorItem;

                        poItemObj.PRODUCT_ID = itemdetailsTemp.catalogueItemID;
                        poItemObj.PO_LINE_ITEM = itemdetailsTemp.ITEM_OF_REQUESITION;
                        poItemObj.PO_CREATOR = $scope.currentUserName;
                        poItemObj.TAX_CODE = itemdetailsTemp.TAX_CODE;
                        poItemObj.DELIVERY_DATE = itemdetailsTemp.Deliver_SCHEDULE;
                        poItemObj.MAT_TYPE = itemdetailsTemp.MAT_TYPE;
                        poItemObj.CITY = '';
                        poItemObj.ORDER_QTY = itemdetailsTemp.qtyDistributed;
                        poItemObj.REMAINING_QTY = +itemdetailsTemp.qtyDistributed;
                        poItemObj.REQ_ITEM_ID = itemdetailsTemp.itemID;
                        poItemObj.REQ_ID = itemdetailsTemp.REQ_ID;

                        poItemObj.UNIT_PRICE_NO_TAX = itemdetailsTemp.revUnitPrice;
                        poItemObj.UNIT_PRICE = itemdetailsTemp.revUnitPriceWithTax;
                        poItemObj.ITEM_TOTAL_PRICE_NO_TAX = itemdetailsTemp.totalPOPriceWithoutTax;
                        poItemObj.ITEM_TOTAL_PRICE = itemdetailsTemp.totalPOPriceWithTax;

                        let prItemObj = _.filter($scope.requirementPRItems, function (prItem) {
                            return prItem.PRODUCT_ID === itemdetailsTemp.catalogueItemID;
                        });

                        if ($scope.requirementDetails.isDiscountQuotation === 1) {
                            poItemObj.DISCOUNT_VALUE = $scope.precisionRound(itemdetailsTemp.revUnitDiscount, 2);

                        } else {
                            poItemObj.DISCOUNT_VALUE = $scope.precisionRound(parseFloat(((itemdetailsTemp.unitPrice - itemdetailsTemp.revUnitPrice) / itemdetailsTemp.unitPrice) * 100), 2);
                        }

                        if (!poItemObj.ORDER_QTY) {
                            poItemObj.quantityError = true;
                        }

                        if (prItemObj && prItemObj.length > 0) {
                            poItemObj.PR_NUMBER = prItemObj[0].PR_NUMBER;
                            poItemObj.PR_ITEM_ID = prItemObj[0].ITEM_ID;
                            poItemObj.CATEGORY_ID = prItemObj[0].CATEGORY_ID;
                            poItemObj.PR_ID = prItemObj[0].PR_ID;
                            poItemObj.ITEM_TEXT_PO = '';
                            poItemObj.DOC_TYPE = '';                            
                        }

                        //Packing & Forwarding charges
                        var packaging = _.filter(currentVendorObj.listRequirementItems, function (reqItem) {
                            return reqItem.productCode === 'Packing & Forwarding charges';
                        });

                        if (packaging && packaging.length > 0) {
                            poItemObj.PACKING_CHARGES = parseFloat((packageValWithTax / totalVendorItems).toFixed(2));
                        }

                        //Freight charges
                        var freightCharges = _.filter(currentVendorObj.listRequirementItems, function (reqItem) {
                            return reqItem.productCode === 'Freight charges';
                        });

                        if (freightCharges && freightCharges.length > 0) {
                            poItemObj.FREIGHT = parseFloat((freightValWithOutTax / totalVendorItems).toFixed(2));                            
                        }

                        //Miscellenous
                        var miscellenous = _.filter(currentVendorObj.listRequirementItems, function (reqItem) {
                            return reqItem.productCode === 'Miscellenous';
                        });

                        if (miscellenous && miscellenous.length > 0) {
                            poItemObj.MISC_CHARGES = parseFloat((miscValWithOutTax / totalVendorItems).toFixed(2));
                        }


                        //userService.getProfileDetails({ "userid": formDetails.VENDOR_ID, "sessionid": $scope.currentUserSessionId })
                        //    .then(function (response) {
                        //        if (response) {
                        //            if (response) {
                        //                formDetails.PAYMENT_TERMS = response.paymentTermDesc;
                        //                formDetails.PAYMENT_TERMS_HIDE = response.paymentTermCode;
                        //                formDetails.INCO_TERMS1 = response.incoTerm;

                        //                $scope.COMPANY_INCO_TERMS.forEach(function (term, idx) {
                        //                    if (term.configValue == formDetails.INCO_TERMS1) {
                        //                        formDetails.INCO_TERMS2 = term.configText;
                        //                    }
                        //                });
                        //            }
                        //        }
                        //    });


                        if (poItemObj.PR_NUMBER) {
                            formDetails.items.push(poItemObj);
                        }
                    }
                });

                return formDetails;
            };

            $scope.GetPRItemsByRequirementId = function () {
                $scope.formDetails.PURCHASE_GROUP_NAMES = '';
                $scope.formDetails.PURCHASE_GROUP_CODE = '';

                PRMPRServices.GetPRItemsByReqId({ reqid: $scope.reqId })
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.requirementPRItems = response;                            
                        }

                        $scope.GetRequirementVendorCodes();
                    });
            };

            $scope.GetPRItemsByRequirementId();

            $scope.GetRequirementPO = function () {
                PRMPOServices.getRequirementPO({ compid: $scope.currentUsercompanyId, reqid: $scope.reqId })
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.requirementPOItems = response;
                        }
                    });
            };

            $scope.GetRequirementPO();

            $scope.GetRequirementVendorCodes = function () {
                auctionsService.getRequirementVendorCodes($scope.reqId, $scope.currentUserSessionId)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.requirementVendorCodeInfo = response;                           
                        }

                        if ($scope.qcsId > 0) {
                            $scope.GetQCSDetails();
                        }
                    });
            };

            $scope.submitPOToSAP = function (isDraft) {
                let sapDataList = [];
                $scope.POServiceStatusText = '';
                if ($scope.MasterPOs && $scope.MasterPOs.length > 0) {
                    $scope.MasterPOs.forEach(function (formDetails, formIndex) {
                        let poDetailsPromises = [];
                        if (formDetails && formDetails.items && formDetails.items.length > 0) {
                            var params = {
                                "details": formDetails,
                                "items": formDetails.items,
                                "compid": $scope.currentUsercompanyId,
                                "user": $scope.currentUserId,
                                "sessionid": $scope.currentUserSessionId
                            };

                            let poPromise = PRMPOService.SavePOScheduleDetails(params);
                            poDetailsPromises.push(poPromise);
                            $q.all(poDetailsPromises).then(function (responses) {
                                if (responses && responses.length > 0) {
                                    let hasErrors = false;
                                    responses.forEach(function (response, resIndex) {
                                        if (!response.errorMessage) {
                                            hasErrors = true;
                                        }
                                    });

                                    if (hasErrors) {
                                        swal({
                                            title: "Thanks !",
                                            text: "Successfully saved.",
                                            type: "success",
                                            showCancelButton: false,
                                            confirmButtonColor: "#F44336",
                                            confirmButtonText: "OK",
                                            closeOnConfirm: true
                                        }, function (isConfirm) {
                                            $state.go('list-pendingPO');
                                        });
                                    } else {
                                        swal("Error!", "Error occured.", "error");
                                    }
                                }
                            });
                        }
                    });
                }
            };

            $scope.detectLinks = function urlify(text) {
                if (text) {
                    var urlRegex = /(https?:\/\/[^\s]+)/g;
                    return text.replace(urlRegex, function (url) {
                        return url;//'<a target="_blank" href="' + url + '">' + url + '</a>';
                    });
                }
            };

        }]);