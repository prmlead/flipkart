﻿prmApp
    .controller('poDomesticZSDMRevFinalCtrl', ["$state", "$stateParams", "$scope", "$rootScope", "auctionsService", "userService", "$http", "$window", "domain",
        "fileReader", "growlService", "$log", "$filter", "ngDialog", "catalogService", "PRMPRServices", "PRMCustomFieldService", "$modal", "PRMPOServices", "reportingService", "PRMPOService", "$q", "workflowService",
        function ($state, $stateParams, $scope, $rootScope, auctionsService, userService, $http, $window, domain,
            fileReader, growlService, $log, $filter, ngDialog, catalogService, PRMPRServices, PRMCustomFieldService, $modal, PRMPOServices, reportingService, PRMPOService, $q, workflowService) {

            $scope.currentUserId = +userService.getUserId();
            $scope.isCustomer = $scope.customerType === "CUSTOMER" ? true : false;
            $scope.currentUserSessionId = userService.getUserToken();
            $scope.currentUsercompanyId = userService.getUserCompanyId();
            $scope.currentUserName = userService.getFirstname() + ' ' + userService.getLastname();
            $scope.currentUserSapId = userService.getSAPUserId();
            $scope.qcsId = $stateParams.qcsId;
            $scope.poNumber = $stateParams.Id;
            $scope.reqId = $stateParams.reqId;
            $scope.prNumber = $stateParams.prNumber ? $stateParams.prNumber : '';
            $scope.poQuotId = $stateParams.quotId;
            $scope.prID = $stateParams.prId;
            $scope.isEditable = $stateParams.isEditable;
            console.log($scope.isEditable);
            $scope.vendorAssignments = $stateParams.detailsObj;
            $scope.poRAWJSON = $stateParams.poRawJSON;
            let poRoundDigits = 2;
            $scope.POServiceStatusText = '';
            $scope.documentType = $stateParams.templateName;
            $scope.selectedVendors = [];
            $scope.currentUIVendor = {};
            $scope.currentUIPlant = {};
            $scope.formDetails = {};
            $scope.formDetails.items = [];
            $scope.reqDepartments = [];
            $scope.requirementPRItems = [];
            $scope.requirementPOItems = [];
            $scope.requirementVendorCodeInfo = [];
            $scope.qcsRequirementDetails = {};
            $scope.minDateMoment = moment();

            $scope.array = [];
            $scope.array1 = {};


            $scope.formDetails.MEMORY_TYPE = 'X';
            $scope.formDetails.PR_NUMBER = '';
            $scope.formDetails.PO_CREATOR = $scope.currentUserSapId;//$scope.requirementDetails && $scope.requirementDetails.prCreator ? $scope.requirementDetails.prCreator : '';
            $scope.formDetails.INCO_TERMS1 = '';
            $scope.formDetails.INCO_TERMS2 = '';
            $scope.formDetails.DOC_TYPE = $scope.documentType;
            $scope.requirementPlants = [];
            $scope.quantityError = false;
            var SelectedVendorDetails;
            $scope.companyConfigList = [];
            $scope.companyConfigStr = '';
            $scope.companyConfigurationArr = ['GST_DETAILS_F1', 'GST_DETAILS_JEEVES'];
            $scope.companyConfigStr = $scope.companyConfigurationArr.join(',').toString();
            $scope.COMPANY_INCO_TERMS = [];
            $scope.MasterPOs = [];
            $scope.deptIDs = [];
            $scope.desigIDs = [];
            $scope.deptIDStr = '';
            $scope.desigIDStr = '';
            $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
            if ($scope.ListUserDepartmentDesignations && $scope.ListUserDepartmentDesignations.length > 0) {
                $scope.ListUserDepartmentDesignations.forEach(function (item, index) {
                    $scope.deptIDs.push(item.deptID);
                    item.listDesignation.forEach(function (item1, index1) {
                        if (item1.isAssignedToUser && item1.isValid) {
                            $scope.desigIDs.push(item1.desigID);
                            $scope.desigIDs = _.union($scope.desigIDs, [item1.desigID]);
                        }
                    });
                });
                $scope.deptIDStr = $scope.deptIDs.join(',');
                $scope.desigIDStr = $scope.desigIDs.join(',');
            }

            /*region start WORKFLOW*/
            $scope.workflowList = [];
            $scope.itemWorkflow = [];
            $scope.obj = {
                auctionVendors: []
            };
            $scope.objNew = {
                auctionVendors: []
            };
            $scope.workflowObj = {};
            $scope.workflowObj.workflowID = 0;
            $scope.currentStep = 0;
            $scope.orderInfo = 0;
            $scope.assignToShow = '';
            $scope.isWorkflowCompleted = false;
            $scope.WorkflowModule = 'PO';
            $scope.disableWFSelection = false;
            /*region end WORKFLOW*/

            //auctionsService.GetCompanyConfiguration($scope.currentUsercompanyId, 'INCO', userService.getUserToken())
            //    .then(function (unitResponse) {
            //        $scope.companyConfigList = unitResponse;
            //        $scope.companyConfigList.forEach(function (item, index) {
            //            if (item.configKey === 'INCO') {
            //                $scope.COMPANY_INCO_TERMS.push(item);
            //            }
            //        });
            //    });

            $scope.precisionRound = function (number) {
                var factor = Math.pow(10, poRoundDigits);
                return Math.round(number * factor) / factor;
            };

            $scope.addItem = function () {
                $scope.formDetails.items.push({
                    ITEM_OF_REQUESITION: '',
                    isDeleted: false
                });
            };

            $scope.deleteItem = function (item) {
                item.isDeleted = true;
            };

            $scope.pageNo = 1;
            $scope.nextpage = function (pageNo) {
                $scope.pageNo = $scope.pageNo + 1;
            };

            $scope.previouspage = function () {
                $scope.pageNo = $scope.pageNo - 1;

            };

            $scope.cancel = function () {
                $window.history.back();
            };

            $scope.itemSourceList = [
                {
                    'key': 1,
                    'value':'Purchase'
                },
                {
                    'key': 21,
                    'value': 'Foc'
                }

            ]

            $scope.shippingWayList = [
                {
                    'key': 1,
                    'value': 'Air'
                },
                {
                    'key': 2,
                    'value': 'Sea'
                },
                {
                    'key': 3,
                    'value': 'Surface'
                },
                {
                    'key': 4,
                    'value': 'By Hand'
                }

            ]
            $scope.requirementDetails = {};
            $scope.requirementDetails.auctionVendors = [];
            $scope.GetRequirementVendorDetails = function () {
                auctionsService.getRequirementVendorDetails($scope.reqId, $scope.currentUserSessionId)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.requirementDetails.auctionVendors = response;

                            $scope.requirementDetails.auctionVendors.forEach(function (vendor) {
                                vendor.listRequirementItems = [];
                                $scope.GetRequirementVendorProductDetails(vendor.VENDOR_ID);

                            })
                        }
                    })
            };

            $scope.GetRequirementVendorProductDetails = function (vendorid) {
                auctionsService.getRequirementVendorProductDetails($scope.reqId, $scope.currentUserSessionId, vendorid)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.requirementDetails.auctionVendors.forEach(function (vendor) {
                                vendor.billToLocationError = false;
                                vendor.shipToLocationError = false;
                                vendor.poDeliveryDateError = false;
                                vendor.poReleaseDateError = false;
                                vendor.gstError = false;
                                vendor.SVC_CENTER_CODE = 'F1-SMart';
                                vendor.PO_TYPE = 'Standard';
                                vendor.showF1Values = _.filter(vendor.listRequirementItems, function (o) { return o.entity == 'F1'; }).length > 0 ? true : false;
                                vendor.listRequirementItems = response;
                                vendor.listRequirementItems.forEach(function (item,idx) {
                                    var lineItem = lineItemNum((idx + 1) * 10);
                                    item.PO_LINE_ITEM = lineItem;
                                    item.ASSIGN_QTY_TEMP = item.ASSIGN_QTY;
                                    item.ORDER_QTY = item.ASSIGN_QTY;
                                    item.ITEM_TOTAL_PRICE_NO_TAX = +(((+item.ORDER_QTY) * item.REV_UNIT_PRICE) * vendor.CURRENCY_FACTOR).toFixed(2);
                                    item.ITEM_TOTAL_PRICE = +((((+item.ORDER_QTY) * item.REV_UNIT_PRICE) + (((+item.ORDER_QTY) * item.REV_UNIT_PRICE) * ((item.S_GST + item.C_GST + item.I_GST) / 100))) * vendor.CURRENCY_FACTOR).toFixed(2);
                                    item.REV_UNIT_PRICE_WITH_TAX = item.REV_UNIT_PRICE + (item.REV_UNIT_PRICE * ((item.S_GST + item.C_GST + item.I_GST) / 100));

                                })
                                vendor.NET_PRICE = _.sumBy(vendor.listRequirementItems, 'TOTAL_PRICE_WITH_TAX');

                            })


                            if ($scope.poNumber != '') {
                                $scope.requirementDetails.auctionVendors.forEach(function (vendor) {
                                    vendor = $scope.poList[0];
                                    vendor.listRequirementItems = response;
                                    vendor.listRequirementItems.forEach(function (item, idx) {
                                        var lineItem = lineItemNum((idx + 1) * 10);
                                        item.PO_LINE_ITEM = lineItem;
                                        item.ASSIGN_QTY_TEMP = _.filter($scope.poList, function (o) { return o.PRODUCT_ID == item.PRODUCT_ID; }).ORDER_QTY;
                                        item.ORDER_QTY = _.filter($scope.poList, function (o) { return o.PRODUCT_ID == item.PRODUCT_ID; }).ORDER_QTY;
                                        item.ITEM_TOTAL_PRICE_NO_TAX = +(((+item.ORDER_QTY) * item.REV_UNIT_PRICE) * vendor.CURRENCY_FACTOR).toFixed(2);
                                        item.ITEM_TOTAL_PRICE = +((((+item.ORDER_QTY) * item.REV_UNIT_PRICE) + (((+item.ORDER_QTY) * item.REV_UNIT_PRICE) * ((item.S_GST + item.C_GST + item.I_GST) / 100))) * vendor.CURRENCY_FACTOR).toFixed(2);
                                        item.REV_UNIT_PRICE_WITH_TAX = item.REV_UNIT_PRICE + (item.REV_UNIT_PRICE * ((item.S_GST + item.C_GST + item.I_GST) / 100));

                                    });
                                    vendor.NET_PRICE = _.sumBy(vendor.listRequirementItems, 'TOTAL_PRICE_WITH_TAX');

                                })
                            }
                        }
                    })
            };

            $scope.GetRequirementVendorDetails();

            function lineItemNum(num) {
                let maxNum = 5;
                return num.toString().padStart(maxNum, "0");
            };



            $scope.convertDate = function (date) {
                if (date) {
                    var ts = moment(date, "DD-MM-YYYY").valueOf();
                    var m = moment(ts);
                    var quotationDate = new Date(m);
                    var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                    return "/Date(" + milliseconds + "000+0530)/";
                } else {
                    return null;
                }
            };


            $scope.submitPOToSAP = function (isDraft) {
                $scope.poList = [];
                $scope.allData = [];

                $scope.mandateError = false;

                $scope.requirementDetails.auctionVendors.forEach(function (vendor) {
                    vendor.billToLocationError = false;
                    vendor.shipToLocationError = false;
                    vendor.poDeliveryDateError = false;
                    vendor.poReleaseDateError = false;
                    vendor.advanceAmountError = false;
                    vendor.gstError = false;


                    if (!vendor.showF1Values) {
                        var billToLocationDisplay = [];
                        billToLocationDisplay = $scope.locationMasterJeeves.filter(function (item) {
                            return vendor.BILL_TO_LOCATION == item.LOCATION_SITE;
                        })
                        var shipToLocationDisplay = [];
                        shipToLocationDisplay = $scope.locationMasterJeeves.filter(function (item) {
                            return vendor.SHIP_TO_LOCATION == item.LOCATION_SITE;
                        })
                    } else {
                        var billToLocationDisplay = [];
                        billToLocationDisplay = $scope.locationMasterF1.filter(function (item) {
                            return vendor.BILL_TO_LOCATION == item.LOCATION_SITE;
                        })
                        var shipToLocationDisplay = [];
                        shipToLocationDisplay = $scope.locationMasterF1.filter(function (item) {
                            return vendor.SHIP_TO_LOCATION == item.LOCATION_SITE;
                        })
                    }

                    if (vendor.BILL_TO_LOCATION == '' || vendor.BILL_TO_LOCATION == undefined || billToLocationDisplay.length == 0) {
                        vendor.billToLocationError = true;
                        $scope.mandateError = true;
                    }
                    if (vendor.SHIP_TO_LOCATION == '' || vendor.SHIP_TO_LOCATION == undefined || shipToLocationDisplay.length == 0) {
                        vendor.shipToLocationError = true;
                        $scope.mandateError = true;
                    }
                    if (vendor.GST_REGISTRATION == '' || vendor.GST_REGISTRATION == undefined) {
                        vendor.gstError = true;
                        $scope.mandateError = true;
                    }
                    if (vendor.PO_DELIVERY_DATE == '' || vendor.PO_DELIVERY_DATE == undefined) {
                        vendor.poDeliveryDateError = true;
                        $scope.mandateError = true;
                    }
                    if (vendor.PO_RELEASE_DATE == '' || vendor.PO_RELEASE_DATE == undefined) {
                        vendor.poReleaseDateError = true;
                        $scope.mandateError = true;
                    }

                    if (+vendor.ADVANCE_AMOUNT > vendor.NET_PRICE_NO_TAX) {
                        vendor.advanceAmountError = true;
                        $scope.mandateError = true;
                    }
                })

                if ($scope.workflowObj.workflowID <= 0) {
                    $scope.errorMessage = 'Please select the workflow';
                }

                if ($scope.mandateError || $scope.workflowObj.workflowID <= 0) {
                    return;
                }

                let sapDataList = [];

                $scope.requirementDetails.auctionVendors.forEach(function (vendor) {

                    vendor.listRequirementItems.forEach(function (item) {
                        item.COMP_ID = $scope.currentUsercompanyId;
                        item.VENDOR_ID = vendor.VENDOR_ID;
                        item.PO_CREATOR = $scope.currentUserName;
                        item.PAYMENT_TERMS = $scope.PAYMENT_TERMS;
                        item.PO_RELEASE_DATE = vendor.PO_RELEASE_DATE;
                        item.PO_DELIVERY_DATE = vendor.PO_DELIVERY_DATE;
                        item.UNIT_PRICE_NO_TAX = item.REV_UNIT_PRICE;
                        item.UNIT_PRICE = item.REV_UNIT_PRICE_WITH_TAX;
                        item.USER = $scope.currentUserId;
                        item.WF_ID = $scope.workflowObj.workflowID;
                        item.REQ_ID = +$scope.reqId;
                        item.PR_ID = $scope.prID;
                        item.QCS_ID = +$scope.qcsId;
                        item.VENDOR_CODE = vendor.VENDOR_CODE;
                        item.NET_PRICE = vendor.NET_PRICE;
                        if (!item.showF1Values) {
                            item.SHIP_TO_LOCATION = _.filter($scope.locationMasterJeeves, function (o) { return o.LOCATION_SITE == vendor.SHIP_TO_LOCATION })[0].LOCATION_NAME
                            item.BILL_TO_LOCATION = _.filter($scope.locationMasterJeeves, function (o) { return o.LOCATION_SITE == vendor.BILL_TO_LOCATION })[0].LOCATION_NAME;
                        } else {
                            item.SHIP_TO_LOCATION = _.filter($scope.locationMasterF1, function (o) { return o.LOCATION_SITE == vendor.SHIP_TO_LOCATION })[0].LOCATION_NAME
                            item.BILL_TO_LOCATION = _.filter($scope.locationMasterF1, function (o) { return o.LOCATION_SITE == vendor.BILL_TO_LOCATION })[0].LOCATION_NAME;
                        }
                        item.VENDOR_SITE_CODE = vendor.VENDOR_SITE_CODE;
                        item.ITEM_SOURCE = vendor.ITEM_SOURCE;
                        item.SVC_CENTER_CODE = vendor.SVC_CENTER_CODE;
                        item.SHIPPING_WAY = vendor.SHIPPING_WAY;
                        item.PO_TYPE = vendor.PO_TYPE;
                        item.GST_REGISTRATION = vendor.GST_REGISTRATION;
                        item.ADVANCE_AMOUNT_OVERALL = vendor.ADVANCE_AMOUNT_OVERALL;
                        $scope.poList.push(item);

                    })

                })
               
                var params = {
                    "poItems": $scope.poList,
                    "sessionID": $scope.currentUserSessionId
                };


                PRMPOService.GeneratePRToPO(params)
                    .then(function (response) {
                        swal("PO Generated Successfully.", '', "success");
                        $state.go('list-pendingPO');

                    });
            };

            $scope.poList = [];

            $scope.getPOscheduleDetails = function () {
                var params = {
                    "ponumber": $scope.poNumber
                };
                PRMPOService.getPODetails(params)
                    .then(function (response) {
                        $scope.reqId = response[0].REQ_ID;
                        $scope.qcsId = response[0].QCS_ID;
                        $scope.poList = response;



                        $scope.GetRequirementVendorDetails();

                    });


            }


            $scope.updatePODetails = function () {
                let poList = [];

                var result = _.filter($scope.requirementDetails.auctionVendors[0].listRequirementItems, function (o) { return +o.ORDER_QTY == 0 });
                if (result.length > 0) {
                    swal("Error!", 'Please enter TOTAL Qty');
                    return;
                }

                if ($scope.workflowObj.workflowID == 0) {
                    swal("Error!", 'Please enter Workflow');
                    return;
                }
                $scope.requirementDetails.auctionVendors[0].listRequirementItems.forEach(function (item) {
                    item.WF_ID = $scope.workflowObj.workflowID;
                    item.USER = $scope.currentUserId;
                    poList.push({ 'WF_ID': item.WF_ID, 'USER': item.USER, 'ITEM_TOTAL_PRICE': item.ITEM_TOTAL_PRICE, 'ITEM_TOTAL_PRICE_NO_TAX': item.ITEM_TOTAL_PRICE_NO_TAX, 'NET_PRICE': item.NET_PRICE, 'ORDER_QTY': item.ORDER_QTY, 'PO_LINE_ITEM': item.PO_LINE_ITEM, 'PO_NUMBER': item.PO_NUMBER, 'AVAILABLE_QTY': item.AVAILABLE_QTY });

                })
                var params = {
                    "poItems": poList,
                    "sessionID": $scope.currentUserSessionId
                };

                PRMPOService.UpdatePODetails(params)
                    .then(function (response) {
                        swal("Updated Successfully.", '', "success");
                        $state.go('list-pendingPO');

                    });
            }



            $scope.qtyDistributionCal = function (vendor, item) {

                item.ITEM_TOTAL_PRICE_NO_TAX = +(((+item.ORDER_QTY) * item.REV_UNIT_PRICE) * vendor.CURRENCY_FACTOR).toFixed(2);
                item.ITEM_TOTAL_PRICE = +((((+item.ORDER_QTY) * item.REV_UNIT_PRICE) + (((+item.ORDER_QTY) * item.REV_UNIT_PRICE) * ((item.S_GST + item.C_GST + item.I_GST) / 100))) * vendor.CURRENCY_FACTOR).toFixed(2);

                vendor.NET_PRICE = _.sumBy(vendor.listRequirementItems, 'TOTAL_PRICE_WITH_TAX');

            }

            $scope.advanceQtyDistribution = function (vendor) {

                var x = vendor.ADVANCE_AMOUNT_OVERALL;

                vendor.listRequirementItems.forEach(function (item) {
                    if (x > item.ITEM_TOTAL_PRICE) {
                        item.ADVANCE_AMOUNT = item.ITEM_TOTAL_PRICE;
                    } else {
                        item.ADVANCE_AMOUNT = x;
                    }
                    x = x - item.ADVANCE_AMOUNT;

                });

            }


            //$scope.detectLinks = function urlify(text) {
            //    if (text) {
            //        var urlRegex = /(https?:\/\/[^\s]+)/g;
            //        return text.replace(urlRegex, function (url) {
            //            return url;//'<a target="_blank" href="' + url + '">' + url + '</a>';
            //        });
            //    }
            //};
            ///*region start WORKFLOW*/

            $scope.getWorkflows = function () {
                workflowService.getWorkflowList()
                    .then(function (response) {
                        $scope.workflowList = [];
                        $scope.workflowListDeptWise = [];
                        $scope.workflowListTemp = response;
                        $scope.workflowListTemp.forEach(function (item, index) {
                            if (item.WorkflowModule == $scope.WorkflowModule) {
                                $scope.workflowList.push(item);
                                $scope.workflowListDeptWise.push(item);
                            }
                        });

                        if (userService.getUserObj().isSuperUser) {
                            $scope.workflowList = $scope.workflowList;
                        }
                        else {
                            $scope.workflowList = [];
                            $scope.workflowListDeptWise.forEach(function (wf, idx) {
                                $scope.deptIDs.forEach(function (dep) {
                                    if (dep == wf.deptID) {
                                        $scope.workflowList.push(wf);
                                    }
                                });
                            });
                        }
                    });
            };

            $scope.getWorkflows();

            $scope.getItemWorkflow = function (wfID, prID, type, itemID) {
                workflowService.getItemWorkflow(wfID, prID, type)
                    .then(function (response) {
                        $scope.itemWorkflow = response;
                        if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                            $scope.currentStep = 0;

                            var count = 0;

                            $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {
                                if (!track.multipleAttachments) {
                                    track.multipleAttachments = [];
                                }

                                if (track.status === 'APPROVED') { track.statusNew = 'Approved'; }
                                if (track.status === 'HOLD') { track.statusNew = 'Hold'; }
                                if (track.status === 'PENDING') { track.statusNew = 'Pending'; }
                                if (track.status === 'REJECTED') { track.statusNew = 'Rejected'; }

                                if (track.status === 'APPROVED' || track.status === 'HOLD') {
                                    $scope.isFormdisabled = true;
                                }

                                if (track.status === 'APPROVED') {
                                    $scope.isWorkflowCompleted = true;
                                    $scope.orderInfo = track.order;
                                    $scope.assignToShow = track.status;

                                }
                                else {
                                    $scope.isWorkflowCompleted = false;
                                }

                                if (track.status === 'REJECTED' && count == 0) {
                                    count = count + 1;
                                }

                                if ((track.status === 'PENDING' || track.status === 'HOLD') && count == 0) {
                                    count = count + 1;
                                    $scope.IsUserApproverForStage(track.approverID);
                                    $scope.currentAccess = track.order;
                                }

                                if ((track.status === 'PENDING' || track.status === 'HOLD' || track.status === 'REJECTED') && $scope.currentStep === 0) {
                                    $scope.currentStep = track.order;
                                    return false;
                                }
                            });
                        }
                    });
            };

            $scope.updateTrack = function (step, status) {

                if (!$rootScope.isUserEntitled(591164)) {
                    swal("Error!", "Workflow Approval Denied", "error");
                    $state.go('home');
                    return;
                }

                $scope.disableAssignPR = true;
                $scope.commentsError = '';
                if (step.comments != null || step.comments != "" || step.comments != undefined) {
                    step.comments = validateStringWithoutSpecialCharacters(step.comments);
                }
                var tempArray = $scope.itemWorkflow[0].WorkflowTracks[$scope.itemWorkflow[0].WorkflowTracks.length - 1];
                if (step.order == tempArray.order && status === 'APPROVED') {
                    $scope.disableAssignPR = false;
                } else {
                    $scope.disableAssignPR = true;
                }

                if ($scope.isReject) {
                    $scope.commentsError = 'Please Save Rejected Items/Qty';
                    return false;
                }

                if (status === 'REJECTED' && (step.comments == null || step.comments == "")) {
                    $scope.commentsError = 'Please enter comments';
                    return false;
                }

                step.status = status;
                step.sessionID = $scope.sessionID;
                step.modifiedBy = userService.getUserId();

                step.moduleName = $scope.WorkflowModule;

                //step.subModuleName = $scope.requirementDetails.title;
                //step.subModuleID = $scope.reqId;

                workflowService.SaveWorkflowTrack(step)
                    .then(function (response) {
                        if (response.errorMessage) {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            $scope.getItemWorkflow();
                            location.reload();
                            //$state.go('approval-qcs-list');
                        }
                    });
            };

            $scope.assignWorkflow = function (moduleID) {
                workflowService.assignWorkflow(({ wID: $scope.workflowObj.workflowID, moduleID: moduleID, user: userService.getUserId(), sessionID: $scope.sessionID }))
                    .then(function (response) {
                        if (response.errorMessage) {
                            growlService.growl(response.errorMessage, "inverse");
                            $scope.isSaveDisable = false;
                        }
                        else {
                            //  $state.go('list-pr');
                        }
                    });
            };

            $scope.IsUserApprover = false;

            $scope.functionResponse = false;

            $scope.IsUserApproverForStage = function (approverID) {
                workflowService.IsUserApproverForStage(approverID, userService.getUserId())
                    .then(function (response) {
                        $scope.IsUserApprover = response;
                    });
            };

            $scope.isApproverDisable = function (index) {

                var disable = true;

                var previousStep = {};

                $scope.itemWorkflow[0].WorkflowTracks.forEach(function (step, stepIndex) {

                    if (index == stepIndex) {
                        if (stepIndex == 0) {
                            if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                                (step.status === 'PENDING' || step.status === 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                        else if (stepIndex > 0) {
                            if (previousStep.status === 'PENDING' || previousStep.status === 'HOLD' || previousStep.status === 'REJECTED') {
                                disable = true;
                            }
                            else if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                                (step.status === 'PENDING' || step.status === 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                    }
                    previousStep = step;
                });

                return disable;
            };

            /*region end WORKFLOW*/

            $scope.isUserBelongsToDeptandDesig = function (deptID, desigID) {
                var isEligible = true;

                if ($scope.deptIDs.indexOf(deptID) != -1 && $scope.desigIDs.indexOf(desigID) != -1) {
                    isEligible = true;
                } else {
                    isEligible = false;
                }

                return isEligible;
            };

            function validateStringWithoutSpecialCharacters(string) {
                if (string) {
                    string = string.replace(/\'/gi, "");
                    string = string.replace(/\"/gi, "");
                    string = string.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                    string = string.replace(/(\r\n|\n|\r)/gm, "");
                    string = string.replace(/\t/g, '');
                    return string;
                }
            };

            $scope.GetDateconverted = function (dateBefore) {
                if (dateBefore) {
                    return new moment(dateBefore).format("DD-MM-YYYY");
                }
            };

            $scope.gstMasterF1 = [];
            $scope.gstMasterJeeves = [];
            auctionsService.GetCompanyConfiguration(0, $scope.companyConfigStr, userService.getUserToken())
                .then(function (unitResponse) {

                    $scope.companyConfigList = unitResponse;
                    var countriesTemp = [];
                    $scope.companyConfigList.forEach(function (item, index) {
                        if (item.configKey == 'GST_DETAILS_F1') {
                            $scope.gstMasterF1.push(item);
                        } else if (item.configKey == 'GST_DETAILS_JEEVES') {
                            $scope.gstMasterJeeves.push(item);
                        } 
                    });

                });

            $scope.locationList = [];
            $scope.locationMasterF1 = [];
            $scope.locationMasterJeeves = [];
            $scope.GetOracleConfiguration = function () {

                $scope.tableColumns = [];
                $scope.tableValues = [];
                $scope.filteredArray = [];
                var params = {
                    "companyId": +$scope.currentUsercompanyId,
                    "type": 'LOCATION',
                    "sessionid": userService.getUserToken()
                };
                auctionsService.getOracleConfiguration(params)
                    .then(function (response) {
                        if (response) {
                            var arr = JSON.parse(response).Table;
                            $scope.locationList = arr;
                            $scope.locationList.forEach(function (item, index) {
                                var digit = item.LOCATION_CODE.toString()[0];

                                if (digit == '7') {
                                    $scope.locationMasterF1.push(item);
                                } else if (digit == '9') {
                                    $scope.locationMasterJeeves.push(item);
                                }
                            });
                            }
                        
                    });
            };
            $scope.GetOracleConfiguration();




            $scope.billLocationDisplay = [];
            $scope.shipLocationDisplay = [];
            $scope.locationSearch = function (value, vendor, type, name) {

                $scope.billLocationDisplay = [];
                $scope.shipLocationDisplay = [];
                
                if (value == '' || value == undefined) {
                    if (name == 'BILL_TO_LOCATION') {
                        vendor.showSelectedBillLocation = false;
                    } else {
                        vendor.showSelectedShipLocation = false;
                    }

                } else {
                    if (name == 'BILL_TO_LOCATION') {
                        vendor.showSelectedBillLocation = true;
                        if (!type) {
                            $scope.billLocationDisplay = $scope.locationMasterJeeves.filter(function (item) {
                                return (String(item.LOCATION_SITE.toLowerCase()).includes(value.toLowerCase()) == true);
                            })
                        } else {
                            $scope.billLocationDisplay = $scope.locationMasterF1.filter(function (item) {
                                return (String(item.LOCATION_SITE.toLowerCase()).includes(value.toLowerCase()) == true);
                            })
                        }
                    } else {
                        vendor.showSelectedShipLocation = true;
                        if (!type) {
                            $scope.shipLocationDisplay = $scope.locationMasterJeeves.filter(function (item) {
                                return (String(item.LOCATION_SITE.toLowerCase()).includes(value.toLowerCase()) == true);
                            })
                        } else {
                            $scope.shipLocationDisplay = $scope.locationMasterF1.filter(function (item) {
                                return (String(item.LOCATION_SITE.toLowerCase()).includes(value.toLowerCase()) == true);
                            })
                        }
                    }
                    
                    
                }
            }

            $scope.fillValue = function (value, vendor,type) {
                if (type == 'BILL_TO_LOCATION') {
                    vendor.BILL_TO_LOCATION = value;
                    vendor.showSelectedBillLocation = false;

                } else {
                    vendor.SHIP_TO_LOCATION = value;
                    vendor.showSelectedShipLocation = false;

                }
            }

        }]);