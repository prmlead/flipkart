﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider
                .state('save-lot-details', {
                    url: '/save-lot-details/:Id/:reqId',
                    templateUrl: 'lot-bidding/views/save-lot-details.html',
                    params: {
                        detailsObj: null
                    }
                })
                .state('view-lot-details', {
                    url: '/lot-details/:Id',
                    templateUrl: 'lot-bidding/views/view-lot-details.html',
                    params: {
                        detailsObj: null
                    }
                })
                .state('view-lot-requirement', {
                    url: '/view-lot-requirement/:Id',
                    templateUrl: 'lot-bidding/views/lot-list-item.html',
                    params: {
                        detailsObj: null
                    }
                })
                .state('req-lot-list', {
                    url: '/req-lot-list',
                    templateUrl: 'lot-bidding/views/lot-list.html',
                    params: {
                        detailsObj: null
                    }
                });
        }]);