﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class UpdateDbValues : Entity
    {
        [DataMember(Name="userId")]
        public int UserId { get; set; }

        [DataMember(Name = "reqId")]
        public int ReqId { get; set; }

        [DataMember(Name = "savingsBeforeNegotiation")]
        public double SavingsBeforeNegotiation { get; set; }

        [DataMember(Name = "minReduceAmount")]
        public double MinReduceAmount { get; set; }

        [DataMember(Name = "venodComparisionPrice")]
        public double VenodComparisionPrice { get; set; }

        [DataMember(Name = "venodPrice")]
        public double VenodPrice { get; set; }
    }
}