﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class MngtRequirementDetails : Entity
    {

        [DataMember(Name = "auctionVendors")]
        public List<VendorDetails> AuctionVendors { get; set; }

        [DataMember(Name = "reqId")]
        public int ReqId { get; set; } 

        [DataMember(Name = "userId")]
        public int UserId { get; set; }     

        [DataMember(Name = "reqTitle")]
        public string ReqTitle { get; set; }

        [DataMember(Name = "companyName")]
        public string CompanyName { get; set; }

        [DataMember(Name = "createdDate")]
        public DateTime? CreatedDate { get; set; }

        [DataMember(Name = "budget")]
        public double Budget { get; set; }

        [DataMember(Name = "status")]
        public string Status { get; set; }

        [DataMember(Name = "beforeNegotiationSavings")]
        public double BeforeNegotiationSavings { get; set; }

        [DataMember(Name = "onNegotiationSavings")]
        public double OnNegotiationSavings { get; set; }




        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "subcategories")]
        public string Subcategories { get; set; }

        [DataMember(Name = "urgency")]
        public string Urgency { get; set; }

        [DataMember(Name = "deliveryLocation")]
        public string DeliveryLocation { get; set; }

        [DataMember(Name = "taxes")]
        public string Taxes { get; set; }





        [DataMember(Name = "paymentTerms")]
        public string PaymentTerms { get; set; }

        [DataMember(Name = "startTime")]
        public DateTime? StartTime { get; set; }

        [DataMember(Name = "endTime")]
        public DateTime? EndTime { get; set; }

        //[DataMember(Name = "deliveryTime")]
        //public DateTime? DeliveryTime { get; set; }


        string deliveryTime = string.Empty;
        [DataMember(Name = "deliveryTime")]
        public string DeliveryTime
        {
            get
            {
                if (!string.IsNullOrEmpty(deliveryTime))
                {
                    return deliveryTime;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    deliveryTime = value;
                }
            }
        }


        [DataMember(Name = "inclusiveTax")]
        public bool InclusiveTax { get; set; }

        [DataMember(Name = "includeFreight")]
        public bool IncludeFreight { get; set; }



        [DataMember(Name = "timeZone")]
        public string TimeZone { get; set; }

        [DataMember(Name = "minReduceAmount")]
        public int MinReduceAmount { get; set; }

        [DataMember(Name = "minBidAmount")]
        public double MinBidAmount { get; set; }


        [DataMember(Name = "quotationFreezTime")]
        public DateTime? QuotationFreezTime { get; set; }



        [DataMember(Name = "mngtRequirementId")]
        public int MngtRequirementId { get; set; }


        string customer = string.Empty;
        [DataMember(Name = "customer")]
        public string Customer
        {
            get
            { 
                if (!string.IsNullOrEmpty(customer))
                    { return customer; }
                else
                    { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { customer = value; }
            }
        }

        string category = string.Empty;
        [DataMember(Name = "category")]
        public string Category
        {
            get
            {
                if (!string.IsNullOrEmpty(category))
                { return category; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { category = value; }
            }
        }

        //string quotationFreezTime = string.Empty;
        //[DataMember(Name = "quotationFreezTime")]
        //public string QuotationFreezTime
        //{
        //    get
        //    {
        //        if (!string.IsNullOrEmpty(quotationFreezTime))
        //        { return quotationFreezTime; }
        //        else
        //        { return ""; }
        //    }
        //    set
        //    {
        //        if (!string.IsNullOrEmpty(value))
        //        { quotationFreezTime = value; }
        //    }
        //}

        string currency = string.Empty;
        [DataMember(Name = "currency")]
        public string Currency
        {
            get
            {
                if (!string.IsNullOrEmpty(currency))
                { return currency; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { currency = value; }
            }
        }

        string attachment = string.Empty;
        [DataMember(Name = "attachment")]
        public string Attachment
        {
            get
            {
                if (!string.IsNullOrEmpty(attachment))
                { return attachment; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { attachment = value; }
            }
        }

        string vendors = string.Empty;
        [DataMember(Name = "vendors")]
        public string Vendors
        {
            get
            {
                if (!string.IsNullOrEmpty(vendors))
                { return vendors; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { vendors = value; }
            }
        }

        string notes = string.Empty;
        [DataMember(Name = "notes")]
        public string Notes
        {
            get
            {
                if (!string.IsNullOrEmpty(notes))
                { return notes; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { notes = value; }
            }
        }








        //[DataMember(Name = "savings")]
        //public double Savings { get; set; }

        //[DataMember(Name = "category")]
        //public string Category { get; set; }








    }
}