﻿prmApp
    .controller('poItem-ListCtrl', ["$rootScope", "$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMPRServices",
        "PRMCustomFieldService", "catalogService", "$location", "PRMUploadServices", "$element", "fileReader", "reportingService","PRMPOService",
        function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPRServices, PRMCustomFieldService, catalogService, $location, PRMUploadServices, $element, fileReader, reportingService, PRMPOService) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;
            $scope.compId = userService.getUserCompanyId();
            $scope.isCustomer = userService.getUserType() === "CUSTOMER" ? true : false;
            $scope.reqLevel = 1;

            $scope.filterDate = {
                poToDate : '',
                poFromDate : ''

            }


            $scope.onlyContracts = $stateParams.onlyContracts ? 1 : 0;
            $scope.excludeContracts = $stateParams.excludeContracts ? 1 : 0;
            if (!$scope.onlyContracts && !$scope.excludeContracts) {
                $scope.onlyContracts = 0;
                $scope.excludeContracts = 0;
                if (window.location.href && window.location.href.toLowerCase().indexOf('list-pendingcontracts') >= 0) { //Backup on F5.
                    $scope.onlyContracts = 1;
                } else {
                    $scope.excludeContracts = 1;
                }
            }

            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;
            $scope.filters = {
                searchKeyword: '',
            }

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
                $scope.GetPOScheduleItemsList(($scope.currentPage - 1), 10);
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };


            $scope.filterDate.poToDate = moment().format('YYYY-MM-DD');
            $scope.filterDate.poFromDate = moment().subtract(30, "days").format("YYYY-MM-DD");

            //$scope.filterByDate = function () {
            //    $scope.getuserStatusReport(0, 10);

            //};

            $scope.setFilters = function (currentPage,type) {


                if ($scope.loadServices) {
                    $scope.GetPOScheduleItemsList(0, 10);
                }

                if (type == 'VENDOR') {
                    $scope.GetitemsListfilters();
                }
                

            };

            $scope.filtersList = {
                requirementList: [],
                poNumberList : [],
                prNumberList :[],
                caseIdList :[],
                skuIdList :[],
                brandList :[],
                categoryList :[],
                poStatusList :[],
                entityList :[],
                creatorList: [],
                supplierList : []
            };


            $scope.filters = {
                reqNumber: {},
                prNumber: {},
                poNumber: {},
                caseID: {},
                skuID: {},
                brand: {},
                category: {},
                poStatus: {},
                entity: {},
                creatorName: {},
                supplier: {}
            };

     

            $scope.getDate = function (value) {
                return userService.toLocalDate(value);
            }
            $scope.tableColumns = [];
            $scope.rows = [];
           
            $scope.GetPOScheduleItemsList = function (recordsFetchFrom, pageSize, downloadToExcel) {


                var poNumbers,reqNumbers, prNumbers, caseIds, skus, brands, categorys, creatorName, postatus, entity;

                if (_.isEmpty($scope.filterDate.poFromDate)) {
                    fromDate = '';
                } else {
                    fromDate = $scope.filterDate.poFromDate;
                }

                if (_.isEmpty($scope.filterDate.poToDate)) {
                    toDate = '';
                } else {
                    toDate = $scope.filterDate.poToDate;
                }


                if (_.isEmpty($scope.filters.poNumber)) {
                    poNumber = '';
                } else if ($scope.filters.poNumber && $scope.filters.poNumber.length > 0) {
                    var poNumbers = _($scope.filters.poNumber)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    poNumber = poNumbers.join(',');
                }

                if (_.isEmpty($scope.filters.reqNumber)) {
                    reqNumber = '';
                } else if ($scope.filters.reqNumber && $scope.filters.reqNumber.length > 0) {
                    var reqNumbers = _($scope.filters.reqNumber)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    reqNumber = reqNumbers.join(',');
                }

                if (_.isEmpty($scope.filters.caseID)) {
                    caseId = '';
                } else if ($scope.filters.caseID && $scope.filters.caseID.length > 0) {
                    var caseIds = _($scope.filters.caseID)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    caseId = caseIds.join(',');
                }


                if (_.isEmpty($scope.filters.prNumber)) {
                    prNumber = '';
                } else if ($scope.filters.prNumber && $scope.filters.prNumber.length > 0) {
                    var prNumbers = _($scope.filters.prNumber)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    prNumber = prNumbers.join(',');
                }


                if (_.isEmpty($scope.filters.skuID)) {
                    sku = '';
                } else if ($scope.filters.skuID && $scope.filters.skuID.length > 0) {
                    var skus = _($scope.filters.skuID)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    sku = skus.join(',');
                }

                if (_.isEmpty($scope.filters.brand)) {
                    brand = '';
                } else if ($scope.filters.brand && $scope.filters.brand.length > 0) {
                    var brands = _($scope.filters.brand)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    brand = brands.join(',');
                }

                if (_.isEmpty($scope.filters.category)) {
                    category = '';
                } else if ($scope.filters.category && $scope.filters.category.length > 0) {
                    var categorys = _($scope.filters.category)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    category = categorys.join(',');
                }

                if (_.isEmpty($scope.filters.poStatus)) {
                    postatus = '';
                } else if ($scope.filters.poStatus && $scope.filters.poStatus.length > 0) {
                    var postatus = _($scope.filters.poStatus)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    postatus = postatus.join(',');
                }
             

                if (_.isEmpty($scope.filters.creatorName)) {
                    creatorName = '';
                } else if ($scope.filters.creatorName && $scope.filters.creatorName.length > 0) {
                    var creatorName = _($scope.filters.creatorName)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    creatorName = creatorName.join(',');
                }

                if (_.isEmpty($scope.filters.supplier)) {
                    supplierName = '';
                } else if ($scope.filters.supplier && $scope.filters.supplier.length > 0) {
                    var supplierName = _($scope.filters.supplier)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    supplierName = supplierName.join(',');
                }


                if (_.isEmpty($scope.filters.brand)) {
                    brand = '';
                } else if ($scope.filters.brand && $scope.filters.brand.length > 0) {
                    var brand = _($scope.filters.brand)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    brand = brand.join(',');
                }


                //$scope.FIELDS_ARR = [];
                //$scope.MAPPING_FIELDS = [];

                var params = {
                    "compid": $scope.isCustomer ? $scope.compId : 0,
                    "uid": $scope.isCustomer ? 0 : $scope.userID,
                    "search": "",
                    "categoryid": category,
                    "productid": sku,
                    "supplier": supplierName,
                    "postatus": postatus,
                    "fromdate": fromDate,
                    "todate": toDate,
                    "page": recordsFetchFrom * pageSize,
                    "pagesize": pageSize,
                    "buyer": creatorName,
                    "poNumber": poNumber,
                    "prNumber": prNumber,
                    "caseID": caseId,
                    "brand": brand,
                    "reqNumber": reqNumber
                };

                //$scope.jsonFields = '';

                //$scope.jsonFields = JSON.stringify(myObject);

                //var params =
                //{
                //    "filters": $scope.jsonFields,
                //    "sessionid": userService.getUserToken()
                //};



                PRMPOService.GetPOScheduleItemsList(params)
                    .then(function (response) {
                        $scope.loadServices = true;
                        if (response) {
                            $scope.arr = JSON.parse(response).Table;
                            if ($scope.arr && $scope.arr.length > 0) {
                                $scope.totalItems = $scope.arr[0].TOTAL_COUNT;
                                $scope.arr.forEach(a => delete a.TOTAL_COUNT);
                                if ($scope.tableColumns.length <= 0) {
                                    $scope.tableColumnsTemp = angular.copy(_.keys($scope.arr[0]));
                                    $scope.tableColumnsTemp.forEach(function (item, index) {
                                        item = item.replaceAll("_", " ");
                                        $scope.tableColumns.push(item);
                                    });
                                }

                                $scope.rows = $scope.arr;
                                $scope.arr.forEach(function (item, index) {
                                    if (item.Date) {
                                        item.Date = moment(item.Date).format("DD-MM-YYYY");
                                    }
                                    var obj = angular.copy(_.values(item));
                                    if (obj) {
                                        item.tableValues = [];
                                        obj.forEach(function (value, valueIndex)
                                        {
                                            var newObj =
                                            {
                                                "displayValue": value,
                                                "displayId":value,
                                                "type": "normal" 
                                            };

                                            if (value && typeof(value) == 'string')
                                            {
                                                if (value.includes("@HYPER_LINK@"))
                                                {
                                                    newObj.type = "link";
                                                    var originalVal = value;
                                                    var returnValue = originalVal.replace(/@HYPER_LINK@/g, '');
                                                    newObj.displayId = returnValue.split("/")[2];
                                                    var modifiedURL = returnValue.replaceAll("DOMESTIC", '/cost-comparisions-qcs').replaceAll("IMPORT", '/import-qcs');
                                                    newObj.displayValue = window.location.href.slice(0, window.location.href.indexOf('#')) + "#" + modifiedURL;
                                                }
                                            }

                                            item.tableValues.push(newObj);
                                        });
                                    }
                                });

                                if (downloadToExcel) {
                                    $scope.excelRows = [];
                                    $scope.excelColumnsTemp = [];
                                    $scope.excelColumns = [];
                                    $scope.rows.forEach(function (rowItem, rowIndex) {
                                        var newObj = Object.assign({}, rowItem);
                                        delete newObj.tableValues;
                                        $scope.excelColumnsTemp.push(newObj);
                                    });
                                    if ($scope.excelColumnsTemp && $scope.excelColumnsTemp.length > 0) {
                                        Object.keys($scope.excelColumnsTemp[0]).forEach(function (key, index) {
                                            var value = '[' + key.replaceAll(" ", "_") + ']' + ' as [' + key.replaceAll("_", " ") + ']';
                                            $scope.excelColumns.push(value);
                                        });
                                        $scope.excelColumns = $scope.excelColumns.join(',');

                                        $scope.excelColumnsTemp.forEach(function (item, index) {
                                            let newObj = {};

                                            for (let key in item) {
                                                let newKey = key.replace(/ /g, '_');  // Replace space with underscore

                                                newObj[newKey] = item[key];

                                                if (typeof item[key] === 'string')
                                                {
                                                    if (item[key].includes("@HYPER_LINK@"))
                                                    {
                                                        var returnValue = item[key].replace(/@HYPER_LINK@/g, '');
                                                        newObj[newKey] = returnValue.split("/")[2];
                                                    }
                                                }
                                            }
                                            $scope.excelRows.push(newObj);
                                        });
                                    }
                                    execute();
                                    $scope.rows = $scope.arr.slice(0, 10);
                                }

                               
                            }
                        } else {
                            $scope.rows = [];
                            $scope.arr = [];
                            $scope.totalItems = 0;
                        }


                        
                    })
                
            }
            $scope.GetPOScheduleItemsList(0, 10);



            $scope.filterValues = [];

            $scope.GetitemsListfilters = function () {
                var params =
                {
                    "userID": userService.getUserId(),
                    "fromDate": $scope.filterDate.poFromDate,
                    "toDate": $scope.filterDate.poToDate,
                    "sessionid": userService.getUserToken(),
                    "type": 'PO'

                };

                let reqNumberListTemp = [];
                let prNumberListTemp = [];
                let poNumberListTemp = [];
                let caseIdListTemp = [];
                let skuIdListTemp = [];
                let brandListTemp = [];
                let categoryListTemp = [];
                let poStatusListTemp = [];
                let entityListTemp = [];
                let creatorNameListTemp = [];
                let supplierNameListTemp = [];
                $scope.loadServices = false;

                auctionsService.GetitemsListfilters(params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.filterValues = response;
                            if ($scope.filterValues && $scope.filterValues.length > 0) {
                                $scope.filterValues.forEach(function (item, index) {
                                    if (item.TYPE === 'REQ_NUMBER') {
                                        reqNumberListTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'PR_NUMBER') {
                                        prNumberListTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'CASE_ID') {
                                        caseIdListTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'SKU_ID') {
                                        skuIdListTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'BRAND') {
                                        brandListTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'CATEGORY') {
                                        categoryListTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'PO_STATUS') {
                                        poStatusListTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'PO_NUMBER') {
                                        poNumberListTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'CREATOR_NAME') {
                                        creatorNameListTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'VENDOR_NAME') {
                                        supplierNameListTemp.push({ id: item.ID, name: item.NAME });
                                    }

                                });

                                $scope.filtersList.requirementList = reqNumberListTemp;
                                $scope.filtersList.prNumberList = prNumberListTemp;
                                $scope.filtersList.poNumberList = poNumberListTemp;
                                $scope.filtersList.caseIdList = caseIdListTemp;
                                $scope.filtersList.skuIdList = skuIdListTemp;
                                $scope.filtersList.brandList = brandListTemp;
                                $scope.filtersList.categoryList = categoryListTemp;
                                $scope.filtersList.poStatusList = poStatusListTemp;
                                $scope.filtersList.entityList = entityListTemp;
                                $scope.filtersList.creatorList = creatorNameListTemp;
                                $scope.filtersList.supplierList = supplierNameListTemp;

                            }
                        } else {

                            $scope.filtersList = {
                                requirementList: [],
                                poNumberList: [],
                                prNumberList: [],
                                caseIdList: [],
                                skuIdList: [],
                                brandList: [],
                                categoryList: [],
                                poStatusList: [],
                                entityList: [],
                                creatorList: [],
                                supplierList: []
                            };
                        }
                    });

            };
            $scope.GetitemsListfilters();
            


            $scope.downloadToExcel = function () {
                $scope.GetPOScheduleItemsList(0, 0, true);
            }


            function execute()
            {
                var mystyle = {
                    headers: true,
                    column: { style: { Font: { Color: "#FFFFFF" }, Interior: { Color: "#4F81BD", Pattern: "Solid" }, Alignment: { Horizontal: "Center" } } },
                    row: { style: { Alignment: { Horizontal: "Center" } } }
                };
                alasql('SELECT ' + $scope.excelColumns + ' INTO XLSXML("PO_ITEM_DETAILS.xls",?) FROM ?', [mystyle, $scope.excelRows]);
                $scope.downloadExcel = false;                
            }


            $scope.routerpolevel = function () {
                $state.go('list-pendingPO');
            };


           
          
        }]);