﻿prmApp.constant('PRMPOServiceDomain', 'svc/PRMPOService.svc/REST/');
prmApp.service('PRMPOService', ["PRMPOServiceDomain", "SAPIntegrationServicesDomain", "userService", "httpServices", "$window",
    function (PRMPOServiceDomain, SAPIntegrationServicesDomain, userService, httpServices, $window) {

        var PRMPOService = this;

        PRMPOService.getPOScheduleList = function (params) {

            params.onlycontracts = params.onlycontracts ? params.onlycontracts : 0;
            params.excludecontracts = params.excludecontracts ? params.excludecontracts : 0;

            let url = PRMPOServiceDomain + 'getposchedulelist?compid=' + params.compid + '&uid=' + params.uid + '&search=' + params.search + '&categoryid=' + params.categoryid
                + '&productid=' + params.productid + '&supplier=' + params.supplier + '&postatus=' + params.postatus
                + '&deliverystatus=' + params.deliverystatus
                + '&plant=' + params.plant
                + '&fromdate=' + params.fromdate + '&todate=' + params.todate
                + '&onlycontracts=' + params.onlycontracts + '&excludecontracts=' + params.excludecontracts + '&ackStatus=' + params.ackStatus + '&buyer=' + params.buyer + '&purchaseGroup=' + params.purchaseGroup
                + '&page=' + params.page
                + '&pagesize=' + params.pagesize + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };


        PRMPOService.GetPOScheduleItemsList = function (params) {

            let url = PRMPOServiceDomain + 'getPOScheduleItemsList?compid=' + params.compid + '&uid=' + params.uid + '&search=' + params.search + '&categoryid=' + params.categoryid
                + '&productid=' + params.productid + '&supplier=' + params.supplier + '&postatus=' + params.postatus               
                + '&fromdate=' + params.fromdate + '&todate=' + params.todate + '&buyer=' + params.buyer 
                + '&poNumber=' + params.poNumber + '&prNumber=' + params.prNumber + '&caseID=' + params.caseID + '&brand=' + params.brand + '&reqNumber=' + params.reqNumber + '&page=' + params.page
                + '&pagesize=' + params.pagesize + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };


        //PRMPOService.GetPOScheduleItemsList = function (params) {
        //    let url = PRMPOServiceDomain + 'getPOScheduleItemsList';
        //    return httpServices.post(url, params);
        //};

        PRMPOService.SavePOScheduleDetails = function (params) {
            let url = PRMPOServiceDomain + 'saveposcheduledetails';
            return httpServices.post(url, params);
        };

        PRMPOService.GeneratePRToPO = function (params) {
            let url = PRMPOServiceDomain + 'generatePRToPO';
            return httpServices.post(url, params);
        };
        PRMPOService.UpdatePODetails = function (params) {
            let url = PRMPOServiceDomain + 'updatePODetails';
            return httpServices.post(url, params);
        };
        PRMPOService.UpdateVendorGstDetails = function (params) {
            let url = PRMPOServiceDomain + 'updateVendorGstDetails';
            return httpServices.post(url, params);
        };

        PRMPOService.getPOScheduleItems = function (params) {
            let url = PRMPOServiceDomain + 'getposcheduleitems?ponumber=' + params.ponumber + '&moredetails=' + params.moredetails + '&forasn=' + params.forasn + '&sessionid=' + userService.getUserToken();

            return httpServices.get(url);
        };

        PRMPOService.getPOScheduleFilterValues = function (params) {
            let url = PRMPOServiceDomain + 'getposchedulefilters?compid=' + params.compid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPOService.editPOInvoice = function (params) {
            let url = PRMPOServiceDomain + 'editPOInvoice';
            return httpServices.post(url, params);
        };


        PRMPOService.getPOInvoiceDetails = function (params) {
            if (!params.vendorID) {
                params.vendorID = 0;
            }
            if (!params.invoiceID) {
                params.invoiceID = 0;
            }
            let url = PRMPOServiceDomain + 'getpoinvoicedetails?ponumber=' + params.ponumber + '&sessionid=' + userService.getUserToken() + '&vendorID=' + params.vendorID + '&invoiceID=' + params.invoiceID;
            return httpServices.get(url);
        };

        PRMPOService.getASNDetails = function (params) {
            let url = PRMPOServiceDomain + 'getasndetails?compid=' + params.compid + '&asnid=' + params.asnid + '&ponumber=' + params.ponumber + '&grncode=' + params.grncode + '&asncode=' + params.asncode + '&sessionid=' + userService.getUserToken() + '&vendorid=' + params.vendorid;
            return httpServices.get(url);
        };

        PRMPOService.getThreeWayMatchingList = function (params) {
            let url = PRMPOServiceDomain + 'getthreewaymatchinglist?compid=' + params.compid + '&userId=' + params.userId + '&search=' + params.search +
                '&fromDate=' + params.fromDate + '&toDate=' + params.toDate  + '&PageSize=' + params.PageSize + '&NumberOfRecords=' + params.NumberOfRecords + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPOService.getASNDetailsList = function (params) {
            let url = PRMPOServiceDomain + 'getasndetailslist?ponumber=' + params.ponumber + '&sessionid=' + userService.getUserToken() + '&vendorid=' + params.vendorid;
            return httpServices.get(url);
        };
        PRMPOService.getPODetails = function (params) {
            let url = PRMPOServiceDomain + 'getpodetails?ponumber=' + params.ponumber + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPOService.saveASNdetails = function (params) {
            let url = PRMPOServiceDomain + 'saveasndetails';
            return httpServices.post(url, params);
        };

        PRMPOService.savePOInvoice = function (params) {
            let url = PRMPOServiceDomain + 'savepoinvoice';
            return httpServices.post(url, params);
        };

        PRMPOService.DeletePODetails = function (params) {
            let url = PRMPOServiceDomain + 'deletePODetails';
            return httpServices.post(url, params);
        };

        PRMPOService.DeleteInvoice = function (params) {
            let url = PRMPOServiceDomain + 'deleteInvoice';
            return httpServices.post(url, params);
        };


        PRMPOService.getPaymentInvoiceDetails = function (params) {
            let url = PRMPOServiceDomain + 'getPaymentInvoiceDetails?sessionid=' + params.sessionID + '&compId=' + params.compId + '&uId=' + params.uId;
            return httpServices.get(url);
        };

        PRMPOService.savePOInvoiceForm = function (params) {
            let url = PRMPOServiceDomain + 'savepoinvoiceform';
            return httpServices.post(url, params);
        };

        PRMPOService.getInvoiceList = function (params) {
            let url = PRMPOServiceDomain + 'getinvoicelist?compid=' + params.COMP_ID + '&userid=' + params.U_ID + '&sessionid=' + userService.getUserToken() + '&fromDate=' + params.fromDate + '&toDate=' + params.toDate + '&ponumber=' + params.PO_NUMBER + '&invoicenumber=' + params.INVOICE_NUMBER;
            return httpServices.get(url);
        };

        PRMPOService.getInvoiceHeaderLevelList = function (params) {
            let url = PRMPOServiceDomain + 'getInvoiceHeaderLevelList?compid=' + params.compID + '&userid=' + params.uID + '&sessionid=' + userService.getUserToken() + '&fromDate=' + params.fromDate + '&toDate=' + params.toDate + '&search=' + params.search;
            return httpServices.get(url);
        };

        PRMPOService.savepoattachments = function (params) {
            let url = PRMPOServiceDomain + 'savepoattachments';
            return httpServices.post(url, params);
        };


        PRMPOService.poApproval = function (params) {
            let url = PRMPOServiceDomain + 'poapproval';
            return httpServices.post(url, params);
        };

        PRMPOService.deletePOInvoice = function (params) {
            if (!params.invoiceId) {
                params.invoiceId = 0;
            }
            if (!params.wfId) {
                params.wfId = 0;
            }
            let url = PRMPOServiceDomain + 'deletepoinvoice?ponumber=' + params.ponumber + '&invoicenumber=' + params.invoicenumber + '&invoiceId=' + params.invoiceId + '&wfId=' + params.wfId + '&sessionid=' + params.sessionid;
            return httpServices.get(url, params);
        };


        PRMPOService.sendForIntegration = function (params) {
            let url = PRMPOServiceDomain + 'sendforintegration?ponumber=' + params.ponumber + '&sessionid=' + params.sessionid;
            return httpServices.get(url, params);
        };

        PRMPOService.sendForInvoiceIntegration = function (params) {
            let url = PRMPOServiceDomain + 'sendforinvoiceintegration?invoicenumber=' + params.invoicenumber + '&sessionid=' + params.sessionid;
            return httpServices.get(url, params);
        };

        PRMPOService.rejectPODetails = function (params) {
            let url = PRMPOServiceDomain + 'rejectPODetails?poNumber=' + params.poNumber + '&status=' + params.status + '&sessionId=' + params.sessionId;
            return httpServices.get(url, params);
        };

        PRMPOService.GeneratePOPDF = function (params) {
            let url = PRMPOServiceDomain + 'generatepopdf?ponumber=' + params + '&sessionid=' + userService.getUserToken();
            let data = httpServices.get(url).then(function (response) {
                if (response) {
                    response = b64toBlob(response, 'application/octet-stream');
                    var linkElement = document.createElement('a');
                    try {
                        var url = $window.URL.createObjectURL(response);
                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute("download", params + ".pdf");

                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    }
                    catch (ex) { }
                    return response;
                }
            }, function (result) {
                //console.log("date error");
            });
        };

        function b64toBlob(b64Data, contentType, sliceSize) {
            contentType = contentType || '';
            sliceSize = sliceSize || 512;

            var byteCharacters = atob(b64Data);
            var byteArrays = [];

            for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                var slice = byteCharacters.slice(offset, offset + sliceSize);

                var byteNumbers = new Array(slice.length);
                for (var i = 0; i < slice.length; i++) {
                    byteNumbers[i] = slice.charCodeAt(i);
                }

                var byteArray = new Uint8Array(byteNumbers);

                byteArrays.push(byteArray);
            }

            var blob = new Blob(byteArrays, { type: contentType });
            return blob;
        }

        return PRMPOService;

    }]);