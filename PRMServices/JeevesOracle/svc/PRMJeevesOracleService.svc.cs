﻿using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using PdfSharp.Pdf;
using PRM.Core.Storages;
using PRMServices.Common;
using PRMServices.Models;
using PRMServices.Models.Vendor;
using PRMServices.SignalR;
using PRMServices.SQLHelper;
using GRID = SendGrid;
using GRID_EMAIL = SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.ServiceModel.Activation;
using System.Threading.Tasks;
using System.Web;
using TheArtOfDev.HtmlRenderer.PdfSharp;
//using PRM.Core.Models.Reports;
using CORE = PRM.Core.Common;
using CATALOG = PRMServices.Models.Catalog;
using System.Text.RegularExpressions;
using System.Net.Http;
using System.IdentityModel.Tokens.Jwt;

using System.Timers;
using SAPFTPIntegration;
using System.Data.SqlClient;
using System.Text;

namespace PRMServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMJeevesOracleService : IPRMJeevesOracleService
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
        public PRMServices prm = new PRMServices();


        public UserToken GetUserToken()
        {
            UserToken userToken = new UserToken();

            var request = new System.Web.HttpContextWrapper(System.Web.HttpContext.Current);
            string authHeader = request.Request.Headers["Authorization"];
            logger.Debug("header" + authHeader);
            if (authHeader != null && authHeader.StartsWith("Basic"))
            {
                string encodedUsernamePassword = authHeader.Substring("Basic".Length).Trim();
                System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("iso-8859-1");
                string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));

                int seperatorIndex = usernamePassword.IndexOf(':');

                var username = usernamePassword.Substring(0, seperatorIndex);
                var password = usernamePassword.Substring(seperatorIndex + 1);
                string token = prm.LoginUserToken(username, password);

                userToken.JWTToken = token;
                userToken.Message = "You are successfully connected to PRM.";
                userToken.ExpiresIn = 1440;
                return userToken;

            }

            else
            {
                //Handle what happens if that isn't the case
                throw new Exception("The authorization header is either empty or isn't Basic.");
                return null;
            }


        }
        public UserToken GetJeevesToken()
        {

            var url1 = ConfigurationManager.AppSettings["JEEVES_CLIENT_URL"];

            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                   | SecurityProtocolType.Tls11
                   | SecurityProtocolType.Tls12
                   | SecurityProtocolType.Ssl3;
            var httpRequest = (HttpWebRequest)WebRequest.Create(url1);

            httpRequest.Method = "POST";
            httpRequest.Timeout = -1;
            httpRequest.ContentType = "application/x-www-form-urlencoded";
            httpRequest.Headers.Add("Cookie", "CSRF-TOKEN=c5522b09-0fe7-488a-b936-f55240050460");

            //string postData = "";
            //if (ConfigurationManager.AppSettings["JEEVES_URL"] == "UAT")
            //{
            //    postData = "client_id=prm_jeeves&client_secret=w73fWQMAT9ksMj5pO2BAxzO42vo2gXPSULDr3mQhY4HEUQhS&grant_type=client_credentials&target_client_id=jeeves";
            //}
            //byte[] data = Encoding.ASCII.GetBytes(postData);
            //httpRequest.ContentLength = data.Length;
            //char[] chars = new char[] { };
            using (var streamWriter = new StreamWriter(httpRequest.GetRequestStream()))
            {

                string postData = "client_id=" + ConfigurationManager.AppSettings["JEEVES_CLIENT_ID"] + "&" +
                              "client_secret=" + ConfigurationManager.AppSettings["JEEVES_CLIENT_SECRET"] + "&" +
                              "grant_type=" + ConfigurationManager.AppSettings["JEEVES_GRANT_TYPE"] + "&" +
                              "target_client_id=" + ConfigurationManager.AppSettings["JEEVES_TARGET_CLIENT_ID"];
                streamWriter.Write(postData);
            }
            var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                UserToken userToken = JsonConvert.DeserializeObject<UserToken>(result);
                var status = (int)httpResponse.StatusCode;
                userToken.Status = status.ToString();
                return userToken;
            }

        }
        public UserToken GetF1Token()
        {

            var url1 = ConfigurationManager.AppSettings["F1_CLIENT_URL"];

            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                   | SecurityProtocolType.Tls11
                   | SecurityProtocolType.Tls12
                   | SecurityProtocolType.Ssl3;
            var httpRequest = (HttpWebRequest)WebRequest.Create(url1);

            httpRequest.Method = "POST";
            httpRequest.Timeout = -1;
            httpRequest.ContentType = "application/json";
            string json = "{\"userName\":\"" + ConfigurationManager.AppSettings["F1_USERNAME"] + "\"," +
                              "\"userPassword\":\"" + ConfigurationManager.AppSettings["F1_USERPASSWORD"] + "\"}";

            byte[] data = Encoding.ASCII.GetBytes(json);
            httpRequest.ContentLength = data.Length;
            using (var streamWriter = new StreamWriter(httpRequest.GetRequestStream()))
            {
                streamWriter.Write(json);
            }

            var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                UserToken userToken = JsonConvert.DeserializeObject<UserToken>(result);
                return userToken;
            }


        }
        public UserToken JeevesRequirementSave(List<JeevesRequirement> jeevesRequirement)
        {
            //validate authorization
            var request = new System.Web.HttpContextWrapper(System.Web.HttpContext.Current);
            var apiKey = request.Request.Headers.GetValues("jwtToken");
            var validateToken = AuthenticateJwt.GetPrincipal(apiKey[0]);

            UserToken userToken = new UserToken();
            try
            {

                PRMUploadService prmUploadService = new PRMUploadService();
                DataTable dt = new DataTable();
                DataTable tempDT1 = new DataTable();
                tempDT1.Clear();
                List<string> columns = "ENTITY;TITLE;UOM;CASE_ID;TYPE_OF_STOCK;REQUIRED_QUANTITY;PRODUCT_CODE;MODEL_NAME;BRAND_PART_CODE;BRAND;BRAND_PRODUCT_CATEGORY_CODE;DELIVERY_LOCATION;PAYMENT_TERMS;CONTACT_DETAILS;URGENCY;CURRENCY;IS_PROCESSED;ROW_ID;FROM_INTEGRATION".Split(';').ToList();
                foreach (var column in columns)
                {
                    tempDT1.Columns.Add(column);
                }

                foreach (var x in jeevesRequirement)
                {
                    foreach (var y in x.requirement_line_items)
                    {
                        var dr = tempDT1.NewRow(); // have new row on each iteration
                        dr["PRODUCT_CODE"] = y.product_code;
                        dr["MODEL_NAME"] = y.model_name;
                        dr["ENTITY"] = x.entity;
                        dr["TITLE"] = x.title;
                        dr["UOM"] = x.uom;
                        dr["CASE_ID"] = x.case_id;
                        dr["TYPE_OF_STOCK"] = x.type_of_stock;
                        dr["REQUIRED_QUANTITY"] = y.required_quantity;
                        dr["BRAND_PART_CODE"] = x.brand_part_code;
                        dr["BRAND"] = x.brand;
                        dr["BRAND_PRODUCT_CATEGORY_CODE"] = x.brand_product_category_code;
                        dr["DELIVERY_LOCATION"] = x.delivery_location;
                        dr["PAYMENT_TERMS"] = x.payment_terms;
                        dr["CONTACT_DETAILS"] = x.contact_details;
                        dr["URGENCY"] = x.urgency;
                        dr["CURRENCY"] = x.currency;
                        tempDT1.Rows.Add(dr);
                    }
                }

                string json = JsonConvert.SerializeObject(tempDT1);
                logger.Debug("SAP REQUIREMENTS:" + JsonConvert.SerializeObject(tempDT1));


                dt = (DataTable)JsonConvert.DeserializeObject(json, (typeof(DataTable)));


                if (validateToken != null && validateToken.Identity.IsAuthenticated)
                {
                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        var prChunks = dt.AsEnumerable().ToList().Chunk(10000);
                        foreach (var chunk in prChunks)
                        {

                            DataTable tempDT = new DataTable();
                            tempDT.Clear();
                            tempDT.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                            foreach (var column in columns)
                            {
                                tempDT.Columns.Add(column);
                            }

                            tempDT.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                            List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                            foreach (DataColumn col in tempDT.Columns)
                            {
                                columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                            }

                            foreach (var row in chunk)
                            {
                                var newRow = tempDT.NewRow();
                                newRow["JOB_ID"] = guid1;
                                newRow["ENTITY"] = GetDataRowVaue(row, "ENTITY");
                                newRow["TITLE"] = GetDataRowVaue(row, "TITLE");
                                newRow["UOM"] = GetDataRowVaue(row, "UOM");
                                newRow["CASE_ID"] = GetDataRowVaue(row, "CASE_ID");
                                newRow["TYPE_OF_STOCK"] = GetDataRowVaue(row, "TYPE_OF_STOCK");
                                string qtyReq = !string.IsNullOrEmpty(GetDataRowVaue(row, "REQUIRED_QUANTITY")) ? GetDataRowVaue(row, "REQUIRED_QUANTITY").Replace(",", "") : "0";
                                newRow["REQUIRED_QUANTITY"] = qtyReq;
                                newRow["PRODUCT_CODE"] = GetDataRowVaue(row, "PRODUCT_CODE");
                                newRow["MODEL_NAME"] = GetDataRowVaue(row, "MODEL_NAME");
                                newRow["BRAND_PART_CODE"] = GetDataRowVaue(row, "BRAND_PART_CODE");
                                newRow["BRAND"] = GetDataRowVaue(row, "BRAND");
                                newRow["BRAND_PRODUCT_CATEGORY_CODE"] = GetDataRowVaue(row, "BRAND_PRODUCT_CATEGORY_CODE");
                                newRow["DELIVERY_LOCATION"] = GetDataRowVaue(row, "DELIVERY_LOCATION");
                                newRow["PAYMENT_TERMS"] = GetDataRowVaue(row, "PAYMENT_TERMS");
                                newRow["CONTACT_DETAILS"] = GetDataRowVaue(row, "CONTACT_DETAILS");
                                newRow["URGENCY"] = GetDataRowVaue(row, "URGENCY");
                                newRow["CURRENCY"] = GetDataRowVaue(row, "CURRENCY");
                                newRow["IS_PROCESSED"] = 0;
                                newRow["DATE_CREATED"] = DateTime.UtcNow;
                                newRow["ROW_ID"] = tempDT.Rows.Count + 1;
                                newRow["FROM_INTEGRATION"] = 1;

                                tempDT.Rows.Add(newRow);
                            }
                            bizClass.BulkInsert(tempDT, "[dbo].[SAP_REQUIREMENT_DETAILS]", columnMappings);
                            tempDT.Rows.Clear();
                        }

                    }

                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_JOB_ID", guid);
                    bizClass.SelectList("erp_process_sap_demand_request_details", sd);
                    //ERROR MESSAGES
                    string query = $@"select * from UPLOAD_MODULE_ERROR_DETAILS where job_id = '{guid}'";
                    List<ModuleBasedError> details = new List<ModuleBasedError>();
                    CORE.DataNamesMapper<ModuleBasedError> mapper = new CORE.DataNamesMapper<ModuleBasedError>();
                    var dataTable = sqlHelper.SelectQuery(query);
                    details = mapper.Map(dataTable).ToList();
                    string combindedString = string.Join(";", details.Select(t => t.REASON.Remove(t.REASON.Length - 1)).ToArray());

                    if (details.Count > 0)
                    {
                        userToken.Message = combindedString;
                        userToken.Success = false;
                        //if (userToken.Message.Contains("Product Code is empty") || userToken.Message.Contains("Product Code with model name is not present"))
                        //{
                        //    userToken.ErrorCode = 411;
                        //}
                        bool isValid = (userToken.Message.Any("Product Code is empty".Contains) || userToken.Message.Contains("Product Code with model name is not present"));
                        if (isValid)
                        {
                            userToken.ErrorCode = 411;
                        }
                    }
                    else
                    {
                        userToken.Message = "Saved Successfully";
                    }
                    //userToken.Message = "Requirements Saved Successfully.";
                }
                else
                {
                    userToken.Message = "You are Unauthorized to connect PRM.";
                    userToken.Success = false;
                }
            }
            catch (Exception ex)
            {
                userToken.ErrorMessage = ex.Message;
                userToken.Message = ex.Message;
                userToken.Success = false;
            }

            return userToken;
        }

        public UserToken JeevesProductSave(List<JeevesProduct> jeevesProduct)
        {
            //authorization
            var request = new System.Web.HttpContextWrapper(System.Web.HttpContext.Current);
            var apiKey = request.Request.Headers.GetValues("jwtToken");
            var validateToken = AuthenticateJwt.GetPrincipal(apiKey[0]);


            UserToken userToken = new UserToken();

            try
            {

                PRMUploadService prmUploadService = new PRMUploadService();
                DataTable dt = new DataTable();
                DataTable tempDT1 = new DataTable();
                tempDT1.Clear();
                List<string> columns = "PRODUCT_CODE;EPN;PRODUCT_NAME;PRODUCT_DESCRIPTION;CATEGORY_NAME;SERVICE_TYPE;SUPER_CATEGORY;SUB_CATEGORY;ITEM_REG_STATUS;MODEL_ID;MODEL_NAME;BRAND_PART_CODE;BRAND;BRAND_PRODUCT_CATEGORY_cODE;JSN;FSN;HSN_CODE;IMEI_NO;ESN_NO;ITEM_SERIALLZED_STATUS;CUSTOMER_PRICE;SF_PRICE;LANDING_PRICE;STOCK_TRANSPORTER_PRICE;HANDLING_CHARGES;ITEM_WEIGHT;UOM;WARRANTY;PRODUCT_LIFE;WARRANTY_COVERAGE;IS_CONSUMABLE;IS_CHARGABLE;IS_RETURNABLE;IS_HIGHVALUE;IS_SERIALIZED;IS_REPLACEMENT;IS_ADVANCE_REPLACEMENT;REPAIR_LEVEL;PRIORITY_LEVEL;DISCOUNT;CONDITION;ENTITY;SUB_ENTITY;VERTICAL;ROW_ID;FROM_INTEGRATION".Split(';').ToList();
                foreach (var column in columns)
                {
                    tempDT1.Columns.Add(column);
                }

                foreach (var x in jeevesProduct)
                {
                    var dr = tempDT1.NewRow(); // have new row on each iteration
                    dr["PRODUCT_CODE"] = x.product_code;
                    dr["EPN"] = x.epn;
                    dr["PRODUCT_NAME"] = x.product_name;
                    dr["PRODUCT_DESCRIPTION"] = x.product_description;
                    dr["CATEGORY_NAME"] = x.category_name;
                    dr["SERVICE_TYPE"] = x.service_type;
                    dr["SUPER_CATEGORY"] = x.super_category;
                    dr["SUB_CATEGORY"] = x.sub_category;
                    dr["ITEM_REG_STATUS"] = x.item_reg_status;
                    dr["MODEL_ID"] = x.model_id;
                    dr["MODEL_NAME"] = x.model_name;
                    dr["BRAND_PART_CODE"] = x.brand_part_code;
                    dr["BRAND"] = x.brand;
                    dr["BRAND_PRODUCT_CATEGORY_cODE"] = x.brand_product_category_code;
                    dr["JSN"] = x.jsn;
                    dr["FSN"] = x.fsn;
                    dr["HSN_CODE"] = x.hsn_code;
                    dr["IMEI_NO"] = x.imei_no;
                    dr["ESN_NO"] = x.esn_no;
                    dr["ITEM_SERIALLZED_STATUS"] = x.item_serialized_status;
                    dr["CUSTOMER_PRICE"] = x.customer_price;
                    dr["SF_PRICE"] = x.sf_price;
                    dr["LANDING_PRICE"] = x.landing_price;
                    dr["STOCK_TRANSPORTER_PRICE"] = x.stock_transporter_price;
                    dr["HANDLING_CHARGES"] = x.handling_charges;
                    dr["ITEM_WEIGHT"] = x.item_weight;
                    dr["UOM"] = x.uom;
                    dr["WARRANTY"] = x.warranty;
                    dr["PRODUCT_LIFE"] = x.product_life;
                    dr["WARRANTY_COVERAGE"] = x.warranty_coverage;
                    dr["IS_CONSUMABLE"] = x.is_consumable;
                    dr["IS_CHARGABLE"] = x.is_chargable;
                    dr["IS_RETURNABLE"] = x.is_returnable;
                    dr["IS_HIGHVALUE"] = x.is_highvalue;
                    dr["IS_SERIALIZED"] = x.is_serialized;
                    dr["IS_REPLACEMENT"] = x.is_replacement;
                    dr["IS_ADVANCE_REPLACEMENT"] = x.is_advance_replacement;
                    dr["REPAIR_LEVEL"] = x.repair_level;
                    dr["PRIORITY_LEVEL"] = x.priority_level;
                    dr["DISCOUNT"] = x.discount;
                    dr["CONDITION"] = x.condition;
                    dr["ENTITY"] = x.entity;
                    dr["SUB_ENTITY"] = x.sub_entity;
                    dr["VERTICAL"] = x.vertical;
                    tempDT1.Rows.Add(dr);
                }

                string json = JsonConvert.SerializeObject(tempDT1);
                logger.Debug("SAP REQUIREMENTS:" + JsonConvert.SerializeObject(tempDT1));


                dt = (DataTable)JsonConvert.DeserializeObject(json, (typeof(DataTable)));


                if (validateToken != null && validateToken.Identity.IsAuthenticated)
                {
                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        var prChunks = dt.AsEnumerable().ToList().Chunk(10000);
                        foreach (var chunk in prChunks)
                        {

                            DataTable tempDT = new DataTable();
                            tempDT.Clear();
                            tempDT.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                            foreach (var column in columns)
                            {
                                tempDT.Columns.Add(column);
                            }
                            tempDT.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));


                            List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                            foreach (DataColumn col in tempDT.Columns)
                            {
                                columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                            }


                            foreach (var row in chunk)
                            {
                                var newRow = tempDT.NewRow();
                                newRow["JOB_ID"] = guid1;
                                newRow["PRODUCT_CODE"] = GetDataRowVaue(row, "PRODUCT_CODE");
                                newRow["EPN"] = GetDataRowVaue(row, "EPN");
                                newRow["PRODUCT_NAME"] = GetDataRowVaue(row, "PRODUCT_NAME");
                                newRow["PRODUCT_DESCRIPTION"] = GetDataRowVaue(row, "PRODUCT_DESCRIPTION");
                                newRow["CATEGORY_NAME"] = GetDataRowVaue(row, "CATEGORY_NAME");
                                newRow["SERVICE_TYPE"] = GetDataRowVaue(row, "SERVICE_TYPE");
                                newRow["SUPER_CATEGORY"] = GetDataRowVaue(row, "SUPER_CATEGORY");
                                newRow["SUB_CATEGORY"] = GetDataRowVaue(row, "SUB_CATEGORY");
                                newRow["ITEM_REG_STATUS"] = GetDataRowVaue(row, "ITEM_REG_STATUS");
                                newRow["MODEL_ID"] = GetDataRowVaue(row, "MODEL_ID");
                                newRow["MODEL_NAME"] = GetDataRowVaue(row, "MODEL_NAME");
                                newRow["BRAND_PART_CODE"] = GetDataRowVaue(row, "BRAND_PART_CODE");
                                newRow["BRAND"] = GetDataRowVaue(row, "BRAND");
                                newRow["BRAND_PRODUCT_CATEGORY_cODE"] = GetDataRowVaue(row, "BRAND_PRODUCT_CATEGORY_cODE");
                                newRow["JSN"] = GetDataRowVaue(row, "JSN");
                                newRow["FSN"] = GetDataRowVaue(row, "FSN");
                                newRow["HSN_CODE"] = GetDataRowVaue(row, "HSN_CODE");
                                newRow["IMEI_NO"] = GetDataRowVaue(row, "IMEI_NO");
                                newRow["ESN_NO"] = GetDataRowVaue(row, "ESN_NO");
                                newRow["ITEM_SERIALLZED_STATUS"] = GetDataRowVaue(row, "ITEM_SERIALLZED_STATUS");
                                newRow["CUSTOMER_PRICE"] = GetDataRowVaue(row, "CUSTOMER_PRICE");
                                newRow["SF_PRICE"] = GetDataRowVaue(row, "SF_PRICE");
                                newRow["LANDING_PRICE"] = GetDataRowVaue(row, "LANDING_PRICE");
                                newRow["STOCK_TRANSPORTER_PRICE"] = GetDataRowVaue(row, "STOCK_TRANSPORTER_PRICE");
                                newRow["HANDLING_CHARGES"] = GetDataRowVaue(row, "HANDLING_CHARGES");
                                newRow["ITEM_WEIGHT"] = GetDataRowVaue(row, "ITEM_WEIGHT");
                                newRow["UOM"] = GetDataRowVaue(row, "UOM");
                                newRow["WARRANTY"] = GetDataRowVaue(row, "WARRANTY");
                                newRow["PRODUCT_LIFE"] = GetDataRowVaue(row, "PRODUCT_LIFE");
                                newRow["WARRANTY_COVERAGE"] = GetDataRowVaue(row, "WARRANTY_COVERAGE");
                                newRow["IS_CONSUMABLE"] = GetDataRowVaue(row, "IS_CONSUMABLE");
                                newRow["IS_CHARGABLE"] = GetDataRowVaue(row, "IS_CHARGABLE");
                                newRow["IS_RETURNABLE"] = GetDataRowVaue(row, "IS_RETURNABLE");
                                newRow["IS_HIGHVALUE"] = GetDataRowVaue(row, "IS_HIGHVALUE");
                                newRow["IS_SERIALIZED"] = GetDataRowVaue(row, "IS_SERIALIZED");
                                newRow["IS_REPLACEMENT"] = GetDataRowVaue(row, "IS_REPLACEMENT");
                                newRow["IS_ADVANCE_REPLACEMENT"] = GetDataRowVaue(row, "IS_ADVANCE_REPLACEMENT");
                                newRow["REPAIR_LEVEL"] = GetDataRowVaue(row, "REPAIR_LEVEL");
                                newRow["PRIORITY_LEVEL"] = GetDataRowVaue(row, "PRIORITY_LEVEL");
                                newRow["DISCOUNT"] = GetDataRowVaue(row, "DISCOUNT");
                                newRow["CONDITION"] = GetDataRowVaue(row, "CONDITION");
                                newRow["ENTITY"] = "Jeeves";
                                newRow["VERTICAL"] = GetDataRowVaue(row, "VERTICAL");
                                newRow["SUB_ENTITY"] = GetDataRowVaue(row, "SUB_ENTITY");
                                newRow["DATE_CREATED"] = DateTime.UtcNow;
                                newRow["ROW_ID"] = tempDT.Rows.Count + 1;
                                newRow["FROM_INTEGRATION"] = 1;

                                tempDT.Rows.Add(newRow);
                            }

                            bizClass.BulkInsert(tempDT, "[dbo].[SAP_MATERIAL_DETAILS]", columnMappings);
                            tempDT.Rows.Clear();


                        }

                    }

                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_JOB_ID", guid);
                    bizClass.SelectList("erp_process_sap_material_details", sd);
                    //ERROR MESSAGES
                    string query = $@"select * from UPLOAD_MODULE_ERROR_DETAILS where job_id = '{guid}'";
                    List<ModuleBasedError> details = new List<ModuleBasedError>();
                    CORE.DataNamesMapper<ModuleBasedError> mapper = new CORE.DataNamesMapper<ModuleBasedError>();
                    var dataTable = sqlHelper.SelectQuery(query);
                    details = mapper.Map(dataTable).ToList();
                    string combindedString = string.Join(";", details.Select(t => t.REASON.Remove(t.REASON.Length - 1)).ToArray());

                    if (details.Count > 0)
                    {
                        userToken.Message = combindedString;
                        userToken.Success = false;
                    }
                    else
                    {
                        userToken.Message = "Saved Successfully";
                    }
                    //userToken.Message = "Products Saved Successfully.";
                }
                else
                {
                    userToken.Message = "You are Unauthorized to connect PRM.";
                    userToken.Success = false;
                }
            }
            catch (Exception ex)
            {
                userToken.ErrorMessage = ex.Message;
                userToken.Message = ex.Message;
                userToken.Success = false;
            }

            return userToken;
        }

        public UserToken JeevesProductBOMSave(List<JeevesProduct> jeevesProduct)
        {
            //authorization
            var request = new System.Web.HttpContextWrapper(System.Web.HttpContext.Current);
            var apiKey = request.Request.Headers.GetValues("jwtToken");
            var validateToken = AuthenticateJwt.GetPrincipal(apiKey[0]);


            UserToken userToken = new UserToken();

            try
            {

                PRMUploadService prmUploadService = new PRMUploadService();
                DataTable dt = new DataTable();
                DataTable tempDT1 = new DataTable();
                tempDT1.Clear();
                List<string> columns = "PRODUCT_CODE;MODEL_NAME;ENTITY".Split(';').ToList();
                foreach (var column in columns)
                {
                    tempDT1.Columns.Add(column);
                }

                foreach (var x in jeevesProduct)
                {
                    var dr = tempDT1.NewRow(); // have new row on each iteration
                    dr["PRODUCT_CODE"] = x.product_code;
                    dr["MODEL_NAME"] = x.model_name;
                    dr["ENTITY"] = "Jeeves";
                    tempDT1.Rows.Add(dr);
                }

                string json = JsonConvert.SerializeObject(tempDT1);
                logger.Debug("SAP REQUIREMENTS:" + JsonConvert.SerializeObject(tempDT1));


                dt = (DataTable)JsonConvert.DeserializeObject(json, (typeof(DataTable)));


                if (validateToken != null && validateToken.Identity.IsAuthenticated)
                {
                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        var prChunks = dt.AsEnumerable().ToList().Chunk(10000);
                        foreach (var chunk in prChunks)
                        {

                            DataTable tempDT = new DataTable();
                            tempDT.Clear();
                            tempDT.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                            foreach (var column in columns)
                            {
                                tempDT.Columns.Add(column);
                            }
                            tempDT.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));


                            List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                            foreach (DataColumn col in tempDT.Columns)
                            {
                                columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                            }


                            foreach (var row in chunk)
                            {
                                var newRow = tempDT.NewRow();
                                newRow["JOB_ID"] = guid1;
                                newRow["PRODUCT_CODE"] = GetDataRowVaue(row, "PRODUCT_CODE");
                                newRow["MODEL_NAME"] = GetDataRowVaue(row, "MODEL_NAME");
                                newRow["ENTITY"] = GetDataRowVaue(row, "ENTITY");
                                newRow["DATE_CREATED"] = DateTime.UtcNow;


                                tempDT.Rows.Add(newRow);
                            }

                            bizClass.BulkInsert(tempDT, "[dbo].[SAP_MATERIAL_DETAILS]", columnMappings);
                            tempDT.Rows.Clear();


                        }

                    }

                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_JOB_ID", guid);
                    bizClass.SelectList("erp_process_sap_update_material_details", sd);
                    userToken.Message = "Products Saved Successfully.";
                }
                else
                {
                    userToken.Message = "You are Unauthorized to connect PRM.";
                    userToken.Success = false;
                }
            }
            catch (Exception ex)
            {
                userToken.ErrorMessage = ex.Message;
                userToken.Message = ex.Message;
                userToken.Success = false;
            }

            return userToken;
        }


        public UserToken JeevesGRNSave(List<JeevesGRN> jeevesGRN)
        {
            //authorization
            var request = new System.Web.HttpContextWrapper(System.Web.HttpContext.Current);
            var apiKey = request.Request.Headers.GetValues("jwtToken");
            var validateToken = AuthenticateJwt.GetPrincipal(apiKey[0]);


            UserToken userToken = new UserToken();

            try
            {

                PRMUploadService prmUploadService = new PRMUploadService();
                DataTable dt = new DataTable();
                DataTable tempDT1 = new DataTable();
                tempDT1.Clear();
                List<string> columns1 = "PO_NUMBER;PO_LINE_ITEM;TYPE_OF_STOCK;GRN_NO;GRN_DATE;GRN_STATUS;INVOICE_DOC;FOC_REQ_NUM;SHIPPED_LOCATION_CODE;VENDOR_SITE_CODE;INVOICE_NO;INVOICE_TYPE;INVOICE_DATE;INVOICE_AMOUNT;PO_LINE_ITEM_DESC;PO_LINE_ITEM_SKU_ID;PO_QTY;RECEIVED_QTY;LOST_QTY;BAD_QTY".Split(';').ToList();

                List<string> columns = "EBELN;EBELP;TYPE_OF_STOCK;MBLNR;CPUDT_MKPF;GRN_STATUS;INVOICE_DOC;FOC_REQ_NUM;LIFNR;VENDOR_SITE_CODE;INVOICE_NO;INVOICE_TYPE;INVOICE_DATE;INVOICE_AMOUNT;MAKTX;MATNR;PO_QTY;MENGE;LOST_QTY;BAD_QTY;ZEILE;ROW_ID".Split(';').ToList();
                foreach (var column in columns1)
                {
                    tempDT1.Columns.Add(column);
                }

                foreach (var x in jeevesGRN)
                {
                    foreach (var y in x.po_line_items)
                    {
                        Response resp = new Response();
                        string attachIds = string.Empty;
                        if (!string.IsNullOrEmpty(x.invoice_doc))
                        {
                            WebClient myWebClient = new WebClient();
                            long tick = DateTime.Now.Ticks;
                            var fileName = System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "INVOICE_GRN_FILE_" + tick + ".pdf");
                            logger.Debug("url>>>" + fileName);
                            myWebClient.DownloadFile(new Uri(x.invoice_doc), fileName);
                            fileName = "INVOICE_GRN_FILE_" + tick + ".pdf";
                            resp = prm.SaveAttachment(fileName);
                            attachIds += Convert.ToString(resp.ObjectID) + ",";
                            attachIds = attachIds.Substring(0, attachIds.Length - 1);
                        }


                        var dr = tempDT1.NewRow(); // have new row on each iteration
                        dr["PO_LINE_ITEM_DESC"] = y.po_line_item_desc;
                        dr["PO_LINE_ITEM_SKU_ID"] = y.po_line_item_sku_id;
                        dr["PO_QTY"] = y.po_qty;
                        dr["RECEIVED_QTY"] = y.received_qty;
                        dr["LOST_QTY"] = y.lost_qty;
                        dr["BAD_QTY"] = y.bad_qty;
                        dr["PO_NUMBER"] = x.po_number;
                        dr["PO_LINE_ITEM"] = genLineItemNum(x.po_line_items.IndexOf(y) + 1);
                        dr["TYPE_OF_STOCK"] = x.type_of_stock;
                        dr["GRN_NO"] = x.grn_no;
                        dr["GRN_DATE"] = x.grn_date;
                        dr["GRN_STATUS"] = x.grn_status;
                        dr["INVOICE_DOC"] = attachIds;
                        dr["FOC_REQ_NUM"] = x.foc_req_num;
                        dr["SHIPPED_LOCATION_CODE"] = x.shipped_location_code;
                        dr["VENDOR_SITE_CODE"] = x.vendor_site_code;
                        dr["INVOICE_NO"] = x.invoice_no;
                        dr["INVOICE_TYPE"] = x.invoice_type;
                        dr["INVOICE_DATE"] = x.invoice_date;
                        dr["INVOICE_AMOUNT"] = x.invoice_amount;
                        tempDT1.Rows.Add(dr);
                    }
                }

                string json = JsonConvert.SerializeObject(tempDT1);


                dt = (DataTable)JsonConvert.DeserializeObject(json, (typeof(DataTable)));


                if (validateToken != null && validateToken.Identity.IsAuthenticated)
                {
                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        var prChunks = dt.AsEnumerable().ToList().Chunk(10000);
                        foreach (var chunk in prChunks)
                        {

                            DataTable tempDT = new DataTable();
                            tempDT.Clear();
                            tempDT.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                            foreach (var column in columns)
                            {
                                tempDT.Columns.Add(column);
                            }

                            tempDT.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                            List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                            foreach (DataColumn col in tempDT.Columns)
                            {
                                columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                            }

                            foreach (var row in chunk)
                            {
                                var newRow = tempDT.NewRow();
                                newRow["JOB_ID"] = guid1;
                                newRow["EBELN"] = GetDataRowVaue(row, "PO_NUMBER");
                                newRow["EBELP"] = GetDataRowVaue(row, "PO_LINE_ITEM");
                                newRow["TYPE_OF_STOCK"] = GetDataRowVaue(row, "TYPE_OF_STOCK");
                                newRow["MBLNR"] = GetDataRowVaue(row, "GRN_NO");
                                newRow["GRN_STATUS"] = GetDataRowVaue(row, "GRN_STATUS");
                                newRow["INVOICE_DOC"] = GetDataRowVaue(row, "INVOICE_DOC");
                                newRow["FOC_REQ_NUM"] = GetDataRowVaue(row, "FOC_REQ_NUM");
                                newRow["LIFNR"] = GetDataRowVaue(row, "SHIPPED_LOCATION_CODE");
                                newRow["VENDOR_SITE_CODE"] = GetDataRowVaue(row, "VENDOR_SITE_CODE");
                                newRow["INVOICE_NO"] = GetDataRowVaue(row, "INVOICE_NO");
                                newRow["INVOICE_TYPE"] = GetDataRowVaue(row, "INVOICE_TYPE");
                                newRow["MAKTX"] = GetDataRowVaue(row, "PO_LINE_ITEM_DESC");
                                newRow["MATNR"] = GetDataRowVaue(row, "PO_LINE_ITEM_SKU_ID");
                                string invoiceAmount = !string.IsNullOrEmpty(GetDataRowVaue(row, "INVOICE_AMOUNT")) ? GetDataRowVaue(row, "INVOICE_AMOUNT").Replace(",", "") : "0";
                                newRow["INVOICE_AMOUNT"] = invoiceAmount;
                                string poQty = !string.IsNullOrEmpty(GetDataRowVaue(row, "PO_QTY")) ? GetDataRowVaue(row, "PO_QTY").Replace(",", "") : "0";
                                newRow["PO_QTY"] = poQty;
                                string receivedQty = !string.IsNullOrEmpty(GetDataRowVaue(row, "RECEIVED_QTY")) ? GetDataRowVaue(row, "RECEIVED_QTY").Replace(",", "") : "0";
                                newRow["MENGE"] = receivedQty;
                                string lostQty = !string.IsNullOrEmpty(GetDataRowVaue(row, "LOST_QTY")) ? GetDataRowVaue(row, "LOST_QTY").Replace(",", "") : "0";
                                newRow["LOST_QTY"] = lostQty;
                                string badQty = !string.IsNullOrEmpty(GetDataRowVaue(row, "BAD_QTY")) ? GetDataRowVaue(row, "BAD_QTY").Replace(",", "") : "0";
                                newRow["BAD_QTY"] = badQty;
                                string grnDate = GetDataRowVaue(row, "GRN_DATE");
                                newRow["CPUDT_MKPF"] = (grnDate == "00000000" || grnDate == "0" || grnDate == "") ? null : grnDate;
                                string invoiceDate = GetDataRowVaue(row, "INVOICE_DATE");
                                newRow["INVOICE_DATE"] = (invoiceDate == "00000000" || invoiceDate == "0" || invoiceDate == "") ? null : invoiceDate;
                                newRow["ZEILE"] = GetDataRowVaue(row, "PO_LINE_ITEM");
                                newRow["DATE_CREATED"] = DateTime.UtcNow;
                                newRow["ROW_ID"] = tempDT.Rows.Count + 1;


                                tempDT.Rows.Add(newRow);
                            }
                            bizClass.BulkInsert(tempDT, "[dbo].[SAP_GRN_DETAILS]", columnMappings);
                            tempDT.Rows.Clear();
                        }

                    }

                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_JOB_ID", guid);
                    bizClass.SelectList("erp_process_sap_grn_details", sd);
                    //ERROR MESSAGES
                    string query = $@"select * from UPLOAD_MODULE_ERROR_DETAILS where job_id = '{guid}'";
                    List<ModuleBasedError> details = new List<ModuleBasedError>();
                    CORE.DataNamesMapper<ModuleBasedError> mapper = new CORE.DataNamesMapper<ModuleBasedError>();
                    var dataTable = sqlHelper.SelectQuery(query);
                    details = mapper.Map(dataTable).ToList();
                    string combindedString = string.Join(";", details.Select(t => t.REASON.Remove(t.REASON.Length - 1)).ToArray());

                    if (details.Count > 0)
                    {
                        userToken.Message = combindedString;
                        userToken.Success = false;
                    }
                    else
                    {
                        userToken.Message = "Saved Successfully";
                    }
                    
                    sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_JOB_ID", guid);
                    DataSet ds = sqlHelper.SelectList("cp_GetThreeWayMatchWorkflow", sd);
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        try
                        {
                            PRMWFService wf = new PRMWFService();
                            string sessionId = guid.ToString();
                            if (ds != null && ds.Tables[0].Rows.Count > 0) 
                            { 
                                int invCreatedBy = ds.Tables[0].Rows[0]["CREATED_BY"] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0]["CREATED_BY"]) : -1;
                                int wfID = ds.Tables[0].Rows[0]["WF_ID"] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0]["WF_ID"]) : -1;
                                int rowID = ds.Tables[0].Rows[0]["ROW_ID"] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0]["ROW_ID"]) : -1;

                                Utilities.CreateSession(sessionId, invCreatedBy, "WEB");
                                //foreach (DataRow row in ds.Tables[0].Rows)
                                //{
                                //    POInvoice inv = new POInvoice();
                                //    inv.INVOICE_ID = row["INVOICE_ID"] != DBNull.Value ? Convert.ToInt32(row["INVOICE_ID"]) : 0;
                                //    inv.WF_ID = row["WF_ID"] != DBNull.Value ? Convert.ToInt32(row["WF_ID"]) : 0;
                                //    inv.CreatedBy = row["CREATED_BY"] != DBNull.Value ? Convert.ToInt32(row["CREATED_BY"]) : 0;
                                //}
                                wf.AssignWorkflow(Convert.ToInt32(wfID), rowID, invCreatedBy, sessionId, "", 0);
                            }
                        }
                        catch (Exception ex) 
                        {
                            logger.Error("error while triggering workflow from GRN API for Jeeves>>>>"+ ex.Message);
                        }
                    }

                }
                else
                {
                    userToken.Message = "You are Unauthorized to connect PRM.";
                    userToken.Success = false;
                }
            }
            catch (Exception ex)
            {
                userToken.ErrorMessage = ex.Message;
                userToken.Message = ex.Message;
                userToken.Success = false;
            }

            return userToken;
        }

        public UserToken JeevesGRNReservedSave(List<JeevesGRNReserved> jeevesGRNReserved)
        {
            //authorization
            var request = new System.Web.HttpContextWrapper(System.Web.HttpContext.Current);
            var apiKey = request.Request.Headers.GetValues("jwtToken");
            var validateToken = AuthenticateJwt.GetPrincipal(apiKey[0]);


            UserToken userToken = new UserToken();

            try
            {

                PRMUploadService prmUploadService = new PRMUploadService();
                DataTable dt = new DataTable();
                DataTable tempDT1 = new DataTable();
                tempDT1.Clear();
                List<string> columns = "CASE_ID;PO_LINE_ITEM_SKU_ID;RESERVED_QTY;RESERVATION_DATE;RESERVATION_STATUS".Split(';').ToList();
                foreach (var column in columns)
                {
                    tempDT1.Columns.Add(column);
                }

                foreach (var x in jeevesGRNReserved)
                {
                    var dr = tempDT1.NewRow(); // have new row on each iteration
                    dr["CASE_ID"] = x.case_id;
                    dr["PO_LINE_ITEM_SKU_ID"] = x.po_line_item_sku_id;
                    dr["RESERVED_QTY"] = x.reserved_qty;
                    dr["RESERVATION_DATE"] = x.reservation_date;
                    dr["RESERVATION_STATUS"] = x.reservation_status;

                    tempDT1.Rows.Add(dr);
                }

                string json = JsonConvert.SerializeObject(tempDT1);
                logger.Debug("SAP GRN:" + JsonConvert.SerializeObject(tempDT1));


                dt = (DataTable)JsonConvert.DeserializeObject(json, (typeof(DataTable)));


                if (validateToken != null && validateToken.Identity.IsAuthenticated)
                {
                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        var prChunks = dt.AsEnumerable().ToList().Chunk(10000);
                        foreach (var chunk in prChunks)
                        {

                            DataTable tempDT = new DataTable();
                            tempDT.Clear();
                            tempDT.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                            foreach (var column in columns)
                            {
                                tempDT.Columns.Add(column);
                            }
                            tempDT.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));


                            List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                            foreach (DataColumn col in tempDT.Columns)
                            {
                                columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                            }


                            foreach (var row in chunk)
                            {
                                var newRow = tempDT.NewRow();
                                newRow["JOB_ID"] = guid1;
                                newRow["CASE_ID"] = GetDataRowVaue(row, "CASE_ID");
                                newRow["PO_LINE_ITEM_SKU_ID"] = GetDataRowVaue(row, "PO_LINE_ITEM_SKU_ID");
                                string reservationDate = GetDataRowVaue(row, "RESERVATION_DATE");
                                newRow["RESERVATION_DATE"] = (reservationDate == "00000000" || reservationDate == "0" || reservationDate == "") ? null : reservationDate;
                                string reservedQty = !string.IsNullOrEmpty(GetDataRowVaue(row, "RESERVED_QTY")) ? GetDataRowVaue(row, "RESERVED_QTY").Replace(",", "") : "0";
                                newRow["RESERVED_QTY"] = reservedQty;
                                newRow["RESERVATION_STATUS"] = GetDataRowVaue(row, "RESERVATION_STATUS");
                                newRow["DATE_CREATED"] = DateTime.UtcNow;


                                tempDT.Rows.Add(newRow);
                            }

                            bizClass.BulkInsert(tempDT, "[dbo].[SAP_GRN_RESERVED_DETAILS]", columnMappings);
                            tempDT.Rows.Clear();


                        }

                    }

                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_JOB_ID", guid);
                    bizClass.SelectList("erp_process_sap_update_grn_details", sd);
                    userToken.Message = "GRN Saved Successfully.";
                }
                else
                {
                    userToken.Message = "You are Unauthorized to connect PRM.";
                    userToken.Success = false;
                }
            }
            catch (Exception ex)
            {
                userToken.ErrorMessage = ex.Message;
                userToken.Message = ex.Message;
                userToken.Success = false;
            }

            return userToken;
        }



        public Response GetVendorJeevesDetails()
        {
            var now = DateTime.Now;
            logger.Info("Jeeves Vendor Job started running on >>" + now);
            UserToken userToken = new UserToken();
            Response response = new Response();

            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<VendorJeevesDetails> listVendorDetail = new List<VendorJeevesDetails>();
            try
            {
                CORE.DataNamesMapper<VendorJeevesDetails> mapper = new CORE.DataNamesMapper<VendorJeevesDetails>();

                string query = $@"select distinct COMP_NAME AS COMPANY_NAME,c.VENDOR_CODE,c.VENDOR_SITE_CODE,c.GST_NUMBER,
                            (SELECT TOP 1 U_CRED_ID from userverification where U_ID = v.U_ID and U_VERIFICATION_TYPE = 'PAN' ) AS PAN_NUMBER,
                            U_FNAME AS CONTACT_NAME,DESIGNATION AS CONTACT_DESIGNATION,U_EMAIL AS EMAIL,U_PHONE AS PHONE_NUMBER,
                            (SELECT CURR_NAME from company where COMP_ID = dbo.GetCompanyID(v.U_ID)) AS CURRENCY,U_FNAME AS CONTACT_PERSON,SALESPERSON AS ENTITY,
                            VENDOR_CATEGORY,ADDRESS,COUNTRY,STATE,CITY,PINCODE,
                            (select  JSON_VALUE(value, '$.SHIPPING_ADDRESS') from OPENJSON(ADDITIONAL_VENDOR_INFORMATION)) AS SHIPPING_ADDRESS,
                            (select  JSON_VALUE(value, '$.SHIPPING_COUNTRY') from OPENJSON(ADDITIONAL_VENDOR_INFORMATION)) AS SHIPPING_COUNTRY,
                            (select  JSON_VALUE(value, '$.SHIPPING_CITY') from OPENJSON(ADDITIONAL_VENDOR_INFORMATION)) AS SHIPPING_CITY,
                            (select  JSON_VALUE(value, '$.SHIPPING_STATE') from OPENJSON(ADDITIONAL_VENDOR_INFORMATION)) AS SHIPPING_STATE,
                            (select  JSON_VALUE(value, '$.SHIPPING_PINCODE') from OPENJSON(ADDITIONAL_VENDOR_INFORMATION)) AS SHIPPING_PINCODE
                            from vendors V
                            left JOIN companygstinfo c on c.VENDOR_ID=v.U_ID where  SALESPERSON = 'JCS-Jeeves Consumer Services Pvt Ltd' and IS_PROCESSED = 0";
                //DataSet ds = sqlHelper.ExecuteQuery(query);

                var dataset = sqlHelper.ExecuteQuery(query);
                listVendorDetail = mapper.Map(dataset.Tables[0]).ToList();

                //if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                //{
                //    foreach (DataRow row in ds.Tables[0].Rows)
                //    {
                //        VendorJeevesDetails vendorDetail = new VendorJeevesDetails();
                //        vendorDetail.company_name = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                //        vendorDetail.vendor_code = row["VENDOR_CODE"] != DBNull.Value ? Convert.ToString(row["VENDOR_CODE"]) : string.Empty;
                //        vendorDetail.vendor_site_code = row["VENDOR_SITE_CODE"] != DBNull.Value ? Convert.ToString(row["VENDOR_SITE_CODE"]) : string.Empty;
                //        vendorDetail.vendor_code = row["VENDOR_CODE"] != DBNull.Value ? Convert.ToString(row["VENDOR_CODE"]) : string.Empty;
                //        vendorDetail.gst_number = row["GST_NUMBER"] != DBNull.Value ? Convert.ToString(row["GST_NUMBER"]) : string.Empty;
                //        vendorDetail.pan_number = row["PAN_NUMBER"] != DBNull.Value ? Convert.ToString(row["PAN_NUMBER"]) : string.Empty;
                //        vendorDetail.contact_name = row["CONTACT_NAME"] != DBNull.Value ? Convert.ToString(row["CONTACT_NAME"]) : string.Empty;
                //        vendorDetail.contact_designation = row["CONTACT_DESIGNATION"] != DBNull.Value ? Convert.ToString(row["CONTACT_DESIGNATION"]) : string.Empty;
                //        vendorDetail.email = row["EMAIL"] != DBNull.Value ? Convert.ToString(row["EMAIL"]) : string.Empty;
                //        vendorDetail.phone_number = row["PHONE_NUMBER"] != DBNull.Value ? Convert.ToString(row["PHONE_NUMBER"]) : string.Empty;
                //        vendorDetail.currency = row["CURRENCY"] != DBNull.Value ? Convert.ToString(row["CURRENCY"]) : string.Empty;
                //        vendorDetail.contact_person = row["CONTACT_PERSON"] != DBNull.Value ? Convert.ToString(row["CONTACT_PERSON"]) : string.Empty;
                //        vendorDetail.vendor_category = row["VENDOR_CATEGORY"] != DBNull.Value ? Convert.ToString(row["VENDOR_CATEGORY"]) : string.Empty;
                //        vendorDetail.entity = row["ENTITY"] != DBNull.Value ? Convert.ToString(row["ENTITY"]) : string.Empty;
                //        vendorDetail.billing_address = new BillingAddress();
                //        vendorDetail.billing_address.value = row["ADDRESS"] != DBNull.Value ? Convert.ToString(row["ADDRESS"]) : string.Empty;
                //        vendorDetail.billing_address.country = row["COUNTRY"] != DBNull.Value ? Convert.ToString(row["COUNTRY"]) : string.Empty;
                //        vendorDetail.billing_address.city = row["STATE"] != DBNull.Value ? Convert.ToString(row["STATE"]) : string.Empty;
                //        vendorDetail.billing_address.state = row["CITY"] != DBNull.Value ? Convert.ToString(row["CITY"]) : string.Empty;
                //        vendorDetail.billing_address.pin_code = row["PINCODE"] != DBNull.Value ? Convert.ToString(row["PINCODE"]) : string.Empty;
                //        vendorDetail.shipping_address = new ShippingAddress();
                //        vendorDetail.shipping_address.value = row["SHIPPING_ADDRESS"] != DBNull.Value ? Convert.ToString(row["SHIPPING_ADDRESS"]) : string.Empty;
                //        vendorDetail.shipping_address.country = row["SHIPPING_COUNTRY"] != DBNull.Value ? Convert.ToString(row["SHIPPING_COUNTRY"]) : string.Empty;
                //        vendorDetail.shipping_address.city = row["SHIPPING_CITY"] != DBNull.Value ? Convert.ToString(row["SHIPPING_CITY"]) : string.Empty;
                //        vendorDetail.shipping_address.state = row["SHIPPING_STATE"] != DBNull.Value ? Convert.ToString(row["SHIPPING_STATE"]) : string.Empty;
                //        vendorDetail.shipping_address.pin_code = row["SHIPPING_PINCODE"] != DBNull.Value ? Convert.ToString(row["SHIPPING_PINCODE"]) : string.Empty;
                //        listVendorDetail.Add(vendorDetail);
                //    }
                //}


                string json = JsonConvert.SerializeObject(listVendorDetail);
                logger.Info("Jeeves Vendor Job json is>>>" + json);

                userToken = GetJeevesToken();
                if (userToken.Status == "200")
                {
                    var url = ConfigurationManager.AppSettings["JEEVES_VENDOR"];

                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                           | SecurityProtocolType.Tls11
                           | SecurityProtocolType.Tls12
                           | SecurityProtocolType.Ssl3;
                    var httpRequest = (HttpWebRequest)WebRequest.Create(url);
                    httpRequest.Method = "POST";

                    httpRequest.Timeout = -1;
                    httpRequest.ContentType = "application/json";

                    httpRequest.Headers.Add("Authorization", "Bearer " + userToken.AccessToken);
                    byte[] data = Encoding.ASCII.GetBytes(json);
                    httpRequest.ContentLength = data.Length;
                    using (var streamWriter = new StreamWriter(httpRequest.GetRequestStream()))
                    {

                        streamWriter.Write(json);
                    }

                    logger.Info("Token generated successfully and hitting the JEEVES_VENDOR URL " + url + " for the job running on >>" + now);
                    dynamic myObject = null;
                    var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                    using (StreamReader Reader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        myObject = JsonConvert.DeserializeObject(Reader.ReadToEnd());
                    }

                    var status = (int)httpResponse.StatusCode;
                    logger.Info("Jeeves Vendor Job Ran Successfully on >>" + now + "and got the response as >>>" + myObject.error_code.ToString());
                    if (status == 200)
                    {
                        if (myObject.error_code.ToString() == "BAD_REQUEST")
                        {
                            response.Message = myObject.error_message.ToString();
                            response.ErrorMessage = response.Message;
                            return response;
                        }
                        else
                        {

                            response.Message = "";
                            response.ErrorMessage = response.Message;
                            return response;
                        }

                    }

                }
                else if (userToken.Status == "400")
                {
                    response.Message = "Unauthorized User";
                    response.ErrorMessage = response.Message;
                    return response;
                }


            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
                response.Message = ex.Message;
                logger.Error("Error in Jeeves Vendor Job>>>> " + ex.Message +" on " + now);
            }
            return response;
        }

        public Response GetPOJeevesDetails(string ponumber)
        {

            UserToken userToken = new UserToken();
            Response response = new Response();

            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<POJeevesScheduleDetails> listpoDetail = new List<POJeevesScheduleDetails>();
            List<POJeevesItems> listDispatcPOItems = new List<POJeevesItems>();

            try
            {
                CORE.DataNamesMapper<POJeevesScheduleDetails> mapper = new CORE.DataNamesMapper<POJeevesScheduleDetails>();
                CORE.DataNamesMapper<POJeevesItems> mapperItemList = new CORE.DataNamesMapper<POJeevesItems>();

                string query = $@"select distinct PO_NUMBER,C.VENDOR_SITE_CODE,v.VENDOR_CODE,v.COMP_NAME as VENDOR_NAME,
                    psd.BILL_TO_LOCATION,psd.SHIP_TO_LOCATION,(SELECT CODE FROM OracleConfigTable WHERE NAME LIKE CONCAT('%',psd.SHIP_TO_LOCATION,'%')) as DELIVERY_LOCATION, 
                    CONVERT(VARCHAR(10), PO_DATE, 126) AS CREATED_AT, 
                    CONVERT(VARCHAR(10), PO_RELEASE_DATE, 126) AS RELEASED_AT, NET_PRICE AS TOTAL_PRICE, 
                    SUM(ORDER_QTY) AS ORDER_QUANTITY,TYPE_OF_STOCK from POScheduleDetails psd 
                    inner join vendors v on v.U_ID = psd.VENDOR_ID
					left join companygstinfo c on c.VENDOR_ID = v.u_id WHERE STATUS = 'CREATE' AND psd.IS_PROCESSED = 0
                    AND PO_QTY_STATUS = 'APPROVED' AND PO_NUMBER = '{ponumber}' GROUP BY PO_NUMBER,C.VENDOR_SITE_CODE,V.VENDOR_CODE,v.COMP_NAME 
                    ,psd.BILL_TO_LOCATION,psd.SHIP_TO_LOCATION,psd.CITY,PO_DATE,PO_RELEASE_DATE,NET_PRICE,TYPE_OF_STOCK ;
                    select distinct PO_NUMBER,LEFT(ProductCode, CHARINDEX('^&&^', ProductCode) - 1) as SKU,ProductDesc as ITEM_DESC,PrefferedBrand as OWNER,ORDER_QTY as OPEN_QUANTITY,ORDER_QTY as TOTAL_QUANTITY,ITEM_TOTAL_PRICE AS COST_INCLUDING_TAX,ITEM_TOTAL_PRICE_NO_TAX AS COST_EXCLUDING_TAX from POScheduleDetails psd 
                    inner join cm_product cp on cp.ProductId = psd.PRODUCT_ID 
                    inner join vendors v on v.U_ID = psd.VENDOR_ID WHERE STATUS = 'CREATE' AND PO_QTY_STATUS = 'APPROVED' AND PO_NUMBER = '{ponumber}' AND psd.IS_PROCESSED=0";
                DataSet ds = sqlHelper.ExecuteQuery(query);

                //var dataset = sqlHelper.ExecuteQuery(query);
                listpoDetail = mapper.Map(ds.Tables[0]).ToList();
                //listDispatcPOItems = mapperItemList.Map(dataset.Tables[1]).ToList();

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0 && ds.Tables[1].Rows[0][0] != null)
                {
                    foreach (DataRow row in ds.Tables[1].Rows)
                    {
                        POJeevesItems poItem = new POJeevesItems();
                        poItem.po_number = row["PO_NUMBER"] != DBNull.Value ? Convert.ToString(row["PO_NUMBER"]) : string.Empty;
                        poItem.owner = row["OWNER"] != DBNull.Value ? Convert.ToString(row["OWNER"]) : string.Empty;
                        poItem.sku = row["SKU"] != DBNull.Value ? Convert.ToString(row["SKU"]) : string.Empty;
                        poItem.item_description = row["ITEM_DESC"] != DBNull.Value ? Convert.ToString(row["ITEM_DESC"]) : string.Empty;
                        poItem.open_quantity = row["OPEN_QUANTITY"] != DBNull.Value ? Convert.ToDecimal(row["OPEN_QUANTITY"]) : 0;
                        poItem.total_quantity = row["TOTAL_QUANTITY"] != DBNull.Value ? Convert.ToDecimal(row["TOTAL_QUANTITY"]) : 0;
                        poItem.attributes = new Attributes();
                        poItem.attributes.cost_including_tax = row["COST_INCLUDING_TAX"] != DBNull.Value ? Convert.ToDecimal(row["COST_INCLUDING_TAX"]) : 0;
                        poItem.attributes.cost_excluding_tax = row["COST_EXCLUDING_TAX"] != DBNull.Value ? Convert.ToDecimal(row["COST_EXCLUDING_TAX"]) : 0;
                        listDispatcPOItems.Add(poItem);
                    }
                }

                foreach (POJeevesScheduleDetails bid in listpoDetail)
                {
                    bid.line_items = new List<POJeevesItems>();
                    bid.line_items = listDispatcPOItems.Where(p => p.po_number == bid.po_number).ToList();
                }

                //if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                //{
                //    foreach (DataRow row in ds.Tables[0].Rows)
                //    {
                //        POJeevesScheduleDetails poDetail = new POJeevesScheduleDetails();
                //        poDetail.po_number = row["PO_NUMBER"] != DBNull.Value ? Convert.ToString(row["PO_NUMBER"]) : string.Empty;
                //        poDetail.vendor_site_code = row["VENDOR_SITE_CODE"] != DBNull.Value ? Convert.ToString(row["VENDOR_SITE_CODE"]) : string.Empty;
                //        poDetail.vendor_name = row["VENDOR_NAME"] != DBNull.Value ? Convert.ToString(row["VENDOR_NAME"]) : string.Empty;
                //        poDetail.vendor_code = row["VENDOR_CODE"] != DBNull.Value ? Convert.ToString(row["VENDOR_CODE"]) : string.Empty;
                //        poDetail.bill_to_location = row["BILL_TO_LOCATION"] != DBNull.Value ? Convert.ToString(row["BILL_TO_LOCATION"]) : string.Empty;
                //        poDetail.ship_to_location = row["SHIP_TO_LOCATION"] != DBNull.Value ? Convert.ToString(row["SHIP_TO_LOCATION"]) : string.Empty;
                //        poDetail.total_price = row["TOTAL_PRICE"] != DBNull.Value ? Convert.ToDecimal(row["TOTAL_PRICE"]) : 0;
                //        poDetail.created_at = row["CREATED_AT"] != DBNull.Value ? Convert.ToString(row["CREATED_AT"]) : string.Empty;
                //        poDetail.released_at = row["RELEASED_AT"] != DBNull.Value ? Convert.ToString(row["RELEASED_AT"]) : string.Empty;
                //        poDetail.destination_asset_code = row["DELIVERY_LOCATION"] != DBNull.Value ? Convert.ToString(row["DELIVERY_LOCATION"]) : string.Empty;
                //        poDetail.order_quantity = row["ORDER_QUANTITY"] != DBNull.Value ? Convert.ToDecimal(row["ORDER_QUANTITY"]) : 0;
                //        poDetail.type_of_stock = row["TYPE_OF_STOCK"] != DBNull.Value ? Convert.ToString(row["TYPE_OF_STOCK"]) : string.Empty;
                //        poDetail.line_items = listDispatcPOItems.Where(p => p.po_number == poDetail.po_number).ToList();
                //        listpoDetail.Add(poDetail);
                //    }
                //}


                string json = JsonConvert.SerializeObject(listpoDetail);

                userToken = GetJeevesToken();
                if (userToken.Status == "200")
                {
                    var url = ConfigurationManager.AppSettings["JEEVES_CREATE_PO"];
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                           | SecurityProtocolType.Tls11
                           | SecurityProtocolType.Tls12
                           | SecurityProtocolType.Ssl3;
                    var httpRequest = (HttpWebRequest)WebRequest.Create(url);
                    httpRequest.Method = "POST";

                    httpRequest.Timeout = -1;
                    httpRequest.ContentType = "application/json";

                    httpRequest.Headers.Add("Authorization", "Bearer " + userToken.AccessToken);
                    byte[] data = Encoding.ASCII.GetBytes(json);
                    httpRequest.ContentLength = data.Length;
                    using (var streamWriter = new StreamWriter(httpRequest.GetRequestStream()))
                    {

                        streamWriter.Write(json);
                    }
                    response.Message = json;

                    dynamic myObject = null;
                    var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                    using (StreamReader Reader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        myObject = JsonConvert.DeserializeObject(Reader.ReadToEnd());
                    }
                    var status = (int)httpResponse.StatusCode;
                    if (status == 200)
                    {
                        response.ErrorMessage = "";
                        return response;

                    }
                    else if(myObject.error_code.ToString() == "BAD_REQUEST")
                        {
                        response.ErrorMessage = myObject.error_message.ToString();
                        return response;
                    }

                }
                else if (userToken.Status == "400")
                {
                    response.ErrorMessage = "Unauthorized User";
                    return response;
                }

            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
                //response.Message = ex.Message;
            }
            return response;
        }

        public Response UpdatePOJeevesDetails(string ponumber)
        {
            UserToken userToken = new UserToken();
            Response response = new Response();

            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<POJeevesScheduleDetails> listpoDetail = new List<POJeevesScheduleDetails>();
            List<POJeevesItems> listDispatcPOItems = new List<POJeevesItems>();

            try
            {
                CORE.DataNamesMapper<POJeevesScheduleDetails> mapper = new CORE.DataNamesMapper<POJeevesScheduleDetails>();
                CORE.DataNamesMapper<POJeevesItems> mapperItemList = new CORE.DataNamesMapper<POJeevesItems>();

                string query = $@"select distinct PO_NUMBER,C.VENDOR_SITE_CODE,v.VENDOR_CODE,v.COMP_NAME as VENDOR_NAME,
                    psd.BILL_TO_LOCATION,psd.SHIP_TO_LOCATION,(SELECT CODE FROM OracleConfigTable WHERE NAME LIKE CONCAT('%',psd.SHIP_TO_LOCATION,'%')) as DELIVERY_LOCATION, 
                    CONVERT(VARCHAR(10), PO_DATE, 126) AS CREATED_AT, 
                    CONVERT(VARCHAR(10), PO_RELEASE_DATE, 126) AS RELEASED_AT, NET_PRICE AS TOTAL_PRICE, 
                    SUM(ORDER_QTY) AS ORDER_QUANTITY,TYPE_OF_STOCK from POScheduleDetails psd 
                    inner join vendors v on v.U_ID = psd.VENDOR_ID
                    left join companygstinfo c on c.VENDOR_ID = v.u_id WHERE STATUS = 'UPDATE' AND PO_NUMBER = '{ponumber}' AND psd.IS_PROCESSED=0
                    AND PO_QTY_STATUS = 'APPROVED' GROUP BY PO_NUMBER,C.VENDOR_SITE_CODE,V.VENDOR_CODE,v.COMP_NAME 
                    ,psd.BILL_TO_LOCATION,psd.SHIP_TO_LOCATION,psd.CITY,PO_DATE,PO_RELEASE_DATE,NET_PRICE,TYPE_OF_STOCK;
                    select distinct PO_NUMBER,LEFT(ProductCode, CHARINDEX('^&&^', ProductCode) - 1) as SKU,ProductDesc as ITEM_DESC,PrefferedBrand as OWNER,ORDER_QTY as OPEN_QUANTITY,ORDER_QTY as TOTAL_QUANTITY,ITEM_TOTAL_PRICE AS COST_INCLUDING_TAX,ITEM_TOTAL_PRICE_NO_TAX AS COST_EXCLUDING_TAX from POScheduleDetails psd 
                    inner join cm_product cp on cp.ProductId = psd.PRODUCT_ID
                    inner join vendors v on v.U_ID = psd.VENDOR_ID WHERE STATUS = 'UPDATE' AND PO_QTY_STATUS = 'APPROVED' AND PO_NUMBER = '{ponumber}' AND psd.IS_PROCESSED=0";
                DataSet ds = sqlHelper.ExecuteQuery(query);
                listpoDetail = mapper.Map(ds.Tables[0]).ToList();
                //listDispatcPOItems = mapperItemList.Map(dataset.Tables[1]).ToList();

                //List<POJeevesItems> listDispatcPOItems = new List<POJeevesItems>();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0 && ds.Tables[1].Rows[0][0] != null)
                {
                    foreach (DataRow row in ds.Tables[1].Rows)
                    {
                        POJeevesItems poItem = new POJeevesItems();
                        poItem.po_number = row["PO_NUMBER"] != DBNull.Value ? Convert.ToString(row["PO_NUMBER"]) : string.Empty;
                        poItem.owner = row["OWNER"] != DBNull.Value ? Convert.ToString(row["OWNER"]) : string.Empty;
                        poItem.item_description = row["ITEM_DESC"] != DBNull.Value ? Convert.ToString(row["ITEM_DESC"]) : string.Empty;
                        poItem.sku = row["SKU"] != DBNull.Value ? Convert.ToString(row["SKU"]) : string.Empty;
                        poItem.open_quantity = row["OPEN_QUANTITY"] != DBNull.Value ? Convert.ToDecimal(row["OPEN_QUANTITY"]) : 0;
                        poItem.total_quantity = row["TOTAL_QUANTITY"] != DBNull.Value ? Convert.ToDecimal(row["TOTAL_QUANTITY"]) : 0;
                        poItem.attributes = new Attributes();
                        poItem.attributes.cost_including_tax = row["COST_INCLUDING_TAX"] != DBNull.Value ? Convert.ToDecimal(row["COST_INCLUDING_TAX"]) : 0;
                        poItem.attributes.cost_excluding_tax = row["COST_EXCLUDING_TAX"] != DBNull.Value ? Convert.ToDecimal(row["COST_EXCLUDING_TAX"]) : 0;
                        listDispatcPOItems.Add(poItem);
                    }
                }

                foreach (POJeevesScheduleDetails bid in listpoDetail)
                {
                    bid.line_items = new List<POJeevesItems>();
                    bid.line_items = listDispatcPOItems.Where(p => p.po_number == bid.po_number).ToList();
                }

                //if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                //{
                //    foreach (DataRow row in ds.Tables[0].Rows)
                //    {
                //        POJeevesScheduleDetails poDetail = new POJeevesScheduleDetails();
                //        poDetail.po_number = row["PO_NUMBER"] != DBNull.Value ? Convert.ToString(row["PO_NUMBER"]) : string.Empty;
                //        poDetail.vendor_site_code = row["VENDOR_SITE_CODE"] != DBNull.Value ? Convert.ToString(row["VENDOR_SITE_CODE"]) : string.Empty;
                //        poDetail.vendor_name = row["VENDOR_NAME"] != DBNull.Value ? Convert.ToString(row["VENDOR_NAME"]) : string.Empty;
                //        poDetail.vendor_code = row["VENDOR_CODE"] != DBNull.Value ? Convert.ToString(row["VENDOR_CODE"]) : string.Empty;
                //        poDetail.bill_to_location = row["BILL_TO_LOCATION"] != DBNull.Value ? Convert.ToString(row["BILL_TO_LOCATION"]) : string.Empty;
                //        poDetail.ship_to_location = row["SHIP_TO_LOCATION"] != DBNull.Value ? Convert.ToString(row["SHIP_TO_LOCATION"]) : string.Empty;
                //        poDetail.total_price = row["TOTAL_PRICE"] != DBNull.Value ? Convert.ToDecimal(row["TOTAL_PRICE"]) : 0;
                //        poDetail.created_at = row["CREATED_AT"] != DBNull.Value ? Convert.ToString(row["CREATED_AT"]) : string.Empty;
                //        poDetail.released_at = row["RELEASED_AT"] != DBNull.Value ? Convert.ToString(row["RELEASED_AT"]) : string.Empty;
                //        poDetail.destination_asset_code = row["DELIVERY_LOCATION"] != DBNull.Value ? Convert.ToString(row["DELIVERY_LOCATION"]) : string.Empty;
                //        poDetail.order_quantity = row["ORDER_QUANTITY"] != DBNull.Value ? Convert.ToDecimal(row["ORDER_QUANTITY"]) : 0;
                //        poDetail.type_of_stock = row["TYPE_OF_STOCK"] != DBNull.Value ? Convert.ToString(row["TYPE_OF_STOCK"]) : string.Empty;
                //        poDetail.line_items = listDispatcPOItems.Where(p => p.po_number == poDetail.po_number).ToList();
                //        listpoDetail.Add(poDetail);
                //    }
                //}
                string json = JsonConvert.SerializeObject(listpoDetail);


                userToken = GetJeevesToken();
                if (userToken.Status == "200")
                {
                    var url = ConfigurationManager.AppSettings["JEEVES_UPDATE_PO"];

                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                           | SecurityProtocolType.Tls11
                           | SecurityProtocolType.Tls12
                           | SecurityProtocolType.Ssl3;
                    var httpRequest = (HttpWebRequest)WebRequest.Create(url);
                    httpRequest.Method = "PUT";

                    httpRequest.Timeout = -1;
                    httpRequest.ContentType = "application/json";

                    httpRequest.Headers.Add("Authorization", "Bearer " + userToken.AccessToken);
                    byte[] data = Encoding.ASCII.GetBytes(json);
                    httpRequest.ContentLength = data.Length;
                    using (var streamWriter = new StreamWriter(httpRequest.GetRequestStream()))
                    {

                        streamWriter.Write(json);
                    }
                    response.Message = json;

                    dynamic myObject = null;
                    var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                    using (StreamReader Reader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        myObject = JsonConvert.DeserializeObject(Reader.ReadToEnd());
                    }

                    var status = (int)httpResponse.StatusCode;
                    if (status == 200)
                    {
                        response.ErrorMessage = "";
                        return response;

                    }
                    else if (myObject.error_code.ToString() == "BAD_REQUEST")
                    {
                        response.ErrorMessage = myObject.error_message.ToString();
                        return response;
                    }
                }

                else if (userToken.Status == "400")
                {
                    response.ErrorMessage = "Unauthorized User";
                    return response;
                }



            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
                //response.Message = ex.Message;
            }

            return response;
        }

        public Response DeletePOJeevesDetails(string ponumber)
        {
            Response response = new Response();
            UserToken userToken = new UserToken();

            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<DeletePOJeeves> listpoDetail = new List<DeletePOJeeves>();
            try
            {
                CORE.DataNamesMapper<DeletePOJeeves> mapper = new CORE.DataNamesMapper<DeletePOJeeves>();

                string query = $"SELECT DISTINCT PO_NUMBER,V.VENDOR_CODE,(SELECT CODE FROM OracleConfigTable WHERE NAME LIKE CONCAT('%',psd.SHIP_TO_LOCATION,'%')) AS DELIVERY_LOCATION FROM POScheduleDetails psd inner join vendors v on v.U_ID = psd.VENDOR_ID WHERE STATUS='DELETE' and PO_NUMBER = '{ponumber}'";
                //DataSet ds = sqlHelper.ExecuteQuery(query);
                var dataset = sqlHelper.ExecuteQuery(query);
                listpoDetail = mapper.Map(dataset.Tables[0]).ToList();


                //if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                //{
                //    foreach (DataRow row in ds.Tables[0].Rows)
                //    {
                //        DeletePOJeeves poDetail = new DeletePOJeeves();
                //        poDetail.po_number = row["PO_NUMBER"] != DBNull.Value ? Convert.ToString(row["PO_NUMBER"]) : string.Empty;
                //        poDetail.destination_asset_code = row["DELIVERY_LOCATION"] != DBNull.Value ? Convert.ToString(row["DELIVERY_LOCATION"]) : string.Empty;
                //        poDetail.vendor_code = row["VENDOR_CODE"] != DBNull.Value ? Convert.ToString(row["VENDOR_CODE"]) : string.Empty;
                //        listpoDetail.Add(poDetail);
                //    }
                //}
                string json = JsonConvert.SerializeObject(listpoDetail);

                userToken = GetJeevesToken();
                if (userToken.Status == "200")
                {
                    var url = ConfigurationManager.AppSettings["JEEVES_DELETE_PO"];
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                           | SecurityProtocolType.Tls11
                           | SecurityProtocolType.Tls12
                           | SecurityProtocolType.Ssl3;
                    var httpRequest = (HttpWebRequest)WebRequest.Create(url);
                    httpRequest.Method = "PUT";

                    httpRequest.Timeout = -1;
                    httpRequest.ContentType = "application/json";

                    httpRequest.Headers.Add("Authorization", "Bearer " + userToken.AccessToken);
                    byte[] data = Encoding.ASCII.GetBytes(json);
                    httpRequest.ContentLength = data.Length;
                    using (var streamWriter = new StreamWriter(httpRequest.GetRequestStream()))
                    {

                        streamWriter.Write(json);
                    }
                    response.Message = json;

                    dynamic myObject = null;
                    var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                    using (StreamReader Reader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        myObject = JsonConvert.DeserializeObject(Reader.ReadToEnd());
                    }

                    var status = (int)httpResponse.StatusCode;
                    if (status == 200)
                    {
                        response.ErrorMessage = "";
                        return response;

                    }
                    else if (myObject.error_code.ToString() == "BAD_REQUEST")
                    {
                        response.ErrorMessage = myObject.error_message.ToString();
                        return response;
                    }

                }
                else if (userToken.Status == "400")
                {
                    response.ErrorMessage = "Unauthorized User";
                    return response;
                }


            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
                //response.Message = ex.Message;
            }

            return response;
        }
        public Response GetInvoiceJeevesDetails(string invoicenumber)
        {

            UserToken userToken = new UserToken();
            Response response = new Response();

            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<InvoiceJeevesScheduleDetails> listinvoiceDetail = new List<InvoiceJeevesScheduleDetails>();
            List<InvoiceJeevesItems> listDispatcInvoiceItems = new List<InvoiceJeevesItems>();

            try
            {

                CORE.DataNamesMapper<InvoiceJeevesScheduleDetails> mapper = new CORE.DataNamesMapper<InvoiceJeevesScheduleDetails>();
                CORE.DataNamesMapper<InvoiceJeevesItems> mapperItemList = new CORE.DataNamesMapper<InvoiceJeevesItems>();

                string query = $@"select distinct INVOICE_NUMBER AS INVOICE_NO,INVOICE_TYPE,(CONVERT(VARCHAR(50),INVOICE_DATE, 127)) AS INVOICE_DATE,SUM(INVOICE_QTY) AS INVOICE_QUANTITY,SUM(pid.ORDER_QTY-INVOICE_QTY) AS REMAINING_QUANTITY 
                   ,INVOICE_AMOUNT,'' AS ENTITY,'' AS ASN_CODE,LOWER(psd.TYPE_OF_STOCK)  AS TYPE_OF_STOCK, 
                   pid.PO_NUMBER,psd.BILL_TO_LOCATION AS BILLED_TO,psd.SHIP_TO_LOCATION AS SHIPPED_TO,(SELECT CODE FROM OracleConfigTable WHERE NAME LIKE CONCAT('%',psd.SHIP_TO_LOCATION,'%')) as DESTINATION_ASSET_CODE
                   ,'' AS SHIPPED_FROM,C.VENDOR_SITE_CODE,V.VENDOR_CODE,v.COMP_NAME AS VENDOR_NAME 
                   from POInvoiceDetails pid 
                   inner join vendors v on v.U_ID = pid.VENDOR_ID
                   left join companygstinfo c on c.VENDOR_ID = v.u_id
                   inner join poscheduledetails psd on psd.po_number = pid.po_number AND PSD.PO_LINE_ITEM = PID.PO_LINE_ITEM 
                   WHERE INVOICE_STATUS = 'CREATE' AND INVOICE_NUMBER = '{invoicenumber}' AND pid.IS_PROCESSED = 0
                   GROUP BY INVOICE_NUMBER,INVOICE_TYPE,INVOICE_DATE,INVOICE_AMOUNT, 
                   pid.PO_NUMBER,psd.BILL_TO_LOCATION,psd.SHIP_TO_LOCATION,psd.CITY,C.VENDOR_SITE_CODE,V.VENDOR_CODE,v.COMP_NAME,psd.TYPE_OF_STOCK ; 
                   select distinct PO_NUMBER, INVOICE_NUMBER AS INVOICE_NO, (CASE WHEN CHARINDEX('^&&^', PRODUCT_CODE) > 0 THEN LEFT(PRODUCT_CODE, CHARINDEX('^&&^', PRODUCT_CODE) - 1) ELSE PRODUCT_CODE END) as ITEM_CODE,PRODUCT_NAME AS ITEM_NAME,INVOICE_QTY AS ACCEPTED_QUANTITY,ORDER_QTY AS QUANTITY 
                   ,(ORDER_QTY-INVOICE_QTY) AS RETURN_QUANTITY,(INVOICE_QTY*NET_PRICE) AS AMOUNT,INVOICE_AMOUNT AS SUB_TOTAL,0 AS TAX,0 AS ASN_QUANTITY,pid.CONVERTED_PDFS AS ATTACHMENTS from POInvoiceDetails pid 
                   WHERE INVOICE_STATUS = 'CREATE' AND INVOICE_NUMBER = '{invoicenumber}' AND pid.IS_PROCESSED = 0 ";
                //DataSet ds = sqlHelper.ExecuteQuery(query);

                var dataset = sqlHelper.ExecuteQuery(query);
                listinvoiceDetail = mapper.Map(dataset.Tables[0]).ToList();
                listDispatcInvoiceItems = mapperItemList.Map(dataset.Tables[1]).ToList();


                foreach (InvoiceJeevesScheduleDetails bid in listinvoiceDetail)
                {
                    bid.po_line_item = new List<InvoiceJeevesItems>();
                    bid.po_line_item = listDispatcInvoiceItems.Where(p => p.invoice_no == bid.invoice_no).ToList();
                }


                //List<InvoiceJeevesItems> listDispatcInvoiceItems = new List<InvoiceJeevesItems>();
                //if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0 && ds.Tables[1].Rows[0][0] != null)
                //{
                //    foreach (DataRow row in ds.Tables[1].Rows)
                //    {
                //        InvoiceJeevesItems invoiceItem = new InvoiceJeevesItems();
                //        invoiceItem.invoice_no = row["INVOICE_NO"] != DBNull.Value ? Convert.ToString(row["INVOICE_NO"]) : string.Empty;
                //        invoiceItem.item_code = row["ITEM_CODE"] != DBNull.Value ? Convert.ToString(row["ITEM_CODE"]) : string.Empty;
                //        invoiceItem.item_name = row["ITEM_NAME"] != DBNull.Value ? Convert.ToString(row["ITEM_NAME"]) : string.Empty;
                //        invoiceItem.quantity = row["QUANTITY"] != DBNull.Value ? Convert.ToDecimal(row["QUANTITY"]) : 0;
                //        invoiceItem.asn_quantity = row["ASN_QUANTITY"] != DBNull.Value ? Convert.ToDecimal(row["ASN_QUANTITY"]) : 0;
                //        invoiceItem.accepted_quantity = row["ACCEPTED_QUANTITY"] != DBNull.Value ? Convert.ToDecimal(row["ACCEPTED_QUANTITY"]) : 0;
                //        invoiceItem.return_quantity = row["RETURN_QUANTITY"] != DBNull.Value ? Convert.ToDecimal(row["RETURN_QUANTITY"]) : 0;
                //        invoiceItem.amount = row["AMOUNT"] != DBNull.Value ? Convert.ToDecimal(row["AMOUNT"]) : 0;
                //        invoiceItem.sub_total = row["SUB_TOTAL"] != DBNull.Value ? Convert.ToDecimal(row["SUB_TOTAL"]) : 0;
                //        invoiceItem.tax = row["TAX"] != DBNull.Value ? Convert.ToDecimal(row["TAX"]) : 0;
                //        invoiceItem.attachment = row["ATTACHMENTS"] != DBNull.Value ? Convert.ToString(row["ATTACHMENTS"]) : string.Empty;
                //        listDispatcInvoiceItems.Add(invoiceItem);
                //    }
                //}

                //if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                //{
                //    foreach (DataRow row in ds.Tables[0].Rows)
                //    {
                //        InvoiceJeevesScheduleDetails invoiceDetail = new InvoiceJeevesScheduleDetails();
                //        invoiceDetail.invoice_no = row["INVOICE_NO"] != DBNull.Value ? Convert.ToString(row["INVOICE_NO"]) : string.Empty;
                //        invoiceDetail.invoice_type = row["INVOICE_TYPE"] != DBNull.Value ? Convert.ToString(row["INVOICE_TYPE"]) : string.Empty;
                //        invoiceDetail.invoice_date = row["INVOICE_DATE"] != DBNull.Value ? Convert.ToString(row["INVOICE_DATE"]) : string.Empty;
                //        invoiceDetail.invoice_quantity = row["INVOICE_QUANTITY"] != DBNull.Value ? Convert.ToDecimal(row["INVOICE_QUANTITY"]) : 0;
                //        invoiceDetail.remaining_quantity = row["REMAINING_QUANTITY"] != DBNull.Value ? Convert.ToDecimal(row["REMAINING_QUANTITY"]) : 0;
                //        invoiceDetail.invoice_amount = row["INVOICE_AMOUNT"] != DBNull.Value ? Convert.ToDecimal(row["INVOICE_AMOUNT"]) : 0;
                //        invoiceDetail.entity = row["ENTITY"] != DBNull.Value ? Convert.ToString(row["ENTITY"]) : string.Empty;
                //        invoiceDetail.po_number = row["PO_NUMBER"] != DBNull.Value ? Convert.ToString(row["PO_NUMBER"]) : string.Empty;
                //        invoiceDetail.asn_code = row["ASN_CODE"] != DBNull.Value ? Convert.ToString(row["ASN_CODE"]) : string.Empty;
                //        invoiceDetail.billed_to = row["BILLED_TO"] != DBNull.Value ? Convert.ToString(row["BILLED_TO"]) : string.Empty;
                //        invoiceDetail.destination_asset_code = row["DESTINATION_ASSET_CODE"] != DBNull.Value ? Convert.ToString(row["DESTINATION_ASSET_CODE"]) : string.Empty;
                //        invoiceDetail.vendor_code = row["VENDOR_CODE"] != DBNull.Value ? Convert.ToString(row["VENDOR_CODE"]) : string.Empty;
                //        invoiceDetail.vendor_site_code = row["VENDOR_SITE_CODE"] != DBNull.Value ? Convert.ToString(row["VENDOR_SITE_CODE"]) : string.Empty;
                //        invoiceDetail.vendor_name = row["VENDOR_NAME"] != DBNull.Value ? Convert.ToString(row["VENDOR_NAME"]) : string.Empty;
                //        invoiceDetail.shipped_from = row["SHIPPED_FROM"] != DBNull.Value ? Convert.ToString(row["SHIPPED_FROM"]) : string.Empty;
                //        invoiceDetail.shipped_to = row["SHIPPED_TO"] != DBNull.Value ? Convert.ToString(row["SHIPPED_TO"]) : string.Empty;
                //        invoiceDetail.type_of_stock = row["TYPE_OF_STOCK"] != DBNull.Value ? Convert.ToString(row["TYPE_OF_STOCK"]) : string.Empty;

                //        invoiceDetail.po_line_item = listDispatcInvoiceItems.Where(p => p.invoice_no == invoiceDetail.invoice_no).ToList();
                //        listinvoiceDetail.Add(invoiceDetail);
                //    }


                //}

                string json = JsonConvert.SerializeObject(listinvoiceDetail);


                userToken = GetJeevesToken();
                if (userToken.Status == "200")
                {
                    var url = ConfigurationManager.AppSettings["JEEVES_CREATE_INVOICE"];

                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                           | SecurityProtocolType.Tls11
                           | SecurityProtocolType.Tls12
                           | SecurityProtocolType.Ssl3;
                    var httpRequest = (HttpWebRequest)WebRequest.Create(url);
                    httpRequest.Method = "POST";

                    httpRequest.Timeout = -1;
                    httpRequest.ContentType = "application/json";

                    httpRequest.Headers.Add("Authorization", "Bearer " + userToken.AccessToken);
                    byte[] data = Encoding.ASCII.GetBytes(json);
                    httpRequest.ContentLength = data.Length;
                    using (var streamWriter = new StreamWriter(httpRequest.GetRequestStream()))
                    {
                        streamWriter.Write(json);
                    }
                    response.Message = json;

                    dynamic myObject = null;
                    var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                    using (StreamReader Reader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        myObject = JsonConvert.DeserializeObject(Reader.ReadToEnd());
                    }

                    var status = (int)httpResponse.StatusCode;
                    if (status == 200)
                    {
                        response.ErrorMessage = "";
                        return response;
                    }
                    else if (myObject.error_code.ToString() == "BAD_REQUEST")
                    {
                        response.ErrorMessage = myObject.error_message.ToString();
                        return response;
                    }

                }

                else if (userToken.Status == "400")
                {
                    response.ErrorMessage = "Unauthorized User";
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
                //response.Message = ex.Message;
            }

            return response;
        }
        public Response DeleteInvoiceJeevesDetails(string invoicenumber)
        {
            UserToken userToken = new UserToken();
            Response response = new Response();

            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<DeleteInvoiceJeeves> listinvoiceDetail = new List<DeleteInvoiceJeeves>();

            try
            {
                CORE.DataNamesMapper<DeleteInvoiceJeeves> mapper = new CORE.DataNamesMapper<DeleteInvoiceJeeves>();

                string query = $@"SELECT INVOICE_NUMBER ,v.VENDOR_CODE,c.VENDOR_SITE_CODE,v.COMP_NAME AS VENDOR_NAME,(SELECT CODE FROM OracleConfigTable WHERE NAME LIKE CONCAT('%',psd.SHIP_TO_LOCATION,'%')) as DESTINATION_ASSET_CODE
                    from POInvoiceDetails pid 
                    inner join vendors v on v.U_ID = pid.VENDOR_ID 
                    left join companygstinfo c on c.VENDOR_ID = v.u_id
                    inner join poscheduledetails psd on psd.po_number = pid.po_number AND PSD.PO_LINE_ITEM = PID.PO_LINE_ITEM WHERE INVOICE_STATUS='DELETE' AND INVOICE_NUMBER = '{invoicenumber}' AND pid.IS_PROCESSED = 0 ";
                //DataSet ds = sqlHelper.ExecuteQuery(query);
                var dataset = sqlHelper.ExecuteQuery(query);
                listinvoiceDetail = mapper.Map(dataset.Tables[0]).ToList();


                //if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                //{
                //    foreach (DataRow row in ds.Tables[0].Rows)
                //    {
                //        DeleteInvoiceJeeves invoiceDetail = new DeleteInvoiceJeeves();
                //        invoiceDetail.destination_asset_code = row["DESTINATION_ASSET_CODE"] != DBNull.Value ? Convert.ToString(row["DESTINATION_ASSET_CODE"]) : string.Empty;
                //        invoiceDetail.vendor_name = row["VENDOR_NAME"] != DBNull.Value ? Convert.ToString(row["VENDOR_NAME"]) : string.Empty;
                //        invoiceDetail.vendor_site_code = row["VENDOR_SITE_CODE"] != DBNull.Value ? Convert.ToString(row["VENDOR_SITE_CODE"]) : string.Empty;
                //        invoiceDetail.invoice_number = row["INVOICE_NUMBER"] != DBNull.Value ? Convert.ToString(row["INVOICE_NUMBER"]) : string.Empty;
                //        listinvoiceDetail.Add(invoiceDetail);
                //    }
                //}

                string json = JsonConvert.SerializeObject(listinvoiceDetail);

                userToken = GetJeevesToken();
                if (userToken.Status == "200")
                {
                    var url = ConfigurationManager.AppSettings["JEEVES_DELETE_INVOICE"];
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                           | SecurityProtocolType.Tls11
                           | SecurityProtocolType.Tls12
                           | SecurityProtocolType.Ssl3;
                    var httpRequest = (HttpWebRequest)WebRequest.Create(url);
                    httpRequest.Method = "PUT";

                    httpRequest.Timeout = -1;
                    httpRequest.ContentType = "application/json";

                    httpRequest.Headers.Add("Authorization", "Bearer " + userToken.AccessToken);
                    byte[] data = Encoding.ASCII.GetBytes(json);
                    httpRequest.ContentLength = data.Length;
                    using (var streamWriter = new StreamWriter(httpRequest.GetRequestStream()))
                    {

                        streamWriter.Write(json);
                    }
                    response.Message = json;

                    dynamic myObject = null;
                    var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                    using (StreamReader Reader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        myObject = JsonConvert.DeserializeObject(Reader.ReadToEnd());
                    }

                    var status = (int)httpResponse.StatusCode;
                    if (status == 200)
                    {
                        response.ErrorMessage = "";
                        return response;
                    }
                    else if (myObject.error_code.ToString() == "BAD_REQUEST")
                    {
                        response.ErrorMessage = myObject.error_message.ToString();
                        return response;
                    }

                }
                else if (userToken.Status == "400")
                {
                    response.ErrorMessage = "Unauthorized User";
                    return response;
                }



            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
                //response.Message = ex.Message;
            }

            return response;
        }
        public Response UpdateInvoiceJeevesDetails(string invoicenumber)
        {
            Response response = new Response();
            UserToken userToken = new UserToken();

            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<InvoiceJeevesScheduleDetails> listinvoiceDetail = new List<InvoiceJeevesScheduleDetails>();
            List<InvoiceJeevesItems> listDispatcInvoiceItems = new List<InvoiceJeevesItems>();
            try
            {
                CORE.DataNamesMapper<InvoiceJeevesScheduleDetails> mapper = new CORE.DataNamesMapper<InvoiceJeevesScheduleDetails>();
                CORE.DataNamesMapper<InvoiceJeevesItems> mapperItemList = new CORE.DataNamesMapper<InvoiceJeevesItems>();

                string query = $@"select distinct INVOICE_NUMBER AS INVOICE_NO,INVOICE_TYPE,(CONVERT(VARCHAR(50),INVOICE_DATE, 127)) AS INVOICE_DATE,SUM(INVOICE_QTY) AS INVOICE_QUANTITY,SUM(pid.ORDER_QTY-INVOICE_QTY) AS REMAINING_QUANTITY 
                    ,INVOICE_AMOUNT,'' AS ENTITY,'' AS ASN_CODE,LOWER(psd.TYPE_OF_STOCK) AS TYPE_OF_STOCK, 
                    pid.PO_NUMBER,psd.BILL_TO_LOCATION AS BILLED_TO,psd.SHIP_TO_LOCATION AS SHIPPED_TO,(SELECT CODE FROM OracleConfigTable WHERE NAME LIKE CONCAT('%',psd.SHIP_TO_LOCATION,'%')) as DESTINATION_ASSET_CODE
                    ,'' AS SHIPPED_FROM,psd.VENDOR_SITE_CODE,psd.VENDOR_CODE,v.COMP_NAME AS VENDOR_NAME
                    from POInvoiceDetails pid 
                    inner join vendors v on v.U_ID = pid.VENDOR_ID 
                    inner join poscheduledetails psd on psd.po_number = pid.po_number AND PSD.PO_LINE_ITEM = PID.PO_LINE_ITEM 
                    WHERE INVOICE_STATUS = 'UPDATE' AND INVOICE_NUMBER = '{invoicenumber}' AND pid.IS_PROCESSED = 0 
                    GROUP BY INVOICE_NUMBER,INVOICE_TYPE,INVOICE_DATE,INVOICE_AMOUNT, 
                    pid.PO_NUMBER,psd.BILL_TO_LOCATION,psd.SHIP_TO_LOCATION,psd.CITY,psd.VENDOR_SITE_CODE,psd.VENDOR_CODE,v.COMP_NAME,psd.TYPE_OF_STOCK;
                    select distinct PO_NUMBER, INVOICE_NUMBER AS INVOICE_NO,(CASE WHEN CHARINDEX('^&&^', PRODUCT_CODE) > 0 THEN LEFT(PRODUCT_CODE, CHARINDEX('^&&^', PRODUCT_CODE) - 1) ELSE PRODUCT_CODE END) as ITEM_CODE,PRODUCT_NAME AS ITEM_NAME,INVOICE_QTY AS ACCEPTED_QUANTITY,ORDER_QTY AS QUANTITY 
                    ,(ORDER_QTY-INVOICE_QTY) AS RETURN_QUANTITY,(INVOICE_QTY*NET_PRICE) AS AMOUNT,INVOICE_AMOUNT AS SUB_TOTAL,0 AS TAX,0 AS ASN_QUANTITY,pid.CONVERTED_PDFS AS ATTACHMENTS from POInvoiceDetails pid 
                    WHERE INVOICE_STATUS = 'UPDATE' AND INVOICE_NUMBER='{invoicenumber}' AND pid.IS_PROCESSED = 0";
                //DataSet ds = sqlHelper.ExecuteQuery(query);

                var dataset = sqlHelper.ExecuteQuery(query);
                listinvoiceDetail = mapper.Map(dataset.Tables[0]).ToList();
                listDispatcInvoiceItems = mapperItemList.Map(dataset.Tables[1]).ToList();


                foreach (InvoiceJeevesScheduleDetails bid in listinvoiceDetail)
                {
                    bid.po_line_item = new List<InvoiceJeevesItems>();
                    bid.po_line_item = listDispatcInvoiceItems.Where(p => p.invoice_no == bid.invoice_no).ToList();
                }


                //List<InvoiceJeevesItems> listDispatcInvoiceItems = new List<InvoiceJeevesItems>();
                //if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0 && ds.Tables[1].Rows[0][0] != null)
                //{
                //    foreach (DataRow row in ds.Tables[1].Rows)
                //    {
                //        InvoiceJeevesItems invoiceItem = new InvoiceJeevesItems();
                //        invoiceItem.invoice_no = row["INVOICE_NO"] != DBNull.Value ? Convert.ToString(row["INVOICE_NO"]) : string.Empty;
                //        invoiceItem.item_code = row["ITEM_CODE"] != DBNull.Value ? Convert.ToString(row["ITEM_CODE"]) : string.Empty;
                //        invoiceItem.item_name = row["ITEM_NAME"] != DBNull.Value ? Convert.ToString(row["ITEM_NAME"]) : string.Empty;
                //        invoiceItem.quantity = row["QUANTITY"] != DBNull.Value ? Convert.ToDecimal(row["QUANTITY"]) : 0;
                //        invoiceItem.asn_quantity = row["ASN_QUANTITY"] != DBNull.Value ? Convert.ToDecimal(row["ASN_QUANTITY"]) : 0;
                //        invoiceItem.accepted_quantity = row["ACCEPTED_QUANTITY"] != DBNull.Value ? Convert.ToDecimal(row["ACCEPTED_QUANTITY"]) : 0;
                //        invoiceItem.return_quantity = row["RETURN_QUANTITY"] != DBNull.Value ? Convert.ToDecimal(row["RETURN_QUANTITY"]) : 0;
                //        invoiceItem.amount = row["AMOUNT"] != DBNull.Value ? Convert.ToDecimal(row["AMOUNT"]) : 0;
                //        invoiceItem.sub_total = row["SUB_TOTAL"] != DBNull.Value ? Convert.ToDecimal(row["SUB_TOTAL"]) : 0;
                //        invoiceItem.tax = row["TAX"] != DBNull.Value ? Convert.ToDecimal(row["TAX"]) : 0;
                //        invoiceItem.attachment = row["ATTACHMENTS"] != DBNull.Value ? Convert.ToString(row["ATTACHMENTS"]) : string.Empty;
                //        listDispatcInvoiceItems.Add(invoiceItem);
                //    }
                //}

                //if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                //{
                //    foreach (DataRow row in ds.Tables[0].Rows)
                //    {
                //        InvoiceJeevesScheduleDetails invoiceDetail = new InvoiceJeevesScheduleDetails();
                //        invoiceDetail.invoice_no = row["INVOICE_NO"] != DBNull.Value ? Convert.ToString(row["INVOICE_NO"]) : string.Empty;
                //        invoiceDetail.invoice_type = row["INVOICE_TYPE"] != DBNull.Value ? Convert.ToString(row["INVOICE_TYPE"]) : string.Empty;
                //        invoiceDetail.invoice_date = row["INVOICE_DATE"] != DBNull.Value ? Convert.ToString(row["INVOICE_DATE"]) : string.Empty;
                //        invoiceDetail.invoice_quantity = row["INVOICE_QUANTITY"] != DBNull.Value ? Convert.ToDecimal(row["INVOICE_QUANTITY"]) : 0;
                //        invoiceDetail.remaining_quantity = row["REMAINING_QUANTITY"] != DBNull.Value ? Convert.ToDecimal(row["REMAINING_QUANTITY"]) : 0;
                //        invoiceDetail.invoice_amount = row["INVOICE_AMOUNT"] != DBNull.Value ? Convert.ToDecimal(row["INVOICE_AMOUNT"]) : 0;
                //        invoiceDetail.entity = row["ENTITY"] != DBNull.Value ? Convert.ToString(row["ENTITY"]) : string.Empty;
                //        invoiceDetail.po_number = row["PO_NUMBER"] != DBNull.Value ? Convert.ToString(row["PO_NUMBER"]) : string.Empty;
                //        invoiceDetail.asn_code = row["ASN_CODE"] != DBNull.Value ? Convert.ToString(row["ASN_CODE"]) : string.Empty;
                //        invoiceDetail.billed_to = row["BILLED_TO"] != DBNull.Value ? Convert.ToString(row["BILLED_TO"]) : string.Empty;
                //        invoiceDetail.vendor_code = row["VENDOR_CODE"] != DBNull.Value ? Convert.ToString(row["VENDOR_CODE"]) : string.Empty;
                //        invoiceDetail.destination_asset_code = row["DESTINATION_ASSET_CODE"] != DBNull.Value ? Convert.ToString(row["DESTINATION_ASSET_CODE"]) : string.Empty;
                //        invoiceDetail.vendor_site_code = row["VENDOR_SITE_CODE"] != DBNull.Value ? Convert.ToString(row["VENDOR_SITE_CODE"]) : string.Empty;
                //        invoiceDetail.vendor_name = row["VENDOR_NAME"] != DBNull.Value ? Convert.ToString(row["VENDOR_NAME"]) : string.Empty;
                //        invoiceDetail.shipped_from = row["SHIPPED_FROM"] != DBNull.Value ? Convert.ToString(row["SHIPPED_FROM"]) : string.Empty;
                //        invoiceDetail.shipped_to = row["SHIPPED_TO"] != DBNull.Value ? Convert.ToString(row["SHIPPED_TO"]) : string.Empty;
                //        invoiceDetail.type_of_stock = row["TYPE_OF_STOCK"] != DBNull.Value ? Convert.ToString(row["TYPE_OF_STOCK"]) : string.Empty;


                //        //poDetail = PRMJeevesOracleService.GetPOObject(row);
                //        invoiceDetail.po_line_item = listDispatcInvoiceItems.Where(p => p.invoice_no == invoiceDetail.invoice_no).ToList();
                //        listinvoiceDetail.Add(invoiceDetail);
                //    }
                //}


                string json = JsonConvert.SerializeObject(listinvoiceDetail);


                userToken = GetJeevesToken();
                if (userToken.Status == "200")
                {
                    var url = ConfigurationManager.AppSettings["JEEVES_UPDATE_INVOICE"];

                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                           | SecurityProtocolType.Tls11
                           | SecurityProtocolType.Tls12
                           | SecurityProtocolType.Ssl3;
                    var httpRequest = (HttpWebRequest)WebRequest.Create(url);
                    httpRequest.Method = "POST";

                    httpRequest.Timeout = -1;
                    httpRequest.ContentType = "application/json";

                    httpRequest.Headers.Add("Authorization", "Bearer " + userToken.AccessToken);
                    byte[] data = Encoding.ASCII.GetBytes(json);
                    httpRequest.ContentLength = data.Length;
                    using (var streamWriter = new StreamWriter(httpRequest.GetRequestStream()))
                    {

                        streamWriter.Write(json);
                    }
                    response.Message = json;

                    dynamic myObject = null;
                    var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                    using (StreamReader Reader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        myObject = JsonConvert.DeserializeObject(Reader.ReadToEnd());
                    }

                    var status = (int)httpResponse.StatusCode;
                    if (status == 200)
                    {
                        response.ErrorMessage = "";
                        return response;

                    }
                    else if (myObject.error_code.ToString() == "BAD_REQUEST")
                    {
                        response.ErrorMessage = myObject.error_message.ToString();
                        return response;
                    }
                }
                else if (userToken.Status == "400")
                {
                    response.ErrorMessage = "Unauthorized User";
                    return response;
                }


            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
                //response.Message = ex.Message;
            }


            return response;
        }
        public UserToken F1RequirementSave(List<F1Requirement> f1Requirement)
        {
            var request = new System.Web.HttpContextWrapper(System.Web.HttpContext.Current);
            var apiKey = request.Request.Headers.GetValues("jwtToken");

            var validateToken = AuthenticateJwt.GetPrincipal(apiKey[0]);


            UserToken userToken = new UserToken();

            try
            {

                PRMUploadService prmUploadService = new PRMUploadService();
                DataTable dt = new DataTable();
                DataTable tempDT1 = new DataTable();
                tempDT1.Clear();
                List<string> columns = "ENTITY;TITLE;CASE_ID;TYPE_OF_STOCK;DELIVERY_LOCATION;REQUIRED_QUANTITY;PRODUCT_CODE;IS_PROCESSED;ROW_ID;FROM_INTEGRATION;BRAND".Split(';').ToList();
                foreach (var column in columns)
                {
                    tempDT1.Columns.Add(column);
                }

                foreach (var x in f1Requirement)
                {
                    foreach (var y in x.requirement_line_items)
                    {
                        var dr = tempDT1.NewRow(); // have new row on each iteration
                        dr["DELIVERY_LOCATION"] = x.delivery_location;
                        dr["PRODUCT_CODE"] = y.product_code;
                        dr["ENTITY"] = x.entity;
                        dr["TITLE"] = x.rfq_title;
                        dr["CASE_ID"] = x.jobsheet_id;
                        dr["TYPE_OF_STOCK"] = x.type_of_stock;
                        dr["REQUIRED_QUANTITY"] = y.required_quantity;
                        tempDT1.Rows.Add(dr);
                    }
                }

                string json = JsonConvert.SerializeObject(tempDT1);
                logger.Debug("SAP REQUIREMENTS:" + JsonConvert.SerializeObject(tempDT1));


                dt = (DataTable)JsonConvert.DeserializeObject(json, (typeof(DataTable)));


                if (validateToken != null && validateToken.Identity.IsAuthenticated)
                {
                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        var prChunks = dt.AsEnumerable().ToList().Chunk(10000);
                        foreach (var chunk in prChunks)
                        {

                            DataTable tempDT = new DataTable();
                            tempDT.Clear();
                            tempDT.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                            foreach (var column in columns)
                            {
                                tempDT.Columns.Add(column);
                            }

                            tempDT.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                            List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                            foreach (DataColumn col in tempDT.Columns)
                            {
                                columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                            }

                            foreach (var row in chunk)
                            {
                                var newRow = tempDT.NewRow();
                                newRow["JOB_ID"] = guid1;
                                newRow["ENTITY"] = GetDataRowVaue(row, "ENTITY");
                                newRow["TITLE"] = GetDataRowVaue(row, "TITLE");
                                newRow["DELIVERY_LOCATION"] = GetDataRowVaue(row, "DELIVERY_LOCATION");
                                newRow["CASE_ID"] = GetDataRowVaue(row, "CASE_ID");
                                newRow["TYPE_OF_STOCK"] = GetDataRowVaue(row, "TYPE_OF_STOCK");
                                string qtyReq = !string.IsNullOrEmpty(GetDataRowVaue(row, "REQUIRED_QUANTITY")) ? GetDataRowVaue(row, "REQUIRED_QUANTITY").Replace(",", "") : "0";
                                newRow["REQUIRED_QUANTITY"] = qtyReq;
                                newRow["PRODUCT_CODE"] = GetDataRowVaue(row, "PRODUCT_CODE");
                                newRow["IS_PROCESSED"] = 0;
                                newRow["DATE_CREATED"] = DateTime.UtcNow;
                                newRow["ROW_ID"] = tempDT.Rows.Count + 1;
                                newRow["FROM_INTEGRATION"] = 1;
                                newRow["BRAND"] = GetDataRowVaue(row, "BRAND");

                                tempDT.Rows.Add(newRow);
                            }
                            bizClass.BulkInsert(tempDT, "[dbo].[SAP_REQUIREMENT_DETAILS]", columnMappings);
                            tempDT.Rows.Clear();
                        }

                    }

                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_JOB_ID", guid);
                    bizClass.SelectList("erp_process_sap_demand_request_details", sd);
                    //ERROR MESSAGES
                    string query = $@"select * from UPLOAD_MODULE_ERROR_DETAILS where job_id = '{guid}'";
                    List<ModuleBasedError> details = new List<ModuleBasedError>();
                    CORE.DataNamesMapper<ModuleBasedError> mapper = new CORE.DataNamesMapper<ModuleBasedError>();
                    var dataTable = sqlHelper.SelectQuery(query);
                    details = mapper.Map(dataTable).ToList();
                    string combindedString = string.Join(";", details.Select(t => t.REASON.Remove(t.REASON.Length - 1)).ToArray());

                    if (details.Count > 0)
                    {
                        userToken.Message = combindedString;
                        userToken.Success = false;
                    }
                    else
                    {
                        userToken.Message = "Saved Successfully";
                    }
                }
                else
                {
                    userToken.Message = "You are Unauthorized to connect PRM.";
                    userToken.Success = false;
                }
            }
            catch (Exception ex)
            {
                userToken.ErrorMessage = ex.Message;
                userToken.Message = ex.Message;
                userToken.Success = false;
            }

            return userToken;
        }

        public UserToken F1ProductSave(List<F1Product> f1Product)
        {
            var request = new System.Web.HttpContextWrapper(System.Web.HttpContext.Current);
            var apiKey = request.Request.Headers.GetValues("jwtToken");

            var validateToken = AuthenticateJwt.GetPrincipal(apiKey[0]);


            UserToken userToken = new UserToken();

            try
            {

                PRMUploadService prmUploadService = new PRMUploadService();
                DataTable dt = new DataTable();
                DataTable tempDT1 = new DataTable();
                tempDT1.Clear();
                List<string> columns = "PRODUCT_CODE;EPN;PRODUCT_NAME;PRODUCT_DESCRIPTION;CATEGORY_NAME;SERVICE_TYPE;SUPER_CATEGORY;SUB_CATEGORY;ITEM_REG_STATUS;MODEL_ID;MODEL_NAME;BRAND_PART_CODE;BRAND;BRAND_PRODUCT_CATEGORY_cODE;JSN;FSN;HSN_CODE;IMEI_NO;ESN_NO;ITEM_SERIALLZED_STATUS;CUSTOMER_PRICE;SF_PRICE;LANDING_PRICE;STOCK_TRANSPORTER_PRICE;HANDLING_CHARGES;ITEM_WEIGHT;UOM;WARRANTY;PRODUCT_LIFE;WARRANTY_COVERAGE;IS_CONSUMABLE;IS_CHARGABLE;IS_RETURNABLE;IS_HIGHVALUE;IS_SERIALIZED;IS_REPLACEMENT;IS_ADVANCE_REPLACEMENT;REPAIR_LEVEL;PRIORITY_LEVEL;DISCOUNT;CONDITION;ENTITY;SUB_ENTITY;VERTICAL;ROW_ID;FROM_INTEGRATION".Split(';').ToList();
                foreach (var column in columns)
                {
                    tempDT1.Columns.Add(column);
                }

                foreach (var x in f1Product)
                {
                    var dr = tempDT1.NewRow(); // have new row on each iteration
                    dr["PRODUCT_CODE"] = x.product_code;
                    dr["EPN"] = x.epn;
                    dr["PRODUCT_NAME"] = x.product_name;
                    dr["PRODUCT_DESCRIPTION"] = x.product_description;
                    dr["CATEGORY_NAME"] = x.category_name;
                    dr["SERVICE_TYPE"] = x.service_type;
                    dr["SUPER_CATEGORY"] = x.super_category;
                    dr["SUB_CATEGORY"] = x.sub_category;
                    dr["ITEM_REG_STATUS"] = x.item_reg_status;
                    dr["MODEL_ID"] = x.model_id;
                    dr["MODEL_NAME"] = x.model_name;
                    dr["BRAND_PART_CODE"] = x.brand_part_code;
                    dr["BRAND"] = x.brand;
                    dr["BRAND_PRODUCT_CATEGORY_cODE"] = x.brand_product_category_code;
                    dr["JSN"] = x.jsn;
                    dr["FSN"] = x.fsn;
                    dr["HSN_CODE"] = x.hsn_code;
                    dr["IMEI_NO"] = x.imei_no;
                    dr["ESN_NO"] = x.esn_no;
                    dr["ITEM_SERIALLZED_STATUS"] = x.item_serialized_status;
                    dr["CUSTOMER_PRICE"] = x.customer_price;
                    dr["SF_PRICE"] = x.sf_price;
                    dr["LANDING_PRICE"] = x.landing_price;
                    dr["STOCK_TRANSPORTER_PRICE"] = x.stock_transporter_price;
                    dr["HANDLING_CHARGES"] = x.handling_charges;
                    dr["ITEM_WEIGHT"] = x.item_weight;
                    dr["UOM"] = x.uom;
                    dr["WARRANTY"] = x.warranty;
                    dr["PRODUCT_LIFE"] = x.product_life;
                    dr["WARRANTY_COVERAGE"] = x.warranty_coverage;
                    dr["IS_CONSUMABLE"] = x.is_consumable;
                    dr["IS_CHARGABLE"] = x.is_chargable;
                    dr["IS_RETURNABLE"] = x.is_returnable;
                    dr["IS_HIGHVALUE"] = x.is_highvalue;
                    dr["IS_SERIALIZED"] = x.is_serialized;
                    dr["IS_REPLACEMENT"] = x.is_replacement;
                    dr["IS_ADVANCE_REPLACEMENT"] = x.is_advance_replacement;
                    dr["REPAIR_LEVEL"] = x.repair_level;
                    dr["PRIORITY_LEVEL"] = x.priority_level;
                    dr["DISCOUNT"] = x.discount;
                    dr["CONDITION"] = x.condition;
                    dr["ENTITY"] = x.entity;
                    dr["SUB_ENTITY"] = x.sub_entity;
                    dr["VERTICAL"] = x.vertical;
                    tempDT1.Rows.Add(dr);
                }

                string json = JsonConvert.SerializeObject(tempDT1);
                logger.Debug("SAP REQUIREMENTS:" + JsonConvert.SerializeObject(tempDT1));


                dt = (DataTable)JsonConvert.DeserializeObject(json, (typeof(DataTable)));


                if (validateToken != null && validateToken.Identity.IsAuthenticated)
                {
                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        var prChunks = dt.AsEnumerable().ToList().Chunk(10000);
                        foreach (var chunk in prChunks)
                        {

                            DataTable tempDT = new DataTable();
                            tempDT.Clear();
                            tempDT.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                            foreach (var column in columns)
                            {
                                tempDT.Columns.Add(column);
                            }
                            tempDT.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));


                            List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                            foreach (DataColumn col in tempDT.Columns)
                            {
                                columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                            }


                            foreach (var row in chunk)
                            {
                                var newRow = tempDT.NewRow();
                                newRow["JOB_ID"] = guid1;
                                newRow["PRODUCT_CODE"] = GetDataRowVaue(row, "PRODUCT_CODE");
                                newRow["EPN"] = GetDataRowVaue(row, "EPN");
                                newRow["PRODUCT_NAME"] = GetDataRowVaue(row, "PRODUCT_NAME");
                                newRow["PRODUCT_DESCRIPTION"] = GetDataRowVaue(row, "PRODUCT_DESCRIPTION");
                                newRow["CATEGORY_NAME"] = GetDataRowVaue(row, "CATEGORY_NAME");
                                newRow["SERVICE_TYPE"] = GetDataRowVaue(row, "SERVICE_TYPE");
                                newRow["SUPER_CATEGORY"] = GetDataRowVaue(row, "SUPER_CATEGORY");
                                newRow["SUB_CATEGORY"] = GetDataRowVaue(row, "SUB_CATEGORY");
                                newRow["ITEM_REG_STATUS"] = GetDataRowVaue(row, "ITEM_REG_STATUS");
                                newRow["MODEL_ID"] = GetDataRowVaue(row, "MODEL_ID");
                                newRow["MODEL_NAME"] = GetDataRowVaue(row, "MODEL_NAME");
                                newRow["BRAND_PART_CODE"] = GetDataRowVaue(row, "BRAND_PART_CODE");
                                newRow["BRAND"] = GetDataRowVaue(row, "BRAND");
                                newRow["BRAND_PRODUCT_CATEGORY_cODE"] = GetDataRowVaue(row, "BRAND_PRODUCT_CATEGORY_cODE");
                                newRow["JSN"] = GetDataRowVaue(row, "JSN");
                                newRow["FSN"] = GetDataRowVaue(row, "FSN");
                                newRow["HSN_CODE"] = GetDataRowVaue(row, "HSN_CODE");
                                newRow["IMEI_NO"] = GetDataRowVaue(row, "IMEI_NO");
                                newRow["ESN_NO"] = GetDataRowVaue(row, "ESN_NO");
                                newRow["ITEM_SERIALLZED_STATUS"] = GetDataRowVaue(row, "ITEM_SERIALLZED_STATUS");
                                newRow["CUSTOMER_PRICE"] = GetDataRowVaue(row, "CUSTOMER_PRICE");
                                newRow["SF_PRICE"] = GetDataRowVaue(row, "SF_PRICE");
                                newRow["LANDING_PRICE"] = GetDataRowVaue(row, "LANDING_PRICE");
                                newRow["STOCK_TRANSPORTER_PRICE"] = GetDataRowVaue(row, "STOCK_TRANSPORTER_PRICE");
                                newRow["HANDLING_CHARGES"] = GetDataRowVaue(row, "HANDLING_CHARGES");
                                newRow["ITEM_WEIGHT"] = GetDataRowVaue(row, "ITEM_WEIGHT");
                                newRow["UOM"] = GetDataRowVaue(row, "UOM");
                                newRow["WARRANTY"] = GetDataRowVaue(row, "WARRANTY");
                                newRow["PRODUCT_LIFE"] = GetDataRowVaue(row, "PRODUCT_LIFE");
                                newRow["WARRANTY_COVERAGE"] = GetDataRowVaue(row, "WARRANTY_COVERAGE");
                                newRow["IS_CONSUMABLE"] = GetDataRowVaue(row, "IS_CONSUMABLE");
                                newRow["IS_CHARGABLE"] = GetDataRowVaue(row, "IS_CHARGABLE");
                                newRow["IS_RETURNABLE"] = GetDataRowVaue(row, "IS_RETURNABLE");
                                newRow["IS_HIGHVALUE"] = GetDataRowVaue(row, "IS_HIGHVALUE");
                                newRow["IS_SERIALIZED"] = GetDataRowVaue(row, "IS_SERIALIZED");
                                newRow["IS_REPLACEMENT"] = GetDataRowVaue(row, "IS_REPLACEMENT");
                                newRow["IS_ADVANCE_REPLACEMENT"] = GetDataRowVaue(row, "IS_ADVANCE_REPLACEMENT");
                                newRow["REPAIR_LEVEL"] = GetDataRowVaue(row, "REPAIR_LEVEL");
                                newRow["PRIORITY_LEVEL"] = GetDataRowVaue(row, "PRIORITY_LEVEL");
                                newRow["DISCOUNT"] = GetDataRowVaue(row, "DISCOUNT");
                                newRow["CONDITION"] = GetDataRowVaue(row, "CONDITION");
                                newRow["ENTITY"] = "F1";
                                newRow["SUB_ENTITY"] = GetDataRowVaue(row, "SUB_ENTITY");
                                newRow["VERTICAL"] = GetDataRowVaue(row, "VERTICAL");
                                newRow["DATE_CREATED"] = DateTime.UtcNow;
                                newRow["ROW_ID"] = tempDT.Rows.Count + 1;
                                newRow["FROM_INTEGRATION"] = 1;


                                tempDT.Rows.Add(newRow);
                            }

                            bizClass.BulkInsert(tempDT, "[dbo].[SAP_MATERIAL_DETAILS]", columnMappings);
                            tempDT.Rows.Clear();


                        }

                    }

                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_JOB_ID", guid);
                    bizClass.SelectList("erp_process_sap_material_details", sd);
                    //ERROR MESSAGES
                    string query = $@"select * from UPLOAD_MODULE_ERROR_DETAILS where job_id = '{guid}'";
                    List<ModuleBasedError> details = new List<ModuleBasedError>();
                    CORE.DataNamesMapper<ModuleBasedError> mapper = new CORE.DataNamesMapper<ModuleBasedError>();
                    var dataTable = sqlHelper.SelectQuery(query);
                    details = mapper.Map(dataTable).ToList();
                    string combindedString = string.Join(";", details.Select(t => t.REASON.Remove(t.REASON.Length - 1)).ToArray());

                    if (details.Count > 0)
                    {
                        userToken.Message = combindedString;
                        userToken.Success = false;
                    }
                    else
                    {
                        userToken.Message = "Saved Successfully";
                    }
                }
                else
                {
                    userToken.Message = "You are Unauthorized to connect PRM.";
                    userToken.Success = false;
                }
            }
            catch (Exception ex)
            {
                userToken.ErrorMessage = ex.Message;
                userToken.Message = ex.Message;
                userToken.Success = false;
            }

            return userToken;
        }

        public UserToken F1ProductBOMSave(List<F1Product> f1Product)
        {
            var request = new System.Web.HttpContextWrapper(System.Web.HttpContext.Current);
            var apiKey = request.Request.Headers.GetValues("jwtToken");

            var validateToken = AuthenticateJwt.GetPrincipal(apiKey[0]);


            UserToken userToken = new UserToken();

            try
            {

                PRMUploadService prmUploadService = new PRMUploadService();
                DataTable dt = new DataTable();
                DataTable tempDT1 = new DataTable();
                tempDT1.Clear();
                List<string> columns = "PRODUCT_CODE;MODEL_ID;ENTITY".Split(';').ToList();
                foreach (var column in columns)
                {
                    tempDT1.Columns.Add(column);
                }

                foreach (var x in f1Product)
                {
                    var dr = tempDT1.NewRow(); // have new row on each iteration
                    dr["PRODUCT_CODE"] = x.product_code;
                    dr["MODEL_ID"] = x.model_id;
                    dr["ENTITY"] = "F1";
                    tempDT1.Rows.Add(dr);
                }

                string json = JsonConvert.SerializeObject(tempDT1);
                logger.Debug("SAP REQUIREMENTS:" + JsonConvert.SerializeObject(tempDT1));


                dt = (DataTable)JsonConvert.DeserializeObject(json, (typeof(DataTable)));


                if (validateToken != null && validateToken.Identity.IsAuthenticated)
                {
                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        var prChunks = dt.AsEnumerable().ToList().Chunk(10000);
                        foreach (var chunk in prChunks)
                        {

                            DataTable tempDT = new DataTable();
                            tempDT.Clear();
                            tempDT.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                            foreach (var column in columns)
                            {
                                tempDT.Columns.Add(column);
                            }
                            tempDT.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));


                            List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                            foreach (DataColumn col in tempDT.Columns)
                            {
                                columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                            }


                            foreach (var row in chunk)
                            {
                                var newRow = tempDT.NewRow();
                                newRow["JOB_ID"] = guid1;
                                newRow["PRODUCT_CODE"] = GetDataRowVaue(row, "PRODUCT_CODE");
                                newRow["MODEL_ID"] = GetDataRowVaue(row, "MODEL_ID");
                                newRow["ENTITY"] = GetDataRowVaue(row, "ENTITY");
                                newRow["DATE_CREATED"] = DateTime.UtcNow;


                                tempDT.Rows.Add(newRow);
                            }

                            bizClass.BulkInsert(tempDT, "[dbo].[SAP_MATERIAL_DETAILS]", columnMappings);
                            tempDT.Rows.Clear();


                        }

                    }

                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_JOB_ID", guid);
                    bizClass.SelectList("erp_process_sap_update_material_details", sd);
                    userToken.Message = "Products Saved Successfully.";
                }
                else
                {
                    userToken.Message = "You are Unauthorized to connect PRM.";
                    userToken.Success = false;
                }
            }
            catch (Exception ex)
            {
                userToken.ErrorMessage = ex.Message;
                userToken.Message = ex.Message;
                userToken.Success = false;
            }

            return userToken;
        }

        public UserToken F1ProductAlternateSave(List<F1Product> f1Product)
        {
            var request = new System.Web.HttpContextWrapper(System.Web.HttpContext.Current);
            var apiKey = request.Request.Headers.GetValues("jwtToken");

            var validateToken = AuthenticateJwt.GetPrincipal(apiKey[0]);


            UserToken userToken = new UserToken();

            try
            {

                PRMUploadService prmUploadService = new PRMUploadService();
                DataTable dt = new DataTable();
                DataTable tempDT1 = new DataTable();
                tempDT1.Clear();
                List<string> columns = "PRODUCT_CODE;EPN;ENTITY".Split(';').ToList();
                foreach (var column in columns)
                {
                    tempDT1.Columns.Add(column);
                }

                foreach (var x in f1Product)
                {
                    var dr = tempDT1.NewRow(); // have new row on each iteration
                    dr["PRODUCT_CODE"] = x.product_code;
                    dr["EPN"] = x.epn;
                    dr["ENTITY"] = "F1";
                    tempDT1.Rows.Add(dr);
                }

                string json = JsonConvert.SerializeObject(tempDT1);
                logger.Debug("SAP REQUIREMENTS:" + JsonConvert.SerializeObject(tempDT1));


                dt = (DataTable)JsonConvert.DeserializeObject(json, (typeof(DataTable)));


                if (validateToken != null && validateToken.Identity.IsAuthenticated)
                {
                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        var prChunks = dt.AsEnumerable().ToList().Chunk(10000);
                        foreach (var chunk in prChunks)
                        {

                            DataTable tempDT = new DataTable();
                            tempDT.Clear();
                            tempDT.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                            foreach (var column in columns)
                            {
                                tempDT.Columns.Add(column);
                            }
                            tempDT.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));


                            List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                            foreach (DataColumn col in tempDT.Columns)
                            {
                                columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                            }


                            foreach (var row in chunk)
                            {
                                var newRow = tempDT.NewRow();
                                newRow["JOB_ID"] = guid1;
                                newRow["PRODUCT_CODE"] = GetDataRowVaue(row, "PRODUCT_CODE");
                                newRow["EPN"] = GetDataRowVaue(row, "EPN");
                                newRow["ENTITY"] = GetDataRowVaue(row, "ENTITY");
                                newRow["DATE_CREATED"] = DateTime.UtcNow;


                                tempDT.Rows.Add(newRow);
                            }

                            bizClass.BulkInsert(tempDT, "[dbo].[SAP_MATERIAL_DETAILS]", columnMappings);
                            tempDT.Rows.Clear();


                        }

                    }

                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_JOB_ID", guid);
                    bizClass.SelectList("erp_process_sap_update_material_details", sd);
                    userToken.Message = "Products Saved Successfully.";
                }
                else
                {
                    userToken.Message = "You are Unauthorized to connect PRM.";
                    userToken.Success = false;
                }
            }
            catch (Exception ex)
            {
                userToken.ErrorMessage = ex.Message;
                userToken.Message = ex.Message;
                userToken.Success = false;
            }

            return userToken;
        }



        public UserToken F1GRNSave(List<F1GRN> f1GRN)
        {
            var request = new System.Web.HttpContextWrapper(System.Web.HttpContext.Current);
            var apiKey = request.Request.Headers.GetValues("jwtToken");

            var validateToken = AuthenticateJwt.GetPrincipal(apiKey[0]);


            UserToken userToken = new UserToken();

            try
            {

                PRMUploadService prmUploadService = new PRMUploadService();
                DataTable dt = new DataTable();
                DataTable tempDT1 = new DataTable();
                tempDT1.Clear();
                List<string> columns1 = "PO_NUMBER;PO_LINE_ITEM;TYPE_OF_STOCK;GRN_NO;GRN_DATE;GRN_STATUS;INVOICE_DOC;FOC_REQ_NUM;SHIPPED_LOCATION_CODE;VENDOR_SITE_CODE;INVOICE_NO;INVOICE_DATE;INVOICE_AMOUNT;PO_LINE_ITEM_DESC;PO_LINE_ITEM_SKU_ID;PO_QTY;RECEIVED_QTY;LOST_QTY;BAD_QTY".Split(';').ToList();

                List<string> columns = "EBELN;EBELP;TYPE_OF_STOCK;MBLNR;CPUDT_MKPF;GRN_STATUS;INVOICE_DOC;FOC_REQ_NUM;LIFNR;VENDOR_SITE_CODE;INVOICE_NO;INVOICE_DATE;INVOICE_AMOUNT;MAKTX;MATNR;PO_QTY;MENGE;LOST_QTY;BAD_QTY;ZEILE;ROW_ID".Split(';').ToList();
                foreach (var column in columns1)
                {
                    tempDT1.Columns.Add(column);
                }

                foreach (var x in f1GRN)
                {
                    foreach (var y in x.po_line_items)
                    {
                        Response resp = new Response();
                        string attachIds = string.Empty;
                        if (!string.IsNullOrEmpty(x.invoice_doc)) 
                        {
                            string[] multiAttachments = x.invoice_doc.Split(',');
                            if (multiAttachments != null && multiAttachments.Length > 0) {
                                foreach (var attach in multiAttachments) 
                                { 
                                    FileUpload fu = new FileUpload();
                                    //var data = attach.Substring(0, 5);
                                    fu.FileName = ReturnExt(attach);
                                    fu.FileBase64 = attach;
                                    resp = SaveFileUpload(fu);
                                    attachIds += Convert.ToString(resp.ObjectID) + ",";
                                }
                            }
                            attachIds = attachIds.Substring(0, attachIds.Length - 1);
                        }
                        var dr = tempDT1.NewRow(); // have new row on each iteration
                        dr["PO_LINE_ITEM_DESC"] = y.po_line_item_desc;
                        dr["PO_LINE_ITEM_SKU_ID"] = y.po_line_item_sku_id;
                        dr["PO_QTY"] = y.po_qty;
                        dr["RECEIVED_QTY"] = y.received_qty;
                        dr["LOST_QTY"] = y.lost_qty;
                        dr["BAD_QTY"] = y.bad_qty;
                        dr["PO_NUMBER"] = x.po_number;
                        dr["PO_LINE_ITEM"] = genLineItemNum(x.po_line_items.IndexOf(y) + 1);
                        dr["TYPE_OF_STOCK"] = x.type_of_stock;
                        dr["GRN_NO"] = x.grn_no;
                        dr["GRN_DATE"] = x.grn_date;
                        dr["GRN_STATUS"] = x.grn_status;
                        dr["INVOICE_DOC"] = attachIds;
                        dr["FOC_REQ_NUM"] = x.foc_req_num;
                        dr["SHIPPED_LOCATION_CODE"] = x.shipped_location_code;
                        dr["VENDOR_SITE_CODE"] = x.vendor_site_code;
                        dr["INVOICE_NO"] = x.invoice_no;
                        dr["INVOICE_DATE"] = x.invoice_date;
                        dr["INVOICE_AMOUNT"] = x.invoice_amount;
                        tempDT1.Rows.Add(dr);
                    }
                }

                string json = JsonConvert.SerializeObject(tempDT1);


                dt = (DataTable)JsonConvert.DeserializeObject(json, (typeof(DataTable)));


                if (validateToken != null && validateToken.Identity.IsAuthenticated)
                {
                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        var prChunks = dt.AsEnumerable().ToList().Chunk(10000);
                        foreach (var chunk in prChunks)
                        {

                            DataTable tempDT = new DataTable();
                            tempDT.Clear();
                            tempDT.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                            foreach (var column in columns)
                            {
                                tempDT.Columns.Add(column);
                            }

                            tempDT.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                            List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                            foreach (DataColumn col in tempDT.Columns)
                            {
                                columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                            }

                            foreach (var row in chunk)
                            {
                                var newRow = tempDT.NewRow();
                                newRow["JOB_ID"] = guid1;
                                newRow["EBELN"] = GetDataRowVaue(row, "PO_NUMBER");
                                newRow["EBELP"] = GetDataRowVaue(row, "PO_LINE_ITEM");
                                newRow["TYPE_OF_STOCK"] = GetDataRowVaue(row, "TYPE_OF_STOCK");
                                newRow["MBLNR"] = GetDataRowVaue(row, "GRN_NO");
                                newRow["GRN_STATUS"] = GetDataRowVaue(row, "GRN_STATUS");
                                newRow["INVOICE_DOC"] = GetDataRowVaue(row, "INVOICE_DOC");
                                newRow["FOC_REQ_NUM"] = GetDataRowVaue(row, "FOC_REQ_NUM");
                                newRow["LIFNR"] = GetDataRowVaue(row, "SHIPPED_LOCATION_CODE");
                                newRow["VENDOR_SITE_CODE"] = GetDataRowVaue(row, "VENDOR_SITE_CODE");
                                newRow["INVOICE_NO"] = GetDataRowVaue(row, "INVOICE_NO");
                                newRow["MAKTX"] = GetDataRowVaue(row, "PO_LINE_ITEM_DESC");
                                newRow["MATNR"] = GetDataRowVaue(row, "PO_LINE_ITEM_SKU_ID");
                                string invoiceAmount = !string.IsNullOrEmpty(GetDataRowVaue(row, "INVOICE_AMOUNT")) ? GetDataRowVaue(row, "INVOICE_AMOUNT").Replace(",", "") : "0";
                                newRow["INVOICE_AMOUNT"] = invoiceAmount;
                                string poQty = !string.IsNullOrEmpty(GetDataRowVaue(row, "PO_QTY")) ? GetDataRowVaue(row, "PO_QTY").Replace(",", "") : "0";
                                newRow["PO_QTY"] = poQty;
                                string receivedQty = !string.IsNullOrEmpty(GetDataRowVaue(row, "RECEIVED_QTY")) ? GetDataRowVaue(row, "RECEIVED_QTY").Replace(",", "") : "0";
                                newRow["MENGE"] = receivedQty;
                                string lostQty = !string.IsNullOrEmpty(GetDataRowVaue(row, "LOST_QTY")) ? GetDataRowVaue(row, "LOST_QTY").Replace(",", "") : "0";
                                newRow["LOST_QTY"] = lostQty;
                                string badQty = !string.IsNullOrEmpty(GetDataRowVaue(row, "BAD_QTY")) ? GetDataRowVaue(row, "BAD_QTY").Replace(",", "") : "0";
                                newRow["BAD_QTY"] = badQty;
                                string grnDate = GetDataRowVaue(row, "GRN_DATE");
                                newRow["CPUDT_MKPF"] = (grnDate == "00000000" || grnDate == "0" || grnDate == "") ? null : grnDate;
                                string invoiceDate = GetDataRowVaue(row, "INVOICE_DATE");
                                newRow["INVOICE_DATE"] = (invoiceDate == "00000000" || invoiceDate == "0" || invoiceDate == "") ? null : invoiceDate;
                                newRow["ZEILE"] = newRow["EBELP"];
                                newRow["DATE_CREATED"] = DateTime.UtcNow;
                                newRow["ROW_ID"] = tempDT.Rows.Count + 1;


                                tempDT.Rows.Add(newRow);
                            }
                            bizClass.BulkInsert(tempDT, "[dbo].[SAP_GRN_DETAILS]", columnMappings);
                            tempDT.Rows.Clear();
                        }

                    }

                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_JOB_ID", guid);
                    bizClass.SelectList("erp_process_sap_grn_details", sd);
                    //ERROR MESSAGES
                    string query = $@"select * from UPLOAD_MODULE_ERROR_DETAILS where job_id = '{guid}'";
                    List<ModuleBasedError> details = new List<ModuleBasedError>();
                    CORE.DataNamesMapper<ModuleBasedError> mapper = new CORE.DataNamesMapper<ModuleBasedError>();
                    var dataTable = sqlHelper.SelectQuery(query);
                    details = mapper.Map(dataTable).ToList();
                    string combindedString = string.Join(";", details.Select(t => t.REASON.Remove(t.REASON.Length - 1)).ToArray());

                    if (details.Count > 0)
                    {
                        userToken.Message = combindedString;
                        userToken.Success = false;
                    }
                    else
                    {
                        userToken.Message = "Saved Successfully";
                    }

                    sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_JOB_ID", guid);
                    DataSet ds = sqlHelper.SelectList("cp_GetThreeWayMatchWorkflow", sd);
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        try
                        {
                            PRMWFService wf = new PRMWFService();
                            string sessionId = guid.ToString();
                            if (ds != null && ds.Tables[0].Rows.Count > 0)
                            {
                                string rowValue = ds.Tables[0].Rows[0]["CREATED_BY"].ToString();
                                Utilities.CreateSession(sessionId, Convert.ToInt32(rowValue), "WEB");
                                foreach (DataRow row in ds.Tables[0].Rows)
                                {
                                    POInvoice inv = new POInvoice();
                                    inv.INVOICE_ID = row["INVOICE_ID"] != DBNull.Value ? Convert.ToInt32(row["INVOICE_ID"]) : 0;
                                    inv.WF_ID = row["WF_ID"] != DBNull.Value ? Convert.ToInt32(row["WF_ID"]) : 0;
                                    inv.CreatedBy = row["CREATED_BY"] != DBNull.Value ? Convert.ToInt32(row["CREATED_BY"]) : 0;
                                    wf.AssignWorkflow(inv.WF_ID, inv.INVOICE_ID, inv.CreatedBy, sessionId, "", 0);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.Error("error while triggering workflow from GRN API for F1>>>>" + ex.Message);
                        }
                    }

                }
                else
                {
                    userToken.Message = "You are Unauthorized to connect PRM.";
                    userToken.Success = false;
                }
            }
            catch (Exception ex)
            {
                userToken.ErrorMessage = ex.Message;
                userToken.Message = ex.Message;
                userToken.Success = false;
            }

            return userToken;
        }

        public Response GetVendorF1Details()
        {
            var now = DateTime.Now;
            logger.Info("F1 Vendor Job Ran Successfully on >>" + now);
            UserToken userToken = new UserToken();
            Response response = new Response();

            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<VendorF1Details> listVendorDetail = new List<VendorF1Details>();
            try
            {
                CORE.DataNamesMapper<VendorF1Details> mapper = new CORE.DataNamesMapper<VendorF1Details>();

                string query = $@"select COMP_NAME AS COMPANY_NAME,concat(c.VENDOR_CODE,c.VENDOR_SITE_CODE) as VENDOR_CODE,'Vendor' as ASSOCIATE_TYPE_CODE,c.GST_NUMBER,
                            (SELECT TOP 1 U_CRED_ID from userverification where U_ID = v.U_ID and U_VERIFICATION_TYPE = 'PAN' ) AS PAN_NUMBER,
                            DESIGNATION AS CONTACT_DESIGNATION,U_EMAIL AS EMAIL_ID,U_PHONE AS MOBILE_NO,U_FNAME AS CONTACT_PERSON,ADDRESS,PINCODE,
                            (select  JSON_VALUE(value, '$.SHIPPING_ADDRESS') from OPENJSON(ADDITIONAL_VENDOR_INFORMATION)) AS SHIPPING_ADDRESS,
                            (select  JSON_VALUE(value, '$.SHIPPING_PINCODE') from OPENJSON(ADDITIONAL_VENDOR_INFORMATION)) AS SHIPPING_PINCODE,
                            'L2' AS ASSOCIATE_LEVEL,'5' AS VENDOR_LEAD_TIME,'2' AS DEFECTIVE_SERVICE,'F1 brand code |10' AS PEMITTED_BRANDS,ORACLE_VENDOR_ID AS ORACLE_CODE
                            from vendors V
                            left JOIN companygstinfo c on c.VENDOR_ID=v.U_ID where SALESPERSON = 'F1 Info Solutions' and IS_PROCESSED = 0";
                //DataSet ds = sqlHelper.ExecuteQuery(query);
                var dataset = sqlHelper.ExecuteQuery(query);
                listVendorDetail = mapper.Map(dataset.Tables[0]).ToList();

                //if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                //{
                //    foreach (DataRow row in ds.Tables[0].Rows)
                //    {
                //        VendorF1Details vendorDetail = new VendorF1Details();
                //        vendorDetail.vendorName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                //        vendorDetail.vendorCode = row["VENDOR_CODE"] != DBNull.Value ? Convert.ToString(row["VENDOR_CODE"]) : string.Empty;
                //        vendorDetail.associateTypeCode = row["ASSOCIATE_TYPE_CODE"] != DBNull.Value ? Convert.ToString(row["ASSOCIATE_TYPE_CODE"]) : string.Empty;
                //        vendorDetail.billingAddress = row["ADDRESS"] != DBNull.Value ? Convert.ToString(row["ADDRESS"]) : string.Empty;
                //        vendorDetail.billingPincode = row["PINCODE"] != DBNull.Value ? Convert.ToString(row["PINCODE"]) : string.Empty;
                //        vendorDetail.shippingAddress = row["SHIPPING_ADDRESS"] != DBNull.Value ? Convert.ToString(row["SHIPPING_ADDRESS"]) : string.Empty;
                //        vendorDetail.shippingPincode = row["SHIPPING_PINCODE"] != DBNull.Value ? Convert.ToString(row["SHIPPING_PINCODE"]) : string.Empty;
                //        vendorDetail.associateLevel = row["ASSOCIATE_LEVEL"] != DBNull.Value ? Convert.ToString(row["ASSOCIATE_LEVEL"]) : string.Empty;
                //        vendorDetail.vendorLeadTime = row["VENDOR_LEAD_TIME"] != DBNull.Value ? Convert.ToInt32(row["VENDOR_LEAD_TIME"]) : 0;
                //        vendorDetail.defectiveService = row["DEFECTIVE_SERVICE"] != DBNull.Value ? Convert.ToInt32(row["DEFECTIVE_SERVICE"]) : 0;
                //        vendorDetail.pemittedBrands = row["PEMITTED_BRANDS"] != DBNull.Value ? Convert.ToString(row["PEMITTED_BRANDS"]) : string.Empty;
                //        vendorDetail.gstinNumber = row["GST_NUMBER"] != DBNull.Value ? Convert.ToString(row["GST_NUMBER"]) : string.Empty;
                //        vendorDetail.panNumber = row["PAN_NUMBER"] != DBNull.Value ? Convert.ToString(row["PAN_NUMBER"]) : string.Empty;
                //        vendorDetail.contactPersonName = row["CONTACT_PERSON"] != DBNull.Value ? Convert.ToString(row["CONTACT_PERSON"]) : string.Empty;
                //        vendorDetail.contactPersonDesignation = row["CONTACT_DESIGNATION"] != DBNull.Value ? Convert.ToString(row["CONTACT_DESIGNATION"]) : string.Empty;
                //        vendorDetail.mobileNo = row["PHONE_NUMBER"] != DBNull.Value ? Convert.ToString(row["PHONE_NUMBER"]) : string.Empty;
                //        vendorDetail.emailId = row["EMAIL"] != DBNull.Value ? Convert.ToString(row["EMAIL"]) : string.Empty;
                //        listVendorDetail.Add(vendorDetail);
                //    }
                //}


                string json = JsonConvert.SerializeObject(listVendorDetail);


                userToken = GetF1Token();
                if (userToken.Status == "200")
                {
                    var url = ConfigurationManager.AppSettings["F1_VENDOR"];

                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                           | SecurityProtocolType.Tls11
                           | SecurityProtocolType.Tls12
                           | SecurityProtocolType.Ssl3;
                    var httpRequest = (HttpWebRequest)WebRequest.Create(url);
                    httpRequest.Method = "POST";

                    httpRequest.Timeout = -1;
                    httpRequest.ContentType = "application/json";

                    httpRequest.Headers.Add("Authorization", "Bearer " + userToken.AccessToken);
                    byte[] data = Encoding.ASCII.GetBytes(json);
                    httpRequest.ContentLength = data.Length;
                    using (var streamWriter = new StreamWriter(httpRequest.GetRequestStream()))
                    {

                        streamWriter.Write(json);
                    }
                    logger.Info("Token generated successfully and hitting the F1_VENDOR URL " + url + " for the job running on >>" + now);
                    dynamic myObject = null;
                    var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                    using (StreamReader Reader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        myObject = JsonConvert.DeserializeObject(Reader.ReadToEnd());
                    }
                    logger.Info("F1 Vendor Job Ran Successfully on >>" + now + "and got the response as >>>" + myObject.error_code.ToString());
                    response.Status = myObject.status.ToString();
                    if (response.Status == "500")
                    {
                        string[] errorArray = ((System.Collections.IEnumerable)myObject.errors).Cast<object>()
                                 .Select(x => x.ToString())
                                 .ToArray();
                        response.Message = string.Join(",", errorArray);
                        response.ErrorMessage = response.Message;
                        return response;
                    }
                    else if (response.Status == "200")
                    {
                        string poNumberString = myObject.poNumber.ToString();
                        response.Message = poNumberString;
                        response.ErrorMessage = response.Message;
                        return response;
                    }

                }
                else if (userToken.Status == "400")
                {
                    response.Message = "Unauthorized User";
                    response.ErrorMessage = response.Message;
                    return response;
                }

            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
                response.Message = ex.Message;
                logger.Error("Error in F1 Vendor Job>>>> "+ ex.Message + " on " + now);
            }
            return response;
        }

        public Response CreatePOF1Details(string ponumber)
        {
            List<POF1Details> listPoDetails = new List<POF1Details>();
            List<POF1Items> f1ItemDetails = new List<POF1Items>();

            UserToken userToken = new UserToken();
            Response response = new Response();

            try
            {
                CORE.DataNamesMapper<POF1Details> mapper = new CORE.DataNamesMapper<POF1Details>();
                CORE.DataNamesMapper<POF1Items> mapperItemList = new CORE.DataNamesMapper<POF1Items>();

                string query = $@"select distinct PO_NUMBER,concat(psd.VENDOR_CODE,psd.VENDOR_SITE_CODE) as VENDOR_CODE, 
                    CONVERT(VARCHAR(10), PO_DATE, 126) AS PO_DATE, (SELECT CODE FROM OracleConfigTable WHERE NAME LIKE CONCAT('%',SHIP_TO_LOCATION,'%')) AS SVC_CENTER_CODE, 
                    ITEM_SOURCE, SHIPPING_WAY from POScheduleDetails psd 
                    inner join vendors v on v.U_ID = psd.VENDOR_ID WHERE STATUS = 'CREATE' 
                    AND PO_QTY_STATUS = 'APPROVED' AND PO_NUMBER = '{ponumber}' GROUP BY PO_NUMBER,psd.VENDOR_CODE,PO_DATE,SHIP_TO_LOCATION 
                    ,ITEM_SOURCE,SHIPPING_WAY,psd.VENDOR_SITE_CODE; 
                    select distinct PO_NUMBER,Productcode as ITEM_CODE,ORDER_QTY as OPEN_QUANTITY,PO_LINE_ITEM, 
                    UNIT_PRICE from POScheduleDetails psd 
                    inner join cm_product cp on cp.ProductId = psd.PRODUCT_ID 
                    inner join vendors v on v.U_ID = psd.VENDOR_ID WHERE STATUS = 'CREATE' AND PO_QTY_STATUS = 'APPROVED' AND PO_NUMBER = '{ponumber}'";
                DataSet ds = sqlHelper.ExecuteQuery(query);

                var dataset = sqlHelper.ExecuteQuery(query);
                listPoDetails = mapper.Map(dataset.Tables[0]).ToList();
                f1ItemDetails = mapperItemList.Map(dataset.Tables[1]).ToList();


                foreach (POF1Details bid in listPoDetails)
                {
                    bid.itemDetails = new List<POF1Items>();
                    bid.itemDetails = f1ItemDetails.Where(p => p.prm360PoNumber == bid.prm360PoNumber).ToList();
                }

                //List<POF1Items> f1ItemDetails = new List<POF1Items>();
                //if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0 && ds.Tables[1].Rows[0][0] != null)
                //{
                //    foreach (DataRow row in ds.Tables[1].Rows)
                //    {
                //        POF1Items f1Items = new POF1Items();
                //        f1Items.prm360PoNumber = row["PO_NUMBER"] != DBNull.Value ? Convert.ToString(row["PO_NUMBER"]) : string.Empty;
                //        f1Items.itemCode = row["ITEM_CODE"] != DBNull.Value ? Convert.ToString(row["ITEM_CODE"]) : string.Empty;
                //        f1Items.itemQty = row["OPEN_QUANTITY"] != DBNull.Value ? Convert.ToDecimal(row["OPEN_QUANTITY"]) : 0;
                //        f1Items.poLineNumber = row["PO_LINE_ITEM"] != DBNull.Value ? Convert.ToInt32(row["PO_LINE_ITEM"]) : 0;
                //        f1Items.unitPrice = row["UNIT_PRICE"] != DBNull.Value ? Convert.ToDecimal(row["UNIT_PRICE"]) : 0;
                //        f1ItemDetails.Add(f1Items);
                //    }
                //}
                //if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                //{
                //    foreach (DataRow row in ds.Tables[0].Rows)
                //    {
                //        POF1Details poDetails = new POF1Details();
                //        poDetails.prm360PoNumber = row["PO_NUMBER"] != DBNull.Value ? Convert.ToString(row["PO_NUMBER"]) : string.Empty;
                //        poDetails.poDate = row["PO_DATE"] != DBNull.Value ? Convert.ToString(row["PO_DATE"]) : string.Empty;
                //        poDetails.serviceCenterCode = row["SVC_CENTER_CODE"] != DBNull.Value ? Convert.ToString(row["SVC_CENTER_CODE"]) : string.Empty;
                //        poDetails.vendorCode = row["VENDOR_CODE"] != DBNull.Value ? Convert.ToString(row["VENDOR_CODE"]) : string.Empty;
                //        poDetails.itemSource = row["ITEM_SOURCE"] != DBNull.Value ? Convert.ToInt32(row["ITEM_SOURCE"]) : 0;
                //        poDetails.shippingWay = row["SHIPPING_WAY"] != DBNull.Value ? Convert.ToInt32(row["SHIPPING_WAY"]) : 0;
                //        poDetails.itemDetails = f1ItemDetails.Where(p => p.prm360PoNumber == poDetails.prm360PoNumber).ToList();
                //        listPoDetails.Add(poDetails);
                //    }
                //}
                string json = JsonConvert.SerializeObject(listPoDetails[0]);

                userToken = GetF1Token();
                if (userToken.Status == "200")
                {
                    var url = ConfigurationManager.AppSettings["F1_CREATE_PO"];

                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                           | SecurityProtocolType.Tls11
                           | SecurityProtocolType.Tls12
                           | SecurityProtocolType.Ssl3;
                    var httpRequest = (HttpWebRequest)WebRequest.Create(url);
                    httpRequest.Method = "POST";

                    httpRequest.Timeout = -1;
                    httpRequest.ContentType = "application/json";

                    httpRequest.Headers.Add("Authorization", "Bearer " + userToken.Token);
                    byte[] data = Encoding.ASCII.GetBytes(json);
                    httpRequest.ContentLength = data.Length;
                    using (var streamWriter = new StreamWriter(httpRequest.GetRequestStream()))
                    {

                        streamWriter.Write(json);
                    }
                    response.Message = json;
                    dynamic myObject = null;
                    var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                    using (StreamReader Reader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        myObject = JsonConvert.DeserializeObject(Reader.ReadToEnd());
                    }
                    response.Status = myObject.status.ToString();
                    if (response.Status == "500")
                    {
                        string[] errorArray = ((System.Collections.IEnumerable)myObject.errors).Cast<object>()
                                 .Select(x => x.ToString())
                                 .ToArray();
                        response.ErrorMessage = string.Join(",", errorArray);
                        return response;
                    }
                    else if (response.Status == "200")
                    {
                        string poNumberString = myObject.poNumber.ToString();
                        response.RemarksType = poNumberString;
                        response.ErrorMessage = "";
                        return response;
                    }
                }
                else if (userToken.Status == "400")
                {
                    response.ErrorMessage = "Unauthorized User";
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
                //response.Message = ex.Message;
            }
            return response;
        }
        public Response updateF1Details(string ponumber)
        {
            List<POF1Details> listpoDetail = new List<POF1Details>();
            List<POF1Items> listF1POItems = new List<POF1Items>();

            UserToken userToken = new UserToken();
            Response response = new Response();
            try
            {
                CORE.DataNamesMapper<POF1Details> mapper = new CORE.DataNamesMapper<POF1Details>();
                CORE.DataNamesMapper<POF1Items> mapperItemList = new CORE.DataNamesMapper<POF1Items>();

                string query = $"select distinct PO_NUMBER,Productcode as ITEM_CODE,ORDER_QTY as OPEN_QUANTITY,PO_LINE_ITEM,F1_PO_NUMBER,UNIT_PRICE from POScheduleDetails psd " +
                    $"inner join cm_product cp on cp.ProductId = psd.PRODUCT_ID " +
                    $"inner join vendors v on v.U_ID = psd.VENDOR_ID WHERE STATUS = 'UPDATE' AND PO_QTY_STATUS = 'APPROVED' AND PO_NUMBER = '{ponumber}'";
                //DataSet ds = sqlHelper.ExecuteQuery(query);

                var dataset = sqlHelper.ExecuteQuery(query);
                //listpoDetail = mapper.Map(dataset.Tables[0]).ToList();
                listF1POItems = mapperItemList.Map(dataset.Tables[0]).ToList();


                //foreach (POF1Details bid in listpoDetail)
                //{
                //    bid.itemDetails = new List<POF1Items>();
                //    bid.itemDetails = listF1POItems.Where(p => p.prm360PoNumber == bid.prm360PoNumber).ToList();
                //}

                //List<POF1Items> listF1POItems = new List<POF1Items>();
                //if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0 && ds.Tables[1].Rows[0][0] != null)
                //{
                //    foreach (DataRow row in ds.Tables[1].Rows)
                //    {
                //        POF1Items poF1Items = new POF1Items();
                //        poF1Items.prm360PoNumber = row["PO_NUMBER"] != DBNull.Value ? Convert.ToString(row["PO_NUMBER"]) : string.Empty;
                //        poF1Items.poLineNumber = row["PO_LINE_ITEM"] != DBNull.Value ? Convert.ToInt32(row["PO_LINE_ITEM"]) : 0;
                //        poF1Items.itemCode = row["ITEM_CODE"] != DBNull.Value ? Convert.ToString(row["ITEM_CODE"]) : string.Empty;
                //        poF1Items.itemQty = row["OPEN_QUANTITY"] != DBNull.Value ? Convert.ToDecimal(row["OPEN_QUANTITY"]) : 0;
                //        poF1Items.f1PoNumber = row["F1_PO_NUMBER"] != DBNull.Value ? Convert.ToString(row["F1_PO_NUMBER"]) : string.Empty;
                //        listF1POItems.Add(poF1Items);
                //    }
                //}
                string json = JsonConvert.SerializeObject(listF1POItems[0]);

             
                userToken = GetF1Token();
                if (userToken.Status == "200")
                {
                    var url = ConfigurationManager.AppSettings["F1_UPDATE_PO"];

                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                           | SecurityProtocolType.Tls11
                           | SecurityProtocolType.Tls12
                           | SecurityProtocolType.Ssl3;
                    var httpRequest = (HttpWebRequest)WebRequest.Create(url);
                    httpRequest.Method = "POST";

                    httpRequest.Timeout = -1;
                    httpRequest.ContentType = "application/json";

                    httpRequest.Headers.Add("Authorization", "Bearer " + userToken.Token);
                    byte[] data = Encoding.ASCII.GetBytes(json);
                    httpRequest.ContentLength = data.Length;
                    using (var streamWriter = new StreamWriter(httpRequest.GetRequestStream()))
                    {

                        streamWriter.Write(json);
                    }
                    response.Message = json;
                    dynamic myObject = null;
                    var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                    using (StreamReader Reader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        myObject = JsonConvert.DeserializeObject(Reader.ReadToEnd());
                    }
                    response.Status = myObject.status.ToString();
                    if (response.Status == "500")
                    {
                        string[] errorArray = ((System.Collections.IEnumerable)myObject.errors).Cast<object>()
                                 .Select(x => x.ToString())
                                 .ToArray();
                        response.ErrorMessage = string.Join(",", errorArray);
                        return response;
                    }
                    else if (response.Status == "200")
                    {
                        response.ErrorMessage = "";
                        return response;
                    }
                }
                else if (userToken.Status == "400")
                {
                    response.ErrorMessage = "Unauthorized User";
                    return response;
                }

            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
                //response.Message = ex.Message;
            }
            return response;

        }


        public Response SaveFileUpload(FileUpload fileupload)
        {
            Response response = new Response();
            try
            {
                if (!string.IsNullOrWhiteSpace(fileupload.FileBase64) && fileupload.FileStream == null)
                {
                    fileupload.FileStream = Convert.FromBase64String(fileupload.FileBase64);
                }

                if (fileupload.FileStream != null)
                {
                    string fileName = string.Empty;
                    long tick = DateTime.Now.Ticks;
                    
                    //fileupload.FileName = fileupload.FileName.Replace("'", "");
                    //var currentFiles = sqlHelper.SelectQuery($"SELECT ATT_ID FROM Attachmentdetails WHERE ATT_PATH LIKE '%_{fileupload.FileName}'");
                    //logger.Debug("Partial Path:" + $"SELECT ATT_ID FROM Attachmentdetails WHERE ATT_PATH LIKE '%_{fileupload.FileName}'");
                    //logger.Debug("Partial Path:" + fileupload.FileName);
                    fileName = System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "INVOICE_GRN_FILE_" + tick + "." + fileupload.FileName);
                    //if (currentFiles != null && currentFiles.Rows.Count > 0)
                    //{
                    //    response.ObjectID = 1;
                    //    return response;
                    //}
                    Utilities.SaveFile(fileName, fileupload.FileStream);
                    fileName = "INVOICE_GRN_FILE_" + tick + "." + fileupload.FileName;
                    response = prm.SaveAttachment(fileName);
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error occured in SaveFileUpload in GRN API SAVE:" + ex.Message);
                return null;
            }
            return response;

        }

        private string ReturnExt(string base64Str) 
        {
            var data = base64Str.Substring(0, 5);
            switch (data.ToUpper())
            {
                case "IVBOR":
                    return "png";
                case "/9J/4":
                    return "jpg";
                case "AAAAF":
                    return "mp4";
                case "JVBER":
                    return "pdf";
                case "AAABA":
                    return "ico";
                case "UMFYI":
                    return "rar";
                case "E1XYD":
                    return "rtf";
                case "U1PKC":
                    return "txt";
                case "MQOWM":
                case "77U/M":
                    return "srt";
                default:
                    return "pdf";
            }
        }

        public Response createF1InvoiceDetails(string invoicenumber)
        {
            List<f1InvoiceDetails> f1InvoceList = new List<f1InvoiceDetails>();
            List<invoiceItems> f1ItemDetailsList = new List<invoiceItems>();

            UserToken userToken = new UserToken();
            Response response = new Response();
            try
            {
                CORE.DataNamesMapper<f1InvoiceDetails> mapper = new CORE.DataNamesMapper<f1InvoiceDetails>();
                CORE.DataNamesMapper<invoiceItems> mapperItemList = new CORE.DataNamesMapper<invoiceItems>();

                string query = $@"select distinct INVOICE_NUMBER AS INVOICE_NO,CONVERT(VARCHAR(10), INVOICE_DATE, 126) AS INVOICE_DATE, 
                    INVOICE_AMOUNT,TAX,pid.CONVERTED_PDFS as ATTACHMENTS, 
                    (SELECT CODE FROM OracleConfigTable WHERE NAME LIKE CONCAT('%',SHIP_TO_LOCATION,'%')) AS SVC_CENTER_CODE, 
                    ITEM_SOURCE,
                    -- concat('https://flipkart.prm360.com/devnew/prm360.html#/createInvoice/',PID.PO_NUMBER,'/',PID.INVOICE_NUMBER,CONVERT(varchar(10), PID.INVOICE_ID)) as ATTACHMENTS,
                    pid.PO_NUMBER
                    from POInvoiceDetails pid 
                    inner join vendors v on v.U_ID = pid.VENDOR_ID
                    inner join poscheduledetails psd on psd.po_number = pid.po_number AND PSD.PO_LINE_ITEM = PID.PO_LINE_ITEM 
                    WHERE INVOICE_STATUS = 'CREATE' AND INVOICE_NUMBER = '{invoicenumber}' 
                    GROUP BY INVOICE_NUMBER,INVOICE_DATE,INVOICE_AMOUNT,TAX,pid.PO_NUMBER,INVOICE_ID,pid.CONVERTED_PDFS,ITEM_SOURCE,SHIP_TO_LOCATION 
                    select distinct pid.PO_NUMBER, CAST(pid.PO_LINE_ITEM AS INT) as PO_LINE_ITEM, PRODUCT_CODE as ITEM_CODE,CAST(INVOICE_QTY AS INT) as INVOICE_QTY,PID.NET_PRICE,F1_PO_NUMBER,PID.LINE_ITEM_AMOUNT 
                    from POInvoiceDetails pid
                    inner join poscheduledetails psd on psd.po_number = pid.po_number AND PSD.PO_LINE_ITEM = PID.PO_LINE_ITEM 
                    WHERE INVOICE_STATUS = 'CREATE' AND INVOICE_NUMBER = '{invoicenumber}' ";
                //DataSet ds = sqlHelper.ExecuteQuery(query);

                var dataset = sqlHelper.ExecuteQuery(query);
                f1InvoceList = mapper.Map(dataset.Tables[0]).ToList();
                f1ItemDetailsList = mapperItemList.Map(dataset.Tables[1]).ToList();


                foreach (f1InvoiceDetails bid in f1InvoceList)
                {
                    bid.itemDetails = new List<invoiceItems>();
                    bid.itemDetails = f1ItemDetailsList.Where(p => p.prm360PoNumber == bid.prm360PoNumber).ToList();

                }

                //List<invoiceItems> f1ItemDetailsList = new List<invoiceItems>();
                //if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0 && ds.Tables[1].Rows[0][0] != null)
                //{
                //    foreach (DataRow row in ds.Tables[1].Rows)
                //    {
                //        invoiceItems f1InvoiceItems = new invoiceItems();
                //        f1InvoiceItems.prm360PoNumber = row["PO_NUMBER"] != DBNull.Value ? Convert.ToString(row["PO_NUMBER"]) : string.Empty;
                //        f1InvoiceItems.poLineNumber = row["PO_LINE_ITEM"] != DBNull.Value ? Convert.ToInt32(row["PO_LINE_ITEM"]) : 0;
                //        f1InvoiceItems.f1PoNumber = row["F1_PO_NUMBER"] != DBNull.Value ? Convert.ToString(row["F1_PO_NUMBER"]) : string.Empty;
                //        f1InvoiceItems.itemCode = row["ITEM_CODE"] != DBNull.Value ? Convert.ToString(row["ITEM_CODE"]) : string.Empty;
                //        f1InvoiceItems.invoicedItemCode = row["ITEM_CODE"] != DBNull.Value ? Convert.ToString(row["ITEM_CODE"]) : string.Empty;
                //        f1InvoiceItems.itemQty = row["INVOICE_QTY"] != DBNull.Value ? Convert.ToInt32(row["INVOICE_QTY"]) : 0;
                //        f1InvoiceItems.unitPrice = row["NET_PRICE"] != DBNull.Value ? Convert.ToInt32(row["NET_PRICE"]) : 0;
                //        f1ItemDetailsList.Add(f1InvoiceItems);
                //    }
                //}
                //if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                //{
                //    foreach (DataRow row in ds.Tables[0].Rows)
                //    {
                //        f1InvoiceDetails invoiceDetails = new f1InvoiceDetails();
                //        invoiceDetails.prm360PoNumber = row["PO_NUMBER"] != DBNull.Value ? Convert.ToString(row["PO_NUMBER"]) : string.Empty;
                //        invoiceDetails.invoiceNo = row["INVOICE_NO"] != DBNull.Value ? Convert.ToString(row["INVOICE_NO"]) : string.Empty;
                //        invoiceDetails.invoiceDate = row["INVOICE_DATE"] != DBNull.Value ? Convert.ToString(row["INVOICE_DATE"]) : string.Empty;
                //        invoiceDetails.invoiceAttachment = row["ATTACHMENTS"] != DBNull.Value ? Convert.ToString(row["ATTACHMENTS"]) : string.Empty;
                //        invoiceDetails.taxRate = row["TAX"] != DBNull.Value ? Convert.ToDecimal(row["TAX"]) : 0;
                //        invoiceDetails.totalAmt = row["INVOICE_AMOUNT"] != DBNull.Value ? Convert.ToDecimal(row["INVOICE_AMOUNT"]) : 0;
                //        invoiceDetails.itemDetails = f1ItemDetailsList.Where(p => p.prm360PoNumber == invoiceDetails.prm360PoNumber).ToList();
                //        f1InvoceList.Add(invoiceDetails);
                //    }
                //}
                string json = JsonConvert.SerializeObject(f1InvoceList[0]);
                logger.Debug("payload for createF1InvoiceDetails>>>> "+" for invoice number>>>"+ invoicenumber+ json);
                userToken = GetF1Token();
                if (userToken.Status == "200")
                {
                    var url = ConfigurationManager.AppSettings["F1_CREATE_INVOICE"];

                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                           | SecurityProtocolType.Tls11
                           | SecurityProtocolType.Tls12
                           | SecurityProtocolType.Ssl3;
                    var httpRequest = (HttpWebRequest)WebRequest.Create(url);
                    httpRequest.Method = "POST";

                    httpRequest.Timeout = -1;
                    httpRequest.ContentType = "application/json";

                    httpRequest.Headers.Add("Authorization", "Bearer " + userToken.Token);
                    byte[] data = Encoding.ASCII.GetBytes(json);
                    httpRequest.ContentLength = data.Length;
                    using (var streamWriter = new StreamWriter(httpRequest.GetRequestStream()))
                    {

                        streamWriter.Write(json);
                    }
                    response.Message = json;
                    dynamic myObject = null;
                    var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                    using (StreamReader Reader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        myObject = JsonConvert.DeserializeObject(Reader.ReadToEnd());
                    }
                    response.Status = myObject.status.ToString();
                    if (response.Status == "500")
                    {
                        string[] errorArray = ((System.Collections.IEnumerable)myObject.errors).Cast<object>()
                                 .Select(x => x.ToString())
                                 .ToArray();
                        response.ErrorMessage = string.Join(",", errorArray);
                        return response;
                    }
                    else if (response.Status == "200")
                    {
                        response.ErrorMessage = "";
                        return response;
                    }
                }
                else if (userToken.Status == "400")
                {
                    response.ErrorMessage = "Unauthorized User";
                    return response;
                }

            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
                response.Message = ex.Message;
            }
            return response;

        }

        public UserToken OracleVendorSave(List<OracleVendor> oraclevendors)
        {
            var request = new System.Web.HttpContextWrapper(System.Web.HttpContext.Current);
            var apiKey = request.Request.Headers.GetValues("jwtToken");

            var validateToken = AuthenticateJwt.GetPrincipal(apiKey[0]);


            UserToken userToken = new UserToken();

            try
            {

                PRMUploadService prmUploadService = new PRMUploadService();
                DataTable dt = new DataTable();
                DataTable tempDT1 = new DataTable();
                tempDT1.Clear();
                List<string> columns = "COMPANY_NAME;VENDOR_CODE;GST_NUMBER;PAN_NUMBER;EMAIL;PHONE_NUMBER;CURRENCY;CONTACT_NAME;BILLING_ADDRESS;BILLING_COUNTRY;BILLING_CITY;BILLING_STATE;BILLING_PINCODE;SHIPPING_ADDRESS;SHIPPING_COUNTRY;SHIPPING_STATE;SHIPPING_CITY;SHIPPING_PINCODE;ENTITY_NAME;VENDOR_TYPE;VENDOR_CATEGORY;VENDOR_SITE_CODE;PAYMENT_METHOD;EMAIL_ALT1;VENDOR_STATUS;VENDOR_SITE_STATUS;VENDOR_INACTIVE_DATE;VENDOR_SITE_INACTIVE_DATE;ORACLE_VENDOR_ID;ORACLE_VENDOR_SITE_ID;REQUEST_ID;RECORD_ID;ROW_ID".Split(';').ToList();
                foreach (var column in columns)
                {
                    tempDT1.Columns.Add(column);
                }

                foreach (var x in oraclevendors)
                {
                    var dr = tempDT1.NewRow(); // have new row on each iteration
                    dr["COMPANY_NAME"] = x.company_name;
                    dr["VENDOR_CODE"] = x.vendor_code;
                    dr["GST_NUMBER"] = x.gst_number;
                    dr["PAN_NUMBER"] = x.pan_number;
                    dr["EMAIL"] = x.email;
                    dr["PHONE_NUMBER"] = x.phone_number;
                    dr["CURRENCY"] = x.currency;
                    dr["CONTACT_NAME"] = x.contact_name;
                    dr["BILLING_ADDRESS"] = x.billing_address;
                    dr["BILLING_COUNTRY"] = x.billing_country;
                    dr["BILLING_CITY"] = x.billing_city;
                    dr["BILLING_STATE"] = x.billing_state;
                    dr["BILLING_PINCODE"] = x.billing_pincode;
                    dr["SHIPPING_ADDRESS"] = x.shipping_address;
                    dr["SHIPPING_COUNTRY"] = x.shipping_country;
                    dr["SHIPPING_STATE"] = x.shipping_state;
                    dr["SHIPPING_CITY"] = x.shipping_city;
                    dr["SHIPPING_PINCODE"] = x.shipping_pincode;
                    dr["ENTITY_NAME"] = x.entity_name;
                    dr["VENDOR_TYPE"] = x.vendor_type;
                    dr["VENDOR_CATEGORY"] = x.vendor_category;
                    dr["VENDOR_SITE_CODE"] = x.vendor_site_code;
                    dr["PAYMENT_METHOD"] = x.payment_method;
                    dr["EMAIL_ALT1"] = x.email_alt1;
                    dr["VENDOR_STATUS"] = x.vendor_status;
                    dr["VENDOR_SITE_STATUS"] = x.vendor_site_status;
                    dr["VENDOR_INACTIVE_DATE"] = x.vendor_inactive_date;
                    dr["VENDOR_SITE_INACTIVE_DATE"] = x.vendor_site_inactive_date;
                    dr["ORACLE_VENDOR_ID"] = x.vendor_id;
                    dr["ORACLE_VENDOR_SITE_ID"] = x.vendor_site_id;
                    dr["REQUEST_ID"] = x.request_id;
                    dr["RECORD_ID"] = x.record_id;

                    tempDT1.Rows.Add(dr);
                }

                string json = JsonConvert.SerializeObject(tempDT1);


                dt = (DataTable)JsonConvert.DeserializeObject(json, (typeof(DataTable)));

                if (validateToken != null && validateToken.Identity.IsAuthenticated)
                {
                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        var prChunks = dt.AsEnumerable().ToList().Chunk(10000);
                        foreach (var chunk in prChunks)
                        {

                            DataTable tempDT = new DataTable();
                            tempDT.Clear();
                            tempDT.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                            foreach (var column in columns)
                            {
                                tempDT.Columns.Add(column);
                            }

                            tempDT.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                            List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                            foreach (DataColumn col in tempDT.Columns)
                            {
                                columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                            }

                            foreach (var row in chunk)
                            {
                                var newRow = tempDT.NewRow();
                                newRow["JOB_ID"] = guid1;
                                newRow["COMPANY_NAME"] = GetDataRowVaue(row, "COMPANY_NAME");
                                newRow["VENDOR_CODE"] = GetDataRowVaue(row, "VENDOR_CODE");
                                newRow["GST_NUMBER"] = GetDataRowVaue(row, "GST_NUMBER");
                                newRow["PAN_NUMBER"] = GetDataRowVaue(row, "PAN_NUMBER");
                                newRow["EMAIL"] = GetDataRowVaue(row, "EMAIL");
                                newRow["PHONE_NUMBER"] = GetDataRowVaue(row, "PHONE_NUMBER");
                                newRow["CURRENCY"] = GetDataRowVaue(row, "CURRENCY");
                                newRow["CONTACT_NAME"] = GetDataRowVaue(row, "CONTACT_NAME");
                                newRow["BILLING_ADDRESS"] = GetDataRowVaue(row, "BILLING_ADDRESS");
                                newRow["BILLING_COUNTRY"] = GetDataRowVaue(row, "BILLING_COUNTRY");
                                newRow["BILLING_STATE"] = GetDataRowVaue(row, "BILLING_STATE");
                                newRow["BILLING_CITY"] = GetDataRowVaue(row, "BILLING_CITY");
                                newRow["BILLING_PINCODE"] = GetDataRowVaue(row, "BILLING_PINCODE");
                                newRow["SHIPPING_ADDRESS"] = GetDataRowVaue(row, "SHIPPING_ADDRESS");
                                newRow["SHIPPING_COUNTRY"] = GetDataRowVaue(row, "SHIPPING_COUNTRY");
                                newRow["SHIPPING_STATE"] = GetDataRowVaue(row, "SHIPPING_STATE");
                                newRow["SHIPPING_CITY"] = GetDataRowVaue(row, "SHIPPING_CITY");
                                newRow["SHIPPING_PINCODE"] = GetDataRowVaue(row, "SHIPPING_PINCODE");
                                newRow["ENTITY_NAME"] = GetDataRowVaue(row, "ENTITY_NAME");
                                newRow["VENDOR_TYPE"] = GetDataRowVaue(row, "VENDOR_TYPE");
                                newRow["VENDOR_CATEGORY"] = GetDataRowVaue(row, "VENDOR_CATEGORY");
                                newRow["VENDOR_SITE_CODE"] = GetDataRowVaue(row, "VENDOR_SITE_CODE");
                                newRow["PAYMENT_METHOD"] = GetDataRowVaue(row, "PAYMENT_METHOD");
                                newRow["EMAIL_ALT1"] = GetDataRowVaue(row, "EMAIL_ALT1");
                                string vendorInactiveDate = GetDataRowVaue(row, "VENDOR_INACTIVE_DATE");
                                newRow["VENDOR_INACTIVE_DATE"] = (vendorInactiveDate == "00000000" || vendorInactiveDate == "0" || vendorInactiveDate == "") ? null : vendorInactiveDate;
                                string vendorSiteInactiveDate = GetDataRowVaue(row, "VENDOR_SITE_INACTIVE_DATE");
                                newRow["VENDOR_SITE_INACTIVE_DATE"] = (vendorSiteInactiveDate == "00000000" || vendorSiteInactiveDate == "0" || vendorSiteInactiveDate == "") ? null : vendorSiteInactiveDate;
                                newRow["ORACLE_VENDOR_ID"] = GetDataRowVaue(row, "ORACLE_VENDOR_ID");
                                newRow["ORACLE_VENDOR_SITE_ID"] = GetDataRowVaue(row, "ORACLE_VENDOR_SITE_ID");
                                newRow["VENDOR_STATUS"] = GetDataRowVaue(row, "VENDOR_STATUS");
                                newRow["VENDOR_SITE_STATUS"] = GetDataRowVaue(row, "VENDOR_SITE_STATUS");
                                newRow["REQUEST_ID"] = GetDataRowVaue(row, "REQUEST_ID");
                                newRow["RECORD_ID"] = GetDataRowVaue(row, "RECORD_ID");
                                newRow["DATE_CREATED"] = DateTime.UtcNow;
                                newRow["ROW_ID"] = tempDT.Rows.Count + 1;


                                tempDT.Rows.Add(newRow);
                            }
                            bizClass.BulkInsert(tempDT, "[dbo].[sap_vendor_details]", columnMappings);
                            tempDT.Rows.Clear();
                        }

                    }

                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_JOB_ID", guid);
                    bizClass.SelectList("erp_process_sap_vendor_details", sd);
                    //ERROR MESSAGES
                    string query = $@"select * from UPLOAD_MODULE_ERROR_DETAILS where job_id = '{guid}'";
                    List<ModuleBasedError> details = new List<ModuleBasedError>();
                    CORE.DataNamesMapper<ModuleBasedError> mapper = new CORE.DataNamesMapper<ModuleBasedError>();
                    var dataTable = sqlHelper.SelectQuery(query);
                    details = mapper.Map(dataTable).ToList();
                    string combindedString = string.Join(";", details.Select(t => t.REASON.Remove(t.REASON.Length - 1)).ToArray());

                    if (details.Count > 0)
                    {
                        userToken.Message = combindedString;
                    }
                    else
                    {
                        userToken.Message = "Saved Successfully";
                    }
                }
                else
                {
                    userToken.Message = "You are Unauthorized to connect PRM.";
                }



            }
            catch (Exception ex)
            {
                userToken.ErrorMessage = ex.Message;
                userToken.Message = ex.Message;
            }

            return userToken;
        }
        //public UserToken OracleHSNCodeSave(List<HSN> hsn)
        //{
        //    var request = new System.Web.HttpContextWrapper(System.Web.HttpContext.Current);
        //    var apiKey = request.Request.Headers.GetValues("jwtToken");

        //    var validateToken = AuthenticateJwt.GetPrincipal(apiKey[0]);


        //    UserToken userToken = new UserToken();

        //    try
        //    {

        //        PRMUploadService prmUploadService = new PRMUploadService();
        //        DataTable dt = new DataTable();
        //        DataTable tempDT1 = new DataTable();
        //        tempDT1.Clear();

        //        List<string> columns = "HSN_CODE;HSN_DESC;HSN_TYPE;HSN_STATUS;RECORD_ID;REQUEST_ID;TYPE".Split(';').ToList();
        //        foreach (var column in columns)
        //        {
        //            tempDT1.Columns.Add(column);
        //        }

        //        foreach (var x in hsn)
        //        {
        //            var dr = tempDT1.NewRow(); // have new row on each iteration
        //            dr["HSN_CODE"] = x.hsn_code;
        //            dr["HSN_DESC"] = x.hsn_desc;
        //            dr["HSN_TYPE"] = x.hsn_type;
        //            dr["HSN_STATUS"] = x.hsn_status;
        //            dr["RECORD_ID"] = x.record_id;
        //            dr["REQUEST_ID"] = x.request_id;
        //            tempDT1.Rows.Add(dr);
        //        }

        //        string json = JsonConvert.SerializeObject(tempDT1);


        //        dt = (DataTable)JsonConvert.DeserializeObject(json, (typeof(DataTable)));

        //        if (validateToken != null && validateToken.Identity.IsAuthenticated)
        //        {
        //            MSSQLBizClass bizClass = new MSSQLBizClass();
        //            var guid1 = Guid.NewGuid();
        //            string guid = guid1.ToString();
        //            if (dt != null && dt.Rows.Count > 0)
        //            {
        //                var prChunks = dt.AsEnumerable().ToList().Chunk(10000);
        //                foreach (var chunk in prChunks)
        //                {

        //                    DataTable tempDT = new DataTable();
        //                    tempDT.Clear();
        //                    tempDT.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
        //                    foreach (var column in columns)
        //                    {
        //                        tempDT.Columns.Add(column);
        //                    }

        //                    tempDT.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
        //                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
        //                    foreach (DataColumn col in tempDT.Columns)
        //                    {
        //                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
        //                    }

        //                    foreach (var row in chunk)
        //                    {
        //                        var newRow = tempDT.NewRow();
        //                        newRow["JOB_ID"] = guid1;
        //                        newRow["HSN_CODE"] = GetDataRowVaue(row, "HSN_CODE");
        //                        newRow["HSN_DESC"] = GetDataRowVaue(row, "HSN_DESC");
        //                        newRow["HSN_TYPE"] = GetDataRowVaue(row, "HSN_TYPE");
        //                        newRow["HSN_STATUS"] = GetDataRowVaue(row, "HSN_STATUS");
        //                        newRow["RECORD_ID"] = GetDataRowVaue(row, "RECORD_ID");
        //                        newRow["REQUEST_ID"] = GetDataRowVaue(row, "REQUEST_ID");
        //                        newRow["TYPE"] = "HSN";

        //                        newRow["DATE_CREATED"] = DateTime.UtcNow;


        //                        tempDT.Rows.Add(newRow);
        //                    }
        //                    bizClass.BulkInsert(tempDT, "[dbo].[OracleConfigurationTable]", columnMappings);
        //                    tempDT.Rows.Clear();
        //                }

        //            }


        //            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
        //            sd.Add("P_JOB_ID", guid);
        //            bizClass.SelectList("erp_process_oracle_configuration_details", sd);
        //            userToken.Message = "HSN Code Saved Successfully.";
        //        }
        //        else
        //        {
        //            userToken.Message = "You are Unauthorized to connect PRM.";
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        userToken.ErrorMessage = ex.Message;
        //        userToken.Message = ex.Message;
        //    }

        //    return userToken;
        //}
        public UserToken OracleTDSSave(List<TDS> tds)
        {
            var request = new System.Web.HttpContextWrapper(System.Web.HttpContext.Current);
            var apiKey = request.Request.Headers.GetValues("jwtToken");

            var validateToken = AuthenticateJwt.GetPrincipal(apiKey[0]);


            UserToken userToken = new UserToken();

            try
            {

                PRMUploadService prmUploadService = new PRMUploadService();
                DataTable dt = new DataTable();
                DataTable tempDT1 = new DataTable();
                tempDT1.Clear();
                List<string> columns = "ID;CODE;STATUS;RECORD_ID;NAME;REQUEST_ID;CONFIG_TYPE;ROW_ID".Split(';').ToList();

                List<string> columns1 = "TDS_ID;TDS_CODE;TDS_STATUS;RECORD_ID;TDS_ENTITY_NAME;REQUEST_ID;TYPE".Split(';').ToList();
                foreach (var column in columns1)
                {
                    tempDT1.Columns.Add(column);
                }

                foreach (var x in tds)
                {
                    var dr = tempDT1.NewRow(); // have new row on each iteration
                    dr["TDS_ID"] = x.tds_id;
                    dr["TDS_CODE"] = x.tds_code;
                    dr["TDS_STATUS"] = x.tds_status;
                    dr["TDS_ENTITY_NAME"] = x.entity_name;
                    dr["RECORD_ID"] = x.record_id;
                    dr["REQUEST_ID"] = x.request_id;
                    tempDT1.Rows.Add(dr);
                }

                string json = JsonConvert.SerializeObject(tempDT1);


                dt = (DataTable)JsonConvert.DeserializeObject(json, (typeof(DataTable)));

                if (validateToken != null && validateToken.Identity.IsAuthenticated)
                {

                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        var prChunks = dt.AsEnumerable().ToList().Chunk(10000);
                        foreach (var chunk in prChunks)
                        {

                            DataTable tempDT = new DataTable();
                            tempDT.Clear();
                            tempDT.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                            foreach (var column in columns)
                            {
                                tempDT.Columns.Add(column);
                            }

                            tempDT.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                            List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                            foreach (DataColumn col in tempDT.Columns)
                            {
                                columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                            }

                            foreach (var row in chunk)
                            {
                                var newRow = tempDT.NewRow();
                                newRow["JOB_ID"] = guid1;
                                newRow["ID"] = GetDataRowVaue(row, "TDS_ID");
                                newRow["CODE"] = GetDataRowVaue(row, "TDS_CODE");
                                newRow["STATUS"] = GetDataRowVaue(row, "TDS_STATUS");
                                newRow["NAME"] = GetDataRowVaue(row, "TDS_ENTITY_NAME");
                                newRow["RECORD_ID"] = GetDataRowVaue(row, "RECORD_ID");
                                newRow["REQUEST_ID"] = GetDataRowVaue(row, "REQUEST_ID");
                                newRow["CONFIG_TYPE"] = "TDS";
                                newRow["DATE_CREATED"] = DateTime.UtcNow;
                                newRow["ROW_ID"] = tempDT.Rows.Count + 1;


                                tempDT.Rows.Add(newRow);
                            }
                            bizClass.BulkInsert(tempDT, "[dbo].[sap_oracle_config_details]", columnMappings);
                            tempDT.Rows.Clear();
                        }

                    }

                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_JOB_ID", guid);
                    bizClass.SelectList("erp_process_sap_config_details", sd);
                    //ERROR MESSAGES
                    string query = $@"select * from UPLOAD_MODULE_ERROR_DETAILS where job_id = '{guid}'";
                    List<ModuleBasedError> details = new List<ModuleBasedError>();
                    CORE.DataNamesMapper<ModuleBasedError> mapper = new CORE.DataNamesMapper<ModuleBasedError>();
                    var dataTable = sqlHelper.SelectQuery(query);
                    details = mapper.Map(dataTable).ToList();
                    string combindedString = string.Join(";", details.Select(t => t.REASON.Remove(t.REASON.Length - 1)).ToArray());

                    if (details.Count > 0)
                    {
                        userToken.Message = combindedString;
                    }
                    else
                    {
                        userToken.Message = "Saved Successfully";
                    }
                }
                else
                {
                    userToken.Message = "You are Unauthorized to connect PRM.";
                }
            }
            catch (Exception ex)
            {
                userToken.ErrorMessage = ex.Message;
                userToken.Message = ex.Message;
            }

            return userToken;
        }
        public UserToken OracleLocationSave(List<Location> location)
        {
            var request = new System.Web.HttpContextWrapper(System.Web.HttpContext.Current);
            var apiKey = request.Request.Headers.GetValues("jwtToken");

            var validateToken = AuthenticateJwt.GetPrincipal(apiKey[0]);


            UserToken userToken = new UserToken();

            try
            {

                PRMUploadService prmUploadService = new PRMUploadService();
                DataTable dt = new DataTable();
                DataTable tempDT1 = new DataTable();
                tempDT1.Clear();
                List<string> columns = "NAME;DESCRIPTION;CODE;STATUS;DATE;ID;RECORD_ID;REQUEST_ID;CONFIG_TYPE;ROW_ID".Split(';').ToList();

                List<string> columns1 = "LOCATION_NAME;LOCATION_SITE;LOCATION_CODE;LOCATION_STATUS;LOCATION_INACTIVE_DATE;LOCATION_ID;RECORD_ID;REQUEST_ID;TYPE".Split(';').ToList();
                foreach (var column in columns1)
                {
                    tempDT1.Columns.Add(column);
                }

                foreach (var x in location)
                {
                    var dr = tempDT1.NewRow(); // have new row on each iteration
                    dr["LOCATION_NAME"] = x.location_name;
                    dr["LOCATION_SITE"] = x.location_site;
                    dr["LOCATION_CODE"] = x.location_code;
                    dr["LOCATION_STATUS"] = x.location_status;
                    dr["LOCATION_INACTIVE_DATE"] = x.inactive_date;
                    dr["LOCATION_ID"] = x.location_id;
                    dr["RECORD_ID"] = x.record_id;
                    dr["REQUEST_ID"] = x.request_id;
                    tempDT1.Rows.Add(dr);
                }

                string json = JsonConvert.SerializeObject(tempDT1);


                dt = (DataTable)JsonConvert.DeserializeObject(json, (typeof(DataTable)));

                if (validateToken != null && validateToken.Identity.IsAuthenticated)
                {
                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        var prChunks = dt.AsEnumerable().ToList().Chunk(10000);
                        foreach (var chunk in prChunks)
                        {

                            DataTable tempDT = new DataTable();
                            tempDT.Clear();
                            tempDT.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                            foreach (var column in columns)
                            {
                                tempDT.Columns.Add(column);
                            }

                            tempDT.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                            List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                            foreach (DataColumn col in tempDT.Columns)
                            {
                                columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                            }


                            string query = @"INSERT INTO [dbo].[OracleConfigTable] ([JOB_ID], [NAME], [DESCRIPTION], [CODE], [STATUS], [DATE],[ID], [RECORD_ID],[REQUEST_ID], 
                                    [CONFIG_TYPE],[DATE_CREATED]) VALUES ";

                            foreach (var row in chunk)
                            {
                                var newRow = tempDT.NewRow();
                                newRow["JOB_ID"] = guid1;
                                newRow["NAME"] = GetDataRowVaue(row, "LOCATION_NAME");
                                newRow["DESCRIPTION"] = GetDataRowVaue(row, "LOCATION_SITE");
                                newRow["CODE"] = GetDataRowVaue(row, "LOCATION_CODE");
                                newRow["STATUS"] = GetDataRowVaue(row, "LOCATION_STATUS");
                                string locationInactiveDate = GetDataRowVaue(row, "LOCATION_INACTIVE_DATE");
                                newRow["DATE"] = (locationInactiveDate == "00000000" || locationInactiveDate == "0" || locationInactiveDate == "") ? null : locationInactiveDate;
                                newRow["ID"] = GetDataRowVaue(row, "LOCATION_ID");
                                newRow["RECORD_ID"] = GetDataRowVaue(row, "RECORD_ID");
                                newRow["REQUEST_ID"] = GetDataRowVaue(row, "REQUEST_ID");
                                newRow["CONFIG_TYPE"] = "LOCATION";
                                newRow["DATE_CREATED"] = DateTime.UtcNow;
                                newRow["ROW_ID"] = tempDT.Rows.Count + 1;
                                tempDT.Rows.Add(newRow);


                                query += $@"('{guid}',";
                                query += $@"'{GetDataRowVaue(row, "LOCATION_NAME")}',";
                                query += $@"'{GetDataRowVaue(row, "LOCATION_SITE")}',";
                                query += $@"'{GetDataRowVaue(row, "LOCATION_CODE")}',";
                                query += $@"'{GetDataRowVaue(row, "LOCATION_STATUS")}',";
                                query += $@"'{GetDataRowVaue(row, "LOCATION_INACTIVE_DATE")}',";
                                query += $@"'{GetDataRowVaue(row, "LOCATION_ID")}',";
                                query += $@"'{GetDataRowVaue(row, "REQUEST_ID")}',";
                                query += $@"'LOCATION',";
                                query += $@"GETUTCDATE()),";
                            }
                            query = query.TrimEnd(',');
                            //bizClass.ExecuteNonQuery_IUD(query);
                            logger.Debug("END GetVENDORDetails QUERY: " + query);
                            bizClass.BulkInsert(tempDT, "[dbo].[sap_oracle_config_details]", columnMappings);
                            tempDT.Rows.Clear();
                        }

                    }

                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_JOB_ID", guid);
                    bizClass.SelectList("erp_process_sap_config_details", sd);
                    //ERROR MESSAGES
                    string query1 = $@"select * from UPLOAD_MODULE_ERROR_DETAILS where job_id = '{guid}'";
                    List<ModuleBasedError> details = new List<ModuleBasedError>();
                    CORE.DataNamesMapper<ModuleBasedError> mapper = new CORE.DataNamesMapper<ModuleBasedError>();
                    var dataTable = sqlHelper.SelectQuery(query1);
                    details = mapper.Map(dataTable).ToList();
                    string combindedString = string.Join(";", details.Select(t => t.REASON.Remove(t.REASON.Length - 1)).ToArray());

                    if (details.Count > 0)
                    {
                        userToken.Message = combindedString;
                    }
                    else
                    {
                        userToken.Message = "Saved Successfully";
                    }
                }
                else
                {
                    userToken.Message = "You are Unauthorized to connect PRM.";
                }
            }
            catch (Exception ex)
            {
                userToken.ErrorMessage = ex.Message;
                userToken.Message = ex.Message;
            }

            return userToken;
        }
        public UserToken OraclePaygroupSave(List<PayGroup> paygroup)
        {
            var request = new System.Web.HttpContextWrapper(System.Web.HttpContext.Current);
            var apiKey = request.Request.Headers.GetValues("jwtToken");

            var validateToken = AuthenticateJwt.GetPrincipal(apiKey[0]);


            UserToken userToken = new UserToken();

            try
            {

                PRMUploadService prmUploadService = new PRMUploadService();
                DataTable dt = new DataTable();
                DataTable tempDT1 = new DataTable();
                tempDT1.Clear();
                List<string> columns = "CODE;DESCRIPTION;RECORD_ID;REQUEST_ID;CONFIG_TYPE;ROW_ID".Split(';').ToList();

                List<string> columns1 = "PAYGROUP_CODE;PAYGROUP_DESC;RECORD_ID;REQUEST_ID;TYPE".Split(';').ToList();
                foreach (var column in columns1)
                {
                    tempDT1.Columns.Add(column);
                }

                foreach (var x in paygroup)
                {
                    var dr = tempDT1.NewRow(); // have new row on each iteration
                    dr["PAYGROUP_CODE"] = x.paygroup_code;
                    dr["PAYGROUP_DESC"] = x.paygroup_desc;
                    dr["RECORD_ID"] = x.record_id;
                    dr["REQUEST_ID"] = x.request_id;
                    tempDT1.Rows.Add(dr);
                }

                string json = JsonConvert.SerializeObject(tempDT1);


                dt = (DataTable)JsonConvert.DeserializeObject(json, (typeof(DataTable)));

                if (validateToken != null && validateToken.Identity.IsAuthenticated)
                {
                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        var prChunks = dt.AsEnumerable().ToList().Chunk(10000);
                        foreach (var chunk in prChunks)
                        {

                            DataTable tempDT = new DataTable();
                            tempDT.Clear();
                            tempDT.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                            foreach (var column in columns)
                            {
                                tempDT.Columns.Add(column);
                            }

                            tempDT.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                            List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                            foreach (DataColumn col in tempDT.Columns)
                            {
                                columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                            }

                            foreach (var row in chunk)
                            {
                                var newRow = tempDT.NewRow();
                                newRow["JOB_ID"] = guid1;
                                newRow["CODE"] = GetDataRowVaue(row, "PAYGROUP_CODE");
                                newRow["DESCRIPTION"] = GetDataRowVaue(row, "PAYGROUP_DESC");
                                newRow["RECORD_ID"] = GetDataRowVaue(row, "RECORD_ID");
                                newRow["REQUEST_ID"] = GetDataRowVaue(row, "REQUEST_ID");
                                newRow["CONFIG_TYPE"] = "PAYGROUP";
                                newRow["ROW_ID"] = tempDT.Rows.Count + 1;

                                newRow["DATE_CREATED"] = DateTime.UtcNow;


                                tempDT.Rows.Add(newRow);
                            }
                            bizClass.BulkInsert(tempDT, "[dbo].[sap_oracle_config_details]", columnMappings);
                            tempDT.Rows.Clear();
                        }

                    }

                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_JOB_ID", guid);
                    bizClass.SelectList("erp_process_sap_config_details", sd);
                    //ERROR MESSAGES
                    string query = $@"select * from UPLOAD_MODULE_ERROR_DETAILS where job_id = '{guid}'";
                    List<ModuleBasedError> details = new List<ModuleBasedError>();
                    CORE.DataNamesMapper<ModuleBasedError> mapper = new CORE.DataNamesMapper<ModuleBasedError>();
                    var dataTable = sqlHelper.SelectQuery(query);
                    details = mapper.Map(dataTable).ToList();
                    string combindedString = string.Join(";", details.Select(t => t.REASON.Remove(t.REASON.Length - 1)).ToArray());

                    if (details.Count > 0)
                    {
                        userToken.Message = combindedString;
                    }
                    else
                    {
                        userToken.Message = "Saved Successfully";
                    }
                }
                else
                {
                    userToken.Message = "You are Unauthorized to connect PRM.";
                }

            }
            catch (Exception ex)
            {
                userToken.ErrorMessage = ex.Message;
                userToken.Message = ex.Message;
            }

            return userToken;
        }

        public UserToken OracleForexSave(List<Forex> forex)
        {
            var request = new System.Web.HttpContextWrapper(System.Web.HttpContext.Current);
            var apiKey = request.Request.Headers.GetValues("jwtToken");

            var validateToken = AuthenticateJwt.GetPrincipal(apiKey[0]);


            UserToken userToken = new UserToken();

            try
            {

                PRMUploadService prmUploadService = new PRMUploadService();
                DataTable dt = new DataTable();
                DataTable tempDT1 = new DataTable();
                tempDT1.Clear();

                List<string> columns = "FCURR;UKURS;FROM_DATE;TO_DATE;RECORD_ID;REQUEST_ID".Split(';').ToList();
                foreach (var column in columns)
                {
                    tempDT1.Columns.Add(column);
                }

                foreach (var x in forex)
                {
                    var dr = tempDT1.NewRow(); // have new row on each iteration
                    dr["FCURR"] = x.currency;
                    dr["UKURS"] = x.rate;
                    dr["FROM_DATE"] = x.from_date;
                    dr["TO_DATE"] = x.to_date;
                    dr["RECORD_ID"] = x.record_id;
                    dr["REQUEST_ID"] = x.request_id;
                    tempDT1.Rows.Add(dr);
                }

                string json = JsonConvert.SerializeObject(tempDT1);


                dt = (DataTable)JsonConvert.DeserializeObject(json, (typeof(DataTable)));

                if (validateToken != null && validateToken.Identity.IsAuthenticated)
                {
                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        var prChunks = dt.AsEnumerable().ToList().Chunk(10000);
                        foreach (var chunk in prChunks)
                        {

                            DataTable tempDT = new DataTable();
                            tempDT.Clear();
                            tempDT.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                            foreach (var column in columns)
                            {
                                tempDT.Columns.Add(column);
                            }

                            tempDT.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                            List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                            foreach (DataColumn col in tempDT.Columns)
                            {
                                columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                            }



                            foreach (var row in chunk)
                            {
                                var newRow = tempDT.NewRow();
                                newRow["JOB_ID"] = guid1;
                                newRow["FCURR"] = GetDataRowVaue(row, "FCURR");
                                newRow["UKURS"] = GetDataRowVaue(row, "UKURS");
                                newRow["FROM_DATE"] = GetDataRowVaue(row, "FROM_DATE");
                                newRow["TO_DATE"] = GetDataRowVaue(row, "TO_DATE");
                                newRow["RECORD_ID"] = GetDataRowVaue(row, "RECORD_ID");
                                newRow["REQUEST_ID"] = GetDataRowVaue(row, "REQUEST_ID");

                                newRow["DATE_CREATED"] = DateTime.UtcNow;
                                tempDT.Rows.Add(newRow);
                            }

                            bizClass.BulkInsert(tempDT, "[dbo].[SAP_CURRENCY_DETAILS]", columnMappings);
                            tempDT.Rows.Clear();
                        }

                    }

                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_JOB_ID", guid);
                    bizClass.SelectList("erp_process_sap_currency_details", sd);
                    userToken.Message = "Forex Saved Successfully.";
                }
                else
                {
                    userToken.Message = "You are Unauthorized to connect PRM.";
                }
            }
            catch (Exception ex)
            {
                userToken.ErrorMessage = ex.Message;
                userToken.Message = ex.Message;
            }

            return userToken;
        }


        public Response GetInvoiceOracleDetails(string type = null, int compId = 0)
        {
           
            UserToken userToken = new UserToken();
            Response response = new Response();


            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<OracleInvoiceDetails> listoracleinvoiceDetail = new List<OracleInvoiceDetails>();
            string json = string.Empty;
            try
            {
                CORE.DataNamesMapper<OracleInvoiceDetails> mapper = new CORE.DataNamesMapper<OracleInvoiceDetails>();

                DataSet ds;
                var routeURL = ConfigurationManager.AppSettings["SITE_LINK"].ToString() + "#/createInvoice/";
                string invoiceNumber = string.Empty;
                string query1 = $@"select INVOICE_TYPE,INVOICE_NUMBER from POInvoiceDetails where INVOICE_NUMBER = '{type}';";
                DataSet ds1 = sqlHelper.ExecuteQuery(query1);
                if (ds1 != null && ds1.Tables[0].Rows.Count > 0) 
                {
                    DataRow row = ds1.Tables[0].Rows[0];
                    type = row["INVOICE_TYPE"] != DBNull.Value ? Convert.ToString(row["INVOICE_TYPE"]) : string.Empty;
                    invoiceNumber = row["INVOICE_NUMBER"] != DBNull.Value ? Convert.ToString(row["INVOICE_NUMBER"]) : string.Empty;
                }


                if (type == "Pre Payment")
                {
                    string query = $@"select distinct PID.INVOICE_NUMBER AS PRM_GENERATED_INVOICE_ID,pid.INVOICE_NUMBER as INVOICE_NO,pid.PO_NUMBER,INVOICE_AMOUNT,V.VENDOR_CODE,V.COMP_NAME as VENDOR_NAME,psd.VENDOR_SITE_CODE,
                        'Prepayment' AS INVOICE_TYPE_LOOKUP,convert(varchar, INVOICE_DATE, 105) as INVOICE_DATE,TRANSACTION_TYPE,
                        'INPUTS' as INTENT_OF_PURCHASE,
                        (SELECT code FROM OracleConfigTable WHERE CONFIG_TYPE='LOCATION' and NAME=BILL_TO_LOCATION) as BILL_TO_LOCATION,
                        (SELECT code FROM OracleConfigTable WHERE CONFIG_TYPE='LOCATION' and NAME=SHIP_TO_LOCATION) as SHIP_TO_LOCATION
                        ,REGISTERED_UNDER_GST,IS_RCM_APPLICABLE,PSD.CURRENCY,EXCHANGE_RATE,PID.PAYMENT_TERMS,
                        'EFT' as PAYMENT_METHOD,(case when PID.ENTITY IN ('JCS-Jeeves Consumer Services Private Limited', 'Jeeves') then 'JCS-Jeeves Consumer Services Pvt Ltd' else 'F1 Info Solutions' end) AS ENTITY,
                        PAYGROUP_DESCRIPTION as PAY_GROUP_DESCRIPTION,IRN_NO,
                        concat('{routeURL}',PID.PO_NUMBER,'/',PID.INVOICE_NUMBER,'/',CONVERT(varchar(10), PID.INVOICE_ID)) as ATTACHMENTS,
                        'ITEM' as LINE_TYPE,
                        (case when PID.ENTITY IN ('JCS-Jeeves Consumer Services Private Limited','Jeeves') then 171 else 172 end) AS SEGMENT1,
                        (case when PID.ENTITY IN ('JCS-Jeeves Consumer Services Private Limited','Jeeves') and TAX_CATEGORY_TYPE = 'Recoverable' then '501010'
                        when PID.ENTITY IN ('JCS-Jeeves Consumer Services Private Limited','Jeeves') and TAX_CATEGORY_TYPE = 'Non Recoverable' then '503935'
                        else '501146' end) AS SEGMENT2,
                        '5205' AS SEGMENT3,
                        (SELECT code FROM OracleConfigTable WHERE CONFIG_TYPE='LOCATION' and NAME=SHIP_TO_LOCATION) as SEGMENT4,
                        '0000000000' AS SEGMENT5,'000' AS SEGMENT6,'0000' AS SEGMENT7,'00000' AS SEGMENT8,
                        (case when PID.ENTITY IN ('JCS-Jeeves Consumer Services Private Limited','Jeeves') then 'JCS_TDS_194(Q)_0.1%' else 'F1_TDS_194(Q)_0.1%' end) as TDS_CODE,psd.HSN_CODE,NATURE_OF_TRANSACTION,
                        CONVERT(int, psd.PO_LINE_ITEM)/10 as LINE_ITEM_NUMBER,pid.PRODUCT_NAME AS LINE_ITEM_NAME
                        ,(pid.INVOICE_QTY*psd.UNIT_PRICE_NO_TAX) AS LINE_ITEM_AMOUNT,0 AS TAX ,'' AS TAX_CODE,CONCAT(PID.PO_NUMBER,'-',GRN_NUMBER) AS REMARKS,INDENT_OF_PURCHASE
                        from POInvoiceDetails pid 
                        inner join vendors v on v.U_ID = pid.VENDOR_ID
                        left join companygstinfo c on c.VENDOR_ID = v.u_id
                        inner join poscheduledetails psd on psd.po_number = pid.po_number AND PSD.PO_LINE_ITEM = PID.PO_LINE_ITEM 
                        left join ThreeWayMatching tw on tw.PO_NUMBER = pid.PO_NUMBER and tw.PO_LINE_ITEM= pid.PO_LINE_ITEM and IS_ERROR=0
                        WHERE pid.C_COMP_ID = {compId} 
                        and pid.INVOICE_NUMBER = '{invoiceNumber}'
                        and pid.INVOICE_TYPE='Pre Payment' and IS_ORACLE_PROCESSED is null
                        GROUP BY INVOICE_ID,pid.INVOICE_NUMBER,INVOICE_TYPE,INVOICE_DATE,INVOICE_AMOUNT,LINE_ITEM_AMOUNT,
                        pid.PO_NUMBER,psd.BILL_TO_LOCATION,psd.SHIP_TO_LOCATION,psd.VENDOR_SITE_CODE,V.VENDOR_CODE,v.COMP_NAME,INVOICE_TYPE,TRANSACTION_TYPE,
                        INDENT_OF_PURCHASE,BILL_TO_LOCATION,SHIP_TO_LOCATION,REGISTERED_UNDER_GST,IS_RCM_APPLICABLE,PSD.CURRENCY,EXCHANGE_RATE,PID.PAYMENT_TERMS,
                        PID.ENTITY,PAYGROUP_DESCRIPTION,IRN_NO,PID.ATTACHMENTS,psd.HSN_CODE,NATURE_OF_TRANSACTION,
                        psd.PO_LINE_ITEM,pid.PRODUCT_NAME,COMMENTS,pid.INVOICE_QTY,PID.NET_PRICE,CGST,SGST,IGST,INDENT_OF_PURCHASE,GRN_NUMBER,TAX_CATEGORY_TYPE;";

                    ds = sqlHelper.ExecuteQuery(query);

                }
                else
                {
                    type = invoiceNumber;
                    sd.Add("P_INVOICE_NUMBER", type);
                    sd.Add("P_COMP_ID", compId);
                    ds = sqlHelper.SelectList("po_getOracleInvoice", sd);

                }
                listoracleinvoiceDetail = mapper.Map(ds.Tables[0]).ToList();

                //List<OracleJeevesItems> listDispatcInvoiceItems = new List<OracleJeevesItems>();
                
                //if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                //{
                //    foreach (DataRow row in ds.Tables[0].Rows)
                //    {
                //        OracleInvoiceDetails oracleinvoice = new OracleInvoiceDetails();
                //        oracleinvoice.po_number = row["PO_NUMBER"] != DBNull.Value ? Convert.ToString(row["PO_NUMBER"]) : string.Empty;
                //        oracleinvoice.invoice_number = row["INVOICE_NO"] != DBNull.Value ? Convert.ToString(row["INVOICE_NO"]) : string.Empty;
                //        oracleinvoice.invoice_amount = row["INVOICE_AMOUNT"] != DBNull.Value ? Convert.ToDecimal(row["INVOICE_AMOUNT"]) : 0;
                //        oracleinvoice.vendor_name = row["VENDOR_NAME"] != DBNull.Value ? Convert.ToString(row["VENDOR_NAME"]) : string.Empty;
                //        oracleinvoice.vendor_number = row["VENDOR_CODE"] != DBNull.Value ? Convert.ToString(row["VENDOR_CODE"]) : string.Empty;
                //        oracleinvoice.vendor_site_code = row["VENDOR_SITE_CODE"] != DBNull.Value ? Convert.ToString(row["VENDOR_SITE_CODE"]) : string.Empty;
                //        oracleinvoice.invoice_type = row["INVOICE_TYPE_LOOKUP"] != DBNull.Value ? Convert.ToString(row["INVOICE_TYPE_LOOKUP"]) : string.Empty;
                //        oracleinvoice.invoice_date = row["INVOICE_DATE"] != DBNull.Value ? Convert.ToString(row["INVOICE_DATE"]) : string.Empty;
                //        oracleinvoice.transaction_type = row["TRANSACTION_TYPE"] != DBNull.Value ? Convert.ToString(row["TRANSACTION_TYPE"]) : string.Empty;
                //        oracleinvoice.nature_of_transaction = row["NATURE_OF_TRANSACTION"] != DBNull.Value ? Convert.ToString(row["NATURE_OF_TRANSACTION"]) : string.Empty;
                //        oracleinvoice.intent_of_purchase = row["INDENT_OF_PURCHASE"] != DBNull.Value ? Convert.ToString(row["INDENT_OF_PURCHASE"]) : string.Empty;
                //        oracleinvoice.bill_to_location = row["BILL_TO_LOCATION"] != DBNull.Value ? Convert.ToString(row["BILL_TO_LOCATION"]) : string.Empty;
                //        oracleinvoice.ship_to_location = row["SHIP_TO_LOCATION"] != DBNull.Value ? Convert.ToString(row["SHIP_TO_LOCATION"]) : string.Empty;
                //        oracleinvoice.registered_under_gst = row["REGISTERED_UNDER_GST"] != DBNull.Value ? Convert.ToString(row["REGISTERED_UNDER_GST"]) : string.Empty;
                //        oracleinvoice.is_rcm_applicable = row["IS_RCM_APPLICABLE"] != DBNull.Value ? Convert.ToString(row["IS_RCM_APPLICABLE"]) : string.Empty;
                //        oracleinvoice.invoice_currency_code = row["CURRENCY"] != DBNull.Value ? Convert.ToString(row["CURRENCY"]) : string.Empty;
                //        oracleinvoice.exchange_rate = row["EXCHANGE_RATE"] != DBNull.Value ? Convert.ToDecimal(row["EXCHANGE_RATE"]) : 0;
                //        oracleinvoice.terms_name = row["PAYMENT_TERMS"] != DBNull.Value ? Convert.ToString(row["PAYMENT_TERMS"]) : string.Empty;
                //        oracleinvoice.payment_method = row["PAYMENT_METHOD"] != DBNull.Value ? Convert.ToString(row["PAYMENT_METHOD"]) : string.Empty;
                //        oracleinvoice.entity = row["ENTITY"] != DBNull.Value ? Convert.ToString(row["ENTITY"]) : string.Empty;
                //        oracleinvoice.pay_group = row["PAYGROUP_DESCRIPTION"] != DBNull.Value ? Convert.ToString(row["PAYGROUP_DESCRIPTION"]) : string.Empty;
                //        oracleinvoice.hsn_sac_code = row["HSN_CODE"] != DBNull.Value ? Convert.ToInt32(row["HSN_CODE"]) : 0;
                //        oracleinvoice.erp_id = row["PRM_GENERATED_INVOICE_ID"] != DBNull.Value ? Convert.ToString(row["PRM_GENERATED_INVOICE_ID"]) : string.Empty;
                //        oracleinvoice.irn_no = row["IRN_NO"] != DBNull.Value ? Convert.ToString(row["IRN_NO"]) : string.Empty;
                //        oracleinvoice.attachement_url = row["ATTACHMENTS"] != DBNull.Value ? Convert.ToString(row["ATTACHMENTS"]) : string.Empty;
                //        oracleinvoice.invoice_received_date = row["INVOICE_DATE"] != DBNull.Value ? Convert.ToString(row["INVOICE_DATE"]) : string.Empty;
                //        oracleinvoice.tds_code = row["TDS_CODE"] != DBNull.Value ? Convert.ToString(row["TDS_CODE"]) : string.Empty;
                //        oracleinvoice.line_number = row["LINE_ITEM_NUMBER"] != DBNull.Value ? Convert.ToInt32(row["LINE_ITEM_NUMBER"]) : 0;
                //        oracleinvoice.line_description = row["LINE_ITEM_NAME"] != DBNull.Value ? Convert.ToString(row["LINE_ITEM_NAME"]) : string.Empty;
                //        oracleinvoice.line_amount = row["LINE_ITEM_AMOUNT"] != DBNull.Value ? Convert.ToDecimal(row["LINE_ITEM_AMOUNT"]) : 0;
                //        oracleinvoice.tax_rate_percentage = row["TAX"] != DBNull.Value ? Convert.ToDecimal(row["TAX"]) : 0;
                //        oracleinvoice.tax_rate_code = row["TAX_CODE"] != DBNull.Value ? Convert.ToString(row["TAX_CODE"]) : string.Empty;
                //        oracleinvoice.inv_line_type = "ITEM";
                //        oracleinvoice.segment1 = row["SEGMENT1"] != DBNull.Value ? Convert.ToString(row["SEGMENT1"]) : string.Empty;
                //        oracleinvoice.segment2 = row["SEGMENT2"] != DBNull.Value ? Convert.ToString(row["SEGMENT2"]) : string.Empty;
                //        oracleinvoice.segment3 = row["SEGMENT3"] != DBNull.Value ? Convert.ToString(row["SEGMENT3"]) : string.Empty;
                //        oracleinvoice.segment4 = row["SEGMENT4"] != DBNull.Value ? Convert.ToString(row["SEGMENT4"]) : string.Empty;
                //        oracleinvoice.segment5 = row["SEGMENT5"] != DBNull.Value ? Convert.ToString(row["SEGMENT5"]) : string.Empty;
                //        oracleinvoice.segment6 = row["SEGMENT6"] != DBNull.Value ? Convert.ToString(row["SEGMENT6"]) : string.Empty;
                //        oracleinvoice.segment7 = row["SEGMENT7"] != DBNull.Value ? Convert.ToString(row["SEGMENT7"]) : string.Empty;
                //        oracleinvoice.segment8 = row["SEGMENT8"] != DBNull.Value ? Convert.ToString(row["SEGMENT8"]) : string.Empty;
                //        oracleinvoice.description = row["REMARKS"] != DBNull.Value ? Convert.ToString(row["REMARKS"]) : string.Empty;
                //        //oracleinvoice.invoice_line_item = listDispatcInvoiceItems.Where(p => p.invoice_no == oracleinvoice.invoice_no).ToList();
                //        listoracleinvoiceDetail.Add(oracleinvoice);
                //    }


                //}

                const string quote = "\"";
                const string quote1 = ":";

                if (listoracleinvoiceDetail != null && listoracleinvoiceDetail.Count() > 0) 
                {
                    json = "{" + quote + "invoice" + quote + quote1 + JsonConvert.SerializeObject(listoracleinvoiceDetail) + "}";
                } else {
                    json = string.Empty;
                }


                if (!string.IsNullOrEmpty(json))
                {
                    if (json.Contains("REPLACE_URL")) 
                    {
                        json = json.Replace("REPLACE_URL", routeURL);
                    }
                }

                logger.Info("Oracle Invoice Json is>>>>"+ json);

                if (string.IsNullOrEmpty(json)) 
                {
                    response.Message = "No Data";
                    string query2 = $@"INSERT INTO [dbo].[ERROR_LOGS] ([API], [SYSTEM], [ERROR_DESCRIPTION], [ID], [DATE_CREATED],[DATA])      
                                VALUES ('ORACLE_INVOICE','ORACLE', '{response.Message}', 'ORALCE-{DateTime.Now.Ticks.ToString()}', GETUTCDATE(),'{response.Message}')";
                    sqlHelper.ExecuteQuery(query2);
                    return response;
                }

                List<OracleInvoiceDetails> test = new List<OracleInvoiceDetails>();

                OracleInvoiceDetails invoiceOracleDetail = JsonConvert.DeserializeObject<OracleInvoiceDetails>(json);
                test.Add(invoiceOracleDetail);

                var url = ConfigurationManager.AppSettings["ORACLE_INVOICE"];
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                       | SecurityProtocolType.Tls11
                       | SecurityProtocolType.Tls12
                       | SecurityProtocolType.Ssl3;
                var httpRequest = (HttpWebRequest)WebRequest.Create(url);
                httpRequest.Method = "POST";

                httpRequest.Timeout = -1;
                httpRequest.ContentType = "application/json";

                byte[] data = Encoding.ASCII.GetBytes(json);
                httpRequest.ContentLength = data.Length;
                using (var streamWriter = new StreamWriter(httpRequest.GetRequestStream()))
                {
                    streamWriter.Write(json);
                }

                var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                var result = "";
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
                var status = (int)httpResponse.StatusCode;
                Console.WriteLine(httpResponse.StatusCode);
                if (status == 200)
                {
                    logger.Debug("oracle invoice result>>>>" + result);
                    if (!string.IsNullOrEmpty(result)) 
                    {
                        string value = result;
                        value = value.Replace("}", "},");
                        value = value.Substring(0, value.Length - 1);
                        string[] values = value.Split(',');
                        logger.Debug("length>>>"+ values.Length);
                        values = values.Where(a => !string.IsNullOrEmpty(a)).ToArray();
                        foreach (var str in values)
                        {
                            if (str.Split(':')[1].ToString().ToUpper() == "E") 
                            {
                                string query2 = $@"INSERT INTO [dbo].[ERROR_LOGS] ([API], [SYSTEM], [ERROR_DESCRIPTION], [ID], [DATE_CREATED],[DATA])      
                                    VALUES ('ORACLE_INVOICE','ORACLE', '{str}', 'ORALCE-{DateTime.Now.Ticks.ToString()}', GETUTCDATE(),'{json}')";
                                sqlHelper.ExecuteQuery(query2);
                            }
                        }
                    }
                    response.Message = json;
                    response.ErrorMessage = url;
                    return response;
                }
                else
                {
                    response.Message = "Failed";
                    string query2 = $@"INSERT INTO [dbo].[ERROR_LOGS] ([API], [SYSTEM], [ERROR_DESCRIPTION], [ID], [DATE_CREATED],[DATA])      
                                VALUES ('ORACLE_INVOICE','ORACLE', '{response.Message}', 'ORALCE-{DateTime.Now.Ticks.ToString()}', GETUTCDATE(),'{json}')";
                    sqlHelper.ExecuteQuery(query2);
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
                response.Message = ex.Message;
                string query2 = $@"INSERT INTO [dbo].[ERROR_LOGS] ([API], [SYSTEM], [ERROR_DESCRIPTION], [ID], [DATE_CREATED],[DATA])      
                                VALUES ('ORACLE_INVOICE','ORACLE', '{response.Message}', 'ORALCE-{DateTime.Now.Ticks.ToString()}', GETUTCDATE(),'{json}')";
                sqlHelper.ExecuteQuery(query2);                
            }

            return response;
        }


        public Response GetHSNOracleDetails(int compID)
        {
            var now = DateTime.Now;
            logger.Info("GethsnOracleDetails Job Ran Successfully on >>>" + now);
            UserToken userToken = new UserToken();
            Response response = new Response();


            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<OracleHSNDetails> listoraclehsnDetail = new List<OracleHSNDetails>();
            string json = string.Empty;
            try
            {
                CORE.DataNamesMapper<OracleHSNDetails> mapper = new CORE.DataNamesMapper<OracleHSNDetails>();

                string query = $@"select distinct HSN_CODE as hsn_sac_code,CategoryName as description,HSN_CODE as meaning from quotations q
                                    inner join cm_product cp on cp.ProductName=q.PROD_ID
                                    left join cm_productcategory cpc on cpc.ProductId=cp.ProductId
                                    left join cm_category cc on cc.CategoryId=cpc.CategoryId
                                    where isnull(HSN_CODE,'') <> '' and cp.companyid = {compID}";
                DataSet ds = sqlHelper.ExecuteQuery(query);
                listoraclehsnDetail = mapper.Map(ds.Tables[0]).ToList();


                //if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                //{
                //    foreach (DataRow row in ds.Tables[0].Rows)
                //    {
                //        OracleHSNDetails oraclehsn = new OracleHSNDetails();
                //        oraclehsn.hsn_sac_code = row["HSN_CODE"] != DBNull.Value ? Convert.ToString(row["HSN_CODE"]) : string.Empty;
                //        oraclehsn.description = row["HSN_DESCRIPTION"] != DBNull.Value ? Convert.ToString(row["HSN_DESCRIPTION"]) : string.Empty;
                //        oraclehsn.meaning = row["HSN_CODE"] != DBNull.Value ? Convert.ToString(row["HSN_CODE"]) : string.Empty;


                //        listoraclehsnDetail.Add(oraclehsn);
                //    }


                //}

                const string quote = "\"";
                const string quote1 = ":";


                json = "{" + quote + "hsncodes" + quote + quote1 + JsonConvert.SerializeObject(listoraclehsnDetail) + "}";


                var url = ConfigurationManager.AppSettings["ORACLE_HSN"];
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                       | SecurityProtocolType.Tls11
                       | SecurityProtocolType.Tls12
                       | SecurityProtocolType.Ssl3;
                var httpRequest = (HttpWebRequest)WebRequest.Create(url);
                httpRequest.Method = "POST";

                httpRequest.Timeout = -1;
                httpRequest.ContentType = "application/json";

                byte[] data = Encoding.ASCII.GetBytes(json);
                httpRequest.ContentLength = data.Length;
                using (var streamWriter = new StreamWriter(httpRequest.GetRequestStream()))
                {
                    streamWriter.Write(json);
                }
                logger.Info("Hitting the ORACLE_HSN URL " + url + " for the job running on >>" + now);
                var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                }
                var status = (int)httpResponse.StatusCode;
                Console.WriteLine(httpResponse.StatusCode);
                logger.Info("GetHSNOracleDetails Job Ran Successfully on >>" + now + "and got the response as >>>" + status);
                if (status == 200)
                {
                    return response;
                }
                else
                {
                    response.Message = "Failed";
                    string query2 = $@"INSERT INTO [dbo].[ERROR_LOGS] ([API], [SYSTEM], [ERROR_DESCRIPTION], [ID], [DATE_CREATED],[DATA])      
                                VALUES ('ORACLE_INVOICE','ORACLE', '{response.Message}', 'ORALCE-{DateTime.Now.Ticks.ToString()}', GETUTCDATE(),'{json}')";
                    sqlHelper.ExecuteQuery(query2);
                    return response;
                }


            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
                string query2 = $@"INSERT INTO [dbo].[ERROR_LOGS] ([API], [SYSTEM], [ERROR_DESCRIPTION], [ID], [DATE_CREATED],[DATA])      
                                VALUES ('ORACLE_INVOICE','ORACLE', '{response.Message}', 'ORALCE-{DateTime.Now.Ticks.ToString()}', GETUTCDATE(),'{json}')";
                sqlHelper.ExecuteQuery(query2);
                response.Message = ex.Message;
                logger.Error("Error in GetHSNOracleDetails Job>>>> " + ex.Message + " on " + now);
            }

            return response;
        }

        public UserToken OraclePaymentDetails(List<OraclePaymentDetails> oraclepaymentdetails)
        {
            var request = new System.Web.HttpContextWrapper(System.Web.HttpContext.Current);
            var apiKey = request.Request.Headers.GetValues("jwtToken");
            var validateToken = AuthenticateJwt.GetPrincipal(apiKey[0]);

            UserToken userToken = new UserToken();

            try
            {

                PRMUploadService prmUploadService = new PRMUploadService();
                DataTable dt = new DataTable();
                DataTable tempDT1 = new DataTable();
                tempDT1.Clear();
                List<string> columns = "PAYMENT_CODE;INVOICE_NUMBER;VENDOR_CODE;PAYMENT_AMOUNT;PAYMENT_DATE;PAYMENT_TRANSACT_ID;ERP_ID;PAYMENT_STATUS;CUSTOMER_INVOICE_NO;RECORD_ID;REQUEST_ID".Split(';').ToList();
                foreach (var column in columns)
                {
                    tempDT1.Columns.Add(column);
                }

                foreach (var x in oraclepaymentdetails)
                {
                    var dr = tempDT1.NewRow(); // have new row on each iteration
                    dr["PAYMENT_CODE"] = x.pymt_ref;
                    dr["INVOICE_NUMBER"] = x.invoice_number;
                    dr["VENDOR_CODE"] = x.vendor_code;
                    dr["PAYMENT_AMOUNT"] = x.payment_amount;
                    dr["PAYMENT_DATE"] = x.payment_date;
                    dr["PAYMENT_TRANSACT_ID"] = x.utr_number;
                    dr["ERP_ID"] = x.erp_id;
                    dr["PAYMENT_STATUS"] = x.payment_status;
                    dr["CUSTOMER_INVOICE_NO"] = x.customer_invoice_no;
                    dr["RECORD_ID"] = x.record_id;
                    dr["REQUEST_ID"] = x.request_id;
                    tempDT1.Rows.Add(dr);
                }

                string json = JsonConvert.SerializeObject(tempDT1);

                dt = (DataTable)JsonConvert.DeserializeObject(json, (typeof(DataTable)));

                if (validateToken != null && validateToken.Identity.IsAuthenticated)
                {
                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        var prChunks = dt.AsEnumerable().ToList().Chunk(10000);
                        foreach (var chunk in prChunks)
                        {

                            DataTable tempDT = new DataTable();
                            tempDT.Clear();
                            tempDT.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                            foreach (var column in columns)
                            {
                                tempDT.Columns.Add(column);
                            }

                            tempDT.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                            List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                            foreach (DataColumn col in tempDT.Columns)
                            {
                                columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                            }

                            foreach (var row in chunk)
                            {
                                var newRow = tempDT.NewRow();
                                newRow["JOB_ID"] = guid1;
                                newRow["PAYMENT_CODE"] = GetDataRowVaue(row, "PAYMENT_CODE");
                                newRow["INVOICE_NUMBER"] = GetDataRowVaue(row, "INVOICE_NUMBER");
                                newRow["VENDOR_CODE"] = GetDataRowVaue(row, "VENDOR_CODE");
                                newRow["PAYMENT_AMOUNT"] = GetDataRowVaue(row, "PAYMENT_AMOUNT");
                                string paymentdate = GetDataRowVaue(row, "PAYMENT_DATE");
                                newRow["PAYMENT_DATE"] = (paymentdate == "00000000" || paymentdate == "0" || paymentdate == "") ? null : paymentdate;
                                newRow["ERP_ID"] = GetDataRowVaue(row, "ERP_ID");
                                newRow["PAYMENT_TRANSACT_ID"] = GetDataRowVaue(row, "PAYMENT_TRANSACT_ID");
                                newRow["CUSTOMER_INVOICE_NO"] = GetDataRowVaue(row, "CUSTOMER_INVOICE_NO");
                                newRow["PAYMENT_STATUS"] = GetDataRowVaue(row, "PAYMENT_STATUS");
                                newRow["RECORD_ID"] = GetDataRowVaue(row, "RECORD_ID");
                                newRow["REQUEST_ID"] = GetDataRowVaue(row, "REQUEST_ID");
                                newRow["DATE_CREATED"] = DateTime.UtcNow;

                                tempDT.Rows.Add(newRow);
                            }
                            bizClass.BulkInsert(tempDT, "[dbo].[SAP_PAYMENT_DETAILS]", columnMappings);
                            tempDT.Rows.Clear();
                        }

                    }

                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_JOB_ID", guid);
                    bizClass.SelectList("erp_process_sap_payment_details", sd);
                    userToken.Message = "Payment Saved Successfully.";
                }
                else
                {
                    userToken.Message = "You are Unauthorized to connect PRM.";
                }

            }
            catch (Exception ex)
            {
                userToken.ErrorMessage = ex.Message;
                userToken.Message = ex.Message;
            }

            return userToken;
        }

        public UserToken OracleTDSCreditNoteDetails(List<OracleTDSCreditNoteDetails> oracletdscreditnotedetails)
        {
            var request = new System.Web.HttpContextWrapper(System.Web.HttpContext.Current);
            var apiKey = request.Request.Headers.GetValues("jwtToken");
            var validateToken = AuthenticateJwt.GetPrincipal(apiKey[0]);

            UserToken userToken = new UserToken();

            try
            {

                PRMUploadService prmUploadService = new PRMUploadService();
                DataTable dt = new DataTable();
                DataTable tempDT1 = new DataTable();
                tempDT1.Clear();
                List<string> columns = "ENTITY;CR_NO;INVOICE_TYPE;INVOICE_DATE;VENDOR_NAME;VENDOR_CODE;VENDOR_SITE_CODE;INVOICE_AMOUNT;INVOICE_CURRENCY;PAYMENT_TERMS;DESCRIPTION;PAYMENT_METHOD_LOOKUP_CODE;INVOICE_NO;LINE_ITEM_NO;LINE_ITEM_AMOUNT;PAY_GROUP;TDS_CODE;ERP_INVOICE_ID;RECORD_ID;REQUEST_ID".Split(';').ToList();
                foreach (var column in columns)
                {
                    tempDT1.Columns.Add(column);
                }

                foreach (var x in oracletdscreditnotedetails)
                {
                    var dr = tempDT1.NewRow(); // have new row on each iteration
                    dr["ENTITY"] = x.entity;
                    dr["CR_NO"] = x.cr_no;
                    dr["INVOICE_TYPE"] = x.invoice_type;
                    dr["INVOICE_DATE"] = x.invoice_date;
                    dr["VENDOR_NAME"] = x.vendor_name;
                    dr["VENDOR_CODE"] = x.vendor_code;
                    dr["VENDOR_SITE_CODE"] = x.vendor_site_code;
                    dr["INVOICE_AMOUNT"] = x.invoice_amount;
                    dr["INVOICE_CURRENCY"] = x.invoice_currency;
                    dr["PAYMENT_TERMS"] = x.payment_terms;
                    dr["DESCRIPTION"] = x.description;
                    dr["PAYMENT_METHOD_LOOKUP_CODE"] = x.payment_method_lookup_code;
                    dr["INVOICE_NO"] = x.invoice_no;
                    dr["LINE_ITEM_NO"] = x.line_item_no;
                    dr["LINE_ITEM_AMOUNT"] = x.line_item_amount;
                    dr["PAY_GROUP"] = x.pay_group;
                    dr["TDS_CODE"] = x.tds_code;
                    dr["ERP_INVOICE_ID"] = x.erp_invoice_id;
                    dr["RECORD_ID"] = x.record_id;
                    dr["REQUEST_ID"] = x.request_id;
                    tempDT1.Rows.Add(dr);
                }

                string json = JsonConvert.SerializeObject(tempDT1);

                dt = (DataTable)JsonConvert.DeserializeObject(json, (typeof(DataTable)));

                if (validateToken != null && validateToken.Identity.IsAuthenticated)
                {
                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        var prChunks = dt.AsEnumerable().ToList().Chunk(10000);
                        foreach (var chunk in prChunks)
                        {

                            DataTable tempDT = new DataTable();
                            tempDT.Clear();
                            tempDT.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                            foreach (var column in columns)
                            {
                                tempDT.Columns.Add(column);
                            }

                            tempDT.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                            List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                            foreach (DataColumn col in tempDT.Columns)
                            {
                                columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                            }

                            foreach (var row in chunk)
                            {
                                var newRow = tempDT.NewRow();
                                newRow["JOB_ID"] = guid1;
                                newRow["ENTITY"] = GetDataRowVaue(row, "ENTITY");
                                newRow["CR_NO"] = GetDataRowVaue(row, "CR_NO");
                                newRow["INVOICE_TYPE"] = GetDataRowVaue(row, "INVOICE_TYPE");
                                string invoiceDate = GetDataRowVaue(row, "INVOICE_DATE");
                                newRow["INVOICE_DATE"] = (invoiceDate == "00000000" || invoiceDate == "0" || invoiceDate == "") ? null : invoiceDate;
                                newRow["VENDOR_NAME"] = GetDataRowVaue(row, "VENDOR_NAME");
                                newRow["VENDOR_CODE"] = GetDataRowVaue(row, "VENDOR_CODE");
                                newRow["VENDOR_SITE_CODE"] = GetDataRowVaue(row, "VENDOR_SITE_CODE");
                                newRow["INVOICE_AMOUNT"] = GetDataRowVaue(row, "INVOICE_AMOUNT");
                                newRow["INVOICE_CURRENCY"] = GetDataRowVaue(row, "INVOICE_CURRENCY");
                                newRow["PAYMENT_TERMS"] = GetDataRowVaue(row, "PAYMENT_TERMS");
                                newRow["DESCRIPTION"] = GetDataRowVaue(row, "DESCRIPTION");
                                newRow["PAYMENT_METHOD_LOOKUP_CODE"] = GetDataRowVaue(row, "PAYMENT_METHOD_LOOKUP_CODE");
                                newRow["INVOICE_NO"] = GetDataRowVaue(row, "INVOICE_NO");
                                newRow["LINE_ITEM_NO"] = GetDataRowVaue(row, "LINE_ITEM_NO");
                                newRow["LINE_ITEM_AMOUNT"] = GetDataRowVaue(row, "LINE_ITEM_AMOUNT");
                                newRow["PAY_GROUP"] = GetDataRowVaue(row, "PAY_GROUP");
                                newRow["TDS_CODE"] = GetDataRowVaue(row, "TDS_CODE");
                                newRow["ERP_INVOICE_ID"] = GetDataRowVaue(row, "ERP_INVOICE_ID");
                                newRow["RECORD_ID"] = GetDataRowVaue(row, "RECORD_ID");
                                newRow["REQUEST_ID"] = GetDataRowVaue(row, "REQUEST_ID");
                                newRow["DATE_CREATED"] = DateTime.UtcNow;

                                tempDT.Rows.Add(newRow);
                            }
                            bizClass.BulkInsert(tempDT, "[dbo].[SAP_TDS_CN_DETAILS]", columnMappings);
                            tempDT.Rows.Clear();
                        }

                    }
                    userToken.Message = "TDS Credit Note Saved Successfully.";
                }
                else
                {
                    userToken.Message = "You are Unauthorized to connect PRM.";
                }

            }
            catch (Exception ex)
            {
                userToken.ErrorMessage = ex.Message;
                userToken.Message = ex.Message;
            }

            return userToken;
        }


        private static string GetDataRowVaue(DataRow row, string columnName)
        {
            if (row.Table.Columns.Contains(columnName))
            {
                return row[columnName] != DBNull.Value ? row[columnName].ToString() : string.Empty;
            }
            else
            {
                return string.Empty;
            }
        }

        public string genLineItemNum(int num)
        {
            var maxNum = 5;
            num = num * 10;
            return num.ToString().PadLeft(maxNum, '0');
        }
    }

}