﻿using PRMServices.Models;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMJeevesOracleService
    {

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "getusertoken")]
        UserToken GetUserToken();

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "getf1token")]
        UserToken GetF1Token();

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "getjeevestoken")]
        UserToken GetJeevesToken();

        [OperationContract]
        [WebInvoke(Method = "POST",
         ResponseFormat = WebMessageFormat.Json,
         BodyStyle = WebMessageBodyStyle.WrappedRequest,
         UriTemplate = "jeevesrequirementsave")]
        UserToken JeevesRequirementSave(List<JeevesRequirement> jeevesRequirement);


        [OperationContract]
        [WebInvoke(Method = "POST",
         ResponseFormat = WebMessageFormat.Json,
         BodyStyle = WebMessageBodyStyle.WrappedRequest,
         UriTemplate = "jeevesproductsave")]
        UserToken JeevesProductSave(List<JeevesProduct> jeevesProduct);

        [OperationContract]
        [WebInvoke(Method = "POST",
         ResponseFormat = WebMessageFormat.Json,
         BodyStyle = WebMessageBodyStyle.WrappedRequest,
         UriTemplate = "jeevesproductbomsave")]
        UserToken JeevesProductBOMSave(List<JeevesProduct> jeevesProduct);


        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "jeevesgrnsave")]
        UserToken JeevesGRNSave(List<JeevesGRN> jeevesGRN);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "jeevesgrnreservedsave")]
        UserToken JeevesGRNReservedSave(List<JeevesGRNReserved> jeevesGRNReserved);

        [OperationContract]
        [WebInvoke(Method = "POST",
         ResponseFormat = WebMessageFormat.Json,
         BodyStyle = WebMessageBodyStyle.WrappedRequest,
         UriTemplate = "f1requirementsave")]
        UserToken F1RequirementSave(List<F1Requirement> f1Requirement);

        [OperationContract]
        [WebInvoke(Method = "POST",
         ResponseFormat = WebMessageFormat.Json,
         BodyStyle = WebMessageBodyStyle.WrappedRequest,
         UriTemplate = "f1productsave")]
        UserToken F1ProductSave(List<F1Product> f1Product);

        [OperationContract]
        [WebInvoke(Method = "POST",
         ResponseFormat = WebMessageFormat.Json,
         BodyStyle = WebMessageBodyStyle.WrappedRequest,
         UriTemplate = "f1productbomsave")]
        UserToken F1ProductBOMSave(List<F1Product> f1Product);

        [OperationContract]
        [WebInvoke(Method = "POST",
         ResponseFormat = WebMessageFormat.Json,
         BodyStyle = WebMessageBodyStyle.WrappedRequest,
         UriTemplate = "f1productalternatesave")]
        UserToken F1ProductAlternateSave(List<F1Product> f1Product);


        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "f1grnsave")]
        UserToken F1GRNSave(List<F1GRN> f1GRN);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "oracleVendorsave")]
        UserToken OracleVendorSave(List<OracleVendor> oraclevendors);

        //[OperationContract]
        //[WebInvoke(Method = "POST",
        //ResponseFormat = WebMessageFormat.Json,
        //BodyStyle = WebMessageBodyStyle.WrappedRequest,
        //UriTemplate = "oracleHSNCodesave")]
        //UserToken OracleHSNCodeSave(List<HSN> hsn);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "oracleLocationsave")]
        UserToken OracleLocationSave(List<Location> location);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "oracleTDSsave")]
        UserToken OracleTDSSave(List<TDS> tds);

        [OperationContract]
        [WebInvoke(Method = "POST",
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.WrappedRequest,
       UriTemplate = "oraclePaygroupsave")]
        UserToken OraclePaygroupSave(List<PayGroup> paygroup);

        [OperationContract]
        [WebInvoke(Method = "POST",
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.WrappedRequest,
       UriTemplate = "oracleforexsave")]
        UserToken OracleForexSave(List<Forex> forex);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getvendorjeevesdetails")]
        Response GetVendorJeevesDetails();

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getpojeevesdetails?ponumber={ponumber}")]
        Response GetPOJeevesDetails(string ponumber);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "updatepojeevesdetails?ponumber={ponumber}")]
        Response UpdatePOJeevesDetails(string ponumber);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "deletepojeevesdetails?ponumber={ponumber}")]
        Response DeletePOJeevesDetails(string ponumber);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getinvoicejeevesdetails?invoicenumber={invoicenumber}")]
        Response GetInvoiceJeevesDetails(string invoicenumber);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "updateinvoicejeevesdetails?invoicenumber={invoicenumber}")]
        Response UpdateInvoiceJeevesDetails(string invoicenumber);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "deleteinvoicejeevesdetails?invoicenumber={invoicenumber}")]
        Response DeleteInvoiceJeevesDetails(string invoicenumber);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getvendorf1details")]
        Response GetVendorF1Details();

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "createF1Details?ponumber={ponumber}")]
        Response CreatePOF1Details(string ponumber);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "updatePoF1details?ponumber={ponumber}")]
        Response updateF1Details(string ponumber);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "createInvoiceDetails?invoicenumber={invoicenumber}")]
        Response createF1InvoiceDetails(string invoicenumber);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getinvoiceOracleDetails?type={type}&compId={compId}")]
        Response GetInvoiceOracleDetails(string type = null, int compId = 0);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "gethsnOracleDetails?compID={compID}")]
        Response GetHSNOracleDetails(int compID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "oraclepaymentdetails")]
        UserToken OraclePaymentDetails(List<OraclePaymentDetails> oraclepaymentdetails);

        [OperationContract]
        [WebInvoke(Method = "POST",
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.WrappedRequest,
       UriTemplate = "oracletdscreditnotedetails")]
        UserToken OracleTDSCreditNoteDetails(List<OracleTDSCreditNoteDetails> oracletdscreditnotedetails);


    }
}
