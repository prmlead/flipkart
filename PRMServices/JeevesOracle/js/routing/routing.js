﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider
                .state('oracleConfiguration', {
                    url: '/oracleConfiguration',
                    templateUrl: 'JeevesOracle/views/oracleConfiguration.html'
                })
                .state('errorLogs', {
                    url: '/errorLogs',
                    templateUrl: 'JeevesOracle/views/errorLogs.html'
                })

             .state('tds', {
                 url: '/tds',
                 templateUrl: 'JeevesOracle/views/tds_CMDN.html'
             });

        }]);