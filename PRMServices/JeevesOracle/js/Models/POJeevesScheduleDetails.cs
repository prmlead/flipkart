﻿using Newtonsoft.Json;
using PRM.Core.Common;
using PRM.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class POJeevesScheduleDetails 
    {
        [DataMember] [DataNames("PO_NUMBER")] public string po_number { get; set; }
        [DataMember] [DataNames("DELIVERY_LOCATION")] public string destination_asset_code { get; set; }
        [DataMember] [DataNames("VENDOR_SITE_CODE")] public string vendor_site_code { get; set; }
        [DataMember] [DataNames("VENDOR_CODE")] public string vendor_code { get; set; }
        [DataMember] [DataNames("VENDOR_NAME")] public string vendor_name { get; set; }
        [DataMember] [DataNames("BILL_TO_LOCATION")] public string bill_to_location { get; set; }
        [DataMember] [DataNames("SHIP_TO_LOCATION")] public string ship_to_location { get; set; }
        [DataMember] [DataNames("CREATED_AT")] public string created_at { get; set; }
        [DataMember] [DataNames("RELEASED_AT")] public string released_at { get; set; }
        [DataMember] [DataNames("TYPE_OF_STOCK")] public string type_of_stock { get; set; }
        [DataMember] [DataNames("ORDER_QUANTITY")] public decimal order_quantity { get; set; }
        [DataMember] [DataNames("TOTAL_PRICE")] public decimal total_price { get; set; }
        [DataMember] [DataNames("PO_LINE_ITEMS")] public List<POJeevesItems> line_items { get; set; }

    }

    public class POJeevesItems
    {
        [JsonIgnore]
        [DataMember] [DataNames("PO_NUMBER")] public string po_number { get; set; }
        [DataMember] [DataNames("SKU")] public string sku { get; set; }
        [DataMember] [DataNames("ITEM_DESC")] public string item_description { get; set; }
        [DataMember] [DataNames("OWNER")] public string owner { get; set; }
        [DataMember] [DataNames("OPEN_QUANTITY")] public decimal open_quantity { get; set; }
        [DataMember] [DataNames("TOTAL_QUANTITY")] public decimal total_quantity { get; set; }
        [DataMember] [DataNames("ATTRIBUTES")] public Attributes attributes { get; set; }


    }

    public class DeletePOJeeves
    {
        [DataMember] [DataNames("PO_NUMBER")] public string po_number { get; set; }
        [DataMember] [DataNames("DELIVERY_LOCATION")] public string destination_asset_code { get; set; }
        [DataMember] [DataNames("VENDOR_CODE")] public string vendor_code { get; set; }


    }


    public class Attributes
    {
        [DataMember] [DataNames("COST_INCLUDING_TAX")] public decimal cost_including_tax { get; set; }
        [DataMember] [DataNames("COST_EXCLUDING_TAX")] public decimal cost_excluding_tax { get; set; }

    }


}