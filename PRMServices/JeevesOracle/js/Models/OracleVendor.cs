﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class OracleVendor
    {
        [DataMember] [DataNames("COMPANY_NAME")] public string company_name { get; set; }
        [DataMember] [DataNames("VENDOR_CODE")] public string vendor_code { get; set; }
        [DataMember] [DataNames("GST_NUMBER")] public string gst_number { get; set; }
        [DataMember] [DataNames("PAN_NUMBER")] public string pan_number { get; set; }
        [DataMember] [DataNames("EMAIL")] public string email { get; set; }
        [DataMember] [DataNames("PHONE_NUMBER")] public string phone_number { get; set; }
        [DataMember] [DataNames("CURRENCY")] public string currency { get; set; }
        [DataMember] [DataNames("CONTACT_NAME")] public string contact_name { get; set; }
        [DataMember] [DataNames("BILLING_ADDRESS")] public string billing_address { get; set; }
        [DataMember] [DataNames("BILLING_CITY")] public string billing_city { get; set; }
        [DataMember] [DataNames("BILLING_STATE")] public string billing_state { get; set; }
        [DataMember] [DataNames("BILLING_PINCODE")] public string billing_pincode { get; set; }
        [DataMember] [DataNames("BILLING_COUNTRY")] public string billing_country { get; set; }

        [DataMember] [DataNames("SHIPPING_ADDRESS")] public string shipping_address { get; set; }
        [DataMember] [DataNames("SHIPPING_COUNTRY")] public string shipping_country { get; set; }
        [DataMember] [DataNames("SHIPPING_STATE")] public string shipping_state { get; set; }
        [DataMember] [DataNames("SHIPPING_CITY")] public string shipping_city { get; set; }
        [DataMember] [DataNames("SHIPPING_PINCODE")] public string shipping_pincode { get; set; }
        [DataMember] [DataNames("ENTITY_NAME")] public string entity_name { get; set; }
        [DataMember] [DataNames("VENDOR_TYPE")] public string vendor_type { get; set; }
        [DataMember] [DataNames("VENDOR_CATEGORY")] public string vendor_category { get; set; }
        [DataMember] [DataNames("VENDOR_SITE_CODE")] public string vendor_site_code { get; set; }
        [DataMember] [DataNames("PAYMENT_METHOD")] public string payment_method { get; set; }
        [DataMember] [DataNames("EMAIL_ALT1")] public string email_alt1 { get; set; }
        [DataMember] [DataNames("VENDOR_STATUS")] public string vendor_status { get; set; }
        [DataMember] [DataNames("VENDOR_SITE_STATUS")] public string vendor_site_status { get; set; }
        [DataMember] [DataNames("VENDOR_INACTIVE_DATE")] public string vendor_inactive_date { get; set; }
        [DataMember] [DataNames("VENDOR_SITE_INACTIVE_DATE")] public string vendor_site_inactive_date { get; set; }
        [DataMember] [DataNames("ORACLE_VENDOR_ID")] public int vendor_id { get; set; }
        [DataMember] [DataNames("ORACLE_VENDOR_SITE_ID")] public int vendor_site_id { get; set; }
        [DataMember] [DataNames("REQUEST_ID")] public int request_id { get; set; }
        [DataMember] [DataNames("RECORD_ID")] public int record_id { get; set; }



    }



}