﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class f1InvoiceDetails
    {
        [DataNames("PO_NUMBER")] public string prm360PoNumber { get; set; }
        [DataMember] [DataNames("INVOICE_NO")] public string invoiceNo { get; set; }
        [DataMember] [DataNames("INVOICE_DATE")] public string invoiceDate { get; set; }
        [DataMember] [DataNames("ATTACHMENTS")] public string invoiceAttachment { get; set; }
        [DataMember] [DataNames("TAX")] public decimal taxRate { get; set; }
        [DataMember] [DataNames("INVOICE_AMOUNT")] public decimal totalAmt { get; set; }
        [DataMember] [DataNames("SVC_CENTER_CODE")] public string serviceCenterCode { get; set; }
        [DataMember] [DataNames("ITEM_SOURCE")] public int itemSource { get; set; }
        [DataMember] [DataNames("ITEM_DETAILS")] public  List<invoiceItems> itemDetails { get; set; }
        

    }

    public class invoiceItems
    {

        [DataMember] [DataNames("PO_NUMBER")] public string prm360PoNumber { get; set; }
        [DataMember] [DataNames("PO_LINE_ITEM")] public int poLineNumber { get; set; }
        [DataMember] [DataNames("F1_PO_NUMBER")] public string f1PoNumber { get; set; }
        [DataMember] [DataNames("ITEM_CODE")] public string itemCode { get; set; }
        [DataMember] [DataNames("ITEM_CODE")] public string invoicedItemCode { get; set; }
        [DataMember] [DataNames("INVOICE_QTY")] public int itemQty { get; set; }
        [DataMember] [DataNames("NET_PRICE")] public decimal unitPrice { get; set; }
        [DataMember] [DataNames("LINE_ITEM_AMOUNT")] public decimal totalPrice { get; set; }


    }
}