﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class F1Requirement
    {
        [DataMember] [DataNames("ENTITY")] public string entity { get; set; }
        [DataMember] [DataNames("TITLE")] public string rfq_title { get; set; }
        [DataMember] [DataNames("REQUIRED_QUANTITY")] public string required_quantity { get; set; }
        [DataMember] [DataNames("CASE_ID")] public string jobsheet_id { get; set; }
        [DataMember] [DataNames("TYPE_OF_STOCK")] public string type_of_stock { get; set; }
        [DataMember] [DataNames("DELIVERY_LOCATION")] public string delivery_location { get; set; }
        [DataMember] [DataNames("REQUIREMENT_LINE_ITEMS")] public List<F1RequirementItems> requirement_line_items { get; set; }
        
    }

    public class F1RequirementItems
    {
        [DataMember] [DataNames("PRODUCT_CODE")] public string product_code { get; set; }
        [DataMember] [DataNames("REQUIRED_QUANTITY")] public string required_quantity { get; set; }


    }


}