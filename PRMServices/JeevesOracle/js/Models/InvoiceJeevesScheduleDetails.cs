﻿using Newtonsoft.Json;
using PRM.Core.Common;
using PRM.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class InvoiceJeevesScheduleDetails 
    {
        [DataMember] [DataNames("INVOICE_NO")] public string invoice_no { get; set; }
        [DataMember] [DataNames("INVOICE_TYPE")] public string invoice_type { get; set; }
        [DataMember] [DataNames("INVOICE_DATE")] public string invoice_date { get; set; }
        [DataMember] [DataNames("INVOICE_QUANTITY")] public decimal invoice_quantity { get; set; }
        [DataMember] [DataNames("REMAINING_QUANTITY")] public decimal remaining_quantity { get; set; }
        [DataMember] [DataNames("INVOICE_AMOUNT")] public decimal invoice_amount { get; set; }
        [DataMember] [DataNames("ENTITY")] public string entity { get; set; }
        [DataMember] [DataNames("PO_NUMBER")] public string po_number { get; set; }
        //[DataMember] [DataNames("ASN_CODE")] public string asn_code { get; set; }
        [DataMember] [DataNames("BILLED_TO")] public string billed_to { get; set; }
        [DataMember] [DataNames("DESTINATION_ASSET_CODE")] public string destination_asset_code { get; set; }
        //[DataMember] [DataNames("VENDOR_NAME")] public string vendor_name { get; set; }
        [DataMember] [DataNames("VENDOR_SITE_CODE")] public string vendor_site_code { get; set; }
        [DataMember] [DataNames("VENDOR_CODE")] public string vendor_code { get; set; }
        [DataMember] [DataNames("SHIPPED_FROM")] public string shipped_from { get; set; }
        [DataMember] [DataNames("SHIPPED_TO")] public string shipped_to { get; set; }
        [DataMember] [DataNames("TYPE_OF_STOCK")] public string type_of_stock { get; set; }
        [DataMember] [DataNames("PO_LINE_ITEMS")] public List<InvoiceJeevesItems> po_line_item { get; set; }

    }
    public class InvoiceJeevesItems
    {
        [JsonIgnore]
        [DataMember] [DataNames("INVOICE_NO")] public string invoice_no { get; set; }
        [DataMember] [DataNames("ITEM_CODE")] public string item_code { get; set; }
        [DataMember] [DataNames("ITEM_NAME")] public string item_name { get; set; }
        [DataMember] [DataNames("QUANTITY")] public decimal quantity { get; set; }
        //[DataMember] [DataNames("ASN_QUANTITY")] public decimal asn_quantity { get; set; }
        [DataMember] [DataNames("ACCEPTED_QUANTITY")] public decimal accepted_quantity { get; set; }
        [DataMember] [DataNames("RETURN_QUANTITY")] public decimal return_quantity { get; set; }
        [DataMember] [DataNames("AMOUNT")] public decimal amount { get; set; }
        [DataMember] [DataNames("SUB_TOTAL")] public decimal sub_total { get; set; }
        [DataMember] [DataNames("TAX")] public decimal tax { get; set; }
        //[DataMember] [DataNames("ATTACHMENTS")] public string attachment { get; set; }

    }

    public class DeleteInvoiceJeeves
    {
        [DataMember] [DataNames("DESTINATION_ASSET_CODE")] public string destination_asset_code { get; set; }
        [DataMember] [DataNames("VENDOR_NAME")] public string vendor_name { get; set; }
        [DataMember] [DataNames("VENDOR_SITE_CODE")] public string vendor_site_code { get; set; }
        [DataMember] [DataNames("INVOICE_NUMBER")] public string invoice_number { get; set; }

    }


    //public class Attributes
    //{
    //    [DataMember] [DataNames("COST_INCLUDING_TAX")] public decimal cost_including_tax { get; set; }
    //    [DataMember] [DataNames("COST_EXCLUDING_TAX")] public decimal cost_excluding_tax { get; set; }

    //}


}