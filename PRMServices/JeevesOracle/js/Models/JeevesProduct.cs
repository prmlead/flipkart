﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class JeevesProduct
    {

        [DataMember] [DataNames("PRODUCT_CODE")] public string product_code { get; set; }
        [DataMember] [DataNames("EPN")] public string epn { get; set; }
        [DataMember] [DataNames("PRODUCT_NAME")] public string product_name { get; set; }
        [DataMember] [DataNames("PRODUCT_DESCRIPTION")] public string product_description { get; set; }
        [DataMember] [DataNames("CATEGORY_NAME")] public string category_name { get; set; }
        [DataMember] [DataNames("SERVICE_TYPE")] public string  service_type { get; set; }
        [DataMember] [DataNames("SUPER_CATEGORY")] public string super_category { get; set; }
        [DataMember] [DataNames("SUB_CATEGORY")] public string sub_category { get; set; }
        [DataMember] [DataNames("ITEM_REG_STATUS")] public string item_reg_status { get; set; }
        [DataMember] [DataNames("MODEL_ID")] public string model_id { get; set; }
        [DataMember] [DataNames("MODEL_NAME")] public string model_name { get; set; }
        [DataMember] [DataNames("BRAND_PART_CODE")] public string brand_part_code { get; set; }
        [DataMember] [DataNames("BRAND")] public string brand { get; set; }
        [DataMember] [DataNames("BRAND_PRODUCT_CATEGORY_cODE")] public string brand_product_category_code { get; set; }
        [DataMember] [DataNames("JSN")] public string jsn { get; set; }
        [DataMember] [DataNames("FSN")] public string fsn { get; set; }
        [DataMember] [DataNames("HSN_CODE")] public string hsn_code { get; set; }
        [DataMember] [DataNames("IMEI_NO")] public string imei_no { get; set; }
        [DataMember] [DataNames("ESN_NO")] public string esn_no { get; set; }
        [DataMember] [DataNames("ITEM_SERIALLZED_STATUS")] public string item_serialized_status { get; set; }
        [DataMember] [DataNames("UOM")] public string uom { get; set; }
        [DataMember] [DataNames("WARRANTY")] public string warranty { get; set; }
        [DataMember] [DataNames("PRODUCT_LIFE")] public string product_life { get; set; }
        [DataMember] [DataNames("WARRANTY_COVERAGE")] public string warranty_coverage { get; set; }
        [DataMember] [DataNames("IS_CONSUMABLE")] public string is_consumable { get; set; }
        [DataMember] [DataNames("IS_CHARGABLE")] public string is_chargable { get; set; }
        [DataMember] [DataNames("IS_RETURNABLE")] public string is_returnable { get; set; }
        [DataMember] [DataNames("IS_HIGHVALUE")] public string is_highvalue { get; set; }
        [DataMember] [DataNames("IS_SERIALIZED")] public string is_serialized { get; set; }
        [DataMember] [DataNames("IS_REPLACEMENT")] public string is_replacement { get; set; }
        [DataMember] [DataNames("IS_ADVANCE_REPLACEMENT")] public string is_advance_replacement { get; set; }
        [DataMember] [DataNames("REPAIR_LEVEL")] public string repair_level { get; set; }
        [DataMember] [DataNames("PRIORITY_LEVEL")] public string priority_level { get; set; }
        [DataMember] [DataNames("CONDITION")] public string condition { get; set; }
        [DataMember] [DataNames("ENTITY")] public string entity { get; set; }
        [DataMember] [DataNames("VERTICAL")] public string vertical { get; set; }
        [DataMember] [DataNames("SUB_ENTITY")] public string sub_entity { get; set; }
        [DataMember] [DataNames("CUSTOMER_PRICE")] public decimal customer_price { get; set; }
        [DataMember] [DataNames("SF_PRICE")] public decimal sf_price { get; set; }
        [DataMember] [DataNames("LANDING_PRICE")] public decimal landing_price { get; set; }
        [DataMember] [DataNames("STOCK_TRANSPORTER_PRICE")] public decimal stock_transporter_price { get; set; }
        [DataMember] [DataNames("HANDLING_CHARGES")] public decimal handling_charges { get; set; }
        [DataMember] [DataNames("ITEM_WEIGHT")] public decimal item_weight { get; set; }
        [DataMember] [DataNames("DISCOUNT")] public decimal discount { get; set; }

    }


}