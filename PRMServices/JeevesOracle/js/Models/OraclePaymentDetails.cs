﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class OraclePaymentDetails
    {
        [DataMember] [DataNames("PAYMENT_CODE")] public string pymt_ref { get; set; }
        [DataMember] [DataNames("INVOICE_NUMBER")] public string invoice_number { get; set; }
        [DataMember] [DataNames("VENDOR_CODE")] public string vendor_code { get; set; }
        [DataMember] [DataNames("PAYMENT_AMOUNT")] public decimal payment_amount { get; set; }
        [DataMember] [DataNames("PAYMENT_DATE")] public string payment_date { get; set; }
        [DataMember] [DataNames("PAYMENT_TRANSACT_ID")] public string utr_number { get; set; }
        [DataMember] [DataNames("ERP_ID")] public string erp_id { get; set; }
        [DataMember] [DataNames("PAYMENT_STATUS")] public string payment_status { get; set; }
        [DataMember] [DataNames("CUSTOMER_INVOICE_NO")] public string customer_invoice_no { get; set; }
        [DataMember] [DataNames("RECORD_ID")] public int record_id { get; set; }
        [DataMember] [DataNames("REQUEST_ID")] public int request_id { get; set; }


    }



}