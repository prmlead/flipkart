﻿using Newtonsoft.Json;
using PRM.Core.Common;
using PRM.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;


namespace PRMServices.Models
{
    [DataContract]
    
    public class POF1Details
    {
        [DataMember] [DataNames("PO_NUMBER")] public string prm360PoNumber { get; set; }
        [DataMember] [DataNames("PO_DATE")] public string poDate { get; set; }
        [DataMember] [DataNames("SVC_CENTER_CODE")] public string serviceCenterCode { get; set; }
        [DataMember] [DataNames("VENDOR_CODE")] public string vendorCode { get; set; }
        [DataMember] [DataNames("ITEM_SOURCE")] public int itemSource { get; set; }
        [DataMember] [DataNames("SHIPPING_WAY")] public int shippingWay { get; set; }  
        [DataMember] [DataNames("ITEM_DETAILS")] public List<POF1Items> itemDetails { get; set; }

    }
    public class POF1Items
    {
        [JsonIgnore]
        [DataMember] [DataNames("PO_NUMBER")] public string prm360PoNumber { get; set; }
        [DataMember] [DataNames("ITEM_CODE")] public string itemCode { get; set; }
        [DataMember] [DataNames("PO_LINE_ITEM")] public int poLineNumber { get; set; }
        [DataMember] [DataNames("OPEN_QUANTITY")] public decimal itemQty { get; set; }
        [DataMember] [DataNames("UNIT_PRICE")] public decimal unitPrice { get; set; }
        [DataMember] [DataNames("F1_PO_NUMBER")] public string f1PoNumber { get; set; }


    }
}