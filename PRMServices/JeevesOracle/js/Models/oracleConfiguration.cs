﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class HSN
    {
        [DataMember] [DataNames("HSN_CODE")] public string hsn_code { get; set; }
        [DataMember] [DataNames("HSN_DESC")] public string hsn_desc { get; set; }
        [DataMember] [DataNames("HSN_TYPE")] public string hsn_type { get; set; }
        [DataMember] [DataNames("HSN_STATUS")] public string hsn_status { get; set; }
        [DataMember] [DataNames("REQUEST_ID")] public int request_id { get; set; }
        [DataMember] [DataNames("RECORD_ID")] public int record_id { get; set; }

    }

    public class TDS
    {
        [DataMember] [DataNames("TDS_ID")] public int tds_id { get; set; }
        [DataMember] [DataNames("TDS_CODE")] public string tds_code { get; set; }
        [DataMember] [DataNames("TDS_STATUS")] public string tds_status { get; set; }
        [DataMember] [DataNames("TDS_ENTITY_NAME")] public string entity_name { get; set; }
        [DataMember] [DataNames("REQUEST_ID")] public int request_id { get; set; }
        [DataMember] [DataNames("RECORD_ID")] public int record_id { get; set; }

    }

    public class Location
    {
        [DataMember] [DataNames("LOCATION_NAME")] public string location_name { get; set; }
        [DataMember] [DataNames("LOCATION_SITE")] public string location_site { get; set; }
        [DataMember] [DataNames("LOCATION_CODE")] public string location_code { get; set; }
        [DataMember] [DataNames("LOCATION_ID")] public int location_id { get; set; }
        [DataMember] [DataNames("LOCATION_STATUS")] public string location_status { get; set; }
        [DataMember] [DataNames("LOCATION_INACTIVE_DATE")] public string inactive_date { get; set; }
        [DataMember] [DataNames("REQUEST_ID")] public int request_id { get; set; }
        [DataMember] [DataNames("RECORD_ID")] public int record_id { get; set; }

    }

    public class PayGroup
    {
        [DataMember] [DataNames("PAYGROUP_CODE")] public string paygroup_code { get; set; }
        [DataMember] [DataNames("PAYGROUP_DESC")] public string paygroup_desc { get; set; }
        [DataMember] [DataNames("REQUEST_ID")] public int request_id { get; set; }
        [DataMember] [DataNames("RECORD_ID")] public int record_id { get; set; }
    }

    public class Forex
    {
        [DataMember] [DataNames("CURRENCY")] public string currency { get; set; }
        [DataMember] [DataNames("RATE")] public string rate { get; set; }
        [DataMember] [DataNames("FROM_DATE")] public string from_date { get; set; }
        [DataMember] [DataNames("TO_DATE")] public string to_date { get; set; }
        [DataMember] [DataNames("REQUEST_ID")] public int request_id { get; set; }
        [DataMember] [DataNames("RECORD_ID")] public int record_id { get; set; }
    }



    public class ErrorLogs
    {
        [DataMember] [DataNames("ERROR_DESCRIPTION")] public string ERROR_DESCRIPTION { get; set; }
        [DataMember] [DataNames("SYSTEM")] public string SYSTEM { get; set; }
        [DataMember] [DataNames("API")] public string API { get; set; }
        [DataMember] [DataNames("DATA")] public string DATA { get; set; }
        [DataMember] [DataNames("ID")] public string ID { get; set; }
        [DataMember] [DataNames("DATE_CREATED")] public DateTime? DATE_CREATED { get; set; }
        [DataMember] [DataNames("TYPE")] public string TYPE { get; set; }
        [DataMember] [DataNames("VALUE")] public string VALUE { get; set; }
        [DataMember] [DataNames("TOTAL_COUNT")] public int TOTAL_COUNT { get; set; }


    }



}