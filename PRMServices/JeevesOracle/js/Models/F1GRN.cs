﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class F1GRN
    {
        [DataMember] [DataNames("PO_NUMBER")] public string po_number { get; set; }
        [DataMember] [DataNames("PO_LINE_ITEM")] public string po_line_item { get; set; }
        [DataMember] [DataNames("TYPE_OF_STOCK")] public string type_of_stock { get; set; }
        [DataMember] [DataNames("GRN_NO")] public string grn_no { get; set; }
        [DataMember] [DataNames("GRN_STATUS")] public string grn_status { get; set; }
        [DataMember] [DataNames("INVOICE_DOC")] public string invoice_doc { get; set; }
        [DataMember] [DataNames("PO_LINE_ITEMS")] public List<F1POItems> po_line_items { get; set; }
        [DataMember] [DataNames("FOC_REQ_NUM")] public string foc_req_num { get; set; }
        [DataMember] [DataNames("SHIPPED_LOCATION_CODE")] public string shipped_location_code { get; set; }
        [DataMember] [DataNames("VENDOR_SITE_CODE")] public string vendor_site_code { get; set; }
        [DataMember] [DataNames("INVOICE_NO")] public string invoice_no { get; set; }
        [DataMember] [DataNames("INVOICE_DATE")] public string invoice_date { get; set; }
        [DataMember] [DataNames("GRN_DATE")] public string grn_date { get; set; }
        [DataMember] [DataNames("INVOICE_AMOUNT")] public decimal invoice_amount { get; set; }
    }

    public class F1POItems
    {

        [DataMember] [DataNames("PO_LINE_ITEM_DESC")] public string po_line_item_desc { get; set; }
        [DataMember] [DataNames("PO_LINE_ITEM_SKU_ID")] public string po_line_item_sku_id { get; set; }
        [DataMember] [DataNames("PO_QTY")] public decimal po_qty { get; set; }
        [DataMember] [DataNames("RECEIVED_QTY")] public decimal received_qty { get; set; }
        [DataMember] [DataNames("LOST_QTY")] public decimal lost_qty { get; set; }
        [DataMember] [DataNames("BAD_QTY")] public decimal bad_qty { get; set; }

    }


}