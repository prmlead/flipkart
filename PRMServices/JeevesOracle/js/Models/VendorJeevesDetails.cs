﻿using PRM.Core.Common;
using PRM.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class VendorJeevesDetails
    {
        [DataMember] [DataNames("COMPANY_NAME")] public string company_name { get; set; }
        [DataMember] [DataNames("VENDOR_CODE")] public string  vendor_code { get; set; }
        [DataMember] [DataNames("VENDOR_SITE_CODE")] public string vendor_site_code { get; set; }
        [DataMember] [DataNames("GST_NUMBER")] public string gst_number { get; set; }
        [DataMember] [DataNames("PAN_NUMBER")] public string pan_number { get; set; }
        [DataMember] [DataNames("CONTACT_NAME")] public string contact_name { get; set; }
        [DataMember] [DataNames("CONTACT_DESIGNATION")] public string contact_designation { get; set; }
        [DataMember] [DataNames("EMAIL")] public string email { get; set; }
        [DataMember] [DataNames("PHONE_NUMBER")] public string phone_number { get; set; }
        [DataMember] [DataNames("CURRENCY")] public string currency { get; set; }
        [DataMember] [DataNames("CONTACT_PERSON")] public string contact_person { get; set; }
        [DataMember] [DataNames("VENDOR_CATEGORY")] public string vendor_category { get; set; }
        [DataMember] [DataNames("ENTITY")] public string entity { get; set; }
        [DataMember] [DataNames("BILLING_ADDRESS")] public BillingAddress billing_address { get; set; }
        [DataMember] [DataNames("SHIPPING_ADDRESS")] public ShippingAddress shipping_address { get; set; }

    }

    public class BillingAddress
    {
        [DataMember] [DataNames("VALUE")] public string value { get; set; }
        [DataMember] [DataNames("COUNTRY")] public string country { get; set; }
        [DataMember] [DataNames("CITY")] public string city { get; set; }
        [DataMember] [DataNames("STATE")] public string state { get; set; }
        [DataMember] [DataNames("PIN_CODE")] public string pin_code { get; set; }

    }
    public class ShippingAddress
    {
        [DataMember] [DataNames("VALUE")] public string value { get; set; }
        [DataMember] [DataNames("COUNTRY")] public string country { get; set; }
        [DataMember] [DataNames("CITY")] public string city { get; set; }
        [DataMember] [DataNames("STATE")] public string state { get; set; }
        [DataMember] [DataNames("PIN_CODE")] public string pin_code { get; set; }

    }

}