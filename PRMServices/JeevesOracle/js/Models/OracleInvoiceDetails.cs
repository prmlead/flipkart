﻿using PRM.Core.Common;
using PRM.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class OracleInvoiceDetails
    {
        [DataMember] [DataNames("PO_NUMBER")] public string po_number { get; set; }
        [DataMember] [DataNames("INVOICE_NO")] public string invoice_number { get; set; }
        [DataMember] [DataNames("INVOICE_AMOUNT")] public decimal invoice_amount { get; set; }
        [DataMember] [DataNames("VENDOR_NAME")] public string vendor_name { get; set; }
        [DataMember] [DataNames("VENDOR_CODE")] public string vendor_number { get; set; }
       
        [DataMember] [DataNames("VENDOR_SITE_CODE")] public string vendor_site_code { get; set; }
        [DataMember] [DataNames("INVOICE_TYPE_LOOKUP")] public string invoice_type { get; set; }
        [DataMember] [DataNames("INVOICE_DATE")] public string invoice_date { get; set; }
        [DataMember] [DataNames("TRANSACTION_TYPE")] public string transaction_type { get; set; }
        [DataMember] [DataNames("NATURE_OF_TRANSACTION")] public string nature_of_transaction { get; set; }
        [DataMember] [DataNames("INTENT_OF_PURCHASE")] public string intent_of_purchase { get; set; }
        [DataMember] [DataNames("BILL_TO_LOCATION")] public string bill_to_location { get; set; }
        [DataMember] [DataNames("SHIP_TO_LOCATION")] public string ship_to_location { get; set; }
        [DataMember] [DataNames("REGISTERED_UNDER_GST")] public string registered_under_gst { get; set; }
        [DataMember] [DataNames("IS_RCM_APPLICABLE")] public string is_rcm_applicable { get; set; }
        [DataMember] [DataNames("CURRENCY")] public string invoice_currency_code { get; set; }
        [DataMember] [DataNames("EXCHANGE_RATE")] public decimal exchange_rate { get; set; }
        [DataMember] [DataNames("PAYMENT_TERMS")] public string terms_name { get; set; }
        [DataMember] [DataNames("PAYMENT_METHOD")] public string payment_method { get; set; }
        [DataMember] [DataNames("ENTITY")] public string entity { get; set; }
        [DataMember] [DataNames("PAY_GROUP_DESCRIPTION")] public string pay_group { get; set; }

        //[DataMember] [DataNames("SHIP_TO")] public string ship_to { get; set; }
        [DataMember] [DataNames("TDS_CODE")] public string tds_code { get; set; }
        [DataMember] [DataNames("HSN_CODE")] public string hsn_sac_code { get; set; }
        [DataMember] [DataNames("PRM_GENERATED_INVOICE_ID")] public string erp_id { get; set; }
        [DataMember] [DataNames("IRN_NO")] public string irn_no { get; set; }
        [DataMember] [DataNames("ATTACHMENT_URL")] public string attachement_url { get; set; }
        [DataMember] [DataNames("INVOICE_RECEIVED_DATE")] public string invoice_received_date { get; set; }
        //[DataMember] [DataNames("INVOICE_LINE_ITEMS")] public List<OracleJeevesItems> invoice_line_item { get; set; }
        [DataMember] [DataNames("LINE_ITEM_NUMBER")] public int line_number { get; set; }
        [DataMember] [DataNames("LINE_ITEM_NAME")] public string line_description { get; set; }
        [DataMember] [DataNames("LINE_ITEM_AMOUNT")] public decimal line_amount { get; set; }
        [DataMember] [DataNames("TAX_CODE")] public string tax_rate_code { get; set; }
        [DataMember] [DataNames("TAX")] public decimal tax_rate_percentage { get; set; }
        [DataMember] [DataNames("REMARKS")] public string description { get; set; }

        [DataMember] [DataNames("LINE_TYPE")] public string inv_line_type { get; set; }
        [DataMember] [DataNames("SEGMENT1")] public string segment1 { get; set; }
        [DataMember] [DataNames("SEGMENT2")] public string segment2 { get; set; }
        [DataMember] [DataNames("SEGMENT3")] public string segment3 { get; set; }
        [DataMember] [DataNames("SEGMENT4")] public string segment4 { get; set; }
        [DataMember] [DataNames("SEGMENT5")] public string segment5 { get; set; }
        [DataMember] [DataNames("SEGMENT6")] public string segment6 { get; set; }
        [DataMember] [DataNames("SEGMENT7")] public string segment7 { get; set; }
        [DataMember] [DataNames("SEGMENT8")] public string segment8 { get; set; }



    }

    public class OracleJeevesItems
    {
        [DataMember] [DataNames("INVOICE_NO")] public string invoice_no { get; set; }
        [DataMember] [DataNames("LINE_ITEM_NUMBER")] public string line_item_number { get; set; }
        [DataMember] [DataNames("LINE_ITEM_NAME")] public string line_item_name { get; set; }
        [DataMember] [DataNames("LINE_ITEM_AMOUNT")] public decimal line_item_amount { get; set; }
        [DataMember] [DataNames("TAX_CODE")] public string tax_code { get; set; }
        [DataMember] [DataNames("TAX")] public decimal tax { get; set; }
        [DataMember] [DataNames("REMARKS")] public string remarks { get; set; }

    }


}