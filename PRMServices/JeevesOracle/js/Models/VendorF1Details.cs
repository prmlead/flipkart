﻿using PRM.Core.Common;
using PRM.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class VendorF1Details
    {
        [DataMember] [DataNames("COMPANY_NAME")] public string vendorName { get; set; }
        [DataMember] [DataNames("VENDOR_CODE")] public string vendorCode { get; set; }
        [DataMember] [DataNames("ASSOCIATE_TYPE_CODE")] public string associateTypeCode { get; set; }
        [DataMember] [DataNames("BILLING_ADDRESS")] public string billingAddress { get; set; }
        [DataMember] [DataNames("BILLING_PINCODE")] public string billingPincode { get; set; }
        [DataMember] [DataNames("SHIPPING_ADDRESS")] public string shippingAddress { get; set; }
        [DataMember] [DataNames("SHIPPING_PINCODE")] public string shippingPincode { get; set; }
        [DataMember] [DataNames("ASSOCIATE_LEVEL")] public string associateLevel { get; set; }
        [DataMember] [DataNames("VENDOR_LEAD_TIME")] public int vendorLeadTime { get; set; }
        [DataMember] [DataNames("DEFECTIVE_SERVICE")] public int defectiveService { get; set; }
        [DataMember] [DataNames("PEMITTED_BRANDS")] public string pemittedBrands { get; set; }
        [DataMember] [DataNames("GST_IN_NUMBER")] public string gstinNumber { get; set; }
        [DataMember] [DataNames("PAN_NUMBER")] public string panNumber { get; set; }
        [DataMember] [DataNames("CONTACT_PERSON_NAME")] public string contactPersonName { get; set; }
        [DataMember] [DataNames("CONTACT_PERSON_DESIGNATION")] public string contactPersonDesignation { get; set; }
        [DataMember] [DataNames("MOBILE_NO")] public string mobileNo { get; set; }
        [DataMember] [DataNames("EMAIL_ID")] public string emailId { get; set; }
        [DataMember] [DataNames("ORACLE_CODE")] public string oracleCode { get; set; }

    }

}