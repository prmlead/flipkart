﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class JeevesRequirement
    {
        [DataMember] [DataNames("ENTITY")] public string entity { get; set; }
        [DataMember] [DataNames("TITLE")] public string title { get; set; }
        [DataMember] [DataNames("REQUIRED_QUANTITY")] public string required_quantity { get; set; }
        [DataMember] [DataNames("UOM")] public string uom { get; set; }
        [DataMember] [DataNames("CASE_ID")] public string case_id { get; set; }
        [DataMember] [DataNames("TYPE_OF_STOCK")] public string type_of_stock { get; set; }
        [DataMember] [DataNames("REQUIREMENT_LINE_ITEMS")] public List<JeevesRequirementItems> requirement_line_items { get; set; }
        [DataMember] [DataNames("PRODUCT_CODE")] public string product_code { get; set; }
        [DataMember] [DataNames("MODEL_NAME")] public string model_name { get; set; }
        [DataMember] [DataNames("BRAND_PART_CODE")] public string brand_part_code { get; set; }
        [DataMember] [DataNames("BRAND")] public string brand { get; set; }
        [DataMember] [DataNames("BRAND_PRODUCT_CATEGORY_CODE")] public string brand_product_category_code { get; set; }
        [DataMember] [DataNames("DELIVERY_LOCATION")] public string delivery_location { get; set; }
        [DataMember] [DataNames("PAYMENT_TERMS")] public string payment_terms { get; set; }
        [DataMember] [DataNames("CONTACT_DETAILS")] public string contact_details { get; set; }
        [DataMember] [DataNames("URGENCY")] public string urgency { get; set; }
        [DataMember] [DataNames("CURRENCY")] public string currency { get; set; }
        [DataMember] [DataNames("FREEZE_TIME")] public DateTime freeze_time { get; set; }
        [DataMember] [DataNames("EXP_DELIVERY_DATE")] public DateTime exp_delivery_date { get; set; }
        [DataMember] [DataNames("NEGOTIATION_TIME")] public DateTime negotiation_time { get; set; }
    }

    public class JeevesRequirementItems 
    {

        [DataMember] [DataNames("PRODUCT_CODE")] public string product_code { get; set; }
        [DataMember] [DataNames("MODEL_NAME")] public string model_name { get; set; }
        [DataMember] [DataNames("BRAND_PART_CODE")] public string brand_part_code { get; set; }
        [DataMember] [DataNames("BRAND")] public string brand { get; set; }
        [DataMember] [DataNames("BRAND_PRODUCT_CATEGORY_CODE")] public string brand_product_category_code { get; set; }
        [DataMember] [DataNames("DELIVERY_LOCATION")] public string delivery_location { get; set; }
        [DataMember] [DataNames("PAYMENT_TERMS")] public string payment_terms { get; set; }
        [DataMember] [DataNames("CONTACT_DETAILS")] public string contact_details { get; set; }
        [DataMember] [DataNames("URGENCY")] public string urgency { get; set; }
        [DataMember] [DataNames("CURRENCY")] public string currency { get; set; }
        [DataMember] [DataNames("FREEZE_TIME")] public DateTime freeze_time { get; set; }
        [DataMember] [DataNames("EXP_DELIVERY_DATE")] public DateTime exp_delivery_date { get; set; }
        [DataMember] [DataNames("NEGOTIATION_TIME")] public DateTime negotiation_time { get; set; }
        [DataMember] [DataNames("ITEM_LEVEL_ATTACHMENTS")] public string item_level_attachments { get; set; }
        [DataMember] [DataNames("REQUIRED_QUANTITY")] public string required_quantity { get; set; }

    }


}