﻿using PRM.Core.Common;
using PRM.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class OracleTDSCreditNoteDetails
    {
        [DataMember] [DataNames("ENTITY")] public string entity { get; set; }
        [DataMember] [DataNames("CR_NO")] public string cr_no { get; set; }
        [DataMember] [DataNames("INVOICE_TYPE")] public string invoice_type { get; set; }
        [DataMember] [DataNames("INVOICE_DATE")] public string invoice_date { get; set; }
        [DataMember] [DataNames("VENDOR_NAME")] public string vendor_name { get; set; }
        [DataMember] [DataNames("VENDOR_CODE")] public string vendor_code { get; set; }
        [DataMember] [DataNames("VENDOR_SITE_CODE")] public string vendor_site_code { get; set; }
        [DataMember] [DataNames("INVOICE_AMOUNT")] public decimal invoice_amount { get; set; }
        [DataMember] [DataNames("INVOICE_CURRENCY")] public string invoice_currency { get; set; }
        [DataMember] [DataNames("PAYMENT_TERMS")] public string payment_terms { get; set; }
        [DataMember] [DataNames("DESCRIPTION")] public string description { get; set; }
        [DataMember] [DataNames("PAYMENT_METHOD_LOOKUP_CODE")] public string payment_method_lookup_code { get; set; }
        [DataMember] [DataNames("INVOICE_NO")] public string invoice_no { get; set; }
        [DataMember] [DataNames("LINE_ITEM_NO")] public string line_item_no { get; set; }
        [DataMember] [DataNames("LINE_ITEM_AMOUNT")] public decimal line_item_amount { get; set; }
        [DataMember] [DataNames("PAY_GROUP")] public string pay_group { get; set; }
        [DataMember] [DataNames("TDS_CODE")] public string tds_code { get; set; }
        [DataMember] [DataNames("ERP_INVOICE_ID")] public string erp_invoice_id { get; set; }
        [DataMember] [DataNames("RECORD_ID")] public int record_id { get; set; }
        [DataMember] [DataNames("REQUEST_ID")] public int request_id { get; set; }
    }


}