﻿using PRM.Core.Common;
using PRM.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class OracleHSNDetails
    {
        [DataMember] [DataNames("HSN_SAC_CODE")] public string hsn_sac_code { get; set; }
        [DataMember] [DataNames("DESCRIPTION")] public string description { get; set; }
        [DataMember] [DataNames("MEANING")] public string meaning { get; set; }
        
    }


}