﻿prmApp
    .controller('oracleConfigCtrl', ["$rootScope", "$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMPOService", "poService",
        "PRMCustomFieldService", "fileReader", "$uibModal", "$filter", "$http", "poDomain","PRMJeevesOracleService","$element",
        function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPOService, poService, PRMCustomFieldService, fileReader, $uibModal, $filter, $http, poDomain, PRMUploadServices, PRMJeevesOracleService, $element) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.CompId = userService.getUserCompanyId();

            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;


            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };
            /*PAGINATION CODE*/
            $scope.moduleName = $stateParams.moduleName;
            $scope.typeName = '';

            $scope.configName = function (value) {
                $scope.typeName = value;
                $scope.GetOracleConfiguration();
            }

            $scope.GetOracleConfiguration = function () {

                $scope.tableColumns = [];
                $scope.tableValues = [];
                $scope.filteredArray = [];
                var params = {
                    "companyId": +$scope.CompId,
                    "type": $scope.typeName ? $scope.typeName : 'TDS',
                    "sessionid": userService.getUserToken()
                };
                auctionsService.getOracleConfiguration(params)
                    .then(function (response) {
                        if (response) {
                            var arr = JSON.parse(response).Table;
                            console.log($scope.filteredArray);
                            if (arr && arr.length > 0) {
                                $scope.tableColumnsTemp = angular.copy(_.keys(arr[0]));
                                $scope.tableColumnsTemp.forEach(function (item, index) {
                                    item = item.replaceAll("_", " ");
                                    $scope.tableColumns.push(item);
                                });
                                $scope.rows = arr;
                                arr.forEach(function (item, index) {
                                    var obj = angular.copy(_.values(item));
                                    if (obj) {
                                        item.tableValues = [];
                                        obj.forEach(function (value, valueIndex) {
                                            item.tableValues.push(value);
                                        });
                                    }
                                });
                                $scope.totalItems = $scope.rows.length;


                            }
                        }
                    });
            };
            $scope.GetOracleConfiguration();




            $scope.getDate = function (value) {
                return userService.toLocalDate(value);
            }

        }]);