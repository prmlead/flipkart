﻿prmApp
    .controller('errorLogsCtrl', ["$rootScope", "$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMPOService", "poService",
        "PRMCustomFieldService", "fileReader", "$uibModal", "$filter", "$http", "poDomain", "PRMJeevesOracleService", "$element",
        function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPOService, poService, PRMCustomFieldService, fileReader, $uibModal, $filter, $http, poDomain, PRMUploadServices, PRMJeevesOracleService, $element) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.CompId = userService.getUserCompanyId();

            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
                $scope.GetErrorLogs(($scope.currentPage - 1), 10, $scope.filters.searchKeyword);
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };
            /*PAGINATION CODE*/
            $scope.filtersList = {
                system: [],
                api: []

            };
            $scope.filters = {
                system: '',
                api: ''
            };

            $scope.filterDate = {
                toDate: '',
                fromDate: ''
            };

            $scope.filterDate.toDate = moment().format('YYYY-MM-DD');
            $scope.filterDate.fromDate = moment().subtract(30, "days").format("YYYY-MM-DD");
            $scope.filterByDate = function () {
                $scope.GetErrorLogs(($scope.currentPage - 1), 10);
                $scope.getErrorListByFilter($scope.type);
            };

            $scope.setFilters = function (currentPage) {
                if ($scope.loadServices) {
                    $scope.GetErrorLogs(($scope.currentPage - 1), 10);
                }
            };
            $scope.GetErrorLogs = function (recordsFetchFrom, pageSize) {

                var system, api= '';

                if (_.isEmpty($scope.filters.system)) {
                    system = '';
                } else if ($scope.filters.system && $scope.filters.system.length > 0) {
                    var systems = _($scope.filters.system)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    system = systems.join(',');
                }

                if (_.isEmpty($scope.filters.api)) {
                    api = '';
                } else if ($scope.filters.api && $scope.filters.api.length > 0) {
                    var apis = _($scope.filters.api)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    api = apis.join(',');
                }
                $scope.tableColumns = [];
                $scope.tableValues = [];
                $scope.filteredArray = [];
                var params = {
                    "USER_ID": $scope.userID,
                    "SYSTEM": system,
                    "API": api,
                    "SEARCH": $scope.filters.searchWord ? $scope.filters.searchWord : '',
                    "FROM_DATE": $scope.filterDate.fromDate,
                    "TO_DATE": $scope.filterDate.toDate,
                    "PAGE_NUM": recordsFetchFrom * pageSize,
                    "PAGE_SIZE": pageSize,
                    "TYPE":$scope.type,
                    "sessionid": userService.getUserToken()
                };
                auctionsService.getErrorLogs(params)
                    .then(function (response) {
                        $scope.loadServices = true;
                        $scope.errorList = [];

                        if (response && response.length > 0) {
                            $scope.errorList = response;
                            $scope.errorList.forEach(function (item, index) {
                                item.DATE_CREATED = new moment(item.DATE_CREATED).format("DD-MM-YYYY");
                            });
                            $scope.totalItems = ($scope.errorList && $scope.errorList.length > 0 ? $scope.errorList[0].TOTAL_COUNT : 0);
                        }
                    });
            };


            $scope.loadServices = false;
            $scope.getErrorListByFilter = function (type) {
                let systemTemp = [];
                let apiTemp = [];
                var params = {
                    "USER_ID": $scope.userID,
                    "FROM_DATE": $scope.filterDate.fromDate,
                    "TO_DATE": $scope.filterDate.toDate,
                    "TYPE":type,
                    "sessionid": userService.getUserToken()
                };

                auctionsService.getErrorListByFilter(params)
                    .then(function (response) {
                        $scope.loadServices = false;
                        $scope.type = type;
                        $scope.GetErrorLogs(($scope.currentPage - 1), 10);
                        $scope.errorFilteredTemp = response;
                        $scope.errorFilteredTemp.forEach(function (item, index) {
                            if (item.TYPE === 'SYSTEM') {
                                systemTemp.push({ name: item.VALUE });
                            } else if (item.TYPE === 'API') {
                                apiTemp.push({ name: item.VALUE });
                            } 
                        });
                        $scope.filtersList.system = systemTemp;
                        $scope.filtersList.api = apiTemp;
                    });

            };
            $scope.getErrorListByFilter('Error');
  





            $scope.getDate = function (value) {
                return userService.toLocalDate(value);
            }

        }]);