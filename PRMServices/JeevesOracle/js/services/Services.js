
prmApp.constant('PRMJeevesOracleServicesDomain', 'JeevesOracle/svc/PRMJeevesOracleService.svc/REST/');
prmApp.service('PRMJeevesOracleService', ["PRMJeevesOracleServicesDomain", "userService", "httpServices", "$window",
    function (PRMJeevesOracleServicesDomain, userService, httpServices, $window) {

        var PRMJeevesOracleService = this;



        PRMJeevesOracleService.jeevesRequirementSave = function () {
            var url = PRMJeevesOracleServicesDomain + 'jeevesrequirementsave';
            return httpServices.get(url);
        };

        PRMJeevesOracleService.jeevesGRNSave = function () {
            var url = PRMJeevesOracleServicesDomain + 'jeevesgrnsave';
            return httpServices.get(url);
        };

        PRMJeevesOracleService.getPOJeevesDetails = function () {
            var url = PRMJeevesOracleServicesDomain + 'getpojeevesdetails';
            return httpServices.get(url);
        };

        PRMJeevesOracleService.oracleVendorSave = function () {
            var url = PRMJeevesOracleServicesDomain + 'oracleVendorsave'; 
            return httpServices.get(url);
        };
        PRMJeevesOracleService.CreatePOF1Details = function () {
            var url = PRMJeevesOracleServicesDomain + 'createF1Details';
            return httpServices.get(url);
        };
        PRMJeevesOracleService.updateF1Details = function () {
            var url = PRMJeevesOracleServicesDomain + 'updatePoF1details';
            return httpServices.get(url);
        };
        PRMJeevesOracleService.createF1InvoiceDetails = function () {
            var url = PRMJeevesOracleServicesDomain + 'createInvoiceDetails';
            return httpServices.get(url);
        };

       
        PRMJeevesOracleService.GetInvoiceOracleDetails = function (params) {
            let url = PRMJeevesOracleServicesDomain + 'getinvoiceOracleDetails';
            return httpServices.get(url, params);
        };



        return PRMJeevesOracleService;

    }]);