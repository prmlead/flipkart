﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Core.Domain.RealTimePrices
{
    public class RealTimeSelectedProduct
    {
        public string Product { get; set; }
        public decimal ThresholdValue { get; set; }


    }
}
