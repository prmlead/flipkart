﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRM.Core.Models
{
    [DataContract]
    public class OtherDetails : Entity
    {
        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "reqID")]
        public int ReqID { get; set; }

        [DataMember(Name = "sessionID")]
        public string SessionID { get; set; }

        [DataMember(Name = "price")]
        public double Price { get; set; }

        [DataMember(Name = "tax")]
        public double Tax { get; set; }

        [DataMember(Name = "freightcharges")]
        public double Freightcharges { get; set; }

        [DataMember(Name = "vendorBidPrice")]
        public double VendorBidPrice { get; set; }        
    }
}