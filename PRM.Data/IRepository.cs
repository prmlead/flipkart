﻿using PRM.Core;
using System.Collections.Generic;

namespace PRM.Data
{
    public interface IRepository<T> where T : class
    {
        int Insert(T entity);

        void InsertMany(IList<T> lstEntity);

        void Update(T entity);

        void Delete(T entity);

        IEnumerable<T> GetAll();

        IEnumerable<T> GetList(object filter);

        T GetById(int Id);





    }
}