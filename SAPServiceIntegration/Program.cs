﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Reflection;

using PO = TestSol.WebReference;
using PR = TestSol.PRService;
using MAT = TestSol.MaterialService;
using VD = TestSol.VendorService;
using GRN = TestSol.GRNService;
using POSCH = TestSol.POScheduleService;

using PRMServices.SQLHelper;
using System.Text.RegularExpressions;

namespace TestSol
{
    class Program
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private static bool writeToDB = true;
        static void Main(string[] args)
        {
            string jobType = ConfigurationManager.AppSettings["JOB_TYPE"].ToString();
            logger.Info("JOB_TYPE:" + jobType);

            if (jobType.Equals("ALL", StringComparison.InvariantCultureIgnoreCase) ||
               jobType.Contains("PO_JOB"))
            {
                GetPODetails();
            }

            if (jobType.Equals("ALL", StringComparison.InvariantCultureIgnoreCase) ||
               jobType.Contains("MAT_JOB"))
            {
                GetMaterailDetails();
            }

            if (jobType.Equals("ALL", StringComparison.InvariantCultureIgnoreCase) ||
                jobType.Contains("VENDOR_JOB"))
            {
                GetVendorDetails();
            }

            if (jobType.Equals("ALL", StringComparison.InvariantCultureIgnoreCase) ||
                jobType.Contains("PR_JOB"))
            {
                GetPRDetails();
            }

            if (jobType.Equals("ALL", StringComparison.InvariantCultureIgnoreCase) ||
                jobType.Contains("GRN_JOB"))
            {
                GetGRNDetails();
            }

            if (jobType.Equals("ALL", StringComparison.InvariantCultureIgnoreCase) ||
                jobType.Contains("POSCH_JOB"))
            {
                GetPOScheduleDetails();
            }
        }

        private static void GetPOScheduleDetails()
        {
            try
            {
                string getPOSCHParams = ConfigurationManager.AppSettings["POSCHParams"];
                logger.Info("getPOSCHParams:" + getPOSCHParams);
                string dates = getPOSCHParams.Split(';')[0];
                string fromDate = dates.Split(':')[0];
                string toDate = dates.Split(':')[1];
                logger.Info("fromDate:" + fromDate);
                logger.Info("toDate:" + toDate);
                string plants = getPOSCHParams.Split(';')[1];
                string fromPlant = "";
                string toPlant = "";
                if (plants.Equals("ALL", StringComparison.InvariantCultureIgnoreCase))
                {
                    plants = "ALL";
                }
                else if (plants.Contains(":"))
                {
                    fromPlant = plants.Split(':')[0];
                    toPlant = plants.Split(':')[1];
                }

                logger.Debug("START GetPOScheduleDetails: " + DateTime.Now.ToString());
                string newURL = ConfigurationManager.AppSettings["Host"].ToString() + "/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_PRM360&receiverParty=&receiverService=&interface=SI_POScheduleDetails_Sync_Out&interfaceNamespace=http%3A%2F%2Fbiocon.com%2FPRM360%2FSAP%2FPOScheduleDetails";
                NetworkCredential netCredential = new NetworkCredential(ConfigurationManager.AppSettings["UserId"].ToString(), ConfigurationManager.AppSettings["UserPwd"].ToString());
                Uri uri = new Uri(newURL);
                ICredentials credentials = netCredential.GetCredential(uri, "Basic");
                POSCH.SI_POScheduleDetails_Sync_OutService test = new POSCH.SI_POScheduleDetails_Sync_OutService();
                test.Url = newURL;
                test.Credentials = credentials;
                test.PreAuthenticate = true;
                test.Timeout = Convert.ToInt32(ConfigurationManager.AppSettings["TimeOut"]);

                List<POSCH.ZSASNUM> S_ASNUM = new List<POSCH.ZSASNUM>();
                S_ASNUM.Add(new POSCH.ZSASNUM()
                {
                    SIGN = "",
                    OPTION = "",
                    LOW = "",
                    HIGH = ""
                });

                List<POSCH.ZSBUKRS> S_BUKRS = new List<POSCH.ZSBUKRS>();
                S_BUKRS.Add(new POSCH.ZSBUKRS()
                {
                    SIGN = "",
                    OPTION = "",
                    LOW = "",
                    HIGH = ""
                });

                List<POSCH.ZSEBELN> S_EBELN = new List<POSCH.ZSEBELN>();
                S_EBELN.Add(new POSCH.ZSEBELN()
                {
                    SIGN = "",
                    OPTION = "",
                    LOW = "",
                    HIGH = ""
                });

                List<POSCH.ZSBADAT> S_ERDAT = new List<POSCH.ZSBADAT>();
                if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["IND_FLAG"].ToString()))
                {
                    S_ERDAT.Add(new POSCH.ZSBADAT()
                    {
                        SIGN = "I",
                        OPTION = "BT",
                        LOW = fromDate,
                        HIGH = toDate
                    });
                }                    

                List<POSCH.ZSMATNR> S_MATNR = new List<POSCH.ZSMATNR>();
                S_MATNR.Add(new POSCH.ZSMATNR()
                {
                    SIGN = "",
                    OPTION = "",
                    LOW = "",
                    HIGH = ""
                });


                List<POSCH.ZSMTART> S_MTART = new List<POSCH.ZSMTART>();
                S_MTART.Add(new POSCH.ZSMTART()
                {
                    SIGN = "I",
                    OPTION = "EQ",
                    LOW = "ZSTR",
                    HIGH = ""
                });
                S_MTART.Add(new POSCH.ZSMTART()
                {
                    SIGN = "I",
                    OPTION = "EQ",
                    LOW = "ZCAP",
                    HIGH = ""
                });
                S_MTART.Add(new POSCH.ZSMTART()
                {
                    SIGN = "I",
                    OPTION = "EQ",
                    LOW = "ZVRP",
                    HIGH = ""
                });
                S_MTART.Add(new POSCH.ZSMTART()
                {
                    SIGN = "I",
                    OPTION = "EQ",
                    LOW = "ZSC2",
                    HIGH = ""
                });
                S_MTART.Add(new POSCH.ZSMTART()
                {
                    SIGN = "I",
                    OPTION = "EQ",
                    LOW = "ZSC1",
                    HIGH = ""
                });
                S_MTART.Add(new POSCH.ZSMTART()
                {
                    SIGN = "I",
                    OPTION = "EQ",
                    LOW = "ZRDM",
                    HIGH = ""
                });

                List<POSCH.ZSWERKS> S_WERKS = new List<POSCH.ZSWERKS>();
                logger.Info("plants:" + plants);
                if (plants.Equals("ALL", StringComparison.InvariantCultureIgnoreCase))
                {
                    S_WERKS.Add(new POSCH.ZSWERKS()
                    {
                        SIGN = "I",
                        OPTION = "BT",
                        LOW = "5100",
                        HIGH = "5199"
                    });
                }
                else if (!string.IsNullOrEmpty(fromPlant) && !string.IsNullOrEmpty(toPlant))
                {
                    S_WERKS.Add(new POSCH.ZSWERKS()
                    {
                        SIGN = "I",
                        OPTION = "BT",
                        LOW = fromPlant,
                        HIGH = toPlant
                    });
                }
                else if (plants.Contains(","))
                {
                    foreach (var plant in plants.Split(','))
                    {
                        S_WERKS.Add(new POSCH.ZSWERKS()
                        {
                            SIGN = "I",
                            OPTION = "EQ",
                            LOW = plant,
                            HIGH = ""
                        });
                    }

                }
                else
                {
                    S_WERKS.Add(new POSCH.ZSWERKS()
                    {
                        SIGN = "I",
                        OPTION = "EQ",
                        LOW = plants,
                        HIGH = ""
                    });
                }

                var poSch = test.SI_POScheduleDetails_Sync_Out(ConfigurationManager.AppSettings["IND_FLAG"].ToString(), S_ASNUM.ToArray(), S_BUKRS.ToArray(), S_EBELN.ToArray(), S_ERDAT.ToArray(), S_MATNR.ToArray(), S_MTART.ToArray(), S_WERKS.ToArray());
                logger.Debug($"TOTAL COUNT GetPOScheduleDetails: {(poSch != null ? poSch.Count().ToString() : "0")}");
                logger.Debug($"END GetPOScheduleDetails: { DateTime.Now.ToString()}");
                List<string> columns = @"WERKS;MATNR;TXZ01;MATKL;LIFNR;NAME1;TELF1;SMTP_ADDR;EBELN;EBELP;AEDAT;ERNAM;ZTERM;MWSKZ;TEXT1;UDATE;EINDT;MTART;ORT01;REGIO;J_1BNBM;ORD_QTY;MEINS;MEINS1;MENGE1;MENGE1Specified;EFFWR;EFFWRSpecified;NETWR1;NETWR1Specified;WAERS;KNUMV;KNUMVSpecified;MENGE2;MENGE2Specified;BANFN;BNFPO;MENGE3;MENGE3Specified;UDATE1;LTEXT1;LFDAT;LTEXT2;FRGKZ;AFNAM;BSART;SRVPOS;ASKTX;KNUMV1;KNUMV1Specified;KNUMV2;KNUMV2Specified;BSTYP;KDATB;KDATE;AEDAT1;ELIKZ;LOEKZ".Split(';').ToList();

                if (poSch != null && poSch.Length > 0 && writeToDB)
                {
                    DataTable dt = new DataTable();
                    dt.Clear();
                    dt.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                    foreach (var column in columns)
                    {
                        dt.Columns.Add(column);
                    }

                    dt.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                    }

                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    int count = 1;
                    var totalChunks = poSch.AsEnumerable().ToList().Chunk(500);
                    foreach (var chunk in totalChunks)
                    {
                        logger.Debug($"Reading :  Chunk: {count}");
                        dt.Rows.Clear();                        
                        string query = @"INSERT INTO [dbo].[SAP_PO_SCHEDULE_DETAILS] VALUES ";
                        foreach (var sch in chunk)
                        {
                            var row = dt.NewRow();
                            row["JOB_ID"] = guid1;
                            query += $@"('{guid}',";
                            foreach (var column in columns)
                            {
                                var temp = sch.GetPropValue<object>(column);
                                if (column.ToUpper().Contains("SPECIFIED"))
                                {
                                    temp = Convert.ToBoolean(temp) ? 1 : 0;
                                }
                                var temp1 = string.Empty;
                                try
                                {
                                    if (temp != null)
                                    {
                                        temp1 = temp.ToString().Replace("'", "''");
                                    }
                                }
                                catch
                                {
                                    logger.Error("PO Schedule Row Failed Value is>>>", temp1);
                                }

                                row[column] = temp1;
                                query += $"'{temp1}',";
                            }

                            row["DATE_CREATED"] = DateTime.UtcNow;
                            dt.Rows.Add(row);
                            //query = query.TrimEnd(',');
                            query += $@"GETUTCDATE()),";
                        }

                        query = query.TrimEnd(',');
                        query += ";";
                        logger.Debug("END GetPOScheduleDetails QUERY: " + query);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            bizClass.BulkInsert(dt, "[dbo].[SAP_PO_SCHEDULE_DETAILS]", columnMappings);
                        }

                        dt.Rows.Clear();
                        //bizClass.ExecuteNonQuery_IUD(query);
                        count++;
                    }

                    if (poSch != null && poSch.Length > 0)
                    {
                        SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                        sd.Add("P_JOB_ID", guid);
                        bizClass.SelectList("erp_process_sap_po_schedule_details", sd);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace, "GETPOSCHEDULE ERROR");
            }
        }

        private static void GetGRNDetails()
        {
            try
            {
                string getGRNParams = ConfigurationManager.AppSettings["GRNParams"];
                logger.Info("getGRNParams:" + getGRNParams);
                string dates = getGRNParams.Split(';')[0];
                string fromDate = dates.Split(':')[0];
                string toDate = dates.Split(':')[1];
                logger.Info("fromDate:" + fromDate);
                logger.Info("toDate:" + toDate);
                string plants = getGRNParams.Split(';')[1];
                string fromPlant = "";
                string toPlant = "";
                if (plants.Equals("ALL", StringComparison.InvariantCultureIgnoreCase))
                {
                    plants = "ALL";
                }
                else if (plants.Contains(":"))
                {
                    fromPlant = plants.Split(':')[0];
                    toPlant = plants.Split(':')[1];
                }
                logger.Debug("START GetGRNDetails: " + DateTime.Now.ToString());
                string newURL = ConfigurationManager.AppSettings["Host"].ToString() + "/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_PRM360&receiverParty=&receiverService=&interface=SI_GRNDetails_Sync_Out&interfaceNamespace=http%3A%2F%2Fbiocon.com%2FPRM360%2FSAP%2FGRNDetails";
                NetworkCredential netCredential = new NetworkCredential(ConfigurationManager.AppSettings["UserId"].ToString(), ConfigurationManager.AppSettings["UserPwd"].ToString());
                Uri uri = new Uri(newURL);
                ICredentials credentials = netCredential.GetCredential(uri, "Basic");
                GRN.SI_GRNDetails_Sync_OutService test = new GRN.SI_GRNDetails_Sync_OutService();
                test.Url = newURL;
                test.Credentials = credentials;
                test.PreAuthenticate = true;
                test.Timeout = Convert.ToInt32(ConfigurationManager.AppSettings["TimeOut"]);

                List<GRN.ZSBADAT> S_BLDAT = new List<GRN.ZSBADAT>();
                S_BLDAT.Add(new GRN.ZSBADAT()
                {
                    SIGN = "",
                    OPTION = "",
                    LOW = "",
                    HIGH = ""
                });

                List<GRN.ZSBADAT> S_BUDAT = new List<GRN.ZSBADAT>();
                if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["IND_FLAG"].ToString()))
                {
                    S_BUDAT.Add(new GRN.ZSBADAT()
                    {
                        SIGN = "I",
                        OPTION = "BT",
                        LOW = fromDate,
                        HIGH = toDate
                    });
                }                

                List<GRN.ZSEBELN> S_EBELN = new List<GRN.ZSEBELN>();
                S_EBELN.Add(new GRN.ZSEBELN()
                {
                    SIGN = "",
                    OPTION = "",
                    LOW = "",
                    HIGH = ""
                });

                List<GRN.ZSMTART> S_MTART = new List<GRN.ZSMTART>();
                S_MTART.Add(new GRN.ZSMTART()
                {
                    SIGN = "I",
                    OPTION = "EQ",
                    LOW = "ZSTR",
                    HIGH = ""
                });
                S_MTART.Add(new GRN.ZSMTART()
                {
                    SIGN = "I",
                    OPTION = "EQ",
                    LOW = "",
                    HIGH = "ZCAP"
                });
                S_MTART.Add(new GRN.ZSMTART()
                {
                    SIGN = "I",
                    OPTION = "EQ",
                    LOW = "ZVRP",
                    HIGH = ""
                });
                S_MTART.Add(new GRN.ZSMTART()
                {
                    SIGN = "I",
                    OPTION = "EQ",
                    LOW = "ZSC2",
                    HIGH = ""
                });
                S_MTART.Add(new GRN.ZSMTART()
                {
                    SIGN = "I",
                    OPTION = "EQ",
                    LOW = "ZSC1",
                    HIGH = ""
                });
                S_MTART.Add(new GRN.ZSMTART()
                {
                    SIGN = "I",
                    OPTION = "EQ",
                    LOW = "ZRDM",
                    HIGH = ""
                });

                List<GRN.ZSWERKS> S_WERKS = new List<GRN.ZSWERKS>();
                logger.Info("plants:" + plants);
                if (plants.Equals("ALL", StringComparison.InvariantCultureIgnoreCase))
                {
                    S_WERKS.Add(new GRN.ZSWERKS()
                    {
                        SIGN = "I",
                        OPTION = "BT",
                        LOW = "5100",
                        HIGH = "5199"
                    });
                }
                else if (!string.IsNullOrEmpty(fromPlant) && !string.IsNullOrEmpty(toPlant))
                {
                    S_WERKS.Add(new GRN.ZSWERKS()
                    {
                        SIGN = "I",
                        OPTION = "BT",
                        LOW = fromPlant,
                        HIGH = toPlant
                    });
                }
                else if (plants.Contains(","))
                {
                    foreach (var plant in plants.Split(','))
                    {
                        S_WERKS.Add(new GRN.ZSWERKS()
                        {
                            SIGN = "I",
                            OPTION = "EQ",
                            LOW = plant,
                            HIGH = ""
                        });
                    }

                }
                else
                {
                    S_WERKS.Add(new GRN.ZSWERKS()
                    {
                        SIGN = "I",
                        OPTION = "EQ",
                        LOW = plants,
                        HIGH = ""
                    });
                }

                var grns = test.SI_GRNDetails_Sync_Out(ConfigurationManager.AppSettings["IND_FLAG"].ToString(), S_BLDAT.ToArray(), S_BUDAT.ToArray(), S_EBELN.ToArray(), S_MTART.ToArray(), S_WERKS.ToArray());

                logger.Debug("TOTAL COUNT GetGRNDetails: " + grns != null ? grns.Count().ToString() : "0");
                logger.Debug("END GetGRNDetails: " + DateTime.Now.ToString());

                List<string> columns = @"BUDAT;ZZSBILL_ENTRY;ZZBILL_ENTRY;ZZDATE_SBILL_ENTRY;SGTXT;WAERS;BLDAT;VFDAT;WRBTR;WRBTRSpecified;ZZGATE_ENT_DATE;ZZGATE_ENTRY;CHARG;LFBNR;BUDAT1;ZEILE;MBLNR;MJAHR;MENGE;MENGESpecified;WRBTR1;WRBTR1Specified;ZZBILL_DOC;ZZLR_DATE;ZZLR_NUMBER;ZZLUT_NUM;HSDAT;MAKTX;MATNR;BWART;WERKS;MENGE1;MENGE1Specified;RDATE;SRVPOS;LFBNR1;LFPOS;STATUS;LGORT;MEINS;CHARG1;LIFNR;NAME1;LAND1;PS_PSP_PNR;EBELN;EBELP;CPUDT_MKPF;CPUTM_MKPF".Split(';').ToList();

                if (grns != null && grns.Length > 0 && writeToDB)
                {
                    DataTable dt = new DataTable();
                    dt.Clear();
                    dt.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                    foreach (var column in columns)
                    {
                        dt.Columns.Add(column);
                    }

                    dt.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                    }

                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    int count = 1;
                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    var totalChunks = grns.AsEnumerable().ToList().Chunk(500);
                    foreach (var chunk in totalChunks)
                    {
                        dt.Rows.Clear();
                        logger.Debug($"Reading :  Chunk: {count}");
                        string query = @"INSERT INTO [dbo].[SAP_GRN_DETAILS] VALUES ";
                        foreach (var grn in chunk)
                        {
                            var row = dt.NewRow();
                            row["JOB_ID"] = guid1;
                            query += $@"('{guid}',";
                            foreach (var column in columns)
                            {
                                var temp = grn.GetPropValue<object>(column);
                                if (column.ToUpper().Contains("SPECIFIED"))
                                {
                                    temp = Convert.ToBoolean(temp) ? 1 : 0;
                                }
                                var temp1 = string.Empty;
                                try
                                {
                                    if (temp != null)
                                    {
                                        temp1 = temp.ToString().Replace("'", "''");
                                    }
                                }
                                catch
                                {
                                    logger.Error("GRN Row Failed Value is>>>", temp1);
                                }

                                row[column] = temp1;
                                query += $"'{temp1}',";
                            }

                            row["DATE_CREATED"] = DateTime.UtcNow;
                            dt.Rows.Add(row);
                            query += $@"GETUTCDATE()),";
                        }


                        query = query.TrimEnd(',');
                        query += ";";
                        logger.Debug("END GetGRNDetails QUERY: " + query);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            bizClass.BulkInsert(dt, "[dbo].[SAP_GRN_DETAILS]", columnMappings);
                        }

                        dt.Rows.Clear();
                        //bizClass.ExecuteNonQuery_IUD(query);
                        count++;
                    }

                    if (grns != null && grns.Length > 0)
                    {
                        SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                        sd.Add("P_JOB_ID", guid);
                        bizClass.SelectList("erp_process_sap_grn_details", sd);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace, "GETGRNDETAILS ERROR");
            }
        }

        private static void GetVendorDetails()
        {
            try
            {
                string VendorParams = ConfigurationManager.AppSettings["VendorParams"];
                List<string> smtartList = VendorParams.Split(',').ToList();
                logger.Info("VendorParams:" + VendorParams);
                logger.Debug("START GetVendorDetails: " + DateTime.Now.ToString());
                string newURL = ConfigurationManager.AppSettings["Host"].ToString() + "/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_PRM360&receiverParty=&receiverService=&interface=SI_VendorDetails_Sync_Out&interfaceNamespace=http%3A%2F%2Fbiocon.com%2FPRM360%2FSAP%2FVendorDetails";
                NetworkCredential netCredential = new NetworkCredential(ConfigurationManager.AppSettings["UserId"].ToString(), ConfigurationManager.AppSettings["UserPwd"].ToString());
                Uri uri = new Uri(newURL);
                ICredentials credentials = netCredential.GetCredential(uri, "Basic");
                VD.SI_VendorDetails_Sync_OutService test = new VD.SI_VendorDetails_Sync_OutService();
                test.Url = newURL;
                test.Credentials = credentials;
                test.PreAuthenticate = true;
                test.Timeout = Convert.ToInt32(ConfigurationManager.AppSettings["TimeOut"]);

                List<VD.ZSBUKRS> S_BUKRS = new List<VD.ZSBUKRS>();
                S_BUKRS.Add(new VD.ZSBUKRS()
                {
                    SIGN = "I",
                    OPTION = "EQ",
                    LOW = "5100",
                    HIGH = ""
                });

                List<VD.ZSEKORG> S_EKORG = new List<VD.ZSEKORG>();
                S_EKORG.Add(new VD.ZSEKORG()
                {
                    SIGN = "I",
                    OPTION = "EQ",
                    LOW = "5100",
                    HIGH = ""
                });

                List<VD.ZSKTOKK> S_KTOKK = new List<VD.ZSKTOKK>();
                S_KTOKK.Add(new VD.ZSKTOKK()
                {
                    SIGN = "I",
                    OPTION = "EQ",
                    LOW = "Z014",
                    HIGH = ""
                });

                S_KTOKK.Add(new VD.ZSKTOKK()
                {
                    SIGN = "I",
                    OPTION = "EQ",
                    LOW = "Z002",
                    HIGH = ""
                });

                S_KTOKK.Add(new VD.ZSKTOKK()
                {
                    SIGN = "I",
                    OPTION = "EQ",
                    LOW = "Z001",
                    HIGH = ""
                });

                List<VD.ZSLIFNR> S_LIFNR = new List<VD.ZSLIFNR>();
                S_LIFNR.Add(new VD.ZSLIFNR()
                {
                    SIGN = "",
                    OPTION = "",
                    LOW = "",
                    HIGH = ""
                });

                List<VD.ZSMTART> S_MTART = new List<VD.ZSMTART>();
                foreach (var val in smtartList)
                {
                    S_MTART.Add(new VD.ZSMTART()
                    {
                        SIGN = "I",
                        OPTION = "EQ",
                        LOW = val,
                        HIGH = ""
                    });
                }

                var vendors = test.SI_VendorDetails_Sync_Out(ConfigurationManager.AppSettings["IND_FLAG"], S_BUKRS.ToArray(), S_EKORG.ToArray(), S_KTOKK.ToArray(), S_LIFNR.ToArray(), S_MTART.ToArray());
                logger.Debug("TOTAL COUNT GetVendorDetails: " + (vendors != null ? vendors.Count().ToString() : "0"));
                logger.Debug("END GetVendorDetails: " + DateTime.Now.ToString());
                List<string> columns = @"NAMEV;NAME1;SMTP_ADDR;TELF1;NAME2;WAERS;KTOKK;TXT30;LIFNR;MATNR;WERKS;STCD3;VEN_ALT_FIR;VEN_ALT_LAS;SMTP_ADDR1;SMTP_ADDR2;SMTP_ADDR3;SMTP_ADDR4;SMTP_ADDR5;SMTP_ADDR6;SMTP_ADDR7;SMTP_ADDR8;SMTP_ADDR9;SMTP_ADDR10;TEL_NUMBER;CITY1;LAND1;ERDAT;ZTERM;TEXT1;INCO1;J_1IGTAXKD;SPERR;SPERM;LOEVM;SPERR1;LOEVM1;ERDAT2;SPERM1;LOEVM2;LVORM3;UTIME;STR_SUPPL1;STR_SUPPL2;HOUSE_NUM1;STREET;STR_SUPPL3;LOCATION;PO_BOX;CITY2;POST_CODE1;POST_CODE2;REGION;BEZEI;SORTL".Split(';').ToList();

                if (vendors != null && vendors.Length > 0 && writeToDB)
                {
                    DataTable dt = new DataTable();
                    dt.Clear();
                    dt.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                    foreach (var column in columns)
                    {
                        dt.Columns.Add(column);
                    }

                    dt.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                    }

                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    int count = 1;
                    var totalChunks = vendors.AsEnumerable().ToList().Chunk(500);
                    logger.Debug("GetVendorDetails GUID: " + guid);
                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    foreach (var chunk in totalChunks)
                    {
                        dt.Rows.Clear();
                        logger.Debug($"Reading :  Chunk: {count}");
                        string query = @"INSERT INTO [dbo].[SAP_VENDOR_DETAILS] VALUES ";
                        foreach (var vendor in chunk)
                        {
                            var row = dt.NewRow();
                            row["JOB_ID"] = guid1;

                            query += $@"('{guid}',";
                            foreach (var column in columns)
                            {

                                var temp = vendor.GetPropValue<object>(column);
                                var temp1 = string.Empty;
                                try
                                {
                                    if (temp != null)
                                    {
                                        temp1 = temp.ToString().Replace("'", "''");
                                    }
                                }
                                catch
                                {
                                    logger.Error("Vendor Row Failed For Value is>>>", temp1);
                                }

                                row[column] = temp1;
                                query += $"'{temp1}',";
                            }

                            row["DATE_CREATED"] = DateTime.UtcNow;
                            dt.Rows.Add(row);
                            query += $@"GETUTCDATE()),";
                        }

                        query = query.TrimEnd(',');
                        query += ";";
                        logger.Debug("END GetVendorDetails QUERY: " + query);

                        if (dt != null && dt.Rows.Count > 0)
                        {
                            bizClass.BulkInsert(dt, "[dbo].[SAP_VENDOR_DETAILS]", columnMappings);
                        }

                        dt.Rows.Clear();
                        //bizClass.ExecuteNonQuery_IUD(query);
                        count++;
                    }

                    if (vendors != null && vendors.Length > 0)
                    {
                        SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                        sd.Add("P_JOB_ID", guid);
                        bizClass.SelectList("erp_process_sap_vendor_details", sd);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace, "GETVENDORDETAILS ERROR");
            }
        }

        private static void GetMaterailDetails()
        {
            try
            {

                string getMATParams = ConfigurationManager.AppSettings["MATParams"];
                logger.Info("getMATParams:" + getMATParams);
                string materials = getMATParams.Split(';')[0];
                List<string> materialLis = materials.Split(',').ToList();
                string plants = getMATParams.Split(';')[1];
                string fromPlant = "";
                string toPlant = "";
                if (plants.Equals("ALL", StringComparison.InvariantCultureIgnoreCase))
                {
                    plants = "ALL";
                }
                else if (plants.Contains(":"))
                {
                    fromPlant = plants.Split(':')[0];
                    toPlant = plants.Split(':')[1];
                }

                logger.Debug("START GetMaterailDetails: " + DateTime.Now.ToString());
                string newURL = ConfigurationManager.AppSettings["Host"].ToString() + "/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_PRM360&receiverParty=&receiverService=&interface=SI_MaterialMaster_Sync_Out&interfaceNamespace=http%3A%2F%2Fbiocon.com%2FPRM360%2FSAP%2FMaterialMaster";
                string materialParametres = ConfigurationManager.AppSettings["MATERIAL_PARAMTRES"].ToString();
                NetworkCredential netCredential = new NetworkCredential(ConfigurationManager.AppSettings["UserId"].ToString(), ConfigurationManager.AppSettings["UserPwd"].ToString());
                Uri uri = new Uri(newURL);
                ICredentials credentials = netCredential.GetCredential(uri, "Basic");
                MAT.SI_MaterialMaster_Sync_OutService test = new MAT.SI_MaterialMaster_Sync_OutService();
                test.Url = newURL;
                test.Credentials = credentials;
                test.PreAuthenticate = true;
                test.Timeout = Convert.ToInt32(ConfigurationManager.AppSettings["TimeOut"]);

                List<MAT.ZSASNUM> S_ASNUM = new List<MAT.ZSASNUM>();
                S_ASNUM.Add(new MAT.ZSASNUM()
                {
                    SIGN = "",
                    OPTION = "",
                    LOW = "",
                    HIGH = ""
                });

                List<MAT.ZSASTYP> S_ASTYP = new List<MAT.ZSASTYP>();
                S_ASTYP.Add(new MAT.ZSASTYP()
                {
                    SIGN = "",
                    OPTION = "",
                    LOW = "",
                    HIGH = ""
                });

                List<MAT.ZSMATNR> S_MATNR = new List<MAT.ZSMATNR>();
                S_MATNR.Add(new MAT.ZSMATNR()
                {
                    SIGN = "",
                    OPTION = "",
                    LOW = "",
                    HIGH = ""
                });



                List<MAT.ZSMTART> S_MTART = new List<MAT.ZSMTART>();
                foreach (var mat in materialLis)
                {
                    S_MTART.Add(new MAT.ZSMTART()
                    {
                        SIGN = "I",
                        OPTION = "EQ",
                        LOW = mat,
                        HIGH = ""
                    });
                }



                List<MAT.ZSWERKS> S_WERKS = new List<MAT.ZSWERKS>();
                logger.Info("plants:" + plants);
                if (plants.Equals("ALL", StringComparison.InvariantCultureIgnoreCase))
                {
                    S_WERKS.Add(new MAT.ZSWERKS()
                    {
                        SIGN = "I",
                        OPTION = "BT",
                        LOW = "5100",
                        HIGH = "5199"
                    });
                }
                else if (!string.IsNullOrEmpty(fromPlant) && !string.IsNullOrEmpty(toPlant))
                {
                    S_WERKS.Add(new MAT.ZSWERKS()
                    {
                        SIGN = "I",
                        OPTION = "BT",
                        LOW = fromPlant,
                        HIGH = toPlant
                    });
                }
                else if (plants.Contains(","))
                {
                    foreach (var plant in plants.Split(','))
                    {
                        S_WERKS.Add(new MAT.ZSWERKS()
                        {
                            SIGN = "I",
                            OPTION = "EQ",
                            LOW = plant,
                            HIGH = ""
                        });
                    }

                }
                else
                {
                    S_WERKS.Add(new MAT.ZSWERKS()
                    {
                        SIGN = "I",
                        OPTION = "EQ",
                        LOW = plants,
                        HIGH = ""
                    });
                }

                var materails = test.SI_MaterialMaster_Sync_Out(ConfigurationManager.AppSettings["IND_FLAG"], S_ASNUM.ToArray(), S_ASTYP.ToArray(), S_MATNR.ToArray(), S_MTART.ToArray(), S_WERKS.ToArray());
                logger.Debug($"TOTAL COUNT GetMaterailDetails: {(materails != null ? materails.Count().ToString() : "0")}");
                logger.Debug("END GetMaterailDetails: " + DateTime.Now.ToString());
                List<string> columns = @"MATKL;WGBEZ;MATNR;MTART;STEUC;MAKTX;PO_TEXT;MEINS;CASNR;GPNUM;ASNUM;ASKTX;MEINS1;MATKL1;MEINH;MEINH1;MEINH2;MEINH3;MEINH4;MEINH5;MEINH6;MEINH7;UMREN;UMRENSpecified;UMREN1;UMREN1Specified;UMREN2;UMREN2Specified;UMREN3;UMREN3Specified;UMREN4;UMREN4Specified;UMREN5;UMREN5Specified;UMREN6;UMREN6Specified;UMREN7;UMREN7Specified;UMREZ;UMREZSpecified;UMREZ1;UMREZ1Specified;UMREZ2;UMREZ2Specified;UMREZ3;UMREZ3Specified;UMREZ4;UMREZ4Specified;UMREZ5;UMREZ5Specified;UMREZ6;UMREZ6Specified;UMREZ7;UMREZ7Specified;TAXIM;LVORM;MMSTA;MSTAE;ERSDA;LVORM1;ERDAT1;AEDAT1;LAEDA;WERKS".Split(';').ToList();

                if (materails != null && materails.Length > 0 && writeToDB)
                {
                    DataTable dt = new DataTable();
                    dt.Clear();
                    dt.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                    foreach (var column in columns)
                    {
                        dt.Columns.Add(column);
                    }

                    dt.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                    }

                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    int count = 1;
                    var totalChunks = materails.AsEnumerable().ToList().Chunk(500);
                    logger.Debug("GetMaterailDetails GUID: " + guid);
                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    foreach (var chunk in totalChunks)
                    {
                        dt.Rows.Clear();
                        logger.Debug($"Reading :  Chunk: {count}");
                        string query = @"INSERT INTO [dbo].[SAP_MATERIAL_DETAILS] VALUES ";
                        foreach (var material in chunk)
                        {
                            var row = dt.NewRow();
                            row["JOB_ID"] = guid1;
                            query += $@"('{guid}',";
                            foreach (var column in columns)
                            {
                                var temp = material.GetPropValue<object>(column);
                                var temp1 = string.Empty;
                                try
                                {
                                    if (temp != null)
                                    {
                                        temp1 = temp.ToString().Replace("'", "''");
                                    }
                                }
                                catch
                                {
                                    logger.Error("Material Row Failed Value is>>>", temp1);
                                }

                                row[column] = temp1;
                                query += $"'{temp1}',";
                            }

                            row["DATE_CREATED"] = DateTime.UtcNow;
                            dt.Rows.Add(row);
                            query += $@"GETUTCDATE()),";
                        }


                        query = query.TrimEnd(',');
                        query += ";";
                        logger.Debug("END GetMaterailDetails QUERY: " + query);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            bizClass.BulkInsert(dt, "[dbo].[SAP_MATERIAL_DETAILS]", columnMappings);
                        }
                            
                        dt.Rows.Clear();
                        //bizClass.ExecuteNonQuery_IUD(query);
                        count++;
                    }

                    if (materails != null && materails.Length > 0)
                    {
                        SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                        sd.Add("P_JOB_ID", guid);
                        bizClass.SelectList("erp_process_sap_material_details", sd);
                    }
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace, "GETMATERIALDETAILS ERROR");
            }
        }

        private static void GetPRDetails()
        {
            try
            {
                string getPRParams = ConfigurationManager.AppSettings["PRParams"];
                logger.Info("getPRParams:" + getPRParams);
                string dates = getPRParams.Split(';')[0];
                string fromDate = dates.Split(':')[0];
                string toDate = dates.Split(':')[1];
                logger.Info("fromDate:" + fromDate);
                logger.Info("toDate:" + toDate);
                string plants = getPRParams.Split(';')[1];
                string fromPlant = "";
                string toPlant = "";
                if (plants.Equals("ALL", StringComparison.InvariantCultureIgnoreCase))
                {
                    plants = "ALL";
                }
                else if (plants.Contains(":"))
                {
                    fromPlant = plants.Split(':')[0];
                    toPlant = plants.Split(':')[1];
                }

                logger.Debug("START GetPRDetails: " + DateTime.Now.ToString());
                string newURL = ConfigurationManager.AppSettings["Host"].ToString() + "/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_PRM360&receiverParty=&receiverService=&interface=SI_PurchaseRequisition_Sync_Out&interfaceNamespace=http%3A%2F%2Fbiocon.com%2FPRM360%2FSAP%2FPurchaseRequisition";
                NetworkCredential netCredential = new NetworkCredential(ConfigurationManager.AppSettings["UserId"].ToString(), ConfigurationManager.AppSettings["UserPwd"].ToString());
                Uri uri = new Uri(newURL);
                ICredentials credentials = netCredential.GetCredential(uri, "Basic");
                PR.SI_PurchaseRequisition_Sync_OutService test = new PR.SI_PurchaseRequisition_Sync_OutService();
                test.Url = newURL;
                test.Credentials = credentials;
                test.PreAuthenticate = true;
                test.Timeout = Convert.ToInt32(ConfigurationManager.AppSettings["TimeOut"]);

                List<PR.ZSBADAT> S_BADAT = new List<PR.ZSBADAT>();
                S_BADAT.Add(new PR.ZSBADAT()
                {
                    SIGN = "I",
                    OPTION = "BT",
                    LOW = fromDate,
                    HIGH = toDate
                });

                List<PR.ZSBSART> S_BSART = new List<PR.ZSBSART>();
                S_BSART.Add(new PR.ZSBSART()
                {
                    SIGN = "I",
                    OPTION = "EQ",
                    LOW = "ZSSR",
                    HIGH = ""
                });
                S_BSART.Add(new PR.ZSBSART()
                {
                    SIGN = "I",
                    OPTION = "EQ",
                    LOW = "ZSCP",
                    HIGH = ""
                });
                S_BSART.Add(new PR.ZSBSART()
                {
                    SIGN = "I",
                    OPTION = "EQ",
                    LOW = "ZPME",
                    HIGH = ""
                });
                S_BSART.Add(new PR.ZSBSART()
                {
                    SIGN = "I",
                    OPTION = "EQ",
                    LOW = "ZPJ",
                    HIGH = ""
                });

                List<PR.ZSPRCTR> S_PRCTR = new List<PR.ZSPRCTR>();
                S_PRCTR.Add(new PR.ZSPRCTR()
                {
                    SIGN = "",
                    OPTION = "",
                    LOW = "",
                    HIGH = ""
                });

                List<PR.ZSWERKS> S_WERKS = new List<PR.ZSWERKS>();
                logger.Info("plants:" + plants);
                if (plants.Equals("ALL", StringComparison.InvariantCultureIgnoreCase))
                {
                    S_WERKS.Add(new PR.ZSWERKS()
                    {
                        SIGN = "I",
                        OPTION = "BT",
                        LOW = "5100",
                        HIGH = "5199"
                    });
                }
                else if (!string.IsNullOrEmpty(fromPlant) && !string.IsNullOrEmpty(toPlant))
                {
                    S_WERKS.Add(new PR.ZSWERKS()
                    {
                        SIGN = "I",
                        OPTION = "BT",
                        LOW = fromPlant,
                        HIGH = toPlant
                    });
                }
                else if (plants.Contains(","))
                {
                    foreach (var plant in plants.Split(','))
                    {
                        S_WERKS.Add(new PR.ZSWERKS()
                        {
                            SIGN = "I",
                            OPTION = "EQ",
                            LOW = plant,
                            HIGH = ""
                        });
                    }

                }
                else
                {
                    S_WERKS.Add(new PR.ZSWERKS()
                    {
                        SIGN = "I",
                        OPTION = "EQ",
                        LOW = plants,
                        HIGH = ""
                    });
                }


                var prs = test.SI_PurchaseRequisition_Sync_Out(ConfigurationManager.AppSettings["IND_FLAG"], S_BADAT.ToArray(), S_BSART.ToArray(), S_PRCTR.ToArray(), S_WERKS.ToArray());
                logger.Debug("TOTAL COUNT GetPRDetails: " + (prs != null ? prs.Count().ToString() : "0"));
                logger.Debug("END GetPRDetails: " + DateTime.Now.ToString());
                List<string> columns = @"PLANT;PLANT_CODE;GMP;PURCHASE_GROUP_CODE;PURCHASE_GROUP_NAME;REQUESITION_DATE;PR_RELEASE_DATE;PR_CHANGE_DATE;PR_NUMBER;PR_CREATOR_NAME;REQUISITIONER_NAME;REQUISITIONER_EMAIL;ITEM_OF_REQUESITION;MATERIAL_GROUP_CODE;MATERIAL_GROUP_DESC;MATERIAL_CODE;MATERIAL_TYPE;MATERIAL_HSN_CODE;MATERIAL_DESCRIPTION;SHORT_TEXT;ITEM_TEXT;UOM;QTY_REQUIRED;MATERIAL_DELIVERY_DATE;LEAD_TIME;PR_TYPE;PR_TYPE_DESC;PR_NOTE;CASNR;WBS_CODE;MFCD_NUMBER;PROJECT_DESCRIPTION;PROFIT_CENTER;ALTERNATE_UOM;ALTERNATE_QTY;SECTION_HEAD;SERVICE_CODE;SERVICE_CODE_DESCRIPTION;SERVICE_QTY;SERVICE_UNIT_OF_MEASURE;ITEM_CATEGORY;ACCOUNT_ASSIGNMENT_CATEGORY;PROJECT_TYPE;SUB_VERTICAL;LOEKZ;ERDAT;EBAKZ".Split(';').ToList();

                if (prs != null && prs.Length > 0 && writeToDB)
                {
                    DataTable dt = new DataTable();
                    dt.Clear();
                    dt.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                    foreach (var column in columns)
                    {
                        dt.Columns.Add(column);
                    }

                    dt.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                    }

                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    Type myType = typeof(MAT.ZSMATFINAL);
                    int count = 1;
                    var totalChunks = prs.AsEnumerable().ToList().Chunk(500);
                    foreach (var chunk in totalChunks)
                    {
                        dt.Rows.Clear();
                        logger.Debug($"Reading :  Chunk: {count}");
                        string query = @"INSERT INTO [dbo].[SAP_PR_DETAILS] VALUES ";
                        int processedRows = 0;
                        foreach (var pr in chunk)
                        {
                            var row = dt.NewRow();
                            row["JOB_ID"] = guid1;
                            query += $@"('{guid}',";
                            foreach (var column in columns)
                            {
                                var temp = pr.GetPropValue<object>(column);
                                var temp1 = string.Empty;

                                try
                                {
                                    if (temp != null)
                                    {
                                        temp1 = temp.ToString().Replace("'", " ");
                                        //temp1 = Regex.Replace(temp1, @"\p{C}+", string.Empty);
                                        if (column.ToUpper().Equals("MATERIAL_CODE") || column.ToUpper().Equals("SERVICE_CODE") && !string.IsNullOrEmpty(temp1))
                                        {
                                            temp1 = temp1.TrimStart(new Char[] { '0' });
                                        }

                                        if (temp1.ToString().ToLowerInvariant() == "true")
                                        {
                                            temp1 = "1";
                                        }
                                        else if (temp1.ToString().ToLowerInvariant() == "false")
                                        {
                                            temp1 = "0";
                                        }
                                    }
                                }
                                catch
                                {
                                    logger.Error("PR Row Failed Value is>>>", temp1);
                                }

                                row[column] = temp1;
                                query += $"'{temp1}',";
                            }

                            processedRows++;
                            if (processedRows < chunk.Count())
                            {
                                query += $@"GETUTCDATE()),";
                                query = query + Environment.NewLine;
                            }
                            else
                            {
                                query += $@"GETUTCDATE())";
                            }

                            row["DATE_CREATED"] = DateTime.UtcNow;
                            dt.Rows.Add(row);
                        }

                        query = query.TrimEnd(',');
                        query += ";";
                        logger.Debug("END GetPRDetails QUERY: " + query);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            bizClass.BulkInsert(dt, "[dbo].[SAP_PR_DETAILS]", columnMappings);
                        }

                        dt.Rows.Clear();
                        //bizClass.ExecuteNonQuery_IUD(query);
                        count++;
                    }

                    if (prs != null && prs.Length > 0)
                    {
                        SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                        sd.Add("P_JOB_ID", guid);
                        bizClass.SelectList("erp_process_sap_pr_details", sd);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace, "GETPRDETAILS ERROR");
            }
        }

        private static void GetPODetails()
        {
            try
            {
                string getPOParams = ConfigurationManager.AppSettings["POParams"];
                logger.Info("getPOParams:" + getPOParams);
                string dates = getPOParams.Split(';')[0];
                string fromDate = dates.Split(':')[0];
                string toDate = dates.Split(':')[1];
                logger.Info("fromDate:" + fromDate);
                logger.Info("toDate:" + toDate);
                string plants = getPOParams.Split(';')[1];
                string fromPlant = "";
                string toPlant = "";
                if (plants.Equals("ALL", StringComparison.InvariantCultureIgnoreCase))
                {
                    plants = "ALL";
                }
                else if (plants.Contains(":"))
                {
                    fromPlant = plants.Split(':')[0];
                    toPlant = plants.Split(':')[1];
                }

                logger.Debug("START GetPODetails: " + DateTime.Now.ToString());
                string newURL = ConfigurationManager.AppSettings["Host"].ToString() + "/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_PRM360&receiverParty=&receiverService=&interface=SI_PODetails_Sync_Out&interfaceNamespace=http%3A%2F%2Fbiocon.com%2FPRM360%2FSAP%2FPODetails";
                NetworkCredential netCredential = new NetworkCredential(ConfigurationManager.AppSettings["UserId"].ToString(), ConfigurationManager.AppSettings["UserPwd"].ToString());
                Uri uri = new Uri(newURL);
                ICredentials credentials = netCredential.GetCredential(uri, "Basic");
                PO.SI_PODetails_Sync_OutService test = new PO.SI_PODetails_Sync_OutService();
                test.Url = newURL;
                test.Credentials = credentials;
                test.PreAuthenticate = true;
                test.Timeout = Convert.ToInt32(ConfigurationManager.AppSettings["TimeOut"]);

                List<PO.ZSBSART> S_BSART = new List<PO.ZSBSART>();
                S_BSART.Add(new PO.ZSBSART()
                {
                    SIGN = "I",
                    OPTION = "EQ",
                    LOW = "ZPSR",
                    HIGH = ""
                });
                S_BSART.Add(new PO.ZSBSART()
                {
                    SIGN = "I",
                    OPTION = "EQ",
                    LOW = "ZPDM",
                    HIGH = ""
                });
                S_BSART.Add(new PO.ZSBSART()
                {
                    SIGN = "I",
                    OPTION = "EQ",
                    LOW = "ZSIM",
                    HIGH = ""
                });
                S_BSART.Add(new PO.ZSBSART()
                {
                    SIGN = "I",
                    OPTION = "EQ",
                    LOW = "ZSBW",
                    HIGH = ""
                });
                S_BSART.Add(new PO.ZSBSART()
                {
                    SIGN = "I",
                    OPTION = "EQ",
                    LOW = "ZSSR",
                    HIGH = ""
                });
                S_BSART.Add(new PO.ZSBSART()
                {
                    SIGN = "I",
                    OPTION = "EQ",
                    LOW = "ZSDM",
                    HIGH = ""
                });

                List<PO.ZSEBELN> S_EBELN = new List<PO.ZSEBELN>();
                S_EBELN.Add(new PO.ZSEBELN()
                {
                    SIGN = "",
                    OPTION = "",
                    LOW = "",
                    HIGH = ""
                });

                List<PO.ZSEINDT> S_EINDT = new List<PO.ZSEINDT>();
                if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["IND_FLAG"].ToString()))
                {
                    S_EINDT.Add(new PO.ZSEINDT()
                    {
                        SIGN = "I",
                        OPTION = "BT",
                        LOW = fromDate,
                        HIGH = toDate
                    });
                }

                List<PO.ZSERNAM> S_ERNAM = new List<PO.ZSERNAM>();
                S_ERNAM.Add(new PO.ZSERNAM()
                {
                    SIGN = "",
                    OPTION = "",
                    LOW = "",
                    HIGH = ""
                });

                List<PO.ZSLIFNR> S_LIFNR = new List<PO.ZSLIFNR>();
                S_LIFNR.Add(new PO.ZSLIFNR()
                {
                    SIGN = "",
                    OPTION = "",
                    LOW = "",
                    HIGH = ""
                });

                List<PO.ZSMTART> S_MTART = new List<PO.ZSMTART>();
                S_MTART.Add(new PO.ZSMTART()
                {
                    SIGN = "",
                    OPTION = "",
                    LOW = "",
                    HIGH = ""
                });

                List<PO.ZSWERKS> S_WERKS = new List<PO.ZSWERKS>();

                logger.Info("plants:" + plants);
                if (plants.Equals("ALL", StringComparison.InvariantCultureIgnoreCase))
                {
                    S_WERKS.Add(new PO.ZSWERKS()
                    {
                        SIGN = "I",
                        OPTION = "BT",
                        LOW = "5100",
                        HIGH = "5199"
                    });
                }
                else if (!string.IsNullOrEmpty(fromPlant) && !string.IsNullOrEmpty(toPlant))
                {
                    S_WERKS.Add(new PO.ZSWERKS()
                    {
                        SIGN = "I",
                        OPTION = "BT",
                        LOW = fromPlant,
                        HIGH = toPlant
                    });
                }
                else if (plants.Contains(","))
                {
                    foreach (var plant in plants.Split(','))
                    {
                        S_WERKS.Add(new PO.ZSWERKS()
                        {
                            SIGN = "I",
                            OPTION = "EQ",
                            LOW = plant,
                            HIGH = ""
                        });
                    }

                }
                else
                {
                    S_WERKS.Add(new PO.ZSWERKS()
                    {
                        SIGN = "I",
                        OPTION = "EQ",
                        LOW = plants,
                        HIGH = ""
                    });
                }


                var pos = test.SI_PODetails_Sync_Out(ConfigurationManager.AppSettings["IND_FLAG"].ToString(), S_BSART.ToArray(), S_EBELN.ToArray(), S_EINDT.ToArray(), S_ERNAM.ToArray(), S_LIFNR.ToArray(), S_MTART.ToArray(), S_WERKS.ToArray());

                logger.Debug("TOTAL COUNT GetPODetails: " + (pos != null ? pos.Count().ToString() : "0"));
                logger.Debug("END GetPODetails: " + DateTime.Now.ToString());

                List<string> columns = @"LTEXT;ZCOSTSAV;ZCOSTSAVSpecified;WAERS;ZCUSTOMERNAME;ZZGATE_ENTRY;BUDAT;MENGE;MENGESpecified;WRBTR;WRBTRSpecified;MATKL;MTART;MAKTX;MATNR;NETPR;NETPRSpecified;ZTERM;MENGE1;MENGE1Specified;WERKS;ERNAM;EINDT;BUDAT1;BEDAT;UDATE;UDAT1;EBELP;MENGE2;MENGE2Specified;MEINS;EFFWR;EFFWRSpecified;ERDAT;ERDAT1;ERDAT2;UTIME;UTIME1;BNFPO;MENGE3;MENGE3Specified;MEINS2;POST1;PRART;EBELN;BANFN;EKGRP;REASON_FOR_DELAY_1;REASON_FOR_DELAY_2;REASON_FOR_DELAY_3;REASON_FOR_DELAY_4;NAME1;POST2;PS_PSP_PNR;BSTYP;KDATB;KDATE;AEDAT;AEDAT1;ELIKZ;LOEKZ;ZZGATE_ENT_TIM;CPUTM;BWART".Split(';').ToList();

                if (pos != null && pos.Length > 0 && writeToDB)
                {
                    DataTable dt = new DataTable();
                    dt.Clear();
                    dt.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                    foreach (var column in columns)
                    {
                        dt.Columns.Add(column);
                    }

                    dt.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                    }

                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    int count = 1;
                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    var totalChunks = pos.AsEnumerable().ToList().Chunk(500);
                    foreach (var chunk in totalChunks)
                    {
                        dt.Rows.Clear();
                        logger.Debug($"Reading :  Chunk: {count}");
                        string query = @"INSERT INTO [dbo].[SAP_PO_DETAILS] VALUES ";
                        foreach (var po in chunk)
                        {
                            var row = dt.NewRow();
                            row["JOB_ID"] = guid1;
                            query += $@"('{guid}',";
                            foreach (var column in columns)
                            {
                                var temp = po.GetPropValue<object>(column);
                                if (column.ToUpper().Contains("SPECIFIED"))
                                {
                                    temp = Convert.ToBoolean(temp) ? 1 : 0;
                                }
                                var temp1 = string.Empty;
                                try
                                {
                                    if (temp != null)
                                    {
                                        temp1 = temp.ToString().Replace("'", "''");
                                    }
                                }
                                catch
                                {
                                    logger.Error("PO Row Failed Value is>>>", temp1);
                                }

                                row[column] = temp1;
                                query += $"'{temp1}',";
                            }

                            query += $@"GETUTCDATE()),";
                            row["DATE_CREATED"] = DateTime.UtcNow;
                            dt.Rows.Add(row);
                        }

                        query = query.TrimEnd(',');
                        query += ";";
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            bizClass.BulkInsert(dt, "[dbo].[SAP_PO_DETAILS]", columnMappings);
                        }
                        
                        dt.Rows.Clear();
                        //bizClass.ExecuteNonQuery_IUD(query);
                        count++;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace, "GETPODETAILS ERROR");
            }
        }
    }

    public static class ObjExtensions
    {
        public static Object GetPropValue(this Object obj, String name)
        {
            foreach (String part in name.Split('.'))
            {
                if (obj == null) { return null; }

                Type type = obj.GetType();
                PropertyInfo info = type.GetProperty(part);
                if (info == null) { return null; }

                obj = info.GetValue(obj, null);
            }
            return obj;
        }

        public static T GetPropValue<T>(this Object obj, String name)
        {
            Object retval = GetPropValue(obj, name);
            if (retval == null) { return default(T); }

            // throws InvalidCastException if types are incompatible
            return (T)retval;
        }

    }
}
