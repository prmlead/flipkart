﻿using PRM.Core.Domain.Configurations.Requirements;
using PRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Services.Configurations
{
   public class RequirementFormService : IRequirementFormService
    {

        public RequirementFormService(IRepository<RequirementForm> requirementFormRepository)
        {
            this._requirementFormRepository = requirementFormRepository;

        }

        private readonly IRepository<RequirementForm> _requirementFormRepository;

        public IList<RequirementForm> GetRequirementForm(int companyId)
        {
            return _requirementFormRepository.GetList(new { Company_Id = companyId, Deleted = false }).ToList();
        }

        public void InsertRequirementFormField(RequirementForm entity)
        {
            entity.Id = _requirementFormRepository.Insert(entity);
        }

        public void UpdateRequirementFormField(RequirementForm entity)
        {
            _requirementFormRepository.Update(entity);
        }

        public void DeleteRequirementFormField(RequirementForm entity)
        {
            _requirementFormRepository.Delete(entity);
        }

        public RequirementForm GetRequirementFormField(int id)
        {
            return _requirementFormRepository.GetById(id);
        }


    }
}
