﻿using Dapper;
using PRM.Core.Domain.Acl;
using PRM.Core.Domain.Requirments;
using PRM.Core.Pagers;
using PRM.Data;
using PRM.Services.Acl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Services.Requirements
{
   public class RequirementTemplateService  : IRequirementTemplateService
    {

        public RequirementTemplateService(IRepository<RequirementTemplate> RequirementTemplateRep,
          IDbProvider dbProvider, IAclMappingService aclMappingService)
        {
            _RequirementTemplateRep = RequirementTemplateRep;
            _dbProvider = dbProvider;
            _aclMappingService = aclMappingService;
        }

        private readonly IRepository<RequirementTemplate> _RequirementTemplateRep;
        private readonly IDbProvider _dbProvider;
        private readonly IAclMappingService _aclMappingService;
        public void DeleteRequirementTemplate(RequirementTemplate entity)
        {
            _RequirementTemplateRep.Delete(entity);
        }

        public RequirementTemplate GetRequirementTemplateById(int id)
        {

            var entity = _RequirementTemplateRep.GetById(id);

            var mapper = _aclMappingService.GetAclMappingList(new { EntityName = "RequirementTemplate", EntityId = entity.Id });
            if (entity.LimitedToDepartment)
                entity.AllowedDepartments = mapper.Where(e => e.Type == (int)AclMappingType.Department).Select(e => e.Value).ToArray();

            return entity;
        }

        public IPagedList<RequirementTemplate> GetRequirementTemplateList(Pager pager, int companyId)
        {
            using (var con = _dbProvider.Connection())
            {
                var query = "SELECT SQL_CALC_FOUND_ROWS  * FROM requirement_template";

                List<String> WhereColumns = new List<string>();

                WhereColumns.Add(string.Format(" Company_Id = {0} ", companyId));
                WhereColumns.Add(string.Format(" Deleted = {0}", 0));

                if (!string.IsNullOrEmpty(pager.Criteria.SearchTerm))
                {
                    WhereColumns.Add(string.Format(" Name Like %{0}%", pager.Criteria.SearchTerm));
                }

                if (pager.Criteria.Filter.Any())
                {

                }

                if (WhereColumns.Any())
                    query = query + string.Format(" WHERE {0}", string.Join(" AND ", WhereColumns));

                query = query + " ORDER BY CreatedOnUtc DESC ";

                if (pager.PageIndex >= 0 && pager.PageSize > 0)
                {
                    query = query + string.Format(" LIMIT {0},{1} ", pager.PageIndex * pager.PageSize, pager.PageSize);
                }

                query += " ; SELECT FOUND_ROWS()";

                var multi = con.QueryMultiple(query);

                var lst = multi.Read<RequirementTemplate>().ToList();
                int total = multi.Read<int>().SingleOrDefault();

                return new PagedList<RequirementTemplate>(lst, pager.PageIndex, pager.PageSize, total);
            }
        }

        public IList<RequirementTemplate> GetRequirementTemplateList(object filter)
        {
            return _RequirementTemplateRep.GetList(filter).ToList();
        }

        public void InsertRequirementTemplate(RequirementTemplate entity)
        {
            var id = _RequirementTemplateRep.Insert(entity);
            entity.Id = id;
            INsertMapping(entity);

        }

        public void UpdateRequirementTemplate(RequirementTemplate entity)
        {
            _RequirementTemplateRep.Update(entity);

            var mapper = _aclMappingService.GetAclMappingList(new { EntityName = "RequirementTemplate", EntityId = entity.Id });

            foreach (var map in mapper)
                _aclMappingService.DeleteAclMapping(map);

            INsertMapping(entity);


        }

        private void INsertMapping(RequirementTemplate entity)
        {
            if (entity.LimitedToDepartment)
            {
                _aclMappingService.InsertDepartmentMapping(entity.Company_Id, "RequirementTemplate", entity.Id, entity.AllowedDepartments);
            }


        }

    }
}
