﻿using PRM.Core.Domain.Masters.GeneralTerms;
using PRM.Core.Pagers;
using System.Collections.Generic;

namespace PRM.Services.Masters.GeneralTerms
{
    public interface IGeneralTermService
    {
        void InsertGeneralTerm(GeneralTerm entity);

        void UpdateGeneralTerm(GeneralTerm entity);

        void DeleteGeneralTerm(GeneralTerm entity);

        GeneralTerm GetGeneralTermById(int id);

        IList<GeneralTerm> GetGeneralTermList(object filter);

        IPagedList<GeneralTerm> GetGeneralTermList(Pager pager,int companyId);
    }
}